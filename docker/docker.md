# Docker

Docker 是一个[开源](https://baike.baidu.com/item/开源/246339)的应用容器引擎，让开发者可以打包他们的应用以及依赖包到一个可移植的镜像中，然后发布到任何流行的 [Linux](https://baike.baidu.com/item/Linux)或[Windows](https://baike.baidu.com/item/Windows/165458) 机器上，也可以实现[虚拟化](https://baike.baidu.com/item/虚拟化/547949)。容器是完全使用[沙箱](https://baike.baidu.com/item/沙箱/393318)机制，相互之间不会有任何接口。

解决了部署时环境难以控制，软件版本难以控制，而导致项目无法正常运行的问题。Docker内核级别的虚拟化。



## 一、Docker 简介

Doceker课程：

- https://www.bilibili.com/video/BV1zR4y1t7Wj?p=166&vd_source=f603c8704f45c1725a1123d19f6f0069
- 狂神说 https://www.bilibili.com/video/BV1og4y1q7M4/?spm_id_from=333.999.0.0



### 1、历史

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/docker历史.png" style="float:left;" />



### 2、文档

Docker基于Go语言开发。开源项目。

官方文档：https://docs.docker.com/

docker hub：https://hub.docker.com/



### 3、虚拟机技术 容器化技术



**虚拟机技术**：模拟完整的操作系统。资源占用多，不必要的软件程序多，启动慢。

**容器化技术**：不是模拟完整的操作系统。只有核心代码，必须的程序与依赖，直接运行在宿主机，宿主机只需要一个内核运行代码即可，启动快，容器之间互相隔离。



### 4、DevOps

- **更快速的交付和部署**

  传统方式：一堆帮助文档，运维人员根据帮助文档配置环境，安装程序

  Docker：打包镜像发布程序，一键完成

- **更便捷的升级和扩缩容**

  使用Docker之后，部署的应用就像搭积木一样，直接将新的程序打包发布就可以了。不必要像传统方式一样，一个软件一个软件的去升级，调试。

- **更简单的系统运维**

  测试环境与发布环境高度一致，不用担心环境不一致的问题。

- **更高效资源利用**

  Docker内核级别的虚拟化。可以在一个物理机上运行多个容器实例，服务器的性能可以压榨到极致。



## 二、Docker 安装

### 1、docker 架构

> Docker uses a client-server architecture. The Docker *client* talks to the Docker *daemon*, which does the heavy lifting of building, running, and distributing your Docker containers. The Docker client and daemon *can* run on the same system, or you can connect a Docker client to a remote Docker daemon. The Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface.

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/docker架构.png" style="float:left;" />

**镜像（image）**

docker镜像就好比一个模板，可以通过这个模板来创建容器服务。通过这个镜像可以创建多个容器服务。

**容器（container）**

docker利用容器技术，独立运行一个或者一组应用，通过镜像来创建。可以将容器理解为建议的linux系统，可以对应用进行命令操作。

**仓库（repository）**

存放镜像的地方，仓库分为公有仓库和私有仓库。



### 2、安装 卸载 docker

https://docs.docker.com/engine/install/centos/

安装

```bash
# 1、清理老版本的docker
yum remove docker \
           docker-client \
           docker-client-latest \
           docker-common \
           docker-latest \
           docker-latest-logrotate \
           docker-logrotate \
           docker-engine
           
# 2、配置镜像仓库
yum install -y yum-utils

yum-config-manager \
    --add-repo \
    http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo # 推荐阿里云镜像仓库
    
    https://download.docker.com/linux/centos/docker-ce.repo # 官方仓库
 
# 3、更新yum软件列表
yum makecache fast

# 4、安装docker依赖 ce表示社区版的 ee表示企业版的
yum install docker-ce docker-ce-cli containerd.io

# 5、查看docker版本
docker version

# 6、启动docker
systemctl start docker

# 7、测试 hello-world 镜像，没有会从仓库拉去镜像，运行，出现Hello from Docker！表示安装成功
docker run hello-world

# 8、查看本地镜像文件
docker images
```

卸载

```bash
yum remove docker-ce docker-ce-cli containerd.io

rm -rf /var/lib/docker
```



### 3、阿里云镜像加速

登录阿里云服务网站，找到容器镜像服务 ---> 镜像加速器 ---> centos

在linux上一次执行以下命令，即可

```bash
sudo mkdir -p /etc/docker

sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://zt6ylzcw.mirror.aliyuncs.com"]
}
EOF

sudo systemctl daemon-reload

sudo systemctl restart docker
```



### 4、底层原理

Docker 为什么比虚拟机快？

- **Docker拥有更少的抽象层**，由于Docker不需要Hypervisor实现硬件资源虚拟化，运行在Docker容器上的程序直接使用的都是实际物理机的硬件资源，因此在Cpu、内存利用率上Docker将会在效率上有明显优势。
- **Docker利用宿主机的内核**，而不需要Guest OS，因此，当新建一个容器时，Docker不需要和虚拟机一样重新加载一个操作系统，避免了引导、加载操作系统内核这个比较费时费资源的过程，当新建一个虚拟机时，虚拟机软件需要加载Guest OS，这个新建过程是分钟级别的，而Docker由于直接利用宿主机的操作系统则省略了这个过程，因此新建一个Docker容器只需要几秒钟。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/docker底层.jpg" style="float:left;" />



## 三、Docker 命令

https://docs.docker.com/reference/

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/docker命令图.png" style="float:left;" />



### 1、镜像基本命令

**搜索**

```bash
## 1、搜索仓库应用
docker search <应用程序镜像名>

## 2、条件搜索，根据stars搜索。查找stars大于3000的应用程序
docker search <应用程序镜像名> --filter=stars=3000
```



**安装、卸载**

```bash
## 2、安装镜像.不加tag，默认安装最新版本
docker pull <应用程序镜像名>

## 3、安装指定版本
docker pull <应用程序镜像名>:<tag>
# 比如： docker pull myslq:5.7

## 4、卸载镜像 rm删除 i镜像
docker rmi <IMAGE ID>

## 删除查询出来的所有镜像
docker rmi $(docker images -q)
```

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/docker安装mysql软件01.png" style="float:left;" />

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/docker安装mysql软件02.png" style="float:left;" />

通过以上安装两个mysql，发现资源公用，这时docker最亮眼的一个技术，可以大大节约资源。



**查询镜像**

```bash
## 查看所有镜像文件
docker images

## 查看所有镜像文件
docker images -a

## 只查看镜像文件的image id
docker images -q
```

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/docker镜像列表.png)

**PEPOSITORY** ：镜像应用程序名称

**TAG** ：镜像应用程序版本号，lastest最新版

**IMAGE ID**：镜像应用程序唯一id

**CERATED**：创建时间

**SIZE**：镜像应用程序大小



**镜像导入、导出**

```bash
# 镜像从docker中导出 -o  === out
docker save -o /root/[自行取导出的镜像名.img] [Image ID]


# 将本地镜像导入到docker中 -i === in
docker load -i [镜像文件]
# 修改镜像名称、tag
docker tag [Image ID] [镜像名]:[版本]
```



### 2、容器基本命令

**创建一个centos容器**

```bash
docker pull centos
```



**启动、退出、进入容器**

```bash
docker run [可选参数] <镜像应用程序名>
# 如: docker run -d -p 8080:8080 --name tomcat fa6a  # fa6a是镜像ID

## 参数说明
--name="Name"  # 容器名字
-d             # 后台方式运行
-it            # 启动，进入交互模式，进入容器查看内容
-p             # 小p，指定容器端口号，也可以主机端口和容器端口的映射 主机端口号映射到容器端口号
                 # -p ip:主机端口:容器端口
                 # -p 主机端口:容器端口
                 # -p 容器端口
-P             # 大P，随机指定端口号


## 启动容器
[root@turbo30 ~] docker run -it centos /bin/bash
[root@2de499bc6971 /]

## 停止、重启
docker stop <容器id>
docker restart <容器id>
docker kill <容器id> 


## 退出容器
exit  # 直接退出并关闭容器
crtl+p+q  # 退出不关闭容器

## 进入容器
docker exec -it <容器id> bash

# 将宿主机的文件拷贝到容器的指定目录下
docker cp [文件] [容器id]:[拷贝的目录位置]
# 如：docker cp demon 88:/usr/local/tomcat/webapps

## 进入容器正在执行的命令行
docker attach <容器id>
```



**查看容器日志**

```bash
docker logs -f [容器ID]
```





**查看容器**

```bash
## 查看当前正在运行的容器
docker ps

## 查看正在运行以及之前运行的容器 详细信息
docker ps -a

## 查看正在运行容器的id
docker ps -q

## 查看正在运行以及之前运行的容器id
docker ps -aq
```



**删除容器**

```bash
## 如果该容器正在运行，那么就需要先停止该容器
docker stop <id>

## 删除容器
docker rm <id>
## 批量删除所有容器
docker rm $(docker ps -a)
```



### 3、其他基本命令



**显示日志**

```bash
# 显示末尾10行日志
docker logs -tf --tail 10 <容器id>
```



**查看容器内部进程信息**

```bash
docker top <容器id>
```



**查看容器详细信息**

```bash
docker inspect <容器id>
```



**进入容器拷贝文件到宿主机**

```bash
## 从docker容器中拷贝文件
docker cp 容器id:容器内路径 目的宿主机文件路径

## docker cp diej234f00e:/home/test.java /home
## 将diej234f00e容器中/home/test.java文件拷贝到宿主机/home目录下
```



**查看docker内存消耗状况**

```bash
docker stats
```



**查看镜像文件构建过程**

```bash
docker history <镜像id>
```



**复制一份镜像并重新命名**

```bash
docker tag <镜像id> <镜像名>:<TAG>
```



**开机自启容器**

```bash
docker update <容器> --restart=always
```





### 4、docker 可视化

portainer 可视化工具

```bash
docker run -d -p 8088:9000 --restart=always -v /var/run/docker.sock:/var/run/docker.sock --privileged=true portainer/portainer
```

访问8088端口即可



## 四、镜像加载原理

> UnionFS(联合文件系统)

拉去镜像文件时，一层一层的数据就是这个。

UnionFS（联合文件系统）：是一种分层、轻量级并且高性能的文件系统，它支持对文件系统的修改作为一次提交来一层层的叠加，同时可以将不同目录挂载到同一个虚拟文件系统下。Union文件系统时Docker镜像基础。镜像可以通过分层进行继承，基于基础镜像（没有父镜像），可以制作各种具体的应用镜像。

特性：一次同时加载多个文件系统，但从外面看起来，只能看到一个文件系统，联合加载会把各层文件系统叠加起来，这样最终文件系统会包含所有底层的文件和目录。

另外，不同 Docker 容器就可以共享一些基础的文件系统层，同时再加上自己独有的改动层，大大提高了存储的效率。



> Docker 镜像加载原理

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/镜像加载原理.png" style="float:left;" />



​		**Bootfs**（boot-file system）主要包含bootloader和kernel，bootloader主要是引导加载kernel，Linux刚启动时会加载bootfs文件系统，在Docker镜像的最底层是bootfs，这一层与我们典型的Linux/unix系统是一样的，包含boot加载器和内核，当boot加载完成之后整个内核就能在内存中了，此时内存的使用权已由bootfs转交给内核，此时系统也会卸载bootfs。

　　**Rootfs**（root-file system），在bootfs之上，包含的就是典型Linux系统中的/dev、/proc、/bin、/etc等标准目录和文件，rootfs就是各种不同操作系统的发行版，比如Ubuntu，Centos等等。

　　对于一个精简的OS，rootfs可以很小，只需要包括最基本的命令、工具和程序就可以了，因为底层直接用宿主机的内核，自己只需要提供rootfs就可以了，因此可见，对于不用的Linux发行版，bootfs基本是一致的，而rootfs会有差别，因此不同的发行版可以公用bootfs。



## 五、Commit 镜像

```bash
docker commit -a="作者" -m="提交描述" <容器id> 镜像名:TAG版本号
```



> 案列：
>
> ​		将从docker仓库拉去下来的tomcat镜像，给webapps文件添加项目，然后发布该容器

```bash
# 拉去tomcat
docker pull tomcat 

# 启动tomcat容器
docker run -d -it -p 8080:8080 tomcat /bin/bash

# webapps中添加项目，将官方tomcat中webapps.dist目录下的项目拷贝到webapps目录下，这样就有默认项目了
cp -r webapps.dist/* webapps

# 测试是否能访问apache tomcat
curl localhost:8080

# 打包容器成为镜像
docker commit -a="heroc" -m="add app in webapps" dd9810554756 tomcat_per:1.0

# 查看打包结果
docker images
```

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/commit镜像.png" style="float:left;" />



如果你需要当前容器的状态，可以commit一个镜像，保存下来。



## 六、容器数据卷

为了解决容器被删了之后数据随之也丢失，数据持久化；为了解决容器之间可以有一个数据共享的技术，就引入了容器数据卷。

将容器中的数据同步到宿主机的目录中。

宿主机创建了数据卷，将其映射到容器中。数据卷改变，对应的容器就相应的改变。就不用反复的使用`docker cp`命令将宿主机的文件拷贝到容器中。



### 创建、查看、删除数据卷

```bash
# 创建名为v01的数据卷
docker volume create v01

# 查看v01数据卷信息
docker volume inspect v01

# --- v01数据卷信息
[
    {
        "CreatedAt": "2023-01-15T18:14:34+08:00",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/v01/_data", // 数据卷文件存放位置
        "Name": "v01",
        "Options": {},
        "Scope": "local"
    }
]

# 查看所有的数据卷
docker volume ls

# 删除数据卷
docker volume rm [数据卷名]
```





### 1、使用数据卷

> 方式一：启动容器时，直接使用 -v 命令

```bash
# 挂载，通过 -v 命令将指定的容器地址的内容挂载到指定的主机地址
docker run -it -v [主机数据卷地址|数据卷名称]:容器地址 <镜像应用程序名> 启动方式


# 运行centos，将容器centos中/home目录下的文件挂载到宿主机/home/ceshi目录下
# 在宿主机/home/ceshi文件下添加删除文件也会同步到容器/home下
docker run -it -v /home/ceshi:/home centos /bin/bash
```



### 2、实战：MySQL数据同步

```bash
# 获取mysql镜像
docker pull mysql:5.7

# 运行容器，后台运行，映射端口，挂载目录，环境配置 配置密码为123456，取名为mysql01
docker run -d -p 3310:3306 -v /home/mysql/conf:/etc/mysql/conf.d -v /home/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 --name="mysql01" mysql:5.7
# 将mysql配置文件和data数据挂载到宿主机，数据持久化
```



### 3、具名和匿名挂载

所有的卷都在`/var/lib/docker/volumes`目录下



**匿名挂载：**

```bash
## 匿名挂载，只指定了容器内的目录
docker run -d -P --name="nginx01" -v /etc/nginx nginx

## 查看挂载列表
[root@turbo30 ~]# docker volume ls
DRIVER              VOLUME NAME
local               3ba73c29a80e67da3bd77fdb08454697006681cd57af404e52a5c1d7f6e44efd

## 查看挂载细节，即可查看挂载目录
[root@turbo30 ~]# docker volume inspect 3ba73c29a80e67da3bd77fdb08454697006681cd57af404e52a5c1d7f6e44efd
[
    {
        "CreatedAt": "2020-08-20T18:38:00+08:00",
        "Driver": "local",
        "Labels": null,
        "Mountpoint": "/var/lib/docker/volumes/3ba73c29a80e67da3bd77fdb08454697006681cd57af404e52a5c1d7f6e44efd/_data",
        "Name": "3ba73c29a80e67da3bd77fdb08454697006681cd57af404e52a5c1d7f6e44efd",
        "Options": null,
        "Scope": "local"
    }
]
```



**具名挂载：**

```bash
## 具名挂载，具体名字挂载，给挂载取一个名字；如以下命令，取名为juming-gz
docker run -d -P --name="nginx02" -v juming-gz:/etc/nginx nginx

## 查看数据卷列表
[root@turbo30 ~]# docker volume ls
DRIVER              VOLUME NAME
local               juming-gz

## 查看具体细节，即挂载目录
[root@turbo30 ~]# docker volume inspect juming-gz
[
    {
        "CreatedAt": "2020-08-20T18:41:33+08:00",
        "Driver": "local",
        "Labels": null,
        "Mountpoint": "/var/lib/docker/volumes/juming-gz/_data",
        "Name": "juming-gz",
        "Options": null,
        "Scope": "local"
    }
]
```



### 4、Dockerfile 实现挂载

**创建一个dockerfile文件，编辑以下命令：**

```shell
FROM nginx

VOLUME ["volume01","volume02"]

CMD echo "===== end ====="

CMD /bin/bash
```

使用nginx镜像，挂载目录volume01和volume02，没有该目录，自动创建.



**创建一个拥有自动挂载脚本的镜像：**

```bash
docker build -f /home/dockerfile -t heroc_nginx:1.0 .
```

将文件dockerfile构建成heroc_nginx:1.0镜像文件



<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/dockerfile生成挂载脚本.png" style="float:left;" />

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/拥有挂载脚本的新镜像生成.png" style="float:left;" />



### 5、拓展：挂载权限

`ro`：read only 只读权限

`rw`：read write 读写权限

只要添加了以上权限，就对应了容器对该目录具有的权限

```bash
## 容器不能对/etc/nginx目录下的文件进行操作，只读。只能通过外部挂载的目录进行修改操作
docker run -d -P --name="nginx01" -v juming-gz:/etc/nginx:ro nginx

## 容器可以对/etc/nginx目录下的文件进行读写操作
docker run -d -P --name="nginx01" -v juming-gz:/etc/nginx:rw nginx
```



### 6、容器之间挂载  容器间数据共享

```bash
# 关键命令
--volumes-from
```



```bash
docekr run -it --name nginx01 heroc_nginx:1.0

## nginx02 与 nginx01 之间挂载，同步数据
docker run -it --name nginx02 --volumes-from nginx01 heroc_nginx:1.0
```

nginx01 容器删除了之后，nginx02同步的数据不会收到影响。文件是双向拷贝的概念。



多个容器之间挂载的目录数据同步之mysql：

```bash
docker run -d -p 3310:3306 -v /etc/mysql/conf.d -v /var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 --name mysql01 mysql:5.7


docker run -d -p 3310:3306 -e MYSQL_ROOT_PASSWORD=123456 --name mysql02 --volumes-from mysql01 mysql:5.7

## 这时候实现两个容器之间挂载的目录数据同步！！
```



## 七、DockerFile

DockerFile 就是用来构建镜像的文件。就是命令脚本。`官方推荐使用Dockerfile命名构建文件的脚本，这样系统会自动去找Dockerfile这个构建文件脚本`

Dockerfile换行可用`\` 



### 1、DockerFile 指令

#### FROM

功能为指定基础镜像，并且必须是第一条指令。

如果不以任何镜像为基础，那么写法为：**FROM scratch**。

同时意味着接下来所写的指令将作为镜像的第一层开始

语法：

```bash
FROM <image>
FROM <image>:<tag>
FROM <image>:<digest> 
```

三种写法，其中<tag>和<digest> 是可选项，如果没有选择，那么默认值为latest



#### MAINTAINER

指定作者、邮箱信息

语法：

```
MAINTAINER <name&email>
```



#### RUN

是构件容器时就运行的命令以及提交运行结果。功能为运行指定的命令，RUN命令有两种格式

```bash
RUN <command>

RUN ["executable", "param1", "param2"]
```

第一种后边直接跟shell命令

- 在linux操作系统上默认 /bin/sh -c
- 在windows操作系统上默认 cmd /S /C

第二种是类似于函数调用。

可将executable理解成为可执行文件，后面就是两个参数。

两种写法比对：

- ```
  RUN /bin/bash -c 'source $HOME/.bashrc; echo $HOME
  ```

- ```
  RUN ["/bin/bash", "-c", "echo hello"]
  ```

注意：多行命令不要写多个RUN，原因是Dockerfile中每一个指令都会建立一层。 多少个RUN就构建了多少层镜像，会造成镜像的臃肿、多层，不仅仅增加了构件部署的时间，还容易出错。

RUN书写时的换行符是\	



#### CMD

是容器启动时执行的命令，在构件时并不运行。在启动容器时指定的命令会覆盖文件中的cmd命令，只执行最后一条命令。功能为容器启动时要运行的命令，语法有三种写法

```bash
CMD ["executable","param1","param2"]

CMD ["param1","param2"]

CMD command param1 param2
```

第三种比较好理解了，就时shell这种执行方式和写法

第一种和第二种其实都是可执行文件加上参数的形式

举例说明两种写法：

- ```
  CMD [ "sh", "-c", "echo $HOME"]
  ```

- ```
  CMD [ "echo", "$HOME" ]
  ```

补充细节：这里边包括参数的一定要用双引号，千万不能写成单引号。原因是参数传递后，docker解析的是一个JSON array





#### LABEL

功能是为镜像指定标签

语法：

```
LABEL <key>=<value> <key>=<value> <key>=<value> ...
```

 一个Dockerfile中可以有多个LABEL，如下：

```
LABEL "com.example.vendor"="ACME Incorporated"
LABEL com.example.label-with-value="foo"
LABEL version="1.0"
LABEL description="This text illustrates \
that label-values can span multiple lines."
```

 但是并不建议这样写，最好就写成一行，如太长需要换行的话则使用\符号

如下：

```
LABEL multi.label1="value1" \
multi.label2="value2" \
other="value3"
```

<u>说明：LABEL会继承基础镜像种的LABEL，如遇到key相同，则值覆盖</u>



#### EXPOSE

功能为暴漏容器运行时的监听端口给外部。但是EXPOSE并不会使容器访问主机的端口，如果想使得容器与主机的端口有映射关系，必须在容器启动的时候加上 -p 或 -P



#### ENV

功能为设置环境变量，在容器启动时会为容器设置环境变量

语法有两种

```
ENV <key> <value>

ENV <key>=<value> ...
```

两者的区别就是第一种是一次设置一个，第二种是一次设置多个



#### ADD

 一个复制命令，把文件复制到镜像中。

如果把虚拟机与容器想象成两台linux服务器的话，那么这个命令就类似于scp，只是scp需要加用户名和密码的权限验证，而ADD不用。**tar类型的文件会自动解压，可以访问网络资源**

语法如下：

```
ADD <src>... <dest>
ADD ["<src>",... "<dest>"]
```

<dest>路径的填写可以是容器内的绝对路径，也可以是相对于工作目录的相对路径

<src>可以是一个本地文件或者是一个本地压缩文件，还可以是一个url

 如果把<src>写成一个url，那么ADD就类似于wget命令

 如以下写法都是可以的：

- ```
  ADD test relativeDir/ 
  ```

- ```
  ADD test /relativeDir
  ```

- ```
  ADD http://example.com/foobar /
  ```

尽量不要把<src>写成一个文件夹，如果<src>是一个文件夹了，复制整个目录的内容,包括文件系统元数据.<src>为压缩包，会自动解压放到<dest>目录下



#### COPY

看这个名字就知道，又是一个复制命令

语法如下：

```
COPY <src>... <dest>

COPY ["<src>",... "<dest>"]
```

与ADD的区别：COPY的<src>只能是本地文件，其他用法一致。不可以解压，不可以访问网络资源



#### ENTRYPOINT

功能是启动时的默认命令。在启动容器时指定的命令会追加在后面一起执行。

语法如下：

```
ENTRYPOINT ["executable", "param1", "param2"]

ENTRYPOINT command param1 param2
```

如果从上到下看到这里的话，那么你应该对这两种语法很熟悉啦。

第二种就是写shell

第一种就是可执行文件加参数

例如：

```bash
ENTRYPOINT ["ls","-a"]
```

在启动容器时：

```bash
docker run -it <image> -l
实则执行的是：
docker run -it <image> ls -a -l # 追加执行命令
```

而CMD如果在启动容器时使用-l命令，那么就会覆盖原有的所有命令，直接执行-l就会报错。

```bash
CMD ["ls","-a"]

docker run -it <image> -l
实则执行的是：
docker run -it <image> -l # 报错
如果要执行-l应该为：
docker run -it <image> ls -l
```





#### VOLUME

可实现挂载功能，可以将容器中/data目录下的文件挂载到宿主机中。

语法为：

```
VOLUME ["/data"]
```

说明：

  ["/data"]可以是一个JsonArray ，也可以是多个值。所以如下几种写法都是正确的

```
VOLUME ["/var/log/"]
VOLUME /var/log
VOLUME /var/log /var/db
```

一般的使用场景为需要持久化存储数据时。

容器使用的是AUFS，这种文件系统不能持久化数据，当容器关闭后，所有的更改都会丢失。

所以当数据需要持久化时用这个命令。



##### volume和run -v的区别，什么时候需要使用volume

容器运行时应该尽量保持容器存储层不发生写操作，对于数据库类需要保存动态数据的应用，其数据库文件应该保存于卷(volume)中。为了防止运行时用户忘记将动态文件所保存目录挂载为卷，在Dockerfile 中，我们可以事先指定某些目录挂载为匿名卷，这样在运行时如果用户不指定挂载，其应用也可以正常运行，不会向容器存储层写入大量数据。

那么`Dockerfile`中的`VOLUME`指令实际使用中是不是就是跟`docker run`中的`-v`参数一样是将宿主机的一个目录绑定到容器中的目录以达到共享目录的作用呢？
并不然，其实`VOLUME`指令只是起到了声明了容器中的目录作为`匿名卷`，但是并没有将匿名卷绑定到宿主机指定目录的功能。
当我们生成镜像的`Dockerfile`中以`Volume`声明了匿名卷，并且我们以这个镜像run了一个容器的时候，docker会在安装目录下的指定目录下面生成一个目录来绑定容器的匿名卷（这个指定目录不同版本的docker会有所不同），我当前的目录为：`/var/lib/docker/volumes/{容器ID}`。

**总结**： `volume`只是指定了一个目录，用以在用户忘记启动时指定`-v`参数也可以保证容器的正常运行。比如mysql，你不能说用户启动时没有指定`-v`，然后删了容器，就把mysql的数据文件都删了，那样生产上是会出大事故的，所以mysql的dockerfile里面就需要配置`volume`，这样即使用户没有指定`-v`，容器被删后也不会导致数据文件都不在了。还是可以恢复的。



##### volume指定的位置在容器被删除以后数据文件会被删除吗

`volume`与`-v`指令一样，容器被删除以后映射在主机上的文件不会被删除。



##### 如果-v和volume指定了不同的位置，会发生什么事呢？

会以`-v`设定的目录为准，其实`volume`指令的设定的目的就是为了避免用户忘记指定`-v`的时候导致的数据丢失，那么如果用户指定了`-v`，自然而然就不需要`volume`指定的位置了。



#### WORKDIR

 镜像的工作目录，启动容器通过命令 -it 直接进入WORKDIR指定的目录下

语法：

```
WORKDIR /path/to/workdir
```



#### ONBUILD

这个命令只对当前镜像的子镜像生效。

语法：

```
ONBUILD [INSTRUCTION]
```

比如当前镜像为A，在Dockerfile种添加：

```
ONBUILD RUN ls -al
```

这个 ls -al 命令不会在A镜像构建或启动的时候执行

此时有一个镜像B是基于A镜像构建的，那么这个ls -al 命令会在B镜像构建的时候被执行。



#### ARG

设置变量

`格式： ARG <name>[=<default value>] `



##### 详解

- 在执行 [docker](https://cloud.tencent.com/product/tke?from=10680) build 时，可以通过  来为声明的变量赋值

--build-arg <参数名>=<值>

- 当镜像编译成功后，ARG 指定的变量将不再存在（ENV指定的变量将在镜像中保留）
- Docker内置了一些镜像创建变量，用户可以直接使用而无须声明，包括（不区分大小写）HTTP_PROXY、HTTPS_PROXY、FTP_PROXY、NO_PROXY



##### ARG 和 ENV 的区别

- ARG 定义的变量只会存在于镜像构建过程，启动[容器](https://cloud.tencent.com/product/tke?from=10680)后并不保留这些变量
- ENV 定义的变量在启动容器后仍然保留



##### 注意

不要通过 ARG 保存密码之类的信息，因为  docker history  还是可以看到所有值的



##### ARG 指令有生效范围

如果在 FROM 指令之前指定，那么只能用于 FROM 指令中

```javascript
ARG DOCKER_USERNAME=library

FROM ${DOCKER_USERNAME}/alpine

RUN set -x ; echo ${DOCKER_USERNAME}
```

复制

-  使用上述 Dockerfile 会发现无法输出 ${DOCKER_USERNAME} 变量的值
- 要想正常输出，必须在 FROM 之后再次指定 ARG

```javascript
# 只在 FROM 中生效
ARG DOCKER_USERNAME=library

FROM ${DOCKER_USERNAME}/alpine

# 要想在 FROM 之后使用，必须再次指定
ARG DOCKER_USERNAME=library

RUN set -x ; echo ${DOCKER_USERNAME}
```

复制

多阶段构建的时候，ARG 定义的变量，每个 FROM 都能用

```javascript
# 这个变量在每个 FROM 中都生效
ARG DOCKER_USERNAME=library

FROM ${DOCKER_USERNAME}/alpine

RUN set -x ; echo 1

FROM ${DOCKER_USERNAME}/alpine

RUN set -x ; echo 2
```



### 2、Dockerfile 实战

> 实战：基于tomcat镜像包，将demo.war包拷贝到镜像中，构建一个新的包含demo.war的镜像包
>
> https://www.bilibili.com/video/BV1zR4y1t7Wj?p=178&spm_id_from=pageDriver&vd_source=f603c8704f45c1725a1123d19f6f0069



#### 构建 tomcat 镜像

基于centos镜像，在这个centos镜像中添加jdk、tomcat，配置环境变量，构建成一个含有jdk和tomcat的centos镜像。

jdk和tomcat压缩包在dockerfile文件同目录下

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/dockerfile构建tomcat镜像.png" style="float:left;" />



构建镜像

```bash
docker build -f dockerfile -t hero_tomcat:1.0 .
```



### 3、发布自己的镜像

> 发布到Docker Hub

登录到自己的docker hub账号

```bash
docker login -u <账户名> -p <密码>
```

发布镜像

```bash
docker push <USERNAEM>/<IMAGE>:<TAG>
```



> 发布到阿里云镜像

进入到阿里云，找到容器镜像服务

创建一个命名空间，然后创建一个容器镜像。

根据阿里云提供的步骤发布的自己镜像。



## 八、Docker 网路

### 1、Docker0

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/docker0.png" style="float:left;" />

只要安装了docker，就会分配一个网卡给docker，即docker0，docker 启动一个容器，就会给容器分配一个ip



### 2、容器与宿主机网路关系

启动一个tomcat容器，然后观察tomcat这个容器的内部ip情况

```bash
# 开启一个tomcat容器
docker run -d -P --name tomcat01 tomcat

# 查看这个容器中的ip情况
docker exec -it tomcat01 ip addr
```

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/tomcat01容器ip.png" style="float:left;" />



然后再查看此时宿主机的ip情况：

```bash
ip addr
```

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/tomcat01宿主机ip.png" style="float:left;" />



因此，会发现宿主机的34连接的tomcat01容器的33，而tomcat01容器的33连接的宿主机的34.

并且宿主机可以ping通容器tomcat01分配的ip

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/ping容器tomcat01.png" style="float:left;" />



### 2、Evth-pair 技术

发现容器与宿主机之间网络存在的这种一对一对的关系，就是Evth-pair技术。

这里的Evth-pair技术就充当容器与宿主机之间的通信桥梁。



### 3、容器之间的网络关系

由于容器与宿主机之间通过Evth-pair技术，可以通信。所以docker的每个容器都可以与宿主机进行通信，因此容器与容器之间也可以互相ping通。

Docker中的网络接口都是虚拟的，虚拟的快。



### 4、 --link

```bash
docker run -d -P --name tomcat03 --link tomcat01 tomcat

## tomcat03容器与tomcat01连接，tomcat03 就可以直接ping通过 tomcat01
## tomcat03可以ping通tomcat01，tomcat01不能ping通tomcat03
```



查看tomcat03容器的hosts

```bash
docker exec -it tomcat03 cat /etc/hosts
```

发现tomcat03容器的hosts将tomcat01的ip地址直接写到hosts文件中

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/--link.png" style="float:left;" />

==--link 就是在hosts文件中增加了映射，不建议使用--link==



### 5、自定义网络

```bash
## 查看所有的docker网络
docker network list

## 自定义创建名为mynet的docker网络
## --driver bridge 桥接模式
## --subnet 192.168.0.0/16 子网段
## --gateway 192.168.0.1 网关
docker network create --driver bridge --subnet 192.168.0.0/16 --gateway 192.168.0.1 mynet

## 查看网络详细信息
docker network inspect <名>

## 删除网络
docker network rm <名>

## 启动容器，使用自定义的网络
docker run -d -P --name tomcat-mynet-01 --net mynet tomcat 
```



==自定义网络的好处，所有容器都是用自定义网络，那么该网络中就有这些容器的信息，不适用--link命令，就能直接通过容器名进行互相ping通。而使用默认的docker0网络，分配的容器之间需要--link指定容器才能单方面ping通。==



### 6、容器与网络 连通

docker 的两个网段不能直接连通，且无法连通。如果想使得一个网络中的A容器与另一个网络中的B容器互通的话。只能使得A容器与B容器的网络进行连通。

```bash
# 命令
docker network connect NETWORK CONTAINER

## 通过以下命令，则可以使得A容器与mynet网络连通
## mynet是B容器的网络名，A为A网络中的容器
docker network connect mynet A
```

即A容器拥有两个ip。

本来A容器与B容器不能互相ping通，而通过命令使得A容器又加入到了mynet网络中，使得A容器与B容器可以ping通。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/网络容器互通.png" style="float:left;" />



### 实战：部署Redis集群

配置一个redis网络，分别有3个master，3个salve从机

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/redis集群部署.png" style="float:left;" />

```bash
## 创建redis网络
docker network create redis --subnet 172.38.0.0/16

# 通过脚本创建6个redsi配置
for port in $(seq 1 6); \
do \
mkdir -p /mydata/redis/node-$(port)/conf
touch /mydata/redis/node-$(port)/conf/redis.conf
cat << EOF >/mydata/redis/node-$(port)/conf/redis.conf
port 6379
bind 0.0.0.0
cluster-enabled yes
cluster-config-file nodes.conf
cluster-node-timeout 5000
cluster-announce-ip 172.38.0.1$(port)
cluster-announce-port 6379
cluster-announce-bus-port 16379
appendonly yes
EOF
done

## 根据创建的配置启动redis
docker run -p 637$(port):6379 -p 16371:16379 --name redis-$(port) \
-v /mydata/redis/node-$(port)/data:/data \
-v /mydata/redis/node-$(port)/conf/redis.conf:/etc/redis/redis.conf \
-d --net redis --ip 172.38.0.1$(port) redis:5.0.9-alpine3.11 redis-server /etc/redis/redis.conf


## 6个reids启动了，随便进入一个容器，创建集群
docker exec -it redis-1 /bin/sh

## 进入后创建集群，--cluster-replicas 1 代表每个主机都必须有1个从机，这里有6个redis，那么会自动分配为3个主机3个从机。
redis-cli --cluster create 172.38.0.11:6379 172.38.0.12:6379 172.38.0.13:6379 172.38.0.14:6379 172.38.0.15:6379 172.38.0.16:6379 --cluster-replicas 1
```

## 九、SpringBoot 打包 Docker 镜像

SpringBoot项目通过maven打包成jar包，然后写一个Dockerfile文件

Dockerfile

```bash
FROM java:8  							# 基本镜像java8版本
COPY *.jar /app.jar   					# 将当前目录下的jar包拷贝到容器中的/app.jar里
CMD ["=====SERVER.PORT:8080====="]		# 提示作用
EXPOSE 8080  							# 对外暴露8080端口
ENTRYPOINT ["java","-jar","/app.jar"] 	# 启动容器时，运行java -jar /app.jar
```



docker打包

```bash
docker build -f dockerfile -t heroc_demo
```



## 十、Docker Compose



### 1、简介

官方文档：https://docs.docker.com/compose/

如果一个项目有几百个微服务，就要启动几百个容器，手动启动肯定不现实。所以又要通过Compose去统一启动，形成一个完整的一个项目，Compose会将这几百个微服务规定在同一个网段下，可以通过服务名进行访问。

> **官方介绍：**
>
> Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration. To learn more about all the features of Compose, see [the list of features](https://docs.docker.com/compose/#features).
>
> Using Compose is basically a three-step process:
>
> 1. Define your app’s environment with a `Dockerfile` so it can be reproduced anywhere.
> 2. Define the services that make up your app in `docker-compose.yml` so they can be run together in an isolated environment.
> 3. Run `docker-compose up` and Compose starts and runs your entire app.
>
> A `docker-compose.yml` looks like this:
>
> ```yaml
> version: '3.8' # 版本要求 https://docs.docker.com/compose/compose-file/compose-file-v3/
> services:
>   mysql:					# 服务名称
>     restart: always			# 代表只要docker启动，那么这个容器就跟着启动
>     image: daocloud.io/library/mysql			# 指定镜像,没有就从远程pull
>     container_name: mysql	# 容器名称
>     build: 					# 构建自定义镜像 有build则根据dockerfile构建镜像使用，image就是给镜像起名字；没有build则通过image找到镜像使用。
>     .context: ../			# 指定dockerfile路径
>      dockerfile: Dockerfile	# 指定dockerfile文件名称
>     ports:
>     - "5000:5000"			# 指定端口号的映射
>     volumes:				# 数据卷映射，宿主机与容器
>     - /home/code:/code
>     - logvolume01:/var/log
>     environment:
>       MYSQL_ROOT_PASSWORD: 123456	# 指定mysql的root用户登录密码
>       TZ: Asia/Shanghai				# 时区
>     links:
>     - redis
>   redis:					# 服务器名称
>     image: redis			# 镜像
> volumes:
>   logvolume01: {}
> ```

Compose 是 Docker 的开源项目。需要安装使用。



### 2、安装

下载：

```bash
curl -L https://get.daocloud.io/docker/compose/releases/download/1.25.5/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
```

设置可运行

```bash
chmod +x /usr/local/bin/docker-compose
```

检查版本

```bash
docker-compose --version
```



### 3、Getting Start

https://docs.docker.com/compose/gettingstarted/

通过Compose去统一启动几百个容器，形成一个完整的一个项目，Compose会将这几百个容器规定在同一个网段下，因此可以通过compose的yaml配置文件中services项下的服务名进行互相访问。

yaml:

```yaml
version: '3'
services:
  web:
    build: .  # 通过dockerfile创建的镜像 启动一个web容器
    ports:
      - "5000:5000"
  redis:
    image: "redis:alpine"
```

php:

```php
import time

import redis
from flask import Flask

app = Flask(__name__)
    # host就是通过服务名redis访问
cache = redis.Redis(host='redis', port=6379)

...
```



**停止compose：**

If you started Compose with `docker-compose up -d`, stop your services once you’ve finished with them:

```
$ docker-compose stop
```

You can bring everything down, removing the containers entirely, with the `down` command. Pass `--volumes` to also remove the data volume used by the Redis container:

```
$ docker-compose down --volumes
```

At this point, you have seen the basics of how Compose works.



### 4、Compose 配置规则

官方文档：https://docs.docker.com/compose/compose-file/

服务的相关配置见官网



配置文件大致分为3个部分：

```yaml
# 1，版本号 主要
version: "版本号"
# 2，服务 主要
services: 
	服务1:
		服务1的相关配置
	服务2: 
		服务2的相关配置
		
# 3，其他配置(根据项目来规定) networks、volumes
networks:
  frontend:
  backend:

volumes:
  db-data:
```



例：

```yaml
version: "3.8"
services:

  redis:
    image: redis:alpine
    ports:
      - "6379"
    networks:
      - frontend
    deploy:
      replicas: 2
      update_config:
        parallelism: 2
        delay: 10s
      restart_policy:
        condition: on-failure

  db:
    image: postgres:9.4
    volumes:
      - db-data:/var/lib/postgresql/data
    networks:
      - backend
    deploy:
      placement:
        max_replicas_per_node: 1
        constraints:
          - "node.role==manager"

  vote:
    image: dockersamples/examplevotingapp_vote:before
    ports:
      - "5000:80"
    networks:
      - frontend
    depends_on:
      - redis
    deploy:
      replicas: 2
      update_config:
        parallelism: 2
      restart_policy:
        condition: on-failure

  result:
    image: dockersamples/examplevotingapp_result:before
    ports:
      - "5001:80"
    networks:
      - backend
    depends_on:
      - db
    deploy:
      replicas: 1
      update_config:
        parallelism: 2
        delay: 10s
      restart_policy:
        condition: on-failure

  worker:
    image: dockersamples/examplevotingapp_worker
    networks:
      - frontend
      - backend
    deploy:
      mode: replicated
      replicas: 1
      labels: [APP=VOTING]
      restart_policy:
        condition: on-failure
        delay: 10s
        max_attempts: 3
        window: 120s
      placement:
        constraints:
          - "node.role==manager"

  visualizer:
    image: dockersamples/visualizer:stable
    ports:
      - "8080:8080"
    stop_grace_period: 1m30s
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    deploy:
      placement:
        constraints:
          - "node.role==manager"

networks:
  frontend:
  backend:

volumes:
  db-data:
```



### 5、根据官方文档实例 创建博客

官方文档：https://docs.docker.com/compose/wordpress/



创建一个`my_wordpress`空目录，进入该目录

```bash
cd my_wordpress/
```



创建一个 `docker-compose.yml` 文件

```yaml
version: '3.3'

services:
   db:
     image: mysql:5.7 # 通过image启动一个容器，自己的jar包什么的，可能需要dockerfile创建一个镜像启动
     volumes:
       - db_data:/var/lib/mysql
     restart: always
     environment:
       MYSQL_ROOT_PASSWORD: somewordpress
       MYSQL_DATABASE: wordpress
       MYSQL_USER: wordpress
       MYSQL_PASSWORD: wordpress

   wordpress:
     depends_on:
       - db
     image: wordpress:latest
     ports:
       - "8000:80"
     restart: always
     environment:
       WORDPRESS_DB_HOST: db:3306 # 找服务名为db的服务连接3306端口
       WORDPRESS_DB_USER: wordpress
       WORDPRESS_DB_PASSWORD: wordpress
       WORDPRESS_DB_NAME: wordpress
volumes:
    db_data: {}
```

在当前目录下compose启动 `docker-compose.yml` 文件，成为一个项目

```bash
# 启动
docker-compose up -d

# 关闭
docker-compose down

# 开启、停止、重启
docker-compose start|stop|restart

# 查看由docker-compose管理的容器
docker-compose ps

# 查看日志
docker-compose logs -f
```

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/compose启动容器.png" style="float:left;" />



此时访问8000端口就可以看到部署的博客了。



## 十一、Docker Swarm

https://docs.docker.com/engine/swarm/how-swarm-mode-works/nodes/

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDocker/swarm-diagram.png" style="float:left;" />



### 1、环境准备

准备4台服务器，分别安装docker



### 2、Swarm 集群搭建

```bash
# 初始化swarm ，ip地址通过 ip addr 查看
docker swarm init --advertise-addr <本机ip地址 内网>

# 管理节点上，生成令牌
docker swarm join-token manager  # 获取管理者的令牌，其他服务器通过该令牌，以manager身份加入到集群
docker swarm join-token work     # 获取工作者的令牌，其他服务器通过该令牌，以work身份加入到集群

# 查看所有节点
docker node ls
```



### 3、Raft 协议

**Raft协议：集群中领导者服务器保证大部分可用，服务器必须大于1台，才可用。**

例如集群有2台服务器，有1台服务器挂了，那么只剩下了1台服务器，此时都不可用。所以必须大于3台服务器，建立集群。



### 4、扩缩容

在集群中，对服务进行扩缩容。随机分配到服务器中。

```bash
# 开启服务容器
docker service -d -p 8080:8080 nginx

# 扩缩容 两种方式：
docker service scale <服务名>=<该服务需要达到的扩展数量>

docker service update --relipcas <数量> <服务名>
```



## 十二、搭建私服

1、修改注册表

修改`/etc/docker/daemon.json`文件，若文件不存在，则手动创建。

添加如下内容：

```shlle
{
    ”registry-mirrors“:["https://docker-cn.com"],
    "insecure-registries":["ip:port"]// 替换相应的IP和端口即可
}
```

2、重启服务

```shell
systemctl daemon-reload
systemctl restart docker
```



## QA

### 矫正基于alpine容器的时区问题

```dockerfile
# 接下来创建镜像
FROM alpine:latest

LABEL MAINTAINER="turbochang@126.com"

# 设置时区为上海：由于alpine的时间标准时区，所以这里要下载时间文件更新为上海时区，为了精简然后再把tzdata删除即可。
RUN apk --update add tzdata && \
    cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    echo "Asia/Shanghai" > /etc/timezone && \
    apk del tzdata && \
    rm -rf /var/cache/apk/*
```

