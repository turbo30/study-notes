# Makefile学习笔记

## 1、相关概念介绍

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/c56aa38a95c5300130366709322f97c47902469e.png)

Makefile中的内容：make的编译规则

Makefile或makefile（*另外也可以指定文件名称）

cmake，nmake等：

https://zhuanlan.zhihu.com/p/111110992

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/cbad26313cff5366507bf7554dba956ae102fa93.png)

## 2、从hello world开始

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/afc6c5a7ec8e99db38a63707cca545326016a4ba.png)



![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/6e4358e21cd52cc7584ec0ccd7e116083c5afa7d.png)



![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/47401ed0e00cee5bbd2679b82e0f23589acde5cb.png)

实测：

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/73766c80253d723c35e4caf0103e090461492314.png)

遇到的一个编译问题：

https://blog.csdn.net/weixin_41767324/article/details/114624550

*另附clion安装教程：

https://blog.csdn.net/chk218/article/details/83510298

https://www.bilibili.com/read/cv18308583/

实测有效

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/6ea05cf710b074f8c404d6e9905fc1ab6821bfff.png)

默认用第一个目标

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/3d915941a256bab561487ad691636dd09b31d1de.png)

a依赖于b时，先执行b，再执行a

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/950cfd9200055819ae13827046be38334db04bd6.png)

一个目标可以有多个依赖

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/ccc8557f57f52685bcfcb83cead0783e7bea334f.png)

执行多条命令

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/bfb77d2391f4da150626dc3806c1879fbdd8e142.png)

执行编译，生成a.out

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/f611df1fcab340b5078faf1a4651111ae8135eab.png)

实测（使用C）

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/18b8ab3e3b6558153f93043b4737238d02a5017b.png)

make clean

报错问题：

https://www.cnblogs.com/mrblug/p/13523089.html

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/a4806e771509ecdcf30995b4b1b351a1768736ea.png)

实测

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/38e405cf2ffde4e9b7c677bd7527be187a09519d.png)



![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/60c17bea7bf08ed83ce422be87122eee06d7dfd8.png)

常用选项

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/b48f265514fd13aeb6f9de1bc639bba8342bee76.png)

make -h

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/4119c4ab1f6d95ff72e74e529cda4f17f82d3c45.png)

make -f mk -n

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/8efee87e952780e5868226ab2bcbb2e1e107abb0.png)

make -s

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/9fa53a7b78f1efc9c50dcad598faa62f33dbf1a3.png)

make -w

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/57864831c41aad10d78ffbf31ab5c960da0d004c.png)

make -C

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/53ffda8a721d794afc97c4e477cf607bb2da3f34.png)

指定目标

## 3、gcc/g++ 编译流程详解

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/d4ca3470f248979335afef9ad5c65256305b96ef.png)

Makefile（不好的示例）

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/0326f1b5bf731895134cf7deafa5cdab77f764ba.png)

分步

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/58a7662b2d8a587cd8912a1d57bacc1c8bf4ad0c.png)

只改了add.cpp，只重新编译改动的文件（及依赖于此的文件）

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/cb39074dcbaef642c259ef9502cb71e45f43d525.png)

再次重新编译

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/70b4470dc019d48dd7d1bfe4893a6af2641b7b07.png)

编译流程

## 4、Makefile中的变量

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/523cf0ec73accee88ca3d4592186f710a6648e7f.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/f79365c043718f006e6d481e3a6bb873d9faa030.png)



![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/96ad80af1f0d44b3c2cd8ae786b373fa68324e0a.png)

方法1

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/d1710b275ded18a0a1cb501aca615b0229fc58ba.png)

方法2

（*附：弹幕指路阮一峰文章

http://www.ruanyifeng.com/blog/2015/02/make.html）

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/081d1208ac6b6aef3f01591c7e5c39c2cc8099f3.png)

系统常量

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/a318bca6f5479b29860aaff632117471109af6bb.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/559a7b78734f7d16f977219a6d990559d05d0907.png)

可以跨平台

## 5、Makefile中的伪目标和模式匹配

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/f3a0b1218e781cac116f3b6d7f9b53fe6274c735.png)

伪目标、模式匹配

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/1fa75574399883f82244da29487c3852093b1db1.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/658528dc8152499b2c00f5987d67b3b2af4e7774.png)

## 6、Makefile运行流程

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/469eef57a3d8ea19dd5c0bd90057a91972848e25.png)

递归

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/6315e515c879b0af1095fe6c3fef176938d28c72.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/caf664a78594d48bb0660bfcc02d814452f1015c.png)

## 7、Makefile编译动态链接库

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/8239376fec679182776317e25349c461dbb53a8c.png)

动态链接库

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/dac753f034b3cdf391e7c91caeac5115d1b758d6.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/c9fae33ac4b20fd8dcd902cbfd3c73cb8d8a591b.png)

命名规则，lib前缀

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/d08b88a6bb2c8044a3917076a6d2d2839d5dcc85.png)

-L：编译时指定，运行时需要再指定

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/ca58dea5fd21327efdf3b280f7cc81fabc126db6.png)

可以把.so复制粘贴到默认动态库路径

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/cabf4b7f7fbfdca56f8eb8952d7d13deda57b964.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/edef25848380311634990f6a2e334290a9c32f0e.png)

或者运行时手动指定

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/f951ac01edb634afe2eeb6abfe140c2255bb06e8.png)

## 8、Makefile编译静态链接库

静态链接库

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/287567994c5b986dd060c6fa61f5531544aa95c8.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/96d45cacf2540e9bd0dc33de69638553a3681064.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/f04072241ef961c920a142aa7ab19b109dc6fe80.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/2378b60c8cab8b728e636a226decb20d6736307e.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/041ddf72eabf649611382f5e582bf02d4e58189d.png)

## 9、Makefile中通用部分做公共头文件（一）

相同的部分

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/e595a20469f6c87468684a9bd1672caa8cab9451.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/8a3c1ae9ee0044da6cc8003ae6cfab1fb729643a.png)

自动推导

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/ebd6366492536aa6bc97eb8cf0b93b91e2bc14ed.png)

include公共头

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/5365b70d8624d297ff73d0eca9772d2ab18dc5d2.png)

公共头

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/1000f7dd874a9243fc407860bdf96b7bb4522175.png)

## 10、Makefile中通用部分做公共头文件（一）

=

:=

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/9558287549d09f9b387849b1861ea98ee694bf0e.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/5d34cae1c42ca945da279f9f8155b9a452f58be6.png)

使用:=

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/d2c932d99f420044df01f3c207fb657e93f572b3.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/b426c07fd2736fdeeaa727be857e6788534e7ec0.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/781c2a80e3a8028fdc28bd8c92209e0f64b89df3.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/71a431ed8c487c162192475725c18c6f93cfb606.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/9acf7aaef65a3a4ef4f8d1acdf54cd36bd340426.png)

## 11、Makefile中调用shell指令

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/e95c91b1d31f636b77aefb0ca28e50b71a0a0f53.png)

## 12、Makefile中的嵌套调用

嵌套调用和多项目同时编译

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/983a62a7e8007d228796bf3070acc4f847e92141.png)

默认有-f Makefile

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/a1ed42af80ae9835509abf52fdcfff8a648a9d77.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/ceaca5b15f9458ad88d844c5de7a75592a012ca6.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/952b242c16af18ba8076a7465bb733bd75b92fab.png)

## 13、Makefile中的条件判断

条件判断

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/62ca9aa413ff1a06ce71d9a03a2aaff2320d4eba.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/3348f2fa991aa6cd0d45507dc18f89b524515334.png)

ifeq之后要有空格

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/04c72a502f8f20bf959363d298ba31040283d9dc.png)



![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/8f28281cfcc92827bdef104bf8b9aed81b98c816.png)

 

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/2b9b481d39c984a0747befb162695d758bf38429.png)



![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/67a27df5b33e7275f168d43802e0abcd1da637a3.png)

命令行传参

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/c5bb9107ff3e47deff849173b502fbb18b8b781f.png)

默认值

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/08d7883b1f053667092b440257e2090ca6585fd1.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/2ff29926991465ef86d0b33a336456695d5dcf65.png)

## 14、Makefile中的循环

循环

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/1641ad39688be76940e5a7ce8ba1017a29f64ed2.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/3b60b479fc1437635fc2f873ec7863ebe755f81b.png)



![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/b1414d341de97c1dee8187c1b0f76b55449d3388.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/5e1f9df1d7551568583af91f91c704f3877e900e.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/4a388f8d4bd7d8f158e92326742d7c77078563c3.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/86c229e209a0f59ece4a1ab2b0a72363e1ea65e7.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/5e2293f858e9b4a1aee10322b713291652a2e8db.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/49c5b561df53c27d226febaecffb353d7f430ccd.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/2507c530976d2f59fffd8d24c280d689ce19fb04.png)

## 15、Makefile中的自定义函数

自定义函数实现和调用

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/ee717809275a59c43fb079642db613258329242c.png)

 

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/7d818c5c11941fe3675051dc6363ce17036f18af.png)

传参

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/2df377d406e6b9e830c87cea91f48df887d31fb1.png)

特殊变量0（指函数本身，名称）

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/bc7b07fdc47ffd029073eaa0557f617c4bf918dd.png)

这里的自定义函数没有返回值

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/448201183cac87e5f6afdb23636dd580f8b4c271.png)

当做字符串处理，注意不要有空格回车

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/1d6c80052b67defeb7a98a904fef9a20649cc2ba.png)

## 16、make-install的实现（一）

make命令

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/ded23f4ecaf8b4b3fc3643da663d52277b1e6b33.png)

 

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/7b232c0e0d80df7093b63830e033a149fba3d1d5.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/0599e97bca5fda7e5f5a52a33c8d67a88a74c9d5.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/e9d05a5282f0c9b4b8124b1af72d20a801a779b7.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/6351911deb551a9d615924d220bc8c18e497ab32.png)

全局可执行（软链接到BIN路径）

## 17、make-install的实现（二）

全局的启停脚本

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/92c55bfc0d8a59d0eea72c7e9cad05cfda29b2ac.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/4afae90193b8f07d888259d79362ec354237a109.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/cecbcc78609d79e1b711e10e7bbe3e3e1086bc76.png)



![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/565ed0b2c92516326af68cfdcd06afae1f787686.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/773ffe0de5a749ffda301589adae26fd3e3e0b91.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/3c2760c1db8f7967d26d9bbc85217e8ab9df4427.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/cd24534252376cffdab1a805e3eac79305f0a2a8.png)

进程后台启动

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/7bcc68d03ab03aa7b6abe34a73bef63aa8f3a9f2.png)

加echo

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/17a53c7e41b4a537489efc3d7b0da1f492f13cd5.png)

前面语法错误，修改格式

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/7c5e4d9ad8a120381b10570ddb7c8e43a5c42497.png)

![img](https://gitee.com/turbo30/study_pic/raw/master/pictureForMakefile/35286a743fab8311c7ee57147cc27e78a0b23695.png)