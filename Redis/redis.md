# Redis



## 一、NoSQL 四大分类

**KV键值对：**

- 新浪：Redis
- 美团：Redis + Tair
- 阿里、百度：Redis + memecache



**文档型数据库（bson格式 和 json一样）：**

- MongDB（一般必须掌握）
  + MongDB 是一个基于分布式文件存储的数据库，C++编写，主要用来处理大量的文档
  + MongDB 是一个介于关系型数据库和非关系型数据库中间的产品
  + MongDB 是NoSQL中功能最丰富的
- ConthDB



**列存储数据库：**

- HBase (大数据会遇到)
- 分布式文件系统



**图关系数据库：**

- 不是存图片的，是存储关系的，比如：朋友圈社交网络
- Neo4j，InfoGrid



![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/noSQL.png)



## 二、Redis概述

> 什么是Redis

Redis（**Re**mote **Di**ctionary **S**erver )，即远程字典服务。是一个开源的使用ANSI C语言编写、支持网络、可基于内存亦可持久化的日志型、Key-Value数据库，并提供多种语言的API。



> Redis的作用

它支持存储的value类型相对更多，包括string(字符串)、list(链表)、set(集合)、zset(sorted set --有序集合)和hash（哈希类型）。

- 内存存储、持久化 (RDB、AOF)
- 效率高，可以用于高速缓存
- 地图信息分析
- 计时器、计数器(如：浏览量)
- ......



> 特性

- 开源
- 持久化
- 集群，可跨语言实现
- 事务



> 默认端口

Redis的默认连接端口为 6379



注意：Redis推荐使用Linux进行开发，Windows已停更很久了。



## 三、Redis安装（Linux）



### 1、安装以及环境配置

- 去官网下载redis的`tar.gz`的安装包



- 将安装包通过TFP传输到服务器中，解压到`/opt`根目录下

```bash
tar -zxvf redis-5.0.8.tar.gz
```

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/redis安装.png)

redis目录下的文件：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/make前.png)



- 在**redis-5.0.8**目录下 安装基本的环境，需要使用的语言

```bash
# 通过yum安装gcc-c++
yum install gcc-c++

# 检查是否安装成功
gcc -v

# 配置文件，直接输入make命令，即可配置redis相关的环境
make
```

执行`make`命令后，多了几个目录：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/make后.png)

```bash
# 确认是否已全部安装成功
make install
```

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/makeinstall.png)



- redis的默认安装路径`/usr/local/bin`

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/redis默认安装路径.png)



- 在`/usr/local/bin`目录下创建一个config目录，拷贝一份redis的config到config目录下，对该配置文件进行修改，使用该配置文件启动redis

  ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/拷贝一份config.png)

  

- 修改配置文件，将redis的启动改为后台启动，默认不是后台启动

  ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/改为后台启动.png)



- 使用创建的config目录下的redis.conf文件启动redis服务

  ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/启动redis.png)



### 2、redis服务器启动连接以及关闭

- **连接redis以及基本使用**

  ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/连接redis以及基本使用.png)

  通过`redis-cli`连接redis默认服务端口`6379`，

  ```bash
  # -h localhost 代表本地主机(如果本地主机连接可省略不写) 
  # -p 6379 代表连接服务的端口号为6379
  redis-cli -h localhost -p 6379
  
  # ping检查是否连通
  ping
  
  #set key value 设置key为name，value为heroC的数据
  set name heroC
  
  # get key 得到key为name的数据
  get name
  
  # 查看redis存储了多少个key
  keys *
  
  # 删除key为name的数据
  del name
  ```



- **查看进程是否开启**

  ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/查看redis进程.png)



- **关闭redis的服务器**

  ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/关闭redis服务器.png)

  后面有参数提示，nosave表示不保存数据，save表示保存数据

  ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/关闭redis服务器1.png)



## 四、性能测试 redis-benchmark

**redis-benchmark** 是一个redis的压力测试工具

性能测试工具可选参数如下所示：

| 序号 | 选项      | 描述                                       | 默认值    |
| :--- | :-------- | :----------------------------------------- | :-------- |
| 1    | **-h**    | 指定服务器主机名                           | 127.0.0.1 |
| 2    | **-p**    | 指定服务器端口                             | 6379      |
| 3    | **-s**    | 指定服务器 socket                          |           |
| 4    | **-c**    | 指定并发连接数                             | 50        |
| 5    | **-n**    | 指定请求数                                 | 10000     |
| 6    | **-d**    | 以字节的形式指定 SET/GET 值的数据大小      | 3         |
| 7    | **-k**    | 1=keep alive 0=reconnect                   | 1         |
| 8    | **-r**    | SET/GET/INCR 使用随机 key, SADD 使用随机值 |           |
| 9    | **-P**    | 通过管道传输 \<numreq> 请求                | 1         |
| 10   | **-q**    | 强制退出 redis。仅显示 query/sec 值        |           |
| 11   | **--csv** | 以 CSV 格式输出                            |           |
| 12   | **-l**    | 生成循环，永久执行测试                     |           |
| 13   | **-t**    | 仅运行以逗号分隔的测试命令列表。           |           |
| 14   | **-I**    | Idle 模式。仅打开 N 个 idle 连接并等待。   |           |



```bash
# 性能测试并发数为50，请求数为10000
redis-benchmark -h localhost -c 50 -n 10000
```

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/性能测试.png)



## 五、基础命令

### 1、redis数据库切换

默认有16个(可到redis配置文件中查看)，不同数据库存不同的值

默认使用索引为0的数据库

```bash
# 切换数据库select index
select 8 # 切换到索引为8的数据库

DBSIZE # 该命令可查看数据库大小
```



### 2、清空数据

```bash
flushall # 清空全部数据库的数据

flushdb # 清空当前数据库的数据
```



## 六、五大数据类型的操作

### #、Key

[关于key的命令操作][https://www.runoob.com/redis/redis-keys.html]

#### . 增删改查

```bash
# set key value 设置key为name，value为heroC的数据
# 修改key的数据也是使用这命令
set name heroC

# get key 得到key为name的数据
get name

# 删除key为name的数据
del name

# 查看redis存储了多少个key
keys *
```



####  . 关于key的操作

```bash
# move key db
move name 1
# 将当前数据库中key为name的数据移动到索引为1的数据库中

# expire key seconds
expire name 5
# 设置key为name的数据将在5秒之后过期并删除

# exists key
exists name
# 查看key为name是否存在，存在返回1，不存在放回0

# persist key
persist name
# 移除key为name的过期时间，将持久保持。

# TTL key (ttl为time to live)
ttl name
# 以秒为单位返回key为name数据的剩余生存时间

randomkey
# 从当前数据库中随机返回一个key

# type key
type name
# 返回key所存储的值的类型

# renamenx key newkey
renamenx name username
# 当数据库不存在username，才允许修改name的名称为username
```



### 1、String 类型

[关于String类型的命令][https://www.runoob.com/redis/redis-strings.html]

#### . 关于value字符串类型的操作

```bash
# set key value 设置key为name，value为heroC的数据
# 修改key的数据也是使用这命令
set name heroC

# setex key seconds value
setex name 10 heroc
# 设置name过期时间为10秒，值为heroc

# setnx key value
setnx name heroc
# 设置name的值为heroc，永久有效。与set不同的是，setnx会判断数据库中是否有该key，如果有则不能设置成功；而set如果有该key，则会覆盖原来key的值，设置成新的值

# setrange key offset value
setrange name 3 heroc
# 替换，将索引为3的位置开始之后的一次替换为heroc

#####################################################################################

# get key 得到key为name的数据
get name

# getrange key start end
getrange name 0 3
# 得到name数据的索引为0~3的子字符串，返回hero
getrange name 0 -1
# 获取全部的字符串

# getset key value
getset name heroc
# 将name的数据修改为heroc，并返回旧值

#####################################################################################

# mset key value [key value ..]
mset name heroC age 21
# 同时设置多个key和vlaue数据，设置了key为name值为heroC的数据，设置了key为age值为21的数据

# msetnx key value [key value ..]
msetnx name heroc age 21
# 与mset不同的是，msetnx会判断数据库中是否有该key，如果有则不能设置成功；而mset如果有该key，则会覆盖原来key的值，设置成新的值（原子性的操作，同时成功同时失败）

# mget key [key ..]
mget name age
# 获取name和age的值数据

# append key value
append name " redis"
# 向key为name的数据追加redis字符串，返回追加后整个value的字符串长度

# strlen key
strlen name
# 返回字符串长度

#####################################################################################

# incr key
incr age
# 将字符串是数值的数据增加1，即将age的值增加1，放回增加后的结果

# incrby key increment
incrby age 5
# 将字符串是数值的数据增加5，即将age的值增加5，放回增加后的结果

# decr key
decr age
# 将字符串是数值的数据减1，即将age的值减1，放回减后的结果

# decrby key increment
decrby age 5
# 将字符串是数值的数据减5，即将age的值减5，放回增加后的结果

#####################################################################################

# 存储对象
# 比如存储user对象，后面跟的id，然后是属性
# set user:1:[name,age...] value
127.0.0.1:6379> setnx user:1:name heroc
(integer) 1
127.0.0.1:6379> setnx user:1:age 21
(integer) 1
# 以user:1:name为key
```



### 2、List 类型

[关于List类型命令][https://www.runoob.com/redis/redis-lists.html]

Redis列表是简单的字符串列表，按照插入顺序排序。你可以添加一个元素到列表的头部（左边）或者尾部（右边）  一个列表最多可以包含 2的32次方 - 1 个元素 (4294967295, 每个列表超过40亿个元素)。

Redis中的List列表是链表结构，可以是栈形式的先进后出存储方式，也可以是队列形式的先进先出，也可以是阻塞队列

#### . 关于list类型的操作

```bash
# lpush key value [value ...]
lpush name one two three
# lpush是list push(也可以理解为left push) ，往list中放如one，two，three这3个值，是从头部插入

# lpushx key value
lpushx n four
# 返回0，因为没有存在名为n的列表
lpushx name four
# 成功将four插入到name列表中。lpushx的作用就是将一个值插入到已存在列表的头部

# rpush key value [value ...]
rpush name right
# 向name列表的尾部也就是右边插入right值

# rpushx key value
rpushx n rightx
# 返回0，因为没有存在名为n的列表
rpushx name rightx
# 成功将rightx插入到name列表中。rpushx的作用就是将一个值插入到已存在列表的尾部(也就是从右边插入)

# lrange key start end
lrange name 0 2
# 查看名为name的list索引为0~2的数据
lrange name 0 -1
# 查看所有数据

# lindex key index
lindex name 0
# 查看索引为0的数据，这里是three，因为先插入one，再插入two，再插入three

# llen key
llen name
# 查看name列表的长度，返回3

# lpop key
lpop name
# 从name列表的头部取出一个值（第一个元素）

# rpop key
rpop name
# 从name列表的尾部取出一个值（最后一个元素）

# lrem key count value
lrem name 1 rightx
# 移除name列表中1个rightx值，列表是可以存储相同值的，count就表示移除列表中多少个value值

# ltrim key start stop
ltrim name 1 2
# 截取name列表中，索引为1开始到2结束的数据，其余数据全部删掉

# rpoplpush source destination
rpoplpush name newlist
# 从name列表的右边取出一个值，到新列表newlist的头部放入该值

# lset key index value
lset name 0 yikex
# 将name列表的索引为0的位置的值替换为yikex

# linsert key before|after pivot value
linsert name before yikex 1
# 往name列表中所有yikex值得前面插入1值
linsert name after yikex 2
# 往name列表中所有yikex值得后面插入2值

#####################################################################################

# blpop key [key ...] timeout
blpop name 10
# 从name列表的头部取出一个值，如果name列表中没有值就会等待，等待时长为10秒，不设置时间会一直等待到列表中有元素为止

# brpop key [key ...] timeout
brpop name 10

# brpoplpush source destination timeout
brpoplpush name newname 10
```



### 3、Set 类型

[关于set类型的命令][https://www.runoob.com/redis/redis-sets.html]

Redis 的 Set 是 String 类型的无序集合。集合成员是唯一的，这就意味着集合中不能出现重复的数据。 

Redis 中集合是通过哈希表实现的，所以添加，删除，查找的复杂度都是 O(1)。

#### . 关于set类型的操作

```bash
# sadd key member1 [member2 ...]
sadd myset 1 2 3
# 向myset中添加成员1，2，3 (不出现重复元素)

# smebers key
smebers myset
# 可查看myset中所有的成员

# sismember key member
sismember myset 4
# 检查myset中是否存在4，如果不存在即返回0，如果存在即返回1 (sismember: set is member)

# scard key
scard myset
# 获取myset中成员个数

# srem key member1 [member2 ...]
srem myset 1
# 移除myset集合中的1，2元素成员

# spop key
spop myset
# 随机移除myset中的元素并返回

# srandmember key [count]
srandmemer myset
# 随机在myset中抽取1个值
srandmember myset 2
# 随机在myset中抽取2个值

# smove source destination member
smove myset newset 2
# 将myset中的2移到newset中

#####################################################################################

set无序集合进行差集运算
# sdiff key1 key2 [key3 ...]
sdiff myset newset
# myset与newset进行差集运算，返回的是myset中在newset中没有的元素

set无序集合进行交集运算
# sinter key1 key2 [key3 ...]
sinter myset newset
# myset与newset进行交集运算，返回两个集合中共同元素

set无序集合进行并集运算
# sunion key1 key2 [key3 ...]
sunion myset newset
# myset与newset进行并集运算，返回两个集合所有元素

以上功能可以用于展示两个人的共同关注（存入用户id）
```



### 4、Hash 类型

[关于Hash类型的命令][https://www.runoob.com/redis/redis-hashes.html]

Hash就是key-map存储格式，map为key-value格式

跟String类型一样，只不过存储的值为k-v格式的

Redis hash 是一个 string 类型的 field 和 value 的映射表，hash 特别适合用于存储对象。

#### . 关于hash类型的操作

```bash
# hset key field value
hset myhash name heroc
# 往myhash中存储了name键heroc值

# hget key field
hget myhash name
# 从myhash中获取键为name对应的值

# hmset key field value [field value ...]
hmset myhash name heroc age 21
# 往myhash中存储了name键heroc值，age键21值

# hmget key field [field ...]
hmget myhash name age
# 从myhash中获取键为name和age对应的值

# hgetall key
hgetall myhash
# 可以获取myhash中存储的所有field值和value值

# hkeys key
hkeys myhash
# 获取myhash中的所有field

# hvals key
hvals myhash
# 获取myhash中的所有value

# hdel key field
hdel myhash age
# 删除myhash里的age

# hlen key
hlen myhash
# 查询myhash里有多少个值

# hexists key field
hexists myhash name
# 查询myhash中是否存在name这个字段

更多命令见连接
```

适合用于对象的存储，key为对象名+id，field为对象属性字段名，value为对应的具体值



### 5、Zset类型

[关于Zset类型命令][https://www.runoob.com/redis/redis-sorted-sets.html]

集合是通过哈希表实现的，所以添加，删除，查找的复杂度都是O(1)

这个类型实现了有序存储，可排序

底层跳表数据结构：https://www.jianshu.com/p/09c3b0835ba6



## 七、三种特殊类型



### 1、geospatial 地理位置

基于Zset类型

```bash
# geoadd key 经度 纬度 城市名 [经度 纬度 城市名 ...]
geoadd china:city 104.065735 30.659462 chengdu 106.504962 29.533155 chongqing
# 添加了一个china:city为key的成都和重庆经度纬度数据

# geopos key 城市名1 [城市名2 ...]
geopos china:city chengdu
# 获取成都的经度纬度数据

# geodist key 城市名1 城市名2 unit
geodist china:city chengdu chongqing km
# 以千米的单位计算成都与重庆的距离
m 表示单位为米
km 表示单位为千米
mi 表示单位为英里
ft 表示单位为英尺

# georadius key 经度 纬度 距离 unit [withcoord|withdist] [count num]
georadius china:key 106 29 1000 km count 200
# 在china:key中查询以经度106纬度29为中心半径1000km以内的200个城市
# withcoord 显示城市的经度纬度
# withdist 显示满足要求的城市到中心点的直线距离

# georadiusbymember key member 距离 [withcoord|withdist] [count num]
georadiusbymember china:city chengdu 1000 km count 200
# 查询并显示在china:key中查询以其中chengdu元素为中心半径1000km以内的200个城市

#####################################################################################

# zrange key start stop
zrange china:city 0 -1
# 可以查看china:city中的所有member

# zrem key member
zrem china:city chongqing
# 即可删除chongqing的记录
```



### 2、Hyperloglog

数据结构，用于基数统计的算法。

高并发情况下有0.81%的错误率。最多占用12KB内存。

**可用于网站统计访问量**

```bash
# pfadd key element [element ...]
pfadd pftest1 1 2 3 4 5 6
pfadd pftest2 4 5 6 7 8 9 0
# 向pftest1和pftest2中添加元素个数

# pfcount key
pfcount pftest1
# 统计pftest1元素个数，返回6

# pfmerge key resource1 resource2 [resource3 ...]
pfmerge pftest pftest1 pftest2
# 将pftest1和pftest2合并

# pfcount key
pfcount pftest
# 统计pftest元素个数，返回10，重复的不计算

```



### 3、Bitmap

位存储，只要存储的数据是可以用0和1表示状态的都可以使用该数据结构存储数据

比如：周一至周日打卡记录，0表示没打卡，1表示打卡，key可以设置位哪个用户的id

```bash
# setbit key offset value
127.0.0.1:6379> setbit status 0 1
(integer) 0
127.0.0.1:6379> setbit status 1 1
(integer) 0
127.0.0.1:6379> setbit status 2 0
(integer) 0
127.0.0.1:6379> setbit status 3 1
(integer) 0
127.0.0.1:6379> setbit status 4 1
(integer) 0
127.0.0.1:6379> setbit status 5 0
(integer) 0
127.0.0.1:6379> setbit status 6 0
# offset表示索引，value表示设置的值

# getbit key offset
getbit status 1
# 返回1，返回的是offset为1的value值

# bitcount key start stop
bitcount status 0 -1
# 查询每个位上有多少个是1的数量
```



## 八、事务

### 1、事务

Redis的事务不是原子性的，而每一条命令是原子性的。

==Redis事务：当事务中存在编译型异常，其中一条命令有错误，那么整个事务都不会得到执行；如果事务中存在运行时异常，其中一条命令有错误，不会影响其他命令的执行。==

**一次性、顺序性、排他性**

redis开启事务之后是将所有的命令都顺序存入队列之中，当启动执行事务后，才会从队列中将命令取出来一次顺序执行。

**redis的事务：**

- 开启事务（命令：multi）
- 命令入队
- 执行事务（命令：exec）
- 放弃事务（命令：discard）



```bash
127.0.0.1:6379> multi  # 开启事务
OK
127.0.0.1:6379> set user s1  # 命令入队
QUEUED
127.0.0.1:6379> set user s2
QUEUED
127.0.0.1:6379> hmset info name heroc age 21
QUEUED
127.0.0.1:6379> getrange user 0 -1
QUEUED
127.0.0.1:6379> hgetall info
QUEUED
127.0.0.1:6379> exec # 一次性顺序性排他性的执行入队的命令
1) OK
2) OK
3) OK
4) "s2"
5) 1) "name"
   2) "heroc"
   3) "age"
   4) "21"
```



### 2、监控

#### . 悲观锁

很悲观，认为任何时候都要出错，因此无论做什么都会加上锁

#### . 乐观锁（redis使用乐观锁）

很乐观，认为任何时候都不一定会出错，因此无论做什么都不一定会加上锁，通过`watch key`来监视，通过`unwatch`来取消监视

```bash
set money 100

watch money # 监视money

multi # 开启事务

decrby money 20

exec # 执行事务的时候，会比较监视的key的值有没有发生变化，如果发生变化则事务执行失败，没有发生变化则事务正常执行

unwatch

```



## 九、Jedis/lettuce 工具jar包 java连接redis



Jedis 是Redis官方推荐使用的Java连接Redis的工具

```xml
<!-- https://mvnrepository.com/artifact/redis.clients/jedis -->
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
    <version>2.9.0</version>
</dependency>

<!-- lettuce在springboot中有被使用 -->
<!-- https://mvnrepository.com/artifact/io.lettuce/lettuce-core -->
<dependency>
    <groupId>io.lettuce</groupId>
    <artifactId>lettuce-core</artifactId>
    <version>5.2.2.RELEASE</version>
</dependency>
```



### 1、Jedis连接

```java
public class JedisTest {
    public static void main(String[] args) throws IOException {
        // 获取ip，端口号，密码
        FileInputStream fileInputStream = 
            new FileInputStream("target/classes/redis-config.properties");
        Properties properties = new Properties();
        properties.load(fileInputStream);

        // Jedis 对象就是Redis。AUTH.IP为公网ip，AUTH.PORT为端口号
        Jedis jedis = new Jedis(properties.getProperty("AUTH.IP"), 
                              Integer.parseInt(properties.getProperty("AUTH.PORT")));
        
        // Jedis 所有方法，就是redis的所有命令
        // Redis 密码 AUTH.PASS为密码
        jedis.auth(properties.getProperty("AUTH.PASS"));
        
        System.out.println(jedis.ping()); // 返回PONG
        
        jedis.close(); // 关闭连接
    }
}
```



### 2、Lettuce连接

连接URL：

```
格式：
redis://[password@]host[:port][/databaseNumber][?[timeout=timeout[d|h|m|s|ms|us|ns]]
完整：redis://mypassword@127.0.0.1:6379/0?timeout=10s
简单：redis://localhost
```



```java
public class LettuceTest {
    public static void main(String[] args) throws IOException {
        FileInputStream fileInputStream = new FileInputStream("target/classes/redis-config.properties");
        Properties properties = new Properties();
        properties.load(fileInputStream);

        // RedisCommands<String, String> commands = RedisClient.create(properties.getProperty("AUTH.URL")).connect().sync();

        RedisClient redisClient = RedisClient.create(properties.getProperty("AUTH.URL")); // 创建客户端
        StatefulRedisConnection<String, String> connect = redisClient.connect(); // 创建线程安全连接
        RedisCommands<String, String> commands = connect.sync(); // 创建同步命令
        
        System.out.println(commands.ping()); // 执行命令

        connect.close(); // 关闭连接
        redisClient.shutdown(); // 关闭客户端
    }
}
```





#### . idea远程连接阿里云redis

防火墙开启端口

```bash
firewall-cmd --zone=public --add-port=6379/tcp --permanent
systemctl restart firewalld
```

阿里云服务器开启安全组

在redis的配置文件中

```bash
bind 0.0.0.0 # 更改可访问的端口号

requirepass 123 # 在配置文件的第511行左右，开启密码保护，这里设置的密码位123
```



### 2、事务

```java
public class JedisTX {

    public static void main(String[] args) throws IOException {
        FileInputStream fileInputStream = new FileInputStream("target/classes/redis-config.properties");
        Properties properties = new Properties();
        properties.load(fileInputStream);

        // Jedis 对象就是Redis
        Jedis jedis = new Jedis(properties.getProperty("AUTH.IP"), Integer.parseInt(properties.getProperty("AUTH.PORT")));
        // Redis 密码
        jedis.auth(properties.getProperty("AUTH.PASS"));

        jedis.flushDB();

        Transaction multi = jedis.multi();
        multi.watch("user1"); // 对user1进行监控，乐观锁
        try{
            multi.set("user1","{'name':'hello','age':'21'}");
            multi.set("user2","{'name':'heroc','age':'21'}");
            int num = 1/0;
            multi.exec(); // 执行事务，如果被监控的key被修改，事务里的命令全部取消执行
        }catch (Exception e){
            multi.discard(); // 取消事务
            System.err.println("出现异常");
        }finally {
            System.out.println(jedis.mget("user1", "user2"));
            jedis.close(); // 关闭连接
        }

    }
}
```



#### . jedis与lettuce的区别

jedis：当多线程使用同一个连接时，是线程不安全的。所以要使用连接池，为每个jedis实例分配一个连接。  

Lettuce：当多线程使用同一连接实例时，是线程安全的。



**Jedis** 在实现上是直接连接的redis server，如果在多线程环境下是非线程安全的，这个时候只有使用JedisPool连接池，为每个Jedis实例增加物理连接。

**Lettuce** 的连接是基于Netty的，连接实例（StatefulRedisConnection）可以在多个线程间并发访问，应为StatefulRedisConnection是线程安全的，所以一个连接实例（StatefulRedisConnection）就可以满足多线程环境下的并发访问，当然这个也是可伸缩的设计，一个连接实例不够的情况也可以按需增加连接实例，也可以配置LettucePool。



### 3、Jedis连接池 SSM

```properties
AUTH.IP=127.0.0.1
AUTH.PORT=1234
AUTH.PASS=1234
#lettuce: AUTH.URL=redis://11je30dis@47.95.2.239:6379

AUTH.MAXTOTAL=20
AUTH.MAXIDLE=20
AUTH.MINIDLE=10
```



```java
public class JedisPoolUtil {
    // jedis连接池
    private static JedisPool jedisPool;
    // ip地址
    private static String ip;
    // 连接密码
    private static String pwd;
    // 端口号
    private static int port;
    // 最大连接数
    private static int maxTotal;
    // 最大空闲连接数
    private static int maxIdle;
    // 最小空闲连接数
    private static int minIdle;

    // 静态代码块，最先加载，并执行一次，对变量进行初始化
    static {
        FileInputStream in = null;
        Properties properties = null;
        String path = JedisPoolUtil.class.getResource("../../../jedis-config.properties").getPath();
        try {
            in = new FileInputStream(path);
            properties = new Properties();
            properties.load(in);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (properties!=null){
            ip = properties.getProperty("AUTH.IP");
            pwd = properties.getProperty("AUTH.PASS");
            port = Integer.parseInt(properties.getProperty("AUTH.PORT"));
            maxTotal = Integer.parseInt(properties.getProperty("AUTH.MAXTOTAL"));
            maxIdle = Integer.parseInt(properties.getProperty("AUTH.MAXIDLE"));
            minIdle = Integer.parseInt(properties.getProperty("AUTH.MINIDLE"));
        }else {
            throw new RuntimeException("jedis配置文件获取异常！");
        }

        initJedisPool();
    }

    /**
     * 初始化redis池
     */
    private static void initJedisPool(){
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(maxTotal);
        config.setMaxIdle(maxIdle);
        config.setMinIdle(minIdle);
        jedisPool = new JedisPool(config, ip, port,5000, pwd);
    }

    // 获取池实例方法
    public static Jedis getJedis(){
        return jedisPool.getResource();
    }
}
```





## 十、Redis配置文件Redis.conf 常用 

[Redis.conf配置文件详解][https://blog.csdn.net/suprezheng/article/details/90679790]



### 1、INCLUDES 引入

可以包含其他配置文件，同spring的import，可以导入多个配置文件

```conf
# 语法示例：
include /path/to/local.conf
```



### 2、NETWORK 网络

用于绑定可访问的IP地址

```bash
# 绑定本地IP
bind 127.0.0.1
# 绑定所有IP
bind 0.0.0.0

# 受保护模式开启
protected-mode yes

# 默认端口号
port 6379
```



### 3、GENERAL 通用

通用设置

```bash
# 开启后台(守护)进程，默认为no没有开启
daemonize yes

# 如果以后台方式运行，就需要指定一个pid文件
pidfile /var/run/redis_6379.pid

# 日志级别 debug verbose notice warning
loglevel notice

# 生成日志的位置文件名
logfile ""

# 数据库数量
databases 16

# 是否总是显示redis的log
always-show-log yes
```



### 4、SNAPSHOTTING 快照 持久化rdb

快照，在规定时间执行快照，以保存数据，达到持久保存。保存的文件扩展名为`.rdb`、`.aof`

因为redis是基于内存操作的非关系型数据库，断电即失，所以需要进行持久化操作。

```bash
#RDB核心规则配置 save <指定时间间隔> <执行指定次数更新操作>，满足条件就将内存中的数据同步到硬盘中。官方出厂配置默认是 900秒内有1个更改，300秒内有10个更改以及60秒内有10000个更改，则将内存中的数据快照写入磁盘。若不想用RDB方案，可以把 save "" 的注释打开，下面三个注释
#   save ""
save 900 1
save 300 10
save 60 10000

#当RDB持久化出现错误后，是否依然进行继续进行工作，yes：不能进行工作，no：可以继续进行工作，可以通过info中的rdb_last_bgsave_status了解RDB持久化是否有错误
stop-writes-on-bgsave-error yes

#配置存储至本地数据库时是否压缩数据，默认为yes。Redis采用LZF压缩方式，但占用了一点CPU的时间。若关闭该选项，但会导致数据库文件变的巨大。建议开启。
rdbcompression yes

#是否校验rdb文件;从rdb格式的第五个版本开始，在rdb文件的末尾会带上CRC64的校验和。这跟有利于文件的容错性，但是在保存rdb文件的时候，会有大概10%的性能损耗，所以如果你追求高性能，可以关闭该配置
rdbchecksum yes
 
#指定本地数据库文件名，一般采用默认的 dump.rdb
dbfilename dump.rdb
 
#数据目录，数据库的写入会在这个目录。rdb、aof文件也会写在这个目录。
#./就是启动配置文件所在的目录
dir ./
```



### 5、REPLICATION 主从复制

```bash
# replicaof <masterip> <masterport>
# 从机的配置文件配置主机ip地址和主机端口号
replicaof 127.0.0.1 6379

# masterauth <master-password>
# 主机的密码配置
```



### 6、SECURITY 安全

```bash
# 设置密码，默认没有密码
# requirepass foobared
requirepass 123
###
命令设置密码：config set requirepass "123"
###
```



### 7、CLIENTS 客户端

```bash
# 最大客户端连接数量
# maxclients 10000
```



### 8、MEMORY MANAGEMENT 内存容量

```bash
# 设置最大的内存容量
# maxmemory <bytes>

# 内存满了的处理策略
# maxmemory-policy noeviction
```



### 9、APPEND ONLY MODE 持久化aof

**Redis 默认不开启aof模式**。它的出现是为了弥补RDB的不足（数据的不一致性），所以它采用日志的形式来记录每个写操作，并追加到文件中。Redis 重启的会根据日志文件的内容将写指令从前到后执行一次以完成数据的恢复工作默认redis使用的是rdb方式持久化，这种方式在许多应用中已经足够用了。但是redis如果中途宕机，会导致可能有几分钟的数据丢失，根据save来策略进行持久化，Append Only File是另一种持久化方式，可以提供更好的持久化特性。Redis会把每次写入的数据在接收后都写入 appendonly.aof 文件，每次启动时Redis都会先把这个文件的数据读入内存里，先忽略RDB文件。

```bash
# 默认不开启aof
appendonly no

# 默认文件名
appendfilename "appendonly.aof"

# appendfsync always # 每次修改都会fsync，以保证数据同步到磁盘，速度慢，消耗性能
appendfsync everysec # 每秒执行一次fsync，可能会有1秒的数据丢失
# appendfsync no # 不执行，由操作系统保证数据同步到磁盘，速度最快
```



## 十一、Redis 持久化 ==重点==

redis是基于内存操作的非关系型数据库，断电即失，所以需要进行持久化操作，保存到磁盘中。



### 1、RDB(Redis Databases)机制 (默认)

在指定时间进行数据快照，将数据快照保存到`.rdb`文件中，写到磁盘，以文件的形式保存下来。下次启动redis时，会自动读取快照中的内容，恢复数据到内存中。

缺点是，还没有到时间就宕机了，那么会损失这个时间段的数据；fork的时候会占用一定的内存空间，进行数据快照。

对数据完整性要求不高

> rdb默认保存文件名：`dump.rdb`



**触发rdb命令：**

- 达到配置文件的触发要求会生成dump.rdb文件
- 输入save命令，会生成dump.rdb文件



**恢复数据：**

将dump.rdb放到启动目录下，就是启动配置文件的目录下，redis在启动时，就可以自动获取该文件，恢复数据

```bash
config get dir
# 获取启动目录

127.0.0.1:6379> config get dir
1) "dir"
2) "/usr/local/bin/config"
# 每个人的启动目录会不一样
```





### 2、AOF(Append Only File)机制

将我们的所有命令都记录下来。恢复数据的时候，将我们的的每一步操作都执行一遍。

> aof默认保存文件名：`appendonly.aof`



只需要将配置文件中的appendonly设置为yes即可启用AOF

```bash
appendonly yes
```



**修复aof文件：**

如果aof文件被损害，可以尝试执行`redis-check-aof --fix`对其进行修复，会将有问题的数据进行清除删除



缺点，存储的文件远远大于rdb，效率慢，性能有消耗。



### . rdb与aof

rdb是按照规定的时间间隔和修改操作数的要求进行数据快照存储，

aof是记录每一次的操作来实现数据持久化操作。

在rdb和aof都开启的情况下，启动redis会优先加载aof，因为aof对操作记录的更加全面更加完整，但是aof可能会潜在bug，这时候rdb就可以修补aof的问题。

如果对数据没有太大的完整性要求，使用rdb就已经足够了，可以保留`save 900 1`的持久化策略。如果要使用aof，当然效率会慢，但是数据完整，不会丢失超过2秒的数据。如果不使用aof，可以省掉很大的IO消耗。

aof有重写规则，可以在配置文件中进行配置。

一般rdb就已经足够使用了。微博就是使用的rdb架构，使用集群，在宕机恢复数据时，比较主从机的rdb文件哪个最新就恢复哪个rdb文件的快照数据。



## 十二、Redis发布订阅



<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/发布订阅.png" style="float:left" />



[发布订阅命令全][https://www.runoob.com/redis/redis-pub-sub.html]

**命令：**

```bash
# 订阅频道
subscribe channel [channel ...]

# 发布消息
publish channel message

# 退订
unsubscribe channel [channel ...]
```



**示例：**

```bash
# 订阅者
127.0.0.1:6379> subscribe heroc
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "heroc"
3) (integer) 1

# 发布者
127.0.0.1:6379> publish heroc "new msg"
(integer) 1

# 订阅者
127.0.0.1:6379> subscribe heroc
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "heroc"
3) (integer) 1
1) "message"
2) "heroc"
3) "new msg"
```

订阅者是一个客户端，发布者是一个客户端，订阅者订阅了heroc频道，发布者在heroc频道发布了一条信息，订阅者就会实时接收到这个信息。



**使用场景**

- 实时聊天室
- 实时消息系统
- 直播的聊天窗口
- 订阅，关注系统

比较复杂的可以使用kafka专业的消息队列



## 十三、Redis主从复制



### 1、主从概念

==主服务器master可以进行读写操作==，当主服务器的数据发生变化，master会发出命令流来保持对slave的更新，而==从服务器slave通常是只读==的（可以通过slave-read-only指定），在主从复制模式下，即便master宕机了，slave是不能变为主服务器进行写操作的

一个master可以有多个slave，即一主多从；而slave也可以接受其他slave的连接，形成“主从链”层叠状结构（cascading-like structure），自 Redis 4.0 起，所有的sub-slave也会从master收到完全一样的复制流。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/主从关系.png)

**主从复制的好处：**

- 数据冗余，实现数据的热备份
- 故障恢复，避免单点故障带来的服务不可用
- 读写分离，负载均衡。主节点负载读写，从节点负责读，提高服务器并发量
- 高可用基础，是哨兵机制和集群实现的基础



==主从复制，必须使用的，真实项目中，不可能使用一个服务。集群，至少需要一主二从。==



### 2、搭建一主二从，简单的伪集群环境

reids服务器，==默认是master角色==。所以只需要将角色切换为从机即可。

```bash
127.0.0.1:6379> info replication
# Replication
role:master   # 默认角色为master
connected_slaves:0   # 连接的从机
```



#### 1）命令主从配置

通过命令进行主从机配置，每次启动该服务器都需要进行主从机的配置，只要宕机就失效。

```bash
# slaveof <host> <port>
# 由于这三台服务器都在同一个阿里云服务器上，所以host为127.0.0.1，端口号
slaveof 127.0.0.1 6379

# 将本机设置为主机
slaveof no one

# 查看主从复制的信息
info replication
```



##### 1个阿里云服务器开启3个redis服务器

- 先复制3个配置文件
- 将3个配置文件的端口号、后台使用的pid、日志文件名、rdb文件名更改了
- 然后使用xshell开3个窗口，分别以3个不同的配置文件启动redis服务器，
- 此时就用了3台redis服务器



##### 一主二从配置

该3台redis服务器都是master角色，因此需要配置从机即可

比如3台服务器的端口好分别为（6379、6380、6381）

认定6379为主机，其余为从机

**从机认主机的命令：**

```bash
# slaveof <host> <port>
# 由于这三台服务器都在同一个阿里云服务器上，所以host为127.0.0.1，端口号就是3个配置文件中配置的

127.0.0.1:6380> slaveof 127.0.0.1 6379
127.0.0.1:6380> info replication
# Replication
role:slave   # 角色改变为slave
master_host:127.0.0.1
master_port:6379

##################################################################################

127.0.0.1:6381> slaveof 127.0.0.1 6379
127.0.0.1:6381> info replication
# Replication
role:slave   # 角色改变为slave
master_host:127.0.0.1
master_port:6379
```



**主机查看主从情况：**

```bash
127.0.0.1:6379> info replication
# Replication
role:master   # 默认角色为master
connected_slaves:2   # 连接的从机为2个
```



#### 2）配置文件配置

在每一个从机中的配置文件配置了以下信息，那么服务器每次启动的时候就会自动配置为从机。永久有效。

```bash
# replicaof <masterip> <masterport>
# 从机的配置文件配置主机ip地址和主机端口号
replicaof 127.0.0.1 6379

# masterauth <master-password>
# 主机的密码配置
```



### 3、全量复制、增量复制

> **断开主机**：
>
> 主机断开之后，从机依旧是连接到主机的，只是没有了写操作，当主机重新启动，又恢复到了正常运作。只有主机可以读写操作，从机只能读操作。
>
> **断开从机**：
>
> 如果是使用命令行配置的从机，那么宕机之后重启，该服务器就是master主机。如果再使用命令配置成从机，配置成功之后，会立马获取主机的数据到本服务器中。
>
> 如果是配置文件配置的从机，那么宕机之后重启，该服务器依旧时salve从机。会在启动的时候获取主机的数据。



**全量复制**：当从机连接到主机之后，会发送sync命令，主机接收到命令之后，会将主机中的所有数据，传输到从机，进行全部同步操作。

**增量复制**：在从机与主机的连接中，如果主机对数据进行了增删改的操作，每一个步骤完成之后，会同步到从机，这每一步都同步，就叫做增量复制。



### 4、哨兵模式(Sentinel) ==重点==



#### 1）哨兵概念

由于主机宕机之后，就只剩下从机，就只有读数据的操作，这是行不通的。在没有哨兵模式之前，主机宕机了之后，都是手动重新设置主机和从机的关系，这样费时费力，还会造成一段时间服务器不可用状态，即带来损失。这样很麻烦，因此诞生了哨兵模式。

哨兵模式是一种特殊的模式，首先Redis提供了哨兵的命令，哨兵是一个独立的进程，作为进程，它会独立运行。其原理是**哨兵通过发送命令，等待Redis服务器响应，从而监控运行的多个Redis实例。**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/哨兵模式.png)

这里的哨兵有两个作用：

- 通过发送命令，让Redis服务器返回监控其运行状态，包括主服务器和从服务器。
- 当哨兵监测到master宕机，会自动将slave切换成master，然后通过**发布订阅模式**通知其他的从服务器，修改配置文件，让它们切换主机。

然而一个哨兵进程对Redis服务器进行监控，可能会出现问题，为此，我们可以使用多个哨兵进行监控。各个哨兵之间还会进行监控，这样就形成了多哨兵模式。

**故障切换（failover）**的过程。假设主服务器宕机，哨兵1先检测到这个结果，系统并不会马上进行failover过程，仅仅是哨兵1主观的认为主服务器不可用，这个现象成为**主观下线**。当后面的哨兵也检测到主服务器不可用，并且数量达到一定值时，那么哨兵之间就会进行一次投票，投票的结果由一个哨兵发起，进行failover操作。切换成功后，就会通过发布订阅模式，让各个哨兵把自己监控的从服务器实现切换主机，这个过程称为**客观下线**。这样对于客户端而言，一切都是透明的。哨兵之间也会互相监控。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/多哨兵模式.png)



#### 2）哨兵配置、启动哨兵



配置3个哨兵，每个哨兵的配置都是一样的。在Redis安装目录下有一个sentinel.conf文件，copy一份进行修改

```bash
# 禁止保护模式
protected-mode no

# 配置监听的主服务器，这里sentinel monitor命令代表监控，mymaster代表被监控服务器的名称(可以自定义)，127.0.0.1代表监控的主服务器，6379代表端口，2代表只有两个或两个以上的哨兵认为主服务器不可用的时候，才会进行failover操作。
sentinel monitor mymaster 127.0.0.1 6379 2

# sentinel author-pass命令定义服务的密码，mymaster是被监控服务器的名称，123是Redis服务器密码
# sentinel auth-pass <master-name> <password>
sentinel auth-pass mymaster 123
```

**其他配置参数：**

| 配置项                           | 参数类型                     | 作用                                                         |
| -------------------------------- | ---------------------------- | ------------------------------------------------------------ |
| port                             | 整数                         | 启动哨兵进程端口                                             |
| dir                              | 文件夹目录                   | 哨兵进程服务临时文件夹，默认为/tmp，要保证有可写入的权限     |
| sentinel down-after-milliseconds | <服务名称><毫秒数（整数）>   | 指定哨兵在监控Redis服务时，当Redis服务在一个默认毫秒数内都无法回答时，单个哨兵认为的主观下线时间，默认为30000（30秒） |
| sentinel parallel-syncs          | <服务名称><服务器数（整数）> | 指定可以有多少个Redis服务同步新的主机，一般而言，这个数字越小同步时间越长，而越大，则对网络资源要求越高 |
| sentinel failover-timeout        | <服务名称><毫秒数（整数）>   | 指定故障切换允许的毫秒数，超过这个时间，就认为故障切换失败，默认为3分钟 |
| sentinel notification-script     | <服务名称><脚本路径>         | 指定sentinel检测到该监控的redis实例指向的实例异常时，调用的报警脚本。该配置项可选，比较常用。一般指向一个.sh的脚本，可以发送邮件通知。 |



**启动哨兵模式：**

执行`redis-sentinel`服务

```bash
# redis-sentinel是可执行的文件  config/sentinel.conf是哨兵配置文件的位置
redis-sentinel config/sentinel.conf
```



**当主机宕机了，哨兵会进行监测，如果主机没有响应，达到了配置文件的要求，就会投票选举新的主机，新的主机产生之后，原来的主机重启后，也只能成为新主机的从机。**



==优点==：

- 哨兵集群，基于主从复制模式，会自动选举新的主机
- 故障可转移，主从可自动投票切换，系统可用性强

==缺点==：

- 集群的容量满了，需要扩容就比较麻烦了，在线扩容麻烦
- 哨兵模式的配置很多，限制也多



---

引用：[哨兵模式原文][https://www.jianshu.com/p/06ab9daf921d]



## 十四、Redis缓存穿透、缓存击穿和雪崩 ==重点==



### 1、缓存穿透

缓存穿透的概念就是，用户需要查询一个数据，然而Redis缓存中没有这个数据，也就是缓存没有命中，那么就会去持久层数据库进行查询，发现也没有，查询就失败。当很多用户查询数据，而这些数据都没有命中，于是都去持久层数据库进行查询，这样给数据库造成了巨大压力，这时候就相当于出现了缓存穿透。



#### . 解决办法



##### 布隆过滤器

[大话布隆过滤器][https://www.cnblogs.com/CodeBear/p/10911177.html]

现在有大量的数据，而这些数据的大小已经远远超出了服务器的内存，现在再给你一个数据，如何判断给你的数据在不在其中。如果服务器的内存足够大，那么用HashMap是一个不错的解决方案，理论上的时间复杂度可以达到O(1)，但是现在数据的大小已经远远超出了服务器的内存，所以无法使用HashMap，这个时候就可以使用“布隆过滤器”来解决这个问题。但是还是同样的，会有一定的“误判率”。布隆过滤器只能缓解缓存击穿。

**原理：**

布隆过滤器就是通过对数据进行固定的计算，计算出对应的映射位置，然后查看对应的映射位置的结果是否一致，如果比较结果一致，那么就存在这个数据，如果比较不一致，那么就不存在这个数据，这个查询就会被抛弃。

**简单过程：**

假如这里有长度为8的布隆过滤器，每个位置都只能时1和0两个结果。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/布隆过滤器01.png)

添加数据，在添加数据的时候，会经过布隆过滤器的计算，假如加入一个数据KEY1，经过计算为4和5，那么就在布隆过滤器的索引4和5的位置映射为1。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/布隆过滤器02.png)

继续添加一个数据KEY2，经过计算为1，那么就在布隆过滤器的索引1位置映射值为1。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForRedis/布隆过滤器03.png)

这样这些数据的在布隆过滤器中就有了记录，布隆过滤器不存值，**只能判断到这个数据一定不存在，而不能判断到这个数据在缓存中是否一定存在**。那么下次有数据过来进行查询的时候，也经过固定计算，查看映射位置是否都是1，如果都是1，那么就继续请求查询，如果只要有一个映射位置的值为0，那么就不成功，这个查询就会被丢弃。



**布隆过滤器只能缓解缓存击穿。为什么这样说？**因为，在经过固定的计算，不排除有可能两个数据的计算结果一致的情况，所以万一有两个数据A和B的计算结果一致，映射的位置一致，而缓存中已经把B这个数据删除了，在用户查询B数据的时候，会通过布隆过滤器，然而在缓存中实际已经不存在了，那么这也会造成缓存穿透，但是这样的概率还是比较小的。所以说只能是缓解缓存击穿，而不能解决缓存击穿。

**布隆过滤器很难做到删除数据**。因为可能存在缓存的两个数据的映射位置一样，如果把这个映射位置清除了，会造成一些数据被查不到，导致不可用。

**布隆过滤器也可以查重，但是不能代表这个数据一定是重复的。**可以查重是因为可以通过计算，知道结果是否被映射，如果没有被映射，那么这个数据是唯一的；如果结果被映射了，这个数据可能是唯一的，这是因为映射位置一致，但实际存储的数据已经过期不存在，这时这个数据就是唯一的，也可能是重复的，这是因为映射位置一致，实际存储的这个数据也存在。这就好比equals方法，处理查询hash值是否一致，还要查询存储的真实值是否一致，才能判断这个两个数据是否一致。

为了解决减少数据计算结果一致的概率，可以增大布隆过滤器的容量长度。



- 优点：由于存放的不是完整的数据，所以占用的内存很少，而且新增，查询速度够快；
- 缺点：随着数据的增加，误判率随之增加；无法做到删除数据；只能判断到这个数据一定不存在，而不能判断到这个数据在缓存中是否一定存在。



##### 存储空对象

顾名思义，就是在Redis缓存中，存入空对象，使得在缓存中命中该数据，但是是空的，这样就不会进入到持久层数据库查询数据。

- 优点：一定程度上解决了缓存穿透的问题，减少了数据库的压力
- 缺点：有可能用户查询的数据很多是需要设置空对象的，那么就会缓存大量的空对象，无用的信息，浪费内存空间。也不能彻底解决缓存穿透的问题。



### 2、缓存击穿

缓存击穿的概念是，有很多用户都在查询同一个数据，而这个数据在Redis中存在，都在请求Redis中的key，这个key很热点，一直经受着大并发访问量。当这个key过期的瞬间，由于还是有很多用户在查询这个key数据，巨大的并发量，瞬间击穿Redis，而进入持久层数据库查询数据。



#### . 解决办法

##### 将热点key设置为永不过期

缓存击穿，发生在key过期的一瞬间，遭受大量并发量，而怼到了持久层的数据库，数据库瞬间抗着巨大并发量的压力。所以可以尝试设置这个key值永不过期。



##### 加互斥锁

缓存击穿之后，持久层数据库会瞬间遭受到巨大的并发量，为了减轻数据库的压力，这时候就使用互斥锁，加锁的机制，只允许一个请求去数据库查询数据，而其他的只能等待或被拒绝。





### 3、雪崩

缓存雪崩是指缓存中数据大批量到过期时间、缓存层出现了错误或不能正常工作了，而查询数据量巨大，引起数据库压力过大甚至down机。



#### . 解决办法



##### Redis集群高可用

既然Redis会宕机，那么就多启动几台Redis服务器，集群工作，保证有缓存在运作，分担压力。

##### 限流降级

大量并发请求数据库，可以使用互斥锁，减轻数据库的压力，只允许一个请求取访问数据库和写缓存，其他请求等待。

##### 数据预热

数据预热的含义就是在正式部署之前，我先把可能的数据先预先访问一遍，这样部分可能大量访问的数据就会加载到缓存中。在即将发生大并发访问前手动触发加载缓存不同的key，设置不同的过期时间，让缓存失效的时间点尽量均匀。



## 十五、分布式锁



### 简单分布式锁原理

分布式锁，就是所有的服务同时去获取一个锁，可以利用redis来成为这个锁。redis有一个set方法，即不存在该`k-v`则可以存入，否则不可存入。所以，所有分布式可以同时存入`key`一样，`value`为`uuid`，识别身份的`k-v`值到redis，而k一样，所以只能有一个服务能够存入成功，存入成功会返回true，否则返回false，返回true表示存入成功，那么该服务器就获得了锁，就可以进行接下来的操作。没有获得到的锁，就应该尝试重新插入值，获取锁。

- 所有服务器存入的key必须都是一样的。
- 存入`k-v`值时，需要给key设置过期时间，以防止，在获取锁后的代码发生异常，无法正常删除k-v释放锁。k-v存入和过期时间的设置 必须是同一时间操作，原子性的操作。
- 删除`k-v`值时，也需要原子性的删除，并且在删除的时候要判断，这个`k-v`，中的value是不是自己的uuid，uuid不一致，说明自己的操作时间已经大于了过期时间，自己的存的`k-v`已被redis释放了，为了避免删除了其他服务器存入的`k-v`，所以需要比较uuid。一致，就将自己存入的`k-v`删除掉，即释放锁。



```java
/**
 * 分布式锁，利用所有服务访问redis，共同插入lock为key，uuid为value的键值数据，插入成功的
 * 则获取锁成功，插入失败则获取锁失败。
 * @return
 */
private Map<String, List<Catalog2Vo>> getCatalogJsonRedisLock(){
    ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
    // 再次查看是否已存入缓存中
    String catalogCache = ops.get("catalogJson");
    if (!StringUtils.isEmpty(catalogCache)){
        return JSON.parseObject(catalogCache,new TypeReference<Map<String, List<Catalog2Vo>>>(){});
    }
    /**
     * 加锁保证原子性，setIfAbsent方法，如果不存在则插入，成功插入返回true，否则返回false，
     * 给值设定过期时间，防止进行数据库查找时，抛出异常而无法解锁，导致其他服务无法插入lock
     * 而无法获取锁，执行查库操作。
     * 向redis插入值 获取锁，以及删除值 释放锁，都必须是原子性的，即统一操作。
     */
    String uuid = UUID.randomUUID().toString();
    Boolean lock = ops.setIfAbsent("lock", uuid, 300, TimeUnit.SECONDS);
    if (lock){
        Map<String, List<Catalog2Vo>> catalogJsonDB = null;
        try {
            // 调用数据库查询
            catalogJsonDB = getCatalogJsonDB();
        }finally {
            /**
             * 可能查库时间用了很久，导致这个key可能过期，就需要执行查询key是否还存在，
             * 存在比较value即uuid是不是一样，一样就删除这个key，不一样说明自己的锁过期，
             * 这是是别人的锁，就不能删除。script是redis原子性操作的lua脚本
             */
            String script = "if redis.call('get',KEYS[1]) == ARGV[1] then return redis.call('del',KEYS[1]) else return 0 end";
            Long result = stringRedisTemplate.execute(new DefaultRedisScript<Long>(script, Long.class), Arrays.asList("lock"), uuid);
            return catalogJsonDB;
        }
    }else {
        /**
         * 插入值失败的，进行重试操作，即重新获取锁
         */
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            log.error("睡眠中断异常...");
        }
        return getCatalogJsonRedisLock();
    }
}
```



## 十六、Redis的相关知识



### 1、Redis是单线程的，数据操作很快？

Redis是单线程的。6.0版本以上网络IO多线程。键值对操作还是单线程。Redis操作数据快的原因是：

- Redis完全基于内存操作，绝大部分请求基于内存操作，非常快速。
- 因为是单线程，所以避免了线程之间的上下文切换，获取锁释放锁和竞争，不会消耗CPU
- 数据结构简单，对数据操作也简单



### 2、Redis是基于内存操作的，内存数据满了会宕机吗？

不会出现内存存满的情况，在使用Redis的时候我们要配置Redis能使用的最大的内存大小，存到一定容量的时候还有Redis的内存淘汰策略。

**在配置文件中可设置使用内存大小：**

```bash
#设置Redis最大占用内存大小为100M`  
maxmemory 100mb
```



### 3、Redis存储对象，需要序列化

Reids存储对象时，该对象需要被序列化，这样存储的序列化后的对象，下次使用的时候，可以反序列化为一个对象，进行使用操作





### 4、为什么说布隆过滤器只能缓解缓存击穿？

因为，在经过固定的计算，不排除有可能两个数据的计算结果一致的情况，所以万一有两个数据A和B的计算结果一致，映射的位置一致，而缓存中已经把B这个数据删除了，在用户查询B数据的时候，会通过布隆过滤器，然而在缓存中实际已经不存在了，那么这也会造成缓存穿透，但是这样的概率还是比较小的。所以说只能是缓解缓存击穿，而不能解决缓存击穿。



### 5、布隆过滤器能删除它记录的数据吗？

布隆过滤器很难删除它映射的数据。因为可能存在缓存的两个数据的映射位置一样，如果把这个映射位置清除了，会造成一些数据被查不到，导致不可用。



### 6、布隆过滤器可以查重吗？怎么判断查重结果？

**布隆过滤器也可以查重，但是不能代表这个数据一定是重复的。**可以查重是因为可以通过计算，知道结果是否被映射，如果没有被映射，那么这个数据是唯一的；如果结果被映射了，这个数据可能是唯一的，这是因为映射位置一致，但实际存储的数据已经过期不存在，这时这个数据就是唯一的，也可能是重复的，这是因为映射位置一致，实际存储的这个数据也存在。这就好比equals方法，处理查询hash值是否一致，还要查询存储的真实值是否一致，才能判断这个两个数据是否一致。



### 7、springboot 使用 redis lettuce 对外内存溢出问题

在压力测试时，redis会出现对外内存溢出，lettuce用了netty进行网络通信 

这是因为lettuce的bug导致netty对外内存溢出 -Xmx300m netty如果没有设置堆外内存 将使用jvm的-Xmx300m 

可通过-Dio.netty.maxDirectMemory进行设置，但是不管设置多大的对外内存，最终依旧会导致这个异常发生，只是时间问题。

解决办法： 

1）升级lettuce 

2）不用lettuce而使用jedis

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
    <exclusions>
        <exclusion>
            <groupId>io.lettuce</groupId>
            <artifactId>lettuce-core</artifactId>
        </exclusion>
    </exclusions>
</dependency>
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
</dependency>
```

