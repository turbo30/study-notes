# Linux 发布项目

本文使用的是阿里云服务器，以及阿里云购买的域名



## 一、环境安装

安装软件一般有三种方式：

- rpm
- 解压缩
- yum在线安装



### 1、JDK 安装 （安装卸载rpm）



#### 1）下载JDK rpm

官网下载

#### 2）安装java环境

```bash
# 检测当前系统是否已安装java环境
java -version

# 卸载jdk
rpm -qa | grep jdk # 过滤出jdk版本信息； -q查询已安装的软件信息 -a显示出文件状态
rpm -e --nodeps <版本号> # 卸载jdk --nodeps是强制的意思，不验证软件包的依赖关系； -e 卸载

# 安装jdk
rpm -ivh <包名.rpm> #i表示安装,v表示显示安装过程,h表示显示进度
```

安装：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/rpm安装jdk.png)

卸载：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/rpm卸载jdk.png)



#### 3）配置环境变量

配置环境变量的文件`/etc/profile`

在profile文件中添加

```bash
export JAVA_HOME=/usr/java/jdk1.8.0_251-i586
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$PATH:$JAVA_HOME/bin
```

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/jdk安装位置.png)



`vim /etc/profile`

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/java环境配置.png)



让配置文件生效的命令：`srouce <需要生效文件全路径>`



> springboot 直接打包成jar包，在linux上
>
> 防火墙开启项目对应的端口，阿里云服务器安全组开启对应的端口，
>
> 执行`java -jar <项目jar包>` 即可运行



### 2、Tomcat 安装 （安装卸载tar.gz）

ssm框架开发的项目 需要打包成war包，放到tomcat中运行



#### 1）下载Tomcat

官网下载	`tar.gz`  压缩文件



#### 2）解压tar.gz压缩文件 安装

```bash
tar -zxvf <解压文件>

# tar -zxvf apache-tomcat-8.5.54.tar.gz
```

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/安装tomcat.png)

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/安装tomcat1.png)



#### 3）启动Tomcat 开启8080端口

> linux系统防火墙和阿里云安全组都要**开启8080端口**
>
> ```bash
> firewall-cmd --zone=public --add-port=8080/tcp --permanent
> systemtcl restart firewalld.service
> ```

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/防火墙开启8080端口.png)



启动：

进入tomcat解压后的文件夹下的bin目录中，执行`./startup.sh`，即可启动tomcat

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/启动tomcat.png)



成功访问：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/成功访问8080.png)



#### 4）域名购买、端口反向代理

> 阿里云购买了域名之后，如果你购买的阿里云服务器是==中国内陆==的，那么域名解析到该服务器，这个==域名必须备案==！！
>
> 域名解析后，如果端口是80 -http 或者443 -https 可以直接访问，如果是8080或者9000等端口，需要使用apache或者nginx做一下反向代理即可(配置文件)！！





### 3、Docker 安装 （安装卸载yum）

该安装必须联网

```
yum -y install <包名>
yum -y remove <包名>
```



[Docker安装过程][https://www.runoob.com/docker/centos-docker-install.html]



**1、监测centos 7**

```bash
cat /etc/redhat-release
```



**2、安装yum环境**

```bash
yum -y install <包名> # yum安装软件的语句，-y是全部选yes

# 安装gcc / gcc-c++
yum -y install gcc
yum -y install gcc-c++


```



**3、清除docker以前的版本**

```bash
yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```



**4、设置仓库**

```bash
yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
  
  
  
yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```



**5、安装Docker Engine-Community 和 containerd**

```bash
yum install docker-ce docker-ce-cli containerd.io
```



**6、启动Docker**

```bash
systemctl start docker
```



**7、测试运行hello-world 映像来验证是否正确安装了 Docker Engine-Community**

```bash
docker run hello-world
```





## 二、防火墙开启端口

```bash
#查看firewall服务状态
systemctl status firewalld

#开启、重启、关闭 firewalld.service服务
service firewalld start
service firewalld restart
service firewalld stop

#查看防火墙规则
firewall-cmd --list-all # 查看全部信息
firewall-cmd --list-ports # 只查看端口信息

# 开启端口
#开端口命令
firewall-cmd --zone=public --add-port=8080/tcp --permanent
#重启防火墙
systemctl restart firewalld.service

#命令含义
--zone #作用域
--add-port=8080/tcp # 添加8080端口，协议为tcp
--permanent # 永久生效，没有此参数重启后失效
```



## 三、部署web项目 maven打war包

1、pom.xml配置，加入war打包的插件

```xml
<build>
    <plugins>
        <plugin>
            <artifactId>maven-war-plugin</artifactId>
            <version>3.2.2</version>
        </plugin>
    </plugins>
    <sourceDirectory>src/main/java</sourceDirectory>
</build>
```



2、idea默认将class的编译文件存放在out目录下，因此需要将其更改到target目录下，因为maven打war包在target目录下，不更改可能没有java编译后的class、web.xml等文件：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/maven打war包.png)

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/编译的class生成位置.png)



3、配置好了之后，maven一定先`clear:clear`，再使用`war:exploded`打包，生成target目录，再运行tomcat编译一下目录，将编译的class文件生成到target目录下。



4、将生成的war目录，放到Linux的tomcat文件目录下的webapps里，然后开启tomcat服务，通过域名端口访问对应的项目即可

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/访问结果.png)

## 四、解决linux启动tomcat很慢

1.用vim编辑器打开tomcat的bin目录下的**catalina.sh**



2.增加一行：**JAVA_OPTS="-Djava.security.egd=file:/dev/./urandom"**



3.重启tomcat，速度即可得到大幅度提升。