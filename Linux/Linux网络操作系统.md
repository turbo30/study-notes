# Linux 网络操作系统



[TOC]



## 一、Ubuntu设置

### 1、图形界面进入文本界面

进入到/etc/default文件下的grub进行编辑

![](.\picture\更改为文本界面.png)

编辑完成，保存，然后执行以下命令。重启，就可以进入文本界面

![](设置更改\更改为文本界面2.png)



### 2、上下左右键和backspace键的正常使用更改

![](设置更改\上下键backspace更改为正常.png)

## 网络配置目录

`cd /etc/sysconfig/network-scripts`

`ifcofig` 是linux查询网络配置



## 解决Xshell 连接阿里云服务器自动断开

#vim /etc/ssh/sshd_config

找到下面两行

#ClientAliveInterval 0
#ClientAliveCountMax 3

去掉注释，改成

ClientAliveInterval 30
ClientAliveCountMax 86400

这两行的意思分别是

1、客户端每隔多少秒向服务发送一个心跳数据

2、客户端多少秒没有相应，服务器自动断掉连接

重启sshd服务

#service sshd restart

#systemctl restart sshd.service



## 二、文件

### 目录介绍

以下是对这些目录的解释：

**/bin**：bin是Binary的缩写, 这个目录存放着最经常使用的命令。

**/boot**： 这里存放的是启动Linux时使用的一些核心文件，包括一些连接文件以及镜像文件。（不要动）

**/dev** ： dev是Device(设备)的缩写, 存放的是Linux的外部设备，在Linux中访问设备的方式和访问文件的方式是相同的。

==**/etc**： 这个目录用来存放所有的系统管理所需要的配置文件和子目录。==

==**/home**：用户的主目录，在Linux中，每个用户都有一个自己的目录，一般该目录名是以用户的账号命名的。==

**/lib**：这个目录里存放着系统最基本的动态连接共享库，其作用类似于Windows里的DLL文件。（不要动）

**/lost+found**：这个目录一般情况下是空的，当系统非法关机后，这里就存放了一些文件。

**/media**：linux系统会自动识别一些设备，例如U盘、光驱等等，当识别后，linux会把识别的设备挂载到这个目录下。

**/mnt**：系统提供该目录是为了让用户临时挂载别的文件系统的，我们可以将光驱挂载在/mnt/上，然后进入该目录就可以查看光驱里的内容了。

==**/opt**：这是给主机额外安装软件所摆放的目录。比如你安装一个ORACLE数据库则就可以放到这个目录下。默认是空的。==

**/proc**：这个目录是一个虚拟的目录，它是系统内存的映射，我们可以通过直接访问这个目录来获取系统信息。

==**/root**：该目录为系统管理员，也称作超级权限者的用户主目录。==

**/sbin**：s就是Super User的意思，这里存放的是系统管理员使用的系统管理程序。

**/srv**：该目录存放一些服务启动之后需要提取的数据。

**/sys**：这是linux2.6内核的一个很大的变化。该目录下安装了2.6内核中新出现的一个文件系统 sysfs 。

==**/tmp**：这个目录是用来存放一些临时文件的。==

==**/usr**：这是一个非常重要的目录，用户的很多应用程序和文件都放在这个目录下，类似于windows下的program files目录。==

**/usr/bin**： 系统用户使用的应用程序。

**/usr/sbin**： 超级用户使用的比较高级的管理程序和系统守护程序。

**/usr/src**： 内核源代码默认的放置目录。

==**/var**：这个目录中存放着在不断扩充着的东西，我们习惯将那些经常被修改的目录放在这个目录下。包括各种日志文件。==

**/run**：是一个临时文件系统，存储系统启动以来的信息。当系统重启时，这个目录下的文件应该被删掉或清除。

==**/www**：存放服务器网站相关的资源，环境，网站项目等==



### 1、文件颜色

下面是linux系统约定不同类型文件默认的颜色：  

白色：表示普通文件  

蓝色：表示目录  

绿色：表示可执行文件  

红色：表示压缩文件  

浅蓝色：链接文件  

红色闪烁：表示链接的文件有问题  

黄色：表示设备文件  

灰色：表示其它文件



### 2、文件夹权限介绍

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/详细文件夹信息.png)



#### 1）文件类型

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/文件类型.jpg)

- “-”表示普通文件；
- “d”表示目录；
- “l”表示链接文件；
- “p”表示管理文件；
- “b”表示块设备文件；
- “c”表示字符设备文件；
- “s”表示套接字文件；



#### 2）文件权限属性

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/文件属性.png)

`drwxr-xr-x`可拆分位`[d][rwx][r-x][r-x]`

[d]表示，该文件为目录文件



**后面三个是对该文件的执行权限，分别为：**

- 第一段表示文件创建者/所有者对该文件所具有的权限
- 第二段表示文件创建者/所有者所在组的其他用户对该文件的权限
- 第三段表示其他组的其他用户所具有的权限



**每个字母的解释：**

- **r** 为read，读权限

- **w** 为write，写权限 (增删该)

- **x** 为excute，执行权限

  以下权限不常见：

- **s或S**（SUID,Set UID）：可执行的文件搭配这个权限，便能得到特权，任意存取该文件的所有者能使用的全部系统资源。请注意具备SUID权限的文件，黑客经常利用这种权限，以SUID配上root帐号拥有者，无声无息地在系统中开扇后门，供日后进出使用。

- **t或T**（Sticky）：/tmp和 /var/tmp目录供所有用户暂时存取文件，亦即每位用户皆拥有完整的权限进入该目录，去浏览、删除和移动文件。



#### 3）数字权限解释

linux系统文件夹644、755、777权限设置详解 ，左至右，

第一位数字代表文件所有者的权限，

第二位数字代表同组用户的权限，

第三位数字代表其他用户的权限。

> 数字表示权限

读取的权限等于4，用r表示；

写入的权限等于2，用w表示；

执行的权限等于1，用x表示；

>举例：
>
>444 r--r--r--
>600 rw------
>644 rw-r--r--
>666 rw-rw-rw-
>700 rwx------
>744 rwxr--r--
>755 rwxr-xr-x
>777 rwxrwxrwx



#### 3）目录/链接 数

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/目录链接数.jpg)

对于目录文件，表示它的第一级子目录的个数。注意此处看到的值要减2才等于该目录下的子目录的实际个数。因为每一个目录里都有`.`和`..`两个目录，所以该目录的第一级目录实际个数需要减2

`.`为当前目录，`..`为上一级目录

对于其他文件，表示指向它的链接文件的个数。



##### 硬链接和软链接 touch、echo、ln

硬链接：A --> B 硬链接就是B硬链接到了A，那么如果A删除了的话，B依旧能够访问A删除之前存储的文件。 

软链接：类似windows上的快捷方式，当软连接删除了，那么就不能直接访问指向的文件内容

```bash
touch f1 # 创建f1文件
ln f1 f2 # 创建硬链接 f2 指向了f1
ln -s f1 f3 # 创建软链接(符号链接) f3 指向了 f1

echo "heroC" >> f1  # 向f1文件写入heroC

cat f1
cat f2
cat f3
# 都可以查看到f1里写的内容
```

![](.\picture\硬链接软链接.png)

硬链接应该要占磁盘空间



#### 4）所有者以及组

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/所有者以及组.jpg)

第一个表示创建者/所有组

第二个表示所在的组



#### 6）文件大小、修改日期、文件名

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/文件大小修改日期名称.png)

第一个表示，文件大小，单位字节

第二个表示，文件最后修改日期

第三个表示，文件的名称



## 三、用户管理、日志

[管理用户和用户组相关命令](https://blog.csdn.net/freeking101/article/details/78201539)



`sudo useradd -m <用户名>` 创建一个用户

`sudo passwd <用户名>` 设置新用户密码

`sudo userdel -r <用户名>` 删除一个用户



`logger "记录"` 日志记录

`/var/log` 该目录下有很多日志

`cat head -n 10 /var/log/syslog` 查看syslog里记录的前十条日志

`cat tail -n 10 /var/log/syslog` 查看syslog里记录的后十条日志



### 1、用户



#### 1）添加用户 useradd

```bash
useradd -m -G<用户组> <用户名>
```

参数说明：

- 选项 :

- - -c comment 指定一段注释性描述。
  - -d 目录 指定用户主目录，如果此目录不存在，则同时使用-m选项，可以创建主目录。
  - -g 用户组 指定用户所属的用户组。
  - -G 用户组，用户组 指定用户所属的附加组。
  - -m　使用者目录如不存在则自动建立。
  - -s Shell文件 指定用户的登录Shell。
  - -u 用户号 指定用户的用户号，如果同时有-o选项，则可以重复使用其他用户的标识号。

- 用户名 :

- - 指定新账号的登录名。



#### 2）删除用户 userdel

```bash
userdel -r <用户名>
```



#### 3）修改用户 usermod

修改已有用户的信息使用usermod命令，其格式如下：

```
usermod 选项 用户名
```

常用的选项包括-c, -d, -m, -g, -G, -s, -u以及-o等，这些选项的意义与useradd命令中的选项一样，可以为用户指定新的资源值。

例如：

```
# usermod -s /bin/ksh -d /home/z –g developer heroC
```

此命令将用户heroC的登录Shell修改为ksh，主目录改为/home/z，用户组改为developer。



#### 4）修改密码 passwd、查看用户cat /etc/passwd

```
超级用户可以修改任何用户的密码
passwd <用户名>

cat /etc/passwd 查看存在的所有用户信息
```

即可修改用户名密码



#### 5）锁定账户 冻结账户 解冻账户

```
passwd -l <用户名>

passwd -u <用户名>

passwd -d <用户名>
```

-l : lock 锁定的选项

-d : 清空密码，没有密码，这样也可以冻结账户



#### 6）查看主机名、修改主机名

```bash
# 命令形式更改主机名，只是暂时的，重启失效
hostname # 查看主机名
hostname heroC # 修改主机名为heroC，重启生效或重连生效

# 通过配置文件修改主机名，长期有效
vim /etc/hostname
```



### 2、用户组



#### 1）查看用户组 cat /etc/group

```
cat /etc/group
```



#### 2）创建用户组 groupadd

```
groupadd [选项] <组名>
```

可以使用的选项有：

- -g GID 指定新用户组的组标识号（GID）。
- -o 一般与-g选项同时使用，表示新用户组的GID可以与系统已有用户组的GID相同。



#### 3）删除用户组 groupdel

```
groupdel <组名>
```



#### 4）修改用户组 groupmod

```
groupmod [选项] <组名>
```

常用的选项有：

- -g GID 为用户组指定新的组标识号。
- -o 与-g选项同时使用，用户组的新GID可以与系统已有用户组的GID相同。
- -n新用户组 将用户组的名字改为新名字

```bash
# 此命令将组group2的组标识号修改为102。
groupmod -g 102 group2

# 将组group2的标识号改为10000，组名修改为group3。
groupmod –g 10000 -n group3 group2
```



### 3、etc/passwd 文件内容解释

Linux系统中的每个用户都在/etc/passwd文件中有一个对应的记录行，它记录了这个用户的一些基本属性。

这个文件对所有用户都是可读的。它的内容类似下面的例子：

```bash
＃ cat /etc/passwd

root:x:0:0:Superuser:/:
daemon:x:1:1:System daemons:/etc:
bin:x:2:2:Owner of system commands:/bin:
sys:x:3:3:Owner of system files:/usr/sys:
adm:x:4:4:System accounting:/usr/adm:
uucp:x:5:5:UUCP administrator:/usr/lib/uucp:
auth:x:7:21:Authentication administrator:/tcb/files/auth:
cron:x:9:16:Cron daemon:/usr/spool/cron:
listen:x:37:4:Network daemon:/usr/net/nls:
lp:x:71:18:Printer administrator:/usr/spool/lp:
```

从上面的例子我们可以看到，/etc/passwd中一行记录对应着一个用户，每行记录又被冒号(:)分隔为7个字段，其格式和具体含义如下：

```
用户名:口令（密码加密后的密文，不可见）:用户标识号:组标识号:注释性描述:主目录:登录Shell
```

- **"用户名"**是代表用户账号的字符串。

通常长度不超过8个字符，并且由大小写字母和/或数字组成。登录名中不能有冒号(:)，因为冒号在这里是分隔符。

为了兼容起见，登录名中最好不要包含点字符(.)，并且不使用连字符(-)和加号(+)打头。

- **“口令”**一些系统中，存放着加密后的用户口令字。

虽然这个字段存放的只是用户口令的加密串，不是明文，但是由于/etc/passwd文件对所有用户都可读，所以这仍是一个安全隐患。因此，现在许多Linux 系统（如SVR4）都使用了shadow技术，把真正的加密后的用户口令字存放到<u>/etc/shadow</u>文件中，而在/etc/passwd文件的口令字段中只存放一个特殊的字符，例如“x”或者“*”。

- **“用户标识号”**是一个整数，系统内部用它来标识用户。

一般情况下它与用户名是一一对应的。如果几个用户名对应的用户标识号是一样的，系统内部将把它们视为同一个用户，但是它们可以有不同的口令、不同的主目录以及不同的登录Shell等。

通常用户标识号的取值范围是0～65 535。0是超级用户root的标识号，1～99由系统保留，作为管理账号，普通用户的标识号从100开始。在Linux系统中，这个界限是500。

- **“组标识号”**字段记录的是用户所属的用户组。

它对应着/etc/group文件中的一条记录。

- **“注释性描述”**字段记录着用户的一些个人情况。

例如用户的真实姓名、电话、地址等，这个字段并没有什么实际的用途。在不同的Linux 系统中，这个字段的格式并没有统一。在许多Linux系统中，这个字段存放的是一段任意的注释性描述文字，用作finger命令的输出。

- **“主目录”**，也就是用户的起始工作目录。

它是用户在登录到系统之后所处的目录。在大多数系统中，各用户的主目录都被组织在同一个特定的目录下，而用户主目录的名称就是该用户的登录名。各用户对自己的主目录有读、写、执行（搜索）权限，其他用户对此目录的访问权限则根据具体情况设置。

- **Shell** 用户登录后，要启动一个进程，负责将用户的操作传给内核，这个进程是用户登录到系统后运行的命令解释器或某个特定的程序，即Shell。

Shell是用户与Linux系统之间的接口。Linux的Shell有许多种，每种都有不同的特点。常用的有sh(Bourne Shell), csh(C Shell), ksh(Korn Shell), tcsh(TENEX/TOPS-20 type C Shell), bash(Bourne Again Shell)等。

系统管理员可以根据系统情况和用户习惯为用户指定某个Shell。如果不指定Shell，那么系统使用sh为默认的登录Shell，即这个字段的值为/bin/sh。

用户的登录Shell也可以指定为某个特定的程序（此程序不是一个命令解释器）。

利用这一特点，我们可以限制用户只能运行指定的应用程序，在该应用程序运行结束后，用户就自动退出了系统。有些Linux 系统要求只有那些在系统中登记了的程序才能出现在这个字段中。

- 系统中有一类用户称为**伪用户**（pseudo users）。

这些用户在/etc/passwd文件中也占有一条记录，但是不能登录，因为它们的登录Shell为空。它们的存在主要是方便系统管理，满足相应的系统进程对文件属主的要求。

常见的伪用户如下所示：

```
伪 用 户 含 义
bin 拥有可执行的用户命令文件
sys 拥有系统文件
adm 拥有帐户文件
uucp UUCP使用
lp lp或lpd子系统使用
nobody NFS使用
```



### 4、etc/group 文件内容解释

用户组的所有信息都存放在/etc/group文件中。

将用户分组是Linux 系统中对用户进行管理及控制访问权限的一种手段。

每个用户都属于某个用户组；一个组中可以有多个用户，一个用户也可以属于不同的组。

当一个用户同时是多个组中的成员时，在/etc/passwd文件中记录的是用户所属的主组，也就是登录时所属的默认组，而其他组称为附加组。

用户要访问属于附加组的文件时，必须首先使用newgrp命令使自己成为所要访问的组中的成员。

用户组的所有信息都存放在/etc/group文件中。此文件的格式也类似于/etc/passwd文件，由冒号(:)隔开若干个字段，这些字段有：

```
组名:口令:组标识号:组内用户列表
```

1. **"组名"**是用户组的名称，由字母或数字构成。与/etc/passwd中的登录名一样，组名不应重复。
2. **"口令"**字段存放的是用户组加密后的口令字。一般Linux 系统的用户组都没有口令，即这个字段一般为空，或者是*。
3. **"组标识号"**与用户标识号类似，也是一个整数，被系统内部用来标识组。
4. **"组内用户列表"**是属于这个组的所有用户的列表/b]，不同用户之间用逗号(,)分隔。这个用户组可能是用户的主组，也可能是附加组。





## 四、文件操作命令

### 1、文件 创建touch / 删除rm

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/touchmkdirrm.png)

**创建文件：**

`touch <文件名.扩展名>`

**删除文件：**

`rm <文件名.扩展名>` 直接删除，只能删除文件

`rm -r <文件名.扩展名/文件夹>` 直接删除，可删除文件或文件夹

`rm -ri <文件名.扩展名/文件夹>` 会提示是否删除，确认之后才会删除，可删除文件或文件夹 (推荐使用)



-f  忽略不存在的文件，不会出现警告，强制删除

-r 递归删除目录

-i 提示是否删除



> ==重要！！！==最好不要使用rm命令，如果要删除文件，将该文件或目录mv移动到专门存放即将删除的文件目录中，如可以创建一个delete文件目录，这样降低误删文件的概率。定时清除delete目录中的文件即可。



### 2、文件夹 创建mkdir / 删除rmdir、rm

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/touchmkdirrm.png)

**创建文件夹：**

`mkdir <文件夹名>` 

**删除文件夹：**

`rmdir <文件夹名>` 直接删除

`rm -r <文件名.扩展名/文件夹>` 直接删除，可删除文件或文件夹

`rm -ri <文件名.扩展名/文件夹>` 会提示是否删除，确认之后才会删除，可删除文件或文件夹 (推荐使用)



-f  忽略不存在的文件，不会出现警告，强制删除

-r 递归删除目录

-i 提示是否删除



### 3、文件[夹] 拷贝cp / 移动mv 重命名

**文件[夹] 拷贝：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/cp.png" style="float:left" />

`cp [操作选项] [文件(夹)全名] [拷贝的目标位置]`  将文件从本目录拷贝到指定目录位置

操作选项： `-i` 可以提示是否这样操作



**文件[夹] 移动以及重命名：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/mv.png" style="float:left" />

`mv [操作选项] [文件(夹)全名] [拷贝的目标位置[新名字]]` 

`mv a.txt aa.txt` ：可以将a.txt重命名为aa.txt

`mv -i a.txt ../` ：将本目录的a.txt文件移动到上一级目录中，并会给出提示是否这样操作



### 4、查看文件内容

#### 1）more命令

`语法：more [选项] [文件名]`

功能：一页一页的显示，方便用户逐页阅读，而最基本的命令就是按**空白键（space）显示下一页**。按**【B】键就会显示上一页**。按【H】键，查看帮助信息。而且还有查找字串的功能，“/字符串”查旬字符串所在处。按【Q】键，跳出more状态。

#### 2）less命令

`语法：less [选项] [文件名]`

功能：less的作用与more十分相似，也可以用来浏览文本文件的内容，less改进了more 不能回头看的问题，可以简单的使用【PageUp】键向上翻。来浏览已经看过的部分，同时因为less并未在一开始就读入整个文件，因此在遇上大型文件的开启时，会比一般的文本编辑器速度快。

使用 q 退出。

在less命令中，

输入 `/内容` 会在文中向下查找该内容

输入 `?内容` 会在文中向上查找该内容



#### 3）cat、nl、tac命令

`[cat][nl][tac] [选项] 文件1 文件2……`

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/cat、nl、tac.png)

nl命令：显示文件内容，并显示行号

cat命令：显示文件内容

tac命令：将文件从最后一行开始倒过来将内容数据输出到屏幕上

#### 4）head、tail命令

`-n` 以行为单位显示，`-c` 以字节为单位显示

`cat head -n <数量> <文件名>` 

> `cat head -n 10 /var/log/syslog` 查看syslog里记录的前十条日志

`cat tail -n <数量> <文件名>` 

> `cat tail -n 10 /var/log/syslog` 查看syslog里记录的后十条日志



## 五、关机、重启

`<tiem>` 为时间，关机重启的执行时间，如果为now，就是马上关机重启

关机命令：`shutdown -h <time>`  、`halt`、`poweroff`

重启命令：`shutdown -r <time>` 、`reboot`

取消关机、重启命令：`shutdown -c`

终止命令：`ctrl+z` 可终止正在执行的命令



## 六、Vim 编辑器

> [Vim操作大全](https://blog.csdn.net/weixin_37657720/article/details/80645991)



### 1、Vim基本使用

命令 `vim <可编辑的文件>`

执行该命令，则可以对该文件进行读写操作。进入后

点击【i】键，就可进入insert状态，此时可以编辑文本；

点击【esc】键，可停止编辑，进入可读状态；

键入【esc】后，键入【:wq】保存并退出、【:x】保存并退出、【ZZ】保存并退出、【:w】保存修改、【:q[uit]】退出当前窗口、【:e】重写加载当前文档、【:e!】重新加载当前文档，并丢弃已做的改动

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/vim键盘图.png)



### 2、Vim 3个模式：命令模式、输入模式、底线命令模式

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/vim模式.png)





### 3、Vim按键说明

除了上面简易范例的 i, Esc, :wq 之外，其实 vim 还有非常多的按键可以使用。

**第一部分：一般模式可用的光标移动、复制粘贴、搜索替换等**

| 移动光标的方法     |                                                              |
| :----------------- | ------------------------------------------------------------ |
| h 或 向左箭头键(←) | 光标向左移动一个字符                                         |
| j 或 向下箭头键(↓) | 光标向下移动一个字符                                         |
| k 或 向上箭头键(↑) | 光标向上移动一个字符                                         |
| l 或 向右箭头键(→) | 光标向右移动一个字符                                         |
| [Ctrl] + [f]       | 屏幕『向下』移动一页，相当于 [Page Down]按键 (常用)          |
| [Ctrl] + [b]       | 屏幕『向上』移动一页，相当于 [Page Up] 按键 (常用)           |
| [Ctrl] + [d]       | 屏幕『向下』移动半页                                         |
| [Ctrl] + [u]       | 屏幕『向上』移动半页                                         |
| +                  | 光标移动到非空格符的下一行                                   |
| -                  | 光标移动到非空格符的上一行                                   |
| ==n< space>==      | 那个 n 表示『数字』，例如 20 。按下数字后再按空格键，光标会向右移动这一行的 n 个字符。 |
| 0 或功能键[Home]   | 这是数字『 0 』：移动到这一行的最前面字符处 (常用)           |
| $ 或功能键[End]    | 移动到这一行的最后面字符处(常用)                             |
| H                  | 光标移动到这个屏幕的最上方那一行的第一个字符                 |
| M                  | 光标移动到这个屏幕的中央那一行的第一个字符                   |
| L                  | 光标移动到这个屏幕的最下方那一行的第一个字符                 |
| G                  | 移动到这个档案的最后一行(常用)                               |
| nG                 | n 为数字。移动到这个档案的第 n 行。例如 20G 则会移动到这个档案的第 20 行(可配合 :set nu) |
| gg                 | 移动到这个档案的第一行，相当于 1G 啊！(常用)                 |
| ==n< Enter>==      | n 为数字。光标向下移动 n 行(常用)                            |



| 搜索替换 |                                                              |
| :------- | ------------------------------------------------------------ |
| /word    | 向光标之下寻找一个名称为 word 的字符串。例如要在档案内搜寻 vbird 这个字符串，就输入 /vbird 即可！(常用) |
| ?word    | 向光标之上寻找一个字符串名称为 word 的字符串。               |
| n        | 这个 n 是英文按键。代表重复前一个搜寻的动作。举例来说， 如果刚刚我们执行 /vbird 去向下搜寻 vbird 这个字符串，则按下 n 后，会向下继续搜寻下一个名称为 vbird 的字符串。如果是执行 ?vbird 的话，那么按下 n 则会向上继续搜寻名称为 vbird 的字符串！ |
| N        | 这个 N 是英文按键。与 n 刚好相反，为『反向』进行前一个搜寻动作。例如 /vbird 后，按下 N 则表示『向上』搜寻 vbird 。 |



| 删除、复制与粘贴 |                                                              |
| :--------------- | ------------------------------------------------------------ |
| x, X             | 在一行字当中，x 为向后删除一个字符 (相当于 [del] 按键)， X 为向前删除一个字符(相当于 [backspace] 亦即是退格键) (常用) |
| nx               | n 为数字，连续向后删除 n 个字符。举例来说，我要连续删除 10 个字符， 『10x』。 |
| ==dd==           | 删除游标所在的那一整行(常用)                                 |
| ==ndd==          | n 为数字。删除光标所在的向下 n 行，例如 20dd 则是删除 20 行 (常用) |
| d1G              | 删除光标所在到第一行的所有数据                               |
| dG               | 删除光标所在到最后一行的所有数据                             |
| d$               | 删除游标所在处，到该行的最后一个字符                         |
| ==d0==           | 那个是数字的 0 ，删除游标所在处，到该行的最前面一个字符      |
| ==yy==           | 复制游标所在的那一行(常用)                                   |
| ==nyy==          | n 为数字。复制光标所在行开始的向下 n 行，例如 20yy 则是复制 20 行(常用) |
| y1G              | 复制游标所在行到第一行的所有数据                             |
| yG               | 复制游标所在行到最后一行的所有数据                           |
| y0               | 复制光标所在的那个字符到该行行首的所有数据                   |
| y$               | 复制光标所在的那个字符到该行行尾的所有数据                   |
| ==p, P==         | p 为将已复制的数据在光标下一行贴上，P 则为贴在游标上一行！举例来说，我目前光标在第 20 行，且已经复制了 10 行数据。则按下 p 后， 那 10 行数据会贴在原本的 20 行之后，亦即由 21 行开始贴。但如果是按下 P 呢？那么原本的第 20 行会被推到变成 30 行。(常用) |
| J                | 将光标所在行与下一行的数据结合成同一行                       |
| c                | 重复删除多个数据，例如向下删除 10 行，[ 10cj ]               |
| ==u==            | 复原前一个动作。(常用)                                       |
| [Ctrl]+r         | 重做上一个动作。(常用)                                       |



**第二部分：一般模式切换到编辑模式的可用的按钮说明**

| 进入输入或取代的编辑模式 |                                                              |
| :----------------------- | ------------------------------------------------------------ |
| ==i, I==                 | 进入输入模式(Insert mode)：i 为『从目前光标所在处输入』， I 为『在目前所在行的第一个非空格符处开始输入』。(常用) |
| a, A                     | 进入输入模式(Insert mode)：a 为『从目前光标所在的下一个字符处开始输入』， A 为『从光标所在行的最后一个字符处开始输入』。(常用) |
| o, O                     | 进入输入模式(Insert mode)：这是英文字母 o 的大小写。o 为『在目前光标所在的下一行处输入新的一行』；O 为在目前光标所在处的上一行输入新的一行！(常用) |
| r, R                     | 进入取代模式(Replace mode)：r 只会取代光标所在的那一个字符一次；R会一直取代光标所在的文字，直到按下 ESC 为止；(常用) |
| ==[Esc]==                | 退出编辑模式，回到一般模式中(常用)                           |



**第三部分：一般模式切换到指令行模式的可用的按钮说明**

| 指令行的储存、离开等指令                                     |                                                              |
| :----------------------------------------------------------- | ------------------------------------------------------------ |
| :w                                                           | 将编辑的数据写入硬盘档案中(常用)                             |
| :w!                                                          | 若文件属性为『只读』时，强制写入该档案。不过，到底能不能写入， 还是跟你对该档案的档案权限有关啊！ |
| :q                                                           | 离开 vi (常用)                                               |
| :q!                                                          | 若曾修改过档案，又不想储存，使用 ! 为强制离开不储存档案。    |
| 注意一下啊，那个惊叹号 (!) 在 vi 当中，常常具有『强制』的意思～ |                                                              |
| ==:wq==                                                      | 储存后离开，若为 :wq! 则为强制储存后离开 (常用)              |
| ZZ                                                           | 这是大写的 Z 喔！若档案没有更动，则不储存离开，若档案已经被更动过，则储存后离开！ |
| :w [filename]                                                | 将编辑的数据储存成另一个档案（类似另存新档）                 |
| :r [filename]                                                | 在编辑的数据中，读入另一个档案的数据。亦即将 『filename』 这个档案内容加到游标所在行后面 |
| :n1,n2 w [filename]                                          | 将 n1 到 n2 的内容储存成 filename 这个档案。                 |
| :! command                                                   | 暂时离开 vi 到指令行模式下执行 command 的显示结果！例如 『:! ls /home』即可在 vi 当中看 /home 底下以 ls 输出的档案信息！ |
| ==:set nu==                                                  | 显示行号，设定之后，会在每一行的前缀显示该行的行号           |
| :set nonu                                                    | 与 set nu 相反，为取消行号！                                 |



### 案例：使用vim编写c语言99乘法表程序，并编译

- vim main.c

```c
#include<stdio.h>
main(){
        int num, i, j;
         for(i = 1; i <= 9; i++){
                for(j = 1; j <= i; j++ ){
                        num=i*j;
                        printf("%d*%d=%d  ", i, j, num);
                }
                printf("\n");
        }

 }
```

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/99乘法表.png)



- 保存退出`wq`，通过`gcc`编译c程序

```bash
gcc -o 99.out main.c # 编译mian.c文件，到指定文件99.out ，没有会自动创建
 # -o 是out，用于指定编译输出到的文件
```



- 执行99.out

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/99乘法表输出.png)



## 七、文件系统 挂载mount 与 卸载umount

Linux中的根目录以外的文件要想被访问，需要将其“关联”到根目录下的某个目录来实现，这种关联操作就是“挂载”，这个目录就是“挂载点”，解除次关联关系的过程称之为“卸载”。只有被挂载了之后，才可以访问里面的内容。

> 注意 “挂载点”的目录需要以下几个要求：
>
> （1）目录事先存在，可以用mkdir命令新建目录；
>
> （2）挂载点目录不可被其他进程使用到；
>
> （3）挂载点下原有文件将被隐藏。

挂载操作必须要在最高权限下使用，一般是root用户。



### 1、U盘 挂载与卸载

1. 通过`fdisk -l`命令，查看磁盘分区情况，也可以查看到u盘被分区的位置，以及u盘文件格式

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/fdisk.png" style="float:left" />

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/fdiskUSB.png" style="float:left" />

   这里可以查看到USB的信息，一个exFAT格式的USB被分区到/dev/sdb1位置

   

2. 通过`mkdir /mnt/usb` 在根目录下创建一个文件夹mnt，在mnt文件夹中创建一个usb文件夹，用于将u盘的内容挂载到usb文件夹中，当然也可以根据自己的喜好创建一个文件夹用于挂载u盘

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/mkdir-mnt-usb.png" style="float:left" />

   

3. 通过`mount /dev/sdb1 /mnt/usb` 将u盘分区的位置挂载到usb目录下，此时就可以在usb目录下访问到u盘内容

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/mount.png" style="float:left" />

   >**mount挂载命令使用方法**
   >
   >`mount –t 类型 –o 挂接方式 磁盘分区目录 挂载位置目录`
   >
   >**-t 详细选项**
   >光盘或光盘镜像： iso9660 
   >
   >DOS fat16文件系统：  msdos 
   >
   >Windows 9x fat32文件系统： vfat
   >
   >Windows NT ntfs文件系统： ntfs 
   >
   >Mount Windows文件网络共享： smbfs(需内核支持)推荐cifs 
   >
   >UNIX(LINUX)文件网络共享： nfs
   >
   >自动: auto
   >
   >**-o 详细选项**
   >loop：用来把一个文件当成硬盘分区挂接上系统
   >ro：采用只读方式挂接设备
   >rw：采用读写方式挂接设备
   >
   >
   >
   >(ubuntu系统)
   >
   >默认不支持exfat格式的U盘，只需要运行如下的命令：sudo apt-get install exfat-utils
   >
   >```
   >出现包不能安装：
   >Package <packagename> has no installation candidate
   >```
   >
   >解决方法如下：
   >
   >在root用户下执行
   >
   >\# apt-get update
   >\# apt-get upgrade

   

4. 通过`mount -l `、`df -h` 可查看所有被挂载的信息

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/mount-l.png" style="float:left" />

   其中就可查看到，刚刚被挂载的u盘的信息

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/mount-lUSB.png" style="float:left" />

   通过`df -h`

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/df-h.png" style="float:left" />

   > `-h` 是 --human-readable 打印出来的信息是人能看得懂的！！！

   

5. 通过`umount /mnt/usb`可以下载挂载

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/umount.png" style="float:left" />

   >语法：
   >
   >`umount [选项] [挂载点]/[设备名]`
   >
   >功能：将使用mount命令挂载的文件系统卸载。
   >
   >注意：卸载时，当前目录不能在挂载点中、不能使用挂载点中的数据。



### 2、镜像文件 挂载与卸载

1. 将镜像文件拷贝到linux系统上。

   `cp -i windows_10_ultimate_x64_2019.iso /` 将windows_10_ultimate_x64_2019.iso文件拷贝到根目录下

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/iso拷贝到根目录.png" style="float:left" />

   通过`du -h <文件名>` 可查看文件的大小

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/du命令.png" style="float:left" />

   

2. 执行命令：`mount -o loop <源文件名称> <挂载目录>`

   `mount -o loop windows_10_ultimate_x64_2019.iso /mnt/cd ` 挂载镜像文件，并进入`/mnt/cd`可查看镜像文件内容

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/iso挂载.png" style="float:left" />

   通过`df -h`查看挂载情况

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/iso通过df查看挂载情况.png" style="float:left" />

   
   
3. 卸载：`umount /mnt/cd`

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/iso卸载挂载.png" style="float:left" />





### 3、系统自动挂载

如果我们想实现开机自动挂载某设备，只要修改**/etc/fstab**文件即可。  

---

/etc/fstab 是文本文件，他储存了系统内的静态挂载点，即每次启动时挂载的文件系统。对/etc/fstab文件进行编辑即可，`vim /etc/fstab`，如图：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/fstab文件.png)

编写内容：

1-要挂载的设备或伪文件系统 　 2-挂载点 　　3-文件系统类型 　　4-挂载选项 　　5-转储频率 　　6-自检次序

编写格式: 

UUID=xxx  	xxx  	xxx  	defaults	  0  	0

---

- **要挂载的设备或伪文件系统**：设备文件、LABEL(LABEL="")、UUID(UUID="")、伪文件系统名称(proc, sysfs)

- **挂载点**：指定的文件夹(需要挂载到哪个文件夹中)
- **文件系统类型** ：需要被挂载的文件系统类型
- **挂载选项**：defaults
- **转储频率**：

   　    **0**：不做备份

      　　**1**：每天转储

      　　**2**：每隔一天转储

- **自检次序**：

​      　 **0**：不自检

   　　**1**：首先自检；一般只有rootfs才用1

---

**UUID**

命令：

`blkid [<文件的分区路径>]`  不写分区路径，则会查看所有分区的UUID

`ls -l /dev/disk/by-uuid` 可查看UUID

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/uuid.png" style="float:left" />

---



1. 通过`vim /etc/fstab`，编辑fstab，添加u盘的信息，可实现自动挂载

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/vim-fstab.png" style="float:left" />

2. 输入命令`shutdown -r now`重启，输入`df -h`查看u盘是否自动挂载

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/查看自动挂载的结果.png" style="float:left" />

   由此，可见已被自动挂载



#### /etc/fstab和/etc/mtab的区别

-   **/etc/fstab文件的作用**: 

　　记录了计算机上硬盘分区的相关信息，启动 Linux 的时候，检查分区的 fsck 命令，和挂载分区的 mount 命令，都需要 fstab 中的信息，来正确的检查和挂载硬盘。 

-  **/etc/mtab文件的作用**： 

    　记载的是现在系统已经装载的文件系统，包括操作系统建立的虚拟文件等；而/etc/fstab是系统准备装载的。 每当 mount 挂载分区、umount 卸载分区，都会动态更新 mtab，mtab 总是保持着当前系统中已挂载的分区信息，fdisk、df 这类程序，必须要读取 mtab 文件，才能获得当前系统中的分区挂载情况。当然我们自己还可以通过读取/proc/mount也可以来获取当前挂载信息 



## 八、文件权限

### 1、权限修改 chmod

`chmod`命令(change modify)

使用该命令：所有用户者

**语法：**

`chmod [ugoa...] [[+-=][rwx]...] [,...] `

- u (user)表示该文件的拥有者，g (group)表示与该文件的拥有者属于同一个群体者，o (other)表示其他以外的人，a (all)表示这三者皆是。
- \+ 表示增加权限、- 表示取消权限、= 表示唯一设定权限。
- r 表示可读取，w 表示可写入，x 表示可执行，X 表示只有当该文件是个子目录或者该文件已经被设定过为可执行。
- 最后参数为需要修改权限的对象

**其他参数说明：**

- -c : 若该文件权限确实已经更改，才显示其更改动作
- -f : 若该文件权限无法被更改也不要显示错误讯息
- -v : 显示权限变更的详细资料
- -R : 对目前目录下的所有文件与子目录进行相同的权限变更(即以递回的方式逐个变更)
- --help : 显示辅助说明
- --version : 显示版本



**示例：**

-r--r--r-- file.txt

```
chmod u+w file.txt
// 给file1.txt的属主添加w写权限   -rw-r--r-- file.txt

chmod go+w,u-w file.txt 
// 给file.txt的属主删除w写权限，属组和其他用户添加w写权限 -r--rw-rw- file.txt
```



-r--r--rw- file1.txt

-r--r--rw- file2.txt

```
chmod ug+w,o-w file1.txt file2.txt
// 给file1.txt和file2.txt的属主和属组添加w写权限，给其他用户删除w写权限
// -rw-rw-r-- file1.txt
// -rw-rw-r-- file2.txt
```



-rw-rw-rw- file.txt

```
chmod a+x,a-w file.txt
// 给file.txt的属主，属组，其他用户都增加执行权限，删除写权限 -r-xr-xr-x file.txt
```



==.......==

```shell
chmod 777
# 将文件的属主，属组，陌生的权限全部改成可读可写可执行
```





### 2、修改属主属组 chown

`chown`命令(change own)

使用该命令：root

**语法：**

`chown [-cfhvR] [--help] [--version] user[:group] file...`

**参数：**

- user : 新的文件拥有者的使用者 ID
- group : 新的文件拥有者的使用者组(group)
- -c : 显示更改的部分的信息
- -f : 忽略错误信息
- -h :修复符号链接
- -v : 显示详细的处理信息
- -R : 处理指定目录以及其子目录下的所有文件
- --help : 显示辅助说明
- --version : 显示版本



**示例：**

将文件 file1.txt 的拥有者设为 runoob，群体的使用者 runoobgroup :

```
chown runoob:runoobgroup file1.txt
```

将目前目录下的所有文件与子目录的拥有者皆设为 runoob，群体的使用者 runoobgroup:

```
chown -R runoob:runoobgroup *
```

将文件 file1.txt 的拥有者设为 runoob:

```
chown runoob file1.txt
```



### 3、修改属组 chgrp

chmod 命令可修改属主和属组，chgrp 命令只能修改属组



```shell
[root@localhost test]# ll
---xrw-r-- 1 root root 302108 11-13 06:03 log2012.log
[root@localhost test]# chgrp -v bin log2012.log
```

"log2012.log" 的所属组已更改为 bin

```shell
[root@localhost test]# ll
---xrw-r-- 1 root bin  302108 11-13 06:03 log2012.log
```



### 4、特殊权限SUID、SGID、Sticky

SUID（set user ID）：如果设置了suid，那么其他用户执行该文件时，会以属主的身份执行

SGID（set group ID）:如果设置了sgid，那么其他用户执行该文件时，会以属组的身份执行

Sticky：如果设置了sticky，那么其他用户执行该文件时，会议陌生用户身份执行



**设置特殊权限**

设置SUID：执行权限以s表示

```
chmod u+s file.txt
// 此时file.txt就设置了SUID权限  -rwsrwxrwx file.txt
```

设置SGID：执行权限以s表示

```
chmod g+s file.txt
// 此时file.txt就设置了SUID权限  -rwxrwsrwx file.txt
```

设置Sticky：执行权限以t表示

```
chmod o+s file.txt
// 此时file.txt就设置了SUID权限  -rwxrwxrwt file.txt
```



**特殊权限数字表示**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/特权.png)



### 5、umask

`umask`命令指定在建立文件时预设的权限掩码。将现有的存取权限减掉权限掩码后，即可产生建立文件时预设的权限。

通俗的讲，就是创建新文件时，生成的默认权限。特权位不参与减法运算，特权位设置什么特权就是什么。后3位由最高权限减去权限掩码得到默认权限。

最高权限为7777

第一个7：特权位，有SUID、SGID、Sticky

第二个7：拥有者权限，rwx

第三个7：所在群组权限，rwx

第四个7：默认用户权限，rwx



**示例1：**

设置权限掩码

```
umask 0002
```

创建一个目录

```
mkdir test
```

查询权限

```
ls -l
drwxrwxr-x ... test/
```



**示例2：**

设置权限掩码

```
umask 5002
```

创建一个目录

```
mkdir test
```

查询权限

```
ls -l
drwsrwxr-t ... test/
```



### 案例：用户创建 分组 设置权限

[管理用户和用户组相关命令](https://blog.csdn.net/freeking101/article/details/78201539)

情景模拟：

假设某一个开发团队，有user和admin这两个用户，这两个用户创建的文件全部存放在/var/data目录下。设这个目录的属组为public。如果设置这个目录，使得user和admin创建的文件可以相互访问和修改，并且不允许其他用户的访问，且user不能删除admin用户创建的文件，admin不能删除user用户创建的文件？写出详细的步骤和并操作验证截图。





1. root用户下，创建两个用户 user 和 admin

   ```
   useradd user
   useradd admin
   // 查看用户是否查看成功
   cat /etc/passwd
   
   // 补充：删除用户
   userdel user
   userdel admin
   // 补充：添加用户
   useradd -g public admin // 在有public组的情况下，可以在创建用户时指定所属组
   ```

2. root用户下，创建 public 组群

   ```
   groupadd public
   
   // 补充：删除组
   groupdel public
   ```

3. root用户下，将两个用户归到public组中

   ```
   usermod -g public user
   usermod -g public admin
   ```

4. root用户下，查看用户与用户组的情况

   ```
   groups user admin
   // 展示结果
   user : public
   admin : public
   ```

   此时，已成功将两个用户归为一组

5. root用户下，创建 / data目录

   ```
   mkdir /data
   ```

6. root用户下，查看data目录的权限情况

   ```
   cd /
   ls -l
   ```

   ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/文件权限案例01.png)

7. root用户下，将该目录的属组改为public

   ```
   chgrp public data
   ```

8. root用户下，如果设置这个目录，使得user和admin创建的文件可以相互访问和修改，并且不允许其他用户的访问

   ```
   chmod g+w,o-rx data
   ```

   ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/文件权限案例02.png)

9. user不能删除admin用户创建的文件，admin不能删除user用户创建的文件

   user用户创建一个user目录，设置权限；admin用户创建admin目录，设置权限

   ```
   // user 用户下
   mkdir user
   chmod o-rx user
   
   // admin 用户下
   mkdir admin
   chmod o-rx admin
   ```

   ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/文件权限案例03.png)

10. 在user目录下创建了a.txt，在admin目录下创建了b.txt。验证，user用户不能删除admin用户的文件，admin用户不能删除user用户的文件

    ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/文件权限案例04.png)



## 九、磁盘分区管理

[分区管理][https://www.cnblogs.com/xiangsikai/p/10683209.html]



### df命令 ( 列出文件系统整体的磁盘使用量 )

```bash
df # 列出文件系统整体的磁盘使用量
df -h # 磁盘占用的空间以兆为单位显示 M
```



### du命令 ( 检查指定目录/文件空间使用量 )

```shell
du # 列出该目录下可见文件占用磁盘的大小
du -a # 列出该目录下所有文件(包括隐藏文件)占用磁盘的大小，可见子文件夹
du -sm /* # 检查根目录下每个目录所占用的容量
```



### 1、磁盘分区介绍

- 基本分区（primary partion）
  - 基本分区也称主分区，引导分区、每块磁盘分区主分区与扩展分区加起来不能大于四个。
  - 基本分区创建后可以立即使用，但是有分区数量上限。

- 扩充分区(extension partion)
  - 每块磁盘内只能划分一块扩展分区
  - 扩展分区内可划分任意块逻辑分区
  - 扩展分区创建后不能直接使用，需要在扩展分区内创建逻辑分区

- 逻辑分区（logical partion）
  - 逻辑分区实在扩展分区内创建的分区
  - 逻辑分区相当与一块存储介质，和其他逻辑分区主分区完全独立



### 2、磁盘类型介绍

- IDE硬盘
  - 驱动器标识符为hdx
  - IDE硬盘最多64个分区
  - 其中“hdx~”表明分区所在设备的类型、hd 表示ide、x表示哪块盘、~表示分区号

- SCSI硬盘
  - 驱动器标识符为sdx
  - 其中“sdx~”表明分区所在设备的类型、sd 表示sde、x表示哪块盘、~表示分区号

- hda1、hda2、hda3、hda5、hda6
  - Linux中规定每块硬盘最多4个主分区（包含扩展分区）任何占用分区都要占用分区号
  - 主分区（包含扩展分区分区号）：1 ~ 4 结束。如：hda1、hda2、hda3
  -    逻辑分区：5 ~ 16  结束。如：hda5、hda6 ..
- hda、hdb、hdc、hdd
  - 增一块按磁盘后面按字母顺序名称
  - a为基本盘，b为基本从属盘，c为辅助主盘，d为辅助从属盘



### 3、fdisk 命令

**用途：观察硬盘之实体使用情形与分割硬盘用**

**使用方法：**

　　一、在 console 上输入 fdisk -l /dev/sda ，观察硬盘之实体使用情形。

　　二、在 console 上输入 fdisk /dev/sda，可进入分割硬盘模式。

**参数：**

```
1. 输入 m 显示所有命令列示。

2. 输入 p 显示硬盘分割情形。

3. 输入 a 设定硬盘启动区。

4. 输入 n 设定新的硬盘分割区。

  4.1. 输入 e 硬盘为[延伸]分割区(extend)。

  4.2. 输入 p 硬盘为[主要]分割区(primary)。

5. 输入 t 改变硬盘分割区属性。（制作交换分区时会用到）

6. 输入 d 删除硬盘分割区属性。

7. 输入 q 结束不存入硬盘分割区属性。

8. 输入 w 结束并写入硬盘分割区属性

9. 输入 l 查看分区可可转换类型。
```

```properties
------------------------分区-------------------------------

# 1、进入磁盘
fdisk  /dev/sda  

# 2、列出当前分区表
p   

# 3、添加新分区
n  

# 4、选择开始的块地址,直接回车默认就可以了
回车  

# 5、输入要添加分区的大小+200M，+1G这样的都能识别
+2G   

# 6、确定
回车  

# 6、写入并退出
w   

分区步骤
```

```properties
# 更新当前分区表给内核 这一步非常重要, 否则你的分区重启才能看到.
partprobe  

# 格式化新建分区
mkfs.ext3 /dev/sda6   

# 挂载
mount /dev/sda6 /data   

格式化挂载步骤
```



### 4、格式化分区命令

**命令：根据需要格式化的格式选择命令**

```
mkfifo       mkfs         mkfs.exfat   mkfs.ext4    mkfs.minix   mkfs.ubifs
mkfontdir    mkfs.bfs     mkfs.ext2    mkfs.fat     mkfs.msdos   mkfs.vfat
mkfontscale  mkfs.cramfs  mkfs.ext3    mkfs.jffs2   mkfs.ntfs
```

> 例如：
>
> 想格式化 /dev/hdb1 为exfat格式
>
> 命令：
>
> mkfs.exfat /dev/hdb1

---

**命令：mke2fs**

- 介绍：mke2fs命令是专门用于管理ext系列文件系统的一个专门的工具。
- 使用格式：mke2fs [选项] 磁盘盘符
- 例如：mke2fs -t ext4 -b 8192 /dev/sdb5

**参数：**

```
-t fs-type:指定文件系统类型（如ext2，ext3，ext4等等），则会从/etc/mke2fs.conf文件中读取默认配置；
-b block-size：设置硬盘的block大小。
-L 'LABEL':设置卷标；
-j：创建ext3文件系统，mkfs.ext3自带了该选项；
-N：设置inode节点的数量；
-m：设置为文件系统预留的块的百分比；
-c：在创建文件系统前进程硬盘自检；
```

**案例：**

```
mke2fs -t ext4 -L 'testdisk' /dev/sdb1
```



### 案例：硬盘挂载、硬盘分区、挂载分区

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/硬盘挂载、硬盘分区、挂载分区.png)



**1、添加一个10G硬盘，不重启情况实现挂载**

硬盘挂载可能出现的问题：

> linux 挂载时 mount: wrong fs type, bad option, bad superblock on /dev/sdb
>
> 注意：这个方法会删掉磁盘上的所有数据，谨慎处理   
>
> 原因：挂载时未格式化，使用的文件系统格式不对 
>
> 解决方案：格式化    
>
> sudo mkfs -t ext4 /dev/sdb  
>
> 再挂载
>
> sudo mount /dev/sdb /xxx/  用df -h检查，发现已挂载

- fdisk-l查看硬盘

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/0901fdisk-l查看硬盘.png" style="float:left;" />

- 创建硬盘挂载位置

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/0902硬盘挂载位置.png" style="float:left;" />

- 硬盘挂载，并查看挂载结果

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/0903硬盘挂载.png" style="float:left;" />



**2、fdisk创建1个主分区和1个扩展分区。在扩展分区里创建2个逻辑分区**

- 通过`fdisk /dev/sdb`创建主分区和扩展分区

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/0904创建主分区扩展分区.png" style="float:left;" />

- 查看主分区、扩展分区情况

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/0905查看主分区扩展分区.png" style="float:left;" />

- 创建2个逻辑分区

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/0906创建两个逻辑分区.png" style="float:left;" />

- 查看逻辑分区情况

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/0907逻辑分区情况.png" style="float:left;" />

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/0907通过fdisk-l查看分区情况.png" style="float:left;" />



**3、通过mkfs等命令，分别格式化为ext3、vfat、ntfs**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/0908更改主分区逻辑分区的文件格式.png" style="float:left;" />



**4、创建/media/sdb1、/media/sdb5、/media/sdb6挂载点**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/0909创建挂载点.png" style="float:left;" />



**5、将三种文件系统分别挂载到挂载点**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForLinux/0910主分区逻辑分区挂载.png" style="float:left;" />





## 十、输入/输出重定向

Linux中的重定向有 **标准输入重定向**、**标准输出重定向**、**标准错误输出重定向**

**标准输入重定向**：命令`stdin`（standard in）

**标准输出重定向**：命令`stdout`（standard out）

**标准错误输出重定向**：命令`stderr` （standard error）



### 1、重定向符号

\> 			输出重定向到一个文件或设备 覆盖原来的文件

\>!		    输出重定向到一个文件或设备 强制覆盖原来的文件 

\>>		   输出重定向到一个文件或设备 追加原来的文件 

\< 			输入重定向到一个程序



2>             将一个标准错误输出重定向到一个文件或设备 覆盖原来的文件  b-shell
2>>           将一个标准错误输出重定向到一个文件或设备 追加到原来的文件
2>&1         将一个标准错误输出重定向到标准输出 注释:1 可能就是代表 标准输出
\>&             将一个标准错误输出重定向到一个文件或设备 覆盖原来的文件  c-shell
|&              将一个标准错误 管道 输送 到另一个命令作为输入



在 bash 命令执行的过程中，主要有三种输出入的状况，分别是：

- 标准输入；代码为 0 ；或称为 stdin ；使用的方式为    <

- 标准输出：代码为 1 ；或称为 stdout；使用的方式为  1>

- 错误输出：代码为 2 ；或称为 stderr；使用的方式为   2>



### 2、重定向演示

```bash
[root @heroC test]# ls -al > list.txt
将显示的结果输出到 list.txt 文件中，若该文件以存在则予以取代！


[root @heroC test]# ls -al >> list.txt
将显示的结果累加到 list.txt 文件中，该文件为累加的，旧数据保留！


[root @heroC test]# ls -al  1> list.txt   2> list.err
将显示的数据，正确的输出到 list.txt 错误的数据输出到 list.err


[root @heroC test]# ls -al 1> list.txt 2> &1
将显示的数据，不论正确或错误均输出到 list.txt 当中！错误与正确文件输出到同一个文件中，则必须以该命令来写！不能写成其它格式！

[root @heroC test]# ls -al 1> list.txt 2> /dev/null
将显示的数据，正确的输出到 list.txt 错误的数据则予以丢弃！ /dev/null ，可以说成是黑洞装置。为空，即不保存。

[root @heroC test]# cat < a.txt 1> b.txt 2> &1
将a.txt中的内容输入到cat命令中然后将成功和错误结果都输出到b.txt文件中
```



### 3、重定向作用

为了减轻重复手写文件的麻烦，就引入了重定向。作用就是可以将一个文件中的内容，或者是输出到屏幕的内容，重定向写入到指定的文件中。

• 当屏幕输出的信息很重要，而且我们需要将他存下来的时候；
• 背景执行中的程序，不希望他干扰屏幕正常的输出结果时；
• 一些系统的例行命令（例如写在 /etc/crontab 中的文件）的执行结果，希望他可以存下来时；
• 一些执行命令，我们已经知道他可能的错误讯息，所以想以『 2> /dev/null 』将他丢掉时；
• 错误讯息与正确讯息需要分别输出时。



## 十一、管道

管道允许用户将一条命令的标准输出作为另一条命令的标准输入用管道连接的那些命令称为过滤器（filter）过滤器是一组Linux命令，他们从标准输入得到输入，经过一系列指定方式的处理，将结果送到标准输出Linux中一些复杂任务没有办法用一条命令解决，所以管道和过滤器十分有用。用符号`|`表示管道。



### 补充：grep命令、wc命令



#### grep

用于查找文件里符合条件的字符串。

```
grep [选项] 需要查找到的字符串
```



#### wc (word count)

用于计算字数。利用wc指令我们可以计算文件的Byte数、字数、或是列数，若不指定文件名称、或是所给予的文件名为"-"，则wc指令会从标准输入设备读取数据。

```
wc [选项] <文件>
```

**选项：**

- -c或--bytes或--chars 只显示Bytes数。
- -l或--lines 只显示行数。
- -w或--words 只显示字数。
- --help 在线帮助。
- --version 显示版本信息。



### 管道示例

```
cat /etc/passwd | grep nologin | wc -l
筛选/etc/passwd文件中含有nologin字符串的数据，并计算有多少行
```



## 十二、进程管理



### 1、ps 命令 查看进程

ps命令用于显示当前进程 (process) 的状态。

`ps -xx`

- -a 显示当前终端运行的所有进程信息
- -u 以用户的信息显示进程
- -x 显示后台运行进程的参数
- -ef 可查看进程的父进程号

示例：

```
ps -aux | grep mysql
# 查看mysql的进程

ps -ef | grep mysql
# 查看mysql所有进程的父进程号
```



### 2、pstree 命令 (进程显示更直接) 查看进程

pstree命令将所有行程以树状图显示，树状图将会以 pid (如果有指定) 或是以 init 这个基本行程为根 (root)，如果有指定使用者 id，则树状图会只显示该使用者所拥有的行程。

`pstree -xx`

- -p 显示父进程号
- -u 显示用户组

```
pstree -pu
```





### 3、kill 命令 结束进程 

`kill -9 <进程号>`

```
kill -9 123
#结束123号的进程
```



### 4、端口扫描

使用`lsof -i`

```bash
lsof -i:3306
# 可以查看3306端口是否有进程使用

lsof -i
# 可以查看开启的端口和进程
```



使用`nmap`  基于centos

```bash
# 安装nmap
yum -y install nmap

# 使用namp扫描端口
# nmap [-sS[-Pn]...] ip
nmap -sS 127.0.0.1
nmap -Pn 127.0.0.1

# 卸载nmap
yum -y remove nmap
```







## 十三、防火墙

```bash
#查看firewall服务状态
systemctl status firewalld

#开启、重启、关闭 firewalld.service服务
service firewalld start
service firewalld restart
service firewalld stop

#查看防火墙规则
firewall-cmd --list-all # 查看全部信息
firewall-cmd --list-ports # 只查看端口信息

# 开启端口
#开端口命令
firewall-cmd --zone=public --add-port=8080/tcp --permanent
#重启防火墙
systemctl restart firewalld.service

#命令含义
--zone #作用域
--add-port=8080/tcp # 添加8080端口，协议为tcp
--permanent # 永久生效，没有此参数重启后失效
```





## ls 命令找不到
-bash: ls: command not found 解决办法

**原因：在设置环境变量时，编辑profile文件没有写正确，导致在命令行下 ls等命令不能够识别。可能是在大家写JAVA_HOME以及其他环境路径的时候修改了PATH**

***解决方案：***
 ***export PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin***









