# Spring Cloud - Alibaba

> 课程视频：
>
> https://www.bilibili.com/video/BV1yE411x7Ky/?spm_id_from=333.788.recommend_more_video.16



maven仓库 readme https://github.com/alibaba/spring-cloud-alibaba/blob/master/README-zh.md

依赖：

```xml
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-alibaba-dependencies</artifactId>
            <version>2.2.0.RELEASE</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
```



## 一、==Nacos== 服务注册与发现



### 1、Nacos 简介

官网：https://nacos.io/zh-cn/

前四个字母分别为Naming和Configuration的前两个字母，最后的s为Service

更易构建云原生应用的动态服务发现、配置管理和服务管理平台。

Nacos = Eureka + Config + Bus



#### 下载安装 nacos 服务端

- **下载地址**：

  https://github.com/alibaba/nacos/releases

- **安装**：

  解压，然后运行startup.cmd

  + 如果报错：db.num is null，说明数据库有问题：

    解决办法 ：

     步骤1：mysql新建库：nacos，字符集：utf8 ，排序规则：utf8_general_ci

     步骤2:   %home%/conf/nacos-mysql.sql文件里的sql脚本执行到本机数据库的nacos库中

    步骤3：%home%/conf/application.properties里修改配置

    ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/nacos配置文件.png)

  + 如果报错：nable to start web server; nested exception is org.springframework.boot.web.server.WebServerException: Unable to start embedded Tomcat

    解决办法：

    %home%\bin\work\Tomcat\localhost下空的，%home%\target下有个nacos-server.jar，心中突然想到平时tomcat启动就是把war或者jar解压到tomcat的目录下，说干就干，解压到localhost下后启动，不报错了。

- **访问**：

  `http://localhost:8848/nacos` 用户名密码默认都是nacos

  ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/nacos页面.png)



### 2、Nacos 服务与注册

默认端口号8848



#### 1）提供者

cloud-provider-payment-9001

> 依赖

```xml
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```



> 配置 (官方配置)

```yaml
server:
  port: 9001

spring:
  application:
    name: provider-payment-alibaba
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848

management:
  endpoints:
    web:
      exposure:
        include: "*"
```



> 业务类

```java
@RestController
public class PaymentAlibabaController {

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/payment/nacos/{id}")
    public String getPayment(@PathVariable("id") String id){
        return "nacos registry, serverPort: " + serverPort + "\t id: " + id;
    }
}
```



> 启动

```java
@SpringBootApplication
@EnableDiscoveryClient
public class ProviderPaymentAlibaba9001 {
    public static void main(String[] args) {
        SpringApplication.run(ProviderPaymentAlibaba9001.class,args);
    }
}
```



启动nacos-server

启动cloud-provider-payment-9001  cloud-provider-payment-9002(与9001一致)

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/nacos服务列表.png)

此时发现，服务器已成功注册到了nacos服务器上。



##### tips：创建虚拟项目

这种方式不稳定，因为是创建的虚拟的copy的项目。由于测试如果需要相同cloud-provider-payment-9001项目，那么可以copy一份该项目，不用手动再创建，就能达到两个项目的效果。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/虚拟01.png" style="float:left" />



<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/虚拟02.png" style="float:left" />

经过以上配置，就得到了与cloud-provider-payment-9001相同的虚拟项目，启动即用。

**需要注意的是，调用9011的接口最终还是在9001上运行的**。



#### 2）消费者

cloudalibaba-consumer-order-nacos-83



> 配置

```xml
server:
  port: 83
spring:
  application:
    name: consumer-order-alibaba
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848

# 自定义配置，设置restTemplate访问的微服务名称，直接通过@Value读取这个配置即可
service-url:
  nacos-user-service: http://provider-payment-alibaba

management:
  endpoints:
    web:
      exposure:
        include: "*"

```



> config

```java
@Configuration
public class ApplicationContextConfig {

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
```



> controller

```java
@RestController
public class ConsumerOrderController {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${service-url.nacos-user-service}")
    private String serverUrl;

    @GetMapping(value = "/consumer/payment/ancos/{id}")
    public String getPaymentInfo(@PathVariable("id") String id){
        return restTemplate.getForObject(serverUrl+"/payment/nacos/"+id,String.class);
    }

}
```



> 主启动

```java
@SpringBootApplication
@EnableDiscoveryClient
public class ConsumerOrderAlibaba83 {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerOrderAlibaba83.class,args);
    }
}
```



**由于nacos集成了netflix的ribbon，所以支持负载均衡。**



### 2、Nacos AP和CP的切换

发送put请求`$NACOS_SERVER:8848/nacos/v1/ns/operator/switches?entry=serverMode&value=CP` CP为切换CP模式



#### 1）AP

如果不需要存储服务级别的信息且服务实例是通过nacos-client注册，并能够保持心跳上报，那么就可以选择AP模式。当前主流的服务如SpringCloud 和 Dubbo服务，都是用于AP模式，AP模式为了服务的可用性而减弱了一致性，因此AP模式下只支持注册临时实例。



#### 2）CP

如果需要在服务级别编辑或者存储配置信息，那么CP是必须，K8S服务和DNS服务则是用于CP模式。CP模式下则支持注册持久化实例，此时则是以Raft协议为集群运行模式，该模式下注册实例之前必须先注册服务，如果服务不存在，则返回错误。



## 二、==Nacos== 服务配置中心

nacos支持配置中心。直接在nacos网站上添加配置文件即可。另外，<u>配置文件目前只支持 `properties` 和 `yaml` 类型。</u>

同时data Id的命名规则：

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/配置中心dataid命名规则.png" style="float:left" />



==**Nacos配置中心，只要修改了配置文件，所有的服务器都会同步最新的配置文件信息。实现了不重启真自动刷新。**==



### 1、使用 获取config

先要在nacos配置中心添加配置文件

cloudalibaba-config-ancos-client-3377



> 依赖

```xml
<!--主要依赖-->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
</dependency>

<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
</dependency>

<!--其他依赖-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <scope>runtime</scope>
    <optional>true</optional>
</dependency>

<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.18.12</version>
</dependency>
```



> 配置文件

bootstrap.yaml

```yaml
spring:
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848
      # 配置文件中心获取配置文件
      config:
        server-addr: localhost:8848
        file-extension: yaml

# 配置中心获取的配置文件名：${spring.application.name}-${spring.profiles.active}.{spring.cloud.nacos.config.file-extension}
# config-ancos-client-dev.yaml
```



也可以通过以下方式获取配置文件。

```properties
spring.cloud.nacos.config.extension-configs[0].data-id=datasource.yml
spring.cloud.nacos.config.extension-configs[0].group=dev
spring.cloud.nacos.config.extension-configs[0].refresh=true
spring.cloud.nacos.config.extension-configs[1].data-id=mybatis.yml
spring.cloud.nacos.config.extension-configs[1].group=dev
spring.cloud.nacos.config.extension-configs[1].refresh=true
```





application.yaml

```yaml
server:
  port: 3377

spring:
  application:
    name: config-ancos-client
  profiles:
    active: dev

# 配置中心获取的配置文件名：${spring.application.name}-${spring.profiles.active}.{spring.cloud.nacos.config.file-extension}
# config-ancos-client-dev.yaml
```



> controller

```java
@RestController
@RefreshScope // 可以实现自动刷新配置
public class ConfigAncosController {

    @Value("${config.info}")
    private String configInfo;

    @GetMapping(value = "/config/info")
    public String getConfigInfo(){
        return configInfo;
    }
}
```



> 主程序入口

```java
@SpringBootApplication
@EnableDiscoveryClient
public class ConfigAncos3377 {
    public static void main(String[] args) {
        SpringApplication.run(ConfigAncos3377.class,args);
    }
}
```



### 2、命名空间分组 分类管理



#### Namespace + Group + DataID

类似Java里面的package名和类名
最外面的namespace是可以用于区分部署环境的，Group和DataID逻辑上区分两个目标对象。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/分类管理.png" style="float:left" />



默认情况：
Namespace=public ，Group=DEFAULT_GROUP，默认Cluster是DEFAULT

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/分类管理解释.png" style="float:left" />



```yaml
spring:
  application:
    name: config-ancos-client
  profiles:
    active: dev
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848
      # 配置文件中心获取配置文件
      config:
        server-addr: localhost:8848
        file-extension: yaml
        gourp: TEST_GROUP # 组名
        namespace: b7377a1c-988e-4ed9-955f-a3741b6d3912 # 指定命名空间
```

则会去配置中心的`b7377a1c-988e-4ed9-955f-a3741b6d3912`命名空间下的`TEST_GROUP`组找`config-ancos-client-dev.yaml`配置文件



务必使用分类管理



## 三、==Nacos== 集群和持久化配置



### 1、持久化

Nacos自带一个嵌入式数据库，derby。达不到集群时的数据一致性。

所以需要整到数据库中，Nacos只支持mysql。



- mysql新建库：nacos，字符集：utf8 ，排序规则：utf8_general_ci

- %home%/conf/nacos-mysql.sql文件里的sql脚本执行到本机数据库的nacos库中

- %home%/conf/application.properties里修改配置

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/nacos配置文件.png)





### 2、Linux版 Nacos+MySql 配置

1个Nginx、3个Nacos注册中心、1个MySql数据库

Linux版的Nacos安装与windows一样的。



在mysql中创建nacos库执行`nacos-mysql.sql`文件。然后更改配置文件，连接到mysql。



### 3、集群配置 

由于linux服务器有限，所以用一台linux服务器实现集群效果。大致意思是一致的。



```bash
# nacos单例内存情况：
JAVA_OPT="${JAVA_OPT} -Xms512m -Xmx512m -Xmn256m"

# nacos集群默认最大占2g内存，初始2g，年轻代1g
JAVA_OPT="${JAVA_OPT} -server -Xms2g -Xmx2g -Xmn1g -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m"
```





> **集群配置**

将`conf`目录下的`cluster.conf.example`文件复制一份命名为`cluster.conf`，添加配置，即集群的主机和端口号：

注意：ip地址必须时linux执行`hostname -i`显示的ip地址。

```conf
172.17.49.149:8847
172.17.49.149:8848
172.17.49.149:8849
```



> **修改启动脚本** (老版本不支持端口方式启动多个实例，就按照以下更改，新版已支持)
>
> 如果本方法不行，就只能多复制几个nacos文件，然后更改配置文件的端口号，启动多个nacos了
>
> 达到传入端口号，启动对应的实例。如：startup.sh -p 3333

在修改启动脚本之前请备份一个脚本。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/nacos集群修改的内容.png)



> **Nginx 负载均衡配置**

```bash
		# 分流，负载均衡到这些服务器上
		upstream cluster{
          server 127.0.0.1:8847;
          server 127.0.0.1:8848;
          server 127.0.0.1:8849;
        }

server
    {
        listen 80;
	server_name 47.95.2.239;
	...
      	location /
      	{
      		# 从根目录，代理到cluster，↑
            proxy_pass http://cluster;
      	}
```



> **启动3个Nacos服务器**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/nacos集群结果.png)



> 启动项目

将cloudalibaba-provider-payment-9002项目的nacos连接服务器的配置改成对应的ip和端口号：

```yaml
server-addr: <ip>:<port>
```



启动项目之后，集群服务器上就有了这个微服务。



## 四、==Sentinel== 熔断与限流

A powerful flow control component enabling reliability, resilience and monitoring for microservices. (面向云原生微服务的高可用流控防护组件)

Sentinel 以流量为切入点，从流量控制、熔断降级、系统负载保护等多个维度保护服务的稳定性。

[官方文档](https://github.com/alibaba/Sentinel/wiki/介绍)



### 1、Sentinel Dashboard 下载运行

下载地址：https://github.com/alibaba/Sentinel/releases

下载好了运行jar包即可。默认端口号8080。账户密码默认是sentinel

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/sentinel控制台.png)



### 2、初始化监控

启动nacos服务，sentinel dashboard控制台



> 导入依赖

```xml
<!--nacos-->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
</dependency>
<!--sentinel与nacos数据持久化-->
<dependency>
    <groupId>com.alibaba.csp</groupId>
    <artifactId>sentinel-datasource-nacos</artifactId>
</dependency>
<!--sentinel-->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
```



> 配置中心

```yaml
server:
  port: 8401

spring:
  application:
    name: sentinel-service
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848
    sentinel:
      transport:
        # 配置sentinel dashboard中心，监控8401
        dashboard: localhost:8080
        # 默认8719，如果8719被占用，会从8719自动+1扫描，直至找到未被占用的端口
        port: 8719

management:
  endpoints:
    web:
      exposure:
        include: "*"
```



> controller

```java
@RestController
public class FlowLimitController {

    @GetMapping(value = "/1")
    public String testA(){
        return "testA";
    }

    @GetMapping(value = "/2")
    public String testB(){
        return "testB";
    }

}
```



> 主程序

```java
@SpringBootApplication
@EnableDiscoveryClient
public class SentinelService8401 {
    public static void main(String[] args) {
        SpringApplication.run(SentinelService8401.class,args);
    }
}
```



![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/sentinel实时监控.png)



### 3、流控规则

流控：流量控制，限流

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/流控规则.png)

在sentinel的控制台，可以添加流量控制规则，其中阈值有QPS和线程数。

**QPS**：每秒钟的请求的数量。如果QPS单机阈值为1，那么在1s中内只能处理1条请求，其余的报错。

**线程数**：处理的线程数量。如果线程数为1，那么这个线程还在处理1个请求时，其余过来的请求直接报错。

**直接**：当api达到限流条件时，直接限流。

**关联**：当关联的资源达到界面设置的阈值时，就限流自己。

**链路**：只记录指定链路上的流量（从入口资源进来的流量，如果达到阈值，就进行限流）。

**快速失败**：直接失败，抛异常。

**Warm Up**：根据codeFactor（冷加载因子，默认3）的值，从阈值/codeFactor，经过预热时长，才达到设置的QPS阈值。<u>具体意思是，如果QPS设置的阈值是10，而Warm Up设置的时长是5，那么刚开始阈值从10/3开始，也就是3开始，经过5s，才能达到设置的QPS阈值10。</u>这就是预热，为了防止突然的访问量，而导致系统崩掉。(场景：秒杀等)

**排队等待**：进来的请求，排队等待。设置等待时间。



### 4、熔断 降级规则

sentinel熔断没有半开状态，Hystrix就有半开状态，半开状态就是系统会自动去检测是否请求异常，没有异常就关闭断路器恢复使用，有异常则继续打开断路器不可用。



![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/sentinel降级规则.png)



**RT（平均响应时间，秒级）**

当 1s 内持续进入 N 个请求，对应时刻的平均响应时间（秒级）均超过阈值（`count`，以 ms 为单位），那么在接下的时间窗口（`DegradeRule` 中的 `timeWindow`，以 s 为单位）之内，对这个方法的调用都会自动地熔断（抛出 `DegradeException`）。注意 Sentinel 默认统计的 RT 上限是 4900 ms，**超出此阈值的都会算作 4900 ms**，若需要变更此上限可以通过启动配置项 `-Dcsp.sentinel.statistic.max.rt=xxx` 来配置。



**异常比例（秒级）**

当资源的每秒请求量 >= N（可配置），并且每秒异常总数占通过量的比值超过阈值（`DegradeRule` 中的 `count`）之后，资源进入降级状态，即在接下的时间窗口（`DegradeRule` 中的 `timeWindow`，以 s 为单位）之内，对这个方法的调用都会自动地返回。异常比率的阈值范围是 `[0.0, 1.0]`，代表 0% - 100%。



**异常数（分钟级）**

当资源近 1 分钟的异常数目超过阈值之后会进行熔断。注意由于统计时间窗口是分钟级别的，若 `timeWindow` 小于 60s，则结束熔断状态后仍可能再进入熔断状态。



注意：异常降级仅针对业务异常，对 Sentinel 限流降级本身的异常（`BlockException`）不生效。为了统计异常比例或异常数，需要通过 `Tracer.trace(ex)` 记录业务异常。示例：

```java
Entry entry = null;
try {
  entry = SphU.entry(key, EntryType.IN, key);

  // Write your biz code here.
  // <<BIZ CODE>>
} catch (Throwable t) {
  if (!BlockException.isBlockException(t)) {
    Tracer.trace(t);
  }
} finally {
  if (entry != null) {
    entry.exit();
  }
}
```

开源整合模块，如 Sentinel Dubbo Adapter, Sentinel Web Servlet Filter 或 `@SentinelResource` 注解会自动统计业务异常，无需手动调用。



### 5、热点规则

何为热点？热点即经常访问的数据。很多时候我们希望统计某个热点数据中访问频次最高的 Top K 数据，并对其访问进行限制。比如：

- 商品 ID 为参数，统计一段时间内最常购买的商品 ID 并进行限制
- 用户 ID 为参数，针对一段时间内频繁访问的用户 ID 进行限制

热点参数限流会统计传入参数中的热点参数，并根据配置的限流阈值与模式，对包含热点参数的资源调用进行限流。热点参数限流可以看做是一种特殊的流量控制，仅对包含热点参数的资源调用生效。



#### 1）热点规则 基础项

对请求地址中的参数进行限流，如果包含了该参数，就根据规则进行限流。

**例如：**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/热点规则.png)

**自定义，降级方法**：

```java
@GetMapping(value = "/hotkey")
// value 取名 唯一，在sentinel热点规则中添加时的资源名； 
// blockHandler 服务降级需要执行的方法，只管sentinel控制台规则的异常，业务运行异常使用fallback
@SentinelResource(value = "hotkey", blockHandler = "defaultHotkeyEmergency")
public String defaultHotkey(@RequestParam(value = "p1", required = false) String p1,
                            @RequestParam(value = "p2", required = false) String p2){
    return "---------> defaultHotkey";
}
public String defaultHotkeyEmergency(String p1, String p2, BlockException ex){
    return "---------> defaultHotkeyEmergency";
}
```



请求：`http://localhost:8401/hotkey?p1=name&p2=pwd`

根据以上规则，会对请求中的索引为0的参数进行限流，即p1参数，每秒钟请求1次，超过则降级，恢复窗口时长1s。这个参数索引与方法中接收参数的顺序一致，限流参数索引0，就是方法中限流参数索引0，就只针对p1。

如果请求：`http://localhost:8401/hotkey?p2=pwd`则不会被限流，因为p2在方法参数列表中的索引是1



#### 2）热点规则 例外项

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/热点规则例外项.png)

以上，参数例外，参数索引的值为列外项中设置的值，即当参数索引为0的值为heroc的阈值是200，其余的值限流阈值都是1。

请求：`http://localhost:8401/hotkey?p1=heroc`，每秒达到了200次才会被服务降级。



### 6、系统规则

**Sentinel 系统自适应限流从整体维度对应用入口流量进行控制**，结合应用的 Load、CPU 使用率、总体平均 RT、入口 QPS 和并发线程数等几个维度的监控指标，通过自适应的流控策略，让系统的入口流量和系统的负载达到一个平衡，让系统尽可能跑在最大吞吐量的同时保证系统整体的稳定性。



### 7、@SentinelResource

> 注意：注解方式埋点不支持 private 方法。

`@SentinelResource` 用于定义资源，并提供可选的异常处理和 fallback 配置项。 `@SentinelResource` 注解包含以下属性：

- `value`：资源名称，必需项（不能为空）
- `entryType`：entry 类型，可选项（默认为 `EntryType.OUT`）
- `blockHandler` / `blockHandlerClass`: `blockHandler` 对应处理 `BlockException` 的函数名称，可选项。blockHandler 函数访问范围需要是 `public`，返回类型需要与原方法相匹配，参数类型需要和原方法相匹配并且最后加一个额外的参数，类型为 `BlockException`。blockHandler 函数默认需要和原方法在同一个类中。若希望使用其他类的函数，则可以指定 `blockHandlerClass` 为对应的类的 `Class` 对象，注意对应的函数<u>必需为 static 函数</u>，否则无法解析。
- `fallback`/`fallbackClass`：fallback 函数名称，可选项，用于在抛出异常的时候提供 fallback 处理逻辑。fallback 函数可以针对所有类型的异常（除了`exceptionsToIgnore`里面排除掉的异常类型）进行处理。fallback 函数签名和位置要求：
  - 返回值类型必须与原函数返回值类型一致；
  - 方法参数列表需要和原函数一致，或者可以额外多一个 `Throwable` 类型的参数用于接收对应的异常。
  - fallback 函数默认需要和原方法在同一个类中。若希望使用其他类的函数，则可以指定 `fallbackClass` 为对应的类的 `Class` 对象，注意对应的函数必需为 static 函数，否则无法解析。
- `defaultFallback`（since 1.6.0）：默认的 fallback 函数名称，可选项，通常用于通用的 fallback 逻辑（即可以用于很多服务或方法）。默认 fallback 函数可以针对所有类型的异常（除了`exceptionsToIgnore`里面排除掉的异常类型）进行处理。若同时配置了 fallback 和 defaultFallback，则只有 fallback 会生效。defaultFallback 函数签名要求：
  - 返回值类型必须与原函数返回值类型一致；
  - 方法参数列表需要为空，或者可以额外多一个 `Throwable` 类型的参数用于接收对应的异常。
  - defaultFallback 函数默认需要和原方法在同一个类中。若希望使用其他类的函数，则可以指定 `fallbackClass` 为对应的类的 `Class` 对象，注意对应的函数必需为 static 函数，否则无法解析。
- `exceptionsToIgnore`（since 1.6.0）：用于指定哪些异常被排除掉，不会计入异常统计中，也不会进入 fallback 逻辑中，而是会原样抛出。

> 注：1.6.0 之前的版本 fallback 函数只针对降级异常（`DegradeException`）进行处理，**不能针对业务异常进行处理**。

特别地，若 blockHandler 和 fallback 都进行了配置，则被限流降级而抛出 `BlockException` 时只会进入 `blockHandler` 处理逻辑。若未配置 `blockHandler`、`fallback` 和 `defaultFallback`，则被限流降级时会将 `BlockException` **直接抛出**（若方法本身未定义 throws BlockException 则会被 JVM 包装一层 `UndeclaredThrowableException`）。



---



cloudalibaba-sentinel-service-8401

由于直接下方法下写服务降级处理方法，与业务之间耦合度高，并且业务不纯洁，所以将服务降级处理的方法，统一安排到一个类里面，然后通过以下注解方式，去找到服务降级的处理方法：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/自定服务降级方法解耦.png)



### 8、Sentinel 注解中 fallback 和 blockHandler

fallback只管理java的异常

blockHandler只管理sentinel的异常



#### 1）ribbon + nacos + sentinel

项目：

cloudalibaba-consumer-order-nacos-ribbon-sentinel-84

```yaml
server:
  port: 84
spring:
  application:
    name: consumer-order-nrs
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848
    sentinel:
      transport:
        dashboard: localhost:8080
        port: 8719
management:
  endpoints:
    web:
      exposure:
        include: "*"

service-url:
  nacos-user-service: http://provider-payment-rs
```



cloudalibaba-provider-payment-ribbon-sentinel-9003

cloudalibaba-provider-payment-ribbon-sentinel-9004

```yaml
server:
  port: 9003
spring:
  application:
    name: provider-payment-rs
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848
management:
  endpoints:
    web:
      exposure:
        include: "*"
```





#### 2）feign + nacos + sentinel

cloudalibaba-consumer-order-nacos-ribbon-sentinel-84

增加配置：

```yaml
# feign 开启 sentinel
feign:
  sentinel:
    enabled: true
```



主启动 增加注解开启：

```java
@EnableFeignClients
```



主要代码在：service层、controller层的openfeign部分



### 9、Sentinel 持久化

一旦我们重启应用，sentinel规则将消失，生产环境需要将配置规则进行持久化。

将限流配置规则持久化进Nacos保存，只要刷新8401某个rest地址，sentinel控制台的流控规则就能看到，只要Nacos里面的配置不删除，针对8401上sentinel上的流控规则持续有效。



项目：cloudalibaba-consumer-order-nacos-ribbon-sentinel-84

> 依赖

```xml
<dependency>
    <groupId>com.alibaba.csp</groupId>
    <artifactId>sentinel-datasource-nacos</artifactId>
</dependency>
```



> 配置

```yaml
server:
  port: 84
spring:
  application:
    name: consumer-order-nrs
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848
    sentinel:
      transport:
        dashboard: localhost:8080
        port: 8719
      # 持久化配置
      datasource:
        ds1:
          nacos:
            server-addr: localhost:8848
            dataId: consumer-order-nrs # 就是本微服务名称
            groupId: DEFAULT_GROUP
            data-type: json
            rule-type: flow

management:
  endpoints:
    web:
      exposure:
        include: "*"


service-url:
  nacos-user-service: http://provider-payment-rs

# feign 开启 sentinel
feign:
  sentinel:
    enabled: true
```



> Nacos服务 在配置中心 添加如下配置

这一步的作用是每次消费者微服务启动时在nacos中定义sentinel的流控规则，从而做到持久化的效果

```json
[
    {
        "resource": "fallback",
        "limitApp": "default",
        "grade": 1,
        "count": 1,
        "strategy": 0,
        "controlBehavior": 0,
        "clusterMode": false
    }
]
```

resource：资源名称

limitApp：来源应用

grade：阈值类型，0表示线程数，1表示QPS

count：单机阈值

strategy：流控模式，0表示直接，1表示关联，2表示链路

controlBehavior：流控效果，0表示快速失败，1表示Warm Up，2表示排队等候

clusterMode：集群模式，false表示非集群，true表示集群



![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/sentinel持久化.png)





## 五、==Seata== 处理分布式事务

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/分布式事务.png)

一次业务操作需要跨多个数据源或需要跨多个系统进行远程调用，就会产生分布式事务问题。**微服务之间，共同处理一个业务，要么同时成功，要么同时失败。**



### 1、Seata 简介 术语

Senta是一款开源的分布式事务解决方案，致力于在微服务架构下提供高性能和简单易用的分布式事务服务

官网：http://seata.io/zh-cn/



> **一加三的套件**

一：

**Transation ID XID** 全局唯一的事务ID

三组件：

**TC (Transaction Coordinator) - 事务协调者**

维护全局和分支事务的状态，驱动全局事务提交或回滚。

**TM (Transaction Manager) - 事务管理器**

定义全局事务的范围：开始全局事务、提交或回滚全局事务。

**RM (Resource Manager) - 资源管理器**

管理分支事务处理的资源，与TC交谈以注册分支事务和报告分支事务的状态，并驱动分支事务提交或回滚。



### 2、处理过程

1. TM向TC申请开启一个全局事务，全局事务创建成功并生成一个全局唯一的XID
2. XID在微服务调用链路的上下文中传播
3. RM向TC注册分支事务，将其纳入XID对应全局事务的管辖
4. TM向TC发起针对XID的全局提交或回滚决议
5. TC调度XID下管辖的全部分支事务完成提交或回滚请求



<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/seata处理过程.png" style="zoom:80%;float:left" />



### 3、seata-server 下载运行 配置文件

下载官网github：https://github.com/seata/seata/releases



- 下载之后解压 本安装压缩包`seata-server-0.9.0.zip`

- 进入conf目录，备份`file.conf`，然后更改f`ile.conf`将其设置为持久化的db存储

  ```bash
  
  ## transaction log store, only used in seata-server
  store {
    ## store mode: file、db、redis
    mode = "db" # db持久化
    
    service {
    #vgroup->rgroup
    # my_test_tx_group为分组名称，需要与应用中的配置分组名称一致
    # "default"为seata-server的集群名称，即seata-server注册到注册中心的名称
    vgroup_mapping.my_test_tx_group = "default"
    ...
    }
  
    ## file store property
  
    ## database store property 设置db相关配置
    db {
      datasource = "druid"
      ## mysql/oracle/postgresql/h2/oceanbase etc.
      dbType = "mysql"
      driverClassName = "com.mysql.jdbc.Driver"
      url = "jdbc:mysql://127.0.0.1:3306/seata"
      user = "mysql"
      password = "自己的密码"
      minConn = 5
      maxConn = 30
      globalTable = "global_table"
      branchTable = "branch_table"
      lockTable = "lock_table"
      queryLimit = 100
      maxWait = 5000
    }
  
    ## redis store property
  
  }
  ```

- 更改了`file.conf`配置文件，需要创建seata数据库，执行conf目录下的sql

  <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/seata数据库.png" style="zoom:80%;float:left" />

- 更改`register.conf`配置文件，注册服务中心

  ```bash
  registry {
    # file 、nacos 、eureka、redis、zk、consul、etcd3、sofa
    type = "nacos" # nacos 服务发现与注册中心
  
    nacos {
      application = "seata-server" # 注册的名称
      serverAddr = "127.0.0.1:8848" # 注册地
      group = "SEATA_GROUP" # 分组
      namespace = ""
      cluster = "default"
      username = ""
      password = ""
    }
    ...
  
  }
  ```

- 启动bin目录下的`seata-server.bat`

  <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/seata启动成功.png" style="zoom:80%;float:left" />





#### . seata 数据库创建

seata数据库，seata服务需要的表

```sql
-- the table to store GlobalSession data
drop table if exists `global_table`;
create table `global_table` (
  `xid` varchar(128)  not null,
  `transaction_id` bigint,
  `status` tinyint not null,
  `application_id` varchar(32),
  `transaction_service_group` varchar(32),
  `transaction_name` varchar(128),
  `timeout` int,
  `begin_time` bigint,
  `application_data` varchar(2000),
  `gmt_create` datetime,
  `gmt_modified` datetime,
  primary key (`xid`),
  key `idx_gmt_modified_status` (`gmt_modified`, `status`),
  key `idx_transaction_id` (`transaction_id`)
);

-- the table to store BranchSession data
drop table if exists `branch_table`;
create table `branch_table` (
  `branch_id` bigint not null,
  `xid` varchar(128) not null,
  `transaction_id` bigint ,
  `resource_group_id` varchar(32),
  `resource_id` varchar(256) ,
  `lock_key` varchar(128) ,
  `branch_type` varchar(8) ,
  `status` tinyint,
  `client_id` varchar(64),
  `application_data` varchar(2000),
  `gmt_create` datetime,
  `gmt_modified` datetime,
  primary key (`branch_id`),
  key `idx_xid` (`xid`)
);

-- the table to store lock data
drop table if exists `lock_table`;
create table `lock_table` (
  `row_key` varchar(128) not null,
  `xid` varchar(96),
  `transaction_id` long ,
  `branch_id` long,
  `resource_id` varchar(256) ,
  `table_name` varchar(32) ,
  `pk` varchar(36) ,
  `gmt_create` datetime ,
  `gmt_modified` datetime,
  primary key(`row_key`)
);
```



事务参与的数据库都需要该日志表

```sql
DROP TABLE `undo_log`;
CREATE TABLE `undo_log` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `branch_id` BIGINT(20) NOT NULL,
  `xid` VARCHAR(100) NOT NULL,
  `context` VARCHAR(128) NOT NULL,
  `rollback_info` LONGBLOB NOT NULL,
  `log_status` INT(11) NOT NULL,
  `log_created` DATETIME NOT NULL,
  `log_modified` DATETIME NOT NULL,
  `ext` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
```





### 4、订单/库存/账户余额 业务实践

> 1.数据准备

创建3个数据库 订单表、库存表、用户表：

```sql
CREATE DATABASE seata_order;
CREATE DATABASE seata_storage;
CREATE DATABASE seata_account;
```

分别在3个数据库中创建表：

```sql
CREATE TABLE `t_account` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT DEFAULT NULL COMMENT '用户id',
  `total` DECIMAL(10,0) DEFAULT NULL COMMENT '总额度',
  `used` DECIMAL(10,0) DEFAULT NULL COMMENT '已用额度',
  `residue` DECIMAL(10,0) DEFAULT NULL COMMENT '剩余额度',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT  INTO `t_account`(`id`,`user_id`,`total`,`used`,`residue`) VALUES (1,1,'1000','0','1000');

CREATE TABLE `t_order` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT DEFAULT NULL COMMENT '用户id',
  `product_id` BIGINT DEFAULT NULL COMMENT '产品id',
  `count` BIGINT DEFAULT NULL COMMENT '数量',
  `money` DECIMAL(10,0) DEFAULT NULL COMMENT '金额',
  `status` TINYINT DEFAULT NULL COMMENT '订单状态: 0创建中 1已完结',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;


CREATE TABLE `t_storage` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `product_id` BIGINT DEFAULT NULL COMMENT '产品id',
  `total` BIGINT DEFAULT NULL COMMENT '总库存',
  `used` BIGINT DEFAULT NULL COMMENT '已用库存',
  `residue` BIGINT DEFAULT NULL COMMENT '剩余库存',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT  INTO `t_storage`(`id`,`product_id`,`total`,`used`,`residue`) VALUES (1,1,100,0,100);
```

每个数据库都需要有一张日志表，是seata需要记录事务的日志表：

```sql
DROP TABLE `undo_log`;
CREATE TABLE `undo_log` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `branch_id` BIGINT(20) NOT NULL,
  `xid` VARCHAR(100) NOT NULL,
  `context` VARCHAR(128) NOT NULL,
  `rollback_info` LONGBLOB NOT NULL,
  `log_status` INT(11) NOT NULL,
  `log_created` DATETIME NOT NULL,
  `log_modified` DATETIME NOT NULL,
  `ext` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

```



> 项目

Module-Order-2001

Module-Storage-2002

Module-Account-2003

先下订单，创建订单，调用库存服务器，减库存，调用账户服务器，减余额，将订单状态改为已付款状态。

环境：

使用seata0.9.0版本，mysql5.6版本



> Module-Order-2001、Module-Storage-2002、Module-Account-2003

```xml
<dependencies>
    <dependency>
        <groupId>com.heroc</groupId>
        <artifactId>cloud-api-commons</artifactId>
        <version>1.0</version>
    </dependency>
    <!-- nacos -->
    <dependency>
        <groupId>com.alibaba.cloud</groupId>
        <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
    </dependency>
    <!-- seata-->
    <dependency>
        <groupId>com.alibaba.cloud</groupId>
        <artifactId>spring-cloud-starter-alibaba-seata</artifactId>
        <exclusions>
            <exclusion>
                <groupId>io.seata</groupId>
                <artifactId>seata-all</artifactId>
            </exclusion>
        </exclusions>
    </dependency>
    <dependency>
        <groupId>io.seata</groupId>
        <artifactId>seata-all</artifactId>
        <version>0.9.0</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-openfeign</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
        <groupId>org.mybatis.spring.boot</groupId>
        <artifactId>mybatis-spring-boot-starter</artifactId>
    </dependency>
    <dependency>
        <groupId>com.alibaba</groupId>
        <artifactId>druid</artifactId>
    </dependency>
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
    </dependency>
    <!--jdbc-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-jdbc</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <optional>true</optional>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
    </dependency>
    <!--hutool 测试雪花算法-->
    <dependency>
        <groupId>cn.hutool</groupId>
        <artifactId>hutool-captcha</artifactId>
        <version>5.2.0</version>
    </dependency>
</dependencies>
```



```yaml
server:
  port: 2001

spring:
  application:
    name: order-service
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848
    alibaba:
      seata:
      	# 事务分组 一定要与seata的file.conf文件中service分组名一致
        tx-service-group: hero_tx_group
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    url: jdbc:mysql://localhost:3306/seata_order?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=UTC
    password: 123456
    username: root
    driver-class-name: com.mysql.cj.jdbc.Driver

feign:
  hystrix:
    enabled: false
  client:
    config:
      default:
      	# 为了防止连接超时，而影响测试效果
      	# 连接时间设置为60s
        connectTimeout: 60000
        # 连接成功后读取时间设置为60s
        readTimeout: 60000

logging:
  level:
    io:
      seata: info

mybatis:
  mapper-locations: classpath*:mapper/*.xml
  type-aliases-package: com.heroc.domain
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
    map-underscore-to-camel-case: true
```



> Module-Order-2001 分布式事务部分OrderServiceImpl
>
> 手动回滚：
>
> ```java
> GlobalTransactionContext.reload(RootContext.getXID()).rollback();
> ```

```java
/**
 * seata的分布式事务注解 @GlobalTransactional
 * name 参数为该事务的名字，全局唯一；rollbackFor 回滚要求
 * @param order  
 * @return
 */
@Override
@GlobalTransactional(name = "hero-create-order", rollbackFor = Exception.class)
public boolean createOrder(Order order) {
    try{
        long start = System.currentTimeMillis();

        // 添加订单
        orderMapper.addOrder(order);
        log.info("添加订单。。。");

        // 减少余额
        CommonResult accountDecrease = accountService.decrease(order.getUserId(), order.getMoney());
        log.info("减少余额。。。" + accountDecrease);

        //int num = 10/0;

        // 减少库存
        CommonResult storageDecrease = storageService.decrease(order.getUserId(), order.getCount());
        log.info("减少库存。。。" + storageDecrease);

        // 将订单状态改为已付款状态
        orderMapper.updateOrderStatus(order.getUserId(), (byte) 0);
        log.info("订单完成。。。");

        long end = System.currentTimeMillis();
        log.info("消耗时间 ----> "+ (end-start));
        return true;
    }catch (Exception e){
        log.info("订单异常" + e.getMessage());
        return false;
    }
}
```



> 关于seata的配置文件 针对seata0.9.0版本

每个项目中classpath都需要放置file.conf和registry.conf配置文件

**file.conf**

`主要更改service、store.db`

```bash
transport {
  # tcp udt unix-domain-socket
  type = "TCP"
  #NIO NATIVE
  server = "NIO"
  #enable heartbeat
  heartbeat = true
  #thread factory for netty
  thread-factory {
    boss-thread-prefix = "NettyBoss"
    worker-thread-prefix = "NettyServerNIOWorker"
    server-executor-thread-prefix = "NettyServerBizHandler"
    share-boss-worker = false
    client-selector-thread-prefix = "NettyClientSelector"
    client-selector-thread-size = 1
    client-worker-thread-prefix = "NettyClientWorkerThread"
    # netty boss thread size,will not be used for UDT
    boss-thread-size = 1
    #auto default pin or 8
    worker-thread-size = 8
  }
  shutdown {
    # when destroy server, wait seconds
    wait = 3
  }
  serialization = "seata"
  compressor = "none"
}
service {
  #vgroup->rgroup
  ## 配置事务组名为hero_tx_group； default是seata在服务注册中心的默认分组
  vgroup_mapping.hero_tx_group = "default"
  #only support single node
  default.grouplist = "127.0.0.1:8091"
  #degrade current not support
  enableDegrade = false
  #disable
  disable = false
  #unit ms,s,m,h,d represents milliseconds, seconds, minutes, hours, days, default permanent
  max.commit.retry.timeout = "-1"
  max.rollback.retry.timeout = "-1"
}

client {
  async.commit.buffer.limit = 10000
  lock {
    retry.internal = 10
    retry.times = 30
  }
  report.retry.count = 5
  tm.commit.retry.count = 1
  tm.rollback.retry.count = 1
}

## transaction log store
store {
  ## store mode: file、db
  ## 配置事务日志存储地为db
  mode = "db"

  ## file store
  file {
    dir = "sessionStore"

    # branch session size , if exceeded first try compress lockkey, still exceeded throws exceptions
    max-branch-session-size = 16384
    # globe session size , if exceeded throws exceptions
    max-global-session-size = 512
    # file buffer size , if exceeded allocate new buffer
    file-write-buffer-cache-size = 16384
    # when recover batch read size
    session.reload.read_size = 100
    # async, sync
    flush-disk-mode = async
  }

  ## database store 
  ## 数据库配置
  db {
    ## the implement of javax.sql.DataSource, such as DruidDataSource(druid)/BasicDataSource(dbcp) etc. 
    datasource = "druid"
    ## mysql/oracle/h2/oceanbase etc.
    db-type = "mysql"
    driver-class-name = "com.mysql.jdbc.Driver"
    url = "jdbc:mysql://localhost:3306/seata"
    user = "root"
    password = "12345"
    min-conn = 1
    max-conn = 3
    global.table = "global_table"
    branch.table = "branch_table"
    lock-table = "lock_table"
    query-limit = 100
  }
}
lock {
  ## the lock store mode: local、remote
  mode = "remote"

  local {
    ## store locks in user's database
  }

  remote {
    ## store locks in the seata's server
  }
}
recovery {
  #schedule committing retry period in milliseconds
  committing-retry-period = 1000
  #schedule asyn committing retry period in milliseconds
  asyn-committing-retry-period = 1000
  #schedule rollbacking retry period in milliseconds
  rollbacking-retry-period = 1000
  #schedule timeout retry period in milliseconds
  timeout-retry-period = 1000
}

transaction {
  undo.data.validation = true
  undo.log.serialization = "jackson"
  undo.log.save.days = 7
  #schedule delete expired undo_log in milliseconds
  undo.log.delete.period = 86400000
  undo.log.table = "undo_log"
}

## metrics settings
metrics {
  enabled = false
  registry-type = "compact"
  # multi exporters use comma divided
  exporter-list = "prometheus"
  exporter-prometheus-port = 9898
}

support {
  ## spring
  spring {
    # auto proxy the DataSource bean
    datasource.autoproxy = false
  }
}
```



**registry.conf**

`主要更改registry.type、registry.nacos`

```bash
registry {
  # file 、nacos 、eureka、redis、zk、consul、etcd3、sofa
  type = "nacos"

  nacos {
    serverAddr = "localhost:8848"
    namespace = ""
    cluster = "default"
  }
  eureka {
    serviceUrl = "http://localhost:8761/eureka"
    application = "default"
    weight = "1"
  }
  redis {
    serverAddr = "localhost:6379"
    db = "0"
  }
  zk {
    cluster = "default"
    serverAddr = "127.0.0.1:2181"
    session.timeout = 6000
    connect.timeout = 2000
  }
  consul {
    cluster = "default"
    serverAddr = "127.0.0.1:8500"
  }
  etcd3 {
    cluster = "default"
    serverAddr = "http://localhost:2379"
  }
  sofa {
    serverAddr = "127.0.0.1:9603"
    application = "default"
    region = "DEFAULT_ZONE"
    datacenter = "DefaultDataCenter"
    cluster = "default"
    group = "SEATA_GROUP"
    addressWaitTime = "3000"
  }
  file {
    name = "file.conf"
  }
}

config {
  # file、nacos 、apollo、zk、consul、etcd3
  type = "file"

  nacos {
    serverAddr = "localhost"
    namespace = ""
  }
  consul {
    serverAddr = "127.0.0.1:8500"
  }
  apollo {
    app.id = "seata-server"
    apollo.meta = "http://192.168.1.204:8801"
  }
  zk {
    serverAddr = "127.0.0.1:2181"
    session.timeout = 6000
    connect.timeout = 2000
  }
  etcd3 {
    serverAddr = "http://localhost:2379"
  }
  file {
    name = "file.conf"
  }
}
```



### 5、Seata 原理分析



#### 1）写隔离

- 开启本地事务，拿到 **本地锁** 。
- 一阶段本地事务提交前，需要确保先拿到 **全局锁** 。
- 拿不到 **全局锁** ，不能提交本地事务。
- 拿 **全局锁** 的尝试被限制在一定时间范围内，超出范围将放弃，并回滚本地事务，释放本地锁。
- 如果回滚操作，必须拿到 **本地锁** ，否则一直重试拿去本地锁，直到拿到本地锁，才执行回滚操作。



以一个示例来说明：

两个全局事务 tx1 和 tx2，分别对 a 表的 m 字段进行更新操作，m 的初始值 1000。

tx1 先开始，开启本地事务，拿到本地锁，更新操作 m = 1000 - 100 = 900。本地事务提交前，先拿到该记录的 **全局锁** ，本地提交释放本地锁。 tx2 后开始，开启本地事务，拿到本地锁，更新操作 m = 900 - 100 = 800。本地事务提交前，尝试拿该记录的 **全局锁** ，tx1 全局提交前，该记录的全局锁被 tx1 持有，tx2 需要重试等待 **全局锁** 。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/seata写隔离01.png" style="zoom:80%;float:left" />

tx1 二阶段全局提交，释放 **全局锁** 。tx2 拿到 **全局锁** 提交本地事务。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/seata写隔离02.png" style="zoom:80%;float:left" />

如果 tx1 的二阶段全局回滚，则 tx1 需要重新获取该数据的本地锁，进行反向补偿的更新操作，实现分支的回滚。

此时，如果 tx2 仍在等待该数据的 **全局锁**，同时持有本地锁，则 tx1 的分支回滚会失败。分支的回滚会一直重试，直到 tx2 的 **全局锁** 等锁超时，放弃 **全局锁** 并回滚本地事务释放本地锁，tx1 的分支回滚最终成功。

因为整个过程 **全局锁** 在 tx1 结束前一直是被 tx1 持有的，所以不会发生 **脏写** 的问题。



#### 2）读隔离

在数据库本地事务隔离级别 **读已提交（Read Committed）** 或以上的基础上，Seata（AT 模式）的默认全局隔离级别是 **读未提交（Read Uncommitted）** 。

如果应用在特定场景下，必需要求全局的 **读已提交** ，目前 Seata 的方式是通过 SELECT FOR UPDATE 语句的代理。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/seata读隔离.png" style="zoom:80%;float:left" />

SELECT FOR UPDATE 语句的执行会申请 **全局锁** ，如果 **全局锁** 被其他事务持有，则释放本地锁（回滚 SELECT FOR UPDATE 语句的本地执行）并重试。这个过程中，查询是被 block 住的，直到 **全局锁** 拿到，即读取的相关数据是 **已提交** 的，才返回。

出于总体性能上的考虑，Seata 目前的方案并没有对所有 SELECT 语句都进行代理，仅针对 FOR UPDATE 的 SELECT 语句。



#### 3）事务提交的两个阶段



##### . 第一阶段 加载

在第一阶段，Seata会拦截“业务SQL”

- 解析SQL语义，找到“业务SQL”要更新的业务数据，在业务数据被更新前，将其保存为“before image”
- 执行“业务SQL”更新业务数据，在业务数据更新之后
- 其保存为“after image”，最后生成行锁

以上操作都在一个数据库事务内完成，这样保证了一阶段的原子性。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/seata第一阶段.png" style="zoom:80%;float:left" />



##### . 第二阶段 提交

因为“业务SQL”在一阶段已经提交至数据库，所以Seata框架只需要将一阶段保存的快照数据和行锁删掉，完成数据清理即可。清理seata数据库中关于本事务存储的信息，以及每个相关操作的数据库中日志表的相关记录清理。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/seata第二阶段提交.png" style="zoom:80%;float:left" />



##### . 第二阶段 回滚

如果遇到异常，需要数据回滚，Seata就需要回滚一阶段已经执行的“业务SQL”，还原业务数据。

回滚方式是用“before image”还原业务数据；但在还原前首先要校验脏写，对比“数据库当前业务数据”和“after image”，如果两份数据完全一致说明没有脏写，可以还原业务数据，如果不一致说明有脏写，出现脏写就需要按照对应策略进行操作。

当然，由于seata正常流程，因为写隔离，获取本地锁、全局锁的方式来看，是不会出现脏写的情况。执行本地事务需要本地锁，提交事务需要获取全局锁，回滚需要本地锁。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/seata第二阶段回滚.png" style="zoom:80%;float:left" />



### 6、熔断 服务降级 事务回滚

**方法一：**

- 直接在服务降级的方法里面抛出异常，让seata感知到，需要回滚

**方法二：**

- 在服务降级的方法里面调用回滚api

  ```java
  GlobalTransactionContext.reload(RootContext.getXID()).rollback();
  ```

**方法三：**

- 服务降级的方法，返回特定状态码，获取到该状态码，然后执行api调用，通知seata回滚，不要继续执行了

  ```java
  GlobalTransactionContext.reload(RootContext.getXID()).rollback();
  ```

  



### 7、Seata 1.3

文档： https://developer.aliyun.com/article/768872

项目：seata1.3.0



#### 依赖

```xml
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-seata</artifactId>
    <exclusions>
        <exclusion>
            <groupId>io.seata</groupId>
            <artifactId>seata-all</artifactId>
        </exclusion>
        <exclusion>
            <groupId>io.seata</groupId>
            <artifactId>seata-spring-boot-starter</artifactId>
        </exclusion>
    </exclusions>
</dependency>
<dependency>
    <groupId>io.seata</groupId>
    <artifactId>seata-spring-boot-starter</artifactId>
    <version>1.3.0</version>
</dependency>
```



#### yaml配置

```yaml
seata:
  enabled: true
  application-id: applicationName
  tx-service-group: my_test_tx_group
  enable-auto-data-source-proxy: true
  use-jdk-proxy: false
  excludes-for-auto-proxying: firstClassNameForExclude,secondClassNameForExclude
  client:
    rm:
      async-commit-buffer-limit: 1000
      report-retry-count: 5
      table-meta-check-enable: false
      report-success-enable: false
      saga-branch-register-enable: false
      lock:
        retry-interval: 10
        retry-times: 30
        retry-policy-branch-rollback-on-conflict: true
    tm:
      commit-retry-count: 5
      rollback-retry-count: 5
    undo:
      data-validation: true
      log-serialization: jackson
      log-table: undo_log
    log:
      exceptionRate: 100
  service:
    vgroup-mapping:
      my_test_tx_group: default
    grouplist:
      default: 127.0.0.1:8091
    enable-degrade: false
    disable-global-transaction: false
  transport:
    shutdown:
      wait: 3
    thread-factory:
      boss-thread-prefix: NettyBoss
      worker-thread-prefix: NettyServerNIOWorker
      server-executor-thread-prefix: NettyServerBizHandler
      share-boss-worker: false
      client-selector-thread-prefix: NettyClientSelector
      client-selector-thread-size: 1
      client-worker-thread-prefix: NettyClientWorkerThread
      worker-thread-size: default
      boss-thread-size: 1
    type: TCP
    server: NIO
    heartbeat: true
    serialization: seata
    compressor: none
    enable-client-batch-send-request: true
  config:
    type: file
    consul:
      server-addr: 127.0.0.1:8500
    apollo:
      apollo-meta: http://192.168.1.204:8801
      app-id: seata-server
      namespace: application
    etcd3:
      server-addr: http://localhost:2379
    nacos:
      namespace:
      serverAddr: localhost
      group: SEATA_GROUP
      userName: ""
      password: ""
    zk:
      server-addr: 127.0.0.1:2181
      session-timeout: 6000
      connect-timeout: 2000
      username: ""
      password: ""
  registry:
    type: file
    consul:
      server-addr: 127.0.0.1:8500
    etcd3:
      serverAddr: http://localhost:2379
    eureka:
      weight: 1
      service-url: http://localhost:8761/eureka
    nacos:
      application: seata-server
      server-addr: localhost
      namespace:
      userName: ""
      password: ""
    redis:
      server-addr: localhost:6379
      db: 0
      password:
      timeout: 0
    sofa:
      server-addr: 127.0.0.1:9603
      region: DEFAULT_ZONE
      datacenter: DefaultDataCenter
      group: SEATA_GROUP
      addressWaitTime: 3000
      application: default
    zk:
      server-addr: 127.0.0.1:2181
      session-timeout: 6000
      connect-timeout: 2000
      username: ""
      password: ""
```



#### 修改registry.type、config.type

文件在下载的seata包下

启动包: seata-->conf-->file.conf，修改store.mode="db或者redis"
源码: 根目录-->seata-server-->resources-->file.conf，修改store.mode="db或者redis"

启动包: seata-->conf-->file.conf，修改store.db或store.redis相关属性。
源码: 根目录-->seata-server-->resources-->file.conf，修改store.db或store.redis相关属性。



#### registry.conf

```bash
registry {
  # file 、nacos 、eureka、redis、zk、consul、etcd3、sofa
  type = "file"

  nacos {
    application = "seata-server"
    serverAddr = "127.0.0.1:8848"
    group = "SEATA_GROUP"
    namespace = ""
    cluster = "default"
    username = ""
    password = ""
  }
  ...
  config {
  # file、nacos 、apollo、zk、consul、etcd3
  type = "file"

  nacos {
    serverAddr = "127.0.0.1:8848"
    namespace = ""
    group = "SEATA_GROUP"
    username = ""
    password = ""
  }
 }
```

改成

```bash
registry {
  # file 、nacos 、eureka、redis、zk、consul、etcd3、sofa
  type = "nacos"

  nacos {
    application = "seata-server"
    serverAddr = "localhost:8848"
    namespace = "public"
    cluster = "default"
    username = ""
    password = ""
  }
...
  config {
  # file、nacos 、apollo、zk、consul、etcd3
  type = "nacos"

  nacos {
    serverAddr = "localhost:8848"
    namespace = "public"
    group = "SEATA_GROUP"
    username = ""
    password = ""
  }
}
```



#### store.mode 和 对应的nacos和db连接配置

#### file.conf

注意数据源类型，比如springboot的默认数据源是hikari而不是druid

```bash
store {
  ## store mode: file、db、redis
  mode = "file"
  ...
  db {
    ## the implement of javax.sql.DataSource, such as DruidDataSource(druid)/BasicDataSource(dbcp)/HikariDataSource(hikari) etc.
    datasource = "druid"
    ## mysql/oracle/postgresql/h2/oceanbase etc.
    dbType = "mysql"
    ## mysql8.0
    ## driverClassName = "com.cj.mysql.jdbc.Driver"
    ## url = "jdbc:mysql://127.0.0.1:3306/seata?serverTimezone=UTC&useUnicode=true&characterEncoding=true"
    driverClassName = "com.mysql.jdbc.Driver"
    url = "jdbc:mysql://127.0.0.1:3306/seata"
    user = "mysql"
    password = "mysql"
  }
 ...
}
```

修改为：

```bash
store {
  ## store mode: file、db、redis
  mode = "db"
  
  ...

  ## database store property
  db {
    ## the implement of javax.sql.DataSource, such as DruidDataSource(druid)/BasicDataSource(dbcp)/HikariDataSource(hikari) etc.
    datasource = "druid"
    ## mysql/oracle/postgresql/h2/oceanbase etc.
    ## mysql8.0
    ## driverClassName = "com.cj.mysql.jdbc.Driver"
    ## url = "jdbc:mysql://127.0.0.1:3306/seata?serverTimezone=UTC&useUnicode=true&characterEncoding=true"
    dbType = "mysql"
    driverClassName = "com.mysql.jdbc.Driver"
    url = "jdbc:mysql://127.0.0.1:3306/seata"
    user = "root"
    password = "123456"
  }
  ...
}
```



#### 配置nacos-config.txt

文件地址：https://github.com/seata/seata/blob/1.3.0/script/config-center/config.txt

官网seata-server-0.9.0的conf目录下有该文件，官网seata-server-0.9.0的conf目录下有该文件，后面的版本无该文件需要手动下载执行。

**修改为自己的服务组名，各个微服务之间使用相同的服务组名，务必保持一致！**

```bash
service.vgroupMapping.my_test_tx_group=default
service.vgroupMapping.my_test_tx_group1=default
service.vgroupMapping.my_test_tx_group2=default
service.vgroupMapping.my_test_tx_group3=default
```

配置seata服务器mysql链接信息，注意数据源类型，比如springboot的默认数据源是hikari而不是druid

```bash
store.mode=db
...
store.db.datasource=druid
## 可使用mysql8.0
## store.db.url=jdbc:mysql://127.0.0.1:3306/seata?serverTimezone=UTC&useUnicode=true&characterEncoding=true
store.db.dbType=mysql
store.db.url=jdbc:mysql://127.0.0.1:3306/seata?useUnicode=true
store.db.user=root
store.db.password=123456
```



#### **执行nacos-config.sh脚本**

脚本地址：https://github.com/seata/seata/blob/1.3.0/script/config-center/nacos/nacos-config.sh

官网seata-server-0.9.0的conf目录下有该文件，后面的版本无该文件需要手动下载执行。

如果本地是windows，使用git工具git bash执行nacos-config.sh脚本，将nacos-config.txt重命名为config.txt放到seata目录下，执行：

```bash
sh nacos-config.sh -h localhost -p 8848 -g SEATA_GROUP -u nacos -w nacos
```

执行完成后nacos会新增seata配置。

需要注意**config.txt**中目录的对应关系，否则可能提示finished，其实未执行成功！

```bash
$ sh nacos-config.sh -h localhost -p 8848
set nacosAddr=localhost:8848
set group=SEATA_GROUP
cat: /d/soft/config.txt: No such file or directory
=========================================================================
 Complete initialization parameters,  total-count:0 ,  failure-count:0
=========================================================================
 Init nacos config finished, please start seata-server.
```



#### Seata Server需要依赖的表

表的地址：https://github.com/seata/seata/blob/develop/script/server/db/mysql.sql

新建数据库seata, 创建如下三个表，用于seata服务， 0.0.9版本才有这个文件1.0.0版本后需要手动添加。

```sql
-- the table to store GlobalSession data
DROP TABLE IF EXISTS `global_table`;
CREATE TABLE `global_table` (
  `xid` VARCHAR(128)  NOT NULL,
  `transaction_id` BIGINT,
  `status` TINYINT NOT NULL,
  `application_id` VARCHAR(32),
  `transaction_service_group` VARCHAR(32),
  `transaction_name` VARCHAR(128),
  `timeout` INT,
  `begin_time` BIGINT,
  `application_data` VARCHAR(2000),
  `gmt_create` DATETIME,
  `gmt_modified` DATETIME,
  PRIMARY KEY (`xid`),
  KEY `idx_gmt_modified_status` (`gmt_modified`, `status`),
  KEY `idx_transaction_id` (`transaction_id`)
);

-- the table to store BranchSession data
DROP TABLE IF EXISTS `branch_table`;
CREATE TABLE `branch_table` (
  `branch_id` BIGINT NOT NULL,
  `xid` VARCHAR(128) NOT NULL,
  `transaction_id` BIGINT ,
  `resource_group_id` VARCHAR(32),
  `resource_id` VARCHAR(256) ,
  `lock_key` VARCHAR(128) ,
  `branch_type` VARCHAR(8) ,
  `status` TINYINT,
  `client_id` VARCHAR(64),
  `application_data` VARCHAR(2000),
  `gmt_create` DATETIME,
  `gmt_modified` DATETIME,
  PRIMARY KEY (`branch_id`),
  KEY `idx_xid` (`xid`)
);

-- the table to store lock data
DROP TABLE IF EXISTS `lock_table`;
CREATE TABLE `lock_table` (
  `row_key` VARCHAR(128) NOT NULL,
  `xid` VARCHAR(96),
  `transaction_id` LONG ,
  `branch_id` LONG,
  `resource_id` VARCHAR(256) ,
  `table_name` VARCHAR(32) ,
  `pk` VARCHAR(36) ,
  `gmt_create` DATETIME ,
  `gmt_modified` DATETIME,
  PRIMARY KEY(`row_key`)
);
```



#### AT模式下每个业务数据库需要创建undo_log表，用于seata记录分支的回滚信息

表的地址：https://github.com/seata/seata/blob/1.3.0/script/client/at/db/mysql.sql

```sql
-- 注意此处0.3.0+ 增加唯一索引 ux_undo_log
CREATE TABLE `undo_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(20) NOT NULL,
  `xid` varchar(100) NOT NULL,
  `context` varchar(128) NOT NULL,
  `rollback_info` longblob NOT NULL,
  `log_status` int(11) NOT NULL,
  `log_created` datetime NOT NULL,
  `log_modified` datetime NOT NULL,
  `ext` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
```



## 六、==雪花算法== 生成全局唯一ID

雪花算法是Twitter开源的一个可以生成64bit，不超过19位的long型id。



#### id生成规则硬性要求

- **全局唯一**
- **递增趋势**
- **单调递增**
- **信息安全**：防止竟对方，通过爬取id知道一些销售量的情况等。
- **含时间戳**



#### id生成系统的可用性要求

- **高可用**：发一个获取分布式ID的请求，服务器就可以保证99.999%的情况下给我创建一个唯一的分布式ID
- **低延迟**：发一个获取分布式ID的请求，服务器响应速度要快
- **高QPS**：假如并发一口气10万个创建分布式ID请求同时过来，服务器要顶得住并一下子成功创建10万个唯一的分布式ID



#### ID生成方案

##### 1）通过UUID生成

UUID生成的ID长度为36位，去掉‘-’符号是32位，对于mysql来说，主键长度越短越好，而主键长度太大了，影响MySQL的读取性能。并且UUID生成的字符串是无序的且无递增趋势、不是单调递增的。MySQL底层用的B+树，由于主键的是UUID，大数据情况下，生成B+树困难，可能造成左右树不饱和，大幅度修改B+树的情况很严重。因此UUID不考虑。



##### 2）MySQL自增生成

通过mysql自增的方式去生成id，在单机上面是可以的，通过`replace into`语句代替`insert`语句，`replace into`语句在执行的时候其实是执行的2条语句，查询然后替换。在分布式系统中是影响性能的，并且如果扩容的话，也不好控制id的自增方式。



##### 3）Redis生成

Redis是单线程(不是最新版的redis，最新版redis是多线程的)的天生保证原子性，可以使用原子操作INCR和INCRBY来实现。由于需要保证高可用，因此需要redis集群，并且配置每个redis自增的起始值，以及步长。如果有5台redis服务器，那么每台redis起始值分别为1，2，3，4，5。且步长为5。ID生成如下：

```
A: 1 6 11
B: 2 7 12
C: 3 8 13
D: 4 9 14
E: 5 10 15
```

但是，为了生成ID，而耗费大量精力去配置redis，以及主从复制规则，哨兵模式规则等，如果有一台redis宕机了，则会出现一部分的id空缺。可实施，但不值当。



##### 4）Snowflake 雪花算法

雪花算法解决了这种需求。  

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/雪花算法.png" style="float:left" />

**结构**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/雪花算法结构.png" style="float:left" />



> **TIPS:**
>
> 注意 `IdUtil.createSnowflake`每次调用会创建一个新的Snowflake对象，不同的Snowflake对象创建的ID可能会有重复，因此请自行维护此对象为单例，或者使用`IdUtil.getSnowflake`使用全局单例对象。



```xml
<dependencies>
    <!--hutool 测试雪花算法-->
    <dependency>
        <groupId>cn.hutool</groupId>
        <artifactId>hutool-captcha</artifactId>
        <version>5.2.0</version>
    </dependency>

    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
</dependencies>
```





```java
@Slf4j
@Component
public class IdGeneratorSnowflake {
    private long workerId = 0; // 终端ID
    private long datacenterId = 1; // 数据中心ID
    // 通过hutool创建一个雪花算法
    private Snowflake snowflake = IdUtil.getSnowflake(workerId, datacenterId);

    @PostConstruct // 该注解是在servlet构造函数之后，servlet的init方法之前执行
    public void init() {
        try {
            workerId = NetUtil.ipv4ToLong(NetUtil.getLocalhostStr());
            log.info("当前机器的workerId:{}", workerId);
        } catch (Exception e) {
            log.info("当前机器的workerId获取失败", e);
            workerId = NetUtil.getLocalhostStr().hashCode();
            log.info("当前机器 workId:{}", workerId);
        }

    }

    public synchronized long snowflakeId() {
        return snowflake.nextId();
    }

    public synchronized long snowflakeId(long workerId, long datacenterId) {
        snowflake = IdUtil.createSnowflake(workerId, datacenterId);
        return snowflake.nextId();
    }
}
```

> 1.**@PostConstruct**说明
>
>    被`@PostConstruct`修饰的方法会在服务器加载Servlet的时候运行，并且只会被服务器调用一次，类似于Serclet的inti()方法。被@PostConstruct修饰的方法会在构造函数之后，init()方法之前运行。
>
> 2.**@PreConstruct**说明
>
>    被`@PreConstruct`修饰的方法会在服务器卸载Servlet的时候运行，并且只会被服务器调用一次，类似于Servlet的destroy()方法。被@PreConstruct修饰的方法会在destroy()方法之后运行，在Servlet被彻底卸载之前。





```java
@RestController
public class SnowflakeController {

    @Autowired
    private IdGeneratorSnowflake generatorSnowflake;

    @GetMapping(value = "/snowflake")
    public String getSnowflake(){
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(2,
                Runtime.getRuntime().availableProcessors(),
                3,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(10),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());

        for (int i = 0; i < 10; i++) {
            threadPool.submit(()->{
                System.out.println(generatorSnowflake.snowflakeId());
            });
        }

        threadPool.shutdown();

        return "id完成";
    }
}
```



## 七、==OSS== 对象存储

官方原生文档：https://help.aliyun.com/document_detail/32008.html?spm=a2c4g.11186623.6.804.270c6d1c0p7AzN

官方springcloud alibaba：https://github.com/alibaba/aliyun-spring-boot/tree/master/aliyun-spring-boot-samples/aliyun-oss-spring-boot-sample



### 1、基本使用

阿里云提供了OSS对象存储。

```xml
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alicloud-oss</artifactId>
</dependency>
```



yaml

```yaml
spring:
  cloud:
    alicloud:
      access-key: 
      secret-key: 
      oss:
        endpoint: 
```

AccessKey管理：https://ram.console.aliyun.com/users/new

OSS：https://oss.console.aliyun.com/bucket/oss-cn-beijing/mall-hero/object



测试：

```java
@SpringBootTest
@RunWith(SpringRunner.class)
public class MallProductApplication10000Test {

    @Autowired
    OSS ossClient;

    @Test
    public void oosT() throws FileNotFoundException {
/*        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = "oss-cn-beijing.aliyuncs.com";
        // 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
        String accessKeyId = "LTAI4G7XD2fAvU2qn5eqVyXa";
        String accessKeySecret = "0qavS7bPwZ6RlvORsZSgf5hD6KDPZi";

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);*/

        // 上传文件流。
        InputStream inputStream = new FileInputStream("C:\\Users\\HEROC\\Desktop\\QQ截图20200824141607.png");
        ossClient.putObject("mall-hero", "1.png", inputStream);

        // 关闭OSSClient。
        ossClient.shutdown();
    }
}
```





### 2、用户上传OSS

用户上传OSS，用户发送请求给服务器，服务器发送签名给用户，用户使用该签名，将文件上传到OSS

#### 获取签名

```java
@RestController
@RequestMapping("/thirdparty")
public class OSSController {

    @Resource
    private OSS ossClient;

    @Value("${spring.cloud.alicloud.access-key}")
    private String accessId;

    @Value("${spring.cloud.alicloud.secret-key}")
    private String accessKey;

    @Value("${spring.cloud.alicloud.oss.endpoint}")
    private String endpoint;

    /**
     * 获取上传oss的签名
     * @return
     */
    @RequestMapping("/oss/policy")
    public Map<String, String> ossPolicy(){
        String bucket = "mall-hero"; // 请填写您的 bucketname 。
        String host = "https://" + bucket + "." + endpoint; // host的格式为 bucketname.endpoint
        // callbackUrl为 上传回调服务器的URL，请将下面的IP和Port配置为您自己的真实信息。
        //   String callbackUrl = "http://88.88.88.88:8888";
        String format = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String dir = format+"/"; // 用户上传文件时指定的前缀。

        Map<String, String> respMap = null;
        try {
            long expireTime = 30;
            long expireEndTime = System.currentTimeMillis() + expireTime * 1000;
            Date expiration = new Date(expireEndTime);
            // PostObject请求最大可支持的文件大小为5 GB，即CONTENT_LENGTH_RANGE为5*1024*1024*1024。
            PolicyConditions policyConds = new PolicyConditions();
            policyConds.addConditionItem(PolicyConditions.COND_CONTENT_LENGTH_RANGE, 0, 1048576000);
            policyConds.addConditionItem(MatchMode.StartWith, PolicyConditions.COND_KEY, dir);

            String postPolicy = ossClient.generatePostPolicy(expiration, policyConds);
            byte[] binaryData = postPolicy.getBytes("utf-8");
            String encodedPolicy = BinaryUtil.toBase64String(binaryData);
            String postSignature = ossClient.calculatePostSignature(postPolicy);

            respMap = new LinkedHashMap<String, String>();
            respMap.put("accessid", accessId);
            respMap.put("policy", encodedPolicy);
            respMap.put("signature", postSignature);
            respMap.put("dir", dir);
            respMap.put("host", host);
            respMap.put("expire", String.valueOf(expireEndTime / 1000));


        } catch (Exception e) {
            // Assert.fail(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            ossClient.shutdown();
        }
        return respMap;
    }
}
```



## 八、==Spring Cache==

https://docs.spring.io/spring-framework/docs/current/spring-framework-reference/integration.html#cache

Spring Cache 默认是不同步的，为了防止缓存击穿导致查询数据库压力增加，可以开启同步。

```java
@Cacheable(value = "category", key = "'redissonTest'", sync = true)
```





### 1、reids + spring cache

使用redisson+spring cache

- 引入依赖：spring-boot-starter-cache、spring-boot-starter-data-redis
- 写配置：
  + 自动配置CacheAutoConfiguration会导入 RedisCacheConfiguration

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
    <exclusions>
        <exclusion>
            <groupId>io.lettuce</groupId>
            <artifactId>lettuce-core</artifactId>
        </exclusion>
    </exclusions>
</dependency>
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
</dependency>

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-cache</artifactId>
</dependency>
```



```yaml
spring:
  redis:
    host: 127.0.0.1
    port: 6379
  cache:
    type: redis
    redis:
      time-to-live: 3000        # 过期时间设置
      
      key-prefix: CACHE_        # key的前缀设置为CACHE_；
                                # 如果指定了前缀存入的key：CACHE_redissonTest；
                                # 没有指定就使用注解@Cacheable的value值：category::redissonTest
      
      use-key-prefix: true      # 开启前缀；如果为false，@Cacheable的key是什么存入的key就是什么
      
      cache-null-values: true   # 允许缓存null值，防止缓存穿透
```



 ### 2、相关注解

For caching declaration, Spring’s caching abstraction provides a set of Java annotations:

- `@Cacheable`: Triggers cache population.	触发数据保存到缓存操作

- `@CacheEvict`: Triggers cache eviction.       触发删除缓存操作

  ```java
  // 表示删除category分区下的所有缓存
  @CacheEvice(value="category", allEntries=true)
  ```

  

- `@CachePut`: Updates the cache without interfering with the method execution.    不影响方法执行更新缓存操作

- `@Caching`: Regroups multiple cache operations to be applied on a method.   组合以上多个操作

  ```java
  // 删除category::test1缓存、category::test2缓存
  @Caching(evict = {
      @CacheEvict(value = "category", key = "‘getLevelOneCategories’"),
      @CacheEvict(value = "category", key = "‘getCatalogJson’")
  })
  ```

  

- `@CacheConfig`: Shares some common cache-related settings at class-level.   在类级别共享缓存的相同配置

开启缓存 `@EnableCaching`



```java
// 表示该方法返回的数据需要放到redis缓存中，放在category和product目录下
// 存的key为 category::redissonTest
// 默认使用jdk序列化
// 在调用该方法的时候，首先去redis中查找是否有该值，没有就执行方法，有就直接返回缓存中的数据
@Cacheable(value = "category", key = "'redissonTest'")
// @Cacheable(value = "category", key = "#root.method.name") 则会使用 category::方法名 作为key
public List<CategoryEntity> getLevel1Categorys(){
    ....;
}
```

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/缓存springcache&redis.png)



### 3、自定义配置   修改 k-v 序列化

```java
@Configuration
// 获取到CacheProperties类
@EnableConfigurationProperties(CacheProperties.class)
public class ProductRedisCacheConfig {

    /**
     * 更改redis缓存的序列化方式
     * @return
     */
    @Bean
    public RedisCacheConfiguration redisCacheConfiguration(CacheProperties cacheProperties){
        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig();
        // key为string序列化
        config = config.serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(new StringRedisSerializer()));
        // value为json序列化
        config = config.serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(new GenericFastJsonRedisSerializer()));

        /**
         * 由于自己配置了redisCacheConfiguration，所以spring自动配置的配置就失效了
         * 因此需要去org.springframework.boot.autoconfigure.cache.RedisCacheConfiguration
         * 中将一下配置复制粘贴过来
         */
        CacheProperties.Redis redisProperties = cacheProperties.getRedis();
        if (redisProperties.getTimeToLive() != null) {
            config = config.entryTtl(redisProperties.getTimeToLive());
        }

        if (redisProperties.getKeyPrefix() != null) {
            config = config.prefixKeysWith(redisProperties.getKeyPrefix());
        }

        if (!redisProperties.isCacheNullValues()) {
            config = config.disableCachingNullValues();
        }

        if (!redisProperties.isUseKeyPrefix()) {
            config = config.disableKeyPrefix();
        }

        return config;
    }
}
```



## 九、分布式锁 ==redisson==



### 1、简单分布式锁原理

分布式锁，就是所有的服务同时去获取一个锁，可以利用redis来成为这个锁。redis有一个set方法，即不存在该`k-v`则可以存入，否则不可存入。所以，所有分布式可以同时存入`key`一样，`value`为`uuid`，识别身份的`k-v`值到redis，而k一样，所以只能有一个服务能够存入成功，存入成功会返回true，否则返回false，返回true表示存入成功，那么该服务器就获得了锁，就可以进行接下来的操作。没有获得到的锁，就应该尝试重新插入值，获取锁。

- 所有服务器存入的key必须都是一样的。
- 存入`k-v`值时，需要给key设置过期时间，以防止，在获取锁后的代码发生异常，无法正常删除k-v释放锁。k-v存入和过期时间的设置 必须是同一时间操作，原子性的操作。
- 删除`k-v`值时，也需要原子性的删除，并且在删除的时候要判断，这个`k-v`，中的value是不是自己的uuid，uuid不一致，说明自己的操作时间已经大于了过期时间，自己的存的`k-v`已被redis释放了，为了避免删除了其他服务器存入的`k-v`，所以需要比较uuid。一致，就将自己存入的`k-v`删除掉，即释放锁。



```java
/**
 * 分布式锁，利用所有服务访问redis，共同插入lock为key，uuid为value的键值数据，插入成功的
 * 则获取锁成功，插入失败则获取锁失败。
 * @return
 */
private Map<String, List<Catalog2Vo>> getCatalogJsonRedisLock(){
    ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
    // 再次查看是否已存入缓存中
    String catalogCache = ops.get("catalogJson");
    if (!StringUtils.isEmpty(catalogCache)){
        return JSON.parseObject(catalogCache,new TypeReference<Map<String, List<Catalog2Vo>>>(){});
    }
    /**
     * 加锁保证原子性，setIfAbsent方法，如果不存在则插入，成功插入返回true，否则返回false，
     * 给值设定过期时间，防止进行数据库查找时，抛出异常而无法解锁，导致其他服务无法插入lock
     * 而无法获取锁，执行查库操作。
     * 向redis插入值 获取锁，以及删除值 释放锁，都必须是原子性的，即统一操作。
     */
    String uuid = UUID.randomUUID().toString();
    Boolean lock = ops.setIfAbsent("lock", uuid, 300, TimeUnit.SECONDS);
    if (lock){
        Map<String, List<Catalog2Vo>> catalogJsonDB = null;
        try {
            // 调用数据库查询
            catalogJsonDB = getCatalogJsonDB();
        }finally {
            /**
             * 可能查库时间用了很久，导致这个key可能过期，就需要执行查询key是否还存在，
             * 存在比较value即uuid是不是一样，一样就删除这个key，不一样说明自己的锁过期，
             * 这是是别人的锁，就不能删除。script是redis原子性操作的lua脚本
             */
            String script = "if redis.call('get',KEYS[1]) == ARGV[1] then return redis.call('del',KEYS[1]) else return 0 end";
            Long result = stringRedisTemplate.execute(new DefaultRedisScript<Long>(script, Long.class), Arrays.asList("lock"), uuid);
            return catalogJsonDB;
        }
    }else {
        /**
         * 插入值失败的，进行重试操作，即重新获取锁
         */
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            log.error("睡眠中断异常...");
        }
        return getCatalogJsonRedisLock();
    }
}
```



### 2、Redisson 分布式锁

redis默认加的锁过期时间是30s



https://redis.io/topics/distlock

```xml
<!--redis 分布式锁-->
<dependency>
    <groupId>org.redisson</groupId>
    <artifactId>redisson</artifactId>
    <version>3.12.0</version>
</dependency>
```



[分布式锁官方文档](https://github.com/redisson/redisson/wiki/8.-分布式锁和同步器)



配置文件

```java
@Configuration
public class ProductRedissonConfig {

    /**
     * 创建分布式锁客户端
     * @return
     */
    @Bean(destroyMethod = "shutdown")
    public RedissonClient redissonClient(){
        Config config = new Config();
        // ssl链接使用 rediss://
        config.useSingleServer().setAddress("redis://47.95.2.239:7963");
        return Redisson.create(config);
    }
}
```



#### 1）可重入锁

将k-v形式存入redis中，value存的是`uuid+当前线程号`

##### . 强大之处

通过测试，redis解决两大问题，这是redis分布式锁的强大之处：

- **锁的自动续期。当在调用lock方法时，没有指定时间，则默认使用redisson设置的30s过期时间，但redisson中存入的值在10s后要过期了，该程序还在正常执行时，redisson会将该值重新设置过期时间为30s。如果调用lock()方法设置了过期时间，那么就不会有看门狗来检查程序是否执行完，而重新设置过期时间。**
- **加锁的业务只要运行完，或者该服务器宕机，redis不会给该值增加过期时间，30s后会自动将锁释放。**
- **获取不到锁，会自动重试**

```java
@Autowired
private RedissonClient redissonClient;

@GetMapping("/hello")
@ResponseBody
public String hello(){
    // 分布式 重入锁
    RLock lock = redissonClient.getLock("lock");
    // 上锁
    lock.lock();
    try{
        Thread.sleep(3000);
    }catch (Exception e){
        log.error("睡眠中断");
    }finally {
        // 释放锁
        lock.unlock();
    }
    return "hello";
}
```



#### 2）读写锁



#### 3）信号量



#### 4）闭锁

