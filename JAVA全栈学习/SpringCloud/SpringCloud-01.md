# Spring Cloud - 01

> 课程视频：
>
> https://www.bilibili.com/video/BV1yE411x7Ky/?spm_id_from=333.788.recommend_more_video.16
>
> https://www.bilibili.com/video/BV1jJ411S7xr?p=1（狂神）
>
> 百度网盘：springcloud alibaba全



```xml
<!--springCloud所有依赖 import是导入依赖，解决单一继承的方式-->
	<dependency>
		<groupId>org.springframework.cloud</groupId>
		<artifactId>spring-cloud-dependencies</artifactId>
		<version>Hoxton.SR7</version>
		<type>pom</type>
		<scope>import</scope>
	</dependency>
<!--springBoot所有依赖 import是导入依赖，解决单一继承的方式-->
	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-dependencies</artifactId>
		<version>2.3.2.RELEASE</version>
		<type>pom</type>
		<scope>import</scope>
	</dependency>
```





## Spring Cloud 与 Spring Boot 版本依赖

https://start.spring.io/actuator/info

点击SpringCloud的 Reference Doc. 可以看到依赖的SpringBoot版本，**SpringBoot的版本由SpringCloud决定**



---

1. 这么多服务，客户端该如何去访问？
2. 这么多服务，服务之间如何通信？
3. 这么多服务，如何治理？
4. 服务挂了，怎么办？



- API网关，服务路由
- HTTP、PRC框架，异步调用
- 服务注册与发现、高可用
- 熔断机制，服务降级 (解决服务器宕机问题)



解决方案：

> **一、Spring Cloud NetFlix**

一站式解决方案。

​	Api网关 ---> zuul组件

​	Feign --->  HttpClient --->   HTTP的通信方式，同步并阻塞

​	服务注册与发现，Eureka

​	熔断机制 Hystrix

(NetFlix 宣布无期限停止维护。生态不在维护，造成脱节)



> **二、Apache Dubbo Zookeeper**

​	Api网关 没有，要么找第三方，要么自己实现。

​	Dubbo是一个高性能基于Java实现的RPC通信框架

​	服务注册与发现 Zookeeper

​	熔断机制 没有，借助Hystrix



> **一、Spring Cloud Alibaba**

一站式解决方案。



**SpringCloud就是一个生态，就是解决以上的问题而集合的一系列组件。**



## 一、关于微服务

### 1、什么是微服务

[微服务文章 martin fowler <\<microservice>>](https://www.cnblogs.com/zgynhqf/p/5323056.html)



### 2、微服务技术栈有哪些

| 微服务条目                             | 落地技术                                                    |
| -------------------------------------- | ----------------------------------------------------------- |
| 服务开发                               | **SpringBoot，Spring，SpringMVC**                           |
| 服务配置与管理                         | Netflix公司的Archaius、阿里的Diamond等                      |
| 服务注册与发现                         | **Eureka、Consul、Zookeeper**                               |
| 服务调用                               | **Rest(Restful)、RPC**、gRPC                                |
| 服务熔断器                             | **Hystrix**、Envoy等                                        |
| 负载均衡                               | **Ribbon、Nginx**等                                         |
| 服务接口调用(客户端调用服务的简化工具) | **Feign**等                                                 |
| 消息队列                               | ActiveMQ、**RabbitMQ**、Kafka、**RocketMQ**                 |
| 服务配置中心管理                       | **SpringCloudConfig**、Chef等                               |
| 服务路由（API网关）                    | **Zuul、Gateway**等                                         |
| 服务监控                               | Zabbix、Nagios、Metrics、Specatator等                       |
| 全链路追踪                             | **Zipkin**、Bbrave、Dapper等                                |
| 服务部署                               | **Docker**、OpenStack、Kubernetes等                         |
| 数据流操作开发包                       | SpringCloud Stream(封装与Redis，Rabbit，Kafa等发送接收消息) |
| 事件消息总线                           | **SpringCloud Bus**                                         |



## 二、SpringCloud

### 1、cloud结构

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/cloud结构.png)



### 2、什么是springCloud

SpringCloud基于SpringBoot提供了一套微服务解决方案，包括服务注册与发现，配置中心，全链路监控，服务网关，负载均衡，熔断器等组件，除了基于Netflix的开源组件做高度抽象封装之外，还有一些选型中立的开源组件。

SpringCloud利用SpringBoot的开发遍历性，巧妙地简化了分布式系统基础设施的开发，SpringCloud为开发人员提供了快速构建分布式系统的一些工具，**包括配置管理，服务注册与发现，断路器，路由，微代理，事件总线，全局锁，决策竞选，分布式会话等**，他们都可以用SpringBoot的开发风格做到一键启动和部署。



SpringBoot并没有重复造轮子，他只是将目前各家公司开发的比较成熟，经得起实际考研的服务框架组合起来，通过SpringBoot风格进行再封装，屏蔽掉了复杂的配置和实现原理，**最终给开发者留出了一套简单易懂，易部署、易维护的分布式系统开发工具包**



SpringCloud是分布式微服务框架下的一站式解决方案，是各个微服务架构落地技术的集合体，俗称微服务全家桶。



### 3、SpringBoot与SpringCloud的关系

- SpringBoot专注于快速方便的开发单个个体微服务
- SpringCloud是关注全局微服务协调整理治理框架，它将SpringBoot开发的一个个微服务整合并管理起来，为各个微服务之间提供：**包括配置管理，服务注册与发现，断路器，路由，微代理，事件总线，全局锁，决策竞选，分布式会话等**
- SpringBoot可以离开SpringCloud独立使用，而SpringCloud是离不开SpringBoot
- **SpringBoot专注于快速、方便的开发单个个体微服务，SpringCloud关注全局的服务治理框架**



### 4、Dubbo与SpringCloud的对比

|              | Dubbo       | SpringCloud                  |
| ------------ | ----------- | ---------------------------- |
| 服务注册中心 | Zookeeper   | Spring Cloud NetFlix Eureka  |
| 服务调用方式 | RPC         | Rest API                     |
| 服务监控     | Dubbo-admin | Spring Boot Admin            |
| 断路器       | 不完善      | Spring Cloud NetFlix Hystrix |
| 服务网关     | \           | Spring Cloud NetFlix Zuul    |
| 分布式配置   | \           | Spring Cloud Config          |
| 服务跟踪     | \           | Spring Cloud Sleuth          |
| 消息总线     | \           | Spring Cloud Bus             |
| 数据流       | \           | Spring Cloud Stream          |
| 批量任务     | \           | Spring Cloud Task            |



## 三、==RestTemplate== 学习环境搭建

见项目 **Spring-cloud**

`springcloud-consumer-dept-80`



### 1、常用方法

RestTemplate常用方法

`getForObject`、`postForObject` 返回对象响应体中数据转换成的对象，基本可以理解为json数据

`getForEntity`、`postForEntity`返回对象为ResponseEntity对象，包含了响应中的一些重要信息，比如响应头、响应状态码、响应体 



## 四、==Eureka== 服务注册与发现 (停更)

### 1、什么是Eureka

- Netflix 在设计Eureka时，就遵循AP原则
- Eureka是Netflix的子模块，也是核心模块之一。Eureka是基于Rest的服务，用于定位服务，以实现云端中间层服务发现和故障转移，服务注册与发现对于微服务来说是非常重要的，有了服务发现与注册，只需要使用服务的标识符，就可以访问到服务，而不需要修改服务调用的配置文件了，功能类似于Dubbo的注册中心，比如Zookeeper；



### 2、原理讲解

- Eureka基本框架
  + SpringCloud封装了Netflix公司开发的Eureka模块来实现服务注册和发现
  + Eureka采用了C-S的架构设计，EurekaServer作为服务注册功能的服务器，他是服务注册中心
  + 而系统中的其他微服务。使用Eureka的客户端连接到EurekaServer并维持心跳连接。这样系统的维护人员就可以通过EurekaServer来监控系统中各个微服务是否正常运行，SpringCloud的一些其他模块(比如Zuul)就可以通过EurekaServer来发现系统中的其他微服务，并执行相关的逻辑。
  + Eureka包括两个组件：Eureka Server和Eureka Client
  + Eureka Server提供服务注册，各个节点启动之后，会在EurekaServer中进行注册，这样Eureka Server中的服务注册表中将会存储所有可用服务节点的信息，服务节点的信息可以在界面中直观看到。
  + Eureka Client是一个Java客户端，用于简化EurekaServer的交互，客户端同时也可具备一个内置的，使用轮询负载算法的负载均衡器。在应用启动之后，将会向EurekaServer发送心跳（默认周期为30s）。如果Eureka Server在多个心跳周期内没有接收到某个节点的心跳，EurekaServer将会从服务注册表中把这个服务节点移除掉（默认周期为90s）
- 三大角色
  + Eureka Server 提供服务的注册与发现
  + Service Provider 将自身服务注册到Eureka中，从而使消费方能够找到并使用
  + Service Consumer 服务消费方从Eureka中获取注册服务列表，从而找到消费服务

### 3、Eureka 注册中心配置

> 导入Eureka服务依赖

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
</dependency>
```

> 配置

```yaml
server:
  port: 7001

# Eureka注册中心配置
eureka:
  server:
    # 扫描失效服务时间间隔，默认60*1000ms
    eviction-interval-timer-in-ms: 2000
    # 关闭自我保护机制，默认开启（推荐开启自我保护机制）
    enable-self-preservation: false
  instance:
    a-s-g-name: localhost # Eureka服务端实体名字
  client:
    register-with-eureka: false # 表示是否向Eureka注册中心注册自己
    fetch-registry: false # false表示自己为注册中心
    service-url: # 根据配置文件配置注册中心地址
      defaultZone: http://${eureka.instance.a-s-g-name}:${server.port}/eureka/
```

> 主程序入口开启

```java
@EnableEurekaServer
```



### 4、Eureka 客户端配置(提供方)

> 导入依赖

```xml
<!--eureka 客户端-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
```

> 配置

```yaml
eureka:
  client:
    service-url:
      # 配置注册中心的url地址
      defaultZone: http://localhost:7001/eureka
  instance:
    # 配置实例的名称
    instance-id: provider-dept8081
    # 开启显示ip地址
    prefer-ip-address: true 
    # 向注册中心发送心跳时间间隔，默认30s，发送一次
    lease-renewal-interval-in-seconds: 1
    # 从发送失败心跳开始计时，2秒后从注册中心剔除。默认90s
    lease-expiration-duration-in-seconds: 2
```

> 开启客户端服务

```java
@EnableEurekaClient
```





### 5、Eureka 自我保护机制

一句话总结：某时刻某一个微服务不可以使用了，eureka不会立刻清理，依旧会对该微服务的信息进行保存。

- 默认情况下，如果EurekaServer在一定时间内没有接收到某个微服务实例的心跳，EurekaServer将会注销该实例（默认90s）。但是当网络分区故障发生时，微服务与Eureka之间无法正常通行，以上行为可能变得非常危险，因为微服务本身其实是健康的，此时本不应该注销这个微服务。Eureka通过自我保护机制来解决这个问题，当EurekaServer节点在短时间内丢失过多客户端时（可能发生了网络分区故障），那么这个节点就会进入自我保护模式。一旦进入自我保护模式，EurekaServer就会保护服务注册表中的信息，不再删除服务注册表中的数据（也就不会注销任何微服务）。当网络故障恢复后，该EurekaServer节点会自动退出自我保护模式。
- 自我保护模式中，EurekaServer会保护服务注册表中的信息，不再注销任何服务实例。当它收到的心跳数重新恢复到阈值以上时，该EurekaServer节点就会自动退出自我保护模式。**它的设计哲学就是宁可保留错误的服务注册信息，也不盲目注销任何可能健康的服务实例。**
- 综上，自我保护模式是一种应对网络异常的安全保护措施。它的架构哲学是宁可同时保留所有微服务（健康的微服务和不健康的微服务都会保留），也不盲目注销任何健康的微服务。使用自我保护模式，可以让Eureka集群更加的健壮和稳定
- 在SpringCloud中，可以使用`eureka.server.enable-self-preservation = false`，禁用自我保护模式【不推荐关闭自我保护机制】

`evication-interval-timer-in-ms:60 #扫描失效服务的间隔时间(缺省为60*1000)`



### 6、Eureka 集群

**模拟集群**： 一台服务注册中心崩了，直接换端口就可以访问集群中的其他服务注册中心

详情见项目 spring-cloud



> 注册中心 服务端 配置文件
>
> 有3个Eureka注册中心，访问端口分别是7001，7002，7003
>
> 在配置defaultZone时，将其余的注册中心与当前注册中心关联起来。

```yaml
server:
  port: 7001

# Eureka注册中心配置
eureka:
  instance:
    a-s-g-name: localhost7001 # Eureka服务端实体名字
  client:
    register-with-eureka: false # 表示是否向Eureka注册中心注册自己
    fetch-registry: false # false表示自己为注册中心
    service-url: # 根据配置文件配置注册中心地址
      #defaultZone: http://${eureka.instance.a-s-g-name}:${server.port}/eureka/
      defaultZone: http://localhost7003:7003/eureka/,http://localhost7002:7002/eureka/
```



> 客户端，提供服务端，配置
>
> 将服务都注册到这3个注册中心

```yaml
eureka:
  client:
    service-url:
      defaultZone: http://localhost7001:7001/eureka,http://localhost7002:7002/eureka,http://localhost7003:7003/eureka
  instance:
    instance-id: provider-dept8081
```

效果：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/eureka集群.png)



### 7、CAP

- C （Consistency）强一致性
- A （Availability）可用性
- P （Partition tolerance） 分区容错性

CAP的三进二：CA、AP、CP

**CAP关注的是数据的粒度，而不是整体的设计。**

==CAP理论的核心==

- 一个分布式系统不可能同时很好的满足一致性，可用性和分区容错性这三个需求
- 根据CAP原理，将NoSQL数据库分成满足CA原则，满足CP原则和满足AP原则三大类：
  + CA：单点集群，满足一致性、可用性的系统，通常可扩展性较差
  + CP：满足一致性、分区容错性的系统，通常性能不是特别高
  + AP：满足可用性、分区容错性的系统，通常可能对一致性要求较低



### 8、Eureka比Zookeeper好在哪里？

著名的CAP理论，一个分布式系统不可能同时满足C(一致性)、A(可用性)、P(容错性)

由于分区容错性P在分布式系统中必须要保证的，因此只能在A和C之间进行权衡。

- Zookeeper保证的是CP
- Eureka保证的是AP



#### 1）Zookeeper保证CP

​	当向注册中心查询服务列表时，我们可以容忍注册中心返回的是几分钟以前的注册信息，但不能接受服务直接down掉不可用。也就是说，服务注册功能对可用性的要求要高于一致性。但是zookeeper会出现这样一种情况，当master节点因为网络故障与其他节点失去联系时，剩余节点会重新选取leader。问题在于，选举leader的时间太长30~120s，且选举期间整个zookeeper是不可用状态，这就导致选举期间注册服务的瘫痪。在云部署的环境下，因为网络问题使得zookeeper集群失去master节点是较大概率会发生的事件，虽然五福最终能够恢复，但是漫长的选举时间导致的注册长期不可用是不能容忍的。



#### 2）Eureka保证AP

​	Eureka在设计时就优先保证可用性。Eureka各个节点都是平等的，几个节点挂掉不会影响正常节点的工作，剩余的节点依然可以提供注册和查询服务。而Eureka的客户端在向某个Eureka注册时，如果发现连接失败，则会自动切换至其他节点，只要有一台Eureka还在，就能保证注册服务的可用性，只不过查到的信息可能不是最新的，除此之外，Eureka还有一种自我保护机制，如果在15分钟内超过85%的节点都没有正常的心跳，那么Eureka就认为客户端与注册中心出现网络故障，此时会出现以下几种情况：

1. Eureka不再从注册列表中移除因为长时间没收到心跳而应该过期的服务
2. Eureka仍然能够接收新服务的注册和查询请求，但是不会被同步到其他节点上
3. 当网络稳定时，当前实例新的注册信息会被同步到其他节点中



==因此，Eureka可以很好的应对网络故障导致部分节点失去联系的情况，而不会像zookeeper那样是整个注册服务瘫痪==



## 五、==Ribbon== 客户端负载均衡工具 (停更 影响不大 在用)

### 1、什么是Ribbon

- Spring Cloud Ribbon 是基于Netflix Ribbon实现的一套==客户端负载均衡的工具==
  - 简单的说，Ribbon是Netflix发布的开源项目，主要功能是提供客户端软件的负载均衡算法，将Netflix的中间层服务连接在一起。Ribbon的客户端组件提供一系列完整的配置项，如：连接超时、重试等。简单的说，就是在配置文件中列出LoadBalacer后面所有的机器，Ribbon会自动的帮助你基于某种规则去连接这些机器。也可以使用Ribbon实现自定义的负载均衡算法



### 2、Ribbon 作用 集中式LB 进程式LB

- LB，即负载均衡（Load Balance），在微服务或分布式集群中经常用的一种应用
- 负载均衡简单的说就是将用户的请求平摊的分配到多个服务上，从而达到系统的HA（高可用）
- 常见的负载均衡软件有Nginx、Lvs等
- dubbo、SpringCloud中均为我们提供了负载均衡，SpringCloud的负载均衡算法可自定义
- 负载均衡简单分类：
  + 集中式LB
    - 即在服务的消费方和提供方之间使用独立的LB设施，如Nginx，由该设施负责把访问请求通过某种策略转发至服务的提供方
  + 进程式LB
    - 将LB逻辑集成到消费方，消费方从服务注册中心获知有哪些地址可用，然后自己再从这些地址中选取出一个合适的服务器
    - ==Ribbon就属于进程内LB==，它只是一个类库，继承于消费方进程，消费方通过它来获取到服务提供方的地址



### 3、Ribbon 使用 集成Eureka

详情见Spring-Cloud项目



#### 1）导入依赖

> 导入依赖

客户端，消费方，导入以下依赖

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>

<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-ribbon</artifactId>
</dependency>
```



#### 2）配置

> 配置文件

不像注册中心注册自己，服务url为3个注册中心地址

```yaml
server:
  port: 8080

eureka:
  client:
    register-with-eureka: false
    service-url:
      defaultZone: http://localhost7001:7001/eureka/,http://localhost7003:7003/eureka/,http://localhost7002:7002/eureka/
```



#### 3）实现负载均衡

> 相关代码

```java
@RestController
public class DeptConsumerController {

    /**
     * RestTemplate提供远程访问http服务的模板类
     * */
    private RestTemplate restTemplate;
    
    /**
     *  从eureka注册中心获取id为springcloud-provider-dept的服务，进行通信访问
     */
    private static final String URL_PREFIX = "http://springcloud-provider-dept";

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/consumer/dept/{id}")
    public Dept get(@PathVariable("id")Long id){
        return restTemplate.getForObject(URL_PREFIX+"/dept/"+id, Dept.class);
    }

    @GetMapping("/consumer/dept/all")
    public List<Dept> getAll(){
        return restTemplate.getForObject(URL_PREFIX+"/dept/all", List.class);
    }
}
```



```java
@Configuration
public class BeanConfig {

    @Bean
    @LoadBalanced // 使用netflix的ribbon负载均衡，默认轮询负载均衡算法
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }

}
```



#### 4）负载均衡 注意事项

见项目 **spring-cloud**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/eureka与ribbon整合.png" style="float:left" />

服务注册中心有3个服务器集群，8081和8082两个提供服务器都以

```yaml
spring:
  application:
    name: springcloud-provider-dept
```

同一微服务名字注册到服务注册中心，消费方，通过ribbon负载均衡，默认轮询方式一次访问8081服务器和8082服务器。去执行同样的请求。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/eureka与ribbon整合01.png" style="float:left" />



#### 5）切换Ribbon内置负载均衡算法

只需要在主程序入口类中加入@Bean，注入到IOC容器中即可指定Ribbon内置负载均衡算法，也可以在config包下指定，只要主程序入口类可以扫描的地方即可：

```java
@Bean
public IRule myRule(){
    return new RandomRule();
}
```



### 4、轮询负载均衡算法 原理

见脑图Ribbon



### 5、自定义 负载均衡算法

详情查看`spring-cloud`的**springcloud-consumer-dept-80**项目



经过源码分析，ribbon中的负载均衡算法都是实现接口`IRule` 



这里自定义一个简单的负载均衡算法：每个服务访问5次，然后切换一个新的服务访问

**一定要注意，自定义负载均衡的类注入到IOC容器中，不能放在主程序的上下文中，不能被spring扫描。**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/自定义负载均衡目录.png" style="float:left" />

改造`RandomRule`这个类：

```java
public class HeroRandomRule extends AbstractLoadBalancerRule {
    /** 计数 */
    private int total = 0;
    /** 计当前是哪个服务器 */
    private int currentServerIndex = 0;

    public HeroRandomRule() {
    }

    @SuppressWarnings({"RCN_REDUNDANT_NULLCHECK_OF_NULL_VALUE"})
    public Server choose(ILoadBalancer lb, Object key) {
        if (lb == null) {
            return null;
        } else {
            Server server = null;

            while(server == null) {
                if (Thread.interrupted()) {
                    return null;
                }

                // 获取可用的服务
                List<Server> upList = lb.getReachableServers();
                // 获取全部的服务
                List<Server> allList = lb.getAllServers();
                int serverCount = allList.size();
                if (serverCount == 0) {
                    return null;
                }

                /*int index = this.chooseRandomInt(serverCount);
                server = (Server)upList.get(index);*/

                /** 自定义 核心算法 */
                if (total < 5){
                    server = (Server)upList.get(currentServerIndex);
                    total++;
                }else {
                    total = 0;
                    currentServerIndex = (++currentServerIndex) % upList.size();
                    server = (Server)upList.get(currentServerIndex);
                    total++;
                }


                if (server == null) {
                    Thread.yield();
                } else {
                    if (server.isAlive()) {
                        return server;
                    }

                    server = null;
                    Thread.yield();
                }
            }

            return server;
        }
    }

    protected int chooseRandomInt(int serverCount) {
        return ThreadLocalRandom.current().nextInt(serverCount);
    }

    @Override
    public Server choose(Object key) {
        return this.choose(this.getLoadBalancer(), key);
    }

    @Override
    public void initWithNiwsConfig(IClientConfig clientConfig) {
    }
}
```



```java
@Configuration
public class HeroRule {

    @Bean
    public IRule heroRandomRule(){
        /** 将自定义的负载均衡注入到IOC容器中 */
        return new HeroRandomRule();
    }

}
```



在程序入口上方，加入`@RibbonClient`注解：

* name 指定服务器，功能区
* configuration 该服务器(功能区)使用的负载均衡算法

```java
@RibbonClient(name = "springcloud-provider-dept", configuration = HeroRule.class)
```



## 六、==Feign== 集成Ribbon使得面向接口 (停更)

**由于Feign已停更，取而代之的是OpenFeign**



### 1、Feign 简介

Feign是声明式的web service客户端，他让微服务之间的调用变得更加简单，类似controller调用service。spring Cloud 集成了Rribbon和Eureka，可在使用Feign时提供负载均衡的http客户端。**Feign集成了Ribbon**



Feign，主要是社区，面向接口编程，这是很多程序员的规范。调用微服务访问两种方法：

- 微服务名字【Ribbon】
- 接口和注解【Feign】



### 2、Feign 作用

- Feign旨在使编程Java Http客户端变得更容易

- 前面使用Ribbon+Rest，利用Rest对Http请求的封装处理，形成了一套模板化的调用方法。但是在实际开发中，由于对服务依赖的调用可能不止一处，往往一个接口会被多处调用，所以通常会针对每个服务自行封装一些客户端来包装这些依赖服务的调用。所以，Feign在此基础上做了进一步的封装，由他来帮助我们定义和实现依赖服务接口的定义。==在Feign的实现下，我们只需要创建一个接口并使用注解方式来配置它（类似于以前Dao接口上标注的Mapper注解，现在是一个微服务接口上面标注一个Feign注解即可）==即可完成对服务提供方的接口绑定，简化了使用Spring Cloud Ribbon时，自动封装服务调用客户端的开发量。

- **Feign集成了Ribbon** 

  利用Ribbon维护了服务列表信息，并且通过轮询实现了客户端的负载均衡，而与Ribbon不同的是：通过Feign只需要定义服务绑定接口且以声明式的方法，优雅而且简单的实现了服务调用。



### 3、Feign 使用 切换负载均衡

Feign只是集成了Ribbon，使得使用Rribbon更加方便，将Ribbon+Rest风格进行了封装，封装成了接口的形式去调用，本质没有变。Feign内置了Ribbon的负载均衡操作，要设置自定义负载均衡算法，还是得导入Ribbon依赖，通过Ribbon方式来实现自定义负载均衡。



项目：`springcloud-consumer-dept-feign-80`



> 导入依赖

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-feign</artifactId>
    <version>1.4.7.RELEASE</version>
</dependency>

<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-ribbon</artifactId>
</dependency>
```



> 提供接口：

```java
@Component
@FeignClient(value = "SPRINGCLOUD-PROVIDER-DEPT")
public interface DeptFeignService {

    @PostMapping("/add")
    Boolean addDept(Dept dept);

    @GetMapping("/dept/{id}")
    Dept getDeptById(@PathVariable("id") Long id);

    @GetMapping("/dept/all")
    List<Dept> getDepts();

}
```

`@FeignClient` 表示指定服务器，从该服务器中获取资源。每一个接口得请求方式以及url都必须与服务器的请求方式和url都一致。这个接口的目的就是其他类调用了这个接口的方法，就能够访问到`SPRINGCLOUD-PROVIDER-DEPT`服务器上的`/dept/{id}`请求的方法。



> 程序入口

```java
@SpringBootApplication
@EnableEurekaClient
@RibbonClient(value = "SPRINGCLOUD-PROVIDER-DEPT", configuration = HeroRule.class)
@EnableFeignClients(basePackages = "com.heroc.service")
public class FeignConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(FeignConsumerApplication.class,args);
    }
}
```

`@EnableFeignClients` 指定扫描的包，即扫描定义的接口DeptFeignService

`@RibbonClient` 是指定服务器，使用指定的自定义的负载均衡算法



也可以在config包下，指定Ribbon自带的负载均衡策略：

```java
@Bean
public IRule myRule(){
    return new RandomRule();
}
```



## 七、==Hystrix== 熔断器 （停更）

服务降级 fallback

服务熔断  break

服务限流 serverlimit

服务监控

......



需要服务降级的情况：

- 运行时异常
- 超时异常
- 服务熔断
- 服务宕机



### 1、服务雪崩

​	多个微服务之间调用的时候，假设微服务A调用微服务B和微服务C，微服务B和微服务C有调用其他的微服务，这就是所谓的“扇出”、如果扇出的链路上某个微服务的调用响应时间过长或者不可用，对微服务A的调用就会占用越来越多的系统资源，进而引起系统崩溃，所谓的“雪崩效应”。

​	对于高流量的应用来说，单一的后端依赖可能会导致所有服务器上的所有资源都在几秒中内饱和。比失败更糟糕的是，这些应用程序还可能导致服务之间的延迟增加，备份队列，线程和其他系统资源紧张，导致整个系统发生更多的级联故障，这些都表示需要对故障和延迟进行隔离和管理，以便单个依赖关系失败，不能取消整个应用程序或系统。



### 2、Hystrix 介绍

​	Hystrix是一个用于处理分布式系统的延迟或容错的开源库，在分布式系统里，许多依赖不可避免的会调用失败，比如超时，异常等。Hystrix能够保证在一个依赖出问题的情况下，不会导致整体服务失败，避免级联故障，以提高分布式系统的弹性。

​	“断路器”本身是一种开关装置，当某个服务单元发生故障之后，通过断路器的故障监控（类似熔断保险丝），**向调用方返回一个服务预期的，可处理的备选响应（FallBack），而不是长时间的等待或抛出调用方法无法处理的异常，这样就可以保证了服务调用方的线程不会被长时间**，不必要的占用，从而避免了故障在分布式系统中的蔓延，乃至雪崩。



### 3、服务熔断

​	熔断机制是对应雪崩效应的一种微服务链路保护机制。就是防止微服务整个系统的雪崩。**服务端进行熔断。**

​	当扇出链路的某个微服务不可用或者响应时间太长时，会进行服务的降级，==进而熔断该节点微服务的调用，快速返回错误的响应信息==。当检测到该节点微服务调用响应正常后恢复调用链路。在SpringCloud框架中熔断机制通过Hystrix实现。Hystrix会监控微服务间调用的状况，当失败的调用到一定阈值，**缺省是5秒内20次调用失败就会启动熔断机制**。熔断机制的注解是`@HystrixCommand`

**见《SpringCloud - Alibaba 之 Hystrix 补充》**



那么，什么是服务熔断呢？
服务熔断：当下游的服务因为某种原因突然**变得不可用**或**响应过慢**，上游服务为了保证自己整体服务的可用性，不再继续调用目标服务，直接返回，快速释放资源。如果目标服务情况好转则恢复调用。
需要说明的是熔断其实是一个框架级的处理，那么这套熔断机制的设计，基本上业内用的是`断路器模式`，如`Martin Fowler`提供的状态转换图如下所示
![img](https://img2018.cnblogs.com/blog/725429/201901/725429-20190130230717121-435467568.jpg)

- 最开始处于`closed`状态，一旦检测到错误到达一定阈值，便转为`open`状态；
- 这时候会有个 reset timeout，到了这个时间了，会转移到`half open`状态；
- 尝试放行一部分请求到后端，一旦检测成功便回归到`closed`状态，即恢复服务；

业内目前流行的熔断器很多，例如阿里出的Sentinel,以及最多人使用的Hystrix
在Hystrix中，对应配置如下

```
//滑动窗口的大小，默认为20
circuitBreaker.requestVolumeThreshold 
//过多长时间，熔断器再次检测是否开启，默认为5000，即5s钟
circuitBreaker.sleepWindowInMilliseconds 
//错误率，默认50%
circuitBreaker.errorThresholdPercentage
```

每当20个请求中，有50%失败时，熔断器就会打开，此时再调用此服务，将会直接返回失败，不再调远程服务。直到5s钟之后，重新检测该触发条件，判断是否把熔断器关闭，或者继续打开。

这些属于框架层级的实现，我们只要实现对应接口就好！



### 4、Hystrix 服务降级 使用

服务熔断，导致服务降级：



> 导入依赖

```xml
<!--hystrix 熔断器-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
</dependency>
```



> 编写代码

```java
@RestController
public class DeptController {
    private DeptService deptService;

    @Autowired
    public void setDeptService(DeptService deptService) {
        this.deptService = deptService;
    }


    @GetMapping("/dept/{id}")
    @HystrixCommand(fallbackMethod = "getDeptByIdHystrix")
    public Dept getDeptById(@PathVariable("id") Long id){
        Dept dept = deptService.queryDeptById(id);
        if (dept == null){
            throw new RuntimeException("id输入有误");
        }
        return dept;
    }

    public Dept getDeptByIdHystrix(@PathVariable("id") Long id){
        return new Dept()
                .setId(id)
                .setName("ID does not exist, there is no corresponding user name @Hystrix")
                .setDbSource("not found datasource");
    }

}
```

重点在于注解`@HystrixCommand` 当执行的getDeptById方法抛出异常时，就会通过注解找到回滚方法getDeptByIdHystrix进行执行熔断代码。



> 程序入口

```java
@SpringBootApplication
@EnableEurekaClient
@EnableCircuitBreaker
public class ProviderDeptHystrixApplication8081 {
    public static void main(String[] args) {
        SpringApplication.run(ProviderDeptHystrixApplication8081.class,args);
    }
}
```

在程序入口要开启`@EnableCircuitBreaker` 断路器



当查询到的dept为null时，就会抛出运行时异常，这时候就会执行getDeptByIdHystrix方法，返回提示值。防止大量请求被阻断而进行等待，等待量大了，内存就会被占用，最终导致雪崩。解决办法就是熔断机制，及时返回结果。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/熔断器效果.png" style="float:left" />

### 5、Hystrix 服务降级

降级服务要与Feign一同实现。**客户端进行降级操作。**

由于大量用户访问一个微服务，而其他微服务访问量很少，为了给访问高的微服务让步，就可以关闭其他访问量极低的微服务。所以就可以通过服务降级来实现。



项目：`springcloud-api`

> 回滚工厂

作用就是，当服务器不可用时，或者关闭时，可以执行回滚工厂的方法，将结果返回给客户端。

```java
@Component
public class DeptFallbackFactory implements FallbackFactory {

    @Override
    public DeptFeignService create(Throwable throwable) {
        return new DeptFeignService() {
            @Override
            public Boolean addDept(Dept dept) {
                return false;
            }

            @Override
            public Dept getDeptById(Long id) {
                return new Dept()
                        .setId(id)
                        .setName("The client provides degradation service, and the server is temporarily shut down. Please try again later.")
                        .setDbSource("not found dbSource");
            }

            @Override
            public List<Dept> getDepts() {
                ArrayList<Dept> depts = new ArrayList<>();
                depts.add( new Dept()
                        .setId(0L)
                        .setName("The client provides degradation service, and the server is temporarily shut down. Please try again later.")
                        .setDbSource("not found dbSource"));
                return depts;
            }
        };
    }
}
```



> feign开启回滚工厂

```java
@Component
@FeignClient(value = "SPRINGCLOUD-PROVIDER-DEPT", fallbackFactory = DeptFallbackFactory.class)
public interface DeptFeignService {

    @PostMapping("/add")
    Boolean addDept(Dept dept);

    @GetMapping("/dept/{id}")
    Dept getDeptById(@PathVariable("id") Long id);

    @GetMapping("/dept/all")
    List<Dept> getDepts();

}
```



项目：`springcloud-consumer-dept-feign-80`

> 配置文件

```yaml
# 开启降级服务
feign:
  hystrix:
    enabled: true
```

作用就是，当服务器不可用时，就执行fallbackfactory的方法



<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/降级服务效果.png" style="float:left" />



### 6、Dashboard流监控



#### 1）Dashboard 监控面板服务搭建

> 导入依赖：

spring2.x 的 hystrix-dashboard 最好使用2.2.1.RELEASE版本。其他版本有本人未能解决的问题

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-netflix-hystrix-dashboard</artifactId>
    <version>2.2.1.RELEASE</version>
</dependency>
```



> 配置：

```yaml
server:
  port: 9001
```



> 开启监控面板：

```java
@SpringBootApplication
@EnableHystrixDashboard
public class HystrixDashboardApplication {
    public static void main(String[] args) {
        SpringApplication.run(HystrixDashboardApplication.class,args);
    }
}
```



> 启动程序访问：

localhost:9001/hystrix

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/hystrix监控面板.png" style="float:left" />



#### 2）Dashboard 提供方配置

由于Hystrix的监控需要访问`https://hystrix-app:port/actuator/hystrix.stream`

所以在提供方，需要注册`/actuator/hystrix.stream`的servlet

**被监控的服务，需要导入的依赖：**

```xml
<!--actuator 监控信息-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>

<!--hystrix-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
</dependency>
```



主程序入口：

```java
@SpringBootApplication
@EnableEurekaClient
@EnableCircuitBreaker
public class ProviderDeptHystrixApplication8081 {
    public static void main(String[] args) {
        SpringApplication.run(ProviderDeptHystrixApplication8081.class,args);
    }

    /**
    * 注册servlet，固定写法，实现对该程序的监控
    */
    @Bean
    public ServletRegistrationBean getServlet(){
        HystrixMetricsStreamServlet streamServlet = new HystrixMetricsStreamServlet();
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(streamServlet);
        registrationBean.setLoadOnStartup(1);
        registrationBean.addUrlMappings("/actuator/hystrix.stream");
        registrationBean.setName("HystrixMetricsStreamServlet");
        return registrationBean;
    }
}
```



输入被监控的hystrix程序的url地址：

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/hystrix监控面板01.png" style="float:left" />

进入监控页面：

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/hystrix监控面板02.png" style="float:left" />

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/hystrix监控面板03.png" style="float:left" />

心跳图中的圆球：

- 圆球越来越大，说明此时请求数量很多，压力越来越大
- 圆球的颜色表示健康程度，绿色表示健康，橙色有一定危险，红色表示不健康 





## 八、==Zuul== 路由网关（停更）



### 1、Zuul 介绍

Zuul包含了对请求的路由和过滤两个最主要的功能：

​	其中路由功能负责将外部请求转发到具体的微服务实例上，是实现外部访问统一入口的基础，而过滤器功能则负责对请求的处理过程进行干预，是实现请求校验，服务聚合等功能的基础。Zuul和Eureka进行整合，将Zuul自身注册为Eureka服务治理下的应用，同时从Eureka中获得其他微服务的信息，即以后的访问微服务都通过Zuul跳转后获得。

​	注意：Zuul服务最终还是会注册进Eureka

​	提供：代理+路由+过滤 三大功能



### 2、Zuul 使用

Zuul也要被注册到注册中心。



> 依赖

eureka、zuul

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>

<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-zuul</artifactId>
    <version>1.4.7.RELEASE</version>
</dependency>
```



> 配置

```yaml
server:
  port: 3011

eureka:
  client:
    service-url:
      defaultZone: http://localhost7001:7001/eureka,http://localhost7002:7002/eureka,http://localhost7003:7003/eureka
  instance:
    instance-id: zuul-3011
    prefer-ip-address: true

spring:
  application:
    name: springcloud-zuul

zuul:
  routes:
    # 不配置原始访问路径 http://localhost:3011/springcloud-provider-dept/dept/1
    # 配置了服务名替换规则，就可以通过 http://localhost:3011/mydept/dept/1 访问
    # 将通过微服务名SPRINGCLOUD-PROVIDER-DEPT访问替换为/mydept/**访问
    mydept.serviceId: SPRINGCLOUD-PROVIDER-DEPT #mydept.serviceId随便定义，需要替换路径的服务名
    mydept.path: /mydept/**
    
  ignored-services: "*" # 禁止通过注册到eureka的服务名进行访问。* 是通配符，忽略所有微服务名
  prefix: /hero # 设置请求的公共前缀，即只能通过访问 http://localhost:3011/hero/mydept/dept/1
  
info:
  app.name: hero-springcloud-zuul
```



> 程序入口

```java
@SpringBootApplication
@EnableZuulProxy // 开启Zuul代理
public class ZuulApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication.class,args);
    }
}
```



该配置后，需要通过 zuul的端口号 + 固定公共前缀 + 代表服务名的mydept + 访问的服务的请求url 进行访问：

`http://localhost:3011/hero/mydept/dept/1`



## 九、==SpringCloud Config== 分布式配置中心

**推荐使用阿里巴巴的Nacos**



<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/springcloud-config.png" style="float:left" />



Spring Cloud Config为微服务架构中的微服务提供集中化的外部配置支持，配置服务器为各个不同微服务应用的所有环节提供一个中心化的外部配置。

Spring Cloud Config 分为**服务端**和**客户端**两部分：

​	服务端也称为 分布式配置中心，它是一个独立的微服务应用，用来连接配置服务器并为客户端提供获取配置信息、加密、解密信息等访问接口。

​	客户端则是通过指定的配置中心来管理应用资源，以及与业务相关的配置内容，并在启动的时候从配置中心获取和加载配置信息。配置服务器默认采用git来存储配置西信息，这样就有助于对环境配置进行版本管理。并且可以通过git客户端工具来方便的管理和访问配置内容。



**Spring Cloud Config 的作用就是，统一管理配置文件，每个项目去配置文件中心获取自己的配置就可以了。而维护人员直接在配置文件中心看配置、更改配置即可。方便了维护，对配置也有了统一管理，也解耦了项目本身于配置的关系。**



### 1、SpringCloud Config Server

服务端的作用就是，让所有客户端都通过服务端去获取远程的配置文件

这里用的git远程存放的配置文件。也可以用svn等

> 依赖

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-config-server</artifactId>
</dependency>

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```



> 配置

```yaml
server:
  port: 3344
spring:
  application:
    name: springcloud-config-server
  cloud:
    config:
      server:
      # 从哪个位置获取配置文件
        git:
          uri: https://gitee.com/turbo30/springcloudConfig.git
```



> 程序入口

```java
@SpringBootApplication
@EnableConfigServer // 开启配置服务
public class ConfigServer {
    public static void main(String[] args) {
        SpringApplication.run(ConfigServer.class,args);
    }
}
```

测试：

`http://localhost:3344/master/springcloudConfig-dev.yml`

规则：

`http://host:port/{label}/{application}-{profile}.yml`



### 2、SpringCloud Config Client

服务器都可以去config服务端获取自己对应的配置文件



> 依赖

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <!--actuator 监控信息-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-config-client</artifactId>
    </dependency>
</dependencies>
```



> 配置

bootstarp.yaml

```yaml
# 系统级别的配置文件,最先加载的配置文件
spring:
  cloud:
    config:
      uri: http://localhost:3344 # config服务，连接服务端
      name: config-client # 获取的配置文件名称
      profile: test # 获取配置文件中的对应的角色
      label: master # 分支名
```



application.yaml

```yaml
# 用户级别的配置
spring:
  application:
    name: springcloud-config-client
```



依赖导入了，配置了，主程序直接启动即可

客户端就可以通过访问服务端，获取到自己需要的配置文件了。



### 3、<u>分布式配置的动态刷新问题</u>

当运维在git仓库上更改了配置，config server可以从git上获取最新的配置，但是config client 却不能获取最新的配置，因为config client 服务在启动的时候加载了bootstrap.yml文件，这时候获取的配置已经加载到了本地，就不会被改变，这里就造成了客户端不能及时更新配置，只能重启服务，才能获取配置。



#### 1）==. .==手动版 动态刷新 实现

> 继续 引入依赖 actuator监控

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```



> 添加配置
>
> bootstrap.yml

```yaml
# 暴露监控端点
management:
  endpoints:
    web:
      exposure:
        include: "*"
```



> 在controller类上加注解

```java
@RefreshScope
```



添加了以上的配置之后，运维在git仓库修改了配置文件之后，需要让对应的服务器也更改配置。那么还需要运维人员为对应的服务器发送一个post请求

```
http://host:post/actuator/refresh
```

通知对应的服务器，重新拉去最新的配置，这样就可以完成不重启项目，而获取最新的配置文件。



**由于手动版动态刷新配置，很麻烦，如果服务器有几百台，写脚本刷新也比较麻烦，也比较费时，还很难做到指定服务器刷新，指定某些服务器不刷新。为了解决这个问题，就引入了Spring Cloud Bus消息总线。**



## 十、==SpringCloud Bus== 消息总线

分布式自动刷新配置功能，SpringCloud Bus 配合 Spring Cloud Config使用可以实现配置的动态刷新。

Bus支持两种消息代理：RabbitMQ、Kafka

Bus使用的rabbitmq会在MQ服务器创建topic类型的交换机，并生成临时队列。发送请求:

`http://localhost:3344/actuator/bus-refresh`会自动刷新所有客户端的配置信息。



**刷新方案1：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/bus流程图.png" style="float:left" />

**刷新方案2：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/bus.png" style="float:left" />

通常选择`刷新方案2`，因为方案1是与客户端打交道，客户端在处理自己的业务的同时还要处理额外的IO操作，给客户端增加了压力，并且业务也不纯粹。所以选择方案2，方案2是在配置服务进行刷新，然后通过消息总线通知所有客户端去更新配置。



### 1、config 服务端

> **config 服务端**
>
> cloud-config-server-3344

依赖：

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```



增加的配置

```yaml
spring:
  application:
    name: config-server
  # rabbitmq配置
  rabbitmq:
    host: 127.0.0.1
    port: 5672
    username: guest
    password: guest1130
 
# 暴露端点，刷新端点
management:
  endpoints:
    web:
      exposure:
        include: "bus-refresh"
```



### 2、config 客户端

> **config 客户端**
>
> 在config 手动版的基础上增加rabbitmq的配置
>
> cloud-config-client-3355

依赖：

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```



增加的配置：

```yaml
spring:
  application:
    name: config-client
  rabbitmq:
    host: 127.0.0.1
    port: 5672
    username: guest
    password: guest1130
```



### 3、效果

RabbitMQ服务生成了交换机和消息队列

**exchange：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/bus交换机.png" style="float:left" />

**queue：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/bus消息队列.png" style="float:left" />



**给配置服务端发送post** `http://localhost:3344/actuator/bus-refresh`，广播刷新配置：

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/post请求刷新所有客户端的配置.png" style="float:left" />



### 4、指定服务器更新配置

发送post请求`http://localhost:3344/actuator/bus-refresh/{destination}`

加入只通知微服务名(spring.application.name)为：config-client 端口号为：3355 更新配置

发送 `http://localhost:3344/actuator/bus-refresh/config-client:3355`就可以指定这个服务器更新配置了。







## 常见问题

1、什么是微服务

2、微服务之间如何独立通信？

3、SpringCloud和Dubbo有哪些区别？

4、SpringBoot和SpringCloud，请谈谈对他们的理解？

5、什么是服务熔断？什么是服务降级？

6、微服务的优缺点分别是什么？说一下在项目中遇到的坑

优点：

松耦合，聚焦单一业务功能，无关开发语言，团队规模降低。在开发中，不需要了解多有业务，
只专注于当前功能，便利集中，功能小而精。微服务一个功能受损，对其他功能影响并不是太大，可以快速定位问题。
微服务只专注于当前业务逻辑代码，不会和 html、css 或其他界面进行混合。可以灵活搭配技术，独立性比较舒服。

缺点：

随着服务数量增加，管理复杂，部署复杂，服务器需要增多，服务通信和调用压力增大，运维工程师压力增大，
人力资源增多，系统依赖增强，数据一致性，性能监控。

7、你知道的微服务技术栈有哪些？

8、eureka和zookeeper都可提供服务注册与发现功能，请说两个区别？

