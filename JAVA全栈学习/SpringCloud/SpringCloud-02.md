# Spring Cloud - 02

> 课程视频：
>
> https://www.bilibili.com/video/BV1yE411x7Ky/?spm_id_from=333.788.recommend_more_video.16



由于NetFlix的停更，停止维护。而使得SpringCloud的技术大幅度升级。

以下是广泛使用的技术栈：

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/cloud升级.png" style="float:left" />



## 一、自动部署Devtools 设置 (开发阶段)

> 导入依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <scope>runtime</scope>
    <optional>true</optional>
</dependency>
```

父工程pom

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <configuration>
                <fork>true</fork>
                <addResources>true</addResources>
            </configuration>
        </plugin>
    </plugins>
</build>
```



> idea设置

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/idea设置自动部署01.png" style="float:left" />



`ctrl+shift+alt+/` 点击register

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/idea设置自动部署02.png" style="float:left" />

勾选上后，重启idea



## 二、==Consul== 服务注册与发现



### 1、Consul 简介

Consul是一个服务管理软件。支持多数据中心下，分布式高可用的，服务发现和配置共享。采用 Raft 算法,用来保证服务的高可用。



### 2、Consul 安装

官网下在Consul压缩包，解压之后，会得到一个exe执行文件

在exe执行文件目录下，打开cmd

```bash
# 查看版本号
consul --version

# 启动consul服务
consul agent -dev
```

访问地址，默认端口号8500，此时就已经启动了服务：

http://localhost:8500/



### 3、Consul 配置

```yaml
spring:
  application:
    name: provider-payment
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
        # 显示ip地址
        prefer-ip-address: true
```



## 三、四大注册中心异同点

CAP关注的是数据的粒度，而不是整体的设计。

| 组件名    | 语言 | CAP    | 服务健康检查 | 对外暴露接口 | Spring Cloud集成 |
| --------- | ---- | ------ | ------------ | ------------ | ---------------- |
| Eureka    | Java | AP     | 可配支持     | HTTP         | 已集成           |
| Zookeeper | Java | CP     | 支持         | 客户端       | 已集成           |
| Consul    | GO   | CP     | 支持         | HTTP/DNS     | 已集成           |
| Nacos     | Java | AP、CP | 支持         | HTTP         | 已集成           |



## 四、==OpenFeign==

对Feign的增强 封装了Ribbon+RestTemplate

### 1、OpenFeign 使用

项目：

cloud-server-eureka-7001

cloud-consumer-order-openfeign-80

cloud-provider-payment-8001



#### 1）依赖

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
```



#### 2）写接口 + @FeignClient

这个接口指定了连接的服务器，以及服务器对应的请求和方法(服务器的controller层)

```java
@Service
@FeignClient(value = "provider-payment")
// FeignClient 注解 指定连接哪个服务器。因为内置ribbon依赖，所以默认使用轮询负载均衡
public interface OrderOpenFeignService {

    @GetMapping(value = "/get/{id}")
    CommonResult<Payment> getPaymentById(@PathVariable("id") Long id);

    @GetMapping(value = "/timeout")
    String getTimeout();
}
```



#### 3）controller层

controller层直接调用接口即可。

```java
@RestController
public class OrderOpenFeignController {

    @Autowired
    private OrderOpenFeignService orderOpenFeignService;

    @GetMapping(value = "/consumer/feign/{id}")
    public CommonResult getInfo(@PathVariable("id") Long id){
        CommonResult<Payment> paymentById = orderOpenFeignService.getPaymentById(id);
        return paymentById;
    }

    @GetMapping(value = "/consumer/timeout")
    public String getTimeout(){
        return orderOpenFeignService.getTimeout();
    }
}
```



#### 4）主方法 @EnableFeignClients

```java
@SpringBootApplication
@EnableFeignClients // 开启使用feign所有客户端
public class ConsumerOrderOpenFeign80 {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerOrderOpenFeign80.class,args);
    }
}
```



### 2、超时控制

由于openfiegn默认连接服务器以及响应时间为1s。超过该时间就会抛出超时异常。有些时候项目容忍超过1s的响应，所以可以更改配置改变默认配置。



在项目`cloud-provider-payment-8001`中写了一个超时请求。

```java
@GetMapping(value = "/timeout")
public String getTimeout(){
    try{
        TimeUnit.SECONDS.sleep(3);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    return serverPort;
}
```

访问后：

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/openfeign超时控制.png" style="float:left" />



**解决办法**

在项目`cloud-consumer-order-openfeign-80`增加ribbon配置：

```yaml
ribbon:
  # 建立连接时间
  ReadTimeout: 5000
  # 建立连接后，读取可用资源时间
  ConnectTimeout: 5000
```

即可解决问题



### 3、日志增强

openfeign提供日志打印，可以查看到详细的日志信息。



> 注入日志bean

```java
@Configuration
public class ApplicationContextBeanConfig {

    /**
     * feign日志注入ioc容器
     * @return
     */
    @Bean
    public Logger.Level loggerLevel(){
        return Logger.Level.FULL;
    }
}
```

日志级别：

- **NONE**  默认的，不显示日志
- **BASIC**  仅记录请求方法、URL、响应状态码及执行时间
- **HEADERS**  除了BASIC中定义的信息之外，还有请求和响应的头信息
- **FULL**  除了HEADERS中定义的信息之外，还有请求和响应的正文及元数据



> 配置

```yaml
logging:
  level:
    # 日志的级别以及监控的接口
    com.heroc.service.OrderOpenFeignService: debug
```



打印结果：

```
2020-08-10 14:24:11.293 DEBUG 16128 --- [p-nio-80-exec-5] com.heroc.service.OrderOpenFeignService  : [OrderOpenFeignService#getPaymentById] <--- HTTP/1.1 200 (51ms)
2020-08-10 14:24:11.293 DEBUG 16128 --- [p-nio-80-exec-5] com.heroc.service.OrderOpenFeignService  : [OrderOpenFeignService#getPaymentById] connection: keep-alive
2020-08-10 14:24:11.293 DEBUG 16128 --- [p-nio-80-exec-5] com.heroc.service.OrderOpenFeignService  : [OrderOpenFeignService#getPaymentById] content-type: application/json
2020-08-10 14:24:11.293 DEBUG 16128 --- [p-nio-80-exec-5] com.heroc.service.OrderOpenFeignService  : [OrderOpenFeignService#getPaymentById] date: Mon, 10 Aug 2020 06:24:11 GMT
2020-08-10 14:24:11.293 DEBUG 16128 --- [p-nio-80-exec-5] com.heroc.service.OrderOpenFeignService  : [OrderOpenFeignService#getPaymentById] keep-alive: timeout=60
2020-08-10 14:24:11.293 DEBUG 16128 --- [p-nio-80-exec-5] com.heroc.service.OrderOpenFeignService  : [OrderOpenFeignService#getPaymentById] transfer-encoding: chunked
2020-08-10 14:24:11.294 DEBUG 16128 --- [p-nio-80-exec-5] com.heroc.service.OrderOpenFeignService  : [OrderOpenFeignService#getPaymentById] 
2020-08-10 14:24:11.294 DEBUG 16128 --- [p-nio-80-exec-5] com.heroc.service.OrderOpenFeignService  : [OrderOpenFeignService#getPaymentById] {"code":200,"msg":"添加数据成功,服务端口号:8001","data":{"id":31,"serial":"aabbbaa31"}}
2020-08-10 14:24:11.294 DEBUG 16128 --- [p-nio-80-exec-5] com.heroc.service.OrderOpenFeignService  : [OrderOpenFeignService#getPaymentById] <--- END HTTP (98-byte body)
```



## 五、==Hystrix 补充==

项目：

cloud-provider-payment-hystrix-8001

cloud-server-eureka-7001



服务降级，一般放在客户端。更具业务而定。



### 1、超时 服务降级

cloud-provider-payment-hystrix-8001

```java
@Service
public class PaymentHystrixService {

    public String hystrix_OK(Long id){
        return Thread.currentThread().getName()+" id:" + id;
    }

    @HystrixCommand(fallbackMethod = "hystrix_TimeoutHandler", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000")
    })
    // fallbackMethod 表示服务降级需要执行的方法
    // commandProperties 表示其他配置 相关属性见类：HystrixCommandProperties 
    // 			@HystrixProperty 配置的表示3秒还没响应就熔断，执行fallbackMethod
    public String hystrix_Timeout(Long id){
        int time = 5;
        try{
            TimeUnit.SECONDS.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Thread.currentThread().getName()+" id:"+id + " time(s):"+time;
    }

    public String hystrix_TimeoutHandler(Long id){
        return Thread.currentThread().getName()+" 系统繁忙，请稍后再试!";
    }
}
```



主程序开启的注解：

```java
@EnableCircuitBreaker
```



### 2、消费方配置

```yaml
# 开启降级服务
feign:
  hystrix:
    enabled: true
```



主启动类：

```java
@EnableHystrix
```



### 3、默认全局 服务降级方法

cloud-consumer-order-openfeign-hystrix-80

当请求中，执行的语句报错，并且加了@HystrixCommand注解，就会去找全局配置的默认服务降级方法，就会执行服务降级处理：

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/默认服务降级方法.png" style="float:left" />



### 4、服务降级与业务逻辑 解耦

cloud-consumer-order-openfeign-hystrix-80



<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/服务降级处理解耦.png" style="float:left" />



实现feign的接口，实现的逻辑为服务降级的执行方法。然后在注解@FeignClient中加入fallback属性，指定到执行的类，就会执行对应的服务降级处理。这样就实现了服务降级与业务逻辑层的解耦。就不用再controller层写大量的fallback的方法。由feign支持。



### 5、服务熔断 机制

报错--->服务降级--->服务恢复



cloud-provider-payment-hystrix-8001



> service层

HystrixProperty中的参数通过`HystrixCommandProperties`得知

```java
@HystrixCommand(fallbackMethod = "paymentCircuitBreaker_fallback", commandProperties = {
    // 开启断路器
    @HystrixProperty(name = "circuitBreaker.enabled", value = "true"), 
    // 请求次数
    @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"), 
    // 窗口期时间毫秒
    @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"),
    // 请求次数失败率
    @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60") 
})
public String paymentCircuitBreaker(Long id){
    if (id <= 0){
        throw new RuntimeException("id 不能为负数");
    }
    String uuid = IdUtil.simpleUUID();
    return Thread.currentThread().getName()+"  "+uuid;
}
public String paymentCircuitBreaker_fallback(Long id){
    return "id 不能为负数 ---- " + id;
}
```

**注解意思是，在最近10s的窗口期内，10次访问中错误率达到60%以上，保险丝就断开，熔断机制就开启，无论是否是正确访问，都会走服务降级处理。在最近10s的窗口期内，降到60%，就关闭熔断机制，正确访问就恢复到原来的逻辑处理。**



> controller 层

```java
@GetMapping(value = "/breaker/{id}")
public String breaker(@PathVariable("id") Long id){
    return paymentHystrixService.paymentCircuitBreaker(id);
}
```

controller层直接调用就是了。



### 6、服务限流 

见 alibaba  sentinel



## 六、==Gateway== 网关

​	是Spring Cloud的全新项目，**基于spring5.0+、springBoot2.0+ 、 Spring Boot webFlux和 Project Reactor等技术开发的网关**，它旨在为微服务框架提供一种简单有效的统一的API路由管理方式。

​	SpringCloud Gateway作为SpringCloud生态系统中的网关，目标是替代Zuul，在SpringCloud 2.0以上版本中，没有对新版本Zuul2.0以上最新高性能版本进行集成，仍然还是使用的Zuul1.x非Reactor模式的老版本。为了提升网关的性能，**SpringCloud Gateway基于WebFlux框架实现的，而WebFlux框架底层则使用了高性能的Reactor模式通信框架Netty**。

​	SpringCloud Gateway的目标提供统一的路由方式且基于Filter链的方式提供了网关基本的功能，例如：安全、监控/指标、限流等。



### 1、Gateway 非阻塞异步模型

Zuul1.x的技术是I/O阻塞的，在性能上较差。而Zuul2.x的新技术是在Zuul1.x上进行了改变，采用了非阻塞模型，但是springcloud没有对zuul2.x进行集成，并且zuul2.x还是个半成品。springcloud的自己研发了gateway项目，而gateway采用的是非阻塞异步模型，基于webflux技术，以及整合了spingboot和reactor，又是自研产品，自身稳固性较高，性能也较高，所以大部分采用的是gateway。



### 2、Gateway 工作流程

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/gateway工作流程.png" style="float:left" />



#### 1）Route 路由

路由是构建网关的基本模块，它由ID，目标URI，一系列的断言和过滤器组成，如果断言为true则匹配该路由。



#### 2）Predicate 断言

参考的是JAVA8的java.util.function.Predicate

开发人员可以匹配HTTP请求中的所有内容(例如请求头或请求参数)，如果请求与断言相匹配则进行路由



#### 3）Filter 过滤

指的是Spring框架中GatewayFilter的实例，使用过滤器，可以在请求被路由前或者之后对请求进行修改。



### 3、配置方式 设置网关

> 依赖

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-gateway</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-devtools</artifactId>
        <scope>runtime</scope>
        <optional>true</optional>
    </dependency>

    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>
</dependencies>
```



> 配置文件
>
> predicates可以断言很多属性，比如cookie、header、method、path等.....
>
> 官方文档`https://docs.spring.io/spring-cloud-gateway/docs/2.2.4.RELEASE/reference/html/`

```yaml
server:
  port: 9527

spring:
  application:
    name: gateway
  cloud:
  # 网关配置
    gateway:
      routes:
        - id: provider-payment-8001-01 # id 唯一
          uri: http://localhost:8001 # 访问提供方服务的地址
          predicates:
            - Path=/get/** # 断言路径一致，就去访问提供方服务对应的路径

        - id: provider-payment-8001-02
          uri: http://localhost:8001
          predicates:
            - Path=/serverport
eureka:
  instance:
    instance-id: gateway-9527
  client:
    service-url:
      defaultZone: http://localhost7001.com:7001/eureka
```

通过访问`http://localhost:9527/payment/discovery`就可以访问到`http://localhost:8001/payment/discovery` 这样只需要向外暴露9527端口就可以了。



> 主程序入口

```java
@SpringBootApplication
@EnableDiscoveryClient
@EnableEurekaClient
public class Gateway9527 {
    public static void main(String[] args) {
        SpringApplication.run(Gateway9527.class,args);
    }
}
```



### 4、代码方式 设置网关

依赖，主程序代码一样，在config包下创建一个GatewayConfig配置类：

```java
@Configuration
public class GatewayConfig {

    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder routeLocatorBuilder){
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
        // 链式编程：
        // route(id,function函数式接口)
        routes.
            route("path_route_heroc_id",
                  r->r.path("/guonei").uri("http://news.baidu.com/guonei"))
            .route("path_route_heroc_id01",
                   r->r.path("/guoji").uri("http://news.baidu.com/guoji"))
            .build();
        return routes.build();
    }
}
```

访问：`http://localhost:9527/guonei` 即可跳转到 `http://news.baidu.com/guonei`



### 5、动态路由配置 常用

将网关注册到注册中心，去注册中心找到微服务，然后通过负载均衡访问服务器 (服务器集群，防止一个服务崩塌，而导致不可用)

```yaml
server:
  port: 9527

spring:
  application:
    name: gateway
  cloud:
    gateway:
      discovery:
        locator:
          enabled: true # 开启从注册中心动态创建路由功能，利用微服务名进行路由
      routes:
        - id: provider-payment-8001-01 # id 唯一
          # uri: http://localhost:8001 # 访问提供方服务的地址
          uri: lb://provider-payment # 通过lb负载均衡方式，访问注册中心的微服务名进行路由访问
          predicates:
            - Path=/get/** # 断言路径一致，就去访问提供方服务对应的路径

        - id: provider-payment-8001-02
          uri: lb://provider-payment
          predicates:
            - Path=/serverport

eureka:
  instance:
    instance-id: gateway-9527
  client:
    service-url:
      defaultZone: http://localhost7001.com:7001/eureka
```



### 6、Gateway 常用 Predicate断言

[Predicate 断言 官方文档](https://docs.spring.io/spring-cloud-gateway/docs/2.2.4.RELEASE/reference/html/#gateway-request-predicates-factories)

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/gateway断言.png" style="float:left" />



#### 1）After 举一反三 Before Between

```yaml
spring:
  cloud:
    gateway:
      routes:
      - id: after_route
        uri: https://example.org
        predicates:
        - After=2017-01-20T17:42:47.789-07:00[America/Denver]
```

意思是这个请求在Jan 20, 2017 17:42之后才能被访问。

**时间生成**：

```java
System.out.println(ZonedDateTime.now());
```

**结果**：

```
2020-08-11T13:00:28.320+08:00[Asia/Shanghai]
```



#### 2）Cookie

```yaml
spring:
  cloud:
    gateway:
      routes:
      - id: cookie_route
        uri: https://example.org
        predicates:
        - Cookie=chocolate, ch.p # 第一个参数是cookie名，第二个参数是正则表达式或具体的值
```

满足要求才可以被访问



#### 3）Header

```yaml
spring:
  cloud:
    gateway:
      routes:
      - id: header_route
        uri: https://example.org
        predicates:
        - Header=X-Request-Id, \d+ # 第一个参数是header名，第二个参数是正则表达式或具体的值
```

满足要求才可以被访问



#### 4）Host

```yaml
spring:
  cloud:
    gateway:
      routes:
      - id: host_route
        uri: https://example.org
        predicates:
        - Host=**.somehost.org,**.anotherhost.org
```



#### 5）Method 、 Path

```yaml
spring:
  cloud:
    gateway:
      routes:
      - id: method_route
        uri: https://example.org
        predicates:
        - Method=GET,POST
```



```yaml
spring:
  cloud:
    gateway:
      routes:
      - id: path_route
        uri: https://example.org
        predicates:
        - Path=/red/{segment},/blue/{segment}
```



### 7、Gateway 的 Filter

[Filter 官方文档](https://docs.spring.io/spring-cloud-gateway/docs/2.2.4.RELEASE/reference/html/#gatewayfilter-factories)



#### 1）自定义Filter

写一个过滤器类，实现`GlobalFilter, Ordered`接口

```java
@Component
@Slf4j
public class HeroGatewayGlobalFilter implements GlobalFilter, Ordered {

    /**
     * 过滤方法
     * @param exchange 可以获取请求、响应等
     * @param chain 放行链
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("************ come in HeroGatewayGlobalFilter");
        // 从请求路径中获取参数username
        String username = exchange.getRequest().getQueryParams().getFirst("username");
        if (StringUtils.isEmpty(username) || username == null){
            log.info("************ 用户名非法");
            // 拦截，返回状态码不被接受
            exchange.getResponse().setStatusCode(HttpStatus.NOT_ACCEPTABLE);
            return exchange.getResponse().setComplete();
        }
        log.info("************ 用户名：" + username);
        // 放行
        return chain.filter(exchange);
    }

    /**
     * 过滤器的优先级，值越小优先级越高
     * @return
     */
    @Override
    public int getOrder() {
        return 0;
    }
}
```

加了该过滤器规则，如下格式 (请求中加入了username)：

`http://localhost:9527/get/30?username=heroc`

才能被访问



### 8、解决跨域配置

```java
@Configuration
public class HerocCorsConfig {

    /**
     * 解决前端跨域访问后端的问题
     * @return
     */
    @Bean
    public CorsWebFilter corsWebFilter(){
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        // 允许所有头请求
        corsConfiguration.addAllowedHeader("*");
        // 允许所有请求方法
        corsConfiguration.addAllowedMethod("*");
        // 允许所有来源
        corsConfiguration.addAllowedOrigin("*");
        // 允许cookie
        corsConfiguration.setAllowCredentials(true);
        // 所有请求接口，都要经过此过滤配置
        source.registerCorsConfiguration("/**",corsConfiguration);
        return new CorsWebFilter(source);
    }
}
```





## 七、==Stream== 消息驱动微服务框架

屏蔽底层消息中间件的差异，降低切换成本，统一消息的编程模式

> https://spring.io/projects/spring-cloud-stream
>
> Spring Cloud Stream is a framework for building highly scalable event-driven microservices connected with shared messaging systems.  --- 摘自官网



### 1、Spring cloud Stream 设计思想

在项目开发中，可能一个项目中会使用多个消息中间件，为了减轻程序员在工作的同时有去学习新的技术，那么就引入了`spring cloud stream`，程序员只需要学习`spring cloud stream`，就可以在多个消息中间件之间来回切换使用了。它目前支持了`rabbitmq`和`kafka`。



在项目开发中，可能一个项目中会使用多个消息中间件，然而消息中间件都有自己的一套架构和规则，如果消息之间要互通无疑是一个很困难的操作，且耦合度也会很高，因此`spring cloud stream`给我们提供了一种解耦合的方式。`通过定义绑定Binder作为中间层，实现了应用程序与消息中间件细节之间的隔离`。



**spring cloud stream 遵循发布订阅模式，即Topic类型**



**处理架构：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/stream处理架构.png" style="float:left" />



### 2、Stream 编码 常用注解

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/stream官方架构图.png" style="float: left; zoom: 60%;" />

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/stream流程套路.png" style="float: left; zoom: 67%;" />

| 组成           | 说明                                                         |
| -------------- | ------------------------------------------------------------ |
| Middleware     | 中间件，目前只支持RabbitMQ和Kafka                            |
| Binder         | Binder是应用与消息中间件之间的封装，目前实行了Kafka和RabbitMQ的Binder，通过Binder可以很方便的连接中间件，可以动态的改变消息类型（对应于Kafka的topic，RabbitMQ的exchange），这些都可以通过配置文件实现 |
| @Input         | 注解标识输入通道，通过该输入通道接收到的消息进入应用程序     |
| @Output        | 注解标识输出通道，发布的消息将通过该通道离开应用程序         |
| @StreamListner | 监听队列，用于消费者的队列的消息接收                         |
| @EnableBinding | 指信道Channel和exchange绑定在一起                            |



### 基于RabbitMQ



### 3、消息驱动 之 生产者

cloud-stream-rabbitmq-provider-8801



> 引入依赖

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-stream-rabbit</artifactId>
</dependency>
```



> 配置

```yaml
server:
  port: 8801

spring:
  application:
    name: provider-stream
  rabbitmq:
    host: 127.0.0.1
    port: 5672
    username: guest
    password: guest1130
  cloud:
    stream:
      binders: # 绑定多个中间件
        defaultRabbit: # 定义的名字，用于bindings整合
          type: rabbit # 消息组件类型 rabbitmq
      bindings: # 服务整合处理 集合，元素需要加 -
         output: # 一个通道的名称
            destination: studyExchange # 交换机名称
            content-type: application/json # 设置消息类型，文本是text/plain
            binder: defaultRabbit # 设置绑定的消息服务具体设置（可能报红，只要与binders下的中间件名称一样即可）

eureka:
  instance:
    instance-id: provider-stream-8801
    prefer-ip-address: true
    lease-renewal-interval-in-seconds: 1
    lease-expiration-duration-in-seconds: 2
  client:
    service-url:
      defaultZone: http://localhost7001.com:7001/eureka
```



> service层

```java
public interface MsgProviderService {
    String send();
}
```



```java
@Slf4j
// 开启绑定通道 source为发布
@EnableBinding(Source.class)
public class MsgProviderServiceImpl implements MsgProviderService {

    // 注入消息通道
    @Autowired
    private MessageChannel output;

    @Override
    public String send() {
        String msg = UUID.randomUUID().toString();
        // 通过消息通道发送消息
        output.send(MessageBuilder.withPayload(msg).build());
        log.info("********* " + msg);
        return null;
    }
}
```



> controller层 生产者

```java
@RestController
public class MsgProviderController {

    @Autowired
    private MsgProviderService msgProviderService;

    @GetMapping(value = "/send")
    public String sendMsg(){
        return msgProviderService.send();
    }

}
```



### 4、消息驱动 之 消费者 手动ack

[消费者 相关配置参数](https://cloud.spring.io/spring-cloud-static/spring-cloud-stream-binder-rabbit/3.0.6.RELEASE/reference/html/spring-cloud-stream-binder-rabbit.html#_rabbitmq_consumer_properties)

cloud-stream-rabbitmq-consumer-8802



> 依赖

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-stream-rabbit</artifactId>
</dependency>
```



> 配置

```yaml
server:
  port: 8802

spring:
  application:
    name: consumer-stream
  rabbitmq:
    host: 127.0.0.1
    password: guest1130
    username: guest
    port: 5672
  cloud:
    stream:
      binders:
        defaultRabbit:
          type: rabbit
      bindings:
        input:
          destination: studyExchange # 交换机名字要一样
          group: myQueue # 指定队列名称 默认持久化，最终队列名称为: <destination>.<group>
          				 # 不配置group 则是临时队列
          content-type: application/json
          binder: defaultRabbit
      rabbit:
        bindings:
          input:
            consumer:
              maxLength: 2 # 队列中最大信息数
              acknowledgeMode: manual # 手动ack
              # bindingRoutingKey: route #   绑定路由，默认值‘#’ 
              # ttl: 10000 # 设置过期时间



eureka:
  instance:
    instance-id: consumer-stream
    prefer-ip-address: true
    lease-renewal-interval-in-seconds: 1
    lease-expiration-duration-in-seconds: 2
  client:
    service-url:
      defaultZone: http://localhost7001.com:7001/eureka

```



> controller层 消费者

```java
@Component
@EnableBinding(Sink.class)
public class MsgConsumerController {

    @Value("${server.port}")
    private String serverPort;

    @StreamListener(Sink.INPUT)
    public void getMsg(Message<String> message,
                       @Header(AmqpHeaders.CHANNEL) Channel channel,
                       @Header(AmqpHeaders.DELIVERY_TAG) Long deliveryTag) throws IOException {
        System.out.println("接收到消息 ---> " + message.getPayload() + " port: " + serverPort);
        // 手动ack
        channel.basicAck(deliveryTag,false);
    }

}
```



### 5、分组消费与持久化



#### 1）分组消费 解决重复消费

如果配置group属性的话，多个消费者，就默认会分配成不同组，生产者通过交换机发送消息，这些消费者都会接收到这个消息，那么就产生了重复消费的情况。如果指定了group属性，多个消费者都在同一个组，那么生产者发送消息，这些消费者中只有一个消费者会获取到这个消息进行消费。

```yaml
spring:
  application:
    name: consumer-stream
  rabbitmq:
    host: 127.0.0.1
    password: guest1130
    username: guest
    port: 5672
  cloud:
    stream:
      binders:
        defaultRabbit:
          type: rabbit
      bindings:
        input:
          destination: studyExchange 
          group: myQueue # 指定队列名称 默认持久化，最终队列名称为: <destination>.<group>
          				 # 不配置group 则是临时队列
```



#### 2）持久化 解决MQ方消息丢失

设置了group之后，就会生成一个持久化的队列。即使宕机了，生产者不停的发送数据，这个消息队列中的数据会保存下来，再次启动该服务器，因为group对应了一个队列。所以依旧会拿到数据。



## 八、==Sleuth== 请求链路跟踪

Spring Cloud Sleuth 提供了一套完整的服务跟踪的解决方案。在分布式中提供了跟踪解决方案并且兼容支持zipkin

Spring Cloud Sleuth负责收集整理数据，zipkin负责视图展现。



### 1、Zipkin 搭建与理解

> zipkin-server 下载地址：
>
> https://dl.bintray.com/openzipkin/maven/io/zipkin/java/zipkin-server/
>
> 运行：zipkin-server-2.12.9-exec.jar 即可 默认端口9411



一条链路通过Trace Id唯一标识，Span标识发起的请求信息，各Span通过Trace Id关联起来。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/sleuth.png" style="float: left; " />

### 2、Sleuth 和 Zipkin 使用

只需要给服务器加入以下配置，就可以实现链路跟踪监控

> 导入依赖
>
> 该依赖包包含了sleuth和zipkin

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-zipkin</artifactId>
</dependency>
```



> 配置

```yaml
spring:  
  zipkin:
    # zipkin 可视图地址
    base-url: http://localhost:9411
  sleuth:
    sampler:
      # 采样率值介于0~1之间，1表示全部采样
      probability: 1
```



本案例跟踪了以下服务：

cloud-consumer-order-80

cloud-provider-payment-8001

启动：cloud-server-eureka-7001  cloud-consumer-order-80  cloud-provider-payment-8001

结果：

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringCloud/zipkin.png" style="float: left; " />

可以观察服务器之间的调用情况，以及反应速度





















