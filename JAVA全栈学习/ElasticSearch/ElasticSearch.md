# ElasticSearch

Elasticsearch是一个基于Lucene的搜索服务器，对Lucene做了一些封装与增强。

Lucene是一套信息检索工具包，不包含搜索引擎，包含了索引结构、排序功能、搜索规则等。

Lucene由Doug Cutting创始。

<u>大数据三件套：ELK</u> (Elasticsearch、Logstash、Kibana)



## 一、简介

### 1、概述

Elasticsearch 是一个**分布式**、**高扩展**、**高实时的搜索**与**数据分析引擎**。它能很方便的使大量数据具有搜索、分析和探索的能力。充分利用Elasticsearch的水平伸缩性，能使数据在生产环境变得更有价值。Elasticsearch 的实现原理主要分为以下几个步骤，首先用户将数据提交到Elasticsearch 数据库中，再通过分词控制器去将对应的语句分词，将其权重和分词结果一并存入数据，当用户搜索数据时候，再根据权重将结果排名，打分，再将返回结果呈现给用户。

用于==全文搜索==、==结构化搜索==、==分析==以及==将这三者混合使用==。

Elasticsearch基于Java开发。



还有一个搜索引擎，solr



## 二、ElasticSearch安装

JDK1.8最低要求

https://www.elastic.co/cn/

下载压缩包，解压即可用。

**==注意：==不能安装在带空格的目录下，这样会导致以后安装的有些插件不能被识别，或者是不能正常运行！！！**



### 1、目录

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/目录.png" style="float:left" />

**config：**

elasticsearch.yml --- es的相关配置 (默认端口号为9200)

jvm.options --- jvm的相关配置 (由于es默认启动需要用1g内存，如内存有限可更改大小-Xms1g -Xmx1g)

log4j2.properties --- 日志相关的配置



### 2、启动

在bin文件夹中双击`elasticssearch.bat`即可启动，

通过`localhost:9200`即可访问es，es会返回一个json数据

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/启动.png" style="float:left" />



### 3、安装可视界面head，解决跨域

>  需要node.js知识，vue知识

可视界面可以查看es中有多少个索引，集群数量信息等。

- 下载elasticsearch-head

  https://github.com/mobz/elasticsearch-head/

- 解压之后，这是一个vue项目，在该压缩包文件下，cmd窗口执行`npm install`，安装好需要的依赖包

- 然后，执行`npm run start`即可，然后访问`http://localhost:9100`即可

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/可视化.png" style="float:left" />

由于elasticsearch默认不支持跨域访问，所以需要在elasticsearch.yml文件中文末加入以下配置：

```yml
http.cors.enabled: true
http.cors.allow-origin: "*" 
# 允许任何地址访问
```

重启，连接，即可。



> 这个head，不建议使用查询，推荐使用Kibana进行数据查询操作。



### 3、9200端口与9300端口

9200端口：是通过http协议发送的请求，来操作es

9300端口：是通过tcp协议发送的请求，来操作es。并且9300端口来操作es已经不建议使用



## 三、Kibana安装

Kibana是开源的分析和可视化平台，用于搜索、查看交互存储在elasticsearch索引中的数据。使用Kibanan可以通过各种图标进行高级数据分析及展示。

下载地址：

https://www.elastic.co/cn/downloads/kibana

下载之后，直接解压即可：

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/kibana.png" style="float:left" />

解压之后，进入bin目录，点击`kibana.bat`即可启动kibana，启动后访问：`http://localhost:5601`

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/kibana页面.png" style="float:left" />



**汉化设置：**

在config目录下，编辑`kibana.yml`配置文件，文末加入：

```yml
i18n.locale: "zh-CN"
```



## 四、ES核心

| ElasticSearch |
| ------------- |
| 索引(indices) |
| types         |
| documents     |
| fields        |

elasticsearch(集群)中，可包含多个索引，索引中可包含多个类型，类型中可包含多个文档，每个文档中又可包含多个字段。

elasticsearch后台将每个索引划分成多个分片，每个分片可在集群中的不同服务器间迁移。

#### 什么是分片？

简单来讲就是咱们在ES中所有数据的文件块，也是**数据的最小单元块**，整个ES集群的核心就是对所有分片的分布、索引、负载、路由等达到惊人的速度和主流关系型数据库的表分区的概念有点类似。

> 实列场景：
>
> 假设 IndexA 有2个分片，我们向 IndexA 中插入10条数据 (10个文档)，那么这10条数据会尽可能平均的分为5条存储在第一个分片，剩下的5条会存储在另一个分片中。



#### 倒排索引

elasticsearch基于lucene的，而lucene使用的索引就是倒排索引，因此elasticsearch就是使用的倒排索引。

既然有倒排索引，那么就有正向索引。

**正向索引**，在mysql中经常使用，它是通过ID去找value的过程。正向索引会走每一条数据记录。

**倒排索引**，就是通过关键字去找到数据的ID，然后再通过ID返回这整条数据的过程。倒排索引，只要有查询的关键字的记录，就会通过关键字直接连接到ID，其余的ID都不会被查询。



<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/倒排索引.png" style="float:left" />

比如上图，经过倒排索引，检出了python和linux两个关键字标签，当搜索linux的时候，就会指定到ID为3和4的文章，其余的文章就不会被查询。如果是正向索引，当搜索linux的时候，会从ID为1开始，每一篇文章中去查询是否有linux这个关键字。因此倒排索引的效率是非常快的，减少了不必要的查询，大大提高了搜索时间。



#### 权重

搜索的词在内容中匹配度越高，那么这个内容的权重就越高，显示就越前面。当然也可以使用某种手段，可以置顶一些内容。比如百度，只要你给广告费，相关搜索时，你的内容就会被置顶显示。



## 五、IK分词器

IK分词器下载，IK分词器对中文比较友好。

IK提供了两种分词算法：`ik_smart`和`ik_max_word`，其中ik_smart为最少切分，ik_max_word为最细粒度划分



### 1、IK安装

https://github.com/medcl/elasticsearch-analysis-ik/releases

==注意：==ik的版本好一定要与elasticsearch的版本号一致！！我用的7.6.1的版本。

下载好之后，创建名为`elasticsearch-analysis-ik-7.6.1`的文件夹(名字可自取)，将其解压到`elasticsearch-analysis-ik-7.6.1`文件夹中，将这个`elasticsearch-analysis-ik-7.6.1`文件夹，移到elasticsearch目录下的plugins目录下即可。

重启就会加载这个插件了。

可在elasticsearch的bin目录下cmd窗口，执行`elasticsearch-plugin list`即可查看插件是否被加载。

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/查看插件.png" style="float:left" />



### 2、IK测试

启动Kibana，进行测试

IK提供了两种分词算法：`ik_smart`和`ik_max_word`，其中ik_smart为最少切分，ik_max_word为最细粒度划分

```json
GET _analyze
{
  "analyzer": "ik_smart",
  "text": "中国共产主义青年团"
}

GET _analyze
{
  "analyzer": "ik_max_word",
  "text": "中国共产主义青年团"
}
```

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/ik测试.png" style="float:left" />



### 3、自定义字典 本地词库

在elasticsearch-analysis-ik目录下的config目录下有一个IKAnalyzer.cfg.xml文件，可配置自定义的字典

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/dic.png" style="float:left" />

这些扩展名为`.dic`的都是默认的字典。

**IKAnalyzer.cfg.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
<properties>
	<comment>IK Analyzer 扩展配置</comment>
	<!--用户可以在这里配置自己的扩展字典 -->
	<entry key="ext_dict"></entry>
	 <!--用户可以在这里配置自己的扩展停止词字典-->
	<entry key="ext_stopwords"></entry>
	<!--用户可以在这里配置远程扩展字典 -->
	<!-- <entry key="remote_ext_dict">words_location</entry> -->
	<!--用户可以在这里配置远程扩展停止词字典-->
	<!-- <entry key="remote_ext_stopwords">words_location</entry> -->
</properties>
```



自定义一个**heroc.dic**字典（在该字典中，写入自己需要组合的词）：

```
heroc测分词
```

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/herocdic.png" style="float:left" />

配置文件加入自定义的字典：

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/自定义dic.png" style="float:left" />



这就配置完了，再次启动ES和Kibana，进行ik测试时，heroc测分词 就能组合成一个词。



**没自定义dic之前：**

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/没自定义dic之前.png" style="float:left" />

**自定义dic之后：**

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/自定义dic之后.png" style="float:left" />



### 4、自定义字典 远程词库

这里使用nginx，将词库，放在nginx中，通过访问nginx来获取词库

```bash
docker pull nginx:1.10

# 启动nginx
docker run --name nginx -p 80:80 nginx:1.10

# 拷贝nginx容器中的文件到当前目录，将配置文件等都拷贝出来，方便以后修改
docker container cp nginx:/etc/nginx .

# 停止启动的nginx容器，然后删除nginx容器，再启动nginx
docker run -p 80:80 --name nginx -v /home/mydata/nginx/html:/usr/share/nginx/html -v /home/mydata/nginx/logs:/var/log/nginx -v /home/mydata/nginx/conf:/etc/nginx -d nginx:1.10
```



将分词字典`fenci.txt`放在，.........../nginx/html/ex/fenci.txt 中，修改ik里的配置文件 IKAnalyzer.cfg.xml，找到放在nginx中分词字典

```xml
# 修改ik里的配置文件 IKAnalyzer.cfg.xml

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
<properties>
	<comment>IK Analyzer 扩展配置</comment>
	<!--用户可以在这里配置自己的扩展字典 -->
	<entry key="ext_dict"></entry>
	 <!--用户可以在这里配置自己的扩展停止词字典-->
	<entry key="ext_stopwords"></entry>
	<!--用户可以在这里配置远程扩展字典 -->
	<entry key="remote_ext_dict">http://<ip>/es/fenci.txt</entry>
	<!--用户可以在这里配置远程扩展停止词字典-->
	<!-- <entry key="remote_ext_stopwords">words_location</entry> -->
</properties>

```

配置完后，重启es，即可





## 六、数据类型

（1）字符串类型： text(可以被分词器解析), keyword(不能被分割)

（2）数字类型：`long`, `integer`, `short`, `byte`, `double`, `float`, `half_float`, `scaled_float`

（3）日期：date

（4）日期 纳秒：date_nanos

（5）布尔型：boolean

（6）Binary：binary

（7）Range: `integer_range`, `float_range`, `long_range`, `double_range`, `date_range`



## 七、Rest风格 增删改查

| method | url                                                   | 描述                              |
| ------ | ----------------------------------------------------- | --------------------------------- |
| PUT    | localhost:9200/索引名称/类型名称/文档id               | 创建文档，指定生成文档id          |
| POST   | localhost:9200/索引名称/类型名称                      | 创建文档，随机生成文当id          |
| POST   | localhost:9200/索引名称/类型名称/文档id/_update       | 修改文档                          |
| DELETE | localhost:9200/索引名称/类型名称/文档id               | 删除文档                          |
| GET    | localhost:9200/索引名称/类型名称/文档id               | 通过文档id查询                    |
| POST   | localhost:9200/索引名称/类型名称/_search              | 查询所有数据                      |
| GET    | localhost:9200/索引名称/类型名称/_search?q=name:heroc | 查询有name属性并且值为heroc的数据 |



### . _cat

```bash
GET _cat/nodes  	# 查看所有节点
GET _cat/health		# 查看es健康状况
GET _cat/master		# 查看主节点
GET _cat/indices	# 查看所有索引
```



### . 索引基本操作

在kibana上进行测试工作



#### 1）创建一个索引

创建了一个test01索引，type01类型，01文档id

```json
PUT /test01/type01/01
{
  "name": "heroc",
  "age": "21"
}
```



#### 2）创建一个指定类型的索引库并添加数据

```json
PUT /test02
{
  "mappings": {
    "properties": {
      "name":{
        "type": "text"
      },
      "age":{
        "type": "integer"
      },
      "birthday":{
        "type": "date"
      }
    }
  }
}

# 向test02索引库中插入01数据，默认类型为 _doc
PUT /test02/_doc/01
{
  "name": "heroc",
  "age": 21,
  "birthday": "1998-11-30"
}

# 查询test02索引库的信息
GET /test02

# 查询所有数据
POST /test02/_doc/_search

# 查询符合条件的数据
GET /test01/_doc/_search?q=name:heroc
```

这就只创建了一个拥有name、age、birthday字段的索引库，类型分别为text、integer、date



#### 3）查询索引库相关信息

```json
# 查看整个ES的健康状况
GET /_cat/health
# 查看所有的索引库
GET /_cat/indices?v
```



#### 4）修改更新

方式一：

直接使用put直接对之前的数据进行覆盖

```json
PUT /test02/_doc/01
{
  "name": "heroc",
  "age": 20,
  "birthday": "1998-11-30"
}
```



方式二【推荐】：

使用post

```json
# 修改test02索引库中01文档数据的age字段的值
POST /test02/_doc/01/_update
{
  "doc":{
    "age": 18
  }
}
```



#### 5）删除

```json
# 删除test02索引库中01文档数据
DELETE /test02/_doc/01

# 删除test02索引库
DELETE /test02
```



### . 花样复杂查询 ==重点==

```json
{
  "took" : 1,
  "timed_out" : false,
  "_shards" : {
    "total" : 1,
    "successful" : 1,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : {
      "value" : 1,
      "relation" : "eq"
    },
    "max_score" : 0.2876821, # 表示最高的匹配度分数
    "hits" : [  # hits就是一个对象，里面包含了查询到的所有符合要求的文档
      {
        "_index" : "test01",
        "_type" : "type01",
        "_id" : "01",
        "_score" : 0.2876821, # 这里表示的是查询结果与文档的匹配度，匹配度越高分数越高
        "_source" : {
          "name" : "heroc",
          "age" : "21"
        }
      }
    ]
  }
}
```



#### 1）规定显示字段、排序、分页

```json
GET /test01/_doc/_search # 这里用GET或POST都可以
{
  "query": {
    "match": {  # 查询匹配要求
      "name": "heroc yikex"  # 需要匹配的内容为heroc yikex
    }
  },
  "_source":["age"], # 规定显示的字段内容
  "sort": [  # 排序
    {
      "age": {  # 以age字段，进行升序排序  desc  asc
        "order": "desc"
      }
    }
  ],
  "from": 0, # 分页查询，从索引为0开始，每页显示2条文档
  "size": 2
}
```



##### 精确查询 模糊查询

**match** ：会使用分词器解析，先分析文档，对其进行匹配查询

> match可以对查询的字段进行分词查询，模糊查询，只要包含了分词的字段即可
>
> 比如：查询的是“马上分力”，查询的时候可以拆分为“马上分力”、“马上”、“分力”、“马”，可用这些字段去查询文档



**trem**：精确查询，会使用倒排索引，也就是文章中包含了这个字段(这个字段不能被拆分)

> trem比如：查询的是“马上分力”，查询的时候不能拆分为“马上”、“分力”、“马”，而只能以“马上分力”去查询文档





#### 2）布尔(复合)过滤器

**复合查询：**

```json
{
   "bool" : {
      "must" :     [],
      "should" :   [],
      "must_not" : [],
   }
}
```

`must`  所有的语句都 必须（must） 匹配，与 AND 等价。  

`must_not`  所有的语句都 不能（must not） 匹配，与 NOT 等价。  

`should`  至少有一个语句要匹配，与 OR 等价。



**结果过滤：**

```json
"filter": {
    "range": {
        "age": {
            "gt": 10,
            "lt": 20  
        }
    }
}
```

`range` 表示范围

`age` 表示需要过滤的字段为age

`gt` 表示 greater than 大于

`lt` 表示 less than 小于

`gte` 表示 greater than equals 大于等于

`lte` 表示 less than equals 小于等于



```json
GET /test01/_doc/_search
{
  "query": {
   "bool": {
     "must": [
       {
         "match": {
           "name": "heroc"
         }
       },
       {
         "match": {
           "age": "21"
         }
       }
     ],
     "filter": {
       "range": {
         "age": {
           "gt": 10,
           "lt": 20
         }
       }
     }
   }
  }
}
```



#### 3）高亮查询

```json
"highlight": {
    "pre_tags": "<h4 style='color:red'>",
    "post_tags": "</h4>", 
    "fields": {
        "name":{}
    }
}
```

pre_tags：高亮字段的前缀

post_tags：高亮字段的后缀

fields为高亮的范围：字段为name的有匹配的数据进行高亮显示



```json
GET /test01/type01/_search
{
  "query": {
   "bool": {
     "must": [
       {
         "match": {
           "name": "heroc"
         }
       }
     ],
   }
  },
  "highlight": {
    "pre_tags": "<h4 style='color:red'>",
    "post_tags": "</h4>", 
    "fields": {
      "name":{}
    }
  }
}
```



## 八、Maven项目使用ES

导入maven依赖：

```xml
<dependency>
    <groupId>org.elasticsearch.client</groupId>
    <artifactId>elasticsearch-rest-high-level-client</artifactId>
    <version>7.6.1</version>
</dependency>
```

简单初始化：

```java
RestHighLevelClient client = new RestHighLevelClient(
        RestClient.builder(
                new HttpHost("localhost", 9200, "http"),
                new HttpHost("localhost", 9201, "http")));

// 插入代码，调用API

client.close(); // 使用完一定要关闭
```

> 如果是使用的springboot一定要注意es的版本号要与本地安装的es版本号一致，可在pom.xml中更改
>
> 由于我安装的es是7.6.1的，因此将springboot的es依赖改为对应的版本号
>
> ```xml
> <properties>
>     <java.version>1.8</java.version>
>     <elasticsearch.version>7.6.1</elasticsearch.version>
> </properties>
> ```



### 1、索引库相关操作

先创建一个工具类：

```java
public class ESConfig {
    // 获取客户端对象
    public static RestHighLevelClient getClient(){
        return new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost",9200,"http")));
    }
}
```





#### 1）创建索引库

```java
private RestHighLevelClient client = ESConfig.getClient();

@Test
public void createIndex() throws IOException {
    // 与kibana创建的索引库一样， PUT /idea_create_index
    // 发起一个创建索引库的请求
    CreateIndexRequest request = new CreateIndexRequest("idea_create_index");
    // 用客户端调用indices方法，开始创建索引库
    CreateIndexResponse response= 
        client.indices().create(request,RequestOptions.DEFAULT);
    // 响应结果
    System.out.println(response);
    client.close();
}
```

通过head可视化工具，查看到索引库创建成功

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/idea创建索引库.png" style="float:left" />

> `client.indices().create(request,RequestOptions.DEFAULT)`
>
> 可以理解为客户端对象，在索引集合中去，创建一个索引库



#### 2）查看索引库是否存在

```java
private RestHighLevelClient client = ESConfig.getClient();

// 查询索引库是否存在，返回布尔值
@Test
public void isExistsIndex() throws IOException {
    GetIndexRequest request = new GetIndexRequest("idea_create_index");
    boolean exists = client.indices().exists(request, RequestOptions.DEFAULT);
    System.out.println(exists);
    client.close();
}
```



#### 3）删除索引库

```java
private RestHighLevelClient client = ESConfig.getClient();

// 删除索引库，返回删除后的响应对象，可通过isAcknowledged方法获取状态
@Test
public void delIndex() throws IOException {
    DeleteIndexRequest request = new DeleteIndexRequest("idea_create_index");
    AcknowledgedResponse delete = 
        client.indices().delete(request, RequestOptions.DEFAULT);
    System.out.println(delete.isAcknowledged());
    client.close();
}
```



### 2、文档相关操作

User.java

```java
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private int age;
    ...
}
```

在pom.xml中导入了fastjson依赖

```xml
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
    <version>1.2.68</version>
</dependency>
```





#### 1）添加文档

```java
private RestHighLevelClient client = ESConfig.getClient();

// 添加文档 PUT /idea_create_index/_doc/1
@Test
public void addDocument() throws IOException {
    IndexRequest request = new IndexRequest("idea_create_index");
    // 设置id
    request.id("1");
    // 设置超时时间
    request.timeout("1s");
    // 将对象转换为json对象，然后存储到request对象中
    request.source(JSON.toJSONString(new User("heroc",21)), XContentType.JSON);
    // 创建document
    IndexResponse index = client.index(request, RequestOptions.DEFAULT);
    // 查看返回的结果
    System.out.println(index.toString());
    // 查看document的状态，CREATE UPDATED
    System.out.println(index.status());
    client.close();
}
```



#### 2）文档是否存在

```java
private RestHighLevelClient client = ESConfig.getClient();

// 查看文档是否存在
@Test
public void isExistsDocument() throws IOException {
    GetRequest request = new GetRequest("idea_create_index","1");
    // 不获取 _source 中的内容，返回的对象中就没有 _source 属性
    request.fetchSourceContext(new FetchSourceContext(false));
    request.storedFields("_none_");
    // 判断是否存在
    boolean exists = client.exists(request, RequestOptions.DEFAULT);
    System.out.println(exists);
    client.close();
}
```

> 输出 true



#### 3）获取文档

```java
private RestHighLevelClient client = ESConfig.getClient();

// 获取文档内容
@Test
public void getDocument() throws IOException {
    GetRequest request = new GetRequest("idea_create_index", "1");
    GetResponse response = client.get(request, RequestOptions.DEFAULT);
    // 获取 _source 中的数据
    System.out.println(response.getSourceAsString());
    // 获取整个返回的文档内容
    System.out.println(response);
    client.close();
}
```

> 输出：
>
> {"age":21,"name":"heroc"}
> {"_index":"idea_create_index","_type":"_doc","_id":"1","_version":1,"_seq_no":0,"_primary_term":1,"found":true,"_source":{"age":21,"name":"heroc"}}



#### 4）更新文档

```java
private RestHighLevelClient client = ESConfig.getClient();

// 更新指定的文档
@Test
public void updateDocument() throws IOException {
    UpdateRequest request = new UpdateRequest("idea_create_index", "1");
    Map<Object, Object> map = new HashMap<>();
    map.put("age",18);
    request.doc(JSON.toJSONString(map),XContentType.JSON);
    UpdateResponse updateResponse = client.update(request, RequestOptions.DEFAULT);
    System.out.println(updateResponse.status());
    client.close();
}
```



#### 5）删除文档

```java
private RestHighLevelClient client = ESConfig.getClient();

// 删除指定的文档
@Test
public void delDocument() throws IOException {
    DeleteRequest request = new DeleteRequest("idea_create_index", "1");
    DeleteResponse deleteResponse = client.delete(request, RequestOptions.DEFAULT);
    System.out.println(deleteResponse.status());
}
```



#### 6）批量操作文档

```json
// 批量添加数据 可举一反三 删除更新查询
@Test
public void bulkDocument() throws IOException {
    BulkRequest bulkRequest = new BulkRequest();
bulkRequest.timeout("10s");

// 制造数据
ArrayList<User> users = new ArrayList<>();
users.add(new User("heroc01",20));
users.add(new User("heroc02",20));
users.add(new User("heroc03",20));
users.add(new User("heroc04",20));
users.add(new User("heroc05",18));
users.add(new User("heroc05",20));
users.add(new User("heroc05",21));

// 批量添加
for (int i = 0; i < users.size(); i++) {
    bulkRequest.add(new IndexRequest("idea_create_index")
                    .id(""+(i+2))
                    .source(JSON.toJSONString(users.get(i)), XContentType.JSON));
}

BulkResponse bulkResponse = client.bulk(bulkRequest, RequestOptions.DEFAULT);
System.out.println(bulkResponse.hasFailures()); // 返回false即代表添加成功
}
```



#### 7）复杂查询

```java
// 复杂自定义查询
@Test
public void searchDocument() throws IOException {
    SearchRequest searchRequest = new SearchRequest("idea_create_index");

    // 通过SearchSourceBuilder来创建查询体
    SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
    TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name", "heroc05");
    sourceBuilder.query(termQueryBuilder);
    sourceBuilder.timeout(new TimeValue(10, TimeUnit.SECONDS));

    searchRequest.source(sourceBuilder);
    SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
    System.out.println(JSON.toJSONString(searchResponse.getHits()));
    System.out.println("----------------------------------------------");
    for (SearchHit hit : searchResponse.getHits().getHits()) {
        System.out.println(hit.getSourceAsMap());
    }
}
```



## 九、Linux 使用ES

linux版本：阿里云centos7

ES版本：elasticsearch-7.6.1

jdk版本：jdk1.8以上



### 1、ES 安装

#### 1）下载安装包

在官网下载ES的linux版本



#### 2）安装过程

> 将.tar.gz安装包通过XTP导入到linux自己的目录中

![](https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/linux_es安装包.png)



> 解压elasticsearch-7.6.1

```bash
tar -zxvf elasticsearch-7.6.1-linux-x86_64.tar.gz
```

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/linux_es解压.png" style="float:left" />



> 进入解压文件中的bin目录下尝试运行elasticsearch，简称es

```bash
./elasticsearch
```

【**报错**】

**unable to load JNA native support library, native methods will be disabled.**

这是由于es中的依赖包jna不能被运行，本地方法不可使用。

【**解决**】

进入es解压文件的lib目录：

将jna的jar包备份移除，或者重命名为不可识别的文件

```bash
mv jna-4.5.1.jar jna-4.5.1.jar.back
```

接着在es解压文件的lib目录下执行：

```bash
wget https://repo1.maven.org/maven2/net/java/dev/jna/jna/4.5.1/jna-4.5.1.jar
```

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/linux_es更改jna包.png" style="float:left" />



【**报错**】

**java.lang.RuntimeException: can not run elasticsearch as root**

报错原因：是因为es官方规定不允许root用户来运行es服务，是为了确保安全

【**解决**】

1，以root用户创建一个新用户

```bash
useradd es
# 添加一个es用户

passwd es
# 给es用户设置密码
```

2，将es文件的属主和属组

```
chown -R es:es elasticsearch-7.6.1
```

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/linux_es更改属主属组.png" style="float:left" />



> 切换到es用户执行elasticsearch

```
su es

./elasticsearch
```

【**报错**】

**org.elasticsearch.bootstrap.StartupException: ElasticsearchException[X-Pack is not supported and Machine Learning is not available for [linux-x86]; you can use the other X-Pack features (unsupported) by setting xpack.ml.enabled: false in elasticsearch.yml]**

不支持x-pack

【**解决**】

在config目录下elasticsearch.yml添加以下配置`vim elasticsearch.yml`

```bash
xpack.ml.enabled:false
```

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/linux_es添加xpack配置.png" style="float:left" />



> 再次启动es，发现启动成功

```bash
# 当前运行
./elasticsearch

# 支持后台运行
./elasticsearch &
# 后台运行要关闭es，直接用kill命令杀掉进程即可
ps -aux | grep slasticsearch
# 可查看es的进程号
```

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/linux_es启动成功.png" style="float:left" />



#### 3）安装成功测试

> 检验是否已成功：

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/linux_es测试.png" style="float:left" />

看到这样的信息，表示es已安装成功，并且可以以localhost正常使用！



**安装总结：**

1. es需要创建一个用户来运行，不能使用root账户运行
2. es的解压文件，属主和属组都是创建的新用户
3. 更改elasticsearch.yml配置文件，忽略xpack的检测：`xpack.ml.enabled:false`
4. 如果启动es服务之后，报错jna不可用，那么需要重新通过wget的方式从maven中央仓库获取jna包







#### 4）linux上的基础操作



> 查看所有索引库

```bash
curl http://localhost:9200/_cat/indices?v
```

> 查看所有索引库的健康状态

```bash
curl http://localhost:9200/_cat/health?v
```

> 创建一个索引库

```bash
curl -XPUT http://localhost:9200/test_index?pretty
```

> 删除一个索引库

```bash
curl -XDELETE http://localhost:9200/test_index?pretty
```



更多命令：[elasticsearch linux上操作es命令详解][https://www.cnblogs.com/sxdcgaq8080/p/11118947.html]



### 2、远程连接 ES

不建议远程连接，因为防火墙开启端口，会造成一定的安全风险



#### 1）防火墙开启端口

root用户执行：

```bash
# 防火墙开启9200端口
firewall-cmd --zone=public --add-port=9200/tcp --permanent

# 重启防火墙
systemctl restart firewalld

# 防火墙关闭9200端口，关闭后一定要重启防火墙才有效
firewall-cmd --zone=public --remove-port=9200/tcp --permanent
```



#### 2）开启安全组

在阿里云服务器管理平台，开启该端口的安全组

![](D:\全栈学习\ElasticSearch\picture\linux_es安全组开启.png)



#### 3）更改elasticsearch.yml配置

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/linux_es更改网络地址端口.png" style="float:left" />



#### 4）运行报错 解决

【**报错**】

**ERROR: [3] bootstrap checks failed**
**[1]: max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]**
**[2]: system call filters failed to install; check the logs and fix your configuration or disable system call filters at your own risk**
**[3]: the default discovery settings are unsuitable for production use; at least one of [discovery.seed_hosts, discovery.seed_providers, cluster.initial_master_nodes] must be configured**

【**解决**】

**[1]**
elasticsearch用户拥有的内存权限太小，至少需要262144
切换为root用户编辑 /etc/sysctl.conf，追加以下内容：

```
vm.max_map_count = 262144
```

追加后执行 sysctl -p



**[2]**

es用户在elasticsearch.yml中添加：

```
bootstrap.memory_lock: false
bootstrap.system_call_filter: false
```



**[3]**

es用户在elasticsearch.yml中Discovery将以下配置打开即可：

```
cluster.initial_master_nodes: ["node-1"]
```



【**报错**】

**org.apache.logging.log4j.core.appender.RollingFileAppender for element RollingFile. java.lang.reflect.InvocationTargetException**

说明没有log4j的支持

【**解决**】

在root用户下安装log4j即可

```bash
yum install -y log4j*
```



#### 5）运行成功

使用root创建的新用户es用户启动服务

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/linux_es启动成功.png" style="float:left" />

成功启动。

测试，输入服务器地址加端口号：

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/linux_es远程测试成功.png" style="float:left" />

访问成功！！



### 3、安装ik

ik版本一定要与es的版本一致，ik是es的一个分词插件，支持基本的中文分词



#### 1）下载安装包

windows下载elasticsearch-analysis-ik-7.6.1的zip压缩包并解压



#### 2）安装过程

解压之后，将解压后的文件通过xftp，传到linux的elasticsearch目录下的plugins目录下

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/linux_es移动ik.png" style="float: left;" />



```bash
# 将ik文件的属主和属组更改为es用户
chown -R es:es ik
```



启动es服务，就可以看到加载了ik插件

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForElasticSearch/linux_es安装ik插件.png" style="float:left" />





## 十、Docker 使用ES

```bash
docker pull elasticsearch:7.4.2

mkdir -p /home/mydata/elasticsearch/config
mkdir -p /home/mydata/elasticsearch/data
mkdir -p /home/mydata/elasticsearch/plugins
echo "http.host:0.0.0.0" >> /home/mydata/elasticsearch/config/elasticsearch.yml

更改目录权限 chmod -R 777 /home/mydata/elasticsearch/

docker run --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -e ES_JAVA_OPTS="-Xms64m -Xmx128m" -v /home/mydata/elasticsearch/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml -v /home/mydata/elasticsearch/data:/usr/share/elasticsearch/data -v /home/mydata/elasticsearch/plugins:/usr/share/elasticsearch/plugins -d elasticsearch:7.4.2
```





## 十一、实战



### 1、爬取数据

Jsoup是用于网页内容解析

```xml
<!-- https://mvnrepository.com/artifact/org.jsoup/jsoup -->
<dependency>
    <groupId>org.jsoup</groupId>
    <artifactId>jsoup</artifactId>
    <version>1.11.3</version>
</dependency>
```



[https://www.cnblogs.com/sxdcgaq8080/p/11118947.html]: 



## 十二、额外补充

### 1、保存 更新

索引customer下的external类型下保存1号数据。(索引看成库、类型看成表)

```json
PUT customer/external/1 	## 发送多次更新操作；保存更新都可

POST customer/external 	## 不指定id，会在自动生成id号，post也可以创建数据，多次发送，每次会新建；保存更新都可

#返回结果

{
    "_index": "customer", 	# index索引库
    "_type": "external", 	# type类型表
    "_id": "1", 			# id号
    "_version": 1,			# 版本号，更新一次就叠加一次
    "result": "created",	# 新建
    "_shards": {
        "total": 2,
        "successful": 1,
        "failed": 0
    },
    "_seq_no": 0,
    "_primary_term": 1
}
```



### 2、查询文档、乐观锁修改

```json
GET customer/external/1 	## 在customer索引库下的external类型中查找id为1的数据

{
    "_index": "customer", 	# 索引库
    "_type": "external",	# 类型
    "_id": "1",				# id号
    "_version": 1,			# 版本号
    "_seq_no": 0,			# 并发控制字段，每次更新就会+1，用来做乐观锁
    "_primary_term": 1,		# 同上，主分片重新分配，如重启就会变化  
    "found": true,
    "_source": {			# 内容
        "name": "heroc"
    }
}

### 乐观锁修改
PUT customer/external/1?if_seq_no=1&if_primary_term=1 ## 则会判断seq_no是否为1，为1则更改数据，修改完了seq_no会递增+1，不为1则会被拒绝，不允更改
```



### 3、更新文档

```json
# 方式一：会对比原来的数据，如果一致则不会发生变化
POST customer/external/1/_update
{
    "doc":{
        "name":"heroc"
    }
}

# 方式二：不会对比原来的数据，不管是否一致，只要发送了该请求，就会更新，版本、结果都会更替改变
POST customer/external/1
{
    "name":"heroc"
}

# 方式三：不会对比原来的数据，不管是否一致，只要发送了该请求，就会更新，版本、结果都会更替改变
PUT customer/external/1
{
    "name":"heroc"
}
```



### 4、删除文档、索引

```json
# 删除文档
DELETE customer/external/1

# 删除索引
DELETE customer
```



### 5、批量 _bulk

```json
POST customer/external/_bulk
## 发送的数据
# 第一条json，锁定索引库、类型表、id号文档；index[create]添加、delete删除、update更新
{
    "index[delete、update、create]":{
        "_id":"1"
    }
}
# 第二条json，文档内容
{
    "name":"heroc"
}


################ 批量添加 customer索引库、external类型表、id号文档
POST _bulk
{
    "index":{
        "_index":"customer",
        "_type":"external",
        "_id":"1"
    }
}
{
    "name":"heroccc",
    "age":"22"
}
```



### 6、查询 _search、 匹配 match、range

```json
GET bank/_search
{
    "query":{			# 查询
  以下多种匹配，任选一：
  全文匹配   "match_all":{}	# 匹配所有
  精准匹配   "match":{ 		# 精准匹配 指定字段匹配
                "account_number":"20",   # 只匹配account_number为20的文档
                "address":"mill road",    # 会匹配含有mill、road单词的文档，分词匹配
    			"address.keyword":"789 Madison Street" # 加上keyword，只筛选出值一模一样的数据
			}
  短语匹配	 "match_phrase":{
                "address":"mill road"	 # 以 mill road 为整体匹配
            }
 多字段匹配  "multi_match":{
     			"query":"mill road",   # 查询mill字段，从state或者address中查询匹配都可
     			"fields":["state","address"] # 比如在state、address中匹配到mill或者road都可
 			}
 复合查询	 "bool":{
     			"must":[ 		# 必须满足的匹配
                    {
                        "match":{
                            "gender":"F"
                        }
                    }
                ],
				"must_not":[	# 必须不匹配
                    {
                        "match":{
                            "age":"38"
                        }
                    }
                ],
				"should":[		# 匹配可以，不匹配也可以。匹配了得分会更高
                    {
                        "match":{
                            "lastname":"wallace"
                        }
                    }
                ],
				"filter":{      # 将结果过滤出年龄在18至40的数据，不会计算相关性得分
                    "range":{
                        "age":{
                            "gte":18,
                            "lte":30
                        }
                    }
                }
 			},
精确值的查询	"term":{ # term只用于值是数字、精确的值做查询，不会分词查询，文本值都是分词倒排索引
                "age":"28"
            }

    },
    "sort":[
        {			# 排序
        	"account_number":"asc"	# 以account_number升序排序
    	},
        {
            "balance":"desc"		# 以balance降序排序
        }
    ],
	"from":10,		# 从第10条开始
	"size":10,		# 返回10条记录
	"_source":["age","balance"] # 指定需要显示的字段，此处设置的显示age字段和balance字段
}
```



### 7、聚合 可嵌套

搜索address中包含mill的所有人的年龄分布以及平均年龄

```json
GET bank/_search
{
    "query":{
        "match":{
            "addrees":"mill"
        }
    },
    "aggs":{	# 聚合
        "ageAgg":{	# 给本聚合取个名字
            "terms":{
            	"field":"age",	# 查询出的结果以age进行聚合
            	"size":"10"		# 只显示聚合后的前10条结果
           	},
    		"aggs":{	# 子聚合，在ageAgg聚合的情况下，查看每个年龄分布的平均工资
                 "ageAvg":{
                 	"avg":{
                    	"filed":"balance"
                   }   
                 }
            }
        },
		"ageAvgAgg":{	# 聚合取名字
            "avg":{		# 求平均值，以age字段
                "field":"age"
            }
        }
    },
	"size":"0"	# 该句 不显示搜索结果，只看聚合结果
}
```



### 8、mapping 映射

https://www.elastic.co/guide/en/elasticsearch/reference/current/removal-of-types.html



#### .  ==7.0之后不用 type==

type都默认为`_doc`

> Indices created in Elasticsearch 7.0.0 or later no longer accept a `_default_` mapping. Indices created in 6.x will continue to function as before in Elasticsearch 6.x. **Types are deprecated in APIs in 7.0**, with breaking changes to the index creation, put mapping, get mapping, put template, get template and get field mappings APIs.

在7.0以后的版本，elasticsearch的mapping不将使用type类型，因为：

> In an Elasticsearch index, fields that have the same name in different mapping types are backed by the same Lucene field internally. In other words, using the example above, the `user_name` field in the `user` type is stored in exactly the same field as the `user_name` field in the `tweet` type, and both `user_name` fields must have the same mapping (definition) in both types.
>
> ......
>
> On top of that, storing different entities that have few or no fields in common in the same index leads to sparse data and interferes with Lucene’s ability to compress documents efficiently.

在同一个Elasticsearch索引中，其中不同映射类型中的同名字段在内部是由同一个Lucene字段来支持的。换句话说，使用上面的例子，user类型中的user_name字段与tweet类型中的user_name字段是完全一样的，并且两个user_name字段在两个类型中必须具有相同的映射（定义）。

这会在某些情况下导致一些混乱，比如，在同一个索引中，当你想在其中的一个类型中将deleted字段作为date类型，而在另一个类型中将其作为boolean字段。

在此之上需要考虑一点，如果同一个索引中存储的各个实体如果只有很少或者根本没有同样的字段，这种情况会导致稀疏数据，并且会影响到Lucene的高效压缩数据的能力。



#### . 映射 指定类型 数据迁移

```json
## 创建my-index这个索引中字段的类型映射
PUT /my-index
{
    "mappings": {
        "properties": { 
            "age": { "type": "integer" },
            "email": { "type": "keyword" },
            "name": { "type":"text" }
        }
    }
}


## 添加新的字段映射
PUT /my-index/_mapping
{
    "properties":{
        "employee-id":{
            "type":"keyword",
            "index":false  # 该字段不被索引，意思就是检索不到
        }
    }
}

## 查看映射结果
GET /my-index/_mapping
```



如果要更新一个索引的映射类型，只能创建新的索引指定修改后的索引，然后将数据迁移到新的索引中

```json
### 7.0之后的迁移方法，没有type
POST _reindex
{
    "source":{	# 数据迁移 源索引
		"index":"twitter"
    },
    "dest":{	# 数据迁移 目标索引
        "index":"new_twitter"
    }
}

### 6.0的数据迁移到7.0
POST _reindex
{
    "source":{	# 数据迁移 源索引
		"index":"twitter",
        "type":"tweet"
    },
    "dest":{	# 数据迁移 目标索引
        "index":"new_twitter"
    }
}
```



### 9、SpringBoot 配置 es



```java
@Configuration
public class MallElasticSearchConfig {

    public static final RequestOptions COMMON_OPTIONS;
    static {
        RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();
//        builder.addHeader("Authorization", "Bearer " + TOKEN);
//        builder.setHttpAsyncResponseConsumerFactory(
//                new HttpAsyncResponseConsumerFactory
//                        .HeapBufferedResponseConsumerFactory(30 * 1024 * 1024 * 1024));
        COMMON_OPTIONS = builder.build();
    }

    @Bean
    public RestHighLevelClient esRestClient() {
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("ip地址", 9200, "http")));
        return client;
    }
}
```















