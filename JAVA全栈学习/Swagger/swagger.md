# Swagger



## 一、依赖包

```xml
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>javax.servlet-api</artifactId>
    <version>4.0.1</version>
</dependency>

<!-- Swagger -->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>2.9.2</version>
</dependency>
<!--swagger-ui.html-->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>2.9.2</version>
</dependency>
<!--doc.html-->
<dependency>
    <groupId>com.github.xiaoymin</groupId>
    <artifactId>swagger-bootstrap-ui</artifactId>
    <version>1.8.7</version>
</dependency>

<!--Json解析工具-->
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>2.10.0</version>
</dependency>
```



## 二、配置

```java
package com.heroc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/7/15
 * Time: 22:13
 * Description:
 * Version: V1.0
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    // 一个Docket决定一个版面
    @Bean
    public Docket docket(Environment environment){

        // 获取spring环境，如果为dev和test模式的环境，返回true，即可访问swagger页面
        Profiles profiles = Profiles.of("dev", "test");
        // 判断当前角色环境是否是dev或者是test，是则返回true，否则返回false
        boolean flag = environment.acceptsProfiles(profiles);

        /**
        * apiInfo 页面基本信息
        * groupName 表示版面，版面对应你的接口
        * enable 是否可访问，true可访问，false不可访问
        * select 和 build 一起使用
        * apis 用于扫描接口的包，可通过指定类注解扫描，可通过指定的包扫描，any是扫描所有，none不扫描
        */
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo()) // 页面信息
                .groupName("heroc")
                .enable(flag)
                .select() // select和build一起使用
                .apis(RequestHandlerSelectors.basePackage("com.heroc.controller")) // 被扫描的包
                // .paths(PathSelectors.ant("/api/**")) // 过滤以/api/开头的所有路径接口
                .build();
    }

    public ApiInfo apiInfo(){
        // 作者信息
        Contact contact = new Contact("heroc", "", "herocheung@foxmail.com");
        return new ApiInfo(
                "User Api 文档" // 该版面的大标题
                , "用户相关的接口" // 该版面相关连接
                , "1.0" // 该版面的版本号
                , "urn:tos"
                , contact // 作者信息
                , "Apache 2.0" // 协议
                , "http://www.apache.org/licenses/LICENSE-2.0" // 协议url地址
                , new ArrayList());
    }
}
```



## 三、spring-mvc.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd
			http://www.springframework.org/schema/context
			https://www.springframework.org/schema/context/spring-context.xsd http://www.springframework.org/schema/mvc https://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <context:component-scan base-package="com.heroc.controller"/>

    <!--扫描设置了注解的swagger的配置文件-->
    <context:component-scan base-package="com.heroc.config"/>

    <mvc:default-servlet-handler/>

    <!--处理restController注解，直接返回中文字符串乱码问题-->
    <mvc:annotation-driven>
        <mvc:message-converters>
            <bean class="org.springframework.http.converter.StringHttpMessageConverter">
                <constructor-arg ref="utf8Charset"></constructor-arg>
            </bean>
        </mvc:message-converters>
    </mvc:annotation-driven>
    <bean id="utf8Charset" class="java.nio.charset.Charset" factory-method="forName">
        <constructor-arg value="UTF-8"/>
    </bean>
    
    <!--swagger相关ui和静态文件的放行-->
    <mvc:resources mapping="swagger-ui.html" location="classpath:/META-INF/resources/"/>
    <mvc:resources mapping="/webjars/**" location="classpath:/META-INF/resources/webjars/"/>

</beans>
```



## 四、web.xml

设置当前为什么角色

dev 为开发模式

pro 为生产模式

`spring.profiles.active` 激活形象

```xml
<context-param>
    <param-name>spring.profiles.active</param-name>
    <param-value>pro</param-value>
</context-param>
```

