# SpringBoot

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringBoot/学习目录.png)



## 一、什么是微服务

[微服务文章 martin fowler <\<microservice>>](https://www.cnblogs.com/zgynhqf/p/5323056.html)



## 二、启动流程原理

 [SPRINGBOOT启动流程及其原理](https://www.cnblogs.com/theRhyme/p/11057233.html)



#### 谈谈对springboot的理解

- 自动装配过程
- run()方法



## 三、配置文件读取yaml、properties

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringBoot/自动读取yaml配置文件和properties文件.png)

注解：

@ConfigurationProperties扫描yaml配置文件，并找到配置文件person开头的对象，将属性与配置文件的key一一对应，进行赋值。

@PropertySource是扫描properties配置文件，与@Value一起使用，@Value中写的是properties中的属性名，占位符。可以找到指定的值，进行赋值。



![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringBoot/yaml随机值占位符.png)

在yaml中，`${}`是占位符的意思。

`${person.hello:hello}` 意思是如果person.hello存在，那么就取该值，如果不存在就取hello

> yaml中可以用横线，java的bean可以自动识别
>
> 比如first-name: tom
>
> java中的属性为firstName，也可以自动注入赋值



## 四、JSR303校验

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringBoot/jsr303.png)

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-validation</artifactId>
</dependency>
```

yaml配置文件支持jsr303校验



[jsr303校验](https://developer.ibm.com/zh/articles/j-lo-jsr303/)

| **Constraint**                | **详细信息**                                             |
| :---------------------------- | :------------------------------------------------------- |
| `@Null`                       | 被注释的元素必须为 `null`                                |
| `@NotNull`                    | 被注释的元素必须不为 `null`                              |
| `@AssertTrue`                 | 被注释的元素必须为 `true`                                |
| `@AssertFalse`                | 被注释的元素必须为 `false`                               |
| `@Min(value)`                 | 被注释的元素必须是一个数字，其值必须大于等于指定的最小值 |
| `@Max(value)`                 | 被注释的元素必须是一个数字，其值必须小于等于指定的最大值 |
| `@DecimalMin(value)`          | 被注释的元素必须是一个数字，其值必须大于等于指定的最小值 |
| `@DecimalMax(value)`          | 被注释的元素必须是一个数字，其值必须小于等于指定的最大值 |
| `@Size(max, min)`             | 被注释的元素的大小必须在指定的范围内                     |
| `@Digits (integer, fraction)` | 被注释的元素必须是一个数字，其值必须在可接受的范围内     |
| `@Past`                       | 被注释的元素必须是一个过去的日期                         |
| `@Future`                     | 被注释的元素必须是一个将来的日期                         |
| `@Pattern(value)`             | 被注释的元素必须符合指定的正则表达式                     |



eg:

```java
@Component
@Validated
public class Person {
    @NotNull
    private String name;
    @Email
    private String email;
}
```



## 五、配置文件的优先级、多环境配置

### 1、配置文件位置

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringBoot/配置文件读取优先级.png)

这四个位置都可以读取到配置文件，但是永远读取优先级高的。



### 2、多环境配置，开发环境、生成环境

```yaml
# 激活dev角色的配置
debug: true
# debug为ture可以查看加载的类
spring:
  profiles:
    active: dev
server:
  port: 8081
  
---
spring:
  profiles: dev
server:
  port: 8082
  
---
spring:
  profiles: test
server:
  port: 8083
  
---
spring:
  profiles: pro
server:
  port: 8084
```

`---`表示不同的配置，将不同配置以`---`分割为不同的配置文件



## 六、Condition注解扩展

- @ConditionalOnBean：仅仅在当前上下文中存在某个对象时，才会实例化一个Bean。
- @ConditionalOnClass：某个class位于类路径上，才会实例化一个Bean。
- @ConditionalOnExpression：当表达式为true的时候，才会实例化一个Bean。
- @ConditionalOnMissingBean：仅仅在当前上下文中不存在某个对象时，才会实例化一个Bean。
- @ConditionalOnMissingClass：某个class类路径上不存在的时候，才会实例化一个Bean。
- @ConditionalOnNotWebApplication：不是web应用，才会实例化一个Bean。
- @ConditionalOnBean：当容器中有指定Bean的条件下进行实例化。
- @ConditionalOnMissingBean：当容器里没有指定Bean的条件下进行实例化。
- @ConditionalOnClass：当classpath类路径下有指定类的条件下进行实例化。
- @ConditionalOnMissingClass：当类路径下没有指定类的条件下进行实例化。
- @ConditionalOnWebApplication：当项目是一个Web项目时进行实例化。
- @ConditionalOnNotWebApplication：当项目不是一个Web项目时进行实例化。
- @ConditionalOnProperty：当指定的属性有指定的值时进行实例化。
- @ConditionalOnExpression：基于SpEL表达式的条件判断。
- @ConditionalOnJava：当JVM版本为指定的版本范围时触发实例化。
- @ConditionalOnResource：当类路径下有指定的资源时触发实例化。
- @ConditionalOnJndi：在JNDI存在的条件下触发实例化。
- @ConditionalOnSingleCandidate：当指定的Bean在容器中只有一个，或者有多个但是指定了首选的Bean时触发实例化。



# SpringBoot Web开发



## 一、静态资源 加载步骤 目录 优先级

org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration

```java
@Override
public void addResourceHandlers(ResourceHandlerRegistry registry) {
    if (!this.resourceProperties.isAddMappings()) {
        logger.debug("Default resource handling disabled");
        return;
    }
    Duration cachePeriod = this.resourceProperties.getCache().getPeriod();
    CacheControl cacheControl = this.resourceProperties.getCache().getCachecontrol().toHttpCacheControl();
    if (!registry.hasMappingForPattern("/webjars/**")) {
        customizeResourceHandlerRegistration(registry.addResourceHandler("/webjars/**")
                                             .addResourceLocations("classpath:/META-INF/resources/webjars/")
                                             .setCachePeriod(getSeconds(cachePeriod)).setCacheControl(cacheControl));
    }
    String staticPathPattern = this.mvcProperties.getStaticPathPattern();
    if (!registry.hasMappingForPattern(staticPathPattern)) {
        customizeResourceHandlerRegistration(registry.addResourceHandler(staticPathPattern)
                                             .addResourceLocations(getResourceLocations(this.resourceProperties.getStaticLocations()))
                                             .setCachePeriod(getSeconds(cachePeriod)).setCacheControl(cacheControl));
    }
}
```



这段代码是加载静态文件的代码：

先在webjars目录下找，找不到去每个依赖包的`classpath:/META-INF/resources/webjars/`目录下找静态文件，依然找不到，会

staticPathPattern表示的是`/**`即，访问路径在`/**`都会去getStaticLocations()方法下的路径找静态文件：

`org.springframework.boot.autoconfigure.web.ResourceProperties`

```java
private static final String[] CLASSPATH_RESOURCE_LOCATIONS = { "classpath:/META-INF/resources/","classpath:/resources/", "classpath:/static/", "classpath:/public/" };
```

即本目录下的这些文件中去找

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringBoot/静态文件.png" style="float:left" />



**优先级**

> 如果静态文件有重名的，
>
> 那么优先访问"classpath:/META-INF/resources/"，访问到了后者都不访问了。
>
> 其次"classpath:/resources/","classpath:/static/","classpath:/public/"



```java
private Optional<Resource> getWelcomePage() {
    String[] locations = getResourceLocations(this.resourceProperties.getStaticLocations());
    return Arrays.stream(locations).map(this::getIndexHtml).filter(this::isReadable).findFirst();
}

private Resource getIndexHtml(String location) {
    return this.resourceLoader.getResource(location + "index.html");
}
```

index.html页面，会去`resourceProperties.getStaticLocations()`方法的路径中去找



> templates 目录，
>
> 必须使用controller才能访问该目录下的页面



## 二、使用thymeleaf

[thymeleaf文档](https://www.thymeleaf.org/doc/tutorials/3.0/usingthymeleaf.html#a-website-for-a-grocery)

> templates 目录，
>
> 必须使用controller才能访问该目录下的页面



html要使用thymeleaf模板，必须引入连接

```html
<html lang="en" xmlns:th="http://www.thymeleaf.org">
```



controller层：

使用ModelAndView，返回test页面，model中存入数据。

```java
@Controller
public class HelloController {

    @GetMapping("/hello")
    public String getMsg(Model model){
        model.addAttribute("msg","<h1>Hello,SpringBoot！</h1>");
        return "test";
    }
}
```

templates 目录下的 test.html 通过thymeleaf获取model中的数据：

```html
<!doctype html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div th:text="${msg}"></div>
<div th:utext="${msg}"></div>
</body>
</html>

```

<img src=".\springBoot\th的text与utext.png" style="float:left" />

`th:text` 将特殊字符进行了转义，`th:utext` 将特殊字符不进行转义



## 三、MVC配置

### 1、自配置试图解析器

```java
/**
 * 自扩展mvc配置  dispatchservlet
 */
@Configuration
public class MyMvcConfig implements WebMvcConfigurer {

    /**
     * 将自己写的视图解析器注册到ioc容器中
     * @return
     */
    @Bean
    public ViewResolver myViewResolver(){
        return new MyViewResolver();
    }

    /**
     * 实现了视图解析器接口，编写自己视图解析器的规则
     */
    public static class MyViewResolver implements ViewResolver{
        @Override
        public View resolveViewName(String s, Locale locale) throws Exception {
            return null;
        }
    }
}
```



## 四、拦截 interceptor

继承HandlerInterceptor接口，写拦截规则：

```java
public class MyInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        String userName = (String) session.getAttribute("userName");
        if (StringUtils.isEmpty(userName)){
            request.getRequestDispatcher("/index.html").forward(request,response);
            return false;
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
```

在mvc扩展配置类中，重写WebMvcConfigurer接口的addInterceptors方法，添加拦截器，并注入到ioc容器中：

```java
@Configuration
public class MyMvcConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new MyInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/","/index.html","/css/**","/js/**","/img/**","/login/user","/pInfo");
    }
}
```



## 五、国际化 i18n

国际化单词 internationalization

i18n 中的18表示首字母i和尾字母n之间有18个单词



在WebMvcAutoConfiguration中有一个WebMvcAutoConfigurationAdapter内部类中`localeResolver()`区域解析器方法，重写接口`LocaleResolver`可以设置语言。

```java
@Bean
@ConditionalOnMissingBean
@ConditionalOnProperty(prefix = "spring.mvc", name = "locale")
public LocaleResolver localeResolver() {
    // 用户设置了LocaleResolver就使用用户的，没有设置，就走默认的AcceptHeaderLocaleResolver
    if (this.mvcProperties.getLocaleResolver() == WebMvcProperties.LocaleResolver.FIXED) {
        return new FixedLocaleResolver(this.mvcProperties.getLocale());
    }
    AcceptHeaderLocaleResolver localeResolver = new AcceptHeaderLocaleResolver();
    localeResolver.setDefaultLocale(this.mvcProperties.getLocale());
    return localeResolver;
}
```

国际化配置与spring的messages有关 `MessageSourceProperties` 中的basename可以配置语言识别的路径

thymeleaf使用`#{}`来识别messages国际化



**步骤如下：**

> 1、创建国际化语言properties

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringBoot/i18n.png" style="float:left" />

先创建一个login.properties，再创建一个login_en_US.properties，会自动合并文件。

从上至下，默认显示的语言，英文语言，中文语言。

login.properties

```properties
login.btn=登录
login.pwd=密码
login.remember=记住我
login.tip=请登录
login.username=用户名
```

login_en_US.properties

```properties
login.btn=Sign in
login.pwd=Password
login.remember=Remember me
login.tip=Please sign in
login.username=Username
```

login_zh_CN.properties

```properties
login.btn=登录
login.pwd=密码
login.remember=记住我
login.tip=请登录
login.username=用户名
```



> 2、index.html 中使用#{}

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Signin Template for Bootstrap</title>
		<!-- Bootstrap core CSS -->
		<link th:href="@{/css/bootstrap.min.css}" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link th:href="@{/css/signin.css}" rel="stylesheet">
	</head>

	<body class="text-center">
		<form class="form-signin" action="dashboard.html">
			<img class="mb-4" th:src="@{/img/bootstrap-solid.svg}" alt="" width="72" height="72">
			<h1 class="h3 mb-3 font-weight-normal" th:text="#{login.tip}">Please sign in</h1>
			<input type="text" class="form-control" th:placeholder="#{login.username}" required="" autofocus="">
			<input type="password" class="form-control" th:placeholder="#{login.pwd}" required="">
			<div class="checkbox mb-3">
				<label>
          <input type="checkbox" value="remember-me"> [[#{login.remember}]]
        </label>
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit">[[#{login.btn}]]</button>
			<p class="mt-5 mb-3 text-muted">© 2017-2018</p>
			<a class="btn btn-sm" th:href="@{/(lang='zh_CN')}">中文</a>
			<a class="btn btn-sm" th:href="@{/(lang='en_US')}">English</a>
		</form>
	</body>
</html>
```



> 3、配置文件

```yaml
# 配置国际化语言，指定语言文件路径，以及默认文件
spring:
  messages:
    basename: i18n.login
```



> 4、实现LocaleResolver本地解析器

```java
public class MyLocaleResolver implements LocaleResolver {

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        // 获取路径中lang参数的值
        String language = request.getParameter("lang");
        // 获取默认配置
        Locale locale = Locale.getDefault();
        // 如果lang参数不为空，则设置语言环境
        if (!StringUtils.isEmpty(language)){
            String[] split = language.split("_");
            // 第一个参数是语言，第二个参数是地区
            locale = new Locale(split[0], split[1]);
        }
        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {

    }
}
```



> 5、将MyLocaleResolver注入到ioc容器中

mvc配置

```java
@Configuration
public class MyMvcConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/index.html").setViewName("index");
    }

    // 方法名必须为localeResolver，否则不被注入成功
    @Bean
    public MyLocaleResolver localeResolver(){
        return new MyLocaleResolver();
    }
}
```



## 六、配合jdbc

spring默认使用`hikari`数据源

> 1、导入两个依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>

<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
</dependency>
```

> 2、配置

```yaml
spring:
  # jdbc
  datasource:
    password: 113011
    username: root
    url: jdbc:mysql://localhost:3306/mybatis?useUnicode=true&serverTimezone=UTC&characterEncoding=utf-8
    driver-class-name: com.mysql.cj.jdbc.Driver
```

有配置，就有对应的properties类，configration类



> 3、使用

```java
@Controller
public class HelloController {

    /**
     * springBoot 中有很多 xxxTemplate
     * 都是封装好了的模板类，拿来即用
     */
    JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @GetMapping("/info")
    @ResponseBody
    public List<Map<String, Object>> info(){
        String sql = "select * from mybatis.person";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);
        return maps;
    }
}
```



## 七、整合druid

> 1、导入依赖

```xml
<!-- https://mvnrepository.com/artifact/com.alibaba/druid -->
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>druid</artifactId>
    <version>1.1.23</version>
</dependency>
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.17</version>
</dependency>
```



> 2、配置

```yaml
spring:
  # jdbc
  datasource:
    password: 123456
    username: root
    url: jdbc:mysql://localhost:3306/mybatis?useUnicode=true&serverTimezone=UTC&characterEncoding=utf-8
    driver-class-name: com.mysql.cj.jdbc.Driver
    type: com.alibaba.druid.pool.DruidDataSource

    #Spring Boot 默认是不注入这些属性值的，需要自己绑定
    #druid 数据源专有配置
    initialSize: 5
    minIdle: 5
    maxActive: 20
    maxWait: 100
    timeBetweenEvictionRunsMillis: 1000
    minEvictableIdleTimeMillis: 10000
    validationQuery: SELECT 1 FROM DUAL
    testWhileIdle: true
    testOnBorrow: false
    testOnReturn: false
    poolPreparedStatements: true

    #配置监控统计拦截的filters，stat:监控统计、log4j：日志记录、wall：防御sql注入
    #如果允许时报错  java.lang.ClassNotFoundException: org.apache.log4j.Priority
    #则导入 log4j 依赖即可，Maven 地址：https://mvnrepository.com/artifact/log4j/log4j
    filters: stat,wall,log4j
    maxPoolPreparedStatementPerConnectionSize: 20
    useGlobalDataSourceStat: true
    connectionProperties: druid.stat.mergeSql=true;druid.stat.slowSqlMillis=500
```

| 属性                                     | 说明                                                         | 建议值                                        |
| :--------------------------------------- | :----------------------------------------------------------- | :-------------------------------------------- |
| url                                      | 数据库的jdbc连接地址。一般为连接oracle/mysql。示例如下：     |                                               |
|                                          | mysql : jdbc:mysql://ip:port/dbname?option1&option2&…        |                                               |
|                                          | oracle : jdbc:oracle:thin:@ip:port:oracle_sid                |                                               |
| username                                 | 登录数据库的用户名                                           |                                               |
| password                                 | 登录数据库的用户密码                                         |                                               |
| initialSize                              | 启动程序时，在连接池中初始化多少个连接                       | 10-50已足够                                   |
| maxActive                                | 连接池中最多支持多少个活动会话                               |                                               |
| maxWait                                  | 程序向连接池中请求连接时,超过maxWait的值后，认为本次请求失败，即连接池没有可用连接，单位毫秒，设置-1时表示无限等待 | 100                                           |
| minEvictableIdleTimeMillis               | 池中某个连接的空闲时长达到 N 毫秒后, 连接池在下次检查空闲连接时，将回收该连接,要小于防火墙超时设置net.netfilter.nf_conntrack_tcp_timeout_established的设置 | 见说明部分                                    |
| timeBetweenEvictionRunsMillis            | 检查空闲连接的频率，单位毫秒, 非正整数时表示不进行检查       |                                               |
| keepAlive                                | 程序没有close连接且空闲时长超过 minEvictableIdleTimeMillis,则会执行validationQuery指定的SQL,以保证该程序连接不会池kill掉,其范围不超过minIdle指定的连接个数。 | true                                          |
| minIdle                                  | 回收空闲连接时，将保证至少有minIdle个连接.                   | 与initialSize相同                             |
| removeAbandoned                          | 要求程序从池中get到连接后, N 秒后必须close,否则druid 会强制回收该连接,不管该连接中是活动还是空闲, 以防止进程不会进行close而霸占连接。 | false,当发现程序有未正常close连接时设置为true |
| removeAbandonedTimeout                   | 设置druid 强制回收连接的时限，当程序从池中get到连接开始算起，超过此值后，druid将强制回收该连接，单位秒。 | 应大于业务运行最长时间                        |
| logAbandoned                             | 当druid强制回收连接后，是否将stack trace 记录到日志中        | true                                          |
| testWhileIdle                            | 当程序请求连接，池在分配连接时，是否先检查该连接是否有效。(高效) | true                                          |
| validationQuery                          | 检查池中的连接是否仍可用的 SQL 语句,drui会连接到数据库执行该SQL, 如果正常返回，则表示连接可用，否则表示连接不可用 |                                               |
| testOnBorrow                             | 程序 **申请** 连接时,进行连接有效性检查（低效，影响性能）    | false                                         |
| testOnReturn                             | 程序 **返还** 连接时,进行连接有效性检查（低效，影响性能）    | false                                         |
| poolPreparedStatements                   | 缓存通过以下两个方法发起的SQL:                               | true                                          |
|                                          | public PreparedStatement prepareStatement(String sql)        |                                               |
|                                          | public PreparedStatement prepareStatement(String sql,        |                                               |
|                                          | int resultSetType, int resultSetConcurrency)                 |                                               |
| maxPoolPrepareStatementPerConnectionSize | 每个连接最多缓存多少个SQL                                    | 20                                            |
| filters                                  | 这里配置的是插件,常用的插件有:                               | stat,wall,slf4j                               |
|                                          | 监控统计: filter:stat                                        |                                               |
|                                          | 日志监控: filter:log4j 或者 slf4j                            |                                               |
|                                          | 防御SQL注入: filter:wall                                     |                                               |
| connectProperties                        | 连接属性。比如设置一些连接池统计方面的配置。                 |                                               |
|                                          | druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000       |                                               |
|                                          | 比如设置一些数据库连接属性:                                  |                                               |



> 3、监控

```java
@Configuration
public class DruidConfig {

    /**
     * 将配置文件中的所有属性与DruidDataSource类的属性绑定，会自动识别
     * @return
     */
    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource druidDataSource(){
        return new DruidDataSource();
    }

    /**
     * ServletRegistrationBean 是spring中的类
     * 由于springboot集成了tomcat，如果需要将bean配置到web.xml中
     * 就得使用ServletRegistrationBean这个类去配置，并将返回的类型，注入到ioc容器中
     * @return
     */
    @Bean
    public ServletRegistrationBean statViewServlet(){
        // 注册servlet的bean，并给定固定访问的url
        // 如同 <servlet-class>StatViewServlet></servlet-class>
        // 如同 <servlet-mapping><url-pattern>/druid/*</url-pattern></servlet-mapping>
        ServletRegistrationBean<StatViewServlet> bean = new ServletRegistrationBean<>(new StatViewServlet(),"/druid/*");
        
        // setInitParameters 如同<servlet><init-param></init-param></servlet>
        
        HashMap<String, String> initParameters = new HashMap<>();
        // 给bean注入参数
        // 登录账户密码 key值是固定的
        initParameters.put("loginUsername","admin");
        initParameters.put("loginPassword","123456");
        // 允许谁可访问
        initParameters.put("allow","");
        // 禁止访问： initParameters.put("admin","127.0.0.1");
        
        bean.setInitParameters(initParameters);
        return bean;
    }
    
    /**
     * 配置druid的过滤器到web.xml中的filter过滤器
     * @return
     */
    @Bean
    public FilterRegistrationBean webStatFilter(){
        FilterRegistrationBean<WebStatFilter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new WebStatFilter());
        HashMap<String, String> initParameters = new HashMap<>();
        // 以下请求，不被统计到druid中
        initParameters.put("exclusions","*.js,*.css,/druid/*");
        bean.setInitParameters(initParameters);
        return bean;
    }
}
```

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringBoot/druid监控.png" style="float:left" />



## 八、整合mybatis

> 1、导入依赖

```xml
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.1.3</version>
</dependency>
```

> 2、配置

```yaml
#mybatis配置
mybatis:
  type-aliases-package: com.heroc.sbtest.pojo
  mapper-locations: classpath:mybatis/mapper/*.xml
  configuration:
    map-underscore-to-camel-case: true
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
```

> 3、使用mybatis

使用与ssm一样使用方式



###  . 事务管理

springboot默认开启的是cglib事务。

使用注解`@Transactional(rollbackFor = Exception.class)`即可事务控制。



更改为jdk事务，配置文件：

```properties
spring.aop.proxy-target-class=false
```





## 九、安全

应该在设计之初就应该考虑安全问题。



### 1、SpringSecurity

> 导入依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
```

> 写配置文件

```java
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 授权
     * 根据http的接口，设置可访问的权限
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/").permitAll()
                .antMatchers("/level1/*").hasRole("vip1")
                .antMatchers("/level2/*").hasRole("vip2")
                .antMatchers("/level3/*").hasRole("vip3");

        /**
         * 进入登录页，登录请求
         * loginPage指定登录页
         * loginProcessingUrl 指定发送的登录表单请求
         * usernameParameter 从请求中获取属性名为user的用户名
         * passwordParameter 从请求中获取属性名为pwd的密码
         * */
        http.formLogin().loginPage("/toLogin").loginProcessingUrl("/usr/login").usernameParameter("user").passwordParameter("pwd");

        /** 由于防止攻击，登出都是post请求，如果使用get请求登出，会被csrf拦截
         * 这里设置csrf关闭使用，spring默认开启
         * */
        http.csrf().disable();
        /** 注销登录，默认发送/logout请求，登出成功跳转到/ */
        http.logout().logoutSuccessUrl("/");

        /**
         * 记住我 功能，将随机生成的token存入了cookie，默认保存2周
         * rememberMeParameter 从请求中获取属性名为remember的值
         * */
        http.rememberMe().rememberMeParameter("remember");
    }

    /**
     * 认证
     * 根据登录，认证，设置角色
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        /** inMemoryAuthentication 表示从内存中获取. 密码必须加密 */
        auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder())
                .withUser("root").password(new BCryptPasswordEncoder().encode("123456")).roles("vip1","vip2","vip3")
                .and()
                .withUser("heroc").password(new BCryptPasswordEncoder().encode("123456")).roles("vip2","vip3");
    }
}
```



#### 1）thymeleaf整合security

> 导入依赖包

```xml
<dependency>
    <groupId>org.thymeleaf.extras</groupId>
    <artifactId>thymeleaf-extras-springsecurity5</artifactId>
</dependency>
```

> 在html中引入命名空间

```html
xmlns:sec="http://www.thymeleaf.org/extras/spring-security"
```

> 语法

```html
sec:authorize="!isAuthenticated()" // 判断是否已认证，已认证返回true

sec:authorize="hasRole('vip1') // 获取角色，是vip1角色就返回true

sec:authentication="name" // 获取认证的用户名
```





### 2、Shiro

> 导入依赖

```xml
<dependency>
    <groupId>org.apache.shiro</groupId>
    <artifactId>shiro-spring</artifactId>
    <version>1.5.3</version>
</dependency>
```

> shiroConfig

```java
@Configuration
public class ShiroConfig{

    // ShiroFilterFactoryBean
    @Bean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("securityManager") DefaultWebSecurityManager securityManager){
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        // 设置安全管理
        shiroFilterFactoryBean.setSecurityManager(securityManager);

        /**
         * shiro内置过滤器
         * 认证过滤器：
         * anon: 无需认证就可以访问
         * authc: 必须认证才能访问
         * user: 必须拥有 记住我 功能才能用
         * 授权过滤器：
         * perms：拥有对某个资源的权限才能访问
         * roles：拥有某个角色权限才能访问
         *
         * 通常可将这些过滤器分为两组
         * anon,authc,authcBasic,user是第一组认证过滤器
         * perms,port,rest,roles,ssl是第二组授权过滤器
         * 注意user和authc不同：当应用开启了rememberMe时,用户下次访问时可以是一个user，但绝不会是authc,因为authc是需要重新认证的
         * user表示用户不一定已通过认证,只要曾被Shiro记住过登录状态的用户就可以正常发起请求,比如rememberMe 说白了,以前的一个用户登录时开启了rememberMe,然后他关闭浏览器,下次再访问时他就是一个user,而不会authc
         *
         */
        // 设置请求需要什么条件，才能被访问
        LinkedHashMap<String, String> filterMap = new LinkedHashMap<>();
        /**
         * 如果时需要某个资源权限才能访问的话，会进入realm的授权方法，验证授权
         */
        filterMap.put("/user/add","perms[user:add]");
        filterMap.put("/user/update","perms[user:update]");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterMap);

        // 以上请求被拦截，则跳转到登录页面
        shiroFilterFactoryBean.setLoginUrl("/toLogin");

        // 登录之后，点击没有权限访问的页面时，会转到该请求
        shiroFilterFactoryBean.setUnauthorizedUrl("/unauth");

        return shiroFilterFactoryBean;
    }

    // DefaultWebSecurityManager
    @Bean(name = "securityManager")
    public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("userRealm") Realm realm){
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        defaultWebSecurityManager.setRealm(realm);
        return defaultWebSecurityManager;
    }

    // realm
    @Bean
    public UserRealm userRealm(){
        return new UserRealm();
    }
}
```

> UserRealm

```java
public class UserRealm extends AuthorizingRealm {
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    // 授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        // 获取授权实例
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        // 获取当前用户
        Subject subject = SecurityUtils.getSubject();
        // 从当前用户中获取重要的信息，这个信息是认证的时候从数据库中查询得到的
        User currentUser = (User)subject.getPrincipal();
        // 从信息中获取权限，给当前用户赋值权限
        info.addStringPermission(currentUser.getPerms());
        return info;
    }

    // 认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        // 将authenticationToken强制转换为UsernamePasswordToken
        UsernamePasswordToken userToken = (UsernamePasswordToken)authenticationToken;
        // 从数据库中获取用户信息
        User user = userService.getUser(userToken.getUsername());
        // 匹配用户名是否正确
        if (!userToken.getUsername().equals(user.getName())){
            // 会抛出异常
            return null;
        }

        // 密码认证的SimpleAuthenticationInfo实现类
        // SimpleAuthenticationInfo(principal用于方法之间传递数据，从数据库获取的密码与token中的密码进行比对，realm的名字)
        return new SimpleAuthenticationInfo(user,user.getPwd(),"");
    }
}
```

> Controller

```java
@RequestMapping("/login")
public String login(@RequestParam("username") String username, @RequestParam("password") String pwd, Model model){
    Subject subject = SecurityUtils.getSubject();
    // shiro的md5Hash加密方式对密码加密
    Md5Hash md5Hash = new Md5Hash(pwd, username, 2);
    UsernamePasswordToken token = new UsernamePasswordToken(username, md5Hash.toString());
    try {
        // 获取到shiro的token之后，就调用login，进行shiro的realm的一系列操作，比如认证，授权等
        subject.login(token);
        model.addAttribute("msg","true");
        return "index";
    }catch (UnknownAccountException e){
        model.addAttribute("msg","用户名不存在");
        return "login";
    }catch (IncorrectCredentialsException e){
        model.addAttribute("msg","密码不正确");
        return "login";
    }
}

@RequestMapping("/unauth")
@ResponseBody
public String unauth(){
    return "未授权，无法访问";
}

@RequestMapping("/logout")
public String logOut(Model model){
    Subject currentUser = SecurityUtils.getSubject();
    // 对当前用户进行退出登录操作
    currentUser.logout();
    model.addAttribute("msg","false");
    return "index";
}
```



#### 1）thymeleaf整合shiro

> 导入依赖

```xml
<dependency>
    <groupId>com.github.theborakompanioni</groupId>
    <artifactId>thymeleaf-extras-shiro</artifactId>
    <version>2.0.0</version>
</dependency>
```



> 注入bean

```java
@Bean
public ShiroDialect shiroDialect(){
    return new ShiroDialect();
}
```





> html导入命名空间

```html
xmlns:shiro="http://www.pollix.at/thymeleaf/shiro"
```



> thymeleaf页面权限控制

```html
<html lang="zh_CN" xmlns:th="http://www.thymeleaf.org"
	  xmlns:shiro="http://www.pollix.at/thymeleaf/shiro">
	  
//作为属性控制
<button  type="button" shiro:authenticated="true" class="btn btn-outline btn-default">
	<i class="glyphicon glyphicon-plus" aria-hidden="true"></i>
</button>
//作为标签
<shiro:hasRole name="admin">
	<button type="button" class="btn btn-outline btn-default">
		<i class="glyphicon glyphicon-heart" aria-hidden="true"></i>
	</button>
</shiro:hasRole>
```

标签说明

```html
guest标签
　　<shiro:guest>
　　</shiro:guest>
　　用户没有身份验证时显示相应信息，即游客访问信息。

user标签
　　<shiro:user>　　
　　</shiro:user>
　　用户已经身份验证/记住我登录后显示相应的信息。

authenticated标签
　　<shiro:authenticated>　　
　　</shiro:authenticated>
　　用户已经身份验证通过，即Subject.login登录成功，不是记住我登录的。

notAuthenticated标签
　　<shiro:notAuthenticated>
　　
　　</shiro:notAuthenticated>
　　用户已经身份验证通过，即没有调用Subject.login进行登录，包括记住我自动登录的也属于未进行身份验证。

principal标签
　　<shiro: principal/>
　　
　　<shiro:principal property="username"/>
　　相当于((User)Subject.getPrincipals()).getUsername()。

lacksPermission标签
　　<shiro:lacksPermission name="org:create">
　
　　</shiro:lacksPermission>
　　如果当前Subject没有权限将显示body体内容。

hasRole标签
　　<shiro:hasRole name="admin">　　
　　</shiro:hasRole>
　　如果当前Subject有角色将显示body体内容。

hasAnyRoles标签
　　<shiro:hasAnyRoles name="admin,user">
　　　
　　</shiro:hasAnyRoles>
　　如果当前Subject有任意一个角色（或的关系）将显示body体内容。

lacksRole标签
　　<shiro:lacksRole name="abc">　　
　　</shiro:lacksRole>
　　如果当前Subject没有角色将显示body体内容。

hasPermission标签
　　<shiro:hasPermission name="user:create">　　
　　</shiro:hasPermission>
　　如果当前Subject有权限将显示body体内容
```



## 十、异步任务、定时任务

异步任务就是，在执行一个操作时，调用多个线程，不会等待这些线程执行完成，就直接完成该操作。可用于发邮件等业务上。



1、在主方法中，开启异步任务，添加使用异步的注解

```java
@EnableAsync
```

2、在需要使用异步操作的方法上添加注解

```java
@Async
```

**eg：**

application

```java
@EnableAsync // 开启异步任务
@SpringBootApplication
public class ShiroSbootApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShiroSbootApplication.class, args);
    }
}
```

service

```java
@Service
public class AsyncService {

    // 该注解，告诉spring，这是一个异步操作
    @Async
    public void hello(){
        try {
            TimeUnit.SECONDS.sleep(5L);
            System.out.println("执行完，异步操作...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```

controller

```java
@Autowired
private AsyncService asyncService;

// 不会等待线程执行完成，才执行下面的语句
@RequestMapping("/async")
@ResponseBody
public String async(){
    asyncService.hello();
    return "OK";
}
```



### 1）邮件发送

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-mail</artifactId>
</dependency>
```

```yaml
spring:
  mail:
    username: turbochang@foxmail.com
    password: kyubzxailpmqbceg
    host: smtp.qq.com
    # qq邮箱需要一个ssl加密协议
    properties:
      mail.smtp.ssl.enable: true
```

```java
@Autowired
private JavaMailSender javaMailSender; 

// 发送简单邮件
@Test
void sendSimpleMail(){
    SimpleMailMessage message = new SimpleMailMessage();
    message.setTo("turbochang@foxmail.com");
    message.setFrom("turbochang@foxmail.com");
    message.setText("简单邮件发送测试....");
    message.setSubject("测试");
    javaMailSender.send(message);
}

// 发送复杂邮件
@Test
void sendMMail() throws MessagingException {
    MimeMessage message = javaMailSender.createMimeMessage();

    MimeMessageHelper helper = new MimeMessageHelper(message);

    helper.setTo("turbochang@foxmail.com");
    helper.setFrom("turbochang@foxmail.com");
    helper.setSubject("复杂邮件测试");
    helper.setText("<h4>复杂邮件发送测试，本次验证码为：</h4><h2 style='color:red'>445670</h2>",true);
    // 添加附件：
    //helper.addAttachment("1.jpg",new File());

    javaMailSender.send(message);
}
```



### 2）定时任务、cron表达式

在springboot入口处开启定时任务

```xml
@EnableScheduling // 开启定时任务
```

在需要定时执行的方法上面加上，注解：

```java
@Scheduled(cron = "0 0/30 10,15 15 * ?")
// spring不支持L符号，不支持年份

0 0/30 10,15 L * ? 2020-2025
// 表示的是，在2020年到2025年期间，每个月的最后一天的10点和15点每隔30分钟执行一次代码
// 2020-1-31 10:00:00
// 2020-1-31 10:30:00
// 2020-1-31 15:00:00
// 2020-1-31 15:30:00
// ...
// 2025-12-31 10:00:00
// 2025-12-31 10:30:00
// 2025-12-31 15:00:00
// 2025-12-31 15:30:00
```

cron表达式：

*Seconds Minutes Hours DayofMonth Month DayofWeek*

> 每一个域可出现的字符如下：
> Seconds:可出现", - * /"四个字符，有效范围为0-59的整数
> Minutes:可出现", - * /"四个字符，有效范围为0-59的整数
> Hours:可出现", - * /"四个字符，有效范围为0-23的整数
> DayofMonth:可出现", - * / ? L W C"八个字符，有效范围为0-31的整数
> Month:可出现", - * /"四个字符，有效范围为1-12的整数或JAN-DEc
> DayofWeek:可出现", - * / ? L C #"四个字符，有效范围为1-7的整数或SUN-SAT两个范围。1表示星期天，2表示星期一， 依次类推
> Year:可出现", - * /"四个字符，有效范围为1970-2099年
>
> 每一个域都使用数字，但还可以出现如下特殊字符，它们的含义是：
> (1)*：表示匹配该域的任意值，假如在Minutes域使用*, 即表示每分钟都会触发事件。
>
> (2)?:只能用在DayofMonth和DayofWeek两个域。它也匹配域的任意值，但实际不会。因为DayofMonth和 DayofWeek会相互影响。例如想在每月的20日触发调度，不管20日到底是星期几，则只能使用如下写法： 13 13 15 20 * ?, 其中最后一位只能用？，而不能使用*，如果使用*表示不管星期几都会触发，实际上并不是这样。
>
> (3)-:表示范围，例如在Minutes域使用5-20，表示从5分到20分钟每分钟触发一次
>
> (4)/：表示起始时间开始触发，然后每隔固定时间触发一次，例如在Minutes域使用5/20,则意味着5分钟触发一次，而25，45等分别触发一次.
>
> (5),:表示列出枚举值值。例如：在Minutes域使用5,20，则意味着在5和20分每分钟触发一次。
>
> (6)L:表示最后，只能出现在DayofWeek和DayofMonth域，如果在DayofWeek域使用5L,意味着在最后的一个星期四触发。
>
> (7)W: 表示有效工作日(周一到周五),只能出现在DayofMonth域，系统将在离指定日期的最近的有效工作日触发事件。例如：在 DayofMonth使用5W，如果5日是星期六，则将在最近的工作日：星期五，即4日触发。如果5日是星期天，则在6日(周一)触发；如果5日在星期一 到星期五中的一天，则就在5日触发。另外一点，W的最近寻找不会跨过月份
>
> (8)LW:这两个字符可以连用，表示在某个月最后一个工作日，即最后一个星期五。
>
> (9)#:用于确定每个月第几个星期几，只能出现在DayofMonth域。例如在4#2，表示某月的第二个星期三。
>
> 举几个例子:
> 0 0 2 1 * ? * 表示在每月的1日的凌晨2点调度任务
> 0 15 10 ? * MON-FRI 表示周一到周五每天上午10：15执行作业
> 0 15 10 ? 6L 2002-2006 表示2002-2006年的每个月的最后一个星期五上午10:15执行作
>
> 一个cron表达式有至少6个（也可能7个）有空格分隔的时间元素。
> 按顺序依次为
> 秒（0~59）
> 分钟（0~59）
> 小时（0~23）
> 天（月）（0~31，但是你需要考虑你月的天数）
> 月（0~11）
> 天（星期）（1~7 1=SUN 或 SUN，MON，TUE，WED，THU，FRI，SAT）
> 年份（1970－2099）



## 十一、整合redis

在springboot2.x之后使用的是lettuce依赖，与redis连接。jedis被替换。



### 1）Jedis与Lettuce的区别

**Jedis** 在实现上是直接连接的redis server，如果在多线程环境下是非线程安全的，这个时候只有使用JedisPool连接池，为每个Jedis实例增加物理连接。

**Lettuce** 的连接是基于Netty的，连接实例（StatefulRedisConnection）可以在多个线程间并发访问，应为StatefulRedisConnection是线程安全的，所以一个连接实例（StatefulRedisConnection）就可以满足多线程环境下的并发访问，当然这个也是可伸缩的设计，一个连接实例不够的情况也可以按需增加连接实例，也可以配置LettucePool。



### 2）配置redis

yaml

```yaml
spring:
  redis:
    host: localhost
    password: 123456
    port: 6379
```



RedisConfig

```java
@Configuration
public class RedisConfig {

    /**
     * 从RedisAutoConfiguration配置中，重写RedisTemplate配置，
     * 可以覆盖原来的RedisTemplate配置（固定写法，可直接使用）
     * @param redisConnectionFactory
     * @return
     * @throws UnknownHostException
     */
    @Bean(name = "redisTemplateCustom")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory)
            throws UnknownHostException {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);

        // jackson序列化配置，spring默认使用的是jdk序列化，因此需要重写配置使用jackson进行序列化
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper objectMapper = new ObjectMapper();
        // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // 指定序列化输入的类型，类必须是非final修饰的，final修饰的类，比如String,Integer等会跑出异常
        objectMapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);

        // String的序列化
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();

        // key、hashKey都采用string的序列方式
        template.setKeySerializer(stringRedisSerializer);
        template.setHashKeySerializer(stringRedisSerializer);
        // value、hashValue都采用jackson序列方式
        template.setValueSerializer(jackson2JsonRedisSerializer);
        template.setHashValueSerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();

        return template;
    }
}
```



### 3）数据库的切换

```java
LettuceConnectionFactory connectionFactory = (LettuceConnectionFactory)redisTemplate.getConnectionFactory();
connectionFactory.setDatabase(0);
connectionFactory.afterPropertiesSet();
redisTemplate.opsForValue().set("key0","test");
```



## 十二、全局异常捕获

```java
@RestControllerAdvice
public class GlobalExceptionHandler {
    /**
     * 全局异常捕获处理，Controller层中抛出的异常都会被捕获
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    public CommonResult handlerException(Exception e){
        return new CommonResult(500,"服务异常，请稍后重试..."+e.getMessage());
    }


    /**
     * 应用到所有@RequestMapping注解方法，在其执行之前初始化数据绑定器
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {}

    /**
     * 把值绑定到Model中，使全局@RequestMapping可以获取到该值
     * @param model
     */
    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute("author", "Magical Sam");
    }

}
```

