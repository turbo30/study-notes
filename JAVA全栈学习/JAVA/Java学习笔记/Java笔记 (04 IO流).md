# JAVA IO流

Java中I/O操作主要是指使用`java.io`包下的内容，进行输入、输出操作。**输入**也叫做**读取**数据，**输出**也叫做**写出**数据。

根据数据的流向分为：输入流和输出流

- **输入流**：把数据从`其他设备`上读取到`内存`中的流。
- **输出流**：把数据从`内存`中写出到`其他设备`上的流。

格局数据的类型分为：**字节流**和**字符流**。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/IO流.png)

输出流的原理(内存 --> 硬盘)：

java程序 --> JVM --> OS(操作系统) --> OS调用写数据的方法 --> 把数据写到硬盘的文件中

输入流的原理(硬盘 --> 内存)：

java程序 --> JVM --> OS(操作系统) --> OS调用读数据的方法 --> 把数据从硬盘文件中读取到内存中



## 一、字节流

一切都为字节，任何文件都是以二进制数字形式保存。在传输过程中都是以二进制数据形式传输。



### 字节输出流



### 1、OutputStream 抽象类 JDK 1.0

`java.io.OutputStream `这个抽象类是表示字节输出流的所有类的超类。 输出流接收输出字节并将其发送到某个接收器。它定义了字节输出流的基本共性功能方法。

已知直接子类：

 `ByteArrayOutputStream`，`FileOutputStream`  ， `FilterOutputStream` ， `ObjectOutputStream` ，`OutputStream`， `PipedOutputStream`

#### 常用方法

- `public void close()` 关闭此输出流并释放与此流相关的任何系统资源
- `public void flush()` 刷新此输出流并强制任何缓冲的输出字节被写出
- `public void write(byte[] b)` 将b.length字节从指定的字节数组写入此输出流
- `public void write(byte[] b, int off, int len)` 从指定的字节数组写入 len节字，从偏移量off开始输出到此输出流
- `public abstract void write(int b)` 将指定的字节输出流，只能传入一个字节大小的十进制整数。

> 注意：close方法必须在完成流之后调用，以此释放相关的系统资源。



### 2、FileOutputStream 类

该类是继承`OutputStream`抽象类的子类

`java.io.FileOutputStream`  类是文件输出流，将内存中的数据写到硬盘中的文件中。

异常：

`IOException`

#### 构造方法( 共5个 )

- `public FileOutputStream(File file) throws FileNotFoundException` 创建文件输出流以写入由指定的 `File`对象表示的文件。(如原来已经有该文件，会被覆盖重写新的数据)
- `public FileOutputStream(String name)throws FileNotFoundException` 创建文件输出流以指定的名称写入文件。(name为文件的路径)。(如原来已经有该文件，会被覆盖重写新的数据)
- `public FileOutputStream(File file,boolean append) throws FileNotFoundException` JDK1.4开始，向文件中写入数据，append为true就是在该文件的文末开始追加新的数据；为false会创建新文件覆盖原文件，并开始写入新数据。(文件不存在，则会自动创建)
- `public FileOutputStream(String name,boolean append) throws FileNotFoundException ` JDK1.1开始，向文件中写入数据，append为true就是在该文件的文末开始追加新的数据；为false会创建新文件覆盖原文件，并开始写入新数据。(文件不存在，则会自动创建)

```java
public class FileOutputStreamConstructor{
    public static void main(String[] args) throws IOException{
        // 以File对象创建输出流
        File file = new File("a.txt");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        
        // 以文件名称创建输出流 当前文件的output文件夹下的a.txt文件
        FileOutputStream fileOutputStream = new FileOutputStream("output/a.txt");
        fileOutputStream.close();
    }
}
```



#### 写出字节流

1. 写出字节：`write(int b)` 方法，每次可以写出一个字节数据  `IOException`

```java
public class DemonFileOutputStream{
    public static void main(String[] args) throws IOException{
        FileOutputStream fileOutputStream = new FileOutputStream("output/a.txt");
        fileOutputStream.write(97); // 像文件中写入的是小写字母a
        fileOutputStream.close();
    }
}
```

**字节流写入文件的原理：**向a.txt文件写入了97，在程序运行的时候，因为是字节流，会将97转为二进制整数1100001写入到a.txt文件中，当打开a.txt文件时，这些文本编辑器，会自动将写入的二进制整数1100001通过编码解析。默认解析规则为：0-127的十进制整数会比对ASCII表，其他值会以系统默认码表查询。这里二进制转换为十进制数为97，则97在ASII表中对应的是小写字母a。那么打开a.txt显示的是 a 。

> ASCII表
>
> 48为0	以此为0~9	9为57
>
> 65为A	以此为A~Z（26个字母）Z为90
>
> 97为a	以此为a~z（26个字母）z为122



当字节十进制在0-127时，会解析ASCII表。当第一个为**负数**，第二个为整数或负数，则会组合在一起，形成2个字节解析为中文。需要注意的是**UTF-8编码一个中文字符占3个字节，GBK编码一个中文占2个字节**。

```java
public class DemonFileOutputStream{
    public static void main(String[] args) throws IOException{
        FileOutputStream fileOutputStream = new FileOutputStream("output/a.txt");
// write(byte[] b)
        byte[] a = {65,67,68,69};
        fileOutputStream.write(a); // 查看a.txt文件写入的是 ACDE
        byte[] b = {-65,-66,-67,68};
        fileOutputStream.write(b); // 查看a.txt文件写入的是 烤紻
        
// write(byte[] b, int off, int len)
        fileOutputStream.write(a, 1, 2); // 查看a.txt文件写入的是 CD
        // 写的是字节数组a的索引为1开始的2个长度的数据 67,68
        
// 把字符串转换成字节数组
        String str = "你好";
        byte[] c = str.getBytes();
        System.out.println(Arrays.toString(byte)); // [-28, -67, -96, -27, -91, -67]
        // 需要注意的是UTF-8编码一个中文字符占3个字节，GBK编码一个中文占2个字节
        fileOutputStream.write(c); // 就会把字符串以字节流的方式写入到a.txt文件中
    }
}
```



#### 数据追加续写与换行

构造方法：

- `public FileOutputStream(File file,boolean append) throws FileNotFoundException` JDK1.4开始，向文件中写入数据，append为true就是在该文件的文末开始追加新的数据；为false会创建新文件覆盖原文件，并开始写入新数据。(文件不存在，则会自动创建)
- `public FileOutputStream(String name,boolean append) throws FileNotFoundException ` JDK1.1开始，向文件中写入数据，append为true就是在该文件的文末开始追加新的数据；为false会创建新文件覆盖原文件，并开始写入新数据。(文件不存在，则会自动创建)

写入换行字符串：

- win：\r\n
- mac：/r
- Linux：/n

```java
public class FileOutputStreamTest {
    public static void main(String[] args) throws IOException {

        FileOutputStream fileOutputStream = new FileOutputStream("b.txt",true);
        for (int i = 0; i < 10; i++) {
            fileOutputStream.write("你好".getBytes());
            fileOutputStream.write("\r\n".getBytes()); // win 下的换行符
        }

        fileOutputStream.close();
    }
}
```



### 字节输入流



### 1、InputStream 抽象类 JDK 1.0

`java.io.InputStream `这个抽象类是表示字节输出流的所有类的超类。 输出流接收输出字节并将其发送到某个接收器。它定义了字节输出流的基本共性功能方法。

已知直接子类： 

`AudioInputStream`， `ByteArrayInputStream` ，` FileInputStream`，  `FilterInputStream`，`InputStream`， `ObjectInputStream`， `PipedInputStream` ， `SequenceInputStream`，`StringBufferInputStream`

#### 常用方法

- `public void close()` 关闭此输入流并释放与此流相关的系统资源
- `public abstract int read()` 从输入流中读取数据的下一个字节。
- `public int read(byte[] b)` 从该输入流读取最多 `byte.length`个字节的数据到字节数组。 返回读取文件中有效字节个数，并且会将有效字节存放在byte数组中。如果读到文末，则返回-1
- `public int read(byte[] b, int off, int len)` 从该输入流读取从off索引开始到最多 `len`字节的数据到字节数组。如果读到文末，则返回-1

>  注意：close方法必须在完成流之后调用，以此释放相关的系统资源。



### 2、FileInputStream 类

该类是继承`InputStream`抽象类的子类

`java.io.FileInputStream` 类是文件输入流，从文件中读取数据。

异常：

`FileNotFoundException`

#### 构造方法

- `public FileInputStream(File file) throws FileNotFoundException` 通过打开实际文件的连接来创建一个FileInputStream，该文件由文件系统中的File对象file命名。

- `public FileInputStream(String name) throws FileNotFoundException` 通过打开实际文件的连接来创建一个FileInputStream，该文件由文件系统中的路径名name命名。

  

> `public int read(byte[] b)` 中byte[]数组的作用，以及返回值是什么？
>
> 起到缓冲作用，返回值为读取字节数的有效个数，当读到文末时，没有有效字节数了就会返回-1。一般数组的长度定义为1024(1kb)
>
> byte[] bytes = new byte[1024];



#### 读取字节流

```java
public class FileInputStreamTest {
    public static void main(String[] args) throws IOException {
        InputStream inputStream = new FileInputStream("a.txt");

        // read() 只读取一个字节的数据并返回该数据，如果读到文末，则返回-1
        /*int read = 0;
        while ( (read = inputStream.read()) != -1){
            // read = inputStream.read(); // 读取了一个字节，指针会自动向后移一位
                System.out.print( read+":" );
                System.out.print((char) read +" ");
        }*/

        int read1 = 0;
        byte[] bytes = new byte[1024];
        while ((read1 = inputStream.read(bytes)) != -1){
            // System.out.println(read1); // 输出读取有效字节个数
            // System.out.println(Arrays.toString(bytes)); // 输出由二进制数据转换成十进制的数
            // System.out.println(new String(bytes)); // 使用String的该构造方法，会将1024个字节全部输出，空格也会输出
            System.out.println(new String(bytes,0, read1)); // 该方法就是从字节数组索引为0开始到有效个数的长度截至，转为字符
        }

        inputStream.close();

    }
}

```



### 练习：文件复制

可复制任何文件

```java
public class CopyFile {
    public static void main(String[] args) throws IOException {

        // 1)复制文本
        /*FileInputStream fileInputStream = new FileInputStream("a.txt");
        byte[] bytes = new byte[1024];
        int read = 0;
        while((read = fileInputStream.read(bytes))! = -1){
            FileOutputStream fileOutputStream = new FileOutputStream("b.txt");
            fileOutputStream.write(bytes,0, read);
        }
        fileOutputStream.close();
        fileInputStream.close();*/

        // 2)复制图片
        /*FileInputStream fileInputStream = new FileInputStream("D:/IMG_20191127_181018.jpg");
        FileOutputStream fileOutputStream = new FileOutputStream("E:/IMG.jpg");
        byte[] bytes = new byte[1024];
        int read = 0;
        while ( (read = fileInputStream.read(bytes)) != -1){
            fileOutputStream.write(bytes,0, read);
        }
        fileOutputStream.close();
        fileInputStream.close();*/
        
        // 3)复制音频
        FileInputStream fileInputStream = new FileInputStream("D:\\媒体\\音乐\\勇气.mp3");
        FileOutputStream fileOutputStream = new FileOutputStream("E:/勇气.mp3");
        byte[] bytes = new byte[1024];
        int read = 0;
        while ( (read = fileInputStream.read(bytes)) != -1){
            fileOutputStream.write(bytes,0, read);
        }
        fileOutputStream.close();
        fileInputStream.close();
    }
}
```

注意：

1. 先关闭写文件的数据流，再关闭读取文件的数据流。(因为写数据，已经写完了，读取的数据肯定就已经读完了)
2. 当复制文件或读取写入的文件数据字节数小的话，建议使用单个读取字节的方法；当复制文件或读取写入文件的数据字节数大的话，建议使用new byte[1024]的整数倍。少使用数组，浪费空间，因为数组中每一位都会占去一定的空间。



### Properties 集合 见集合框架

Properties是唯一个跟IO流相关的双列集合



## 二、字符流

当使用字节流读取文本时，可能会有一个问题，就是遇到中文字符时，可能不会显示完整的字符，那是因为一个中文字符可能会占用多个字节存储。所以java提供了一些字符流类，以字符为单位读写数据，专门用于处理文本文件。



### 字符输入流



### 1、Reader 抽象类 JDK 1.1

`java.io.Reader` 用于读取**字符**的抽象类。是字符输入流的顶层父类(超类)。子类必须实现的唯一方法是`read(char []，int off，int len)`和`close()`。无论中英文数字等都可是一个字符。它定义了字节输出流的基本共性功能方法。

已知直接子类： 

`BufferedReader` ，`CharArrayReader`，  `FilterReader`， `InputStreamReader` ， `PipedReader` ， `StringReader`

#### 常用方法

- `public abstract void close() throws IOException` 子类实现类。关闭流并释放与之相关联的任何系统资源。
- `public int read() throws IOException` 读取一个字符。字符读取，作为0到65535（ `0x00-0xffff` ）范围内的整数，如果已经达到流的末尾，则为-1
- `public int read(char[] cbuf) throws IOException` 将字符读入到字符数组中。读取的字符数，如果已经达到流的结尾，则为-1 
- `public abstract int read(char[] cbuf, int off, int len) throws IOException`  子类实现类。读取字符到从字符数组的索引off开始读取len个长度的字符。如果已经达到流的结尾，则为-1



### 2、FileReader 类

`java.io.FileReader` 类继承 `InputStreamReader` (该类见转换流)继承 `Reader` 

该类是阅读字符文件的便利类。 该类的构造函数使用系统默认字符编码和默认字节缓冲区。

> 注意：
>
> 1. 字符编码：字节与字符的对应规则。win10中默认使用的是GBK编码，一中文字符占用2个字节。idea中使用的是UTF-8编码，一中文字符占用3个字节。



#### 构造方法

- `public FileReader(String fileName) throws FileNotFoundException` 创建一个新的 `FileReader` ，给定要读取的文件的名称或路径。
- `public FileReader(File file) throws FileNotFoundException` 创建一个新的 `FileReader` ，给定要读取的类型为File对象。



#### 读取字符流

```java
public class FileReaderTest {
    public static void main(String[] args) throws IOException {
        // 用 read() 方法
        /*FileReader fileReader = new FileReader("a.txt");
        int read = 0;
        while ((read = fileReader.read()) != -1){
            System.out.print(read + ":");
            System.out.print( (char)read + " ");
        }
        fileReader.close();*/

        // 用 read(char[] cbuf) 方法
        FileReader fileReader = new FileReader("a.txt");
        char[] cbuf = new char[1024]; // 存储读取到的字符个数
        int read = 0; // 读取有效字符个数
        while ((read = fileReader.read(cbuf)) != -1){
            //System.out.println(Arrays.toString(cbuf));
            System.out.println(new String(cbuf, 0, read));
        }
        fileReader.close();
    }
}

```



### 字符输出流



### 1、Writer 抽象类 JDK 1.1

`java.io.Writer` 用于从内存中向文件中写入字符流的抽象类。 子类必须实现的唯一方法是write（char  []，int，int），flush（）和close（）。  然而，大多数子类将覆盖这里定义的一些方法，以便提供更高的效率，附加的功能或两者。 

已知直接子类： 

`BufferedWriter` ， `CharArrayWriter`， `FilterWriter` ，`OutputStreamWriter`，`PipedWriter`，`PrintWriter`， `StringWriter`

#### 常用方法

- `public abstract void close() throws IOException` 关闭于字符输出流有关的系统资源。
- `public abstract void flush() throws IOException` 刷新流。将内存中的数据刷新到文件中进行存储。
- `public abstract void write(char[] cbuf,int off,int len) throws IOException` 以字符数组的形式，从字符数组的索引off起len长度截至写到文件中。(抽象方法，用于给子类实现)
- `public void write(char[] cbuf) throws IOException` 将字符数组写入到文件中。
- `public void write(String str) throws IOException` 将字符串写入到文件中。
- `public void write(String str,int off,int len) throws IOException` 以字符数组的形式，从字符数组的索引off起len长度截至写到文件中。



### 2、FileWriter 类

`java.io.FileWriter` 类继承 `OutputStreamWriter`(该类见转换流) 继承 `Writer` 

该类是写入字符文件的便利类。 该类的构造函数使用系统默认字符编码和默认字节缓冲区。

#### 构造方法

- `public FileWriter(File file) throws IOException` 创建文件输出流以写入由指定的 `File`对象表示的文件。(如原来已经有该文件，会被覆盖重写新的数据)
- `public FileWriter(String name)throws IOException` 创建文件输出流以指定的名称写入文件。(name为文件的路径)。(如原来已经有该文件，会被覆盖重写新的数据)
- `public FileWriter(File file,boolean append) throws IOException` JDK1.4开始，向文件中写入数据，append为true就是在该文件的文末开始追加新的数据；为false会创建新文件覆盖原文件，并开始写入新数据。(文件不存在，则会自动创建)
- `public FileWriter(String name,boolean append) throws IOException` JDK1.1开始，向文件中写入数据，append为true就是在该文件的文末开始追加新的数据；为false会创建新文件覆盖原文件，并开始写入新数据。(文件不存在，则会自动创建)



```java
public class FileWriterTest {
    public static void main(String[] args) throws IOException {
        // 如果该文件存在，那么会覆盖原来的文件
        // FileWriter fileWriter = new FileWriter("c.txt"); 

        // 如果为true，有该文件就追加，无该文件，就新建再添加内容
        FileWriter fileWriter = new FileWriter("c.txt",true);
        
        // write(int c)
         fileWriter.write(48);

        // write(char[] cbuf)
        char[] cbuf1 = {'1','1','3','0','你','好'};
        fileWriter.write(cbuf1);

        // write(char[] cbuf, int off, int len)
        char[] cbuf2 = {'1','1','3','0','你','好'};
        fileWriter.write(cbuf2,2,4); // 超出索引会报IndexOutofBoundsException异常

        // 换行
        fileWriter.write("\r\n");
        
        // write(String s)
        fileWriter.write("heroC *** 真厉害");
        
        // write(String s, int off, int len)
        fileWriter.write("笔名heroC",2,5); // 超出索引会报StringIndexOutofBoundsException异常
        
        fileWriter.flush();
        fileWriter.close();
    }
}

```



### flush() 和 close() 的区别

**输出流都有flush方法**

`flush` 会将内存缓冲区的数据，刷新到文件中去，并且输出流还可继续使用；

`close` 先将内存缓冲区的字符刷新到文件中，才会将相关的系统资源释放掉，输出流不可再用。

**注意：**

​	write方法，是将字符写入到内存缓冲区（字符转换为字节的过程）；

​	flush方法，是将内存缓冲区的字符，刷新到文件中；

​	close方法，先将内存缓冲区的字符刷新到文件中，才会将相关的系统资源释放掉。

**如果不调用`flush方法`或者`close方法`，是不会将数据从内存中写入到文件中的。**

**防止io阻塞，一般建议先flush再close**





### <u>字符流实现原理</u>

在我看源码分析得出。

`FileReader`和`FileWriter`在构造方法中都调用父类`InputReader`、`OutputWriter`构造方法并且传入了new的一个字节流，将字节对象传入父类`Reader`、`Writer`抽象类中，进行处理。父类可通过读取到的字节或者要写入的字节，通过父类转换流默认的系统编码UTF-8进行读取解码或编码。

以FileReader为例：

```java
// 1）FileRader构造方法源码
public FileReader(String fileName) throws FileNotFoundException {
        super(new FileInputStream(fileName)); // 调用父类，并传递了输入字节流给父类转换流
    }

// 2）FileInputStream构造方法
public InputStreamReader(InputStream in) {
        super(in); // 转换流将字节流有通过调用父类构造方法并将字节对象传入到该类的父类
        try {
            sd = StreamDecoder.forInputStreamReader(in, this, (String)null); // ## check lock object 在转换流中，对该字节流进行处理解码，默认UTF-8
        } catch (UnsupportedEncodingException e) {
            // The default encoding should always be available
            throw new Error(e);
        }
    }

// 3）Reder抽象类
protected Reader(Object lock) {
        if (lock == null) { 
            throw new NullPointerException();
        }
        this.lock = lock; // 将从子类传递进来的字节流，进行赋值，在其它方法中进行相应处理加工
    }

```



## 三、IO流的异常捕获



### 1、JDK 7之前使用try catch finally捕获异常

```java
public class Test {
    public static void main(String[] args) {
        // 因为在try代码块中定义输出流变量，finally代码块不能使用try代码块中的变量，作用域问题，因此提高变量作用域，代码块外定义，又因为在外定义有需要赋初始值，所以赋值null
        FileOutputStream fileOutputStream = null;
        try{
            fileOutputStream = new FileOutputStream("a.txt");
            fileOutputStream.write(48);
        }catch (IOException e){
            System.out.println("输出流IO异常");
        }finally {
            // 判断输出流是否为null，如果不判断如果为null，会报出异常NullPointerException
            if(fileOutputStream != null){
                try{
                    fileOutputStream.close();
                }catch (IOException e){
                    System.out.println("Close的IO异常");
                }
            }
        }
    }
}
```



### 2、JDK 7捕获异常新特性

```java
public class CatchTest {
    public static void main(String[] args) {
        // try() 括号中定义流，当流有异常会自动释放
       try(FileOutputStream fileOutputStream = new FileOutputStream("a.txt")){
           fileOutputStream.write(48);
       }catch (IOException e){
           System.out.println("输出流IO异常");
       }
    }
}
```



### 3、JDK 9捕获异常新特性

```java
public class CatchTest {
    public static void main(String[] args) throws IOException{
       
        FileOutputStream fileOutputStream = new FileOutputStream("a.txt");
        
        try(fileOutputStream){
            fileOutputStream.write(48);
        }catch (IOException e){
            System.out.println("输出流IO异常");
        }
        
    }
}

```



## 四、缓冲流

缓冲流在基础的字节流和字符流上做功能的增强，能够高效读写的缓冲流，能够转换编码的转换流，能够持久化存储对象的序列化。

缓冲流也称高效流，是对四个基本的`Filexxx`流的增强，可分：

- **字节缓冲流**：`BufferedOutputStream` 、`BufferedInputStream`
- **字符缓冲流**：`BufferedWriter` 、`BufferedReader` 

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/缓冲流.png)

缓冲流的基本原理，是在创建流对象时，会创建一个内置的默认大小的缓冲区数组，通过缓冲区的读写，将所有读取的数据存入数组中，然后一次性返回进行数据处理(读写原理一样)，减少系统的IO次数，从而提高读写的效率。



### 1、字节缓冲流

#### 构造方法

- `public BufferedInputStream(InputStream in)` 创建一个新的缓冲输入流，系统默认缓冲区大小
- `public BufferedInputStream(InputStream in,int size)` 创建一个新的缓冲输入流，指定一个缓冲区大小
- `public BufferedOutputStream(OutputStream out)` 创建一个新的缓冲输出流，系统默认缓冲区大小
- `public BufferedOutputStream(OutputStream out,int size)` 创建一个新的缓冲输出流，指定一个缓冲区大小

```java
public class BufferedStreamTest {
    public static void main(String[] args) throws IOException{
        bufferedStream();
    }

    public static void bufferedStream() throws IOException {
        // 字节输入流
        FileInputStream fileInputStream = new FileInputStream("a.txt");
        // 字节输出流
        FileOutputStream fileOutputStream = new FileOutputStream("b.txt");
        // 定义一个字节输入流的缓冲流
        BufferedInputStream bufferedIn = new BufferedInputStream(fileInputStream);
        // 定义一个字节输出流的缓冲流
        BufferedOutputStream bufferedOut = new BufferedOutputStream(fileOutputStream);

        // 缓冲流读取，然后缓冲流写出
        byte[] bytes = new byte[1024];
        int read = 0;
        while ((read = bufferedIn.read(bytes)) != -1){
            bufferedOut.write(bytes, 0 , read); // 写入有效数据
        }
        bufferedOut.close();
        bufferedIn.close();
    }
}

// 注意：
// 字节流可以读取含中文的文件，但不能对读取出来的字节进行操作，不然会因为编码不同而出现乱码现象。通过字节流直接读取然后直接写入到另一个文件中，是没有问题的。
//如：将a.txt文件含中文的内容，读取到内存，再直接写入到b.txt中，是没有问题的。
```



### 2、字符缓冲流

#### 构造方法

- `public BufferedReader(Reader in)` 创建一个新的缓冲输入流，系统默认缓冲区大小
- `public BufferedReader(Reader in,int size)` 创建一个新的缓冲输入流，指定一个缓冲区大小
- `public String readLine() throws IOException` 按每行读取，以终止符结束(换行符'\r' 回车符'\n' 换行回车符'\r\n')，终止符不会被读入。如果全部已读完，返回null。
- 
- `public BufferedWriter(Writer out)` 创建一个新的缓冲输出流，系统默认缓冲区大小
- `public BufferedWriter(Writer out,int size)` 创建一个新的缓冲输出流，指定一个缓冲区大小
- `public void newLine()` 该方法可根据操作系统写入一个符合该操作系统的换行符，是BufferedWriter类特有的方法

```java
public class BufferedChTest {
    public static void main(String[] args) throws IOException {
        bufferedCh();
    }

    public static void bufferedCh() throws IOException {
        // 创建一个字符读取流
        FileReader fileReader = new FileReader("a.txt");
        // 创建一个字符写出流
        FileWriter fileWriter = new FileWriter("c.txt",true);
        // 定义一个字符读取缓冲流
        BufferedReader bufferedRe = new BufferedReader(fileReader);
        // 定义一个字符写出缓冲流
        BufferedWriter bufferedWr = new BufferedWriter(fileWriter);

        // 读取并写到另一个文件操作
        char[] ch = new char[1024];
        int len = 0;
         // bufferedWr.newLine(); 向文件写入一个换行
        while ((len = bufferedRe.read(ch)) != -1){
            bufferedWr.write(ch,0, len);
            bufferedWr.flush();
        }

        bufferedWr.close();
        bufferedRe.close();
    }
}
```



### 练习：文章段落排序

```
3.夫人之相与，俯仰一世。或取诸怀抱，悟言一室之内；或因寄所托，放浪形骸之外。虽趣舍万殊，静躁不同，当其欣于所遇，暂得于己，快然自足，不知老之将至；及其所之既倦，情随事迁，感慨系之矣。向之所欣，俯仰之间，已为陈迹，犹不能不以之兴怀，况修短随化，终期于尽！古人云：“死生亦大矣。”岂不痛哉！
1.永和九年，岁在癸丑，暮春之初，会于会稽山阴之兰亭，修禊事也。群贤毕至，少长咸集。此地有崇山峻岭，茂林修竹，又有清流激湍，映带左右，引以为流觞曲水，列坐其次。虽无丝竹管弦之盛，一觞一咏，亦足以畅叙幽情。
4.每览昔人兴感之由，若合一契，未尝不临文嗟悼，不能喻之于怀。固知一死生为虚诞，齐彭殇为妄作。后之视今，亦犹今之视昔，悲夫！故列叙时人，录其所述，虽世殊事异，所以兴怀，其致一也。后之览者，亦将有感于斯文。
2.是日也，天朗气清，惠风和畅。仰观宇宙之大，俯察品类之盛，所以游目骋怀，足以极视听之娱，信可乐也。
```

将乱序的《兰亭集序》文章进行排序。

```java
// 读取文章的每一个段落，一个段落就是一行
// 将该行的数字和内容以符号'.'进行分割
// 存储到HashMap中，key为段落号，value为段落内容
// 按照顺序从hashMap中取出，然后写入到文件中
public class ParagraphSort {
    public static void main(String[] args) throws IOException {
        sort();
    }

    public static void sort() throws IOException{
        FileReader fileReader = new FileReader("c.txt");
        BufferedReader bufferedRe = new BufferedReader(fileReader);

        FileWriter fileWriter = new FileWriter("d.txt");
        BufferedWriter bufferedWr = new BufferedWriter(fileWriter);

        HashMap<String,String> hashMap = new HashMap<>();

        String content;
        while ((content = bufferedRe.readLine()) != null){
            String[] split = content.split("\\."); // . 为特殊符号所以要防止转义
            hashMap.put(split[0],split[1]);
        }

        for (int i = 1; i <= hashMap.size(); i++) {
            String key = String.valueOf(i);
            String value = hashMap.get(key);
            bufferedWr.write(key+"."+value);
            bufferedWr.newLine(); // 因为没有读取换行符，所以就需要写入一个换行
        }

        bufferedWr.close();
        bufferedRe.close();
    }
}
```



## 五、转换流

转换流是InputStreamReader、OutputStreamWriter，他们的父类分别是Reader、Writer。转换流的作用就是将字节按照指定的编码规则进行解码或编码。



### 1、字符编码和字符集

**字符编码：**

计算机中存储的信息都是二进制的，而我们见到的文字等都是二进制数转换之后的结果。按照这种规则，将字符存储到计算机中，称之为**编码**。反之，将存储在计算机中的二进制数按照某种规则解析显示出来，称之为**解码**。编码和解码要是同一种规则，才会正常显示，不然会乱码。

编码：字符 --> 字节

解码：字节 --> 字符

- 字符编码：就是一套自然语言的字符与二进制数之间的对应规则关系。

**字符集：**

- 字符集：也叫编码表。是一个系统支持的所有字符的集合，包括各国家文字、标点符号、图形符号、数字等。

常见字符集：Unicode字符集、GBK字符集、ACSII字符集

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/字符集.png)

ASCII字符集是最基础的编码，正数一个字节存储0-127个字符。

GBK字符集，GB(国标)，用2个字节存储汉字。有GB2312、GBK、GB18030存储的汉字从少到多。各个编码一个字存储的字节不同。

Unicode字符集，其中UTF-8通用切常用字符集，一个汉字3个字节。



### 2、InputStreamReader 类

父类为`Reader`抽象类，拥有父类所有的共性方法。

`java.io.InputStreamReader`可以读取以该文本的编码规则读取该文本的内容，以达到正确读取该文本，不出现乱码情况。

读取的字节，以某种编码规则进行解码或编码。

#### 构造方法

一共4个

- `public InputStreamReader(InputStream in)`创建一个使用系统默认字符编码的InputStreamReader
- `public InputStreamReader(InputStream in,String charsetName) throws UnsupportedEncodingException`创建一个指定字符集的InputStreamReader (参数charsetName不区分大小写)

```java
public class InReaderTest {
    public static void main(String[] args) throws IOException {
        read_utf_8();
        read_gbk();
    }

    public static void read_utf_8() throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream("utf_8.txt"),"utf-8");
        char[] ch = new char[1024];
        int len;
        while ((len = inputStreamReader.read(ch)) != -1){
            System.out.println(new String(ch,0,len));
        }
    }

    // 以gbk的形式读取gbk编码的文本，然后解析之后向控制台输出系统默认的utf-8编码的文本
    public static void read_gbk() throws IOException{
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream("gbk.txt"),"GBK");
        char[] ch = new char[1024];
        int len;
        while ((len = inputStreamReader.read(ch)) != -1){
            System.out.println(new String(ch,0,len));
        }
    }
}
```



### 3、OutputStreamWriter 类

父类为`Writer`抽象类，拥有父类所有的共性方法。

`java.io.InputStreamWriter`以指定的编码规则，编码文本到硬盘文件中。

写入的字节，以某种编码规则进行解码或编码。

#### 构造方法

一共4个

- `public OutputStreamWriter(OutputStream out)`创建一个使用系统默认字符编码的OutputStreamWriter
- `public OutputStreamWriter(OutputStream out,String charsetName) throws UnsupportedEncodingException`创建一个指定字符集的OutputStreamWriter (参数charsetName不区分大小写)

```java
public class OutWriterTest {
    public static void main(String[] args) throws IOException {
        write_utf_8();
        write_gbk();
    }

    public static void write_utf_8() throws IOException {
        // 不指定编码，默认为UTF-8
        //OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream("utf_8.txt"));
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream("utf_8.txt"),"utf-8");
        outputStreamWriter.write("你好我是utf-8编码");
        outputStreamWriter.flush();
        outputStreamWriter.close();
    }

    public static void write_gbk() throws IOException{
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream("gbk.txt"),"GBK");
        outputStreamWriter.write("你好我是GBK编码");
        outputStreamWriter.flush();
        outputStreamWriter.close();
    }
}
```

UTF-8一个中文3个字节，ASCII码里的符号都是一个字节；GBK一个中文2个字节，ASCII码里的符号都是一个字节



### 练习：转换文件编码

将GBK编码的文本文件，转换成UTF-8编码的文本文件

```java
public class TransTest {
    public static void main(String[] args) throws IOException {
        trans();
    }

    private static void trans() throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream("gbk.txt"),"GBK");
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream("gbkToutf-8.txt"),"utf-8");
        
        char[] ch = new char[1024];
        int len = 0; // 读取到的有效字节个数
        while ((len = inputStreamReader.read(ch)) != -1){
            outputStreamWriter.write(ch,0,len);
            outputStreamWriter.flush();
        }
        
        outputStreamWriter.close();
        inputStreamReader.close();
    }
}
```



## 六、序列化和反序列化

java提供了一种对象**序列化**的机制。用一个字节序列可以表示一个对象，该字节序列包含该`对象的数据、对象的类型、对象中存储的属性`等信息。字节序列化写出到文件之后，相当于文件中**持久保存**了一个对象的信息。

反之，该字节序列还可以从文件中读取回来，重构对象，对她进行**反序列化**。

**需要注意的是**：对象中不仅包含了字符，还包含了其他，所以使用字节流进行序列化。当对象被序列化之后，存储在硬盘的文件中的内容是二进制字节，我们无法看得懂的，所以需要看懂，java机制也提供了反序列化，可将内容进行反序列，通过Object接收该对象，又可以重新使用该对象。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/序列化与反序列化.png)



### 1、ObjectOutputStream 类

继承了字节输出流，可用父类方法

#### 常用构造方法

- `public ObjectOutputStream(OutputStream out) throws IOException` 创建一个写入指定的OutputStream的ObjectOutputStream。



#### 特有的成员方法

- `public final void writeObject(Object obj) throws IOException` 将指定对象写入到ObjectOutputStream中



#### 序列化操作 Serializable接口

**一个对象要想被序列化，==必须满足==两个要求：**

- 该类==必须实现`java.io.Serializable`接口==，`Serializable`是一个标记接口（因为该接口只是作为一个可序列化或反序列化的一个通行证，会自动生产需要被序列化的这个类生产一个UID，所以只是一个标记，因此是标记型接口），不实现此接口的类将不会是任何状态序列化或反序列化，会派出`NotSerializableException`
- 该类的所有属性必须是可序列化的。如果有一个属性不需要可序列化的，则该属性必须著名是瞬态的，使用`transient`关键字修饰。

```java
public class Student implements Serializable {
    private String name;
    private int age;

    public Student() {
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }
}

```



```java
public class ObjectSerializableTest {
    public static void main(String[] args) throws IOException {
        outputObject();
    }

    private static void outputObject() throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("student.txt"));
        objectOutputStream.writeObject(new Student("heroC",18));
        objectOutputStream.flush();
        objectOutputStream.close();
    }
}
```





### 2、ObjectInputStream 类

#### 常用构造方法

- `public ObjectInputStream(InputStream in) throws IOException` 创建一个写入指定的InputStream的ObjectInputStream。



#### 特有的成员方法

- `public final Object readObject() throws IOException,ClassNotFoundException` 将指定对象读取到到ObjectInputStream中



```java
public class ObjectSerializableTest {
    public static void main(String[] args) throws IOException,ClassNotFoundException {
        inputObject();
    }

    private static void inputObject() throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("student.txt"));
        Object object = objectInputStream.readObject();
        //Student student = (Student)object;
        System.out.println(object); // 对对象的toString方法
        objectInputStream.close();
    }
}
```



### 3、InvalidException 序列号冲突异常

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/解决InvalidException.png)

在一个类实现序列化接口之后，会自动生成一个该类的serialVersionUID(序列化号)，在序列化时，会将序列号一同序列化，在反序列时，会匹配文件中和该类的class文件中的序列号是否相同，如果相同就反序列化成功，如果不相同则抛出`InvalidException`异常。当该类只要更改就会重新生成一个新的序列号。

**不给定的话因为不同的JVM之间的序列化算法是不一样的，不同的JDK也可能不一样，不利于程序的移植，可能会让反序列化失败。**

解决方法是，自己给定该类一个不变的序列号，这样被序列化之后，无论怎么更改该类，反序列都会成功。**通过在可序列化类中声明为“serialVersionUID”的字段（该字段必须是静态的、最终的long类型字段）显示声明。**

```java
private static final long serialVersionUID = 1L;
// 序列化UID字段，可以是任意值
```



### 4、transient与static

**transient 修饰符**

transient修饰符是瞬态的意思，被修饰的属性不能被序列化，只是标识这个属性不需要序列化。如果不需要该属性被序列化，推荐使用transient修饰符

**static 修饰符**

被static修饰的属性，是与对象同时间创建的，是静态的，是会被其他对象使用的。通过该static修饰符修饰的属性，也不能被序列化。



个人总结：被序列化的属性基本上都是动态的。



### 练习：序列化集合

将对象保存到集合中，然后依次将其序列化。

```java
public class SerialCollectionTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        serialCollection();
    }

    private static void serialCollection() throws IOException,ClassNotFoundException {
        // 将对象保存到集合中
        ArrayList<Student> arrayList = new ArrayList<>();
        arrayList.add(new Student("heroC",18));
        arrayList.add(new Student("yikeX",20));

        // 将该集合序列化
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("serialCollection.txt"));
        objectOutputStream.writeObject(arrayList);
        objectOutputStream.flush();

        // 读取集合反序列化
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("serialCollection.txt"));
        Object object = objectInputStream.readObject();
        ArrayList<Student> studentArrayList = (ArrayList<Student>)object;
        for(Student student : studentArrayList){
            System.out.println(student);
        }
    }
}
```



## 七、打印流

平时我们在控制台打印输出，是调用`print`方法和`println`方法完成的，这两个方法都来自于`java.io.PrintStream`类，该类能够方便地打印各种数据类型的值，是一种便捷的输出方式。



### 1、PrintStream 类

printstream类继承了outputstream

> System.out.println(); 
>
> 该句就是通过System类的静态常量PrintStream类型的out变量来调用println方法
>
> ```java
> public final static PrintStream out = null;
> ```

**特点：**

- 只负责数据的输出，不负责数据的读取
- 与其他输出流不同，PrintStream 永远不会抛出 IOException
- 有特有的方法print、println方法，可以输出任意类型的值

**注意：**

- 如果使用继承自父类的write方法写数据，那么在查看该数据的时候会自动查询编码，根据编码显示对应的数据信息；如写入97，那么最终会显示a
- 如果使用print、println方法写数据，那么写的数据会原样输出；如97，那么最终就会显示97



#### 常用构造方法

- `public PrintStream(String fileName) throws FileNotFoundException`使用指定的文件名创建一个新的打印流。输出的目的地

- `public PrintStream(File file) throws FileNotFoundException`
- `public PrintStream(OutputStream out)` 

```java
public class PrintStreamTest {
    public static void main(String[] args) throws FileNotFoundException {
        printTest();
    }

    public static void printTest() throws FileNotFoundException {
        PrintStream printStream = new PrintStream("printStream.txt");
        printStream.write(97); // 在printStream.txt输出a
        printStream.println(97); // 在printStream.txt输出97
        printStream.close();
    }
}

```



### 2、更改System.out.println()的输出目的

```java
public class PrintStreamTest {
    public static void main(String[] args) throws FileNotFoundException {
        setSystemOutTarget();
    }
	public static void setSystemOutTarget() throws FileNotFoundException {
        PrintStream printStream = new PrintStream("printStream.txt");
        System.out.println("不巧，被输出到控制台了"); // 在控制台中输出
        System.setOut(printStream); // 将out的输出目的更改了
        System.out.println("heroC"); // 在printStream.txt输出
        printStream.close();
    }
}
```



## 八、正确关闭流

```java
/**
 * 关闭给定的io流
 */
public static void close(Closeable...closes){
    for (Closeable closeable : closes) {
        try {
            if(closeable!=null){
                closeable.close();                  
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

/**
 * 调用close()方法
 */
 public static void main(String[] args) {
    FileInputStream fis = null;
    FileOutputStream fos = null;
    try {
        fis = new FileInputStream(new File("E:\\iotest\\1.bmp"));
        fos = new FileOutputStream(new File("E:\\iotest\\1_copy.bmp"));
        //其他代码
        //......
    } catch (IOException e) {
        e.printStackTrace();
    } finally{
        close(fos,fis);
    }
}
```


## 九、BIO NIO AIO

　　BIO：同步并阻塞，服务器实现模式为一个连接一个线程，即客户端有连接请求时服务器端就需要启动一个线程进行处理，如果这个连接不做任何事情会造成不必要的线程开销，当然可以通过线程池机制改善。

　　NIO：同步非阻塞，服务器实现模式为一个请求一个线程，即客户端发送的连接请求都会注册到多路复用器上，多路复用器轮询到连接有I/O请求时才启动一个线程进行处理。

　　AIO：异步非阻塞，服务器实现模式为一个有效请求一个线程，客户端的I/O请求都是由OS先完成了再通知服务器应用去启动线程进行处理。