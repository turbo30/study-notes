# Cloneable接口



java克隆技术，首先需要知道java是值引用的。所以当一个对象赋值给另一个对象变量的时候，其实是把空间地址赋值给了另一个对象变量，最终造成了两个变量指向了同一个引用空间，即同一个对象。因此不能这样直接赋值，需要克隆。



Student类：

```java
public class Student{
    private String name;
    private int age;
    private Subject subject;

    public Student() {
    }

    public Student(String name, int age, Subject subject) {
        this.name = name;
        this.age = age;
        this.subject = subject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", subject=" + subject +
                '}';
    }
}
```

Subject类：

```java
public class Subject{
    private String subName;

    public Subject() {
    }

    public Subject(String subName) {
        this.subName = subName;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "subName=" + subName +
                '}';
    }
}

```



> 不使用克隆，直接赋值

```java
public class CloneTest {
    public static void main(String[] args) throws CloneNotSupportedException {
        /**
         * 不实现Cloneable
         */
        Student student = new Student("heroc", 21, new Subject("Java"));
        Student stu = student;
        stu.setName("heroc_no_clone");
        System.out.println(student);
        System.out.println(stu);
        /**
         * 不实现cloneable结果，stu和student都指向了一个引用空间地址，即都指向了一个对象
         * Student{name='heroc_no_clone', age=21, subject=Subject{subName=Java}}
         * Student{name='heroc_no_clone', age=21, subject=Subject{subName=Java}}
         */
    }
}
```



### 浅克隆

> 实现Cloneable接口，实现clone方法
>
> 浅克隆

```java
public class Student implements Cloneable{
    private String name;
    private int age;
    private Subject subject;
    
	......
        
    @Override
    protected Student clone() throws CloneNotSupportedException {
        Student student = (Student)super.clone();
        return student;
    }
}
```



```java
public class CloneTest {
    public static void main(String[] args) throws CloneNotSupportedException {
        /**
         * student实现Cloneable接口，实现clone方法，浅克隆
         */
        Student student = new Student("heroc", 21, new Subject("Java"));
        Student stu = student;
        stu.setName("heroc_no_clone");
        stu.getSubject().setSubName("java编程");
        System.out.println(student);
        System.out.println(stu);
        /**
         * 运行结果，可以知道是浅克隆，
         * 如果subject也需要克隆，subject也需要实现Cloneable接口，这样就实现了深克隆
         * Student{name='heroc', age=21, s  ubject=Subject{subName=java编程}}
         * Student{name='heroc_no_clone', age=21, subject=Subject{subName=java编程}}
         */
    }
}
```



### 深克隆

深克隆就是将所有对象都实现Cloneable接口，实现完全克隆

Student类：

```java
public class Student implements Cloneable{
    private String name;
    private int age;
    private Subject subject;
    
	......
        
    @Override
    protected Student clone() throws CloneNotSupportedException {
        Student student = (Student)super.clone();
        student.subject = (Subject)subject.clone();
        return student;
    }
}
```



Subject类：

```java
public class Subject implements Cloneable{
    private String subName;
	
    ......
    
    @Override
    protected Subject clone() throws CloneNotSupportedException {
        Subject subject = (Subject) super.clone();
        return subject;
    }
}

```



测试：

```java
public class CloneTest {
    public static void main(String[] args) throws CloneNotSupportedException {
        /**
         * student和subject都实现Cloneable接口，实现clone方法，深克隆
         */
        Student student = new Student("heroc", 21, new Subject("Java"));
        // Student stu = student; // 该操作赋值的是引用空间地址
        Student stu = student.clone(); 
        stu.setName("heroc_no_clone");
        stu.getSubject().setSubName("java编程");
        System.out.println(stu);
        /**
         * 运行结果，完全实现克隆
         * Student{name='heroc', age=21, subject=Subject{subName=Java}}
         * Student{name='heroc_no_clone', age=21, subject=Subject{subName=java编程}}
         */

    }
}
```



### 总结

在对象的赋值的时候，由于java是值传递(值引用)，赋值就会使得两个变量都指向一个引用空间，由于都指向了同一个引用空间，即指向了同一个对象，随便更改哪个，该对象的内容都会发生变化。所以需要使用到clone()，将对象完全复制出来开辟一个空间，需要克隆的类需要实现Cloneable接口，然后重写并调用父类的clone方法，需要注意的是默认的是浅克隆，意思就是这个对象中属性引用了其他对象，那么这个克隆过来的对象中的这个属性还是与原来的属性都指向的同一个引用空间，所以要实现深度克隆，就需要将这个对象中属性引用的那个对象也需要实现Coneable接口。