# 线程池 Lambda表达式



## 一、线程池

如果并发的线程数量很多，并且每一个线程执行的任务时间短暂就结束了，这样频繁创建线程就会大大降低系统效率，因为频繁创建线程和销毁线程需要时间。

在Java中可以通过线程池来达到线程可以复用，就是执行完一个任务，并不被销毁，而是可以继续执行其他任务。



### 1、线程池概念及原理

- 线程池：其实就是一个容纳很多线程的容器，其中的线程可以反复使用，省去了频繁创建线程对象的操作，无需反复创建线程而消耗过多资源。

由于线程池中很多操作都与优化资源相关，不过多赘述。

**线程池的工作原理：**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/线程池原理.png)

线程池就是一容器，可通过集合来存储线程。当程序第一次启动时，创建多个线程，保存到一个集合中，当需要用的时候，就从集合中取出线程使用，使用完成之后，再把使用完的线程添加到集合中，供给其他需要线程的任务使用。**JDK1.5之后，内置了线程池，可直接使用。**

**合理使用线程池的好处：**

1. 减低资源消耗
2. 提高响应速度
3. 提高线程可管理性



### 2、线程池的使用 Executors类 JDK1.5

Java里面执行线程的工具接口`java.util.concurrent.Executor`。线程池接口为`java.util.concurrent.ExecutorService`。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/Executor.png" style="float:left" />

要配置一个线程池是比较复杂的，因此`java.util.concurrent.Executors` 线程工厂类提供了一些静态方法，可以生成常用的线程池。官方建议使用`Executors`工程类创建线程池。



#### 常用方法

**Executors类**

- `public static ExecutorService newFixedThreadPool(int nThreads)` 返回的是线程池接口的实例对象，用线程池接口接收。（创建的是有界线程池，也就是池中的线程个数可以只当最大数量）

**ExecutorService接口**

- `public Future<?> submit(Runnable task)` 获取线程池中的某个线程对象，并执行

  > Futrue接口：用来记录线程任务执行完毕后产生的结果。线程池的创建与使用

- `public void shutdown()` 关闭/销毁线程池。一般不会使用该方法。



**RunnableImp.java**

```java
public class RunnableImp implements Runnable {
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " 已从线程池中获取线程使用");
    }
}
```

**Test.java**

```java
public class Test {
    public static void main(String[] args) {
        // 创建一个线程池，线程池中的线程数量为2个
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(new RunnableImp()); // pool-1-thread-1 已从线程池中获取线程使用
        executorService.submit(new RunnableImp()); // pool-1-thread-1 已从线程池中获取线程使用
        executorService.submit(new RunnableImp()); // pool-1-thread-1 已从线程池中获取线程使用
        executorService.submit(new RunnableImp()); // pool-1-thread-2 已从线程池中获取线程使用

        // 销毁线程池 不建议使用，因为线程池的作用就是为了线程能够重复使用
        //executorService.shutdown(); 
        
         // 因为线程池已经被销毁，所以会报异常RejectedExecutionException
        //executorService.submit(new RunnableImp());
    }
}
```



### 3、关于线程池OOM问题见 JUC



## 二、Lambda表达式

【关于函数式接口，函数式编程见 **Java笔记08**】

面向对象的思想：

​		做一件事情，找一个能解决这个事情的对象，调用对象的方法，完成事情。

函数式编程思想：

​		只要能获取到结果，谁去做的，怎么做的都不重要，重视的是结果，不重视过程。



**注意：**

**使用匿名内部类在编译时，会生成一个class文件，而使用Lambda不会生成class文件，因此执行的时候Lambda会比匿名内部类效率高。**



### 1、体验Lambda表达式 JDK1.8

JDK1.8新特性

作用在于将冗余的代码去除掉，直接写要点。

```java
public class LambdaTest {
    public static void main(String[] args) {
        // 比较冗余的写法创建线程，做任务
        Runnable r = new Runnable(){
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + " 新线程");
            }
        };
        new Thread(r).start();

        // 使用jdk1.8 新特性lambda
        new Thread(() -> System.out.println(Thread.currentThread().getName() + "    lambda新线程") ).start();
    }
}
```



### 2、Lambda标准格式

Lambda省去了面向对象的条条框框，格式由3个部分组成：

- 一些参数
- 一个箭头
- 一段代码

```
(参数) -> {代码}
```

#### 练习（无参无返回）

有一个cook接口

```java
public interface Cook {
    void method();
}
```

Test测试类，用Lambda实现

```java
public class Test {

    public static void main(String[] args) {
        invokeCook( () -> System.out.println("做饭了。") );
        // Lambda表达式，如同创建一个匿名内部类，实现接口，并重写方法，将该实例传给方法invokeCook执行
    }

    public static void invokeCook(Cook cook){
        cook.method();
    }
}
```

#### 练习（有参有返回）

Person数组按照age排序

```java
public class PersonSort {
    public static void main(String[] args) {
        Person[] people = {
                new Person("heroC",22),
                new Person("yikeX",18),
                new Person("he",32)
        };

        // 排序传统写法
        /*Arrays.sort(people, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getAge()-o2.getAge();     // 按年龄升序
            }
        });*/

        // 排序Lambda写法
        Arrays.sort(people, (Person o1, Person o2) -> {
            return o1.getAge() - o2.getAge();
        });
        
        for(Person p : people){
            System.out.println(p);
        }
    }
}
```



### 3、Lambda省略格式

省略规则

1. 小括号内参数的类型可以省略
2. 如果小括号内有且仅有一个参，则小括号可以省略
3. 如果大括号有且仅有一个语句，无论是否有返回值，都可以省略大括号、return关键字及语句分号。

#### 练习：使用Lambda省略格式

```java
// 上个练习，Person数组按照age排序,	Lambda省略格式
// 排序Lambda写法
        Arrays.sort( people, (o1, o2) -> o1.getAge() - o2.getAge() );
```



### 4、Lambda使用前提

1. 使用Lambda必须具有接口，且要求**接口中有且仅有一个抽象方法**。
2. 使用Lambda必须具有**上下文推断**。

> 备注：有且仅有一个抽象方法的接口，称为“**函数式接口**”。

