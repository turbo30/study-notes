# Java 高级阶段



## 一、泛型 JDK5

### 1、泛型概述

**是JDK 5的新特性(JDK 1.5)**

1）创建集合对象，可不使用泛型

好处：

- 集合不使用泛型，默认的类型就是Object类型，可以存储任意类型的数据

弊端：

- 不安全，容易发生异常

```java
public static void mian(Stirng[] args){
    ArrayList list = new ArrayList();
    list.add("abc");
    list.add(1);
    System.out.println(list.length()); // 这里会报错，抛出异常，因为存储的类型不同
}
```



2）创建集合对象，使用泛型

好处：

- 避免了类型转换的麻烦，存储什么类型，取出的就是什么类型
- 把运行期异常，转到了编代码期

弊端：

- 泛型是什么类型，就只能存储什么类型



### 2、自定义泛型

#### 自定义泛型类

```java
public class GenericClass<E>{
    private E value;
    
    public E getValue(){
        return value;
    }
    public void setValue(E value){
        this.value = value;
    }
}
```

此处类后面加了一个\<E>就是泛型。

#### 含有泛型的方法

格式：

修饰符 <泛型> 返回值类型 方法名(参数列表(使用泛型)){方法体; }

泛型方法，可根据传递的参数来确定是什么类型。

```java
public class GenericMethod{
    // 定义一个含有泛型的方法
    public <M> void method01(M m){
        System.out.println(m);
    }
    // 定义一个含有泛型的静态方法
    public static <S> void method02(S s){
        System.out.println(s);
    }
}
```

#### 含有泛型的接口

```java
public interface GenericInterface<E> {
    public abstract void method(E e);
}
```

可通过继承接口，然后对接口的泛型进行指定



### 3、泛型通配符

泛型通配符：`?`

泛型通配符只能通过参数传递使用。不具备存储功能。

```java
public class Test{
    public static void main(String[] args){
        ArrayList<Integer> listInt = new ArrayList<>();
        listInt.add(1);
        listInt.add(2);
        
        ArrayList<String> listStr = new ArrayList<>();
        listStr.add("a");
        listStr.add("b");
        
        printArray(listInt);
        printArray(listStr);
    }
    
    public static void printArray(ArrayList<?> list){
        Iterator<?> inter = list.Interator();
        while(inter.hasNext()){
            System.out.println(inter.next());
        }
    }
}
```



### 4、泛型高级使用，上限下限

泛型的上限 限定：`Collection<? extends Object>` 此时的泛型必须是Object的子类或者本身

泛型的下限 限定：`Collection<? super Object>` 此时的泛型必须是Object的父类或者本身	

```java
public class Test{
    public static void main(String[] args){
        // 注意：从JDK 1.7 开始，右侧的尖括号内可以不写泛型，但本身是要写的。
        Collection<Integer> listInt = new ArrayList<>();
        Collection<String> listStr = new ArrayList<>();
        Collection<Number> listNum = new ArrayList<>();
        Collectiont<Object> listObj = new ArrayList<>();
        
        getElm1(listInt);
        getElm1(listStr); // 报错
        getElm1(listNum);
        getElm1(listObj); // 报错
        
        getElm2(listInt); // 报错
        getElm2(listStr); // 报错
        getElm2(listNum);
        getElm2(listObj);
    }
    
    public static void getElm1(Collection<? extends Number>){}
    public static void getElm2(Collection<? super Number>){}
    }
}
```



### 5、泛型擦除

Java 泛型的参数只可以代表类，不能代表个别对象。由于 Java 泛型的类型参数之实际类型在编译时会被消除，所以无法在运行时得知其类型参数的类型。Java 编译器在编译泛型时会自动加入类型转换的编码，故运行速度不会因为使用泛型而加快。-----百度百科



#### (1) 泛型擦除的体现
通过以下代码来感受以下什么是泛型擦除：
```java
public class Erase {

    public static void main(String[] args) {
    	// 创建两个不同泛型的list集合
        List<Integer> integerList = new ArrayList<>();
        List<String> stringList = new ArrayList<>();
        
        System.out.println(integerList.getClass() == stringList.getClass());

        System.out.println("integerList----> "+integerList.getClass().getName());
        System.out.println("stringList----> "+stringList.getClass().getName());
    }
}
```
输出结果
```
true
integerList----> java.util.ArrayList
stringList----> java.util.ArrayList
```
很明显发现，两个集合的泛型不同，但是比较class，返回的是true，并且通过反射获取到的className是java.util.ArrayList
不管是 ArrayList\<Integer> 还是 ArrayList\<String>，在编译完成后都会被编译器擦除成了 ArrayList。

`Java 泛型擦除是 Java 泛型中的一个重要特性，其目的是避免过多的创建类而造成的运行时的过度消耗。所以，想 ArrayList<Integer> 和 ArrayList<String> 这两个实例，其实类实例是同一个。`



#### (2) 可以把其他类型的数据放到指定了泛型的list集合中吗？
当然是可以的。因为泛型擦除的原因。完全可以将不相干的类型的数据存入指定泛型的集合中。

如下所示添加，肯定是要报错的，因为在编译的时候设定了规则，编译是不可能通过的。



<img src="https://img-blog.csdnimg.cn/20200914204422115.png" alt="在这里插入图片描述" style="float:left;" />



按理说，因为泛型擦除的原因，完全是可以将不相干类型的数据放到指定泛型的集合中的。其实，可以换一种思路，当list集合运行起来之后，这个list集合因为泛型擦除是没有指定泛型的，即class是没有泛型的，所以可以在运行期间获取这个list集合，然后将值放到该list集合中。

```java
public class Erase {
    
    public static void main(String[] args) {
        Erase erase = new Erase();
        erase.testErase();
    }

    List<Integer> integerList = new ArrayList<>();
    List<String> stringList = new ArrayList<>();

    public void testErase(){
        Double d = 10d;
        try {
        	// 通过反射机制获取字段，然后将double类型的数据放入泛型为Integer集合中
            Field integerField = this.getClass().getDeclaredField("integerList");
            integerField.setAccessible(true);
            List integerFieldList = (List) integerField.get(this);
            integerFieldList.add(d);

			// 通过反射机制获取字段，然后将double类型的数据放入泛型为String集合中
            Field stringField = this.getClass().getDeclaredField("stringList");
            stringField.setAccessible(true);
            List stringFieldList = (List) stringField.get(this);
            stringFieldList.add(d);
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("integerList ---> "+integerList);
        System.out.println("stringList ---> "+stringList);
    }
}
```
输出结果
```
integerList ---> [10.0]
stringList ---> [10.0]
```



## 二、集合框架

集合：集合是java中的一种容器，可用于存储多个数据。

**集合与数组的区别**

- 数组长度是固定的。集合长度是可变的。
- 数组存储的是同一类型的元素，可存储基本数据类型值。而集合存储的是对象，且对象类型可以不一致，也可以一致，在开发过程中，当对象多时使用集合进行存储。



### <u>Collection集合框架 常用关系结构图</u>

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/集合框架.png)

Collection是最顶层的接口，List集合和Set集合都是继承Collection接口的接口



#### 1、Collection\<E> 接口集合 JDK 1.2

Collection是最顶层的接口。

##### 常用方法

- `public abstract boolean add(E e)`：向数组集合中添加元素
- `public abstract boolean remove(E e)`：删除索引值为index的元素
- `public abstract int size()`：获取数组集合的长度大小，返回元素个数
- `public abstract boolean contains(Object o)`：判断是否包含指定元素
- `public abstract void clear()`：清空所有元素
- `public abstract Object[] toArray()`：将集合转为一个数组

```java
public class TestCollection{
    public static void main(String[] args){
        // 注意：从JDK 1.7 开始，右侧的尖括号内可以不写泛型，但本身是要写的。
        Collection<String> coll = new ArrayList<>(); // 由于接口不可实例化，所以使用多态进行实例
        coll.add("A");
        coll.add("B");
        coll.add("C");
        System.out.println(coll); // 输出 [A,B,C]
        coll.remove("B");
        System.out.println(coll); // 输出 [A,C]
    }
}
```



#### 2、List\<E> 接口集合 JDK 1.2

`java.util.List`接口  注意越界，越界异常：IndexOutOfBundsException

List集合特点：

1. 有序的集合，存储元素和取出元素的顺序一致
2. 有索引，包含了一些带索引的方法
3. 允许存储重复的元素

##### 常用方法 (特有)

- `public abstract void add(int index, E element)`：将指定的元素，添加到集合中的指定位置中
- `public abstract E get(int index)`：返回集合中指定位置的元素
- `public abstract E remove(int index)`：移除列表中指定位置的元素，返回的是被移除的元素
- `public abstract E set(int index, E element)`：用指定元素替换集合中指定位置的元素，返回值的更新前的元素

```java
public class TestArrayList{
    public static void main(String[] args){
        List<String> list = new List<>();
        list.add("a");
        list.add("b");
        list.add("a");
        System.out.println(list); // [a,b,a]
        
        String reElement = list.remove(1);
        System.out.println(reElement); // b
        System.out.println(list); // [a,a]
        
        String seElement = list.set(1, "c");
        System.out.println(seElement); // a
        System.out.println(list); // [a,c]
        
        // 遍历list的3种方法
        for(String s: list){
            System.out.println(s);
        }
        
        for(int i = 0; i < list.size(); i++){
            System.out.println(list.get(i));
        }
        
        Iterator<String> iterator = list.iterator();
        if(iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
```



#### 3、ArrayList\<E>集合 JDK 1.2

数组的长度不可变化，但是ArrayList集合的长度是可以随意变化的。E为泛型，泛型只能是引用类型。可调整大小的数组的实现的List接口。

如果查询比较多的话，就使用ArrayList集合。因为增删改的操作时，每次都要调用底层的源代码，进行数组的大小调整，效率较慢。

数组结构就是查询快，增删慢。

##### 常用方法

- `public boolean add(E e)`：向数组集合中添加元素
- `public E get(int index)`：得到索引值为index的元素
- `public E remove(int index)`：删除索引值为index的元素，并返回被删除的元素
- `public int size()`：获取数组集合的长度大小，返回元素个数
- `public boolean contains(Object o)`：判断是否包含指定元素
- `public int indexOf(Object o)`：返回该元素的索引值
- `public int lastIndexOf(Object o)`：返回该元素最后一次出现的索引值

```java
public class TestArrayList{
    public static void main(String[] args){
        // 注意：从JDK 1.7 开始，右侧的尖括号内可以不写泛型，但本身是要写的。
        ArrayList<String> list = new ArrayList<>();
        System.out.println(list); // 输出 []
        list.add("A");
        list.add("B");
        list.add("C");
        System.out.println(list); // 输出 [A,B,C]
    }
}
```

IDEA遍历arraylist集合快捷键：listfor+回车



#### 4、LinkedList\<E>集合 JDK 1.2

`java.util.LinkedList` 类

LinkedList集合的特点：

1. List接口有的特点（有序集合、有索引、可重复元素）
2. 底层是一个链表结构：查询慢，增删块
3. 该集合包含了大量的首位元素的方法



##### 常用方法

- `public void addFirst(E e)`：将指定元素插入到开头位置

- `public void addLast(E e)`：将指定元素插入到结尾位置
- `public void push(E e)`：同addFirst方法

- `public void clear()`：清空列表
- `public E removeFirst()`：将开头元素移除，返回该元素
- `public E removeLast()`：将结尾元素移除，返回该元素
- `public E pop()`：同removeFirst方法，返回该元素
- `public boolean isEmpty()`：如列表不包含元素，则返回true



#### 5、Vector\<E> 集合 JDK 1.0

**线程安全** 每一个方法都使用`synchronized`关键字

之前是独立的集合，从Java 2平台v1.2，这个类被改造为实现[`List`接口。它与新集合实现不同， `Vector`是同步的(单线程)。 如果不需要线程安全的实现，建议使用`ArrayList`代替`Vector`  。 

在最开始jdk1.0的时候

- 实现添加元素方法为：`public void addElement(E obj)` ，jdk1.2之后继承了list接口，有了`add()`方法添加元素。
- 遍历元素的方法为：`public Enumeration<[E]> elements()` ，返回一个Enumeration集合，该集合与Iterator迭代器类似。Enumeration集合有两个方法：`boolean hasMoreElements()`测试是否还有更多的元素 和`E nextElement()` 返回此枚举下一个元素。jdk1.2之后可以使用iterator迭代器对该集合遍历。



#### 6、Set\<E> 接口集合 JDK 1.2

`java.util.Set`接口和`java.util.List`接口一样，同样继承自`Conllection`接口，它与`Conllection`接口中的方法基本一致，并没有`Conllection`接口进行功能的扩充，只是比`Conllection`接口更加严格了。与`List`接口不同的是，`Set`接口中元素无序，并且都会以某种规则保证存入的元素不出现重复。

`Set`接口集合有多个子类，这里我们介绍其中的`java.util.HashSet`、`java.util.LinkedHashSet`这两个集合。

> tips：Set集合取出元素的方式可以采用迭代器、增强for

特点：

1. 存储元素无序
2. 存储元素不重复
3. 无索引



##### HashSet、LinkedHashSet、TreeSet<u>底层 源码</u>

```java
// 1************************************************************************************
public HashSet() {
    map = new HashMap<>();
}
// add方法
	private static final Object PRESENT = new Object();
    public boolean add(E e) {
        return map.put(e, PRESENT)==null;
    }
	

// 2************************************************************************************
public LinkedHashSet() {
    super(16, .75f, true);
}
---> 
    HashSet(int initialCapacity, float loadFactor, boolean dummy) {
    	map = new LinkedHashMap<>(initialCapacity, loadFactor);
    }
	---> private transient HashMap<E,Object> map;

// 3************************************************************************************
public TreeSet() {
    this(new TreeMap<E,Object>());
}
```

**可见底层是HashMap，HashMap底层是哈希表**



#### 7、HashSet\<E> 集合 JDK 1.2

此类实现`Set`接口，由哈希表（实际也是`HashMap`实例）支持

特点：

1. 不允许存储重复元素
2. 没有索引
3. 是一个无序集合，存储元素和取出元素的顺序可能不一致
4. 底层是一个<u>哈希表结构</u>(**查询的速度非常快**)

```java
public class Test{
    public static void main(String[] args){
        HashSet<Integer> hashSet = new HashSet<>();
        hashSet.add(1);
        hashSet.add(3);
        hashSet.add(2);
        hashSet.add(1);
        
        Iterator<Integer> iterator = hashSet.iterator();
        if(iterator.hasNext()){
            System.out.println(iterator.next()); // 输出 1，2，3
        }
        
        for(Integer i : hashSet){
            System.out.println(i); // 输出 1，2，3
        }
    }
}
```

由上可看出，HashSet的特点。



##### <u>HashSet存储数据的结构(哈希表) native</u>

[Hash表结构](https://blog.kuangstudy.com/index.php/archives/379/)

**哈希表结构**：在jdk8之前是数组+链表；jdk8开始时数组+链表/红黑树(查询速度快)

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/hash表结构.png)

在jdk8开始，当相同hashcode下的链表长度超过8位就会自动转换为红黑树，这样提高查询速度。

> **tips：了解红黑树，请移步至Java数据结构与算法：红黑树.md**



 **hashCode()方法**

`public native int hashCode()` 

Object类hashCode方法原生写法，应该是将物理地址转换成整数然后将该整数通过hash算法，算出值。

> native主要用于方法上： 
>
>  1、一个native方法就是一个Java调用非Java代码的接口。一个native方法是指该方法的实现由非Java语言实现，比如用C或C++实现。 
>
> 2、在定义一个native方法时，并不提供实现体（比较像定义一个Java Interface），因为其实现体是由非Java语言在外面实现的。



对象的内容通过hash函数的算法就得到了hashcode。该hashcode值就是在hash表中对应的位置。HashCode的存在主要是为了<u>查找的快捷性</u>。

> 为什么hashcode查找更快？
>
> 假如hash表中有1、2、3、4、5、6、7、8个位置，存第一个数，hashcode为1，该数就放在hash表中1的位置，存到100个数字，hash表中8个位置会有很多数字了，1中可能有20个数字，存101个数字时，他先查hashcode值对应的位置，假设为1，那么就有20个数字和他的hashcode相同，他只需要跟这20个数字相比较(equals)，如果没一个相同，那么就放在1这个位置，这样比较的次数就少了很多，实际上hash表中有很多位置，这里只是举例只有8个，所以比较的次数会让你觉得也挺多的，实际上，如果hash表很大，那么比较的次数就很少很少了。



##### <u>set集合存储不重复元素的原理</u>

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/set不重复原理.png)

**equals方法和hashcode的关系**

先通过hashcode来比较，如果hashcode相等，那么就用equals方法来比较两个对象是否相等。

1. 如果两个对象equals相等，那么这两个对象的HashCode一定也相同
2. 如果两个对象的HashCode相同，不代表两个对象就相同，只能说明这两个对象在散列存储结构中，存放于同一个位置

> 如果对象的equals方法被重写，那么对象的HashCode方法也要重写。
>
> **equals方法与hashcode方法必须一起重写。**



##### <u>存储自定义类型，重写hashcode、equals</u>

自定义的类型，一定要重写hashcode和equals方法！！！

**Person.java**

```java
// 要求：名字、年龄一样则视为同一个人
// 自定义Person类
public class Person {
    private String name;
    private String age;

    public Person() {
    }

    public Person(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }
}
```



**Person.java**

```java
// 通过快捷 重写hashcode和equals方法

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name) &&
                Objects.equals(age, person.age);
    }


// 可以明显看到hashCode方法，将对象中的内容进行hash值，所以输入内容一样得出的hash值肯定是一样的，也不排除hash冲突。如果不重写的话，调用的Object的hashCode方法，那么就是通过对象的物理地址进行hash算法，结果肯定不同。
// 有了hash冲突，就得用equals方法去判断存储内容是不是一样的，如果不一样就存入hash结构表中，如果一样则存入。不重写equals，调用的是Object类的equals方法，该方法底层是用==比较
```



**Test.java**

```java
public class Test {
    public static void main(String[] args) {
        Person p1 = new Person("heroC","18");
        Person p2 = new Person("heroC","18");
        Person p3 = new Person("张还行","18");

        HashSet<Person> set = new HashSet<>();
        set.add(p1);
        set.add(p2);
        set.add(p3);
        System.out.println(set);
        System.out.println(p1.equals(p2));
    }
}
// 没有重写hashcode和equals方法输出的结果：
// [Person{name='张还行', age='18'}, Person{name='heroC', age='18'}, Person{name='heroC', age='18'}]
// false

// 重写hashcode和equals方法输出的结果：
// [Person{name='heroC', age='18'}, Person{name='张还行', age='18'}]
// true

```



#### 8、LinkedHashSet\<E> 集合 JDK 1.2

HashSet保证元素唯一，但元素存放没有顺序，因此有了LinkedHashSet类，`java.util.LinkedHashSet`，该类实现了Set接口同时也是HashSet类的子类。它是链表和哈希表组合的一个数据存储结构。

特点：

底层是一个哈希表(数组+链表/红黑树)+链表；多了一条链表(用于记录元素的存储顺序)，保证元素有序

```java
public class Test {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<>();
        set.add("www");
        set.add("abc");
        set.add("heroC");
        System.out.println(set); // [heroC, abc, www] 无序

        LinkedHashSet<String> linked = new LinkedHashSet<>();
        linked.add("www");
        linked.add("abc");
        linked.add("heroC");
        System.out.println(linked); // [www, abc, heroC] 有序
    }
}
```



#### 9、TreeSet\<E> 集合 JDK 1.2

`java.util.TreeSet` 底层红黑树

特点：

1. 无索引
2. 无重复元素
3. **可按照自然顺序自动排序**

```java
public class Test {
    public static void main(String[] args) {
        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(45);
        treeSet.add(234);
        treeSet.add(234);
        treeSet.add(13);
        treeSet.add(10);
        System.out.println(treeSet); // 输出 [10, 13, 45, 234]
        
        Iterator<Integer> iterator = treeSet.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
        
        for (Integer i: treeSet){
            System.out.println(i);
        }
    }
}
```



### Collections集合工具类

从JDK1.2开始 `java.util.Collections` extends Object 该类是集合工具类，对集合进行操作的类。**该工具类，可以调用同步集合（调用的同步集合在方法内加入了synchronized关键字）。**

#### 常用方法

- `public static <T> boolean addAll(Collection<? super T> c, T... elements)` 将所有指定的元素添加到指定的集合。 要添加的元素可以单独指定或作为数组指定。
- `public static void shuffle(List<?> list)` 使用默认的随机源随机排列指定的列表。 所有排列都以大致相等的可能性发生。
- `public static <T extends Comparable<? super T> > void sort(List<T> list)` 将集合按照默认规则排序。
- `public static <T> void sort(List<T> list, Comparator<? super T> c)` 将集合中的元素按照指定规则排序。



```java
public static void main(String[] args){
    ArrayList<String> list = new ArrayList<>();
    
    Collections.addAll(list, "a","b","c","d","e");
    System.out.println(list); // 输出 [a,b,c,d,e]
    
    Collectons.shuffle(list);
    System.out.println(list); // 输出 [b,d,a,c,e] 每次输出结果不同，作用在于打乱顺序
    
    Collections.sort(list);
    System.out.println(list); //  输出 [a,b,c,d,e] 默认规则，自然规则排序
}
```



自定义类型的集合，排序( 常用方法3关于自定义类型的集合实现方法 )：

Person.java

```java
public class Person implements Comparable<Person>{
    private String name;
    private int age;
    
    public Person(){
        
    }
    
    public Person(String name, int age){
        this.name = name;
        this.age = age;
    }
    
    // 此处省略 getter/setter 方法
    
    @Override
    public int compareTo(Person p){
        return this.getAge() - p.getAge(); // *****按照年龄升序排序 反过来减就是降序 
    }
}
```

Test.java

```java
public class Test{
    public static void main(String[] args){
        ArrayList<Person> list = new ArrayList<>();
        Collections.addAll(list, new Person("a", 28),new Person("b", 18),new Person("c", 38));
        
        Collections.sort(list);
        System.out.println(list);
        // 输出 [Person{"b", 18},Person{"a", 28},Person{"c", 38}]
    }
}
```

注意：自定义类型的集合，一定要实现 Comparable\<E> 接口，并且重写排序规则方法 compareTo(Object o) ，这样在自定义类型的集合在排序的时候，才不会报错。java内置的类型实现了该接口，并重写了该方法。



两个sort方法的比较：

#### Comparable与Comparator的区别

- Comparable：是自己this对象与另一个对象，进行比较的规则。实现Comparable接口重写CompareTo方法。
- Comparator：是第三方来给定规则，来排序这个集合里的所有元素。自定义类型就不用实现Comparable接口了不用重写方法了。

```java
ArrayList<Person> list = new ArrayList<>();
Collections.addAll(list, new Person("a", 28),new Person("b", 18),new Person("c", 38));

Collections.sort(list, new Comparator<Person>(){
    @Override
    public int compare(Person p1, Person p2){
        return p1.getAge() - p2.getAge(); // 按年龄升序
    }
});

// 如果年龄一样，按照名字排序
ArrayList<Person> listA = new ArrayList<>();
Collections.addAll(listA, new Person("a", 28),new Person("c", 18),new Person("b", 18));

Collections.sort(listA, new Comparator<Person>(){
    @Override
    public int compare(Person p1, Person p2){
        int result =  p1.getAge() - p2.getAge(); // 按年龄升序，如果年龄一样
        if( result == 0){
            result = p1.getName().charAt(0) - p2.getName().charAt(0); // 按照名字的第一个字符顺序排序。如果名字第一个字符一样，那么在添加在这个集合中时，谁在前，谁就排在前面
        }
        return result;
    }
});
System.out.println(listA); // 输出 [Person{"b", 18},Person{"c", 18},Person{"a", 28}]

```



#### 初始化一个静态/不可变的map集合

```java
public class test{
    private static final Map<String,String> map;
    static {
        Map<String,String> map1 = new HashMap<>();
        map1.put("1","11");
        map1.put("2","22");
        Map<String, String> map2 = Collections.unmodifiableMap(map1);
        map = map2;
    }
    public static void main(String[] args) {
        map.put("3","33"); // 报异常 UnsupportedOperationException
    }
}
```

同样Collections工具类，还有其他不可修改的集合方法

`unmodifiableCollection(Collection<? extends T> c)`

`unmodifiableList(List<? extends T> list)`

`unmodifiableSet(Set<? extends T> s)` 





### <u>Map<K,V> 集合框架 常用实现类</u>

`java.util.Map<K,V>` 双列集合接口 键值对

K 和 V 都是泛型，根据传递进来的值的类型所决定

特点：

1. 键唯一，值不唯一，但一个键必须对应一个值
2. 键和值的类型可相同也可不相同
3. 无序

**常用实现类：**

- **HashMap<K,V>**  底层是Hash表(查询速度快)。  此实现提供了所有可选的地图操作，并允许`null的`值和`null`键。 （  `HashMap`类大致相当于`Hashtable` ，除了它是不同步的，并允许null）。这个类不能保证地图的顺序;  特别是，它不能保证在一段时间内保持不变。
- **LinkedHashMap<K,V>** 继承HashMap集合，因在Hash表的基础上，用了链表记录数据顺序，所以该类可保证元素的顺序。



#### 1、Map<K,V> 接口集合 JDK 1.2

Map接口定义了很多方法。



##### 常用方法

- `public abstract V put(K key, V value)` 把键值存入到map集合中，返回该键的值，第一次插入该key，返回null
- `public abstract V remove(Object key)` 把指定的键所对应的键值对删除，返回被删除元素的值，不存在返回null
- `public abstract V get(Object key)` 获取指定的键的值，不存在返回null
- `public abstract boolean containsKey(Object key)` 判断集合中是否包含指定的键

**Map集合的遍历：**

- `public abstract Set<K> keySet()` 获取Map集合中的所有键，存储到Set集合中
- `public abstarct Set<Map.Entry<K,V>> entrySet() ` 获取Map集合中所有的键值对对象的集合(Set集合)



#### 2、HashMap<K,V> 集合 JDK 1.2

特点：

1. key值不能重复
2. 无序

> Map<String,String> map = new HashMap<>();
>
> 1. 工作中不用hashmap。
>
> 2. 该创建对象默认等于new HashMap<>(16，0.75)；
>
>    16为数组默认长度，0.75为默认负载因子(当元素填满数组的75%的时候，才扩容数组长度)

##### 常用方法 <u>eg中代码返回值细节</u>

```java
public class Test{
    public static void main(String[] args){
        showOne();
        showTwo();
    }
    
    public static void showOne(){
        Map<String,String> map = new HashMap<>();
        String s1 =  map.put("a","1");
        System.out.println(s1); // 输出 null 因为存放的key为a，在map中还没有a这个key，所以返回对应值就为null
        String s2 = map.put("a","2");
        System.out.println(s2); // 输出字符串 1 因为存放key为a，由于map中已经有a这个key了，key唯一，所以将旧value替换，将旧value值返回
    }
    
    public static void showTwo(){
        Map<String,Integer> map = new HashMap<>();
        map.put("a",1);
        map.put("b",2);
        map.put("c",3);
        Integer in1 = map.remove("b");
        System.out.println(in1); // 输出 2
        int in1 = map.remove("b");
        System.out.println(in1); // 抛出异常，因为在map中已经没有b这个key，所以会返回null，由于int类型不能接收null值，所以就会报错。建议使用对象类型接收返回值。 这里可以使用int是因为自动拆箱。
    }
    
    public static void showThree(){
        Map<String,Integer> map = new HashMap<>();
        map.put("a",1);
        map.put("b",2);
        map.put("c",3);
    }
}
```



##### keySet方法遍历

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/keySet方法遍历.png)

```java
public calss Test{
    public static void main(String[] args){
        showOne();
    }
    
    public static void showOne(){
        Map<String,Integer> map = new HashMap<>();
        map.put("a",1);
        map.put("b",2);
        map.put("c",3);
        
        Set<String> set = map.keySet();
        // 迭代器遍历
        Iterator<String> it = set.iterator();
        while(it.hasNext()){
            Integer value = map.get(it.next());
            System.out.println(it.next()+" = "+value);
        }
        // 增强for循环遍历
        for(String s: map.keySet()){
            Integer value = map.get(s);
            System.out.println(s+" = "+value);
        }
    }
}
```



##### Entry键值对对象以及遍历Map (推荐该方法遍历，提高效率)

`Map`中存放的是两种对象，一种为**key**，一种为**value**，它们在`Map`中是一一对应关系，这一对对象又称做`Map`中的一个`Entry(项)`。`Entry`将键值对的对应关系封装成了一个对象。即键值对对象，这样我们在遍历`Map`集合时，就可以从每一个键值对对象中获取对应的键与对应的值。

既然Entry表示了一对键值对，那么也同样提供了获取对应键值的方法：

- `public K getKey()` 获取Entry对象中的键
- `public V getValue()` 获取Entry对象中的值

在Map集合中提供了获取所有Entry对象的方法：

- `public Set<Map.Entry<K,V>> entrySet()` 获取到Map中所有键值对对象的集合(Set集合)

  Map.Entry< K, V >  通过Map外部类找到内部类确定一个类型( Entry是Map的一个内部类 )

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/Entry对象.png)



```java
public class Test{
    public static void main(String[] args){
        
    }
    
    public static void showForEntry(){
        Map<String,Integer> map = new HashMap<>();
        map.put("a",1);
        map.put("b",2);
        map.put("c",3);
        
        Set< Map.Entry<String,Integer> > set = map.entrySet();
        // 迭代器遍历
        Iterator< Map.Entry<String,Integer> > it = set.iterator();
        while(it.hasNext()){
            Map.Entry<String,Integer> entry = it.next();
            String s = entry.getKey();
            Integer i = entry.getValue();
            System.out.println(s+" = "+v); // 输出每一个键值对
        }
        // for增强循环
        for(Map.Entry<String,Integer> entry : set){
            String s = entry.getKey();
            Integer i = entry.getValue();
            System.out.println(s+" = "+v); // 输出每一个键值对
        }
    }
}
```



##### HashMap存储自定义类型键值

练习：每位学生（姓名，年龄）都有自己的家庭住址。将学生对象和家庭住址存储到map集合中。学生作为key，家庭作为value。

> 注意，学生的姓名和年龄都相同，则为同一名学生。

Student.java

```java
public class Student{
    private String name;
    private int age;
    
    public Person() {
    }

    public Person(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
    
    // 因为不能存入同一名学生，所以必须重写hashCode和equals方法
    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name) &&
                Objects.equals(age, person.age);
    }

}
```

Test.java

```java
public class Test{
    public static void main(String[] args){
        show();
    }
    
    public static void show(){
        Map<Student,String> map = new HashMap<>();
        map.put(new Student("a",18), "成都市武侯区");
        map.put(new Student("b",38), "成都市青羊区");
        map.put(new Student("c",28), "成都市武侯区");
        map.put(new Student("c",28), "成都市武侯区");
        
        Set< Map.Entry<Student,String> > set = map.entrySet();
        for(Map.Entry<Student,String> entry : set){
            Student stu = entry.getKey();
            String str = entry.getValue();
            System.out.println(stu+" --- "+str);
        }
    }
}
```

> **TIP**
>
> 建议使用entry方式进行遍历，因为entry已经把k-v取出来了。而使用keySet遍历，还要到map集合中获取value值，效率比较低。



#### 3、LinkedHashMap<K,V> 集合 JDK 1.2

LinkedHashMap类继承了HashMap类，该类可以保证元素的存取的顺序。底层原理是，Hash表+链表(记录元素的顺序)。

特点：

1. key值不能重复
2. 存取有顺序

```java
public class Test{
    public static void main(String[] args){
        test();
    }
    
    public static void test(){
        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put("a","1");
        hashMap.put("c","3");
        hashMap.put("b","2");
        System.out.println("hashMap:"+hashMap); // 输出结果是没有任何顺序的
        
        LinkedHashMap<String,String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("a","1");
        linkedHashMap.put("c","3");
        linkedHashMap.put("b","2");
        System.out.println("linkedHashMap:"+linkedHashMap); // 输出结果与存入的顺序一致
    }
}
```



#### 4、Hashtable<K,V> 集合 JDK 1.0 

**线程安全**

`java.util.Hashtable<K,V>`  最早的双列集合。它与新集合实现不同， `Hashtable`是线程安全的集合，同步的(单线程)。键和值不能为null。在JDK1.2之后，	才改造为实现Map接口。

Hashtable存入null值，则会报异常：`NullPointerException`

```java
public static void main(String[] args){
    HashMap<String,String> hashMap = new HashMap<>();
    hashMap.put(null,"a");
    hashMap.put("c","d");
    hashMap.put(null,null);
    System.out.println(hashMap); // 输出 {null=null,c=d}
    
    Hashtable<String,String> hashTable = new Hashtable<>();
    hashTable.put(null,"a");
    System.out.println(hashTable); // 报异常 NullPointerException 空指针异常
    //hashTable.put("b",null);
    //System.out.println(hashTable); // 报异常 NullPointerException 空指针异常
}
```



#### 5、TreeMap<K,V> 集合 JDK1.2

一个**红黑树**基于`NavigableMap`实现。**该类不同步，线程不安全。**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/treemap.png)

LinkedHashMap是按照插入顺序排序，而TreeMap是按照Key的自然顺序或者Comprator的顺序进行排序。在实现原理上LinkedHashMap是双向链表，TreeMap是红黑树。TreeMap还有个好兄弟叫TreeSet，实现原理是一样的。



#### 6、==重要==负载因子，数组长度在2的次方，当链表长度>=8时扩容成红黑树？



- **负载因子**

  当我们将负载因子不定为0.75的时候（两种情况）：

  **1、 假如负载因子定为1（最大值）**，那么只有当元素填满组长度的时候才会选择去扩容，虽然负载因子定为1可以最大程度的提高空间的利用率，但是会增加hash碰撞，以此可能会增加链表长度，因此查询效率会变得低下（因为链表查询比较慢）。hash表默认数组长度为16，好的情况下就是16个空间刚好一个坑一个，但是大多情况下是没有这么好的情况。

  >  **结论：所以当加载因子比较大的时候：节省空间资源，耗费时间资源**

  

  **2、加入负载因子定为0.5（一个比较小的值）**，也就是说，直到到达数组空间的一半的时候就会去扩容。虽然说负载因子比较小可以最大可能的降低hash冲突，链表的长度也会越少，但是空间浪费会比较大。

  > **结论：所以当加载因子比较小的时候：节省时间资源，耗费空间资源**

  

  但是我们设计程序的时候肯定是会在空间以及时间上做平衡，那么我们能就需要在时间复杂度和空间复杂度上做折中，选择最合适的负载因子以保证最优化。所以就选择了0.75这个值，Jdk那帮工程师一定是做了大量的测试，得出的这个值吧~
  
  

- **hash表的数组长度总在2的次方**

  **1：**

  ```java
  // WeakHashMap.java 源码：
  /**
  * Returns index for hash code h.
  */
  private static int indexFor(int h, int length) {
      return h & (length-1);
  }
  ```

  扩容也是以2的次方进行扩容，是因为2的次方的数的二进制是10..0，在二的次方数进行减1操作之后，二进制都是11...1，那么和hashcode进行与操作时，数组中的每一个空间都可能被使用到。

  如果不是2的次方，比如数组长度为17，那么17的二进制是10001，在indexFor方法中，进行减1操作为16，16的二进制是10000，随着进行与操作，很明显，地址二进制数末尾为1的空间，不会得到使用，比如地址为10001，10011，11011这些地址空间永远不会得到使用。因此就会造成大量的空间浪费。

  所以必须得是2的次方，可以合理使用数组空间。

  **2：**

  ```java
  扩容临界值 = 负载因子 * 数组长度
  ```

  负载因子是0.75即3/4，又因为数组长度为2的次方，那么相乘得到的扩容临界值必定是整数，这样更加方便获得一个方便操作的扩容临界值。

  

- **当链表长度>=8时构建成红黑树**

  ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/红黑树8长度产生.png)

  利用泊松分布计算出当链表长度大于等于8时，几率很小很小

  当put进来一个元素，通过hash算法，然后最后定位到同一个桶（链表）的概率会随着链表的长度的增加而减少，当这个链表长度为8的时候，这个概率几乎接近于0，所以我们才会将链表转红黑树的临界值定为8。
  
  > **tips：了解红黑树，请移步至Java数据结构与算法：红黑树 AVL树.md**



#### 7、==重要==为什么jdk8，hashmap底层会用红黑树，而不使用AVL树？

> 首先需要了解什么是红黑树，什么是AVL树。请移步至Java数据结构与算法：红黑树 AVL树.md

红黑树和AVL树增删改查的时间复杂度平均和最坏情况都是在**O(lgN)**，包括但不超过。

**红黑树性质：**

1. 节点不是黑色就是红色
2. 根节点必须为黑色
3. 不能有两个连续红色节点
4. 叶子节点是黑色
5. 从根节点到叶子节点经过的黑节点数量相同

特点：最长路径不会超过最短路径的2倍。

**AVL性质：**

1. 任何节点的两个子树的高度最大差别为1



在jdk8中hashmap的hash表桶中的链表长度大于8时，会将链表转为红黑树。虽然红黑树与AVL树的时间复杂度都为O(lgN)，但是在调整树上面花费的时间相差很大。因为AVL树是平衡二叉树，要求严苛，任何节点的两个子树的高度最大差别为1，因此每次插入一个数或者删除一个数，最坏情况下，会使得AVL树进行很多次调整，为了保证符合AVL树的规则，调整时间花费较多。而红黑树，在时间复杂度上与AVL树相持平，但是在调整树上没有AVL树严苛，它允许局部很少的不完全平衡，但最长路径不会超过最短路径的2倍，这样以来，最多只需要旋转3次就可以使其达到平衡，调整时间花费较少。

最重要的一点，在JUC中有一个`CurrentHashMap`类，该类为线程同步的hashmap类，当高并发时，需要在意的是时间，由于AVL树在调整树上花费的时间相对较多，因此在调整树的过程中，其他线程需要等待的时间就会增长，这样导致效率降低，所以会选择红黑树。

总结：在增加、删除的时间复杂度相同的情况下，调整时间相对花费较少的是红黑树，因此选择红黑树。



#### 8、==重要==既然红黑树那么好，为什么不一来就使用红黑树？

因为经过泊松定律知道，一个在负载因子为0.75时，出现的hash冲突，在一个桶中的链表长度大于8的几率是很少很少几乎为0，如果一来就使用红黑树，由于增删频繁，从而会调整树的结构，反而增加了负担，浪费时间，而直接使用链表增删反而比红黑树快很多，因此为了增加效率，而只是在长度大于8时使用红黑树。



#### 9、==重要== hashmap在get和put的时候为什么使用尾插法，而摒弃了头插法？

这是因为多线程并发操作下，可能形成环化。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/头插法01.png" style="float:left;" />

比如`线程T1`将要添加一个B元素进来，此时`线程T2`正在resize，达到了扩容临界值，所以需要重计算，在重计算中，`线程T1`的B元素插在了A元素的头上：

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/头插法03.png" style="float:left;" />

由于`线程T2`重计算数组长度后，扩容之后，在旧数组中同一条Entry链上的元素，通过重新计算索引位置后，有可能被放到了新数组的不同位置上。所以，元素A可能会插到元素B头上，形成了环状，死循环：

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/头插法04.png" style="float:left;" />

为了解决这个问题，在jdk8之后就使用了**尾插法**！最终不会形成环化。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/头插法02.png" style="float:left;" />

#### 10. ==重要==虽然尾插法解决了这个问题，为什么在高并发下还是不能使用hashmap呢？

因为在hashmap中，没有锁的化，高并发下一个线程put的值，另一个线程可能不能及时get到最新put的值。所以要使用currentHashMap，用的锁+尾插法



### 练习：计算一个字符串每个字符出现的次数

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/字符串每个字符出现的次数.png)

```java
public class Test{
    public static void mian(String[] args){
        forTest01();
    }
    
    public static void forTest01(){
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        
        char[] charArray = str.toCharArray();
        HashMap<Character,Integer> hashMap = new HashMap<>();
        for(int i = 0; i < charArray.length(); i++){
            char c = charArray.get(i);
            if( hashMap.containsKey(c) ){
                Integer in = hashMap.get(c);
                ++in;
                hashMap.put(c,in);
            }else{
                hashMap.put(c,1);
            }
        }
        
        Set<Map.Entry<Character,Integer>> set = hashMap.entrySet();
        for(Map.Entry<Character,Integer> entry : set){
            System.out.println(entry.getKey()+"---"+entry.getValue());
        }
    }
    
    public static void forTest02(){
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        HashMap<Character,Integer> hashMap = new HashMap<>();
        for(char c : str.toCharArray()){
            if( hashMap.containsKey(c) ){
                Integer in = hashMap.get(c);
                ++in;
                hashMap.put(c,in);
            }else{
                hashMap.put(c,1);
            }
        }
        
        for(Character key : hashMap.keySet()){
            Integer value = hashMap.get(key);
            System.out.println(key+"---"+value);
        }
    }
}
```





### JDK9 的新特性之一 of()方法

JDK9的新特性：

List接口，Set接口，Map接口：里面增加了一个静态方法of，可以给集合一次性添加多个元素

当集合中存储的元素的个数已经确定了，要确定元素已经不能再改变的情况下使用。

注意：

1. of方法只适用于List接口，Set接口，Map接口，不适用于接口的实现类
2. of方法的返回值是一个不能改变的集合，集合不能再使用add、put方法添加元素，会抛出`UnsupportedOperationException`异常
3. Set接口和Map接口调用of方法的时候，不能有重复的值或key值，否则会抛出`IllegalArgumentException`异常

```java
List<String> list = List.of("1","2","3");
Set<String> set = Set.of("a","b","c");
Map<String,Integer> map = Map.of("a",1,"b",2,"c",3);
```



### 练习：斗地主发牌案例

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/斗地主发牌案例.png)



```java
package com.LFJava.doudizhu;

import java.util.*;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/2/24
 * Time: 18:00
 * Description:
 * Version: V1.0
 */
public class Doudizhu {
    public static void main(String[] args) {
        // 准备牌的花色以及数字
        List<String> listType = new ArrayList<>();
        List<String> listNmuber = new ArrayList<>();
        Collections.addAll(listType,"♣","♦","♥","♠");
        Collections.addAll(listNmuber,"2","A","K","Q","J","10","9","8","7","6","5","4","3");

        // 将花色与数字拼接在一起，存入到map双列集合中
        Map<Integer,String> poker = new HashMap<>();
        int index = 0;
        poker.put(index,"大王");
        ++index;
        poker.put(index,"小王");
        ++index;
        for (String number : listNmuber) {
            for(String type : listType){
                poker.put(index, type + number);
                ++index;
            }
        }
        //System.out.println(poker);

        // 将map中的key值取出来，存入到list集合中，并将key值的顺序打乱
        List<Integer> numbers = new ArrayList<>();
        Set<Integer> set = poker.keySet();
        for (Integer nums: set) {
            numbers.add(nums);
        }
        Collections.shuffle(numbers);
        //System.out.println(numbers);

        // 将打乱的key值，存放到每个玩家的list集合中
        List<Integer> diPai = new ArrayList<>();
        List<Integer> player01 = new ArrayList<>();
        List<Integer> player02 = new ArrayList<>();
        List<Integer> player03 = new ArrayList<>();
        for (int i = 0; i < numbers.size() ; i++) {
            if( i >= 51){ // 一定要先将底牌存进去
                diPai.add(numbers.get(i));
            }else if( i % 3 == 0){
                player01.add(numbers.get(i));
            }else if( i % 3 == 1){
                player02.add(numbers.get(i));
            }else if( i % 3 == 2){
                player03.add(numbers.get(i));
            }
        }
        /*System.out.println(diPai);
        System.out.println(player01);
        System.out.println(player02);
        System.out.println(player03);*/

        // 发牌
        show("heroC",poker,player01);
        show("yikeX",poker,player02);
        show("Vincent",poker,player03);
        show("底牌",poker,diPai);

    }

    // 该方法，将每个玩家的存key值的list集合，先将key值排序，再将每个key值遍历出来，找到对应的map里的value值
    public static void show(String playerName, Map<Integer,String> poker, List<Integer> playerList){
        System.out.print(playerName+":");
        Collections.sort(playerList);
        for(Integer i : playerList){
            String s = poker.get(i);
            System.out.print("  "+s+"  ");
        }
        System.out.println();
    }
}
```

输出结果：

```text
heroC: 大王 小王 ♦A ♠A ♣J ♥10 ♠10 ♦9 ♥9 ♣8 ♦7 ♣6 ♠6 ♣5 ♠5 ♣3 ♦3  
yikeX: ♣2 ♥2 ♦K ♣Q ♦Q ♥Q ♠Q ♦J ♥J ♦10 ♣9 ♠9 ♦8 ♦5 ♠4 ♥3 ♠3  
Vincent: ♦2 ♠2 ♣A ♥A ♥K ♠K ♠J ♣10 ♥8 ♣7 ♥7 ♠7 ♦6 ♥6 ♥5 ♦4 ♥4  
底牌: ♣K ♠8 ♣4 
```



## 三、迭代器

### 1、Iterator\<E>集合 接口 JDK 1.2

在程序开发中，经常需要遍历集合中的所有元素。针对这种需求，JDK专门提供了一个接口`java.util.Iterator`。`Iterator`接口也是Java集合的一员，但它与`Collection`、`Map`接口不同，该两集合主要用于存储元素，而`Iterator`主要用于迭代访问(遍历)`Collection`中的元素，因此`Iterator`对象也被称之为迭代器。

想要遍历Collection集合，那么就要获取该集合迭代器完成迭代操作：

- `public Iterator iterator()`：获取集合对应的迭代器，用来遍历集合中的元素。

**迭代**：即Collection集合元素的通用获取方式。在取元素之前要判断集合中有没有元素，如果有，就把这个元素取出来，继续在判断，如果还有就再取出来。直到所有元素被取出。这种方法专业术语成为迭代。

由于Collection抽象类，没有提供下标，要遍历元素，就得使用Iterator迭代器来遍历。

#### 常用方法

- `public boolean hasNext()`：迭代中是否还有下一个元素
- `public E next()`：返回迭代中的下一个元素

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/迭代器.png)

```java
public class TestIterator{
    public static void main(String[] args){
        Collection<String> coll = new ArrayList<>();
        coll.add("A");
        coll.add("B");
        coll.add("C");
        
        // 通过while循环迭代获取元素
        Iterator<String> iterator = coll.iterator(); // 实列化迭代器，指针指向 -1
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
        
        // 通过for循环迭代获取元素 hasNext()判断之后指针会自动下移
        for(Iterator<String> it = coll.iterator(); it.hasNext();){
            System.out.println(iterator.next());
        }
    }
}
```



### 2、Enumeration\<E> 集合 接口 JDK 1.0

枚举迭代与Iterator迭代类似，这是JDK1.0遍历Vector集合的接口。

注意：该接口的功能由Iterator接口复制。  此外，Iterator还添加了一个可选的删除操作，并具有较短的方法名称。  新的实现应该考虑使用迭代器优先于枚举。

JDK1.2以上基本不用该接口来遍历获取元素了。

#### 常用方法

- `boolean hasMoreElements()`：测试该枚举是否包含更多元素
- `E nextElement()`：如果此枚举对象至少有一个要提供的元素，则返回此枚举的下一个元素。



## 四、Properties 类 属性集与IO流结合

`java.util.Properties`继承于`Hashtable` ，来表示一个持久的属性集。它使用简直结构存储数据，每一个键及其对应的值都是一个字符串。该类也被许多java类使用，比如获取系统属性，`System.getProperties`方法就是返回一个`Properties`对象。

`Properties`类表示一组持久的属性。  `Properties`可以保存到流中或从流中加载。唯一和IO流相结合的集合

- 可以使用Properties的方法**store**，将集合中的临时数据，持久化存储到硬盘文件中
- 可以使用Properties的方法**load**，将硬盘中文件保存的键值对，读取到集合中使用

始于：JDK 1.0



### 1、构造方法

- `public Properties()`创建一个空的属性列表。
- `public Properties(Properties default)`创建一个具有指定默认值的空属性列表。



### 2、常用方法

- `public Object setProperties(String key, String value)` 保存一对属性
- `public String getProperties(String key)` 使用此属性列表中指定的键搜索属性值
- `public Set<String> stringPropertiesName()` 所有键的名称集合



- `public void load(InputStream inStream) throws IOException` 将硬盘中的数据通过字节IO流加载到集合中
- `public void load(Reader reader) throws IOException` 将硬盘中的数据通过字符IO流加载到集合中
- `pubilc void store(OutputStream outStream, String comments) throws IOException` 将集合中的数据通过字节IO流输出存入到硬盘文件中。第二个参数为注释，给该存储给一个注释(不能使用中文，会乱码，默认UNICODE编码)，一般使用空字符串
- `public void store(Writer writer, String comments) throws IOException` 将集合中的数据通过字符IO流输出存入到硬盘文件中。第二个参数为注释，给该存储给一个注释(不能使用中文，会乱码，默认UNICODE编码)，一般使用空字符串

```java
public class PropertiesTest {
    public static void main(String[] args){
        ioInProperties();
    }

    public static void forProperties(){
        // 向Properties存储数据
        Properties properties = new Properties();
        properties.setProperty("heroC","1130");
        properties.setProperty("yikeX","0405");
        properties.setProperty("turbo30","2019");
        // 遍历Properties
        Set<String> strings = properties.stringPropertyNames();
        for (String s : strings){
            System.out.println("key " + s + "---" + "value " + properties.getProperty(s) );
        }
    }

    public static void ioOutProperties(){
        // 方法store有两个，字节流存储(不能读取中文)和字符流存储
        // 存储到硬盘中是采用unicode编码
        try(FileOutputStream fileOutputStream = new FileOutputStream("d.txt")){
            Properties properties = new Properties();
            properties.setProperty("heroC","1130");
            properties.setProperty("yikeX","0405");
            properties.store(fileOutputStream,"");
        }catch (IOException e){
            System.out.println("IO有异常");
        }
    }

    public static void ioInProperties(){
        // 方法load有两个，字节流读取(不能读取中文)和字符流读取
        try(FileReader fileReader = new FileReader("d.txt")){
            Properties properties = new Properties();
            properties.load(fileReader);
            Set<String> strings = properties.stringPropertyNames();
            for (String key : strings) {
                System.out.println(key + "=" + properties.get(key));
            }
        }catch (IOException e){
            System.out.println("IO有异常");
        }
    }
}
```



## 五、

### 1、快速失败（fail—fast）

在用迭代器遍历一个集合对象时，如果遍历过程中对集合对象的内容进行了修改（增加、删除、修改），则会抛出 Concurrent Modification Exception。

**原理：**迭代器在遍历时直接访问集合中的内容，并且在遍历过程中使用一个 modCount 变量。集合在被遍历期间如果内容发生变化，就会改变 modCount 的值。每当迭代器使用 hashNext()/next() 遍历下一个元素之前，都会检测 modCount 变量是否为 expectedmodCount 值，是的话就返回遍历；否则抛出异常，终止遍历。

**注意：**这里异常的抛出条件是检测到 **modCount != expectedmodCount** 这个条件。如果集合发生变化时修改 modCount 值刚好又设置为了 expectedmodCount 值，则异常不会抛出。因此，不能依赖于这个异常是否抛出而进行并发操作的编程，这个异常只建议用于检测并发修改的 bug。

**场景：**java.util 包下的集合类都是快速失败的，不能在多线程下发生并发修改（迭代过程中被修改）。

### 2、安全失败（fail—safe）

采用安全失败机制的集合容器，在遍历时不是直接在集合内容上访问的，而是先复制原有集合内容，在拷贝的集合上进行遍历。

**原理：**由于迭代时是对原集合的拷贝进行遍历，所以在遍历过程中对原集合所作的修改并不能被迭代器检测到，所以不会触发 Concurrent Modification Exception。

\>缺点：基于拷贝内容的优点是避免了 Concurrent Modification Exception，但同样地，迭代器并不能访问到修改后的内容，即：迭代器遍历的是开始遍历那一刻拿到的集合拷贝，在遍历期间原集合发生的修改迭代器是不知道的。



**场景：**java.util.concurrent 包下的容器都是安全失败，可以在多线程下并发使用，并发修改。