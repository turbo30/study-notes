# JUC 并发编程（重点）

并发编程进阶

**Java中默认有2个线程**：main线程、GC(垃圾回收)线程

Java真的能开启线程吗？ 不能



## 一、概知

JUC：`java.util.concurrent` 包的简称。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/JUC.png" style="float:left;" />

- 获取CPU的核数

```java
public class Test01 {
    public static void main(String[] args) {
        // 获取CPU的核数
        // CPU密集性 IO 密集性
        System.out.println(Runtime.getRuntime().availableProcessors());
    }
}
```

并发编程的本质：充分利用cpu的资源



生产者与消费者问题



## 二、Lock 接口 (重点)

常用传教Lock的方法：

`Lock lock = new ReentrantLock()`



### 1、ReentrantLock 类 

实现了 Lock接口

**构造方法：**

```java
// ReentrantLock类有两个构造器
public ReentrantLock() {
    sync = new NonfairSync(); // 获得非公平锁
}

public ReentrantLock(boolean fair) {
    sync = fair ? new FairSync() : new NonfairSync(); // 如果为true则获得公平锁
}
```

默认构造器创建一个非公平锁，传入true则会获得公平锁

**非公平锁**：其他线程可以插队，可实现花费时间少的线程可以优先使用

**公平锁**：当线程被cpu调用了，那么就必须得去执行，不能被改变

```java
// 固定写法 try catch Finally
public class LockTest {
    public static void main(String[] args) {
        SaleTickets saleTickets = new SaleTickets();
        new Thread(()-> {for (int i = 0; i < 50; i++) saleTickets.sale();},"A").start();
        new Thread(()-> {for (int i = 0; i < 50; i++) saleTickets.sale();},"B").start();
        new Thread(()-> {for (int i = 0; i < 50; i++) saleTickets.sale();},"C").start();
    }
}

class SaleTickets{
    private int tickets = 50;
    // 创建非公平锁对象
    Lock lock = new ReentrantLock();
    public void sale(){
        // 设置锁
        lock.lock();
        try{
            if(tickets > 0){
                System.out.println(Thread.currentThread().getName() + ": 卖出了第"+ tickets-- + "号票");
            }
        }catch (Exception e){
            System.out.println("异常");
        }finally {
            // 释放锁
            lock.unlock();
        }
    }
}
```



### 2、<u>Lock与Synchronized的区别</u> 面试

1. synchronized 是java内置的关键字。Lock是一个类
2. synchronized 无法判断锁的状态。而Lock锁可以判断锁的状态。
3. synchronized 是自动释放锁。Lock得调用unlock方法释放锁 (如果不释放锁，则会产生死锁状态)。
4. synchronized 如果是有两个线程，有一个线程在执行过程中被阻塞，那么另一个线程就会一直等待。Lock锁则不一定会等待下去 (可通过调用tryLock方法去避免这个问题)。
5. synchronized 可重入锁，不可中断的非公平锁。Lock也是可重入的锁，并可以设置锁的公平与非公平锁
6. synchronized 适合锁少量的代码同步问题。Lock适合锁大量的同步代码。



### 3、<u>防止线程虚假唤醒</u>

**synchronized 来实现防止线程虚假唤醒**

```java
public class SynchTest {
    public static void main(String[] args) {
        Number number = new Number();
        // 创建了3个线程
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    number.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"A").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    number.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"B").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    number.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"C").start();
        
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    number.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"D").start();

    }
}

class Number{
    private int num = 0;

    public synchronized void increment() throws InterruptedException {
        // 如果num不是0，就进入等待状态
        while (num != 0){
            this.wait();
        }
        num ++ ;
        System.out.println(Thread.currentThread().getName() + " ==> " + num);
        // 通知等待中的线程
        this.notifyAll();
    }

    public synchronized void decrement() throws InterruptedException {
        // 如果num是0，就进入等待状态
        while (num == 0){
            this.wait();
        }
        num -- ;
        System.out.println(Thread.currentThread().getName() + " ==> " + num);
        this.notifyAll();
    }
}
```

#### 解决虚假唤醒分析 面试

如上代码，如果在Number类同步代码块中使用`if`判断`num`的值进而进行等待操作，在2个线程之间通信是不会出现虚假唤醒的情况。而如果是大于2个线程，还是使用`if`判断就会出现问题。因为，当调用`this.notifyAll();`时，会唤醒所有等待的线程，唤醒之后，如果时`if`的话，就不会再去判断`num`是否满足条件，会在之前执行等待的代码开始继续往下执行。而使用`while`，线程醒了进入BLOCK状态，被cpu调用之后，还会去判断`num`是否符合要求，直到不符合，才会继续执行`while`以外的代码。这样就保证了线程的安全。`while`循环的作用就是保证了符合要求的才可以进行之后的操作。



### 4、Condition 接口 JDK 1.5

**JUC 线程之间的通信**

synchronized 与 Lock 的对应关系

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/Lock的等待与通知.png)

`java.utils.concurrent.locks interface Condition` 接口中

- `void await() throws InterruptedException`
- `void signal()`
- `void signalAll()`

与传统的wait方法和notify方法没有什么不同，只是这是Lock接口专门使用的

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/Condition.png)

```java
public class LockConditionTest {
    public static void main(String[] args) {
        Numbers number = new Numbers();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    number.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"A").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    number.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"B").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    number.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"C").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    number.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"D").start();
    }
}

class Numbers{
    private int num = 0;
    Lock lock = new ReentrantLock();
    Condition condition = lock.newCondition();

    public void increment() throws InterruptedException {
        lock.lock();
        try{
            // 如果num不是0，就进入等待状态
            while (num != 0){
                condition.await();
            }
            num ++ ;
            System.out.println(Thread.currentThread().getName() + " ==> " + num);
            // 通知等待中的线程
            condition.signalAll();
        }catch (Exception e){
            System.out.println("异常");
        }finally {
            lock.unlock();
        }
    }

    public void decrement() throws InterruptedException {
        lock.lock();
        try{
            // 如果num不是0，就进入等待状态
            while (num == 0){
                condition.await();
            }
            num -- ;
            System.out.println(Thread.currentThread().getName() + " ==> " + num);
            // 通知等待中的线程
            condition.signalAll();
        }catch (Exception e){
            System.out.println("异常");
        }finally {
            lock.unlock();
        }
    }
}
```

这样写，与普通方式没什么区别。



### 5、Condition实现精准通知唤醒

- 可以使线程之间有序的执行，精准的通知和唤醒指定的线程

```java
public class LockConditionTest {
    public static void main(String[] args) {
        Numbers number = new Numbers();
        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                number.printA();
            }
        },"A").start();

        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                number.printB();
            }
        },"B").start();

        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                number.printC();
            }
        },"C").start();
    }
}

class Numbers{
    private int num = 1;  // num=1 线程A执行， num=2 线程B执行， num=3 线程C执行
    Lock lock = new ReentrantLock();
    Condition condition1 = lock.newCondition();
    Condition condition2 = lock.newCondition();
    Condition condition3 = lock.newCondition();

    public void printA(){
        lock.lock();
        // 判断等待 执行 通知
        try{
            // 如果num不等于1，就等待，不执行
            while (num != 1){
                condition1.await();
            }
            num = 2;
            System.out.println(Thread.currentThread().getName());
            condition2.signal(); // 这句话与notify有点区别，该意思是，给condition2发送通知，让condition2去执行
        }catch (Exception e){
            System.out.println("异常");
        }finally {
            lock.unlock();
        }
    }

    public void printB(){
        lock.lock();
        try {
            while ( num != 2){
                condition2.await();
            }
            num = 3;
            System.out.println(Thread.currentThread().getName());
            condition3.signal();
        }catch (Exception e){
            System.out.println("异常");
        }finally {
            lock.unlock();
        }
    }

    public void printC(){
        lock.lock();
        try {
            while ( num != 3){
                condition3.await();
            }
            num = 1;
            System.out.println(Thread.currentThread().getName());
            condition1.signal();
        }catch (Exception e){
            System.out.println("异常");
        }finally {
            lock.unlock();
        }
    }
}

// 线程有顺序的执行并输出：
// A
// B
// C
// ...
```



### 6、关于锁的问题 面试

```java
// 1. 哪个语句先输出
public class LockProblem8 {
    public static void main(String[] args) {
        Info info = new Info();

        new Thread(()->info.msg()).start();

        try {
            TimeUnit.SECONDS.sleep(5); // 使已启动的线程睡眠5秒，常用的方法
        } catch (InterruptedException e) {
            System.out.println("异常");
        }

        new Thread(()->info.call()).start();
    }
}

class Info{
    public synchronized void msg(){
        System.out.println("发短信...");
    }
    public synchronized void call(){
        System.out.println("打电话...");
    }
}
// 这里的锁为同一个对象锁，两个线程的锁都是info这个对象
// 先输出 发短信... 再输出 打电话...
```



```java
// 2 哪个语句先输出
public class LockProblem8 {
    public static void main(String[] args) {
        Info info1 = new Info();
        Info info2 = new Info();

        new Thread(()->info1.msg()).start();

        new Thread(()->info2.call()).start();
    }
}

class Info{

    public synchronized void msg(){
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            System.out.println("异常");
        }
        System.out.println("发短信...");
    }

    public synchronized void call(){
        System.out.println("打电话...");
    }
}
// 因为两个线程都是非同一把锁，锁对象都不一样。因为不是同样的所对象，所以互不影响。又因为msg()方法有睡眠
// 先输出 打电话... 在输出 发短信...
```



```java
// 3 哪个语句先输出
public class LockProblem8 {
    public static void main(String[] args) {
        Info info1 = new Info();
        Info info2 = new Info();

        new Thread(()->info1.msg()).start();

        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            System.out.println("异常");
        }

        new Thread(()->info2.call()).start();
    }
}

class Info{
    public static synchronized void msg(){
        System.out.println("发短信...");
    }

    public static synchronized void call(){
        System.out.println("打电话...");
    }
}
// 因为同步代码被static修饰，所以锁对象都是Info的Class对象，在类加载器加载的时候就生成了的
// 所以先输出 发短信... 在输出 打电话...
```



```java
// 4 哪个语句先输出
public class LockProblem8 {
    public static void main(String[] args) {
        Info info1 = new Info();
        Info info2 = new Info();

        new Thread(()->info1.msg()).start();

        new Thread(()->info2.call()).start();
    }
}

class Info{
    public static synchronized void msg(){
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            System.out.println("异常");
        }
        System.out.println("发短信...");
    }

    public void call(){
        System.out.println("打电话...");
    }
}
// 由于第二个线程调用的是普通方法，没有锁的竞争
// 所以先输出 打电话... 在输出 发短信...
```



## 三、<u>解决集合类线程不安全</u>

使用大部分集合 会报异常 `ConcurrentModificationException`(并发修改异常) 线程不同步原因。



#### 解决集合同步

**关于List集合**

1. 使用`Vector`集合，继承了List集合，始于jdk1.0

   Vector之所以线程安全，是因为在每个方法上添加了关键字synchronized

   ```java
   public synchronized boolean add(E e) {
       modCount++;
       ensureCapacityHelper(elementCount + 1);
       elementData[elementCount++] = e;
       return true;
   }
   ```

   

2. `Collections.synchronizedList(new ArrayList<>()); ` 

   通过Collections集合的工具类，包装集合，达到线程安全。只不过**它不是加在方法的声明处，而是方法的内部**。

   ```java
   SynchronizedCollection(Collection<E> c) {
       this.c = Objects.requireNonNull(c);
       mutex = this;
   }
   SynchronizedCollection(Collection<E> c, Object mutex) {
       this.c = Objects.requireNonNull(c);
       this.mutex = Objects.requireNonNull(mutex);
   }
   
   public int size() {
       synchronized (mutex) {return c.size();}
   }
   public boolean isEmpty() {
       synchronized (mutex) {return c.isEmpty();}
   }
   ```

   可以看到通过synchronized同步锁，套了起来。

   

3. `new CopyOnWriteArrayList<>();` 

   JUC中的类，JUC可解决并发线程安全问题。顾名思义：写入时复制。多个线程，每个线程在写入时，将写入的数据进行复制，然后再插入到集合中，保证其他线程写入的数据不被覆盖。

   源码：

   ```java
    private transient volatile Object[] array;
   ```

   遍历`Vector/SynchronizedList`是需要自己手动加锁的。

   CopyOnWriteArrayList使用迭代器遍历时不需要显示加锁，看看`add()、clear()、remove()`与`get()`方法的实现可能就有点眉目了。

   ```java
   public boolean add(E e) {   
       // 加锁
       final ReentrantLock lock = this.lock;
       lock.lock();
       try {
   
           // 得到原数组的长度和元素
           Object[] elements = getArray();
           int len = elements.length;
   
           // 复制出一个新数组
           Object[] newElements = Arrays.copyOf(elements, len + 1);
   
           // 添加时，将新元素添加到新数组中
           newElements[len] = e;
   
           // 将volatile Object[] array 的指向替换成新数组
           setArray(newElements);
           return true;
       } finally {
           lock.unlock();
       }
   }
   
   ```

   通过代码我们可以知道：在添加的时候就上锁，并**复制一个新数组，增加操作在新数组上完成，将array指向到新数组中**，最后解锁。

   【总结】

   - **在修改时，复制出一个新数组，修改的操作在新数组中完成，最后将新数组交由array变量指向**。
   - **写加锁，读不加锁**

---

**关于Set集合**

1. `Collections.synchronizedSet(Set<E> set)` 方法解决并发
2. `CopyOnWriteArraySet ` 类解决并发

原理与List集合一样

---

**关于Map集合**

1. `Collections.synchronizedMap(Map<K,V> map)`

2. `ConcurrentHashMap<K,V>()`

   [要知道ConcurrentHashMap原理][https://blog.csdn.net/weixin_44460333/article/details/86770169]



#### 解决并发几个方法的区别

- Vector被CopyOnWriteArrayList替代，是因为Vector在每个方法都是使用的Synchronized关键字，而CopyOnWriteArrayList是使用的Lock锁，因此后者效率高很多
- Vector和Collections都是使用的Synchronized关键字，前者在方法上，后者在方法中使用。并且两个都在原集合进行操作。



**JUC解决并发代码：**

利用循环创建多个线程，每个线程都需要往list集合中添加数据，模拟高并发

```java
public class Test01{
    public static void main(String[] args) {
        // JUC 解决
        List<String> list = new CopyOnWriteArrayList<>();

        for (int i = 1; i <= 10; i++) {
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,4));
                System.out.println(Thread.currentThread().getName() + list);
            },String.valueOf(i)).start();
        }
    }
}
```



## 四、Callable 进阶 FutureTask

`java.util.concurrent interface Callable<V> `

函数式接口，只有一个call方法。该接口与Runnble类似。只不过该接口有返回值，可抛出异常。

Thread类没有关于Callable的构造方法。因此Callable只能通过Runnable实现类`java.utils.concurrent.FutureTask<V>`的构造方法，与Thread类进行连接

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/Callable.png)

```java
public class CallableUpTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<String> futureTask = new FutureTask<>(new CallableUp());
        new Thread(futureTask).start(); // 创建Callable线程
        // 两个线程去执行Callable中的call方法，只打印一次call，是因为有缓存
        new Thread(futureTask).start(); 
        
        // 获取返回值，可能会产生阻塞，是因为在执行call方法时，可能时间很长，一般最后去获取返回值
        System.out.println(futureTask.get()); 
    }
}

class CallableUp implements Callable<String>{
    @Override
    public String call() throws Exception {
        System.out.println("call");
        return "12345";
    }
}
// 输出
// call
// 12345
```

**细节：**

- Callable有缓存
- 获取返回值可能会发生阻塞



## 五、JUC常用工具(组件)类 JDK 1.5

都实现了AQS类

### 1、CountDownLatch

`java.util.concurrent.CountDownLatch ` 类，是一个减计数器

#### 构造方法

- `public CountDownLatch(int count)` 设置计数的总数

#### 常用方法

- `public void await() throws InterruptedException` 等待计数为0，计数为0，才可以执行之后的代码

- `public boolean await(long timeout,TimeUnit unit) throws InterruptedException` 

  timeout：等待时间

  unit：timeout参数的时间单位

  返回结果：true计数为0；false到达了时间且计数还没有为0

- `public void countDown()` 计数减1

- `public long getCount()` 返回当前计数

```java
public class CountDownLatchTest {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(6); // 设置总数为6

        for (int i = 1; i <= 6; i++) {
            new Thread(()->{
                countDownLatch.countDown(); // -1
                System.out.println(Thread.currentThread().getName() + " Go out...");
            },String.valueOf(i)).start();
        }

        countDownLatch.await(); // 当计数为0，才会执行下面的代码

        System.out.println("所有线程都执行完毕了...");
    }
}
```

**输出：**

```
1 Go out...
2 Go out...
3 Go out...
4 Go out...
5 Go out...
6 Go out...
所有线程都执行完毕了...
```



### 2、CyclicBarrier

`java.util.concurrent.CyclicBarrier ` 类，是一个加计数器

#### 构造方法

- `public CyclicBarrier(int parties)` 设置一个需要到达数量的值，到达了parties这个值，不会执行特定的动作
- `public CyclicBarrier(int parties, Runnable barrierAction)` 设置一个需要到达数量的值，到达了parties这个值，会执行特定的动作

#### 常用方法

- `public int await() throws InterruptedException,BrokenBarrierException` await方法，该线程进入阻塞状态，屏障值就+1，阻塞线程到达了屏障设定的parties数，就会执行构造器中第二参数barrierAction的动作(第二个参数是Runable接口的实现，通过调用run方法执行)
- `public int getParties()` 获得需要达到parties的值

```java
public class CyclicBarrierTest {
    public static void main(String[] args) {
        // 创建一个加计数器
        CyclicBarrier cyclicBarrier = new CyclicBarrier(6,()-> System.out.println("所有规定的线程都进入到了阻塞状态，达到了屏障"));

        for (int i = 1; i <= 6; i++) {
            final int num = i;
            new Thread(()->{
                System.out.println(Thread.currentThread().getName() + " 执行了");
              // await方法，该线程进入阻塞状态，屏障值就+1，阻塞线程到达了屏障设定6，就会执行构造器中第二参数的动作(第二个参数是Runable接口的实现，通过调用run方法执行)
                try {
                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " 继续执行");
            }, String.valueOf(i)).start();
        }
    }
}
```

**输出：**

```
1 执行了
2 执行了
3 执行了
4 执行了
5 执行了
6 执行了
所有规定的线程都进入到了阻塞状态，达到了屏障
6 继续执行
1 继续执行
4 继续执行
3 继续执行
2 继续执行
5 继续执行
```



### 3、Semaphore

`java.util.concurrent.Semaphore` 类，就是一个许可证(信号量)

#### 构造方法

- `public Semaphore(int permits)` 设置许可证的个数及非公平公平设置
- `public Semaphore(int permits,boolean fair)` 设置许可证的个数及给定公平设置

#### 常用方法

- `public void acquire() throws InterruptedException` 获得许可证，未获取到的线程等待。
- `public void release()` 释放许可证

```java
public class SemaphoreTest {
    public static void main(String[] args) {
        // 设置许可证个数。 用于 限流！
        Semaphore semaphore = new Semaphore(3);
        for (int i = 1; i <= 6; i++) {
            new Thread(()->{
                try {
                    // 线程获取许可证，未获取到的等待。
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName() + " 获得许可证");
                    TimeUnit.SECONDS.sleep(2);
                    System.out.println(Thread.currentThread().getName() + " 释放许可证");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    // 释放许可证
                    semaphore.release();
                }
            }, String.valueOf(i)).start();
        }
    }
}
```



### 4、Exchanger

Exchanger类源于java.util.concurrent包，它可以在两个线程之间传输数据，Exchanger中的`public V exchange(V x)`方法被调用后等待另一个线程到达交换点（如果当前线程没有被中断），然后将已知的对象传给它，返回接收的对象。

如果担心线程一直等待，可以使用`exchange(V x, long timeout, TimeUnit unit)`方法设置等待时长。

```java
public class ExchangerTest {
    private Exchanger<String> exchanger = new Exchanger<>();
    private ExecutorService executorService = Executors.newFixedThreadPool(2);

    @Test
    public void test(){
        executorService.execute(()->{
            try {
                Thread.currentThread().setName("0");
                String a = "线程A";
                String B = exchanger.exchange(a);  // 与调用这个方法的另一个线程进行数据的交换，
                                                  // 调用了该方法之后，会一直阻塞等待另一个线程进行数据交换
                System.out.println(Thread.currentThread().getName()+": "+ B);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        executorService.execute(()->{
            try {
                Thread.currentThread().setName("1");
                String b = "线程B";
                String A = exchanger.exchange(b);
                System.out.println(Thread.currentThread().getName()+": "+ A);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        });
    }
}
```

```
0: 线程B
1: 线程A
// 用exchanger传输数据，两个线程之间必须使用同一个exchanger对象
```



### - CountDownLatch 与 CyclicBarrier 的区别

CountDownLatch 是减数的计数器。当值为0后，才使得调用了CountDownLatch的await()方法的线程被唤醒，继续执行之后的任务。并且**CountDownLatch不可以重新设置计数总数值**。

CyclicBarrier 是加数的计数器，到达了指定的屏障数，才可以被唤醒继续执行。通过线程通过调用CyclicBarrier的await()方法，被阻塞之后，即增加了一个屏障。当达到了屏障指定的数，就会执行特定的动作 (构造器第二个参数barrierAction) 。特定动作执行完了之后，被阻塞的线程会被唤醒，各线程继续执行await()方法之后的任务。**CyclicBarrier可以通过reset()方法重置计数器的屏障数**。



## 六、ReadWriteLock 接口 JDK 1.5

`java.util.concurrent.locks interface ReadWriteLock`接口，读写锁

作用，在执行写入操作时，只能允许一个线程去进行写入操作；可允许多个线程同时去执行读取操作。更加细粒化的去加锁。

该接口的实现类：`ReentrantReadWriteLock`

#### 实现类构造方法

- `public ReentrantReadWriteLock()`

#### 实现类常用方法

- `writeLock()` 写锁（独占锁）
- `readLock()` 读锁（共享锁）

```java
public class ReadWriteLockTest {
    public static void main(String[] args) {
        MapTest mapTest = new MapTest();

        for (int i = 1; i <= 4; i++) {
            final int num = i;
            new Thread(()->{
                mapTest.put(num+"", num+"");
            }, String.valueOf(i)).start();
        }

        for (int i = 1; i <= 4; i++) {
            final int num = i;
            new Thread(()->{
                mapTest.get(num+"");
            }, String.valueOf(i)).start();
        }
    }
}

class MapTest{
    private volatile Map<String, String> map = new HashMap<String, String>();
    ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    public void put(String key, String value){
        // 设置写锁
        readWriteLock.writeLock().lock();
        try{
            // 业务
            System.out.println(Thread.currentThread().getName()+ "写入");
            map.put(key,value);
            System.out.println(Thread.currentThread().getName()+ "写入OK");
        }catch (Exception e){
            System.out.println("异常");
        }finally {
            // 释放写锁
            readWriteLock.writeLock().unlock();
        }
    }

    public void get(String key){
        // 设置读锁
        readWriteLock.readLock().lock();
        try{
            System.out.println(Thread.currentThread().getName()+ "读取");
            map.get(key);
            System.out.println(Thread.currentThread().getName()+ "读取OK");
        }catch (Exception e){
            System.out.println("异常");
        }finally {
            // 释放读锁
            readWriteLock.readLock().unlock();
        }
    }
}
```

**输出：**

```
1写入
1写入OK
2写入
2写入OK
3写入
3写入OK
4写入
4写入OK
1读取
1读取OK
4读取
2读取
4读取OK
2读取OK
3读取
3读取OK
```



## 七、 队列 JDK 1.5

### 1个非阻塞队列

### 1、CurrentLinkedQueue\<E> 

书P161



### 7个阻塞队列

### 阻塞队列BlockingQueue\<E> 接口

`java.util.concurrent interface BlockingQueue`接口，**阻塞队列**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/blockingqueue.png)

> 以上图除BlockingQueue的实现类，出现的都是接口

常用实现类`LinkedBlockingQueue`、`ArrayBlockingQueue`

#### 4组API

| 方式         | 抛出异常  | 返回布尔，不抛异常 | 等待阻塞 | 超时 等待阻塞                           |
| ------------ | --------- | ------------------ | -------- | --------------------------------------- |
| 添加         | add(E e)  | offer(E e)         | put(E e) | offer(E e, long timeout, TimeUnit unit) |
| 移除         | remove()  | poll()             | take()   | poll(long timeout, TimeUnit unit)       |
| 监测队首元素 | element() | peek()             | /        | /                                       |

```java
/*抛出异常*/
public static void test01(){
    // 实例化阻塞队列，并给一个容量
    BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);

    blockingQueue.add("a");
    blockingQueue.add("b");
    blockingQueue.add("c");
    // System.out.println(blockingQueue.element()); // 底层调用peek()
    // IllegalStateException: Queue full
    // blockingQueue.add("d");

    System.out.println(blockingQueue.remove());
    System.out.println(blockingQueue.remove());
    System.out.println(blockingQueue.remove());
    // NoSuchElementException
    // System.out.println(blockingQueue.remove());
}
```

```java
/*不抛出异常，返回值*/
public static void test02(){
    // 实例化阻塞队列，并给一个容量
    BlockingQueue<Integer> blockingQueue = new ArrayBlockingQueue<>(3);

    System.out.println(blockingQueue.offer(1));
    System.out.println(blockingQueue.offer(2));
    System.out.println(blockingQueue.offer(3));
    // System.out.println(blockingQueue.offer("d")); // 返回false

    System.out.println(blockingQueue.poll());
    System.out.println(blockingQueue.poll());
    System.out.println(blockingQueue.poll());
    // System.out.println(blockingQueue.poll()); // 返回null

}
```

```java
/*阻塞等待*/
public static void test03() throws InterruptedException {
    // 实例化阻塞队列，并给一个容量
    BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);

    blockingQueue.put("a");
    blockingQueue.put("a");
    blockingQueue.put("a");
    // blockingQueue.put("a"); // 一直处于等待

    System.out.println(blockingQueue.take());
    System.out.println(blockingQueue.take());
    System.out.println(blockingQueue.take());
    // System.out.println(blockingQueue.take()); // 一直处于等待
}
```

```java
/*超时阻塞等待 退出*/
public static void test04() throws InterruptedException {
    // 实例化阻塞队列，并给一个容量
    BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);

    blockingQueue.offer("a");
    blockingQueue.offer("b");
    blockingQueue.offer("c");
    // blockingQueue.offer("d",2, TimeUnit.SECONDS); // 超时会退出等待，返回false

    System.out.println(blockingQueue.poll());
    System.out.println(blockingQueue.poll());
    System.out.println(blockingQueue.poll());
    // System.out.println(blockingQueue.poll(2, TimeUnit.SECONDS)); // 超时会退出，并且返回null
}
```



### 1、ArrayBlockingQueue\<E> 类

数组结构的有界阻塞队列



### 2、LinkedBlockingQueue\<E> 类

链表结构的有界阻塞队列



### 3、PriorityBlockingQueue\<E> 类

按照优先级排序的无界阻塞队列



### 4、DelayQueue\<E> 类

是一个支持延时获取元素的无界阻塞队列



### 5、SynchronousQueue\<E> 类

`java.util.concurrent.SynchronousQueue<E> `是BlockingQueue的实现类，同步队列

**该同步队列，不存储值。只能put一个值，并且take之后，才能再次往里面put一个值。**

```java
public class SynchronousQueueTest {
    public static void main(String[] args) {
        BlockingQueue<String> sq = new SynchronousQueue<>();

        new Thread(()->{
            try {
                System.out.println(Thread.currentThread().getName() + " 存入");
                sq.put("a");
                System.out.println(Thread.currentThread().getName() + " 存入");
                sq.put("b");
                System.out.println(Thread.currentThread().getName() + " 存入");
                sq.put("c");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"T1").start();

        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(2);
                System.out.println(Thread.currentThread().getName() + " 取出");
                sq.take();
                TimeUnit.SECONDS.sleep(2);
                System.out.println(Thread.currentThread().getName() + " 取出");
                sq.take();
                TimeUnit.SECONDS.sleep(2);
                System.out.println(Thread.currentThread().getName() + " 取出");
                sq.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"T2").start();
    }
}
```

**输出：**

```
T1 存入
T2 取出
T1 存入
T2 取出
T1 存入
T2 取出
```



### 6、LinkedTransferQueue\<E> 类

链表结构的无界阻塞队列



### 7、LinkedBlockingDeque\<E> 类

链表结构的双向阻塞队列





## 八、线程池进阶

3大方法、7大参数、4种拒绝 自定义线程池



### 1、3大方法

不推荐使用该3大方法，请使用`ThreadPoolExecutor`创建线程池

- `Executors.newSingleThreadExecutor()` 单个线程
- `Executors.newFixedThreadPool(int num)` 固定数线程的线程池
- `Executors.newCacheThreadPool()` 根据任务的情况，而规定线程池中的线程数

 

```java
// newSingleThreadExecutor
/*
只允许一个线程执行，当核心线程数全部都在正在执行(第一个参数)，进来的其他任务将会被加入到LinkedBlockingQueue队列中，由于这里的LinkedBlockingQueue的最大长度为Integer.MAX_VALUE，所以是无限大，由于会无限将任务存储到队列中，可能会造成OOM，最终会导致系统瘫痪，不安全。
由于LinkedBlockingQueue可以很大，所以等待时间参数以及最大线程数的设置是无效的。
*/
public static ExecutorService newSingleThreadExecutor() {
    return new FinalizableDelegatedExecutorService
        (new ThreadPoolExecutor(1, 1,
                                0L, TimeUnit.MILLISECONDS,
                                new LinkedBlockingQueue<Runnable>()));
}

// newFixedThreadPool
/*
只允许nThreads线程执行，当核心线程数全部都在正在执行(第一个参数)，进来的其他任务将会被加入到LinkedBlockingQueue队列中，由于这里的LinkedBlockingQueue的最大长度为Integer.MAX_VALUE，所以是无限大，由于会无限将任务存储到队列中，可能会造成OOM，最终会导致系统瘫痪，不安全。
由于LinkedBlockingQueue可以很大，所以等待时间参数以及最大线程数的设置是无效的。
*/
public static ExecutorService newFixedThreadPool(int nThreads) {
    return new ThreadPoolExecutor(nThreads, nThreads,
                                  0L, TimeUnit.MILLISECONDS,
                                  new LinkedBlockingQueue<Runnable>());
}

// newCachedThreadPool
/*
该方法，核心线程数为0，而可创建的最大线程数却为Integer.MAX_VALUE,无限创建线程，也会造成OOM，一个新线程会创建一个栈空间，而栈空间就会占内存。SynchronousQueue队列是一个不存储数据的队列，即放入即取出。当没有任务可以使用时，闲着的线程会等待60L秒后，如果还是没有任务执行，这个线程就会被销毁关闭。
*/
public static ExecutorService newCachedThreadPool() {
    return new ThreadPoolExecutor(0, Integer.MAX_VALUE,
                                  60L, TimeUnit.SECONDS,
                                  new SynchronousQueue<Runnable>());
}
```

通过3大方法，查看源码，发现这3大方法都`new ThreadPoolExecutor`该对象去创建线程池。阿里java开发手册强制规定，不可以使用Excutors工具类去创建线程池，因参数值为`Integer.MAX_VALUE`，最大线程支持21亿，这样高的并发量，会导致OOM (OutOfMemory内存溢出) 。阿里官方**强烈推荐使用`ThreadPoolExecutor`来创建线程池**。



### 2、7大参数

#### ThreadPoolExecutor 类

```java
public ThreadPoolExecutor(int corePoolSize, // 线程核心数(一直存在的线程数量)
                          int maximumPoolSize, // 能创建的最大线程数
                          long keepAliveTime, // 设定除核心线程以外的线程，超过该时间未使用就关闭
                          TimeUnit unit, // 设定时间的单位
                          BlockingQueue<Runnable> workQueue
                          // 阻塞队列，用于存储等待使用线程的任务
                          ThreadFactory threadFactory, // 固定
                          RejectedExecutionHandler handler
                          // 拒绝超额任务的方式(4种)
                         ) {
        if (corePoolSize < 0 ||
            maximumPoolSize <= 0 ||
            maximumPoolSize < corePoolSize ||
            keepAliveTime < 0)
            throw new IllegalArgumentException();
        if (workQueue == null || threadFactory == null || handler == null)
            throw new NullPointerException();
        this.acc = System.getSecurityManager() == null ?
                null :
                AccessController.getContext();
        this.corePoolSize = corePoolSize;
        this.maximumPoolSize = maximumPoolSize;
        this.workQueue = workQueue;
        this.keepAliveTime = unit.toNanos(keepAliveTime);
        this.threadFactory = threadFactory;
        this.handler = handler;
}
```



#### 执行过程原理

任务一来，先检查核心线程是否已经被全部占用，如果没有全部占用，那么就使用核心线程数(核心线程数在初始化的时候就创建出来做准备的)，如果已被全部占用，那么这个任务就会被加入到阻塞队列中。如果阻塞队列已满，那么就会创建新的线程，但是总线程数必须小于等于最大线程数。如果，最大线程数，每个线程都被占用，并且阻塞队列已满，那么就会根据拒绝方式返回不同的拒绝结果。



### 3、4种拒绝

```java
// 最大限度任务：maximumPoolSize + workQueue
new ThreadPoolExecutor.AbortPolicy(); // 拒绝超过最大限度任务，并抛出异常
new ThreadPoolExecutor.CallerRunsPolicy(); // 哪儿来的任务回哪儿去，让你的原线程执行
new ThreadPoolExecutor.DiscardPolicy(); // 丢弃超过最大限度任务，不抛出异常
new ThreadPoolExecutor.DiscardOldestPolicy(); // 进来的线程任务，尝试与最早的任务竞争，竞争不到，丢弃，不抛出异常
```



### 4、<u>自定义线程池</u>

```java
public class PoolTest {
    public static void main(String[] args) {
        // 获取该运行时，CPU的核数，作为最大线程数
        int processors = Runtime.getRuntime().availableProcessors();
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(
            			1,
                        processors,
                        3,
                        TimeUnit.SECONDS,
                        new ArrayBlockingQueue<>(3),
                        Executors.defaultThreadFactory(),
                        new ThreadPoolExecutor.CallerRunsPolicy());

        for (int i = 1; i <= 8; i++) {
            poolExecutor.submit(()->{
                System.out.println(Thread.currentThread().getName() + " OK");
            });
        }

        poolExecutor.shutdown();
    }
}
```

**输出：**

```
pool-1-thread-2 OK
pool-1-thread-3 OK
pool-1-thread-1 OK
main OK
pool-1-thread-3 OK
pool-1-thread-4 OK
pool-1-thread-2 OK
main OK
```



## 九、CPU密集型 IO密集性 混合型

### <u>线程池最大线程数应该如何定义？</u> 面试

第一种：

**CPU密集型**：最大线程数应该等于==CPU核数+1==，这样最大限度提高效率。

```java
// 通过该代码获取当前运行环境的cpu核数
Runtime.getRuntime().availableProcessors();
```



第二种：

**IO密集型**：主要是进行IO操作，执行IO操作的时间较长，这时cpu出于空闲状态，导致cpu的利用率不高。线程数为==2倍CPU核数==。当其中的线程在IO操作的时候，其他线程可以继续用cpu，提高了cpu的利用率。



第三种：

**混合型**：如果CPU密集型和IO密集型执行时间相差不大那么可以拆分；如果两种执行时间相差很大，就没必要拆分了。



第四种(了解)：

在IO优化中，线程等待时间所占比越高，需要线程数越多；线程cpu时间占比越高，需要越少线程数。因此：

最佳线程数目=（（线程等待时间+线程cpu时间）/ 线程cpu时间）* cpu数目



## 十、ForkJoinPool类 JDK 1.7

ForkJoin 并发执行任务，提高效率，在大数据量表现显著。最适合的是计算密集型的任务。



**ForkJoin工作原理**

- 是将大量的数据分成多个子任务处理，然后合并。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/ForkJoin工作原理.png" style="float:left;" />

**ForkJoin特点**

- 工作窃取：该线程的任务执行完之后，就会去窃取其他线程没有执行完的任务，把任务拿到自己这里来执行，提高效率。
- 用的双端队列

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/ForkJoin.png" style="float:left;" />



`java.util.concurrent.ForkJoinPool`类继承Executor。

#### 常用方法

- `public <T> ForkJoinTask<T> submit(ForkJoinTask<T> task)` 提交一个ForkJoinTask来执行。



ForkJoinTask是一个抽象类方法，如果要使用ForkJoinPool去执行代码，就得继承ForkJoinTask的子抽象类去实现，然后使用ForkJoinPool的submmit方法去使用

<img src=".\pictureForJUC\ForkJoinTask.png" style="float:left;" />

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/ForkJoinTask关系图.png" style="float:left;" />

#### ForkJoinTask\<V> 抽象类

- `public final ForkJoinTask <V> fork()` 把拆分任务压入到线程中
- `public final V join()`  返回计算结果

#### RecursiveAction 抽象类

- `protected abstract void compute()`这个任务执行的主要计算。 

#### RecursiveTask\<V> 抽象类

- `protected abstract V compute()` 这个任务执行的主要计算。



#### LongStream 接口

- `static LongStream range(long startInclusive,long endExclusive)` 返回从startInclusive(含)至  endExclusive通过增量步骤 1的最终结果。 [start，end）

- `static LongStream range(long startInclusive,long endExclusive)  ` 返回从startInclusive至  endExclusive(含)通过增量步骤 1的最终结果。 (start，end]

- `LongStream parallel()` 使其为并行流
- `long reduce(long identity, LongBinaryOperator op)` 合并流产生的元素，并产生单个值返回。



#### 案例 使用ForkJoin,并行Stream计算大数据的和

Recursive 递归

parallel 平行

**ForkJoinDemon.java**

```java
public class ForkJoinDemon extends RecursiveTask<Long> {
    private long start;
    private long end;

    private final long temp = 10000L; // 规定一个临界值

    public ForkJoinDemon(long start, long end) {
        this.start = start;
        this.end = end;
    }

    // 递归合并
    @Override
    protected Long compute() {
        // 如果计算的两个值在临界值以内，就直接使用for循环计算
        if((end - start) < temp){
            long sum = 0L;
            for (long i = start; i <= end; i++) {
                sum += i;
            }
            return sum;
        }else {
            // 计算中间值，将其分为两个线程两个对象去进行计算，在new的新对象中，
            // 也会去判断是否在临界值以内，否则还会继续new一个新对象，进入新线程中计算
            long middle = (end + start)/2;
            ForkJoinDemon task1 = new ForkJoinDemon(start, middle);
            task1.fork(); // 把拆分任务压入到线程中
            ForkJoinDemon task2 = new ForkJoinDemon(middle + 1, end);
            task2.fork();
            return task1.join()+task2.join(); // 将两个任务的返回值，合并在一起
        }
    }
}
```

**ForkJoinTest.java**

```java
public class ForkJoinTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // test01(); // 结果：500000000500000000  时间：723
         test02(); // 结果：500000000500000000  时间：641
        // test03(); // 结果：500000000500000000  时间：467
    }

    // 普通方法for循环计算
    public static void test01(){
        long start = System.currentTimeMillis();
        long sum = 0L;
        for (int i = 1; i <= 10_0000_0000L; i++) {
            sum += i;
        }
        long end = System.currentTimeMillis();
        System.out.println("结果：" + sum + "  时间：" + (end-start));
    }

    // 使用ForkJoin 并行计算
    public static void test02() throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();
        long sum = 0L;

        ForkJoinPool joinPool = new ForkJoinPool();
        ForkJoinDemon joinDemon = new ForkJoinDemon(0L, 10_0000_0000L);
        ForkJoinTask<Long> submit = joinPool.submit(joinDemon);
        sum = submit.get();

        long end = System.currentTimeMillis();
        System.out.println("结果：" + sum + "  时间：" + (end-start));
    }

    // 使用Stream流 并行计算
    public static void test03(){
        long start = System.currentTimeMillis();

        // 设置计算范围，增1计算，使用并行，将并行线程的值合并成一个值，返回
        long sum = LongStream.rangeClosed(0, 10_0000_0000L).parallel().reduce(0, (a, b) -> a + b);

        long end = System.currentTimeMillis();
        System.out.println("结果：" + sum + "  时间：" + (end-start));
    }
}
```



## 十一、CompletableFuture\<T>类 异步回调 JDK1.8

`java.util.concurrent.CompletableFuture<T> `类，异步回调类。如同前端的Ajax。如在主线程new了该类，该类会创建一个新的线程去执行任务，并且主线程和该新线程互不影响。可同时进行。



#### 构造方法

- `public CompletableFuture()` 创建一个新的不完整的CompletableFuture

#### 常用方法

- `public static CompletableFuture<Void> runAsync(Runnable runnable)` 异步完成任务，并没有返回值
- `public static <U> CompletableFuture<U> supplyAsync(Supplier<U> supplier)` 异步完成任务，有返回值
- `public CompletableFuture<T> whenComplete(BiConsumer<? super T,? super Throwable> action)` 当成功完成异步任务的回调函数。
- `public CompletableFuture<T> exceptionally(Function<Throwable,? extends T> fn)` 捕获异常，并由返回值。



```java
public class CompletableFutureTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // 没有返回值
        CompletableFuture<Void> runAsync = 
            CompletableFuture.runAsync(() -> System.out.println("已执行runAsync 200"));
        runAsync.get(); // 获取结果值，如果一直执行不完，该方法就会被阻塞，一直等待去get

        // 有返回值
        CompletableFuture<Integer> supplyAsync = CompletableFuture.supplyAsync(() -> {
            // System.out.println(19/0);
            return 200;
        });
        //System.out.println(supplyAsync.get());

        // 当成功完成
        Integer info = supplyAsync.whenComplete((t, e) -> {
            System.out.println(t); // 获得成功执行完成的返回值，如果执行出错为null
            System.out.println(e); // 如果执行不成功，获取异常信息，如果没有异常为null
        }).exceptionally((e) -> { // 如果没有异常不执行
            e.getMessage(); // 将执行失败的异常信息获取出来
            return 404; // 有返回值
        }).get();

        System.out.println(info);

    }
}
/* 输出：
已执行runAsync 200
200
null
200*/
```



## 十二、JMM (java memory model)

> **什么是JMM**

JMM：java内存模型(java memory model)。可理解为一种概念、约定。因为在不同的硬件生产商和不同的操作系统下，内存的访问逻辑有一定的差异，结果就是当你的代码在某个系统环境下运行良好，并且线程安全，但是换了个系统就出现各种问题。Java内存模型，就是为了屏蔽系统和硬件的差异，让一套代码在不同平台下能到达相同的访问结果。读写的一种规则。



> **内存划分**

JVM在设计时候考虑到，如果JAVA线程每次读取和写入变量都直接操作主内存，对性能影响比较大，所以每条线程拥有各自的工作内存，工作内存中的变量是主内存中的一份拷贝，线程对变量的读取和写入，直接在工作内存中操作，而不能直接去操作主内存中的变量。但是这样就会出现一个问题，当一个线程修改了自己工作内存中变量，对其他线程是不可见的，会导致线程不安全的问题。因为JMM制定了一套标准来保证开发者在编写多线程程序的时候，能够控制什么时候内存会被同步给其他线程。



> **内存交互操作**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/JMM.png" style="float:left; zoom:100%" />

**内存交互操作有8种(4对)，虚拟机实现必须保证每一个操作都是原子的，不可在分的（对于double和long类型的变量来说，load、store、read和write操作在某些平台上允许例外）**

- lock   （锁定）：作用于主内存的变量，把一个变量标识为线程独占状态

- unlock （解锁）：作用于主内存的变量，它把一个处于锁定状态的变量释放出来，释放后的变量才可以被其他线程锁定
- read  （读取）：作用于主内存变量，它把一个变量的值从主内存传输到线程的工作内存中，以便随后的load动作使用
- load   （载入）：作用于工作内存的变量，它把read操作从主内存中得到的变量值放入工作内存的变量副本中
- use   （使用）：作用于工作内存中的变量，它把工作内存中的变量传输给执行引擎，每当虚拟机遇到一个需要使用到变量的值，就会使用到这个指令
- assign （赋值）：作用于工作内存中的变量，它把一个从执行引擎中接受到的值放入工作内存的变量副本中
- store  （存储）：作用于主内存中的变量，它把一个从工作内存中一个变量的值传送到主内存中，以便后续的write使用
- write 　（写入）：作用于主内存中的变量，它把store操作从工作内存中得到的变量的值放入主内存的变量中

**JMM对这八种指令的使用，制定了如下规则：**

- 不允许read和load、store和write操作之一单独出现。即使用了read必须load，使用了store必须write

- 不允许线程丢弃他最近的assign操作，即工作变量的数据改变了之后，必须告知主存
- 不允许一个线程将没有assign的数据从工作内存同步回主内存
- 一个新的变量必须在主内存中诞生，不允许工作内存直接使用一个未被初始化的变量。就是怼变量实施use、store操作之前，必须经过assign和load操作
- 一个变量同一时间只有一个线程能对其进行lock。多次lock后，必须执行相同次数的unlock才能解锁
- 如果对一个变量进行lock操作，会清空所有工作内存中此变量的值，在执行引擎使用这个变量前，必须重新load或assign操作初始化变量的值
- 如果一个变量没有被lock，就不能对其进行unlock操作。也不能unlock一个被其他线程锁住的变量
- 对一个变量进行unlock操作之前，必须把此变量同步回主内存



**问题程序：**

```java
public class VolatileTest {
    private static int num = 0;
    public static void main(String[] args) throws InterruptedException { // main线程

        new Thread(()->{ // 线程1 对主内存的变化不知道
            while (num == 0){

            }
        }).start();

        TimeUnit.SECONDS.sleep(2);

        num = 1;
        System.out.println("num => "+num);
    }
}
```

如上程序，有两个线程，一个main线程，一个线程1。线程1被启动之后，该线程从主内存中，获取到了num值为0，此时就进入了死循环。往下，主线程修改了num的值，为1。因此主内存中的num值变了，可是线程1还是一直在死循环，这时因为，当main线程更改了num值之后，对线程1是不可见的，因此导致线程1没有重新去读取主内存的num值，却线程1的工作内存还一直是num=0的结果。



## 十三、Volatile 关键字

> **请谈谈你对volatile的理解**

Volatile是java虚拟机提供**轻量级同步机制**

1. 保证可见性
2. **不保证原子性**
3. 禁止指令重排



#### 保证可见性

```java
public class VolatileTest {
    private static volatile int num = 0;
    public static void main(String[] args) throws InterruptedException { // main线程

        new Thread(()->{ // 线程1
            while (num == 0){

            }
        }).start();

        TimeUnit.SECONDS.sleep(2);

        num = 1;
        System.out.println("num => "+num);
    }
}
// 给num加了volatile关键字，当该变量值发生变化时，线程1就会重新去主内存读取，保证可见性。
// 读取之后，线程1因为不满足num==0，就会退出。
```



#### volatile++复合操作不保证原子性

原子性：不可分割。如线程A在执行任务的时候，不能被打扰，不能被分割。要么成功，要么就失败。

```java
public class VolatileTest01 {
    private static volatile int num = 0;
    public static void add(){
        num++;
    }
    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                for (int j = 0; j < 1000; j++) {
                    add();
                }
            }).start();
        }

        // 判断线程数是否大于2，java有2个默认线程，main线程，gc线程
        while (Thread.activeCount() > 2){
            Thread.yield(); // 让线程礼让，防止一直占用cpu
        }

        System.out.println(num);
    }
}
// 输出：9965
```

如上程序，可见volatile是**不具有原子性**的，线程执行任务时，容易被扰乱。别看是一行代码num++，在汇编中num++有几行代码：

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/不保证原子性.png" style="float:left;" />



**解决原子性**

1. 使用lock锁或synchronized (但是消耗资源，效率不高)
2. 使用`java.util.concurrent.atomic`包中的类，底层是CAS，代码如下：

```java
public class VolatileTest01 {
    // 原子性整数对象  
    private static volatile AtomicInteger atomicInteger = new AtomicInteger(0);
    public static void add(){
        atomicInteger.getAndIncrement(); // +1方法 通过CAS去操作的，见十五知识点深入理解CAS
    }
    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                for (int j = 0; j < 1000; j++) {
                    add();
                }
            }).start();
        }

        // 判断线程数是否大于2，java有2个默认线程，main线程，gc线程
        while (Thread.activeCount() > 2){
            Thread.yield(); // 让线程礼让，防止一直占用cpu
        }

        System.out.println(atomicInteger);
    }
}
// 输出：10000
```



#### 单个volatile操作保证原子性

```java
public class Test{
    volatile long v1 = 1L;
    
    public void set(Long l){
        v1 = l;
    }
}
```

set方法中的赋值操作，即对定义了volatile的变量，是有原子性的。



#### 禁止指令重排

指令重排：

源代码 --> 编译器优化重排 --> 指令并行也可能会重排 --> 内存系统也会重排 --> 执行

**处理器指令重排的时候，会考虑数据之间的依赖性。**

```java
int x = 1; // 1
int y = 2; // 2
x = x + 5; // 3
y = x + x; // 4

我们所期望的执行顺序：1234  系统可能执行的顺序是：2134 1324 (这就是重排)
不可能是 4123
```



**指令重排可能会导致出错**

x,y,a,b默认值为0

| 线程A | 线程B |
| ----- | ----- |
| x=a   | y=b   |
| b=1   | a=2   |

正常输出应该是：x = 0  ；  y = 0

由于各个线程执行的代码之间没有依赖关系，可能会出现以下状况：

| 线程A | 线程B |
| ----- | ----- |
| b=1   | a=2   |
| x=a   | y=b   |

输出异常结果：x = 2   ；  y = 1；



**而volatile会避免指令重排，加入了volatile关键字，会在volatile关键字上下文位置，加入cpu屏障，保证代码顺序不改变，保证不重排。**



## 十四、彻底玩转单例模式

单例模式，即单个实例，只有一个实例



### 1、饿汉式

饿汉模式，可以想象一个很饿的人，需要立马吃东西，饿汉模式便是这样，在类加载时就创建对象，由于在类加载时就创建单例，因此不存在线程安全问题。反射可破坏。

```java
public class HungryDemon {
    private static final HungryDemon hungry = new HungryDemon();

    private HungryDemon() {

    }

    public static HungryDemon getInstance(){
        return hungry;
    }

}
// 测试
class HungryTest{
    public static void main(String[] args) {
        HungryDemon instance1 = HungryDemon.getInstance();
        HungryDemon instance2 = HungryDemon.getInstance();
        System.out.println(instance1.equals(instance1));  // true，说明实例一样
    }
}
```

但饿汉式也存在一定的问题，即如果在该类里面存在大量开辟空间的语句，如很多数组或集合，但又不马上使用他们，这时这样的单例模式会消耗大量的内存，影响性能。



### 2、懒汉式

顾名思义，懒汉式，就是懒，即在类加载时并不会立马创建单例对象，而是只生成一个单例的引用，即可以延时加载。

**单线程懒汉式：**

```java
public class LazyDemon {
    private static LazyDemon lazy;

    private LazyDemon() {}

    public static LazyDemon getInstance(){
        if(lazy == null){
            lazy = new LazyDemon();
        }
        return lazy;
    }
}
// 测试
class LazyTest{
    public static void main(String[] args) {
        LazyDemon instance1 = LazyDemon.getInstance();
        LazyDemon instance2 = LazyDemon.getInstance();
        System.out.println(instance1.equals(instance2)); // true
    }
}s
```

该懒汉式，在单线程下是安全的，但是在多线程之下是不安全的。

比如：

```java
public class LazyDemon {
    private static LazyDemon lazy;

    private LazyDemon() {
        System.out.println(Thread.currentThread().getName()+" OK");
    }

    public static LazyDemon getInstance(){
        if(lazy == null){
            lazy = new LazyDemon();
        }
        return lazy;
    }
}
// 测试
class LazyTest{
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                LazyDemon.getInstance();
            }).start();
        }
    }
}
/* 输出结果：
Thread-0 OK
Thread-2 OK
Thread-1 OK
// 可见，这次执行至少创建了3个实例，就不是单实例了
```

**解决：**可通过加Lock锁解决，也可以通过synchronized去解决，但是效率低。

```java
// 在 LazyDemon 方法上加上了 synchronized 关键字
public static synchronized LazyDemon getInstance(){
    if(lazy == null){
        lazy = new LazyDemon();
    }
    return lazy;
}
/* 输出结果：
Thread-0 OK  永远只有一个单实例*/

// 在 LazyDemon 方法上加上了 Lock 锁
private static Lock lock = new ReentrantLock();
public static LazyDemon getInstance(){
    lock.lock();
    try{
        if(lazy == null){
            lazy = new LazyDemon();
        }
    }catch (Exception e){
        e.printStackTrace();
    }finally {
        lock.unlock();
        return lazy;
    }
}
/* 输出结果：
Thread-0 OK  永远只有一个单实例*/
```



**多线程安全懒汉式**

```java
public class LazyDemon {
    private static LazyDemon lazy;

    private LazyDemon() {
        System.out.println(Thread.currentThread().getName()+" OK");
    }

    public static synchronized LazyDemon getInstance(){
        if(lazy == null){
            lazy = new LazyDemon();
        }
        return lazy;
    }
}

class LazyTest{
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                LazyDemon.getInstance();
            }).start();
        }
    }
}
```







### 3、DCL懒汉式

DCL懒汉(双重监测懒汉式)，同样是在类加载时只提供一个引用，不会直接创建单例对象，不需要对整个方法进行同步，缩小了锁的范围，只有第一次会进入创建对象的方法，提高了效率。

```java
public class DCLLazyDemon {
    private static volatile DCLLazyDemon dclLazy; // 1

    private DCLLazyDemon(){
        System.out.println(Thread.currentThread().getName()+ " OK");
    };

    public static DCLLazyDemon getInstance(){
        if (dclLazy == null){
            synchronized (DCLLazyDemon.class){
                if(dclLazy==null){
                    dclLazy = new DCLLazyDemon();
                }
            }
        }
        return dclLazy;
    }
}
// 测试
class DCLLazyTest{
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                DCLLazyDemon.getInstance();
            }).start();
        }
    }
}
```

**1号代码为什么要加volatile关键字？**

在极端情况时，会出现问题。可能会出现重排问题。在new一个实例时，不是原子性操作，创建实例分为

```
1.分配内存空间
2.执行构造方法
3.将空间地址复制给变量
以上3步执行完成之后，才创建完整的一个实例。
```

可能当底层重排的时候，执行顺序为132。以这个顺序执行到3，但还没来得及执行2还没来得及将对象初始化，这时又来了一个线程在执行这个方法，此刻dclLazy就已经不是null了，但是dclLazy引用的是空间是空的，该空间是没有任何东西的，这时这个线程返回的对象就是不存在的。因此就会出现问题。为了解决该问题，需要在1号代码上加volatile关键字。`private static volatile DCLLazyDemon dclLazy;`



#### 通过反射破坏单实例

##### 第1种

```java
public class DCLLazyDemon {
    private static volatile DCLLazyDemon dclLazy;

    private DCLLazyDemon(){
        System.out.println(Thread.currentThread().getName()+ " OK");
    };

    public static DCLLazyDemon getInstance(){
        if (dclLazy == null){
            synchronized (DCLLazyDemon.class){
                if(dclLazy==null){
                    dclLazy = new DCLLazyDemon();
                }
            }
        }
        return dclLazy;
    }
}

class DCLLazyTest{
    public static void main(String[] args) throws Exception {
        Constructor<DCLLazyDemon> constructor = DCLLazyDemon.class.getDeclaredConstructor(null);
        constructor.setAccessible(true);
        DCLLazyDemon instance1 = constructor.newInstance();
        DCLLazyDemon instance2 = constructor.newInstance();
        System.out.println(instance1);
        System.out.println(instance2);
    }
}
/*输出：
main OK
main OK
singletonmode.DCLLazyDemon@74a14482
singletonmode.DCLLazyDemon@1540e19d
可见被创建了2个实例，这里是反射通过构造器去创建对象的，因此可以给构造器一个信号量*/
```

**解决：**

```java
public class DCLLazyDemon {
    private static volatile DCLLazyDemon dclLazy;
    private static boolean flag = false; // 1

    private DCLLazyDemon(){
        synchronized (DCLLazyDemon.class){
            if(flag == false){ // 2
                flag = true; // 如果为false，设置为true并初始化对象
            }else {
                throw new RuntimeException("不要使用反射来破坏");
            }
        }
    };

    public static DCLLazyDemon getInstance(){
        if (dclLazy == null){
            synchronized (DCLLazyDemon.class){
                if(dclLazy==null){
                    dclLazy = new DCLLazyDemon();
                }
            }
        }
        return dclLazy;
    }
}

class DCLLazyTest{
    public static void main(String[] args) throws Exception {
        Constructor<DCLLazyDemon> constructor = DCLLazyDemon.class.getDeclaredConstructor(null);
        constructor.setAccessible(true);
        DCLLazyDemon instance1 = constructor.newInstance();
        DCLLazyDemon instance2 = constructor.newInstance();
        System.out.println(instance1);
        System.out.println(instance2);
    }
}
/*输出：
Caused by: java.lang.RuntimeException: 不要使用反射来破坏
通过添加代码1，修改代码2。当通过反射来创建多个实例，会得到异常抛出。*/
```



##### 第2种

通过上一种的升级，但还是可能会出现安全问题。尽管信号变量通过加密成了一个很难破解的变量，但是依旧会有被破解的可能。如果被破解知道了信号量，那么安全问题如下：

```java
public class DCLLazyDemon {
    private static volatile DCLLazyDemon dclLazy;
    private static boolean flag = false;

    private DCLLazyDemon(){
        synchronized (DCLLazyDemon.class){
            if(flag == false){
                flag = true;
            }else {
                throw new RuntimeException("不要使用反射来破坏");
            }
        }
    };

    public static DCLLazyDemon getInstance(){
        if (dclLazy == null){
            synchronized (DCLLazyDemon.class){
                if(dclLazy==null){
                    dclLazy = new DCLLazyDemon();
                }
            }
        }
        return dclLazy;
    }
}

class DCLLazyTest{
    public static void main(String[] args) throws Exception {
        Constructor<DCLLazyDemon> constructor = DCLLazyDemon.class.getDeclaredConstructor(null);
        // 如果获取到了信号量的变量，就可通过反射获取该变量
        Field flag = DCLLazyDemon.class.getDeclaredField("flag");
        // 并设置该变量不进行安全监测
        flag.setAccessible(true);
        constructor.setAccessible(true);
        DCLLazyDemon instance1 = constructor.newInstance();
        // 在第一个实例创建完成之后，将信号量的值更改为true，又可以创建一个实例
        flag.set(instance1,false);
        DCLLazyDemon instance2 = constructor.newInstance();
        System.out.println(instance1);
        System.out.println(instance2);
    }
}
/*输出：
singletonmode.DCLLazyDemon@677327b6
singletonmode.DCLLazyDemon@14ae5a5
创建了2个实例*/
```

如何解决该问题呢？

**通过反射的分析newInstance()源码**

```java
if ((clazz.getModifiers() & Modifier.ENUM) != 0)
throw new IllegalArgumentException("Cannot reflectively create enum objects");
```

得出，枚举不能被反射创建。因此使用单例枚举式



### 4、静态内部类

使用静态内部类在类加载器加载的时候static就已经被创建，解决了**线程安全**问题，并实现了延时加载。可被反射破坏。

```java
public class StaticClass {
    private static StaticClass staticClass;

    private StaticClass(){
        System.out.println(Thread.currentThread().getName() + " OK");
    };

    private static class StaticClassIn{
       private static final StaticClass instance = new StaticClass();
    }

    public static StaticClass getInstance(){
        return StaticClassIn.instance;
    }
}
// 测试
class StaticClassTest{
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                StaticClass.getInstance();
            }).start();
        }
    }
}
```



### 5、枚举

```java
public enum  EnumDemon {

    INSTANCE;

    public static EnumDemon getInstance(){
        return INSTANCE;
    }
}

class EnumTest{
    public static void main(String[] args) throws Exception{
        EnumDemon instance1 = EnumDemon.getInstance();
        EnumDemon instance2 = EnumDemon.getInstance();
        System.out.println(instance1.equals(instance2)); // true
    }
}
```



#### 尝试反射获取枚举实例

**通过反射的分析newInstance()源码**

```java
if ((clazz.getModifiers() & Modifier.ENUM) != 0)
throw new IllegalArgumentException("Cannot reflectively create enum objects");
```

得出，枚举不能被反射创建。



##### 第一种尝试

**【查看枚举的构造器】**

**通过查看IDEA生成的class文件**：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/单例枚举idea.png)

**通过javap -p命令对class文件进行反编译**：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/单例枚举javap.png)

得知，==枚举其实是一个final类继承了枚举类==。

这里得知枚举的构造器为空构造器。

```java
public enum  EnumDemon {

    INSTANCE;

    public static EnumDemon getInstance(){
        return INSTANCE;
    }
}

class EnumTest{
    public static void main(String[] args) throws Exception{
        Constructor<EnumDemon> constructor = EnumDemon.class.getDeclaredConstructor(null);
        constructor.setAccessible(true);
        EnumDemon enumDemon = constructor.newInstance();
        System.out.println(enumDemon);

    }
}
```

输出：

```java
//Exception in thread "main" java.lang.NoSuchMethodException:
// singletonmode.EnumDemon.<init>()
```

说明这构造器是假的。尝试失败！



##### 第二种尝试

**通过jad工具反编译class字节码文件，会生成一个java文件**：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/单例枚举jad.png)

通过jad反编译生成的java文件，可以看到构造器是有参数的。这才是枚举的真正构造器。

```java
public enum  EnumDemon {

    INSTANCE;

    public static EnumDemon getInstance(){
        return INSTANCE;
    }
}

class EnumTest{
    public static void main(String[] args) throws Exception{
        // 通过查看到的真正构造器的参数，进行反射获取实例
        Constructor<EnumDemon> constructor = EnumDemon.class.getDeclaredConstructor(String.class,int.class);
        constructor.setAccessible(true);
        EnumDemon enumDemon = constructor.newInstance();
        System.out.println(enumDemon);

    }
}
```

**输出：**

```java
Exception in thread "main" java.lang.IllegalArgumentException: 
// Cannot reflectively create enum objects
	at java.lang.reflect.Constructor.newInstance(Constructor.java:417)
	at singletonmode.EnumTest.main(EnumDemon.java:30)
```

看到了源码的那句话，`Cannot reflectively create enum objects` 证实了枚举单例不可通过反射手段获取实例。尝试失败！



### 总结

- 饿汉式：线程安全（反射可破坏），调用效率高，不能延时加载
- 懒汉式：线程安全（反射可破坏）,调用效率不高，可以延时加载
- DCL懒汉式：线程安全（反射可破坏）。由于JVM底层模型原因，偶尔出现问题，不建议使用。
- 静态内部类式：线程安全（反射可破坏），调用效率高，可以延时加载
- 枚举单例：线程安全，调用效率高，不能延时加载



## 十五、深入理解CAS JDK1.5

在计算机科学中，比较和交换（Conmpare And Swap）是用于实现多线程同步的原子指令。 它将内存位置的内容与期望值进行比较，只有在相同的情况下，将该内存位置的内容修改为新的给定值。 这是作为单个原子操作完成的。 原子性保证新值基于最新信息计算; 如果该值在同一时间被另一个线程更新，则写入将失败。 操作结果必须说明是否进行替换; 这可以通过一个简单的布尔响应（这个变体通常称为比较和设置），或通过返回从内存位置读取的值来完成。

JAVA1.5开始引入了CAS，主要代码都放在JUC的atomic包下。



- CAS(**compareAndSet比较并设置方法**)在java层面是：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/CASJava层.png)

通过源码：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/CASJava层底层.png)

如果这个类的期望值(此刻的值)是0，那么就将更新的值赋值给这个类，成为该类的最新值，成功赋值返回true，否则返回false。



- CAS(**compareAndSwapInt比较并设置方法**)在底层是：

Unsafe类是java底层源码，native修饰的，通过C++语言，通过原语对cpu进行操作。原语是若干个指令组成，并且必须是连续执行来完成一个任务，过程中不能被其他线程插入。

通过`AtomicInteger atomicInteger = new AtomicInteger(0);`的`atomicInteger.getAndIncrement();`方法，得知源码

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/CAS底层.png)

在执行CAS语句时：var1，var2获得==内存此刻的值==，与之前获得的==内存值(期望值)==`var5`，进行比较，比较相同，则进行`var5=var5+var4`并返回`var5`

var1为当前对象
var2为内存偏移量
var4为1
var5为获取当前对象var1和内存偏移量var2在内存中的值
我们看到有一个do while去比较更新，如果内存中值和当前值相同则新增1，不相同在接着比较。从而我们也可以知道CAS的缺点：如果CAS失败就会一直进行尝试，长时间不成功，可能会跟CPU带来很大的开销。



#### CAS会出现ABA问题

如线程1从内存X中取出A，这时候另一个线程2也从内存X中取出A，并且线程2进行了一些操作将内存X中的值变成了B，然后线程2又将内存X中的数据变成A，这时候线程1进行CAS操作发现内存X中仍然是A，然后线程1操作成功。虽然线程1的CAS操作成功，但是整个过程就是有问题的。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/ABA.png" style="zoom:80%;" />



#### 解决ABA 原子引用

所以JAVA中提供了`AtomicStampedReference`或`AtomicMarkableReference`原子引用来处理会发生ABA问题的场景，主要是在对象中额外再增加一个标记来标识对象是否有过变更。



##### - AtomicStampedReference遇到的坑

```java
AtomicStampedReference<Integer> stampedReference = new AtomicStampedReference<>(1,1);
// 第一个参数值是Reference值，就是需要更新的Interge类型的值
// 第二个参数值是stamp值，用于记录版本信息，用于解决ABA的问题
```

大坑就在此，AtomicStampedReference的构造器：

```java
public AtomicStampedReference(V initialRef, int initialStamp) {
    pair = Pair.of(initialRef, initialStamp);
}
```

这里的Pair是AtomicStampedReference的内部类，用于存储更新值和邮票值，存储的Integer类型的数字在java中是有缓存的，**缓存范围是**`-128~127`，所以当AtomicStampedReference类初始化为这个之间的数字时，自动从缓存中获取，因此地址也是引用的缓存中的地址。在后续进行比较交换时：

```java
stampedReference.compareAndSet(
                    1, 
                    2, 
                    stampedReference.getStamp(), 
                    stampedReference.getStamp() + 1);
```

期望值是1，1在缓存中，会引用缓存中的内存地址，并且初始时也是1，期望值于当前值的引用地址都是一样的，compareAndSet方法通过==进行判断值期望值于当前值是否相等：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/原子引用.png)

因此不会有任何错误，比较交换都会成功返回ture。

==但是==：如果你初始化为1130并且更新值时，也是输入的1130为期望值：

```java
AtomicStampedReference<Integer> stampedReference = new AtomicStampedReference<>(1130,1);
// 更新值方法
stampedReference.compareAndSet(
                    1130, 
                    2, 
                    stampedReference.getStamp(), 
                    stampedReference.getStamp() + 1);
```

这种结果会出错，会返回false。这时由于当初始化时，传入的1130已经超过了缓存值，会创建一个新的内存空间存储这个Integer类型的值，而当调用`compareAndSet`时，也传入了1130，这个也已经超过了缓存值，因此又会创建一个内存空间存储这个Integer值，由于`compareAndSet`方法是通过`==`进行比较的，不是通过`equals`进行比较值的，因此期望值于当前值的引用地址不一样导致无法更新值成功，所以会返回false。

**解决办法：**

既然知道原因了，那么就很好解决了，在更新值的时候，直接使用`stampedReference.getReference()`方法获得当前值作为期望值就可以了。这样期望值和当前值的引用地址都一样，可以确保交换成功！，此时就返回ture了。

```java
stampedReference.compareAndSet(
                    stampedReference.getReference(),
                    2,
                    stampedReference.getStamp(),
                    stampedReference.getStamp() + 1)
```



---



**实例：**

```java
public class Test01{
    public static void main(String[] args) throws Exception {
        AtomicStampedReference<Integer> stampedReference = new AtomicStampedReference<>(1,1);
        int stamp = stampedReference.getStamp(); // 获取当前邮票值 为 1

        new Thread(()->{
            // 对值进行CAS操作，并将邮票更新
            System.out.println("A "+stampedReference.compareAndSet(
                    1, 
                    2, 
                    stampedReference.getStamp(), 
                    stampedReference.getStamp() + 1));
            System.out.println("a1 => "+stampedReference.getStamp());
            // 对值进行CAS操作，并将邮票更新
            System.out.println("A "+stampedReference.compareAndSet(
                    2, 
                    1, 
                    stampedReference.getStamp(), 
                    stampedReference.getStamp() + 1));
            System.out.println("a2 => "+stampedReference.getStamp());
        }).start();


        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 对值进行CAS操作，期望值与内存值相同，会更新为3。但是内存邮票值与期望邮票值不同，所以整个CAS操作失败。
            // 因此可以解决ABA的问题，就不会出现原有的ABA的问题了。
            System.out.println("B "+stampedReference.compareAndSet(
                    1, 
                    3, 
                    stamp, 
                    stampedReference.getStamp() + 1));
            System.out.println("b1 => "+stampedReference.getStamp());
        }).start();
    }
}
```

**输出：**

```
A true
a1 => 2
A true
a2 => 3
B false
b1 => 3
```

该原子引用，虽然使用了ABA的方式，线程A将原来的值赋了新值之后又将其赋值到原来的值，但是线程B操作的时候，会发现邮票值有变动，因此会操作失败。解决了ABA的问题。



#### CAS实现原子操作3大问题

- ABA问题
- CAS执行时间长，会消耗CPU资源，开销大
- CAS操作只能执行保证一个共享变量



#### ==？==使用synchronized会比CAS操作慢和效率低

- (悲观锁) 使用synchronized关键字，只允许一个线程进入同步代码块进行操作，而其他线程就必须在同步代码块外等待，此时其他线程就进入了BLOCKED状态，不占用CPU资源。而当前线程操作完之后，会释放锁，其他线程会去抢锁，如果抢到了就会被掉到CPU中进行执行。很明显，使用synchronized的同步代码，线程会在阻塞状态进入运行状态之间切换，有一个上下文切换的过程，该过程会浪费时间。

  > 上下文切换：
  >
  > 同步代码中，一个线程拿到锁从阻塞状态进入运行状态的时候，就被调度CPU中执行，一个线程除了去执行代码之外，线程本身也是有一些数据的，这些数据会调度到CPU cache中；而执行完之后，会从运行状态进入阻塞状态或是其他状态，这时就会将该线程的数据和线程从CPU中拿出去，换下一个进程进来。这样个过程就是上下文切换。



- (乐观锁) 使用CAS操作，CAS是比较并交换，因为CAS操作时原子性的，始终只会存在一个线程去执行CAS，所以不会存在线程安全问题。那么就会使得所有线程去获操作的时候，都会去比较，如果比较正确就执行交换，如果不正确就再次循环去拿新的值。当一个线程在CAS操作时，其他线程都会一直循环，空轮训，所有线程一直都是在执行中，少了上下文的切换。所以CAS操作效率会高很多，执行速度会快很多。但是大量的线程都再执行，长期轮训，会占用大量CPU资源。



**总结：**由于synchronized在线程调度上有上下文切换，浪费很多时间，而CAS操作的每一个线程都是一直循环然后比较值，没有上下文的切换，所以CAS会快很多。



## 十六、AQS

### AQS介绍

`java.util.concurrent.locks` 包中的 AbstractQueuedSynchronizer (抽象队列同步器)类

**该类是用于构建锁和同步器的框架。**使用该类可以简单且高效的构造出应用广泛的同步器，比如：ReentrantLock, Semaphore, ReentrantReadWriteLock, FutureTask等。同样我们也能继承该类，去实现自己需求的同步器。



### AQS原理

AQS核心思想是，**如果被请求的共享资源空闲，则将当前请求资源的线程归为有效工作线程，并且该资源设置为锁定状态。其他线程如果要请求该共享资源，由于该资源被占有，因此无法请求成功，那么就需要一套线程阻塞等待以及唤醒时锁分配的机制**。这个机制AQS使用**CLH队列锁**实现的，即将暂时获取不到锁的线程加入到队列中。



在AbstractQueuedSynchronizer类中有介绍**CLH**(Craig, Landin, and Hagersten)，CLH队列是一个虚拟的双向队列(虚拟的双向队列是不存在队列实例，而是通过Node节点之间的关系关联)

AQS原理图：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/CLH.png)

AQS使用int类型的`state标示锁的状态`。使用CAS对同步状态进行原子性操作实现状态修改。

```java
private volatile int state;  // The synchronization state. volatile保证线程可见
```

获取同步状态的源代码：

```java
// 获取同步的状态
protected final int getState() {
        return state;
    }
// 设置同步的状态
protected final void setState(int newState) {
    state = newState;
}
// 通过CAS设置同步的状态
protected final boolean compareAndSetState(int expect, int update) {
    return unsafe.compareAndSwapInt(this, stateOffset, expect, update);
}
```



### AQS中Node节点常量含义

```java
volatile int waitStatus;

int CANCELLED =  1;//waitStatus值为1时表示该线程节点已释放（超时、中断），已取消的节点不会再阻塞。
int SIGNAL    = -1;//waitStatus为-1时表示该线程的后续线程需要阻塞，即只要前置节点释放锁，就会通知标识为 SIGNAL 状态的后续节点的线程 
int CONDITION = -2; //waitStatus为-2时，表示该线程在condition队列中阻塞（Condition有使用）
int PROPAGATE = -3;//waitStatus为-3时，表示该线程以及后续线程进行无条件传播（CountDownLatch中有使用）共享模式下， PROPAGATE 状态的线程处于可运行状态 
waiteStatus 为 0:          None of the above // 初始状态
```



### AQS定义两种资源共享方式

**Exclusive（独占）**：只有一个线程能执行，如ReentrantLock。又可分为公平锁和非公平锁：

- 公平锁：按照线程在队列中的排队顺序，先到者先拿到锁

- 非公平锁：当线程要获取锁时，无视队列顺序直接去抢锁，谁抢到就是谁的

**Share（共享）**：多个线程可同时执行，如Semaphore/CountDownLatch。Semaphore、CountDownLatch、 CyclicBarrier、ReadWriteLock的Read锁。

不同的自定义同步器争用共享资源的方式也不同。自定义同步器在实现时只需要实现共享资源 state 的获取与释放方式即可，至于具体线程等待队列的维护（如获取资源失败入队/唤醒出队等），AQS已经在底层实现好了。



### AQS底层用模板方法模式

AQS底层是模板方法模式的，如果需要自定义同步器，一般方法是：继承AQS，并重写指定方法(无非是按照自己定义的规则对state的获取与释放)；将AQS组合在自定义同步组件的实现中，并调用模板方法，而这些模板方法会调用重写的方法。

需要重写的方法：

```java
isHeldExclusively()//该线程是否正在独占资源。只有用到condition才需要去实现它。
tryAcquire(int)//独占方式。尝试获取资源，成功则返回true，失败则返回false。
tryRelease(int)//独占方式。尝试释放资源，成功则返回true，失败则返回false。
tryAcquireShared(int)//共享方式。尝试获取资源。负数表示失败；0表示成功，但没有剩余可用资源；正数表示成功，且有剩余资源。
tryReleaseShared(int)//共享方式。尝试释放资源，成功则返回true，失败则返回false。
```

以上方法默认抛出`UnsupportedOperationException`异常，**AQS类中的其他方法都是final ，所以无法被其他类使用**，只有这几个方法可以被其他类使用。

以ReentrantLock为例，state初始化为0，表示未锁定状态。A线程lock()时，会调用tryAcquire()独占该锁并将state+1。此后，其他线程再tryAcquire()时就会失败，直到A线程unlock()到state=0（即释放锁）为止，其它线程才有机会获取该锁。当然，释放锁之前，A线程自己是可以重复获取此锁的（state会累加），这就是可重入的概念。但要注意，获取多少次就要释放多么次，这样才能保证state是能回到零态的。



### 自定以至多2个线程获取同步锁

自定义简单的同步组件，该工具至多只允许2个线程同时访问，超过2个线程的访问将被阻塞，加入到同步队列中，这个自定义规则的工具为TwinsLock。
由于允许2个线程同时获取同步锁，所以该同步组件是共享的。

```java
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class TwinsLock implements Lock {

    private final Sync sync = new Sync(2); // 设置最多几个线程可以获取同步状态

	// 内部类 ，重写同步器中的共享相关的方法，(通俗的说AQS就是写锁的规则)
    private static final class Sync extends AbstractQueuedSynchronizer{
        Sync(int count) {
            if(count <= 0){
                throw new IllegalArgumentException("count must large than zero");
            }
            setState(count); // 设置状态
        }

        // 尝试获取同步状态，state减
        @Override
        protected int tryAcquireShared(int reduceCount) {
            for(;;){
                int current = getState(); // 获取当前AQS记录的状态
                int newCount = current - reduceCount; // 得到新的状态
                if(newCount < 0 || compareAndSetState(current, newCount)){
                    return newCount; 
                    // 返回值大于等于0表示获取成功，否则同步状态获取失败
                }
            }
        }

        // 尝试释放同步状态，因为可能存在2个线程同时释放同步状态，
        // 为了保证安全，所以需要CAS保证原子性
        @Override
        protected boolean tryReleaseShared(int returnCount) {
            for(;;){
                int current = getState();
                int newCount = current + returnCount;
                if (compareAndSetState(current, newCount)){
                    return true; // 返回true表示，同步释放成功
                }
            }
        }
    }

    @Override
    public void lock() {
        /*状态加1，sync.acquireShared(1);这个方法是AQS里的方法，
        * 该方法会调用我们重写的tryAcquireShared(arg)方法，尝试获取锁，
        * 如果返回值小于0，则表示获取不成功，那么就会调用doAcquireShared(arg)
        * 方法，将该线程加入到队列中，以ACS方法加入队列，会无限循环直至加入队列成功*/
        sync.acquireShared(1);
    }

    @Override
    public void unlock() {
        /*状态减1，sync.releaseShared(1);会调用我们重写的tryReleaseShared(arg)
        * 方法，进行释放锁操作，直到释放锁成功，释放成功返回true，
        * 会调用doReleaseShared()方法唤醒后继节点*/
        sync.releaseShared(1);
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        System.out.println("已中断");
    }

    @Override
    public boolean tryLock() {
        return false;
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return false;
    }

    @Override
    public Condition newCondition() {
        return null;
    }
}
```

测试类：

```java
import org.junit.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

public class TwinsLockTest {

    @Test
    public void test(){
        final Lock lock = new TwinsLock(); // 创建锁的实例

		// 写一个局部类，继承Thread
        class Worker extends Thread{
            @Override
            public void run() {
                while (true){ // 无限循环获取锁，释放锁
                    lock.lock();
                    try {
                        TimeUnit.SECONDS.sleep(1);
                        System.out.println(Thread.currentThread().getName());
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }finally {
                        lock.unlock();
                    }
                }
            }
        }

		// 创建10个线程，去争夺锁，达到测试结果
        for (int i = 0; i < 10; i++){
            Worker worker = new Worker();
            worker.setDaemon(true); // 设置为守护线程
            worker.start();
        }

		// 该方法是每隔一秒进行一次换行，目的是打印结果好看，哈哈哈
        for (int i = 0; i < 10 ; i++) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println();
        }
    }
}
```



## 十七、各种锁的理解

### 1、公平锁、非公平锁

**公平锁**：非常公平，任何线程获得公平锁，那么就会执行锁中业务直到结束，过程中任何进程都不得干预打扰。不能插队。

**非公平锁**：在获得非公平锁之后，执行代码过程中，其他线程可以插入执行，该线程暂停执行，等待其他线程执行完毕，才可继续执行。可插队。（默认非公平锁）



### 2、共享锁、独占锁

**共享锁**：就是读取锁里的读锁，进行读操作，任何经常都可获得该锁。

**独占锁**：就是读取锁里的写锁，只允许一个线程获得该锁。



### 3、可重入锁

**可重入锁**(递归锁)：拿到了外面的锁，内部的锁也都统统拿到了。

lock锁与synchronized锁都是可重入锁，但是有一定的区别。

lock锁**必须配对**，有上锁对应必须就有解锁，不然会死锁。



### 4、自旋锁

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/自旋锁.png" style="float:left;" />

上图，CAS的源码分析就是一个自旋锁。自旋锁一定是使用CAS做为底层。因为CAS是C++写的原语操作。

#### 自定义简单的锁

```java
public class MyLock {
    private AtomicReference<Thread> atomicReference = new AtomicReference<>();

    public void lock(){
        // 获取当前执行的线程
        Thread thread = Thread.currentThread();
        System.out.println(thread.getName()+ " => 正在获取自旋锁");
        // CAS操作，自旋锁
        while (!atomicReference.compareAndSet(null,thread)){

        }
    }

    public void unLock(){
        Thread thread = Thread.currentThread();
        // 解锁，将thread赋值为null，上面的自旋锁就可以解除
        atomicReference.compareAndSet(thread,null);
    }
}

class MyLockTest{
    public static void main(String[] args) {
        MyLock myLock = new MyLock();
        
        new Thread(()->{
            myLock.lock();
            try{
                System.out.println("A线程正在执行...");
                TimeUnit.SECONDS.sleep(5);
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                System.out.println("A线程执行完毕！");
                myLock.unLock();
            }
        },"A").start();

        new Thread(()->{
            myLock.lock();
            try{
                System.out.println("B线程正在执行...");
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                System.out.println("B线程执行完毕！");
                myLock.unLock();
            }
        },"B").start();
    }
}
```

**输出：**

```
A => 正在获取自旋锁
A线程正在执行...(等5秒)
B => 正在获取自旋锁
A线程执行完毕！
B线程正在执行...
B线程执行完毕！
```



### 5、死锁

就是两个线程都持有各自的锁，但是又要获取对方的锁，此时对方的锁都没有释放，那么就会进入死锁状态。

```java
public class DieLock implements Runnable{
    private String lock1;
    private String lock2;

    public DieLock(String lockA, String lockB) {
        this.lock1 = lockA;
        this.lock2 = lockB;
    }

    @Override
    public void run() {
        synchronized (lock1){
            System.out.println(Thread.currentThread().getName() + "要获取" + lock2);
            try {
                TimeUnit.SECONDS.sleep(4);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            synchronized (lock2){
                System.out.println(Thread.currentThread().getName() + "要获取" + lock1);
            }
        }
    }
}

class DieTest{
    public static void main(String[] args) {
        String lockA = "LockA";
        String lockB = "LockB";

        // 注意锁的对象
        new Thread(new DieLock(lockA,lockB),"1 ").start();
        new Thread(new DieLock(lockB,lockA),"2 ").start();
    }
}
```

**输出：**

```
1 要获取LockB
2 要获取LockA
(程序进入无限死锁状态...)
```



#### 死锁排查 jps

除了==看日志==，还可以==看堆栈信息==。



- 可在Terminal下输入`jps -l` (java process)定位正在进行的进程号：

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/jps.png" style="float:left;" />

- 查询到编号为13860的进程，是DieTest类的。可通过`jstack 13860`(java stack)查看具体信息：

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJUC/死锁排查.png" style="float:left;" />

可查到详细信息，两个进程互相等待对方的锁。



### 6、偏向锁、轻量级锁、重量级锁

https://www.jianshu.com/p/36eedeb3f912

偏向锁、轻量级锁、重量级锁适用于不同的并发场景：

- 偏向锁：无实际竞争，且将来只有第一个申请锁的线程会使用锁。
- 轻量级锁：无实际竞争，多个线程交替使用锁；允许短时间的锁竞争。
- 重量级锁：有实际竞争，且锁竞争时间长。

另外，如果锁竞争时间短，可以使用自旋锁进一步优化轻量级锁、重量级锁的性能，减少线程切换。

如果锁竞争程度逐渐提高（缓慢），那么从偏向锁逐步膨胀到重量锁，能够提高系统的整体性能。



## 关于进程、线程、并发、并行的面试问题

1. **Java中默认有几个线程？**

   > 默认2个线程，一个main线程，一个GC线程

2. **Java真的能开启线程吗？**

   > 不能。
   >
   > 通过源码`private native void start0();`分析，java只能通过本地方法去调用开启线程。由底层c语言区操作的。

3. **并发与并行的区别？**

   > 并发是多个线程同一时间段共同执行一个资源；
   >
   > 并行是多个线程同一时刻同行。







## JAVA8的ConcurrentHashMap为什么放弃了分段锁?

java8的ConcurrentHashMap使用的是synchronized+cas

**为什么不用ReentrantLock而用synchronized ?**

- 减少内存开销:如果使用ReentrantLock则需要节点继承AQS来获得同步支持，增加内存开销，而1.8中只有头节点需要进行同步。
- 内部优化:synchronized则是JVM直接支持的，JVM能够在运行时作出相应的优化措施：锁粗化、锁消除、锁自旋等等。

