# 函数式接口 函数式编程



## 一、函数式接口

### 1、概念定义

函数式接口在Java中是指：**有且仅有一个抽象方法的接口**。

函数式接口，即适用于函数式编程场景的接口。而Java中的函数式编程体现就是Lambda，所以函数式接口就是可以适用于Lambda使用的接口。只有确保接口中有且仅有一个抽象方法，Java中的Lambda才能顺利地进行推导。

> “语法糖”是指使用更加方便，但是原理不变的代码语法。



**格式：**

```java
public interface 名字 {
    //只能有一个抽象方法
    public abstract method();
}
```



### 2、@FunctionalInterface 注解

该注解是检查该接口是否为函数式接口，不是函数式接口就会编译报错。

报错原因：

1. 没有抽象方法；
2. 抽象方法大于一个。



### 3、自定义函数式接口

一般作为方法的参数和返回值类型使用。

```java
// 写了一个函数式接口
@FunctionalInterface
public interface MyFunInterface {
    public abstract void method();
}
```

- 方法的参数：

  ```java
  public class Test{
      // 函数式接口作为方法参数
      public static void show(MyFunInterface myFunInterface){
          myFunInterface.method();
      }
      
      public static void main(String[] args){
          // 第一种
          show(new MyFunInterface(){
              @Override
              public void method(){
                  System.out.println("通过匿名内部类，实现函数式接口");
              }
          });
          
          // 第二种
          show(() -> System.out.println("通过Lambda，实现函数式接口"));
      }
  }
  ```

  **注意：使用匿名内部类在编译时，会生成一个class文件，而使用Lambda不会生成class文件，因此执行的时候Lambda会比匿名内部类效率高。**

  

## 二、函数式编程

在兼顾面向对象特性的基础上，Java语言通过Lambda表达式与方法引用等，为开发者打开了函数式编程大门。

### 1、Lambda的延迟执行

Lambda的特点就是延迟加载。

#### 性能浪费的日志案例

注：日志可以帮助我们快速的定位问题，记录程序执行过程中的情况，以便项目的监控和优化。

```java
public class PrintLog {
    public static void showLog(int level, String msg){
        if(level==1){
            System.out.println(msg);
        }
    }

    public static void main(String[] args) {
        String msg1 = "Hello";
        String msg2 = "Java";
        String msg3 = "heroC";
        showLog(1,msg1 + msg2 + msg3);
    }
}
```

此打印日志程序，存在性能浪费，如果在main方法中，调用showLog方法，传入的等级不是1，那么字符串依然会被拼接，字符串拼接之后判断等级不是1，就白拼接了，也浪费了资源和性能。

#### 通过Lambda优化日志案例

函数式接口：

```java
// 创建一个函数式接口
@FunctionalInterface
public interface PrintLogInterface {
    public abstract String printLog();
}
```

测试类：

```java
public class PrintLog {

    public static void showLog(int level, PrintLogInterface printLogInterface){
        if(level == 1){
            System.out.println(printLogInterface.printLog());
        }
    }

    public static void main(String[] args) {
        String msg1 = "Hello";
        String msg2 = "Java";
        String msg3 = "heroC";
        int level = 1;
        showLog(level,() -> msg1 + msg2 + msg3);
    }
}
```

**优化分析：**

通过使用Lambda表达式，Lambda表达式有一个很大的有点就是**延迟执行**，调用showLog方法，只将参数传递进去了，先判断是否等于1，判断了如果等于1才会去调用Lambda表达式实现的接口类，进行拼接字符串，然后输出，否则不会拼接字符串。



### 2、使用Lambda作为参数和返回值

- 使用Lambda作为参数

  ```java
  // 写了一个函数式接口
  @FunctionalInterface
  public interface MyFunInterface {
      public abstract void method();
  }
  ```

  ```java
  // 测试类
  public class Test{
      // 函数式接口作为方法参数
      public static void show(MyFunInterface myFunInterface){
          myFunInterface.method();
      }
      
      public static void main(String[] args){
          show(() -> System.out.println("通过Lambda，实现函数式接口"));
      }
  }
  ```

  

- 使用Lambda作为返回值

  ```java
  public class Test {
      public static Comparator<String> newComparator(){
        return (a, b) -> a.length() - b.length(); // 这里就是作为返回值
      };
  
      public static void main(String[] args) {
          String[] arr = {"aaa","bb","c"};
          Arrays.sort(arr,newComparator());
          System.out.println(Arrays.toString(arr));
          // 输出 [c, bb, aaa]
      }
  }
  ```

  

## 三、4大函数式接口 JDK 1.8

主要在`java.util.function`包中



### 1、Supplier\<T> 接口

`java.util.function Interface Supplier<T>`  接口中包含了一个抽象方法`T get()`。用来获取一个泛型参数指定类型的对象数据。

该接口被称为生产型接口，指定接口是什么类型，那么接口中的get方法就会产生什么类型的数据。

```java
public class SupplierTest {
    public static String getString(Supplier<String> sup){
        return sup.get();
    }

    public static int getInt(Supplier<Integer> sup){
        return sup.get();
    }

    public static void main(String[] args) {
        String str = getString(() -> "heroC");
        /*getString(new Supplier<String>() {
            @Override
            public String get() {
                return "heroC";
            }
        });*/
        System.out.println(str);

        int strInt = getInt(() -> 1130);
        System.out.println(strInt);
    }
}
```



#### 练习：求数组元素中最大的数

```java
public class ArrayMaxNum {
    public static int getMax(Supplier<Integer> sup){
        return sup.get();
    }

    public static void main(String[] args) {
        int[] arr = {245,54,2366,23,6543,1130};

        int max1 = getMax(() -> {
            int max = arr[0];
            for (int i = 0; i < arr.length-1; i++) {
                if (arr[i] < arr[i+1]) {
                    max = arr[i+1];
                }
            }
            return max;
        });
        
        System.out.println(max1);
    }
}
```



### 2、Consumer\<T> 接口

`java.util.function Interface Consumer<T>` 接口正好与Supplier接口相反，它不是生产一个数据，而是消费（使用）一个数据，其数据类型由泛型决定。



**抽象方法：**`void accept(T t)` ，意为消费一个数据类型。

```java
public class ConsumerInterfaceImp {
    public static void consumerString(String str, Consumer<String> con){
        con.accept(str);
    }

    public static void main(String[] args) {
        consumerString("heroC",(str) -> {
            // 将字符串反转
            String s = new StringBuffer(str).reverse().toString();
            System.out.println(s);
        });
    }
}
```



**默认方法：**`default Consumer<T> andThen(Consumer<? super T> after)` 需要两个Consumer接口，就可以把两个Consumer接口组合在一起，在对数据进行消费。

```java
// Consumer默认方法源码
default Consumer<T> andThen(Consumer<? super T> after) {
    Objects.requireNonNull(after);
    return (T t) -> { accept(t); after.accept(t); };
}
```

**eg：**

```java
public class ConsumerInterfaceImp {

    public static void consumerAndThen(String str, Consumer<String> con1, Consumer<String> con2){
        // 两个Consumer接口对象，使用默认方法andThen，调用accept方法
        // 执行步骤：con1.accept(str); con2.accept(str); 谁在前面谁先对数据进行消费
        // 消费对象互不影响，即con1消费的是str，con2也消费的是str
        // 如果由多个Consumer对象，可以一直链式调用andThen方法
        con1.andThen(con2).accept(str);
    }

    public static void main(String[] args) {
        consumerAndThen("heroC",
                (str) -> System.out.println(str.toUpperCase()),
                (str) -> System.out.println(str));
    }
}
```

#### 练习：格式化打印信息

下面字符串数组中，存储了很多信息，按照`姓名：xxx，性别：xxx`格式打印出来。使用Consumer接口的Lambda表达式。

```java
public static void main(String[] args){
    String[] array = {"heroC,男", "欧阳娜娜,女"};
}
```

**解答：**

```java
public class ConsumerInterfaceExample {
    public static void printInfo(String[] arr, Consumer<String> con1, Consumer<String> con2){
        for(String str : arr){
            con1.andThen(con2).accept(str);
        }
    }

    public static void main(String[] args) {
        String[] array = {"heroC,男", "欧阳娜娜,女"};
        printInfo(array, str ->{
            System.out.print("姓名：" + str.split(",")[0] + "，");
        }, str ->{
            System.out.println("性别：" + str.split(",")[1]);
        });
    }
}
```



### 3、Predicate\<T> 接口

`java.util.function Interface Predicate<T>`接口，有时候我们需要对某种类型的数据进行判断，返回Boolean值。

**抽象方法：**`boolean test(T t)`

```java
public class PredicateImp {
    public static void method(Predicate<String> pre){
        boolean b = pre.test("heroC");
        System.out.println(b);
    }

    public static void main(String[] args) {
        method(s -> s.length() > 5);
    }
}
// 输出 false
```



**默认方法：**

&& 有fase则fase

|| 有true则true

- `default Predicate<T> and(Predicate<? super T> other)` 有两个或以上的Predicate对象，将结果使用and方法(&&)进行比较，得出最终结果。
- `default Predicate<T> or(Predicate<? super T> other)` 有两个或以上的Predicate对象，将结果使用or方法(||)进行比较，得出最终结果。
- `default Predicate <T> negate()` 调用该方法，可将结果取反

```java
public class PredicateImp {
    // &&
    public static void andTest(Predicate<String> pre1, Predicate<String> pre2){
        boolean b = pre1.and(pre2).test("heroC"); // 谁在前谁先处理test方法的字符串
        System.out.println(b);
    }

    // ||
    public static void orTest(Predicate<String> pre1, Predicate<String> pre2){
        boolean b = pre1.or(pre2).test("heroC");
        System.out.println(b);
    }

    // 取反
    public static void negateTest(Predicate<String> pre1){
        Boolean negate = pre1.negate().test("heroC");
        System.out.println(negate);
    }

    public static void main(String[] args) {
        // 输出 true
        andTest(s -> s.contains("h"), s -> s.contains("C"));
        // 输出 true
        orTest(s -> s.contains("a"), s -> s.contains("C"));
        // 输出 false
        negateTest(s -> s.contains("h"));
    }
}
```



#### 练习：集合信息的筛选

数组中有多条信息，通过Predicate接口筛选符合要求的信息并存到集合ArrayList中，需要满足两个条件：

1. 必须为女生；
2. 姓名为4个字。

```java
public static void main(String[] args){
    String[] array = {"迪丽热巴,女", "古力娜扎,女", "赵丽颖,女", "马尔哈扎,男"};
}
```

**解答：**

```java
public class PredicateExample {
    public static boolean getInfoBoolean(String str, Predicate<String> pre1, Predicate<String> pre2){
        return pre1.and(pre2).test(str);
    }

    public static void main(String[] args) {
        String[] array = {"迪丽热巴,女", "古力娜扎,女", "赵丽颖,女", "马尔哈扎,男"};
        ArrayList<String> arrayList = new ArrayList<>();

        // 筛选信息，将符合的信息加入到集合中
        for (String str : array){
            boolean infoBoolean = getInfoBoolean(
                    str,
                    s -> (s.split(",")[0].length() == 4),
                    s -> s.split(",")[1].equals("女")
            );
            if (infoBoolean){
                arrayList.add(str);
            }
        }

        for(String s: arrayList){
            System.out.println(s);
        }
    }
}

// 输出：
// 迪丽热巴,女
// 古力娜扎,女
```



### 4、Function<T, R> 接口

`java.util.function Interface Function<T,R>`接口，用来根据一个类型的数据得到另一个类型的数据，前者称为前置条件，后者称为后置条件。将T类型的数据，转换为R类型的数据返回

**抽象方法：**`R apply(T t)` 根据T类型，返回一个R类型的结果。

```java
public class FunctionImp {
    public static int toInt(Function<Character, Integer> function){
        return function.apply('C');
    }

    public static void main(String[] args) {
        int toInt = toInt(s -> (int)s);
        System.out.println(toInt); // 输出 67
    }
}
```



**默认方法：**

- `default <V> Function<T,V> andThen(Function<? super R,? extends V> after)`  需要两个Function接口，就可以把两个Function接口组合在一起，在对数据进行处理，返回V类型数据。

**具体使用：**将String类型的数据转换成Integer类型，然后加上10，再将Integer类型转为String类型，返回出结果。

```java
public class FunctionImp {
    
    public static String functionAndThen(
        Function<String, Integer> fun1, Function<Integer, String> fun2){
        // fun1先调用apply方法，将11转换为Integer对象，然后加了10，得出21，
        // 将21值传给下一个fun2调用的apply方法
        return fun1.andThen(fun2).apply("11");
    }

    public static void main(String[] args) {
        String strResult = functionAndThen(
            str -> Integer.parseInt(str) + 10, 
            num -> String.valueOf(num));
        System.out.println(strResult);
    }
}
```



#### 练习：自定义函数模型拼接

**需求：**

1. 将字符串截取年龄；Function<String, String> fun1
2. 将上一步的年龄字符串转为int类型；Function<String, Integer> fun2
3. 将上一步的int类型加上100，返回出int类型的结果。Function<Integer,Integer> fun3

```java
public class FunctionExample {
    public static void method(String str,
                              Function<String, String> fun1,
                              Function<String, Integer> fun2,
                              Function<Integer,Integer> fun3){
        Integer apply = fun1.andThen(fun2).andThen(fun3).apply(str);
        System.out.println(apply);
    }

    public static void main(String[] args) {
        method("heroC,20",
                str -> str.split(",")[1],
                str -> Integer.parseInt(str),
                num -> (num + 100) );
    }
}
// 输出 120
```



## 四、Stream流 JDK 1.8

说到Stream容易想到 IO Stream，而流不一定是IO流。在Java 8中，得益于Lambda所带来的函数式编程，引入了一个全新的Stream概念，用于解决已有集合类库有的弊端。



### 引言 ：通过案例展示Stream流的作用

**需求分析：**

1. 在ArrayList集合中，筛选出姓张的数据；
2. 从筛选出姓张的数据中，再筛选出名字长度为3；
3. 将结果打印出来。

**普通操作：**

```java
public class PrefaceNormal {
    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("张杰");
        arrayList.add("张还行");
        arrayList.add("王小");
        arrayList.add("张怡");
        arrayList.add("唐山");

        // 筛选姓张的
        ArrayList<String> listA = new ArrayList<>();
        for(String str: arrayList){
            if(str.startsWith("张")){
                listA.add(str);
            }
        }

        // 筛选3个字的
        ArrayList<String> listB = new ArrayList<>();
        for(String str: arrayList){
            if(str.length() == 3){
                listB.add(str);
            }
        }

        // 遍历结果
        for (String str : listB){
            System.out.println(str); // 输出：张还行
        }
    }
}
```

可见用了很多次增强for循环，代码量很多，很冗杂，不美观，效率低。

**Stream操作：**

```java
public class PrefaceStream {
    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("张杰");
        arrayList.add("张还行");
        arrayList.add("王小");
        arrayList.add("张怡");
        arrayList.add("唐山");

        arrayList.stream()
            	// Lambda实现Predicate接口中的test方法，返回Boolean值
                .filter(s -> s.startsWith("张")) 
                .filter(s -> s.length() == 3)
            	// Lambda实现Customer接口中的accept方法
                .forEach(name -> System.out.println(name)); 
        // 不对原有的集合内容做改变，只是对符合要求的数据进行筛选
    }
}
```



### 1、流式思想

![](https://gitee.com/turbo30/study_pic/raw/master/prictureForStream/流式思想.png)

这里的`filter`、`map`、`skip`都是对函数模型进行操作，集合元素并没有真正被处理。只有当终结方法`count`执行的时候，整个模型才会按照指定策略执行操作。而这得益于Lambda的延迟执行特性。

> 备注：Stream流 其实是一个集合元素的函数模型，它并不是集合，也不是数据结构，其本身不存储任何元素(或地址值)



### 2、Stream特点



**Stream是一个来数据源的元素队列**

- 元素式特定类型的对象，形成一个队列。Java中的**Stream并不会存储元素**，而是按需计算，使用一次之后，再次调用就会报异常。

```java
public class PrefaceStream {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("张还行", "heroC", "yikeX");
        stream.filter(name -> name.startsWith("张"))
              .forEach(name -> System.out.println(name));
        
        // stream使用了一次了，再使用一次，会报IllegalStateException异常
        // 说明之前的流已经关闭，不能再使用了。
        stream.forEach(name -> System.out.println(name));
    }
}
```

- 数据源 流的来源。可以是集合，数组等。



**Stream操作两个基础的特征：**

- **Pipelining(流水线)**：中间操作都会返回流对象本身。这样多个操作可以串联成一个管道，如同流式风格。这样做可以对操作进行优化，比如延迟执行和短路。
- **内部迭代**：以前对集合遍历都是通过Iterator或者增强for的方式。显示的在集合外部进行迭代，这叫做外部迭代。Stream提供了内部迭代的方式，流可以直接调用遍历方法。

当使用一个流的时候，通常包括三个基本步骤：获取一个数据源 → 数据转换 → 执行操作获取想要的结果，每次转换原有Stream对象不改变，返回一个新的Stream对象，这就允许对其操作可以像链条一样排列，变成一个管道。



### 3、获取Stream流对象

`java.util.stream Interface Stream<T>` 接口

**获取流的方式：**

- 所有`Collection`集合都可以通过`stream`默认方法获取流
- `stream`的静态方法`of`可以获取对应的流，of方法是可变参数，可变参数底层就一数组

```java
public class CollectionStream {
    public static void main(String[] args) {
        // List集合获取流
        List<String> list = new ArrayList<>();
        Stream<String> stream = list.stream();
        // Set集合获取流
        Set<String> set = new HashSet<>();
        Stream<String> stream1 = set.stream();
        // 键获取流
        Map<String,String> map1 = new HashMap<>();
        Collection<String> collKey = map1.keySet();
        Stream<String> stream2 = collKey.stream();
        // 值获取流
        Map<String,String> map2 = new HashMap<>();
        Collection<String> collValue = map2.values();
        Stream<String> stream3 = collValue.stream();
        // 键值对获取流
        Map<String,String> map3 = new HashMap<>();
        Collection<Map.Entry<String, String>> entries = map3.entrySet();
        Stream<Map.Entry<String, String>> stream4 = entries.stream();
        // 流的静态方法of，可变参数获取流
        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5);
        // 流的静态方法of，数组获取流
        Integer[] num = {1,2,3,4};
        Stream<Integer> num1 = Stream.of(num);
        String[] str = {"a","b"};
        Stream<String> str1 = Stream.of(str);
    }
}
```



### 4、常用方法

![](https://gitee.com/turbo30/study_pic/raw/master/prictureForStream/Stream常用方法.png)

流模型的操作很丰富，这里介绍一些常用的API。这些方法可以分为两种：

- **延迟方法**：返回值类型仍然是Stream接口自身类型的方法，因此支持链式调用。
- **终结方法**：返回类型不再是Stream接口自身类型的方法，因此不再支持链式调用。常见`count`和`forEach`方法，更多见API文档。



#### 逐一处理：forEach

forEach与增强for是不同的

`void forEach(Consumer<? super T> action)` 

```java
public class PrefaceStream {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("张还行", "heroC", "yikeX");
        stream.forEach(name -> System.out.println(name)); // 对流中的每一个数据进行处理
    }
}
```



#### 过滤：filter

可通过filter将一个流转换成另一个流

`Stream<T> filter(Predicate<? super T> predicate)`

```java
public class PrefaceStream {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("张还行", "heroC", "yikeX");
        stream.filter(name -> name.startsWith("张")) // 可以将流过滤转换成另一个只符合要求的流
              .forEach(name -> System.out.println(name)); // 对新的流每个数据进行遍历处理
    }
}
// 输出：张还行
```



#### 映射：map

将流中的方法映射到另一个流中

`<R> Stream<R> map(Function<? super T,? extends R> mapper)`

将`T类型`的数据转换成`R类型`的数据就是“映射”。

```java
public class PrefaceStream {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("1", "2", "3", "4");
        Stream<Integer> stream1 = stream.map(str -> Integer.parseInt(str));
        stream1.forEach(num -> System.out.print(num+" "));
    }
}
// 输出：1 2 3 4 
```



#### 统计个数：count

`long count()` 返回long类型数据，统计流中元素的个数

```java
public class PrefaceStream {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("1", "12", "23", "1234");
        long count = stream.filter(str -> str.startsWith("1")).count();
        System.out.println(count);
    }
}
// 输出：3
```



#### 取用前几个：limit

limit方法可以对流中的数据进行截取，只取用前n个。

`Stream<T> limit(long maxSize)`

```java
public class PrefaceStream {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("1", "12", "23", "1234");
        stream.limit(3).forEach(str -> System.out.print(str+" "));
    }
}
// 输出：1 12 23
```



#### 跳过前几个：skip

如果流的当前长度大于n，则跳过前n个元素；否则将会得到一个长度为0的空流。

`Stream<T> skip(long n)`

```java
public class PrefaceStream {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("1", "12", "23", "1234");
        stream.skip(2).forEach(str-> System.out.print(str+" "));
    }
}
// 输出：23 1234 
```



#### 组合：concat

将有两个流，合并成一个流

`static <T> Stream<T> concat(Stream<? extends T> a,Stream<? extends T> b)`

> 备注：这是一个静态方法，与`java.lang.String`当中的`concat`方法是不同的。

```java
public class PrefaceStream {
    public static void main(String[] args) {
        Stream<String> stream1 = Stream.of("heroC");
        Stream<String> stream2 = Stream.of("yikeX");
        Stream.concat(stream1,stream2).forEach(str -> System.out.print(str+" "));
    }
}
// 输出：heroC yikeX 
```



### 练习：集合元素的处理

**题目：**

现在有两个ArrayList集合存储队伍当中的多个成员姓名，使用Stream流完成以下操作：

1. 第一个队伍只要名字为3个字的成员姓名；存储到一个新集合中。
2. 第一个队伍筛选后只要前3个人名；存储到一个新集合中。
3. 第二个队伍只要姓张的成员姓名；存储到一个新集合中。
4. 第二个队伍筛选之后不要前2个人名；存储到一个新集合中。
5. 将两个筛选后的队伍合并为一个队伍；存储到一个新集合中。
6. 根据姓名创建`Person`对象；存储到一个新集合中。
7. 打印整个队伍的`Person`对象信息。

```java
public class StreamExample {
    public static void main(String[] args) {
        ArrayList<String> one = new ArrayList<>();
        one.add("张还行");
        one.add("张还");
        one.add("张行");
        one.add("施柏宇");
        one.add("林俊杰");
        one.add("张一山");

        // 1. 第一个队伍只要名字为3个字的成员姓名；存储到一个新集合中。
        Collection<String> nameThree = new ArrayList<>();
        one.stream().filter(name -> name.length()==3).forEach(name -> nameThree.add(name));

        // 2. 第一个队伍筛选后只要前3个人名；存储到一个新集合中。
        Collection<String> nameThird = new ArrayList<>();
        nameThree.stream().limit(3).forEach(name -> nameThird.add(name));

        ArrayList<String> two = new ArrayList<>();
        two.add("张一山");
        two.add("黄轩");
        two.add("张行");
        two.add("张若昀");
        two.add("罗晋");
        two.add("张子枫");

        // 3. 第二个队伍只要姓张的成员姓名；存储到一个新集合中。
        Collection<String> nameZhang = new ArrayList<>();
        two.stream().filter(name -> name.startsWith("张")).forEach(name -> nameZhang.add(name));

        // 4. 第二个队伍筛选之后不要前2个人名；存储到一个新集合中。
        Collection<String> nameSkip = new ArrayList<>();
        nameZhang.stream().skip(2).forEach(name -> nameSkip.add(name));

        // 5. 将两个筛选后的队伍合并为一个队伍；存储到一个新集合中。
        Collection<String> nameConcat = new ArrayList<>();
        Stream.concat(nameThird.stream(), nameSkip.stream()).forEach(name -> nameConcat.add(name));

        // 6. 根据姓名创建`Person`对象；存储到一个新集合中。
        Collection<Person> namePerson = new ArrayList<>();
        nameConcat.stream().forEach(name -> namePerson.add(new Person(name)));

        // 7. 打印整个队伍的`Person`对象信息。
        long count = namePerson.stream().count();
        System.out.println("筛选后的队伍总人数为："+count);
        namePerson.stream().forEach(namePer -> System.out.println(namePer));
    }
}
```

**输出：**

```
筛选后的队伍总人数为：5
Person{name='张还行'}
Person{name='施柏宇'}
Person{name='林俊杰'}
Person{name='张若昀'}
Person{name='张子枫'}
```



## 五、方法引用



### 1、语法引用符

双冒号`::`为引用运算符，而它所在的表达式被称为方法引用。如果Lambda要表达的函数方案已经存在于某个方法的实现中，那么则可通过双冒号来引用该方法作为Lambda的替代者。

语义分析

如：`System.out`对象中有一个重载的`println`方法，其中`System.out`是已经存在的对象，其中`println`方法是已经存在的方法，所以有2种写法：

- Lambda表达式写法：`s -> System.out.println(s)`
- 方法引用写法：`System.out::println`

第一种语义指：拿到参数之后经Lambda之手，继而传给System.out.println方法去处理。

第二种语义指：直接让`System.out`中的`println`方法来取代Lambda。

注：Lambda中传递的参数一定是方法引用中的那个方法可以接收的类型，否则会报异常。



### 2、通过对象名引用成员方法

对象一定要是存在的，不然无法使用。

**PrintString.java**

```java
@FunctionalInterface
public interface PrintString {
    String print(String str);
}
```

**StringToUppercase.java**

```java
public class StringToUppercase {
    public String stringToUppercase(String str){
        return str.toUpperCase();
    }
}
```

**StringToUppercaseTest.java**

```java
public class StringToUppercaseTest {
    public static void printUppercase(PrintString p){
        System.out.println(p.print("heroC"));
    }
    public static void main(String[] args) {
        StringToUppercase obj = new StringToUppercase();
        // 使用方法引用，实现PrintString接口的print方法，注重方法体，该方法需要返回Stirng类型的数据。
        // 方法体实则是，通过obj对象调用了stringToUppercase方法。而这个方法作为了函数式接口print抽象方法的具体实现。将参数传递进去执行结果。参数可前后推导
        printUppercase(obj::stringToUppercase);
        /*
        printUppercase()方法是使用的函数式接口因此需要实现PrintString中的print方法，又因为给print方法传递了字符串“heroC”
        printUppercase(new PrintString(){
        	@Override
        	String print(String str){
        		obj.stringToUppercase(String str);
        	}
        });
        */
    }
}
// 输出：HEROC
```



### 3、通过类名称引用静态方法

**PrintString.java**

```java
@FunctionalInterface
public interface PrintString {
    String print(String str);
}
```

**StringToUppercase.java**

```java
public class StringToUppercase {
    public static String stringToLowercase(String str){
        return str.toLowerCase();
    }
}
```

**StringToUppercaseTest.java**

```java
public class StringToUppercaseTest {
    public static void printUppercase(PrintString p){
        System.out.println(p.print("heroC"));
    }
    public static void main(String[] args) {
        // 使用方法引用，实现PrintString接口的print方法，注重方法体，该方法需要返回Stirng类型的数据。
        // 方法体实则是，通过方法名调用了stringToLowercase静态方法。
        // 将参数传递进去执行结果。参数可前后推导
        printUppercase(StringToUppercase::stringToLowercase);
    }
}
// 输出：heroc
```



### 4、通过super引用成员方法

使用前提，必须有子父类关系。

PrintInfo.java

```java
@FunctionalInterface
public interface PrintInfo {
    void print();
}
```

Human.java

```java
public class Human {
    public void printHuman(){
        System.out.println("我是父类Human");
    }
}
```

Man.java

```java
public class Man extends Human {
    @Override
    public void printHuman() {
        System.out.println("我是子类Man");
    }

    public void method(PrintInfo prin){
        prin.print();
    }

    public void printMethod(){
        // 通过方法引用，调用super父类的printHuman方法
        method(super::printHuman);
    }

    public static void main(String[] args) {
        new Man().printMethod();
    }
}
// 输出：我是父类Human
```



### 5、通过this引用成员方法

**PrintInfo.java**

```java
@FunctionalInterface
public interface PrintInfo {
    void print();
}
```

**Human.java**

```java
public class Human {
    public void printHuman(){
        System.out.println("通过this引用打印");
    }

    public void method(PrintInfo prin){
        prin.print();
    }

    public void methodTest(){
        // 调用this中的printHuman方法重写接口的print方法
        method(this::printHuman);
    }

    public static void main(String[] args) {
        new Human().methodTest();
    }
}
// 输出：通过this引用打印
```



### 6、类的构造器引用

使用`类名::new`的格式表示

**PersonInterface.java**

```java
@FunctionalInterface
public interface PersonInterface {
    Person func(String name);
}
```

**Person.java**

```java
public class Person {
    private String name;

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}
```

**Test.java**

```java
public class Test {
    public static void method(PersonInterface per){
        Person person = per.func("heroC");
        System.out.println(person);
    }

    public static void main(String[] args) {
        // 将PersonInterface的func方法重写了，调用了Person构造器，因为传递了heroC字符串
        // 会自动调用带参构造器
        method(Person::new);
    }
}
// 输出：Person{name='heroC'}
```



### 7、数组的构造器引用

如：`int[]::new`

**ArrayInterface.java**

```java
public interface ArrayInterface {
    int[] createArray(int len);
}
```

**ArrayImp.java**

```java
public class ArrayImp {
    public static int[] method(int len, ArrayInterface arr){
        return arr.createArray(len);
    }

    public static void main(String[] args) {
        // 数组构造器引用
        System.out.println( method(10, int[]::new).length );
    }
}
// 输出：10
```

