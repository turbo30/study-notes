# JAVA 基础阶段

Write once, run anywhere...

Java基础、流程控制、方法、数组



## 一、Java特性优势与3大版本

**Java特性优势**

简单性、面向对象、可移植性、高性能、分布式、动态性、多线程、安全性、健壮性、跨平台性



**3大版本** ( 一个框架也可说一个规范 )

JavaSE：标准版（桌面程序，控制台开发...）

JavaME：嵌入式开发（手机，小家电...）

JavaEE：企业级开发（Web端，服务器开发...）

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/javaSEMEEE关系.png)



## 二、JDK，JRE，JVM

**JDK**： Java Development Kit (Java开发者工具)

**JRE**：Java Runtime Envrionment (Java运行环境)

**JVM**：Java Virtual Machine (Java虚拟机，可在任何环境下运行)

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/jdk、jre、jvm.png)

首先需要确定的是JDK里是包含JRE的，而JRE里又包含JVM，它们区别在于面向的服务对象不同所以进行了不同的包装。

`JVM`：JVM是面向操作系统的，它负责把程序运行编译成的.Class字节码解释成系统所能识别的指令并执行，同时也负责程序运行的内存的管理。

`JRE`：JRE是面向于程序的，JRE对JVM进行了一层包装，它除了提供JVM的功能之外，还提供了一套语言需要编译成Class后在JVM内运行所依赖环境（比如说 String.class、Object.class等这种运行时必须依赖的对象）。

`JDK`：JDK是面向更上层的开发人员，JDK在JRE的基础上又进行了一层包装，它除了提供运行功能之外，还对开发人员提供了一套常用的开发工具和类库，来方便开发人员写代码（比如说方便部署的编译工具，方便开发的工具类等）。

## 三、JDK8 安装

JDK 8 是运用最广的开发者工具，所以推荐使用JDK 8版本



### 环境变量配置

PATH_HOME变量：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/JAVA_HOME.png)

PATH变量:

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/PATH.png)

CLASSPATH变量：

其中%...%是引用变量，其中JAVA_HOME是已经创建好了的系统变量，这里引用可以将变量名的地址引用过来，path需要配置jre目录下的bin目录和根目录下的bin目录，该俩目录下有相关的java内部的小程序。



### 程序运行机制

~~~txt
javac 编译 .java 生成 .class
java 执行 .class
~~~

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/程序运行机制.png)



编译型：将文件进行完全编译之后，在其他任何地方都可以识别

解释型：走一步，解释一次，按需求进行解释，达到一步实现一个需求



### 什么是IDE、SDK

**IDE(Integrated Development Environment)  集成开发环境**

用于提供程序开发环境的应用程序，一般包括代码编辑器、编译器、调试器和图形用户界面等工具。集成了代码编写功能、分析功能、编译功能、调试功能等一体化的开发软件服务套。

比如：Eclipse、Intellij Idea 等



**SDK (Software Development Kit) 软件开发工具包**

比如：JDK 8



### .jar 是什么文件

jar包就是压缩包，是以java设定的规则生成扩展名为.jar的压缩包，里面存放的是各种class文件。常称驱动。



## 四、Java基本规范



### 1、关键字

<a name="keyW"> </a>

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/关键字.png" style="zoom:80%; float:left;" />

**标识符命名规则：**

- 所有的标识符都应该以字母(A-Z a-z)，美元符($)，下划线(_)开始
- 首字符之后可以是字母，美元符，下划线或数字等其他合法字符组合
- 不能使用关键字作为变量名或方法
- 标识符是区分大小写的
- 可以使用中文作为变量名，但一般不建议，也不建议使用拼音



### 2、数据类型

- 强类型语言

  要求变量的使用要严格符合规定，所有变量都必须先定义后才能使用

- 弱类型语言

  要求变量的使用符合规定 比如： JS、VB

- Java的数据类型分为

  + 基本类型（primitive type）：数值类型、布尔类型
    * 整型：byte(1字节)、short(2字节)、int(4字节)、long(8字节)**一定要使用L**
    * 浮点型：float(4字节)**一定要使用F**、double(8字节)
    * 字符型：char(2字节)
    * 布尔型：boolean

  + 引用类型（reference type）：类、接口、数组String等

```java
public class Demo{
    public static void main(String[] args){
        // 八大基本数据类型
        
        // 整数
        byte num1 = 10;
        short num2 = 20;
        int num3 = 30; // 最常用
        long num4 = 30L; // Long类型要在数字后面加一个L，标识是一个Long类型(为方便区分long类型一定要用L)
        
        // 小数
        float num5 = 20.1F; // float类型要在数字后面加一个F，标识是一个float类型
        double num6 = 2.1415926;
        
        // 字符
        char letter = 'A';
        // 字符串，String不是关键字，它是一个类
        // String name = "hero";
        
        // 布尔值
        boolean flagT = true;
        boolean flagF = false;
    }
}
```

 注意：byte与byte计算会转换成int类型，short与short进行计算也会转换成int类型。如果还是需要byte类型或者是short类型就必须得强转，但会有数据溢出的情况。



#### <u>关于数据类型的面试问题</u>

- **银行业务怎么表示？**

  银行业务与钱有关，而float和double类型表示的浮点数是有限的、离散的、舍入误差、大约性、接近但不等于性；所以避免使用float和double来进行比较使用。

  有专门的数学工具类：**BigDecimal**

```java
// 以下的float与double的比较都是有问题的，再一次证明浮点数的误差性

float f = 0.1f; // 0.1
double d = 1.0/10; // 0.1
System.out.println(f==d);
// 输出结果为false

float f1 = 232323123412f;
float f2 = f1 + 1;
System.out.println(f1==f2);
// 输出结果为true
```

- **Boolean类型占几个字节？**

  未精确定义字节。在java中所操作的Boolean值，在编译之后都是用java虚拟机中的int数据类型来代替，而Boolean数组将会别编译成java虚拟机中的byte数组，每个元素Boolean元素占8位。





#### 数据类型比较以及String类型的拓展

- 字符：

```java 
// 字符
char letter1 = 'A';
char letter2 = '中';

System.out.println(letter1);
System.out.println((int)letter1);
System.out.println(letter2);
System.out.println((int)letter2);
// 输出结果为：
// A
// 65
// 中
// 20013

// 以Unicode的编码形式赋值
char ucode = '\u0061';
System.out.println(ucode);
// 输出 a
```

字符类型在存储字符的时候都是以Unicode表来存储的，本质还是存储的数字。



- String：

  字符串，String不是关键字，它是一个类

```java
// String的比较

String a = new String("hello world");
String b = new String("hello world");
System.out.println(a==b);
// 输出false

String c = "hello world";
String d = "hello world";
System.out.println(c==d);
// 输出true
```

第一个输出false的原因，是因为这里new了一个String实体，在内存中new了一个存储空间，a变量new了一个String类型的空间，b变量new了一个String类型的空间，两者new的是不同的空间，因此地址也是不同的。所以在作比较的时候是地址之间的比较，因此就输出false；

第二个是直接将值赋值给变量，这里的两个变量都存储的是值，又因为值是相同的，所以在作比较的时候，自然就是相等的，因此就输出true。



### 3、类型转换(<u>JDK7新特性,转换类型注意的问题,面试</u>)

- 由于java是强类型语言，所以要进行有些运算的时候的，需要用到类型转换

```java
低 -----------> 高
byte,short,char -> int -> long -> float -> double
```

**强制类型转换**：等级高的向等级低的转换

**自动类型转换**：等级低的向等级高的转换

```java
// 强制类型转换
int i = 30;
byte b = (byte)i;

// 自动类型转换
// JDK7新特性，数字之间可以用下划线分割
long l = 10_0000_0000;
float f = l;

// 关于类型转换的运算注意溢出问题
int money = 10_0000_0000;
int years = 20;
// int total = money * years; 超出范围，会溢出
// long total = money * years; 因为是两个int类型数据在相乘，相乘之后还是int类型，这时已经溢出，再转换为long类型，结果也是错的
long total = money * ( (long)years ); 
// 这里是将years先强制转换为long类型，money也会自动转换成long类型，最终是long类型相乘，结果真确
System.out.println(total);
// 输出 20000000000
```

- **注意**

1. 不能对布尔值进行转换；
2. 不能把对象类型转换为不相干的类型；
3. 再把高容量转换为低容量的时候，强制转换，注意溢出问题；
4. 转换的时候可能会出现内存溢出问题，精度问题！



**相加，只要有long类型，最终不同类型相加之后得出的结果一定是long类型；只要有double类型，最终不同类型相加之后得出的结果一定是double类型**





### 4、变量、常量、命名规范



#### 1） 变量和变量作用域

- 变量必须是合法的[标识符](#keyW)

- 每个变量都有类型，基本类型和引用类型

- 变量声明是一句完整的语句

```java
public class Variable{
    static int allClicks = 0; // 类变量
	String str = "hello"; // 实例变量
    
    public void method(){
        int i = 0; // 局部变量
    }
}
```

类变量：在类中使用static关键字声明的变量为类变量，在类的方法中可以直接使用该变量

实例变量：在其他类中使用该变量，需要new出对象，通过调用get，set方法使用

局部变量：只在一个方法内有作用



#### 2）常量

常量(Constant)：初始化后不能再改变的值。

常量一般使用大写字符，关键字**final**。

> **final 常量名 = 值；**

~~~java
final double PI = 3.14;
~~~



#### 3） <u>命名规范</u>

**常量**：大写字母和下划线 MAX_VALUE

**变量、方法名**：驼峰命名法 lastName 、 runRun()

**类名**：首字母大写和驼峰命名法 GoodMan



### 5、运算符

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/运算符.png" style="zoom:70%; float:left" />

**相加，只要有long类型，最终不同类型相加之后得出的结果一定是long类型；只要有double类型，最终不同类型相加之后得出的结果一定是double类型**



#### 1） ++ 与 -- 的区分

```java
int a = 3;

int b = a++; // 执行完这行代码后，先给b赋值，再自增；如同：
// int b = a;
// a = a + 1;

int c = ++a; // 执行完这行代码前，先自增，再给b赋值；如同：
// a = a + 1;
// int c = a;
```



#### 2）幂运算等复杂的数学运算

会使用工具类，如Math类。

幂运算调用Math的pow方法可是进行幂运算。

```java
int pow = Math.pow(3,2);
System.out.println(pow);
// 输出9 ，即 3*3
```



#### 3）逻辑运算&&与||的执行特点 <u>注意</u>

```java
int a = 3;
int b = 5;
System.out.println( (a>b) && (a++<b) );
System.out.println(a);
// 输出false
// 3
```

以上运算可以验证，当进行&&运算时，前面遇到了false那么后面的语句是不执行的，因此a并没有自增，依然是3。

同理||运算，当前面遇到true时，后面是不会执行的；



#### 4）<u>位运算符、字符串连接面试问题</u>

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/位运算.png" style="zoom:70%; float:left" />

- **如何最快输出2*8的结果 ？**

要想最快的输出运算结果，需要直接与位运算打交道，这样在编译的时候直接与底层打交道，效率极高，免除了其他的复杂的转换。

```java
System.out.println(2<<3);
// 输出结果为16，即 2*2=4 4*2=8 8*2=16 以2为基础乘3次2
```

**解析：(<span style="color:red">请死记</span>)**

<< 相当于 *2；

\>> 相当于 /2



- **以下结果有区别吗？**

```java
int a = 10;
int b = 20;
System.out.println(""+a+b);
System.out.println(a+b+"");
```

是有区别的。

第一个输出：1020；原因是当字符串在运算前方出现，那么之后的+就会自动将其拼接起来，形成字符串

第二个输出：30；原因是因为字符串在a+b之后，在执行的时候a+b默认为+运算，然后遇到了字符串，则会将计算结果与空字符串加起来，得出最终结果



### 6、JavaDoc

参数信息：

- @author 作者名
- @version 版本号
- @since 指明需要最早使用的jdk版本
- @param 参数名
- @return 返回值情况
- @throws 异常抛出情况

```java
public class Doc{
    /**
    *@author zhang
    *@version 1.0
    */
    public static void main(String[] args){
        String name;
        /**
    	*@author zhang
    	*@version 1.0
    	*@param name
    	*@return value
    	*@throws Exception
    	*/
        public String test(String name) throws exception{
            return name;
        }
    }
}
```

javadoc命令是用来生成自己API文档的

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/javadoc.png)



## 五、Java流程控制



### 1、Scanner对象

引用于java.util.Scanner，是jdk5的新特性，可通过Scanner类来获取用户的输入

语法：

> **Scanner scanner = new Scanner(System.in);**
>
> **scanner.close();**

<span style="color:red">注意</span>：凡是属于IO流的，使用完都需要将其关闭，否则会占用资源

```java
Scanner scanner = new Scanner(System.in);
System.out.println("请输入：");
String str = scanner.nextLine();
System.out.println("你输入的是："+str);
scanner.close();
```



#### next()与nextLine()的区别

- next()
  1. 一定要读取到有效字符后才可以结束输入
  2. 只要输入有效字符之后，将其后面输入的空白作为分割符或者结束符
  3. next()不能得到带有空格的字符串
- nextLine()
  1. 以Enter为结束符，也就是说该方法返回的是输入回车之前的所有字符
  2. 可以获得空白



**hasNext()**：可以判断是否有下一个字符

**hasNextLine()**：可以判断是否有下一行

有nextInt()、hasNextInt()、nextLineByte()、hasNextLineByte() ...



### 2、顺序结构

**JAVA的基本结构就是顺序结构**，除非特别指明，否则就按照顺序一句一句执行。

顺序结构是**最简单**的算法结构，<span style="color:red">是任何一个算法**都离不开的**一种基本算法结构</span>。



### 3、选择结构

#### 1）if选择结构

- if单选择结构

  ```java
  if(true){
      ...
  }
  ```

- if双选择结构

  ```java
  if(true){
      ...
  }else{
      ...
  }
  ```

- if多选择结构

  ```java
  if(布尔表达式1){
      ...
  }else if(布尔表达式2){
      ...
  }else if(布尔表达式3){
      ...
  }else{
      ...
  }
  ```

- 嵌套的if结构

  ```java
  if(true){
      ...
      if(true){
          ...
     	 }
  }
  ```



#### 4）Switch选择结构 <u>拓展hashCode方法</u>

```java
char grade = 'B';
switch(grade){
    case 'A':
        System.out.println("优秀");
        break;
    case 'B':
        System.out.println("良好");
    case 'C':
        System.out.println("差");
    default:
        System.out.println("成绩无效！");
}
```

以上语句为switch...case选择结构，当grade为B时，将执行case为B以下的所有语句，即为**case穿透**。

如果grade为A，那么将case为A的语句执行完毕后，遇到break，则跳出switch。

<span style="color:red">**注意:**</span>

switch的表达式可以使用String类型，是<span style="color:red">JDK 7增加的新特性</span>！！！

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/switch与String.png)

通过.java编译生成的.class文件，在IDEA软件中打开.class文件，该IDEA可以对.class文件进行反编译。可以很清楚的看到String类型在编译之后，实际上是使用hashCode()方法将字符串转成了hash值来进行比较！！！



### 4、循环结构

#### 1）while循环

```java
int i = 2;
while(i!=2){
    System.out.println(i);
}
// 不输出
```

#### 2）do...while循环

```java
int i = 2;
do{
    System.out.println(i);
}while(i!=2);
// 输出2
```

#### 3）for循环

```java
for(int i = 0; i<100; i++){
    ...
}
```

for循环语句是支持迭代的一种通用结构，是<span style="color:red">最有效、最灵活</span>的循环结构。

##### 九九乘法表

```JAVA
for(int i=1; i<=9; i++){
    for(int j=1; j<=i; j++){
        System.out.print(j+"*"+i+"="+(i*j)); // 按行输出
    }
    System.out.println(); // 每行输完，换行
}
```

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/99乘法表.png" style="float:left;" />



#### 4）foreach 增强for循环

jdk5增加的新特性。java5 引入了一种主要用于数组或集合的增强型for循环。

```java
int[] numbers = {10,20,30};
for(int x:numbers){
    System.out.print(x);
}
// 输出 10 20 30
```

将numbers数组中的元素从头开始依次赋值给x，来进行循环体中的语句。



##### for与foreach

经遍历10w数据量

for与foreach在数组集合中遍历没有什么太大区别，for会比foreach好一丢丢

但是在遍历linkedArrayList集合的时候foreach效率好很多。

> 《阿里巴巴手册》表示
>
> 不能在foreach中使用add/remove等方法，因为在高并发下，可能会发生ConcurrentModificationException 异常。取而代之是Iterator方式，并且需要对其加锁。



#### 5）break、continue区别

- break

  break在任何循环语句的主题部分，均可用break控制循环的流程。break用于<span style=
  "color:red">强行退出循环</span>，不执行循环中剩余的部分。(也可在switch中使用)

- continue

  continue语句用于在循环语句体中，用于<span style="color:red">终止某一次的循环过程</span>，之后的语句就不执行，跳回循环最开始的地方又开始继续执行下一次循环。

##### <u>寻找质数，最优方法</u>

带标签的continue，类似于goto( java中没有goto )

  ```java
// 寻找101~150之间的质数  标签做了解就可以了
outer:for(int i=101; i<=150; i++){
      for(int j=2; j<i/2; j++){
          if(i%j == 0){
              continue outer;
          }
      }
    System.out.print(i+"\t");
  }

// 寻找1~100的质数：只能被1和它本身整除
int flag = 0;
for(int i=1; i<=100; i++){
    for(int j=1; j<=i; j++){
        if(i%j == 0){
            flag++;
        }
    }
    if(flag == 2){
        System.out.print(i+"\t");
    }
}
  ```

  解析：需要知道的是，判断质数只需要判断<span style="color:red">该数能不能被2到该数除以2的数整除</span>，只要出现一个整除就不是质数；这里的outer是为for定义的标签，continue outer可以返回到标签定义位置，继续执行。



## 六、Java方法

Java方法是语句的集合，它们在一起执行一个功能。

**设计方法的原则**：方法本意是功能块，就是实现某个功能的语句块的集合。我们设计方法的时候，最好保持方法的原子性，<span style="color:red">就是一个方法只能实现一个功能，这样利于我们后期的扩展</span>。

方法尽量写在main方法的外面，在main方法中调用，还有利于JVM的实现。



### 1、方法的定义

方法包含一个方法头和一个方法体。

- 修饰符
- 返回值类型
- 方法名
- 参数类型
- 方法体

```java
修饰符 返回值类型 方法名(参数类型 参数名){
    ...
    方法体
    ...
    return 返回值;
}
```

#### return两个作用

第一个：返回值；第二个：终止方法。



#### <u>值传递与引用传递 很重要</u> 

[为什么说Java中只有值传递](https://www.cnblogs.com/wchxj/p/8729503.html)

- 值传递（pass by value）是指在调用函数时将实际参数复制一份传递到函数中，这样在函数中如果对参数进行修改，将不会影响到实际参数。    

```java
public class Demon1{
    public static void main(String[] args){
        int a = 1;
        System.out.println(a);
        // 输出1
        change(a);
        System.out.println(a);
        // 输出1
    }
    
    public static void change(int a){
        a = 10;
    }
}
```

通过测试，两次输出结果a都是等于1，因为这是值传递。当调用change方法的时候，传给change方法的是1这个值，所以把1这个值传给了change方法定义的形参a，此时只是把方法的形参a的值改变成了10，且没有返回。而主方法中变量a的值仍然是没有改变的。所以是把值进行了传递，即值传递。



- 引用传递（pass by reference）是指在调用函数时将实际参数的地址直接传递到函数中，那么在函数中对参数所进行的修改，将影响到实际参数。(**引用传递本质是值传递**)

```java
public class Demon2{
    public static void main(String[] args){
        Person person = new Person();
        System.out.println(person.name);
        // 输出 null
        Demon2.change(person);
        Sytem.out.println(person.name);
        // 输出 张还行
    }
    
    public static void change(Person person){
        person.name = "张还行";
    }
}

class Person{
    String name;
}
```

new一个类，就是创建一个实例对象，就是开辟一个空间。通过测试，发现两次输出结果不同，这里是引用传递，实则就是值传递。第一次输出，因为没有给实例的属性name赋值，所以默认值为null；第二次输出是张还行，因为调用了change方法，将new出的这个实体对象值，传递给了change方法，change方法接收到了实体对象这个值，该方法将这个实体对象值里的name属性的值更改了，而这个实体对象的值是new出的一个存储空间，存放了属性，这里将实体对象给change方法，实则就是把这个实体对象的地址值传了过去，那么更改的就是地址所对应的空间里的属性，那么主方法也是引用的这个空间，change方法也是同样引用的这个空间，所以第二次会输出张还行。实则就是值传递，只是这个值比较特殊，是一个空间地址值。

**Java中是值传递**的，当传递对象参数是值会改变，是因为值的内容是对象的引用。



### 2、方法的重载

##### <u>笔试可能遇到</u>

重载就是在一个类中，有相同的函数名称，但形参不同的函数。

**规则**：

- 方法名称必须相同
- 参数列表必须不同
- 方法的返回类型可以相同也可以不同
- 仅仅返回类型不同不足以成为方法的重载

**实现理论**：方法名称相同时，JVM(java虚拟机)编译器会根据调用方法的参数个数、参数类型等去逐个匹配，以选择对应的方法，如果匹配失败，则编译器报错。



### 3、命令行传参

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/terminal传参.png" style="zoom:75%; float:left" />

通过terminal，执行程序，并实现传递参数



### 4、可变参数

- JDK 1.5开始，Java支持传递同类型的可变参数给一个方法；
- 在方法声明中，在指定参数类型后加一个省略号(...)；
- 一个方法中只能指定一个可变参数，它必须是方法的最后一个参数。任何普通的参数必须在之前声明。

```java
public static void printMax(int... numbers){
    if(numbers.length == 0){
        System.out.println("No argument passed");
        return;
    }
    
    int result = numbers[0];
    
    for(int i = 1; i < numbers.length; i++){
        if(numbers[i] > result){
            result = numbers[i];
        }
    }
    System.out.println("The max value is:"+ result);
}
```

实则传入的可变参数就是一个数组，可以传递多个不确定个数的值，以数组的形式存储。



### 5、递归

递归就是自己调用自己的过程。

eg：

```java
// 阶乘
public static int f(int n){
    if(n == 1){
        return 1;
    }else{
        return n * f(n-1);
    }
}
```

#### <u>递归在java中的缺点</u>

因为java使用的是栈机制，在使用递归的时，如果大量调用自身方法，那么在栈中就存在很多该方法，使得在物理上存储空间会大量被使用，以至于产生很多额外时间的开销，足够多时可能引起程序崩溃卡死，递归深度越大，时控性越不好。所以在java中，调用自身次数太多了，能不用递归就不用递归。



### 6、静态方法和非静态方法

#### <u>静态方法和非静态方法的理解</u>

- 静态方法的关键字：static

  静态的方法就是类的方法，当类存在的时候，该方法就已经存在。不同类之间可直接通过 `类名.静态方法名` 调用；同类的静态方法，可互相直接调用。

- 非静态方法，不加static

  非静态方法需要在类进行实例化之后，才被系统创建出来，所以晚于静态方法存在。不同类之间在调用非静态方法时，需要new一个对象，才能调用；同类之间静态方法不可直接调用非静态方法，因为未被实例，非静态方法是不存在的。

续见：[Java学习笔记(02核心阶段) 一、6、](.\Java笔记 (02 OOP 异常 常用类).md)



## 七、Java数组

数组是相同类型数据的有序集合。每个数据称一个数组元素，每个元素可通过下标来进行访问。



### 1、数组声明创建与初始化

语法：

> dataType[] arrayRefVar;	// 首选，推荐使用
>
> dataType arrayRefVar[]; 	// 效果相同，但不推荐使用
>
> dataType[] arrayRefVar = new dataType[arraySize];	// 使用new方法创建数组，需要指定数组大小

获取数组长度：arrays.length;

- 静态初始化

  ```java
  int[] a = {1,2,3};
  Man[] mans = {new Man(1,1), new Man(2,2)};
  ```

- 动态初始化

  ```java
  int[] a = new int[10];
  a[0] = 1;
  a[1] = 2;
  ```
  
  没有赋值时，系统会隐式初始化，会赋给默认值。
  
  **数字默认是0，char类型默认u0000，Boolean类型默认false，引用类型默认null**
  
- 以下都是合法的定义数组

  ```java
  int num[][] = new int[1][];
  int []num1[] = new int[1][];
  int[][] num2 = new int[1][];
  int[][] num3 = new int[1][2];
  ```

  前面定义括号可以随意放，但是后面括号必须在类型后面，并且第一个括号必须有初始值。



### 2、内存分析

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/二维数组.png)

**数组也是对象**，数组对象都是保存在堆中的。一个数组的长度是确定的，不可变的。

下标越界则报异常：ArrayIndexOutofBounds



### 3、多维数组

多维数组可以看成数组的数组。比如二维数组就是一个特殊的一维数组，其每一个元素都是一个一维数组。

二维数组：

> int a[\][] = new int[2]\[5];

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/数组内存分析.png)

### 4、Arrays类

- 数组的工具类java.util.Arrays
- Arrays类中的方法都是static修饰的静态方法，在使用的时候可以直接使用类名进行调用，可以不用使用对象来调用。
- 具体有以下常用功能：
  + 给数组赋值：通过 fill 方法
  + 对数组排序：通过 sort 方法，按升序
  + 比较数组：通过 equals 方法比较数组中元素是否相等
  + 查找数组元素：通过 binarySearch 方法能对排序好的数组进行二分查找法操作
  + 打印数组元素：通过 toString 方法



### 5、冒泡排序 <u>面试常遇见</u>

看到**嵌套循环**，应立马得出这个算法的<span style="color:red">时间复杂度为O(n2)</span>

```java
public static int[] sort(int[] array){
    int temp = 0;
    // 如果在还没有循环完，就已经排好序了，就可以利用设置的flag跳出循环，可以节约循环时间(优化)
    boolean flag = false;
    
    // 外层循环，作用是指定循环次数，每一次循环都会将该次循环中的最小的数或者最大的数找出来
    // 一次循环找出一个数
    for(int i = 0; i < array.lenght; i++){
        // 内层循环，作用是每一次循环，对所有元素，进行依此两两比较
        for(int j = 0; j < array.lenght-1; j++){
            if(array[j+1] < array[j]){
                temp = array[j];
                array[j] = array[j+1];
                array[j+1] = temp;
                flag = true;
            }
        }
    }
    
    if(flag == false){
        break;
    }
    
    return array;
}
```



### 6、稀疏数组 <u>重点 压缩数组</u>

当一个数组中大部分都是0，或者为同一值的数组时，可以使用稀疏数组来保存该数组。

稀疏数组就是存入原数组的有效值的数组，对原数组进行有效压缩。

稀疏数组的处理方式：

- 记录数组一共有几行几列，有多少个不同值
- 把具有不同值的元素和行列及值记录在一个小规模的数组中，从而缩小程序的规模

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/稀疏数组.png)

稀疏数组：[0] 存入的是原数组有几行几列几个有效值；从[1]开始存入的是第几行第几列值是多少



### 7、二分查找法(折半查找法) <u>面试常见</u>

```java
public class Test{
    public static void main(Strings[] args){
        int[] a = {45,34,23,12,10,55,76};
        new Test.binarySearch(a,10);
    }
    
    public void binarySearch(int[] a, int b){
        Array.sort(a);
        int low = 0;
        int high = a.length - 1;
        while( low <= high ){
            int mid = (low + high) / 2;
            if( b < a[mid] ){
                high = mid - 1;
            }
            if( b > a[mid] ){
                low = mid + 1;
            }
            if( b == a[mid] ){
                System.out.println("查找成功！");
                break;
            }
        }
    }
}
```

