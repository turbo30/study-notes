# 线程Thread



## 一、线程概念



### 1、并发与并行

**并发**：指两个或多个事件在同段时间内发生。

**并行**：指两个或多个事件在同一时刻同时发生。

**并行比并发效率高。**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/并发与并行.png)

> 注意：
>
> 单核处理器的计算机肯定是不能并行处理多个任务，只能是多个任务在单个CPU上并发运行。同理，线程也是一样的，从宏观角度上理解线程是并行的，但从微观角度上分析却是串行运行的，即一个线程一个线程的去运行，当系统只有一个CPU时，线程会以某种顺序执行多个线程，把这个情况称之为线程调度。



### 2、线程与进程

- 进程：是指一个内存中运行的应用程序，每个进程都有一个独立的内存空间，一个应用程序可以同时运行多个进程；进程也是程序的一次执行过程，是系统运行程序的基本单位；系统运行一个程序即是一个进程从创建、运行到消亡的过程。
- 线程：线程是进程中的一个执行单元，负责当前进程中程序的执行，一个进程中至少有一个线程。一个进程中可以有多个线程，这个应用程序也可以称之为多线程程序。

简而言之：一个程序运行后至少有一个进程，一个进程至少有一个线程。多线程效率高，多个线程之间互不影响。



### 3、线程调度

- **分时调度**

  所有线程轮流使用CPU的使用权，平均分配每个线程占用CPU的时间

- **抢占式调度**

  优先让优先级高的线程使用CPU，如果线程优先级相同，那么会随机选择一个(线程随机性)，Java使用的为抢占式调度。

  + 可在任务管理器中详细信息一栏设置线程的优先级
  + CPU在多个线程中高速切换。优先级高的线程使用CPU的几率就大很多，使用时间也会多很多。如果CPU是多核多线程的，那么比CPU单核单线程的处理线程效率高。



## 二、线程

Thread、Runnable、Callable

**3种创建方式：**

- Thread 类 --- 继承Thread类
- Runnable接口 --- 实现Runnable接口
- Callable接口 --- 实现Callable接口



**注意：**

thread不建议使用，因为OOP单继承的局限性。

推荐使用实现runnable接口的方式，创建线程，避免OOP单继承的局限性、灵活方便、方便一个对象被多个线程调用。



**Java中默认有2个线程**：main线程、GC(垃圾回收)线程



### 1、主线程

在Java中，main方法就是一个主线程。

单线程程序：java程序中只有一个线程执行main方法开始，从上往下依次执行。当该线程中有一处发生异常，那么该线程就会终止，就不会往后继续执行。

JVM执行main方法，main方法会进入栈内存，JVM会找操作系统开辟一条main方法通向CPU的执行路劲，CPU可通过该路径执行main方法，而这个路径有一个名字叫mian(主)线程

```java
public class Test{
    public static void main(String[] args){
        Person p1 = new Person("xi");
        p1.run();
        System.out.println(0/0); // 在这里会抛出ArithmeticException，就会终止程序，后面代码不会执行
        Person p2 = new Person("wa");
        p2.run();
    }
}
```



### 2、多线程原理

当线程对象创建之后，调用start方法，就会启动新的一个线程，JVM就会通知OS给新的线程开辟一条路径，main线程和新的线程同时会抢夺一个CPU，CPU的调度是不能人为干预，都是由CPU随机调取其中一个线程来处理，在两个线程之间来回高速切换。

#### 多线程内存原理

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/多线程内存原理.png)

执行main线程，main方法就会进入到栈内存中，main线程开始执行，第一行代码new了一个线程对象，那么该对象就会存在推内存中；下一行代码调用了该对象的run方法，那么就会将run方法引入到主线程的栈内存中执行；下一行代码调用了start方法，那么就会开辟一个新的栈内存，该栈内存就引入该线程对象里的run方法，在新的栈内存中执行，也就是开启了新的一个线程。CPU就会在多个线程之间进行随机挑选，来回高速切换执行。

==简而言之，只要线程对象调用了start方法，就会开辟新的栈内存空间，执行该线程对象的run方法，栈内存之间的执行互不影响，相互独立。==



#### run()与start() 区别

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/线程.png)

**线程不一定立即执行，由CPU调度安排。**





### 3、Thread类

`java.lang.Thread `类实现了Runnable接口，代表线程，所有的线程对象都必须是Thread类或其子类的实例或者可以让Thread类做代理。每个线程的作用是完成一定的任务，实际上就是执行一段程序流。Java使用线程执行体来代表这段程序流。

#### 构造方法

常用

- `public Thread()` 分配一个新的Thread对象
- `public Thread(String name)` 分配一个指定名字的Thread对象
- `public Thread(Runnable target)` 分配一个带有指定目标新的Thread对象
- `public Thread(Runnable target, String name)`分配一个带有指定目标并为其指定名字的Thread对象

#### 常用方法 yield() join()

- `public String getName()` 获取线程的名称
- `public void setName()` 改变线程的名称
- `public void start()` 导致此线程开始执行; Java虚拟机调用此线程的`run`方法。线程只能启动一次，多次启动非法。
- `public void run()` 此线程要执行的任务代码
- `public static void sleep(long millis) throws InterruptedException ` 使当前正在运行的线程暂时停止执行的毫秒数。报中断异常
- `public static Thread currentThread()` 返回对当前正在执行的线程对象的引用
- `public static void yield()` 线程礼让，退出线程，进入可运行状态，并且线程调度的时候也可能会被cpu选中。yield()方法会通知线程调度器放弃对处理器的占用，并不会释放锁，但调度器可以忽视这个通知。yield()方法主要是为了保障线程间调度的连续性，防止某个线程一直长时间占用cpu资源。但是他的使用应该基于详细的分析和测试。这个方法一般不推荐使用，它主要用于debug和测试程序，用来减少bug以及对于并发程序结构的设计。(可释放cpu资源，不释放锁)
- `public final void join() throws InterruptedException` 强行插队，线程调用这个方法，就会先执行该线程，并该线程死亡了其他线程才能继续执行。



Java通过继承Thread类来**创建**并**启动多线程**的步骤如下：

1. 定义Thread类的子类，并重写`run()`方法，该`run()`方法的方法体就是代表了线程需要完成的任务，因此`run()`方法称为线程执行体；
2. 创建Thread子类的实例，即创建了线程对象；
3. 调用线程对象的`start()`方法来启动该线程。

```java
// 使用继承Thread类，通过实例调用start方法开启一个新的线程
public class ThreadTest extends Thread {

    public ThreadTest() {
    }

    public ThreadTest(String name) {
        super(name); // 将给线程设置的名称传给父类进行设置
    }
    
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println(getName() + i + "次");
        }
    }

    public static void main(String[] args) {
        // ThreadTest threadTest = new ThreadTest("heroC");
        ThreadTest threadTest = new ThreadTest();
        threadTest.setName("heroC"); // 给该线程设置名称
        threadTest.start();
        for (int i = 0; i < 1000; i++) {
            System.out.println("main线程执行" + i + "次");
        }
        System.out.println(Thread.currentThread().getName());// 输出 main；获取当前线程的名称。Thread.currentThread()获取到了当前线程的对象，然后在调用getName()获取该线程的名称
    }
}
```



### 练习：通过继承线程类，开启多线程下载网络图片

该练习，要使用到FileUtils类，该类为io工具类，是apache旗下的。

```java
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.net.URL;

public class DownPricFileUtis extends Thread{
    private String url;
    private String name;

    public DownPricFileUtis(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public void run() {
        Downloader d1 = new Downloader();
        d1.downLoader(url,name);
        System.out.println(name + "已下载完成！");
    }

    public static void main(String[] args) {
        DownPricFileUtis downP1 = new DownPricFileUtis("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1582892937486&di=1f14ad0c117a0695ff8d4701d91a1b9f&imgtype=jpg&src=http%3A%2F%2Fimg3.imgtn.bdimg.com%2Fit%2Fu%3D637719543%2C1600461480%26fm%3D214%26gp%3D0.jpg","1.jpg");
        DownPricFileUtis downP2 = new DownPricFileUtis("https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=1858174317,3137906118&fm=26&gp=0.jpg","2.jpg");
        System.out.println("正在下载，请稍后...");

        // 开启2条线程下载，会自动执行重写的run方法
        downP1.start();
        downP2.start();
    }
}

class Downloader{
    public void downLoader(String url, String name){
        try {
            FileUtils.copyURLToFile(new URL(url), new File(name));
        } catch (IOException e) {
            System.out.println("IO异常，downLoader方法");
        }
    }
}
```



### 4、Runable接口

`java.lang.Runnable` 创建一个线程是声明实现类`Runnable`接口。该接口只有一个`run`方法。

推荐使用Runnable对象，因为Java单继承的局限性

实现线程步骤：

1. 定义Runnable接口的实现类，并重写该接口的run方法；
2. 创建Runnable实现类的实例对象，并以此实例作为Thread的target来创建Thread对象，该Thread才是真正的线程对象；
3. 调用线程的start方法来启动创建一个新的线程。

```java
// 实现Runnable接口
public class Test implements Runnable{

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            // 获取当前线程的引用之后，获取当前线程的名字
            System.out.println(Thread.currentThread().getName() + ": " + i);
        }
    }

    public static void main(String[] args) {
        Test test = new Test();
        // 通过Thread类将Runnable实现类传过去，并给线程命名，并启动
        new Thread(test,"heroC").start();
        for (int i = 0; i < 1000; i++) {
            System.out.println(Thread.currentThread().getName() + ": " + i);
        }
    }
}
```



### 5、Thread类 与 Runnable接口 的区别 面试

Thread类实现了Runnable接口。如果一个类继承Thread，则不适合资源共享。但是如果实现了Runable接口的类，因为不是继承了Thread类，就不是真正的线程代码，则很多类都可以使用该类的代码，达到资源共享的目的。

【总结】

实现Runnable接口比继承Thread类所具有的**优势**：

1. **适合多个相同的程序代码的线程去共享同一个资源；**

   (如果多个程序需要同一个线程的代码，直接用Runnable实现的类去写run方法，然后不同程序将该类引用过去，new一个Thread方法将该类创建成一个线程执行run代码)

2. **可以避免java中的单继承的局限性；**

   (很多时候当你继承了一个父类，你这个类又要使用Thread类，因不能多继承，所以无法实现相应功能，为了解决该问题，就可以使用线程代理Runnable来创建一个线程)

3. **增加程序的健壮性，实现解耦操作，代码可以被多个线程共享，代码和线程独立；**

4. **线程池只能放入实现Runanble或Callable类线程，不能直接放入继承Thread的类。**

> 扩充：
>
> 在java中，每次程序运行至少启动2个线程。一个是main线程，一个是垃圾收集线程。因为每当使用java命令执行一个类的时候，实际上都会启动一个JVM，每个JVM其实是在操作系统中启动了一个进程。



### 6、匿名内部类实现线程创建

匿名内部类，就是没有名字的类。只是把类的方法，提到了new对象的后面来写了，直接使用接口或者父类来创建一个匿名的类。

格式1（使用有参构造方法）避免错，推荐使用格式：

```text
new 父类/接口(构造方法的参数){
	重写方法/实现方法
};
```

格式2（使用无参构造方法）：

```text
new 父类( () - > {
	重写方法/实现方法
});
```

实战：

```java
public class NoNameTest {
    public static void main(String[] args) {
        
        // 创建了一个匿名类，该匿名类继承了Thread类，并重写了run方法，调用了star方法启动线程
        new Thread("Thread"){
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getName() + ": " + i);
                }
            }
        }.start();

        
        // 创建了一个匿名类，实现了Runnable接口，并重写了run方法，调用了start方法启动线程
        /*Runnable runnable = new Runnable(){   // 多态，接口变量与一个接口的实现类
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getName() + ": " + i);
                }
            }
        };*/
        // 可简写成：
        Runnable runnable = () -> {  // 多态，接口变量与一个接口的实现类
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + ": " + i);
            }
        };
        new Thread(runnable,"Runnable").start();

        // Runnable还可以简化：
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getName() + ": " + i);
                }
            }
        },"RunableSimple").start();
    }
}
```



### 7、Callable\<V> 接口

`java.util.concurrent Interface Callable<V>`

只有一个抽象类，`V call() trows 异常`

好处：

1. 可以返回值
2. 可以抛出异常

```java
public class CallableTest implements Callable<Boolean> {
    // 重写call方法
    @Override
    public Boolean call() throws Exception {
        System.out.println(Thread.currentThread().getName()+"   Callable执行的线程...");
        return true;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // 创建线程池
        ExecutorService executorService = Executors.newFixedThreadPool(3);// 不建议这样创建线程池，会出现OOM，具体见JUC

        // 提交线程，创建线程
        Future<Boolean> s1 = executorService.submit(new CallableTest());
        Future<Boolean> s2 = executorService.submit(new CallableTest());
        Future<Boolean> s3 = executorService.submit(new CallableTest());

        // 获取线程返回值
        Boolean sr1 = s1.get();
        Boolean sr2 = s2.get();
        Boolean sr3 = s3.get();

        // 关闭线程池服务
        executorService.shutdownNow();
    }
}
```







## 三、线程安全



### 1、线程安全

有多个线程在同时运行，而这些线程都是相互独立，互不影响的。假如多个线程同时买100张电影票，每个线程运行结果就如同单线程运行的结果一样，每个线程都会去卖这100张票，就会出现线程安全问题，多个线程会出现卖出同一张票的情况以及会出现卖出负数票的情况。

通过卖票案例，演示线程出现安全问题：

```java
public class TicketSalesProblem implements Runnable{
    // 有100张票
    private int tickets = 100;

    /*
    执行买票操作
     */
    @Override
    public void run() {
        while (tickets > 0){
            // 为了提高安全问题的出现率，每次让线程暂停再执行，模拟处理器处理速度满的情况，
            // 这时就会出现大量重复票和负数票
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                System.out.println("中断异常");
            }
            System.out.println( Thread.currentThread().getName() + "已售出第" + tickets-- + "号票");
        }
    }

    public static void main(String[] args) {
        // 一个Runnable实例对象，开启3个线程
        TicketSalesProblem ticketSalesProblem = new TicketSalesProblem();
        new Thread(ticketSalesProblem,"售票窗口A  ").start();
        new Thread(ticketSalesProblem,"售票窗口B  ").start();
        new Thread(ticketSalesProblem,"售票窗口C  ").start();
    }
}

// 该案例输出了大量重复的票和负数票
/*
...
售票窗口A  已售出第44号票
售票窗口B  已售出第44号票
售票窗口C  已售出第43号票
...
售票窗口C  已售出第2号票
售票窗口C  已售出第1号票
售票窗口B  已售出第0号票
售票窗口A  已售出第-1号票
...
*/
```

**出现0号票和负数票的原因：**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/线程安全卖票案例.png)

**出现重复票的原因：**

在执行过程中，该电脑是4核的，可并行执行线程，所以难免两个线程会同时执行输出语句，这时候tickets的值都是一样的，还没有发生变化，所以在这一时刻，会卖出同一个号码的票。	



**注意：**

线程安全问题是不能产生的，解决方案是，当线程在访问共享资源的时候，无论该线程是否失去了CPU执行权，其他线程都必须等待，等待当前线程完成所有操作，其他线程才可以操作。只要有一个进程在操作该共享资源，其他线程都必须等待。保证，始终只有一个线程在执行共享资源代码就可。



### 2、线程同步 synchronized

当我们使用多个线程访问同一资源时候，且多个线程中对资源有写的操作或更改的操作，就容易出现线程安全问题。要解决多线程并发访问一个资源的安全性问题，Java中提供了同步(synchronized)机制来解决。

> Synchronized实现原理
>
> Synchronized很多人称之为“重量级锁”，但是在JAVA SE6之后对Synchronized进行了优化，在某些情况下已经没有那么重了。且需要借助于操作系统mutex Lock的帮助，完成同步操作。当一个线程挂起和唤醒的时候，需要借助操作系统来完成，从用户态转变为内核态，并且线程之间的上下文切换会消耗一定的时间，所以效率比较低。Synchronized用在静态方法上面，锁对象是.class；Synchronized用在动态方法上面锁对象是this；Synchronized用于代码块，称之为同步代码块，锁对象就是指定的对象。确保锁对象唯一即可。
>
> synchronized关键字**并非一开始就该对象加上重量级锁**，也是从偏向锁，轻量级锁，再到重量级锁的过程。
> 这个过程也告诉我们，假如我们一开始就知道某个同步代码块的竞争很激烈、很慢的话，那么我们一开始就应该使用重量级锁了，从而省掉一些锁转换的开销。
>
> **Synchronized执行过程：**
>
> 1. 检测Mark Word里面是不是当前线程的ID，如果是，表示当前线程处于偏向锁
> 2. 如果不是，则使用CAS将当前线程的ID替换Mard Word，如果成功则表示当前线程获得偏向锁，置偏向标志位1
> 3. 如果失败，则说明发生竞争，撤销偏向锁，进而升级为轻量级锁。
> 4. 当前线程使用CAS将对象头的Mark Word替换为锁记录指针，如果成功，当前线程获得锁
> 5. 如果失败，表示其他线程竞争锁，当前线程便尝试使用自旋来获取锁。
> 6.  如果自旋成功则依然处于轻量级状态。
> 7. 如果自旋失败，则升级为重量级锁。
>
> **偏向锁，轻量级锁，重量级锁：**
>
> - 偏向锁：无实际竞争，且将来只有第一个申请锁的线程会使用锁。
> - 轻量级锁：无实际竞争，多个线程交替使用锁；允许短时间的锁竞争。
> - 重量级锁：有实际竞争，且锁竞争时间长。



根据卖票案例简述：

```
窗口A线程进入操作的时候，窗口B窗口C只能在外面等待，窗口A操作结束，窗口A窗口B窗口C才有机会抢夺CPU去执行代码。也就是说某一个进程修改共享资源的时候，其他线程不能修改该资源，等待修改完毕同步之后，才能去抢夺CPU资源，完成对应的操作，保证了数据的同步性，解决了线程不安全的现象。
```

为了保证每个线程都能正常执行原子操作，Java引入了线程同步机制。

有3种方式完成同操作：

1. 同步代码块
2. 同步方法
3. 锁机制



#### 1）同步代码块

使用关键字`synchronized`的代码块，表示只对这个区块的资源实行互斥访问。

**格式：**

```java
synchronized(锁对象){
	可能会出现线程安全问题的代码
}
```

**同步锁：**

对象的同步锁只是一个概念，可以想象为在对象上标记了一个锁。

1. 锁对象 可以是任意类型
2. 多个线程对象 要使用同一把锁

> 注意：
>
> 在任何时候，最多允许一个线程有同步锁，谁拿到这个锁就可以进入代码块，其他线程只能在外面等着(BLOCKED)。

**缺点：**

​		程序频繁的判断锁，获取锁，释放锁，程序的效率会降低。



**使用同步代码块解决线程安全：**

```java
public class SolveTicketSalesProblem implements Runnable{
    // 有100张票
    private int tickets = 100;
    // 创建一个锁对象
    Object object = new Object();
    /*
    执行买票操作
     */
    @Override
    public void run() {
        while (true){
            // 同步代码块
            synchronized (object){
                // 为了提高安全问题的出现率，每次让线程暂停再执行，模拟处理器处理速度满的情况
                // 因为有同步代码块，锁对线程的标记，所以不会出现线程安全问题
                // 一定要在锁里面判断票是否还有
                if(tickets > 0){
                    try {
                        TimeUnit.MILLISECONDS.sleep(500);
                    } catch (InterruptedException e) {
                        System.out.println("中断异常");
                    }
                    System.out.println( Thread.currentThread().getName() + "已售出第" + tickets-- + "号票");
                }
            }
        }
    }

    public static void main(String[] args) {
        SolveTicketSalesProblem salesProblem = new SolveTicketSalesProblem();
        new Thread(salesProblem,"售票窗口A").start();
        new Thread(salesProblem,"售票窗口B").start();
        new Thread(salesProblem,"售票窗口C").start();
    }
}
```



##### 同步代码块的原理

多个线程执行一个共享资源，当某个线程抢到了CPU执行权就会进入run方法开始执行，当执行到synchronized同步代码块时，就会检查有没有锁对象，如果有锁对象，就会携带者锁对象进入到同步代码块进行执行，如果没有锁对象，那么就会在同步代码块外便阻塞等待，直到其他线程释放了锁对象，该线程拿到锁对象为止。所以，同步代码块中，永远只能一个线程在执行。

总结：同步中的线程，没有执行完不会释放锁对象，同步外的线程没有锁对象进不去同步。



#### 2）同步方法

使用关键字`synchronized`修饰的方法，叫做同步方法，保证A线程执行该方法的时候，其他线程都在方法外等着。

**格式：**

```java
public synchronized void method(){
    可能会产生线程安全的代码
}
```

> 同步方法的同步锁是谁？
>
> 如果是static修饰的方法，使用的是当前方法所在类的字节码对象(类.class)
>
> 如果非static修饰的方法，同步锁就是使用了this对象(多个线程都是操作的一个对象，所以this对象也是唯一的)



**使用同步方法代码如下：**

```java
public class SolveTicketSalesProblem2 implements Runnable {
    // private int tickets = 100;
    private static int tickets = 100;

    @Override
    public void run() {
        while (true){
            // buyTickets();
            buyTicketsStatic();
        }
    }

    // 非静态方法 同步锁就是给该类实例化的对象, this
    // 有锁对象的线程才能执行该同步方法，否则在该同步方法外等待
    /*public synchronized void buyTickets(){
        if(tickets > 0){
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                System.out.println("中断异常");
            }
            System.out.println( Thread.currentThread().getName() + "已售出第" + tickets-- + "号票");
        }
    }*/

    // 静态方法 同步锁是本类的class属性，就是class文件
    // 因为静态方法创建与类同步的，早于实例之前，所以使用class文件对象作为锁对象
    public static synchronized void buyTicketsStatic(){
        if(tickets > 0){
            try {
               TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                System.out.println("中断异常");
            }
            System.out.println( Thread.currentThread().getName() + "已售出第" + tickets-- + "号票");
        }
    }

    public static void main(String[] args) {
        SolveTicketSalesProblem2 salesProblem2 = new SolveTicketSalesProblem2();
        new Thread(salesProblem2,"售票窗口A").start();
        new Thread(salesProblem2,"售票窗口B").start();
        new Thread(salesProblem2,"售票窗口C").start();
    }
}
```



#### 3）Lock 接口 JDK 1.5

`java.util.concurrent.locks interface Lock` 机制提供了比`synchronized`代码块和`synchronized`方法更为广泛的锁定操作，同步代码块/同步方法具有的功能Lock都有，除此之外更强大，更体现面向对象。

Lock锁也称之为同步锁。加锁和释放锁被方法化了，如下：

- `public void lock()` 获得同步锁
- `public void unlock()` 释放同步锁

`java.util.concurrent.locks.ReentrantLock ` 类实现了Lock接口。可通过该类使用lock方法和unlock方法。

**使用如下：**

```java
public class SolveTicketSalesProblem3 implements Runnable{
    private int tickets = 100;

    Lock lock = new ReentrantLock(); // 创建一个锁对象（多态）

    @Override
    public void run() {
        while (true){
            lock.lock(); // 获取同步锁
            if(tickets > 0){
                try {
                    TimeUnit.MILLISECONDS.sleep(500);
                    System.out.println( Thread.currentThread().getName() + "已售出第" + tickets-- + "号票");
                } catch (InterruptedException e) {
                    System.out.println("中断异常");
                }finally {
                    lock.unlock(); // 无论代码是否发生异常，都会执行finally，将锁释放
                }
            }
        }
    }

    public static void main(String[] args) {
        SolveTicketSalesProblem2 salesProblem2 = new SolveTicketSalesProblem2();
        new Thread(salesProblem2,"售票窗口A").start();
        new Thread(salesProblem2,"售票窗口B").start();
        new Thread(salesProblem2,"售票窗口C").start();
    }
}
```



## 四、进程状态

进程的5种状态：创建态、就绪态、运行态、阻塞态、终止态

**创建态**完成创建进程的一系列工作进入**就绪态。**

**就绪态**除处理机外的其他条件都已具备，等待进程被**调度**则可进入**运行态，**若**时间片已到**或**处理机被抢占**进程将返回**就绪态。**

**运行态**中的进程用“系统调用”的方式申请系统某种资源，或等待某个事件发生时，进程进入**阻塞态**。该过程是进程***主动行为***。（运行态----->阻塞态具有**单向性**）

**阻塞态**中的进程若其申请的资源得到分配，或等待事件已发生，可重新进入**就绪态**(阻塞态------>就绪态具有**单向性**)

**运行态**中的进程若***运行结束***或运行中**遇到不可修复的错误**则进入**终止态**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/进程状态.png)





## 五、线程状态



### 1、线程状态(生命周期)概述

当线程被创建并启动后，它既不是一启动就进入了执行状态，也不是一直处于执行状态。在线程生命周期中，在API`java.lang.Thread.State`这个枚举中给出了6种线程状态 (始于JDK1.5)：

| 线程状态                | 线程状态发生条件                                             |
| ----------------------- | ------------------------------------------------------------ |
| New(新建)               | 尚未启动的线程处于此状态。还未调用start方法                  |
| Runnable(可运行)        | 在Java虚拟机中执行的线程处于此状态，可能正在运行自己的代码，也可能没有，这取决于操作系统处理器。 |
| Blocked(锁阻塞)         | 当一个线程视图获取一个对象锁，而该对象锁被其他线程拥有，这时就会进入阻塞状态；当该线程持有锁时，就会进入Runnable状态。 |
| Waiting(无限等待)       | 一个线程在等待另一个线程执行一个(唤醒)特定动作时，该线程进入waiting状态，通过锁对象调用Object.wait()。进入这个状态后是不能自动唤醒，必须等待另一个线程通过锁对象调用notify或者notifyAll方法才能被唤醒。 |
| Timed Waiting(计时等待) | 同waiting状态，有几个方法有超时参数，调用他们将进入TimedWaiting状态。这个状态将一直保持到超时期满或者接收到唤醒通知。带有超时参数的常用方法有**Thread.sleep(long timeout)、Object.wait(long timeout)** |
| Terminated(被终止)      | 因为run方法正常退出而死亡，或者因为没有捕获异常终止了run方法而死亡。 |

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/线程状态.png)

### 2、Timed Waiting(计时等待)

Timed Waiting状态在线程安全的卖票案例中就已经使用了，待用sleep方法，就时让线程进入计时等待状态，在该状态下就是让线程休眠，不执行任何代码，以”减慢线程“。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/计时等待.png)

**实现一个计时器，计数100，在每个数字之间停1秒，每隔10个数字输出一个字符串。**

```java
public class TimedWaiting implements Runnable {
    @Override
    public void run() {
        for (int i = 1; i <= 100; i++) {
            try {
                System.out.println(i);
                if(i % 10 == 0){
                    System.out.println("--------");
                }
                Thread.sleep(1000); // 进入休眠状态，Timed Waiting
            } catch (InterruptedException e) {
                System.out.println("中断异常");
            }
        }
    }

    public static void main(String[] args) {
        new Thread(new TimedWaiting()).start();
    }
}
```



### 3、BLOCKED (阻塞状态)

当多个线程抢夺cpu执行权，当一个线程抢到了cpu执行权，执行run方法，run方法种有同步锁，当该同步锁被其他线程夺走了，那么该线程就不能执行同步代码，所以就会进入blocked阻塞状态。

另外waiting状态和timedwaiting状态被唤醒之后，也不能拿到锁对象，因此也会进入blocked阻塞状态。

直到拿到锁对象，才会进入runnable状态去执行同步代码。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/阻塞状态.png)



### 4、Waiting (无限等待状态)

一个线程在等待另一个线程执行一个(唤醒)特定动作时，该线程进入waiting状态，通过Object调用wait方法进入waiting状态。进入这个状态后是不能自动唤醒，必须等待另一个线程Object调用notify或者notifyAll方法才能被唤醒。也就时线程之间通信，告知你该醒了。

#### 案例 顾客与老板包子

实现一个顾客需要买包子，告知老板我要卖包子，顾客进入等待状态，老板花5秒时间去做包子，做好了之后告知顾客，顾客得到告知，开始吃包子。

**代码实现：**

```java
// 为什么这里顾客线程永远先拿到锁？
// 因为顾客线程先执行，先start()，所以会先拿到锁去执行自己的类中的run方法
public class WaitingTest {
    public static void main(String[] args) {
        Object obj = new Object(); // 创建一个锁对象，两个线程通过同一个锁对象进行通信交流

        // 创建一个匿名内部类，顾客
        new Thread(){
            @Override
            public void run() {
                synchronized (obj){
                    try {
                        System.out.println("老板，我要买包子。");
                        
                        // 进入无限等待状态，这时cpu执行权就让出去了，并将锁对象释放，必须等待被唤醒，才可以继续执行
                        obj.wait(); 
                        
                        // 顾客线程接收到了老板的通知，被唤醒
                        System.out.println("谢谢老板，你的包子真好吃！"); 
                    } catch (InterruptedException e) {
                        System.out.println("wait()中断异常");;
                    }
                }
            }
        }.start();

        // 创建一个匿名内部类，老板
        new Thread(){
            @Override
            public void run() {
                synchronized (obj){
                    try {
                        // 拿到锁对象，获得cpu执行权，线程进入休眠状态，这过程中不会释放锁对象，花5秒时间做包子，5秒后自动唤醒
                        sleep(5000); 
                        System.out.println("顾客，你的包子做好了。");
                        obj.notify(); // 5秒后，告知顾客线程，你的包子好了，不会释放锁
                    } catch (InterruptedException e) {
                        System.out.println("sleep()中断异常");
                    }
                }
            }
        }.start();
    }
}
```



### 5、Object类中的方法

#### wait(long timeout)与sleep(long timeout)区别

**都来自不同的类**

wait ==》 Object类中

sleep ==》 Thread类中

- `public final void wait(long timeout) throws InterruptedException` Object锁对象调用该方法。导致当前线程等待，直到另一个线程调用此对象的`notify()`方法或`notifyAll()`方法，或指定的时间已过则会自动唤醒。进入Timed Waiting状态。该方法必须在**同步代码块中使用**。**会释放锁。**
- `public void sleep(long timeout) throws InterruptedException` 线程调用该方法 ，进入Timed Waiting状态，时间一过会自动唤醒线程。**任何地方使用**。**不会释放锁。**



#### wait()和wait(long timeout)区别

- wait()方法会释放资源释放锁，进入Waiting状态
- wait(long timeout)方法会释放资源释放锁，进入TimedWaiting状态



#### sleep(long timeout)

- sleep(long timeout)方法会释放锁，会释放资源，进入timedWaiting状态，超过时间会自动唤醒。也可以通过Thread.interrupt()方法唤醒。

- ```java
  Thread thread = new Thread(() -> {
      System.out.println(".........");
      try {
          Thread.sleep(4000);
      } catch (InterruptedException e) {
          System.out.println("中断");
      }
      System.out.println(",,,,,,,,,,");
  });
  thread.start();
  thread.interrupt();
  
  // 会立马输出
  .........
  中断
  ,,,,,,,,,,
  // 不使用interrupt()
  先输出.........
  过4s再输出,,,,,,,,,,
  ```

  

#### notify()和notifyAll()区别

会让进入waiting状态的线程唤醒过来，进入到blocked阻塞状态，等待线程执行完代码之后，释放锁，才会有机会去抢到锁而执行。因此**notify和notifyAll是不会立刻释放锁**的，只是通知线程你被唤醒，你可以抢夺cpu执行权。只有代码执行完了，该线程才会释放锁。

- `public final void notify()` 会随机唤醒等待中的一个线程，先唤醒等待时间久的。

- `public final void notifyAll()` 唤醒所有等待Waiting中的线程。



## 六、线程等待唤醒机制



### 1、线程之间通信

线程通信也就是线程之间的合作。就如同顾客与老板的案例。

**为什么要处理线程间通信：**

多个线程是并发执行的，在默认情况下CPU是随机切换线程的，当我们需要多个线程共同完成一件任务时，并且有规律的去执行任务，那么多线程之间就必须得有一些协调通信。

通过等待唤醒机制保证线程间通信有效利用资源。



### 2、等待唤醒机制

等待唤醒机制是多个线程的一种协作机制。就是一个线程进行了规定操作之后，就进入了等待状态(**wait()**)，等待其他线程执行完他们的指定代码过后，再将其唤醒(**notify()**)；在有多个线程进行等待时，如果需要，可以使用notifyAll()来唤醒所有等待的线程。 

**等待唤醒中的方法**

等待唤醒机制就是用于解决线程通信的问题，使用到以下3个方法：

1. wait：线程不再活动，不再参与调度，进入wait set(等待集合)中，释放锁对象，因此不会浪费CPU资源，也不会去竞争锁，这时的线程状态就是WAITING。他还要的等待别的线程执行一个特别的动作，也就是“通知notify”在这个对象上等待的线程从wait set中释放出来，重新进入到调度队列中。
2. notify：则选取所通知对象的wait set中的一个线程释放。先唤醒等待时间久的。
3. notifyAll：则释放所有通知对象的wait set上的全部线程。

> 注意：
>
> 哪怕只通知了一个等待的线程，被通知线程也不能立即恢复执行，因为它当初中断的地方是在同步块内，而此刻锁对象已经被其他线程占有，所以它需要再次尝试获取锁对象（很可能面临其他线程的竞争），成功之后才能开始恢复执行当初调用wait方法之后的代码。
>
> 总结：
>
> - 如果能获取锁，线程就能从WAITING状态变为RUNNABLE状态
> - 如果不能获取锁，从wait set出来，进入 entry set(进入集合) ，线程从WAITITNG状态又变成BLOCKED状态



**notify和wait方法的使用注意事项：**

1. notify和wait方法都必须使用同一个锁对象，因为notify只能唤醒同一个锁对象的线程。
2. notify和wait方法都必须在同步代码块或同步方法中调用，可以保证锁对象唯一。
3. notify和wait方法必须是Object类中的方法。因此所有的类对象都可以成为锁对象。



### 练习：生产者与消费者问题

等待唤醒机制其实就是经典的“生产者与消费者”的问题。

```
包子铺线程生产包子，吃货线程消费包子。当包子没有时，吃货线程等待，包子铺线程生产包子，并通知吃货线程，因为已经有包子了，那么包子铺线程进入等待状态。接下来，吃货线程能否进一步执行取决于锁的获取情况。如果吃货获得锁，那么就执行吃包子的动作，包子吃完了，并通知包子铺线程，吃货线程进入等待，包子铺线程能否进一步执行则取决于锁的获取情况。
```

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/生产者与消费者等待唤醒机制.png)



**Baozi.java**

```java
public class Baozi {
    private String pi; // 包子皮
    private String xian; // 包子馅
    private boolean zhuangTai = false; // 判断是否有包子,初始化没有包子

    public String getPi() {
        return pi;
    }

    public void setPi(String pi) {
        this.pi = pi;
    }

    public String getXian() {
        return xian;
    }

    public void setXian(String xian) {
        this.xian = xian;
    }

    public boolean isZhuangTai() {
        return zhuangTai;
    }

    public void setZhuangTai(boolean zhuangTai) {
        this.zhuangTai = zhuangTai;
    }
}
```

**Shengchan.java**

```java
public class Shengchan implements Runnable{
    // 创建一个锁对象
    private Baozi baozi;

    public Shengchan(Baozi baozi) {
        this.baozi = baozi;
    }

    @Override
    public void run() {
        int count = 0; // 定义一个变量，用于生产不同的包子
        while (true){
            synchronized (baozi){
                if(baozi.isZhuangTai()){ // 如果有包子，就等待
                    try {
                        baozi.wait();
                    } catch (InterruptedException e) {
                        System.out.println("生产wait()有异常！");
                    }
                }
                // 如果没有包子就生产包子
                if(count % 2 == 0){
                    baozi.setPi("薄皮");
                    baozi.setXian("三鲜馅");
                }else {
                    baozi.setPi("厚皮");
                    baozi.setXian("大葱牛肉馅");
                }
                count++;

                System.out.println("请稍等，正在为你生产包子...");
                try {
                    Thread.sleep(5000); // 让线程等待5秒钟，假设用5秒在生产包子
                } catch (InterruptedException e) {
                    System.out.println("生产sleep()有异常！");
                }
                System.out.println("你的"+ baozi.getPi() + baozi.getXian() + "包子，已为你生产完成，请享用！");

                // 将包子的状态设置为有包子，并通知等待的线程
                baozi.setZhuangTai(true);
                baozi.notify();

            }
        }
    }
}
```

**Xiaofei.java**

```java
public class Xiaofei implements Runnable {
    // 创建一个锁对象
    private Baozi baozi;

    public Xiaofei(Baozi baozi) {
        this.baozi = baozi;
    }

    @Override
    public void run() {
        while (true){
            synchronized (baozi){
                if(baozi.isZhuangTai()){ // 如果为true就是有包子，就吃包子
                    System.out.println("正在吃" + baozi.getPi() + baozi.getXian() + "包子...");
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        System.out.println("消费sleep()有异常！");
                    }

                    baozi.setZhuangTai(false); // 将包子设为false表示没有包子了
                    System.out.println("包子已吃完，老板我还要包子。");
                    System.out.println("-------------------------------------");
                    try {
                        baozi.notify(); // 通知等待的生产线程，你要准备生产把包子了
                        baozi.wait();
                    } catch (InterruptedException e) {
                        System.out.println("消费wait()有异常！");
                    }
                }else {
                    baozi.notify(); // 通知等待的生产线程，你要准备生产把包子了
                }
            }
        }
    }
}
```

**Test.java**

```java
public class Test {
    public static void main(String[] args) {
        Baozi baozi = new Baozi(); // 创建一个包子类
        // 将包子类给两个线程，保证了锁对象一致，两个线程也可以对同一资源(同包子实例)进行处理
        new Thread(new Shengchan(baozi)).start(); 
        new Thread(new Xiaofei(baozi)).start();
    }
}
```



## 七、线程正确停止

方法：

1. 利用次数，不建议死循环
2. **建议使用标志位，如建立flag通过判断true，false**
3. 不建议使用JDK的stop方法，interrupt方法，destroy放等一些过时的方法

```java
public class CallableTest implements Runnable {
    private boolean flag = true;

    @Override
    public void run() {
        while (flag){
            System.out.println("线程run");
        }
    }

    public void stop(){
        flag = false;
    }

    public static void main(String[] args) {
        CallableTest callableTest = new CallableTest();
        new Thread(callableTest).start();
        for (int i = 0; i < 10000; i++) {
            if(i == 5000){
                callableTest.stop();
                System.out.println("线程被停止了");
            }
            System.out.println(i);
        }
    }
}
```



## 八、线程优先级

线程优先级用数字表示，**范围1~10**

> 注意：
>
> 不是优先级高的就能够被优先执行，执行结果还是的看cpu的调度。
>
> 优先级高了其实就是增加了权重，被执行的几率就大了很多。

**Thread.MAX_PRIORITY = 10;**

**Thread.MIN_PRIORITY = 1;**

**Thread.NORM_PRIORITY = 5;**

线程调用方法获取和设置优先级：

- `getPriority()`
- `setPriority(int num)`

超过最大值、最小值会报异常



## 九、守护(daemon)线程

- 线程分为**守护线程(后端线程)**和**用户线程(前端线程)**

- 虚拟机必须确保用户线程执行完毕
- 虚拟机不用等待守护线程执行完毕，如：后台记录日志，监控内存，垃圾回收(GC)等

**用户线程执行完了，程序就结束了。不用管守护线程。**

**自己创建的正常线程，main线程都是用户线程。**



调用方法设置守护线程：

- `setDaemon(boolean flag)`

传值为true，则设置为守护线程。默认为false。



## 十、ThreadLocal\<T> 类 JDK 1.2

`java.lang.ThreadLocal<T>` ThreadLocal类是为共享变量在每一个线程中创建一个副本，每个线程可以访问自己内部的副本变量。创建一个ThreadLocal类一定是私有的、静态的。**ThreadLocal使用场合主要解决多线程中数据数据因并发产生不一致问题。**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/ThreadLocalMap.png)

#### 常用方法

- `public T get()`

  获取副本中的value

- `public void set(T value)`

  设置副本中的value



### 1、在Spring中的应用场景

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/spring事务.png)

在Spring中的事务操作就会使用到ThreadLocal类，因为在事务操作过程中，会连接数据库很多次，那么连接数据库就会去连接池获取一个连接对象，为了防止每次获得的连接对象不一样，就使用ThreadLocal类将第一次获取连接对象的时候就将连接对象放入到线程自己的副本中，当这个整个事务操作过程中，就可以保证连接对象一致，保证了对数据库操作的安全性。



### 2、ThreadLocal运用

目的：让所有线程都对共享变量num进行+5操作，每个线程输出的结果都为5。

**不使用ThreadLocal类的线程不安全：**

```java
public class Test{
    private int num = 0;

    public void show(){
        Thread[] threads = new Thread[5];
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(()-> {
                num+=5;
                System.out.println(Thread.currentThread().getName() + " ===> " + num);
            });
        }
        for (int i = 0; i < threads.length; i++) {
            threads[i].start();
        }
    }

    public static void main(String[] args) {
        Test t = new Test();
        t.show();
    }

}
```

输出：

```
Thread-0 ===> 5
Thread-1 ===> 10
Thread-2 ===> 15
Thread-4 ===> 20
Thread-3 ===> 25
```

由于，所有线程都在使用共享变量num，又每一个线程都会对num变量进行+5操作，最后输出的结果值就会出现问题，就会出现线程不安全的问题。



**加入ThreadLocal类，实现共享变量成为线程私有的副本：**

```java
public class Test{
    // 通过ThreadLocal去封装一个共享变量
    private static ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>(){
        @Override
        protected Integer initialValue() {
            return 0;
        }
    };

    public void show(){
        Thread[] threads = new Thread[5];
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(()-> {
                // 获取当前线程的threadLocal中的值
                Integer num = threadLocal.get();
                num += 5;
                // 设置当前线程的threadLocal中的值
                threadLocal.set(num);
                System.out.println(Thread.currentThread().getName() + " ===> " + threadLocal.get());
            });
        }
        for (int i = 0; i < threads.length; i++) {
            threads[i].start();
        }
    }

    public static void main(String[] args) {
        Test t = new Test();
        t.show();
    }

}
```

输出结果：

```
Thread-0 ===> 5
Thread-2 ===> 5
Thread-1 ===> 5
Thread-3 ===> 5
Thread-4 ===> 5
```



### 3、源码分析

**Thread.java**

```java
/* ThreadLocal values pertaining to this thread. This map is maintained
* by the ThreadLocal class. */
// 定义了一个ThreadLocal中内部类ThreadLocalMap类型的变量
ThreadLocal.ThreadLocalMap threadLocals = null; 
```

**ThreadLocal.java**

```java
 static class ThreadLocalMap {
     /**
     * The entries in this hash map extend WeakReference, using
     * its main ref field as the key (which is always a
     * ThreadLocal object).  Note that null keys (i.e. entry.get()
     * == null) mean that the key is no longer referenced, so the
     * entry can be expunged from table.  Such entries are referred to
     * as "stale entries" in the code that follows.
     */
     static class Entry extends WeakReference<ThreadLocal<?>> {
         /** The value associated with this ThreadLocal. */
         Object value;

         Entry(ThreadLocal<?> k, Object v) { // Entry内部类
             super(k);
             value = v;
         }
     }
     
     private static final int INITIAL_CAPACITY = 16; 
     private Entry[] table;
     private int size = 0;
     private int threshold; // Default to 0
     private void setThreshold(int len) {
         threshold = len * 2 / 3;
     }
     private static int nextIndex(int i, int len) {
         return ((i + 1 < len) ? i + 1 : 0);
     }
     private static int prevIndex(int i, int len) {
         return ((i - 1 >= 0) ? i - 1 : len - 1);
     }
        /**
         * Construct a new map initially containing (firstKey, firstValue).
         * ThreadLocalMaps are constructed lazily, so we only create
         * one when we have at least one entry to put in it.
         */
     //ThreadLocalMap构造函数
     ThreadLocalMap(ThreadLocal<?> firstKey, Object firstValue) { 
         table = new Entry[INITIAL_CAPACITY]; // 初始化entry数组的大小 初始大小为16
         int i = firstKey.threadLocalHashCode & (INITIAL_CAPACITY - 1);
         table[i] = new Entry(firstKey, firstValue); // 经过hash算法，找到一个坑，将内容存到对应的数组的位置
         size = 1;
         setThreshold(INITIAL_CAPACITY);
     }
 }
```

可见ThreadLocal中的内部类ThreadLocalMap类中又有一个内部类Entry类。该Entry类的构造函数，需要传入ThreadLocal实例和value值，两个参数。



```java
// 创建一个ThreadLocal实例
private static ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>()
```

- 通过实例调用get()方法 ----> ThreadLocal.java

  ```java
  public T get() {
      Thread t = Thread.currentThread(); // 获取调用该方法的当前线程
      ThreadLocalMap map = getMap(t); // 通过当前线程去获取当前线程实例中ThreadLocalMap对象
      if (map != null) { // 如果map不为空
          // 就获取ThreadLocalMap对象中的Entry数组中存储的this为key的entry对象
          // 这里的this对象为调用该方法的ThreadLocal对象
          ThreadLocalMap.Entry e = map.getEntry(this); 
          if (e != null) { // 如果该entry对象不为空，就说明有这个对象的存在
              @SuppressWarnings("unchecked")
              T result = (T)e.value; // 获取entry对象中的值
              return result; // 返回这个值
          }
      }
      // 如果map对象为空，就说明该线程还没有创建ThreadLocalMap对象，该线程变量threadLocals = null
      // 就需要通过setInitialValue()，获取一个map对象并且创建一个新的entry
      return setInitialValue();
  }
  
  
  // getMap()
  ThreadLocalMap getMap(Thread t) {
      return t.threadLocals;
  }
  
  
  // setInitialValue()
  private T setInitialValue() {
      // 调用ThreadLocal类中的initialValue方法，获取默认的初始值null，重写该方法，可以将共享变量设置成初始值，设置成线程私有的副本变量
      T value = initialValue(); 
      Thread t = Thread.currentThread(); // 获取当前线程对象
      ThreadLocalMap map = getMap(t);
      if (map != null)
          map.set(this, value);
      else
          createMap(t, value); // 如果map依旧为空，就创建一个ThreadLocalMap对象
      return value; // 将默认值返回出去
  }
  
  
  // createMap
  void createMap(Thread t, T firstValue) {
      t.threadLocals = new ThreadLocalMap(this, firstValue); 
      // 给当前线程创建一个ThreadLocalMap对象，并初始化Entry数组，然后将键值对插入到Entry数组中
  }
  ```

- 通过实例调用set()方法 ----> ThreadLocal.java

  ```java
  public void set(T value) {
      Thread t = Thread.currentThread(); // 获取当前线程对象
      ThreadLocalMap map = getMap(t); // 获取当前线程实例中的ThreadLocalMap对象
      if (map != null)
          map.set(this, value);
      else
          createMap(t, value);
  }
  
  
  // set
  private void set(ThreadLocal<?> key, Object value) {
  
      // We don't use a fast path as with get() because it is at
      // least as common to use set() to create new entries as
      // it is to replace existing ones, in which case, a fast
      // path would fail more often than not.
  
      Entry[] tab = table; // table是内部类ThreadLocalMap中定义的entry数组
      int len = tab.length;
      int i = key.threadLocalHashCode & (len-1); // threadLocal对象经过hash算法，找到数组中的位置
  
      // 该方法遍历entry数组，如果存在key值为threadLocal对象，就覆盖值
      for (Entry e = tab[i]; e != null; e = tab[i = nextIndex(i, len)]) { 
          
          ThreadLocal<?> k = e.get();
  
          if (k == key) { // 
              e.value = value;
              return;
          }
  
          if (k == null) {
              replaceStaleEntry(key, value, i);
              return;
          }
      }
  
      tab[i] = new Entry(key, value);
      int sz = ++size;
      if (!cleanSomeSlots(i, sz) && sz >= threshold)
          rehash();
  }
  ```



### 4、总结

以上源码可以分析出，每一个线程都有一个ThreadLocalMap变量，而ThreadLocalMap是在ThreadLocal类中，而ThreadLocalMap有一个内部类Entry，用于存储ThreadLocal对象和value值的。在ThreadLocalMap中，定义了Entry数组，初始大小为16，可以扩容。该Entry数组中可以存放很多个以ThreadLocal对象为key和共享变量为value的Entry对象。所以，创建了一个ThreadLocal实列，在对应线程中get和set值是针对与该线程操作的，线程之间都是互相隔离，不打扰的。每一个线程都有自己的线程副本。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/Thread使用线程副本的内存分析.png)

**重点来了，突然我们ThreadLocal是null了，也就是要被垃圾回收器回收了，但是此时我们的ThreadLocalMap生命周期和Thread的一样，它不会回收，这时候就出现了一个现象。那就是ThreadLocalMap的key没了，但是value还在，这就造成了内存泄漏。**（因为ThreadLocal中的key是弱引用，而value是强引用。当ThreadLocal没有被强引用时，在进行垃圾回收时，key会被清理掉，而value 不会被清理掉，这时如果不做任何处理，value将永远不会被回收，产生内存泄漏。）

**解决办法：使用完ThreadLocal后，执行remove操作，避免出现内存溢出情况。**



​	

ThreadLocal中有ThreadLocalMap，ThreadLocalMap中Entry内部类

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/ThreadLocal方法.png" style="float:left" />