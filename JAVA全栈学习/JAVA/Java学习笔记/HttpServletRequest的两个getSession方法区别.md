# HttpServletRequest

java的API

`javax.servlet.http.HttpServletRequest`

HttpServletRequest的两个getSession方法区别.md

## getSession方法

- getSession()  `HttpSession getSession()`

  返回与此请求关联的当前HttpSession，如果该请求没有会话，则创建一个会话(新的HttpSession)。

- getSession(boolean create)

  返回与此请求关联的当前 HttpSession，如果没有当前会话并且 create 为 true，则返回一个新会话(新的HttpSession)。 如果 create 为 false 并且该请求没有有效的 HttpSession，则此方法返回 null。



总结：

getSession() 结果与 getSession(true) 相同