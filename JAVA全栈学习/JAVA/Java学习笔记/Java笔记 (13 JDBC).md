# JDBC

JDBC (Java DataBase Connectivity) Java数据库连接



## 一、什么是JDBC ？

JDBC (Java DataBase Connectivity) Java数据库连接。

JDBC是Java官方(SUN公司)定义的实现java与关系型数据库连接的规则即接口。各个数据库公司，分别使用了java去实现了该统一的接口，并生成了驱动jar包。导入相应jar包之后，以方便程序员使用JDBC通过jar包去操作数据库。



## 二、JDBC使用

### 1、导入驱动jar包

```xml
<!-- 通过maven导入mysql的jar包 -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.15</version>
</dependency>
```



### 2、IDEA连接数据库

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForJDBC/idea连接mysql数据库1.png" style="float:left;" />

<img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForJDBC/idea连接mysql数据库2.png" style="float:left;" />



### 3、Java代码操作数据库

```java
public class JDBCDemon {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        // 注册mysql的驱动包
        Class.forName("com.mysql.cj.jdbc.Driver"); // mysql 5之后可省略
        // 获取数据库连接对象
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root", "113011");
        // sql语句
        String sql = "update stu set age = 20 where id = 4";
        // 获取执行sql的对象Statement
        Statement statement = con.createStatement();
        // Statement对象执行sql语句，并返回受影响行数
        int count = statement.executeUpdate(sql);
        System.out.println(count);
        // 释放资源
        statement.close();
        con.close();
    }
}
```

**正常处理异常写法：**

```java
public static void main(String[] args) {
    Connection connection = null;
    Statement statement = null;
    ResultSet resultSet = null;
    try {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root", "113011");
        statement = connection.createStatement();
        String sql = "select * from dep";
        resultSet = statement.executeQuery(sql);
        while (resultSet.next()){
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            // Object obj = resultSet.getObject(1) // 用于接收不知道类型的字段对应的数据
            System.out.println("id: " + id + "," + "name: " + name);
        }
    } catch (SQLException e) {
        System.out.println("异常");
    }finally {
        try {
            if(resultSet != null){
                resultSet.close();
            }
        } catch (SQLException e) {
            System.out.println("异常");
        }

        try {
            if(statement != null){
                statement.close();
            }
        } catch (SQLException e) {
            System.out.println("异常");
        }

        try {
            if(connection != null){
                connection.close();
            }
        } catch (SQLException e) {
            System.out.println("异常");
        }
    }
}
```



#### 1）DriverManager 类

`java.sql.DriverManager` 



##### 常用方法

- ==注册驱动==

  `public static void registerDriver(Driver driver) throws SQLException`  这是DriverManager中的注册驱动的方法

  ```java
  Class.forName("com.mysql.jdbc.Driver"); // 注册mysql的驱动包
  // 这句话是将类加载到jre中，使用这句话不用调用registerDriver方法就能注册驱动包
  ```

  **com.mysql.cj.jdbc.Driver 源码：**

  ```java
  // 通过查看com.mysql.cj.jdbc.Driver类的源码
  static {
      try {
          DriverManager.registerDriver(new Driver());
      } catch (SQLException var1) {
          throw new RuntimeException("Can't register driver!");
      }
  }
  // 源码中有一个静态代码块，在该类已加载时，就进行驱动包注册了，所以去反射加载该类，很方便
  ```

  > **注意：**在mysql 5之后，该句`Class.forName("com.mysql.jdbc.Driver"); `可省略，运行程序，会自动注册驱动包，因为mysql加了配置文件。里面帮我们写了自动注册驱动的包路径。
  >
  > <img src="https://gitee.com/herocheung/study_pic/raw/master/pictureForJDBC/驱动注册省略.png" style="float:left;" />

- ==获取数据库连接==

  `public static Connection getConnection(String url,String user,String password)throws SQLException` 静态方法，获取数据库连接

  参数：

  + url：形式为 jdbc:subprotocol:subname 的数据库网址
  + user：用户名
  + password：密码

  > 注意：DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root", "113011")
  >
  > 如果是连接本地ip地址，默认端口号3306。那么可以简写为
  >
  > DriverManager.getConnection("jdbc:mysql:///db1", "root", "113011")



#### 2）Connection 接口

`java.sql interface Connection` 接口类

**功能：**

1. 获取执行sql语句对象
2. 管理事务



##### 常用抽象方法

- `Statement createStatement() throws SQLException` 

  创建一个 Statement 对象，用于将SQL语句发送到数据库。  没有参数的SQL语句通常使用 Statement 对象执行。  如果相同的SQL语句执行了很多次，那么使用`PreparedStatement`对象可能会更有效。

- `PreparedStatement prepareStatement(String sql) throw SQLException`

  创建一个`PreparedStatement`对象，用于将参数化的SQL语句发送到数据库。

- `void setAutoCommit(boolean autoCommit)throws SQLException`

  当参数为false则是关闭自动提交，就是开启事务管理，就是需要手动提交

- `void rollback() throws SQLException`

  回滚事务，当执行不成功，则不会更改数据

- `void commit() throws SQLException`

  提交事务



#### 3）Statement 接口

`java.sql Interface Statement` 接口，用于执行静态SQL语句并返回其生成的结果的对象。



##### 常用抽象方法

- `boolean execute(String sql) throws SQLException`

  该语句，可以执行任何一条sql语句

- `int executeUpdate(String sql) throws SQLException`

  执行SQL中的DML语句(insert、update、delete)，也可执行DDL语句(执行DDL语句不常用)

- `ResultSet executeQuery(String sql) throws SQLException`

  执行SQL中的DQL语句，并返回单个`ResultSet`对象

  

#### 4）ResultSet 接口

`java.sql Interface ResultSet` 结果集接口，用于封装查询结果的接口



##### 常用抽象方法

- `boolean next() throws SQLException` 

  将光标从当前位置向前移动一行。 `ResultSet`光标最初位于第一行之前;  第一次调用方法`next`使第一行成为当前行;  第二个调用使第二行成为当前行，依此类推。

- `void beforeFirst() throws SQLException`

  将指针指定到查询结果的第一行数据

- `void afterLast() throws SQLException` 

  将指针指定到查询结果的最后一行

- `boolean previous() throws SQLException`

  将指针指定到前一行

- `boolean absolute(int row) throws SQLException`

  将指针指到指定的一行

- `getXXX(列字段名参数)` 

  该方法通过数据类型，获取指定字段名的数据。

  可传入int类型的值：表示列的编号，如要获取第一列那么就传入1

  可传入String类型的值：表示表的字段名
  
  

##### 练习：将查询到的数据封装到类中并遍历

```java
public static void main(String[] args) {
    Connection connection = null;
    Statement statement = null;
    ResultSet resultSet = null;
    ArrayList<Dep> arrayList = new ArrayList<>(); // 用于存储结果的集合
    
    try {
        Class.forName("com.mysql.cj.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root", "113011");
        statement = connection.createStatement();
        String sql = "select * from dep";
        resultSet = statement.executeQuery(sql);
        while (resultSet.next()){
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            arrayList.add(new Dep(id,name)); // 将结果封装到类中，并存放在集合中
        }
        // 遍历
        arrayList.forEach((list)-> System.out.println(list));
    } catch (SQLException e) {
        System.out.println("异常");
    }finally {
        try {
            if(resultSet != null){
                resultSet.close();
            }
        } catch (SQLException e) {
            System.out.println("异常");
        }

        try {
            if(statement != null){
                statement.close();
            }
        } catch (SQLException e) {
            System.out.println("异常");
        }

        try {
            if(connection != null){
                connection.close();
            }
        } catch (SQLException e) {
            System.out.println("异常");
        }
    }
}
```



#### 5）PrepareStatement 接口

`java.sql Interface PrepareStatement` 执行动态sql语句

```java
PreparedStatement pstmt = 
    			  con.prepareStatement("UPDATE EMPLOYEES SET SALARY = ? WHERE ID = ?");
pstmt.setBigDecimal(1, 153833.00);    // 将第一个？，赋值成153833.00，类型是BigDecimal
pstmt.setInt(2, 110592);  // 将第二个？，赋值成110592，类型是int
rs = pstmt.executeQuery();
```



##### 常用抽象方法

- `setXxx(int parameterIndex, Xxx value)`

  Xxx代表类型，API中有很多这样的方法，所以为了方便，用Xxx代替类型。该方法时将sql的占位符`?`，换成指定的值。

  parameterIndex 是需要将第几个的占位符赋值(从1开始)

  value 是值

- `boolean execute(String sql) throws SQLException`

  该语句，可以执行任何一条sql语句

- `int executeUpdate(String sql) throws SQLException`

  执行SQL中的DML语句(insert、update、delete)，也可执行DDL语句(执行DDL语句不常用)

- `ResultSet executeQuery(String sql) throws SQLException`

  执行SQL中的DQL语句，并返回单个`ResultSet`对象



  ## 三、JDBCUtils 自定义工具类

如果要在很多类中，连接数据库，那么就要写很多冗余的代码，重复的代码，为了解决这个繁杂的操作。可以自定义一个JDBCUtils工具类，用静态方法来封装这些代码，那么在使用的时候，直接调用就可以了。

jdbc.properties

```properties
url=jdbc:mysql://localhost:3306/db1
user=root
password=113011
driver=com.mysql.cj.jdbc.Driver
```

JDBCUtils.java

```java
public class JDBCUtils {
    private static String url;
    private static String user;
    private static String password;

    static {
        try {
            
            Properties properties = new Properties();
            ClassLoader classLoader = JDBCUtils.class.getClassLoader();
            URL resource = classLoader.getResource("jdbc.properties");
            // System.out.println(Demo.class.getClassLoader().getResource("").getPath());
            // 最好检查以下来加载器获取数据的默认的路径，才把properties文件放到对应路径之下
            String path = resource.getPath();
            properties.load(new FileReader(path));
            
            url = properties.getProperty("url");
            user = properties.getProperty("user");
            password = properties.getProperty("password");
            
            Class.forName(properties.getProperty("driver"));
        } catch (IOException e) {
            System.out.println("properties异常");
        }
    }

    // 连接
    public static Connection getConnection(){
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            System.out.println("连接异常");
        }
        return connection;
    }

    // 释放资源
    public static void close(Statement statement, Connection connection){
        if(statement!=null){
            try {
                statement.close();
            } catch (SQLException e) {
                System.out.println("statement异常");
            }
        }

        if(connection!=null){
            try {
                connection.close();
            } catch (SQLException e) {
                System.out.println("connection异常");
            }
        }
    }

    // 释放资源
    public static void close(ResultSet resultSet, Statement statement, Connection connection){
        if(resultSet!=null){
            try {
                resultSet.close();
            } catch (SQLException e) {
                System.out.println("resultSet异常");
            }
        }else if(statement!=null){
            try {
                statement.close();
            } catch (SQLException e) {
                System.out.println("statement异常");
            }
        }else if(connection!=null){
            try {
                connection.close();
            } catch (SQLException e) {
                System.out.println("connection异常");
            }
        }
    }
}
```

JDBCDemon.java

```java
public class JDBCDemon{
    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<Dep> arrayList = new ArrayList<>();
        try {
            connection = JDBCUtils.getConnection();
            
            statement = connection.createStatement();
            
            String sql = "select * from dep";
            
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                arrayList.add(new Dep(id,name));
            }
            arrayList.forEach((list)-> System.out.println(list));
        } catch (SQLException | NullPointerException exception) {
            System.out.println("异常");
        }finally {
            JDBCUtils.close(resultSet,statement,connection);
        }
	}
}
```



## 四、提高JDBC安全，<u>注入安全</u>

### 关于Statement与PrepareStatement的区别

Statement：执行静态sql语句(有安全问题)

PrepareStatement：执行动态sql语句



###  .  登录案例

Statement 不安全的写法：

```java
public static boolean selectByUsernamePwd(String username, String pwd) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/db1";
			con = DriverManager.getConnection(url, "root", "113011");
			stmt = con.createStatement();
			String sql = "select * from user where username= '"+username+"' and pwd= '"+pwd+"'"; // 超极不安全
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (con != null)
					con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
```

**解析：**

如果用户随便输入登录名，但是密码输入的是 ==123' or '1'='1==

那么就会登录成功。

sql生成语句是：

==select * from user where username= 'hero' and pwd= '123 '  or   '1'='1‘==

可分析为`or`前部分为 false，但是`or`后部分为 true，那么结果为true，就会查询到数据表中的所有数据，那么就是有数据返回，则在java执行语句中，就会返回true，那么就会登录成功。

```
sql解析成：
select * from user where false or true;
最终查询的是true，永真。因此即使前面输入用户名，在密码中输入or1=1就能成功登录。
```

为了解决这个问题，就引入了动态sql执行对象，PrepareStatement



PrepareStatement 安全案例：

```java
public static boolean selectByUPSafe(String username, String pwd) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/db1;
			con = DriverManager.getConnection(url, "root", "113011");
			String sql = "select * from user where username = ? and pwd = ?";
			pstmt = con.prepareStatement(sql); // 获取PreparedStatement并且预编译sql
			pstmt.setString(1, username); // 获取第一个问号，并传值
			pstmt.setString(2, pwd); // 获取第二个问号，并传值
			rs = pstmt.executeQuery();
			if(rs.next()) {
				return true;
			}else {
				return false;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (con != null)
					con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
```



## 五、JDBC 控制事务 Connection实例实现

- `void setAutoCommit(boolean autoCommit)throws SQLException`

  当参数为false则是关闭自动提交，就是开启事务管理，就是需要手动提交

- `void commit() throws SQLException`

  提交事务

- `void rollback() throws SQLException`

  回滚事务，当执行不成功，则不会更改数据。开启了手动提交数据，不用显示调用该方法，也可以数据回滚



### . 练习：转账案例

```java
public class Transaction {
    public static void main(String[] args) {
        Statement statement = null;
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            
            connection.setAutoCommit(false); // ***********传入false就是，手动提交事务
            
            statement = connection.createStatement();
            String sql1 = "update sal set salary = (salary - 500) where id = 1";
            String sql2 = "update sal set salary = (salary + 500) where id = 2";
            statement.executeUpdate(sql1);
            // int i  = 10/0;
            statement.executeUpdate(sql2);
            
            connection.commit(); // ***********提交数据
            
            System.out.println("转账成功");
        } catch (SQLException | ArithmeticException e) {
            try {
                
                connection.rollback(); // ***********显示定义回滚
                
                System.out.println("转账失败");
            } catch (SQLException ex) {
                System.out.println("rollback 回滚异常");
            }
        }finally {
            JDBCUtils.close(statement,connection);
        }

    }
}
```



## 六、数据库连接池

一个程序如果频繁连接数据库，断开数据库，是十分消耗资源的。为了解决该问题，就引入了数据库连接池。

数据库连接池：是一个存放数据库连接的容器(集合)，当系统初始之后就会创建好该容器，将连接对象创建出来并存入该容器中，当需要连接数据库时，就从该容器中获取连接对象进行数据库连接访问，访问之后，会将连接对象归还给容器。

优点：

1. 减少资源浪费
2. 效率高



### . DataSource 接口 JDK 1.4

`javax.sql Interface DataSource` 接口，有sun公司提供的数据源(数据库连接池)接口。由数据库厂商去实现该接口。



#### 常用抽象方法

- `Connection getConnection() throws SQLException;` 

  获取Connection对象的抽象方法

- `Connection getConnection(String username, String password) throws SQLException;`

  获取Connection对象的抽象方法。第一个参数为数据库的用户名，第二个参数时密码



归还连接：

如果该连接对象是从数据库连接池中获取的，那么最后调用close()方法，不是将连接对象释放掉，而是将其归还给原来的数据连接池中。



### 1、C3P0

数据库连接池技术

1. 导入jar包：

   ```xml
   <dependency>
       <groupId>com.mchange</groupId>
       <artifactId>c3p0</artifactId>
       <version>0.9.5</version>
   </dependency>
   
   <dependency>
       <groupId>com.mchange</groupId>
       <artifactId>mchange-commons-java</artifactId>
       <version>0.2.12</version>
   </dependency>
   ```

2. 定义配置文件 `c3p0.properties` 或者 `c3p0-config.xml`，定义在classpath路径之下，C3P0会自动去找该配置文件。

   ```xml
   <c3p0-config>  
   	<!-- 默认连接配置-->
       <default-config>  
           <property name="driverClass">com.mysql.cj.jdbc.Driver</property>  
           <property name="jdbcUrl">jdbc:mysql://localhost:3306/day16</property>  
           <property name="user">root</property>  
           <property name="password">root</property>  
             
           <!--初始大小，默认3-->
           <property name="initialPoolSize">10</property>
           
           <!--最大空闲时间，超过空闲时间的连接将被丢弃。为0或负数则永不丢弃。默认为0-->
           <property name="maxIdleTime">30</property>
           
           <!--连接池中保留的最大连接数，默认15-->
           <property name="maxPoolSize">20</property>
           
           <!--连接池中保留的最小连接数，默认15-->
           <property name="minPoolSize">5</property>
           
           <!--定义在从数据库获取新连接失败后重复尝试获取的次数，默认为30-->
           <property name="acquireRetryAttempts">30</property>
           
           <!--两次连接中间隔时间，单位毫秒，默认为1000-->
           <property name="acquireRetryDelay">30</property>
           
           <!--当连接池用完时客户端调用getConnection()后等待获取新连接的时间，超时后将抛出					SQLException，如设为0则无限期等待。单位毫秒，默认为0-->
           <property name="checkoutTimeout">0</property>
           
           <!--当连接池中的连接用完时，C3P0一次性创建新连接的数目-->
           <property name="acquireIncrement">0</property>
           
           <!--DBC的标准参数，用以控制数据源内加载的PreparedStatement数量。但由于预缓存的Statement属 			  于单个Connection而不是整个连接池。所以设置这个参数需要考虑到多方面的因素，如果					maxStatements与 maxStatementsPerConnection均为0，则缓存被关闭。默认为0；-->
           <property name="maxStatements">0</property>
       </default-config>  
   	
       
       
        <!-- 针对其他数据库的连接配置-->
       <named-config name="oracle">  
           <!--配置其他数据库，语法与默认配置的一直--> 
       </named-config>  
   </c3p0-config>
   ```

3. 创建核心对象，数据库连接池对象 CombopooledDataSource 是DataSource的实现类，是在c3p0包下的

   **CombopooledDataSource()**，该构造方法会调用配置文件的默认配置

   **CombopooledDataSource(String configName)**，该构造方法会调用配置文件name为configName的相关配置

4. 获取连接对象：getConnection()

```java
public class C3P0Demon {
    public static void main(String[] args) {
        // 获取连接对象
        DataSource dataSource = new ComboPooledDataSource();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            String sql = "select * from sal";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                String salary = resultSet.getString(3);
                System.out.println(id+"     "+name+"    "+salary);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}
```



### 2、Druid (常用)

数据库连接池技术

由阿里巴巴提供的数据库连接技术。

1. 导入jar包 ：

   ```xml
   <!-- https://mvnrepository.com/artifact/com.alibaba/druid -->
   <dependency>
       <groupId>com.alibaba</groupId>
       <artifactId>druid</artifactId>
       <version>1.1.10</version>
   </dependency>
   ```

2. 定义配置文件 `druid.properties` ，可以叫任意名称，可以放在任意目录下，需手动加载配置文件

   ```properties
   driverClassName=com.mysql.cj.jdbc.Driver
   url=jdbc:mysql://localhost:3306/db1
   username=root
   password=113011
   initialSize=5
   maxActive=10
   maxWait=3000
   ```

3. 获取数据库连接池对象，通过durid提供的工厂类来获取 DruidDataSourceFactory

   ```java
   DataSource dataSource = DruidDataSourceFactory.createDataSource(Properties properties);
   dataSource.getConnection();
   ```

4. 获取连接对象：getConnection()

```java
public class DruidDemon {
    public static void main(String[] args) throws Exception {
        Connection connection = null;
        
        InputStream resourceAsStream = DruidDemon.class.getClassLoader().getResourceAsStream("druid.properties");
        Properties properties = new Properties();
        properties.load(resourceAsStream);
        
        DataSource dataSource = DruidDataSourceFactory.createDataSource(properties);
        
        for (int i = 0; i < 10; i++) {
            connection = dataSource.getConnection();
            System.out.println(connection); // 能够打印出10个不同的连接对象
        }
        connection.close();
    }
}
```



##### JDBCUtils 提升

为了使用数据库连接池，更加方便，可以写一个含有静态代码块，静态方法的工具类，使用的时候直接调用该工具类的方法就可以了。

```java
/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/3/15
 * Time: 18:06
 * Description: 针对Druid连接池的工具类
 * Version: V1.0
 */
public class JDBCUtils {
    private static DataSource dataSource = null;
    private static Connection connection = null;
    static {
        try {
            InputStream resourceAsStream = JDBCUtils.class.getClassLoader().getResourceAsStream("druid.properties");
            Properties properties = new Properties();
            properties.load(resourceAsStream);
            dataSource = DruidDataSourceFactory.createDataSource(properties);
        } catch (IOException e) {
            System.out.println("Properties异常");
        } catch (Exception e) {
            System.out.println("createDataSource(properties) 异常");
        }
    }

    public static Connection getConnection(){
        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            System.out.println("dataSource.getConnection() 异常");
        }finally {
            return connection;
        }
    }
    
    public static DataSource getDataSource(){
        return dataSource;
    }

    public static void close(ResultSet resultSet, Statement statement, Connection connection){
        if(resultSet!=null){
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(statement!=null){
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(connection!=null){
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(Statement statement, Connection connection){
        if(statement!=null){
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(connection!=null){
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(Connection connection){
        if(connection!=null){
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
```



## 七、Spring JDBC 使用数据源

Spring框架对JDBC进行了简单的封装。提供了JdbcTemplate对象简化JDBC的开发



### 1、JdbcTemplate 类

依赖于数据库连接池

`org.springframework.jdbc.core`包下的类

1. 导入jar包

   ```xml
   <dependency>
               <groupId>org.springframework</groupId>
               <artifactId>spring-beans</artifactId>
               <version>4.3.8.RELEASE</version>
           </dependency>
   
           <dependency>
               <groupId>org.springframework</groupId>
               <artifactId>spring-core</artifactId>
               <version>4.3.8.RELEASE</version>
           </dependency>
   
           <dependency>
               <groupId>org.springframework</groupId>
               <artifactId>spring-jdbc</artifactId>
               <version>4.3.8.RELEASE</version>
           </dependency>
   
           <dependency>
               <groupId>org.springframework</groupId>
               <artifactId>spring-tx</artifactId>
               <version>4.3.8.RELEASE</version>
           </dependency>
   
           <dependency>
               <groupId>commons-logging</groupId>
               <artifactId>commons-logging</artifactId>
               <version>1.2</version>
           </dependency>
   ```

2. 创建JdbcTemplate对象

   ```java
   new JdbcTemplate(DataSource dataSource);
   // dataSource 数据源，也就是数据库连接池的类
   ```

3. 调用JdbcTemplate的方法完成CRUD的操作



#### 常用方法

- `public int update(final String sql) throws DataAccessException`

  该方法用于执行DML语句，增删改

- `public Map<String, Object> queryForMap() throws DataAccessException`

  执行DQL语句，将查询到的语句封装到Map集合中，只能存储一条记录，将字段值分别存为键值对 (没写参数)

- `public Map<String, Object> queryForList() throws DataAccessException`

  执行DQL语句，将查询到的语句封装到List集合中 (没写参数)

- `public <T> T query() throws DataAccessException`

  执行DQL语句，将查询到的语句封装为javabean对象 (没写参数)

- `public <T> T queryForObject() throws DataAccessException`

  执行DQL语句，将查询到的语句封装到Object对象中 (没写参数)



### 练习：操作sal表

1. 修改id=1的工资为10000；
2. 添加一条记录；
3. 删除上一条添加的记录；
4. 查询id=1的记录，将其封装为map集合；
5. 查询所有的记录，将其封装为List集合；
6. 查询所有的记录，将其封装为javabean对象(sal类对象中)；
7. 查询总记录数

```java
public class JDBCTemplate {
    // 创建jdbcTemplate对象，并使用自己写的实现了Druid数据库连接池的JDBCUtils工具类来获取DataSource
    JdbcTemplate jdbcTemplate = new JdbcTemplate(JDBCUtils.getDataSource());

    public static void main(String[] args) {
        JDBCTemplate template = new JDBCTemplate();
        template.test01();
        template.test02();
        template.test03();
        template.test04();
        template.test05();
        template.test06_2();
        template.test07();
    }

    public void test01(){
        // 更新数据库
        String updateSQL = "update sal set salary = 10000 where id = ?";
        // 其中的一个方法，第一个传入sql语句，第二个传入于占位符一一对应的值，执行完会自动释放资源
        int count = jdbcTemplate.update(updateSQL,1);
        System.out.println(count);
    }

    public  void test02(){
        String insertSQL = "insert into sal(name, salary) values(? , ?)";
        int count = jdbcTemplate.update(insertSQL, "vincent", 8000);
        System.out.println(count);
    }

    public  void test03(){
        String deleteSQL = "delete from sal where id = ?";
        int count = jdbcTemplate.update(deleteSQL, 3);
        System.out.println(count);
    }

    public  void test04(){
        String selectFirstSQL = "select * from sal where id = ?";
        Map<String, Object> map = jdbcTemplate.queryForMap(selectFirstSQL, 1);
        System.out.println(map);
    }

    public  void test05(){
        String selectForList = "select * from sal";
        // 将每一条记录封装到map集合，再将map集合添加到list集合中
        List<Map<String, Object>> list = jdbcTemplate.queryForList(selectForList);
        for (Map<String, Object> stringObjectMap : list) {
            System.out.println(stringObjectMap);
        }
    }

    public  void test06_1(){
        String selectForJavaBean = "select * from sal";
        // 将每一条记录封装到sal类中，在将该类封装到list集合中
        // 该方法，第二个参数是一个RowMapper函数式接口，有一个抽象方法 T mapRow(ResultSet var1, int var2)
        List<Sal> query = jdbcTemplate.query(selectForJavaBean, (resultSet, i) -> {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String salary = resultSet.getString("salary");
            Sal sal = new Sal(id,name,salary);
            return sal;
        });
        for (Sal sal : query) {
            System.out.println(sal);
        }
    }

    public void test06_2(){
        String selectForJavaBean = "select * from sal";
        // 通过创建一个BeanPropertyRowMapper<Sal>()类，自动封装，简化了test06_1的自己封装
        // 通过反射获取到Sal类的信息，因此可以将查询到的数据封装到Sal类中，在封装到list集合中
        List<Sal> query = jdbcTemplate.query(selectForJavaBean, new BeanPropertyRowMapper<Sal>(Sal.class));
        for (Sal sal : query) {
            System.out.println(sal);
        }
    }

    public void test07(){
        String selectForCount = "select COUNT(id) from sal";
        // 将结果封装到Integer类中，利用反射
        Integer integer = jdbcTemplate.queryForObject(selectForCount,Integer.class);
        System.out.println(integer);
    }
}
```



