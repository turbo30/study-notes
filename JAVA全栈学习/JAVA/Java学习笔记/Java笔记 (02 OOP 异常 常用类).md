# JAVA 核心阶段

Write once, run anywhere...

面向对象编程 OOP



## 一、面向对象编程OOP

面向对象编程(Object-Oriented Programming,OOP)

本质：<span style="color:red">以类的方式组织代码，以对象的组织(封装)数据</span>。

特点：<span style="color:red">封装性、继承性、多态性</span>



从认识的角度：先有具体的对象，然后从这些对象中总结出共同的特点，产生类。

从代码运行角度：从类创建出一个一个具体的对象。



### 1、面向过程 & 面向对象

- **面向过程思想**

  这是一个线性思维，就是分析出解决问题所需要的步骤，然后用函数把这些步骤一步一步实现。

  **优点**：性能比面向对象高，因为类调用时需要实例化，开销比较大，比较消耗资源;比如单片机、嵌入式开发、 Linux/Unix等一般采用面向过程开发，性能是最重要的因素。

  **缺点**：没有面向对象易维护、易复用、易扩展

- **面向对象思想**

  把构成问题事务分解成各个对象，建立对象的目的不是为了完成一个步骤，而是为了描叙某个事物在整个解决问题的步骤中的行为。

  **优点**：易维护、易复用、易扩展，由于面向对象有封装、继承、多态性的特性，可以设计出低耦合的系统

  **缺点**：性能比面向过程低

<span style="color:red">对于描述复杂的事物，为了从宏观上把握、从整体上去合理分析，我们需要用面向对象的思路来分析整个系统。但是，具体到微观操作，仍然需要面向过程的思路去处理。</span>



### 2、类与对象的关系

- 类是一种抽象的数据类型，它是对某一类事物整体描述\定义，但并不能代表某一个具体事物
- 对象是抽象概念的具体实例



### 3、构造器(构造方法)详解

一类什么都不写，在编译完成之后，**它也会存在一个无参构造的空方法**。

构造方法的作用是实例化初始值。

- 使用new关键字，必须要有构造方法；
- 当定义了有参构造方法，还需要使用无参构造方法，那么就必须显示定义无参构造方法
- 为了避免程序，定义类的时候将无参构造都显示定义，留着备用也可



### 4、创建对象内存分析

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/new内存分析.png)



### 5、OOP特点 <u>重点</u>



#### 1）封装性

追求<span style="color:red">高内聚，低耦合</span>。高内聚就是类的内部数据操作由自己操作完成，低耦合就是尽量暴露少量的方法给外部使用。

通常，应禁止直接访问一个对象中数据的实际表示，而应通过操作接口来访问，这称为信息隐藏。

记住一句话：<span style="color:red">属性私有，get/set</span>

【**意义**】

1. 通过 get/set方法 对属性进行封装，在 get/set方法 中可以对数据进行检验，通过才可以赋值或者被获取，这样可以使内部数据更安全，避免破坏系统。

2. 提高程序安全性，保护数据

3. 隐藏代码的实现细节
4. 统一接口
5. 增加系统可维护性



#### 2）继承性 

**关键字：extends**

Java中类**只有单继承，没有多继承**

继承是类和类之间的关系。继承关系的两个类，一个为子类(派生类)，一个为父类(基类)。

一个类默认会继承Object类。IDEA可通过<kbd>Ctrl</kbd>+<kbd>H</kbd>查看继承关系。



##### 类不能被继承的情况

1. 如果类被`final`修饰，那么此类不可以被继承；
2. 如果类中只有`private`的构造方法，那么此类不可以被继承。

**原因**

> （1）一个类一定会有构造函数，如果不写，那就是默认的无参构造函数，如果写，就只有所写的构造函数。        （2）子类的构造函数一定会调用父类的构造函数，但是如果父类中只有私有的构造方法，那么子类就无法调用父类，就会有问题。



##### super详解 <u>关于super的重点</u>

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/继承性super.png)

当new了子类的无参构造方法，那么系统会先执行`super();`调用父类默认的无参构造方法(不管有没有`super();`这句话，有则去执行，无则系统默认添加该语句去执行)，然后才会执行子类的构造方法。

super注意点：

1. super调用父类的构造方法，必须在子类构造方法的第一个；
2. super必须只能出现在子类的方法或者构造方法中；
3. super和this不能同时调用构造方法；
4. this(); 调用本类的构造方法、 super(); 调用父类的构造方法。



##### 方法重写 4种权限修饰符

重写都是方法的重写，与属性无关。

需要有继承关系，子类重写父类的方法！

- 方法名必须相同

- 参数列表必须相同

- 修饰符(范围可以扩大但不能缩小)：public > protected > (default)不是关键字 > private

  ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/四种权限修饰符.png)

- 抛出异常(范围可以缩小但不能扩大)

重写，子类的方法必须和父类的方法一致，就是重新给这个方法赋予新的功能，即方法体不同！

【**意义**】

1. 父类的功能，子类不一定需要，或者不一定满足，这样就可以重写来满足子类的需求



#### 3）多态性 <u>面试高频问题</u>

##### 重点理解多态性

多态就是用基类的引用指向子类的对象。

> 比如有一个父类superClass，它有2个子类subClass1，subClass2。superClass有一个方法 func(),两个子类都重写了这个方法。那么我们可以定义一个superClass的引用obj，让它指向一个子类的对象，比如superClass obj = new subClass1()；那么我们调用obj.func()方法时候，会进行动态绑定，也就是obj它的实际类型的func()方法，即subClass1的func()方法。同样你写superClass obj = new subClass2()；obj.func()其实调用的是subClass2的func()方法。这种由于子类重写父类方法，然后用父类引用指向子类对象，调用方法时候会进行动态绑定，这就是多态。多态对程序的扩展具有非常大的作用，比如你要再有一个subClass3，你需要改动的东西会少很多。

多态是同一个行为具有多个不同表现形式或形态的能力。多态就是同一个接口，使用不同的实例而执行不同操作。

**多态存在的三个必要条件** 

- 继承
- 重写
- 父类引用指向子类对象

当使用多态方式调用方法时，首先检查父类中是否有该方法，如果没有，则编译错误；如果有，再去调用子类的同名重写方法。  

**多态的好处**：可以使程序有良好的扩展，并可以对所有类的对象进行通用处理。



【个人理解】父类可以写很多方法，子类继承父类，然后对父类的方法进行重写，满足子类的需求，然后在使用时，父类引用指向子类对象`Person person = new Student();`，那么很简单的就可以直接调用父类的方法，展现了子类对象重写的这个方法的具体表现。



##### instanceof

判断一个对象类型（有无父子或继承关系）

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/instanceof.png)



如果有继承关系，那么可以进行类型的转换，进行类型转换之后就可以使用那个类自己的方法。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/继承关系类型转换.png)



如图，obj进行强制类型转换成Student类型之后，就可以使用它本身自己的方法了。

【**注意**】

- 向上转型：子类转为父类，可能会丢失自己本来的一些方法。
- 向下转型：父类转为子类，强制转换。
- 转型是为了方便方法的调用，减少重复代码，有效提高利用率，简洁性。



### 6、Static关键字详解

前见：[Java学习笔记(01基础阶段) 六、6、](.\Java学习笔记(01基础阶段).md)

#### <u>静态代码块 匿名代码块 构造器</u>

```java
public class Demon{
    // 1 最早加载，且只执行一次
    static{
        System.out.println("静态代码块");
    }
    
    // 2 作用可以赋初始值
    {
        System.out.println("匿名代码块");
    }
    
    // 3
    public Demon(){
        System.out.println("构造方法");
    }
    
    public static void main(String[] args){
        Demon d1 = new Demon();
        System.out.println("======================");
        Demon d2 = new Demon();
    }
}
```

输出结果：

> 静态代码块
>
> 匿名代码块
>
> 构造方法
>
> ======================
>
> 匿名代码块
>
> 构造方法

通过执行顺序，可知静态代码块最先执行，它与类一同加载，并且只执行一次。



#### 静态导入包

```java
// 静态导入包
import static java.lang.Math.random;

public class Test{
    public static void main(String[] args){
        System.out.println( random() );
    }
}
```

通过静态导入包的方式，可以不用使用类来调用方法，可以直接使用该方法。比如上示例子，如不静态导入包，则必须使用`Math.random()`。



### 7、抽象类

**关键字：abstract**

- abstract修饰符可用来修饰方法即抽象方法，也可以修饰类即抽象类。
- 抽象类中可以没有抽象方法，但是有抽象方法的类一定要声明为抽象类。
- 抽象类是用来让子类继承的(单继承)；抽象方法是只有声明没有方法的实现，它是用来让子类实现的。
- 子类继承抽象类，就必须要实现抽象类没有实现的抽象方法，否则该子类也要声明为抽象类。
- 抽象类中可以写普通方法。
- 构造方法，类方法不能声明为抽象方法。
- 抽象类除了不能实例化对象之外，类的其它功能依然存在，成员变量、成员方法和构造方法的访问方式和普通类一样。



#### 1）<u>抽象类不能被实例化，但是有构造方法吗</u>？ <u>面试</u>

> 可以有，抽象类可以声明并定义构造函数。只是不能被实例化而已。
>
> 因为你不可以创建抽象类的实例，所以构造函数只能通过构造函数链调用（Java中构造函数链指的是从其他构造函数调用一个构造函数）。例如，当你创建具体的实现类。



#### 2）<u>如果你不能对抽象类实例化，那么构造函数的作用是什么？</u>

>
> 它可以用来初始化抽象类内部声明的通用变量，并被各种实现使用。另外，即使你没有提供任何构造函数，编译器将为抽象类添加默认的无参数的构造函数，没有的话你的子类将无法编译，因为在任何构造函数中的第一条语句隐式调用super()。



#### 3）<u>抽象类存在的意义</u>

> 就是为了增加代码的重用性，提高开发效率，有较好的可扩展性。
>
> 比如当游戏需要创建一个角色的时候，很多特征都是相同的，但是又有某些方法是独特的，所以可以继承抽象类，将抽象类中的抽象方法，进行独特化，创建出属于每个角色的特征。



### 8、接口

**关键字：interface**

```java
public interface User{
    // 接口中定义的变量都是常量 public static final
    // public static final int AGE = 18;
    int AGE = 18;
    
    // 系统会默认给你添加 public abstract
    // public abstract void run();
    void run();
}
```



【**作用**】

- 接口通过接口的方法去约束别人的实现
- 定义一些方法，让别人去实现
- 接口不能被实例化，没有构造方法
- 类通过implements可实现多个接口
- 必须要实现接口中的所有方法



#### 1）普通类、抽象类、接口的区别

**普通类**：只有具体的实现。

**抽象类**：具体实现和规范(抽象方法)都有。

**接口**：只有规范！约束和实现分离：面向接口编程



### 9、N种内部类

内部类就是一个类的内部再定义一个类。

分为：成员内部类、静态内部类、局部内部类、匿名内部类

#### 成员内部类

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/内部类外部类.png" style="float:left" />



#### 静态内部类

```java
public class Outer{
    private int id = 10;
    public void out(){
        System.out.println("This is Outer");
    }
    
    public static class Inner{
        public void in(){
            System.out.println("This is Inner");
        }
    }
}
```

静态内部类，与外部类一同加载。所以静态内部类，不能访问外部类的非静态成员。



#### 局部内部类

```java
public class Outer{
    // 局部内部类
    public void method(){
        class Inner{
            public void in(){
                
            }
        }
    }
}
```



#### 匿名内部类

```java
public class Test{
    public static void main(String[] args){
        // 没有变量来接收实体化的类
        new Apple().eat();
        // 通过匿名内部类方法写的接口，就可以在主类种new一个接口来实现接口的方法
        // 返回的是结果，并不是存储它的变量
        UserService userService = new UserService(){
            @Override
            public void hello(){
                
            }
        }
    }
}

class Apple{
    public void eat(){
        System.out.println("Eatting...");
    }
}

interface UserService{
    void hello();
}
```



## 二、异常机制



### 1、两种类型的异常 一个错误

- 检查性异常：最具代表的检查性异常是用户错误或问题引起的异常，这是程序员无法预见的。例如要打开一个不存在的文件时，一个异常就发生了，这些异常在运行时不能被简单的忽略。
- 运行时异常：运行时异常时可能被程序员避免的异常。
- 错误ERROR：错误不是异常，而是脱离程序员控制的问题。例如栈溢出时，一个错误就发生了，它们在编译也检查不到。



### 2、异常体系结构

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/Throwable.png)

Java把异常当作对象来处理，并定义一个基类java.lang.Throwable作为所有异常的超类。

`AWTError` 当发生严重的 Abstract Window Toolkit 错误时，抛出此错误

`EOFException` 表示在输入过程中意外地到达文件结束或流结束。

`ArithemeticException` 算数异常

`InterruptedException` 线程sleep方法的中断异常

`UnknownHostException` 不知道地址异常

`ConnectException` 客户端连接异常

`IllegalStateException` 流被使用了一次，再次使用，就会报该异常

 `ConcurrentModificationException` 并发修改异常

在Java API中已经定义了许多异常类，这些异常类分为两大类，错误Error和异常Excepton。

#### <u>面试问题</u>

throwable有几个分支？常见exception有哪些？ 见体系图(背住，笔试)

一行代码实现内存溢出？

```java
int[] intArray = new int[1_000_000_000]; // 数组会分配空间，不断扩展内存 OutOfMemoryError
```

堆栈溢出？

```java
public void test(){ // 无限递归
    test();
} // StackOverflowError 堆栈溢出
```





### 3、异常处理5个关键字

抛出异常、捕获异常

**try、catch、finally、throw、throws**



#### 1）try、catch、finally 捕获异常

```java
public class Test{
    public static void main(String[] args){
        int a = 1;
        int b = 0;
        
        try{ // try异常监控区域
            System.out.println(a/b);
        }catch(ArithmeticException e){ // catch捕获异常并处理异常
            System.out.println("程序出现异常，变量b不能为0");
        }finally{ // 处理善后工作，不管上代码块执不执行，最终都会执行finally里的代码
            System.out.println("finally");
        }
        
        // finally 可以不写，往往是必写的，它的作用是可以关闭IO等资源的关闭！比如如果scanner需要捕获异常，后面有需要关闭scanner流，则需要在finally中进行处理
    }
}
```



```java
public class Test{
    public static void main(String[] args){
        int a = 1;
        int b = 0;
        
        try{
            System.out.println(a/b);
        }catch(Error e){
            System.out.println("Error");
        }catch(Exception e){
            System.out.println("Exception");
        }catch(Throwable t){
            System.out.println("Throwable");
        }finally{
            System.out.println("finally");
        }
    }
}
```

可使用多个catch，但是从上至下的捕获类型为以此增大。具体看**异常体系结构**。



#### 2）throw、throws 声明异常

```java
public class Test{
    public static void main(String[] args){
        try{
            new Test.test(1,0);
        }catch(ArithmeticException e){
            // e.printStackTrace(); 打印异常
            System.out.println("程序出现异常，变量b不能为0");
        }
    }
    
    public void test(int a, int b) throws ArithmeticException{
        if( b == 0 ){
            throw new ArithmeticException();
        }
    }
}
```

throw是方法里语句上使用的抛出异常，throws是在方法上使用的抛出异常。



### 4、自定义异常

自定义异常继承Exception类就可以了

MyException.java

```java
public class MyException extends Exception{
    private int detail;
    
    public MyException(int a){
        this.detail = a;
    }
    
    @Override
    public String toString(){
        return "MyException{"+detail+"}";
    }
}
```

Test.java

```java
public class Test{
    public static void main(String[] args){
        try{
            new Test.test(11);
        }catch(MyException e){
            System.out.println(e);
        }
    }
    
    public void test(int a) throws MyException{
        if(a>10){ // 如果值大与10就抛出自定义的异常
            throw new MyException(a);
        }
        System.out.println("OK");
    }
}

// 会输出 MyException{11}
```



### 5、实际应用中关于异常的经验总结

- 处理运行时异常时，采用逻辑去合理规避，同时辅助 try-catch 处理
- 在多重catch块后面，可以增加一个catch(Exception)来处理可能会被遗漏的异常
- 对于不确定的代码，也可以加上 try-catch ，处理潜在的异常
- 尽量去处理异常，切忌只简单地掉用 printStackTrace() 去打印输出
- 具体如何处理异常，要根据不同的业务需求和异常类型去决定
- 尽量添加finally语句块去释放占用的资源



## 三、常用类

### 1、Object类与Objects类

#### Object类

java.lang.Object类 **JDK 1.0**

重要的两个方法：toString() 、equals()

- Object是类层次结构的根(最顶层)类。<u>每个类都是用Object为超(父)类</u>。

- 所有对象(包括数组)都实现这个类的方法。

- 打印对象名字，其实就是调用对象的toString方法，一般会**重写toString方法**，否则输出结果为对象空间地址，没实际意义。
- Object的equals方法，是比较的对象的地址值，和基本数据类型的值。所以一般会**重写equals方法**，比如String类就是重写了equals方法，去比较属性的值，而不是比较对象的地址。

#### Objects类

JDK7 添加了Objects工具类

在比较两个对象的时候，Object的equals方法容易抛出空指针异常，而Objects的equals方法就优化了这个问题。

Objects的equals方法源码：

```java
public static boolean equals(Object a, Object b){
    return (a == b) || (a != null && a.equals(b));
}
```

运用：

```java
String s1 = null;
String s2 = "abc";
Objects.equals(s1, s2); // flase 用Objects工具类，会容忍null的情况
```





### 2、基本数据类型 包装类

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/基本数据类型包装类.png" style="float:left" />

在这八个类中，除了Character和Boolean以外，其他的都是”数字型“，”数字型“都是java.lang.Number的子类。Number类是抽象类，因此它的抽象方法，所有子类都需要实现。Number类提供了抽象方法：intValue()、longValue()、floatValue()、doubleValue()，意味着所有的”数字型“包装类都可以互相转型。

- 这里展示**Integer类**的使用，其他包装类用法类似：

```java
public class Test{
    public static void main(String[] args){
        // 基本数据类型转换成包装类对象
        Integer a = new Integer(10);
        Integer b = Integer.valueOf(20); // 官方推荐用法
        
        // 包装类对象转换成基本数据类型
        int c = b.intValue();
        double d = b.doubleValue();
        
        // 把字符串转成包装类对象
        Integer e = new Integer("30"); // 从构造器的可见，只能传入纯属数字的字符串
        Integer f = Integer.parseInt("40");
        
        // 把包装类对象转成字符串
        String g = f.toString();
        
        // 包装类中的常量
        System.out.println(Integer.MAX_VALUE);
    }
}
```



#### 1）自动装箱、自动拆箱

自动装箱、自动装箱过程，其实就是编译器将不规范的语句，进行规范化。

自动装箱、自动装箱实则是为了简化程序员写较多代码。

```java
public class Test{
    public static void main(String[] args){
        Integer a = 10; // 实际编译器会自动装箱 Integer a = Integer.valueOf(20); 
        int b = a; // 实际编译器会自动拆箱 int b = a.intValue();
    }
}
```



#### 2）<u>缓存源码分析</u>

```java
// 数字类型，缓存范围为[-128，127]
Integer in1 = Integer.valueOf(-128);
Integer in2 = -128;
System.out.println(in1 == in2); // true，因为-128在缓存范围内
System.out.println(in1.equals(in2)); // true

Integer in3 = 1234;
Integer in4 = 1234;
System.out.println(in3 == in4); // false，因为1234不在缓存范围内
System.out.println(in3.equals(in4)); // true
```

valueOf()方法源码：

```java
public static Integer valueOf(int i){
    if(i >= IntegerCache.low && i <= IntegerCache.high)
        return IntegerCache.cache[i + (-IntegerCache.low)];
    return new Integer(i);
}
```

通过点击 valueOf 进行源码分析，valueOf方法会对传入的值进行缓存判断，通过继续查看缓存源代码，会发现在程序初始化时就会加载cache数组，里面就存入了-128到127的数。所以在把-128赋值给`in1`和`in2`时，都会指向系统最开始初始化的同一个引用空间所以`in1==in1`返回的是true。而当1234赋值给`in3`和`in4`时，因为超过了缓存空间，就会创建一个新的实例，把引用地址赋给变量，由于`in3`和`in4`指向的不是同一个空间，所以`in3==in4`返回的是false。而equals方法是对里面存储的数值进行比较，因为数值是一样的所以返回的true。



### 3、String类 <u>== 与 equals</u>

**String类 JDK1.0**

String类是不可变对象，因为它是存储在final修饰的char数组，数组不可修改，即不可变的Unicode字符序列。不同字符串进行拼接，就会产生新的字符串，多了会占用大量内存，效率低。

比如：substring()方法是对字符串的截取操作，但本质是读取原字符串内容生成新的字符串。

```java
String str1 = "aaabcb";
String str2 = str1.substring(2,5); // 截取数组中下标为2到下标为4的字符串 [2,5)
String str3 = str1.substring(0,str1.indexOf("c")); // 截取从索引值为0到元素为c的位置
System.out.println(str2); // 输出 abc
System.out.println(str3); // 输出 aaab

// 也可通过hashcode方法判断是否为同一个对象
System.out.println(Integer.toHexString(str1.hashCode()));
System.out.println(Integer.toHexString(str2.hashCode()));
// 很显然输出的是不同的，所以字符串对象是不同的
```

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/equals.png)



【注意】通过3和4点，在做字符串比较的时候不要使用`==`，使用`equals`方法来进行比较。因为`==`比较的是值，当是对象的时候，比较的就是引用地址，而`equals`比较的是值和引用空间地址里存储的值。



#### new String 与 String字符 区别

> String实质是字符数组，两个特点：1、该类不可被继承；2、不可变性(immutable)
>
> 例如 String s1 = new String("myString");
>
> 和 String s1 = "myString"; 
>
> 第一种方式通过关键字new定义过程：在程序编译期，编译程序先去字符串常量池检查，是否存在“myString”,如果不存在，则在常量池中开辟一个内存空间存放“myString”；如果存在的话，则不用重新开辟空间，保证常量池中只有一个“myString”常量，节省内存空间。然后在内存堆中开辟一块空间存放new出来的String实例，在栈中开辟一块空间，命名为“s1”，存放的值为堆中String实例的内存地址，这个过程就是将引用s1指向new出来的String实例
>
> 第二种方式直接定义过程：在程序编译期，编译程序先去字符串常量池检查，是否存在“myString”，如果不存在，则在常量池中开辟一个内存空间存放“myString”；如果存在的话，则不用重新开辟空间。然后在栈中开辟一块空间，命名为“s1”，存放的值为常量池中“myString”的内存地址



#### 1）StringBuilder类与StringBuffer类

**StringBuilder类和StringBuffer类一个可变的字符序列。**

**StringBuffer类 JDK 1.0**

**StringBuilder类 JDK 1.5**

StringBuilder类和StringBuffer类都继承了AbstractStringBuilder抽象类，都是可变的对象，接收的是char[] value数组。



**历史：**String和StringBuffer是由sun公司设计的；StringBuilder是由微软公司设计的。



##### StringBuilder类常用方法

- `Object.append()`：追加字符，返回调用方法的本身，可传入任何类型的数据
- `Object.reverse()`：反转内容
- `Object.toString()`：将缓冲区内容转为字符串String类型

```java
StringBuilder sb = new StringBuilder("abcdefg");
System.out.println(Integer.toHexString(sb.hashCode())); // 输出 15db9742
System.out.println(sb); // 输出 abcdefg

sb.setCharAt(2,'M');
System.out.println(Integer.toHexString(sb.hashCode())); // 输出 15db9742
System.out.println(sb); // 输出 abMdefg
```

以上，再次证明了是修改的同一个字符串里面的值，而不是读取值，然后创建的一个对象来存储值。



【区别】

- Sting类 不可变，线程不安全
- StringBuffer类 可变，线程安全，但效率低
- StringBuider类 可变，线程不安全，但效率高。初始容量为16字节，超出容量，会自动扩容。



### 4、Date、DateFormat、SimpleDateFormat、Calendar、GregorianCalendar类



#### 1）Date类

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/date.png" style="float:left;" />

在计算机世界，把**1970年1月1日 00:00:00**定位基准时间，每个度量单位是毫秒。

long类型的变量表示时间，从基准时间开始获取毫秒数，往前或往后得到指定的时间。

```java
long now = System.currentTimeMillis(); // 获取当前系统的时间
```



**Date类的常用方法**

- **Date()** 默认构造方法，分配一个Date对象，并初始化此对象为系统当前的日期和时间，可以精确到毫秒
- **Date(long date)** 分配Date对象并初始化此对象，以表示从标准基准时间加上指定的毫秒数得到指定的时间
- **boolean after(Date when)** 测试此日期是否在指定日期之后
- **boolean before(Date when)** 测试此日期是否在指定日期之前
- **bollean equals(Object obj)** 比较两个日期的相等性
- **long getTime()** 返回从标准基准时间到此Date对象时间的毫秒数
- **String toString()** 把Date对象转换为String类型(dow mon dd hh:mm:ss zzz yyyy)

【注意】

指定日期的Date()方法，已被弃用，已被Calendar.set()方法替代。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/指定日期.png" style="float:left;" />



#### 2）DateFormat抽象类、SimpleDateFormat类

把时间对象转化为指定格式的字符串。反之，把指定格式的字符串转为时间对象。

DateFormat类是抽象类，一般使用它的子类SimpleDateFormat类来实现。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/日期格式化字符含义.png)

```java
public class Test{
    public static void main(String[] args){
        // new出SimpleDateFormat对象,一定要传入被格式化的字符串样式
        SimpleDateFormat s1 = new SimpleDateFormat("yyyy年MM月dd日 hh:mm:ss");
        SimpleDateFormat s2 = new SimpleDateFormat("yyyy-MM-dd");
        // 将时间对象转成字符串
        String dayTime = s1.format(new Date());
        System.out.println(dayTime);
        System.out.println(s2.format(new Date()));
        System.out.println(new SimpleDateFormat("hh:mm:ss").format(new Date()));
        // 将符合指定格式的字符串转成时间对象，字符串格式需要与指定格式一致
        String time = "2020-2-11";
        Date date = s2.parse(time);
        System.out.println(date);
    }
}
```



#### 3）Calendar抽象类、GregorianCalendar类

Calendar抽象类，为我们提供了关于日期计算的相关的抽象方法。

GregorianCalendar(公历)类是Calendar类的具体实现子类，提供了世界上大多数国家/地区使用的标准日历系统。

GregorianCalendar类有很多构造器，可进入源码查看。

**常用方法：**

```java
public class Test{
    public static void main(String[] args){
        // 获取
        Calendar calendar = new GregorianCalendar(2020,10,9,22,10,50);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH); // 也可使用 Calendar.DATE
        int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
        
        // 设置
        Calendar calendar1 = new GregorianCalendar(); // 什么都不传入，则默认是系统当前时间
        calendar1.set(Calendar.YEAR,8012); // 设置年份
        
        // 日期计算
        calendar.add(Calendar.DATE,100); // 将2020.10.9 的日期加100天的日期
        calendar.add(Calendar.YEAR,-100); // 2020.10.9 往前100年
        
        // 日期对象和时间对象的转化
        Date date = calendar.getTime();
        canlendar.setTime(new Date());
    }
}
```



```java
//获取时间加一年或加一月或加一天
Date date = new Date();
Calendar cal = Calendar.getInstance();
cal.setTime(date);//设置起时间
System.out.println("111111111::::"+cal.getTime());
cal.add(Calendar.YEAR, 1);//增加一年
cal.add(Calendar.DATE, 1);//增加一天  
cal.add(Calendar.DATE, -10);//减10天  
cal.add(Calendar.MONTH, 1);//增加一个月   
System.out.println("输出::"+cal.getTime());
```



#### 4）计算当前日期前一年是星期几

```java
public static void main(String[] args) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SS E");
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());
    calendar.add(Calendar.YEAR,-1);
    Date time = calendar.getTime();
    System.out.println(dateFormat.format(time));
}
```

```
2019-11-08 16:26:31:988 星期五
```



#### 5）实战：控制台输出可视化日历程序

```java
package com.LFJava.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TestDate {
    public static void main(String[] args) {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日");
        String sDate = dateFormat.format(date);
        System.out.println(sDate);

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int day = calendar.get(Calendar.DAY_OF_MONTH); // 获取当前系统的日(意思是用get方法得到当前系统的DAY_OF_MONTH值)
        int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH); // 通过调用getActualMaximum()方法可以计算出当前月的最大日期
        calendar.set(Calendar.DAY_OF_MONTH,1); // 将DAY_OF_MONTH常量设置为1
        int startDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK); // 得到将DAY_OF_MONTH设置成1时，是星期几
        System.out.println("日\t一\t二\t三\t四\t五\t六");

        for (int j = 1; j < startDayOfWeek; j++){
            System.out.print("\t");
        }

        for (int i = 1; i <= days; i++) {

            if(calendar.get(Calendar.DAY_OF_MONTH) == day){
                System.out.print(calendar.get(Calendar.DAY_OF_MONTH)+"*\t");
            }else {
                System.out.print(calendar.get(Calendar.DAY_OF_MONTH)+"\t");
            }
            if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY){
                System.out.println();
            }
            calendar.add(Calendar.DAY_OF_MONTH,1);
        }

    }
}

```



### 5、Math类、Random类的常用方法

#### Math类

```java
public class TestMath{
    public static void main(String[] args){
        // 去整相关操作
        Math.ceil(3.2); // 4.0
        Math.floor(3.2); // 3.0
        Math.round(3.2); // 3
        Math.round(3.8); // 4
        // 绝对值、开方、幂操作
        Math.abs(-45); // 45
        Math.sqrt(64); // 8.0
        Math.pow(5,2); // 25.0
        // Math类中常用常量
        Math.PI; // 3.141592653589793
        Math.E; // 2.718281828459045
        // 随机数
        Math.random(); // [0,1)之间的数
    }
}
```

#### Random类

```java
public class TestMath{
    public static void main(String[] args){
        Random rand = new Random();
        // 随机生成[0,1)之间的double类型的数
        rand.nextDouble();
        // 随机生成[0,1)之间的int类型的数
        rand.nextInt();
        // 随机生成[0,1)之间的float类型的数
        rand.nextFloat();
        // 随机生成flase和true
        rand.nextBoolean();
        // 随机生成[0,10)之间的int类型的数
        rand.nextInt(10);
        // 随机生成[20,30)之间的int类型的数
        20 + rand.nextInt(10);
        // 随机生成[20,30)之间的double类型的数
        20 + (int)(rand.nextDouble()*10);
    }
}
```



### 6、File类的基本使用

java.io.File类：文件和目录。

File类的常见构造方法：public File(String pathname)



#### File类常见方法

```java
public class TestFile{
    public static void main(String[] args){
        File f = new File("d:/a.txt");
        // File f = new File("d:\\a.txt");
        
        f.renameTo(new File("d:/b.txt")); // 修改文件名
        f.exists(); // File是否存在
        f.isDirectory(); // File是否是目录
        f.isFile(); // File是否是文件
        new Date(f.lastModified()); // File最后修改时间
        f.length(); // File的大小
        f.getName(); // File的文件名(包括扩展名)
        f.getPath(); // File的路径
        f.getAbsolutePath(); // File的绝对路径
        
        System.getProperty("user.dir"); // 获取当前.java文件的存储路径
        
        File f2 = new File("c.txt"); 
        f2.createNewFile(); // 不加路径，则会默认在当前文件下创建创建
    }
}
```



#### 创建空文件和目录

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForJava/File类.png" style="float:left" />

```java
public class TestFile{
    public static void main(String[] args){
        File f1 = new File("d:/a.txt");
        f1.createNewFile();
        f1.delete();
        
        File f2 = new File("d:/电影/华语/大陆");
        boolean flag1 = f2.mkdir();
        System.out.println(flag1); // 创建失败 false
        boolean flag2 = f2.mkdirs();
        System.out.println(flag2); // 创建成功 true
    }
}
```



#### 实战 利用递归打印目录

```java
public class PrintFileTree{
    public static void main(String[] args){
        File f = new File("d:/");
        PrintFileTree.printFile(f, 0);
    }
    
    static void printFile(File file, int level){
        // - 输出的多少，决定文件的层数
        for(int i = 0; i < level; i++){
            System.out.print("-");
        }
        System.out.println(file.getName());
        if(file.isDirectory()){
            File[] files = file.listFiles();
            for(file temp: files){
                printFile(temp, level + 1);
            }
        }
    }
}
```



### 7、枚举Enum类 JDK1.5

**关键字：enum**

java.lang.Enum类 

枚举就是用来存放常量的，每一个常量都是默认`public static final`修饰的。

- 不要使用枚举的高级用法，会增加程序的复杂性。
- 枚举类，经过编译之后，其实是一个类，继承了枚举类

```java
public class TestEnum{
    public static void main(String[] args){
        Season season = Season.SPRING;
        switch(season){
            case SPRING:
                System.out.println("春天");
                break;
            case SUMMER:
                System.out.println("夏天");
                break;
            case AUTUMN:
                System.out.println("秋天");
                break;
            case WINTER:
                System.out.println("冬天");
                break;
            default:
                break;
        }
    }
    
    enum Season{
        SPRING,SUMMER,AUTUMN,WINTER
    }
}
```



### 8、System类

java.lang.System类中提供了大量的静态方法，可获取与系统相关的信息或系统级别操作，在其API中，常用方法有：

- `pulic static long currentTimeMillis()`：返回以毫秒为单位的当前时间；
- `public static void arraycopy(Object src, int srcPos, Object dest, int destPos, int length)`：将数组中指定的数据拷贝到另一个数组中

#### currentTimeMillis(）

```java
System.out.println(System.currentTimeMillis());
```

#### arraycopy()

- src：源数组
- srcPos：源数组中的起始位置
- dest：目标数组
- destPos：目标数组起始位置
- length：要复制的数组元素的个数

```java
String[] s1 = {1,2,3,4,5,6};
String[] s2 = {4,5,6,7,8,9};
System.arraycopy(s1, 0, s2, 0, 3);
System.out.println(s2.toStirng()); // [1,2,3,7,8,9]
```

