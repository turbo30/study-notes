# Java Web 01



## 一、Web概念

web（World Wide Web）即全球广域网，也称为万维网，它是一种基于超文本和HTTP的、全球性的、动态交互的、跨平台的分布式图形信息系统。是建立在Internet上的一种网络服务，为浏览者在Internet上查找和浏览信息提供了图形化的、易于访问的直观界面，其中的文档及超级链接将Internet上的信息节点组织成一个互为关联的网状结构。



web应用程序：可以提供浏览器访问的程序。



### URI、URL、URN

1. `URI`：Uniform Resource Identifier，统一资源标志符  (相当于一个人身份)
2. `URL`：Uniform Resource Location，统一资源定位符  (要通过具体地址去找到这个身份的人)
3. `URN`： Uniform Resource Name，统一资源名称  (相当于这身份的人的名字)

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForWeb/URI关系图.png)

**举例说明：**

URI：北京xxx公司的总经理
URN：北京xxx公司的总经理张三	
URL：中国北京市海淀区长安街35号北京xxx公司的总经理张三



### Web应用程序组成部分

- html/css/js
- jsp、servlet
- java程序
- jar包
- 配置文件 (properties、yaml)



### Http / Https 区别

- HTTPS协议需要到CA申请证书，一般免费证书很少，需要交费。
- HTTP协议运行在TCP之上，所有传输的内容都是明文。HTTPS运行在SSL/TLS之上，SSL/TLS运行在TCP之上，所有传输的内容都经过加密的。(Transport Layer Security、Secure Sockets Layer)
- HTTP和HTTPS使用的是完全不同的连接方式，用的端口也不一样，前者是80，后者是443。
- HTTPS可以有效的防止运营商劫持，解决了防劫持的一个大问题。



## 二、Web服务器

服务器是一个被动的操作，用于处理客户的一些请求和给客户一些响应信息。



### 1、3个常见技术 ASP、PHP、JSP

**ASP：**

- 微软公司的，使用的是C#，国内最早流行
- 在HTML中嵌入了VB的脚本，ASP + COM
- 使用ASP开发，因在一个页面中基本上都是大量业务代码，造成页面混乱，后期难以维护，维护成本高
- 用的服务器是微软自带的服务器`IIS(Internet Information Services服务器)` 在控制面版，程序于卸载，启用或关闭Windows功能可以开启或关闭



**PHP：**

- PHP开发动态页面，开发速度块，功能强大，简单
- 无法承载大访问量的情况，具有局限性



**JSP/Servlet：**

- sun公司(现已被Oracle公司收购)主推的B/S框架
- 基于Java语言，可承载三高(高并发，高性能，高可用)问题带来的影响
- 语言类似ASP，易于ASP程序员迁移到 JSP 开发



### 2、IIS、Tomcat

IIS(Internet Information Services服务器)微软的服务器，是处理Windows自带的服务请求。



Tomcat服务器，Tomcat 技术先进、性能稳定，而且免费，因而深受Java 爱好者的喜爱并得到了部分软件开发商的认可，成为目前比较流行的Web 应用服务器。Tomcat和IIS等Web服务器一样，具有处理HTML页面的功能，另外它还是一个Servlet和JSP容器，独立的Servlet容器是Tomcat的默认模式。



## 三、Tomcat 详解 (重要)

>  Tip：工作3~5年可以尝试写Tomcat服务器。



### 1、Tomcat目录介绍

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForWeb/tomcat目录介绍.png" style="float:left" />



### 2、conf 配置目录

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForWeb/conf目录.png" style="float:left" />



#### 1）server.xml 核心配置文件

==配置文件中有默认的3个端口号==

- **8005**：监听关闭服务器的端口号
- **8080**：负责建立HTTP连接。在通过浏览器访问Tomcat服务器的Web应用时，使用的就是这个连接器
- **8009**：负责和其他的HTTP服务器建立连接，在把Tomcat与其他HTTP服务器集成时，就需要用到这个连接器



**配置内容：**

- 可配置端口号；

- 可配置存放项目的文件地址，默认wbapps；

- 可配置host，访问地址，默认localhost(127.0.0.1)；

  ```xml
  <Host name="localhost"  appBase="webapps"
              unpackWARs="true" autoDeploy="true">
  ```




#### 2）webapps 目录结构

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForWeb/webapps.png" style="float:left" />

将项目放如该文件目录下，开启服务器，就可以访问了。

如访问docs：`localhost:8080/docs` 意思是访问docs目录，如果不指定访问目录，则会自动访问ROOT目录



**网站应有的目录结构：**

```java
webapps: 
	- ROOT
    - heroC  (网站的目录)
        - WEB-INF
        	- calsses	(java程序)
        	- lib	(web应用依赖的jar包)
        	- web.xml	(网站配置文件)
        - index.html (主页)
        - static (静态文件)
        	- css
        	- js
        	- img
```



### 3、项目部署tomcat

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForWeb/项目部署tomcat1.png)

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForWeb/项目部署tomcat2.png)



### 4、==_==IDEA使用Tomcat部署web项目的过程

在idea中配置的tomcat，在运行时idea不会把项目放到原本tomcat的webapps目录下该路径下，而是复制一份足够的配置文件，到 `${user.home}/.IntelliJIdea/system/tomcat` 目录下：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForWeb/ideaTomcat.png)

也就是说每个项目都有属于自己的一份tomcat配置，互不干扰。

每个项目的配置文件夹中有一个 `/conf/Catalina/localhost/ROOT.xml` 文件，内容如下：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Context path="" docBase="xxx" />
```

path是指在访问此项目时，是否需要添加额外的路径，如果为空，则直接使用域名或者ip就可以访问到该项目：127.0.0.1;

docBase是指要运行的项目的部署位置，`xxx` 就是我的项目源代码的位置。



整个项目启动过程就是：

首先maven build项目，将构建结果写到项目的out\artifacts目录下，然后idea复制一份tomcat的配置文件到`${user.home}/.IntelliJIdea/system/tomcat `中，之后启动tomcat安装目录下的`catalina.bat`文件，tomcat读取idea复制的配置文件，找到项目位置，然后就运行起来了。



### <u>关于服务器面试题</u>



**1、在tomcat中能否将`localhost`域名改成`www.heroc.com`或其他域名？**

> 如果直接修改Tomcat的conf/server.xml配置文件
>
> ```xml
> <Host name="localhost"  appBase="webapps"
>          unpackWARs="true" autoDeploy="true">
> ```
>
> 将localhost改成`www.heroc.com`去启动tomcat服务器是不可以访问的，访问会失败。
>
> 要去**更改window的底层配置文件**，将本地地址`127.0.0.1`别名修改成`www.heroc.com`才可以通过该域名访问。具体操作如下：
>
> 1、conf/server.xml配置文件 (此操作改不改都无所谓，只要系统配置文件，可将地址映射到定义的字符串上)
>
> ```xml
> <Host name="www.heroc.com"  appBase="webapps"
>          unpackWARs="true" autoDeploy="true">
> ```
>
> 2、C:\Windows\System32\drivers\etc\hosts 添加以下一句话 (重点，不区分带小写)
>
> ```
> 127.0.0.1		www.heroc.com
> ```
>
> 然后启动Tomcat，就可以通过`http://www.heroc.com:8080/` 来访问Tomcat服务器上`webapps`目录下的网页了（注意清除缓存，再访问）
>
> ![image-20200319191814992](https://gitee.com/turbo30/study_pic/raw/master/pictureForWeb/image-20200319191814992.png)



**2、请谈谈网站是如何进行访问的。**

>当用户从浏览器中输入一个域名，并点击回车时
>
>1，会先检查本机`C:\Windows\System32\drivers\etc\hosts`文件中查找是否有该域名的映射，有则解析本机地址，并返回该地址内相关的信息；
>
>2，如果本机hosts配置文件中，没有该域名的映射，那么会去DNS(域名服务系统)中查找是否有该域名，如果有该域名，则会找到该域名的服务地址，进行信息的返回或访问；
>
>3，如果DNS中也没有则直接报错，无该网页
>
>![](https://gitee.com/turbo30/study_pic/raw/master/pictureForWeb/访问网站流程.png)



## 四、Http协议

HTTP (超文本传输协议) 是一个简单的请求-响应协议，通常用于TCP之上。端口号：80

- 文本：html，js，css...
- 超文本：图片，音频，视频...

HTTPS (超文本传输安全协议) 端口号 443



### IP地址划分

网关：一个公有ip和一个私有ip

公有ip下根据机器的数量不同，选择适合的ip段(ip还可分为A类、B类、C类、D、E)，每个ip段可分配的个数不同。所有私有ip只能访问主机的私有ip地址，由主机访问公有ip，去访问网站，然后将数据返回给私有ip的机器。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForWeb/网关私有ip.png)



### HTTP/0.9

- HTTP/0.9是**第一个版本的HTTP协议**，已过时。它的组成极其简单，**只允许客户端发送GET这一种请求**，且**不支持请求头**。由于没有协议头，造成了**HTTP/0.9协议只支持一种内容，即纯文本**。不过网页仍然支持用HTML语言格式化，同时**无法插入图片**。
- HTTP/0.9具有**典型的无状态性**，每个事务独立进行处理，事务结束时就释放这个连接。由此可见，HTTP协议的无状态特点在其第一个版本0.9中已经成型。**一次HTTP/0.9的传输首先要建立一个由客户端到web服务器的TCP连接，由客户端发起一个请求，然后由web服务器返回页面内容，然后连接会关闭**。如果请求的页面不存在，也不会返回任何状态码。

------

### HTTP/1.0

HTTP协议的**第二个版本**，第一个**在通讯中指定版本号的HTTP协议版本**，至今仍被广泛使用。**相对于HTTP/0.9增加了下面几个特性**：

- **请求与响应支持头部**。
- **响应对象以一个响应状态码开始**。
- **响应对象不只限于超文本**。
- 开始**支持客户端通过POST方法向web服务器提交数据，支持GET、HEAD、POST方法**。
- **支持长连接（但默认还是使用短连接）、缓存机制以及身份认证**。

------

### HTTP/1.1

HTTP协议的**第三个版本**是HTTP/1.1，是目前使用最广泛的协议版本。HTTP/1.1是目前主流的HTTP协议版本，相对于HTTP/1.0新增了如下内容：

- **默认为长连接**
  HTTP/1.1支持长连接（PersistentConnection）和请求的流水线（Pipelining）处理，**在一个TCP连接上可以传送多个HTTP请求和响应**，**减少了建立和关闭连接的消耗和延迟**，**在HTTP/1.1中默认开启Connection：keep-alive**，**一定程度上弥补了HTTP/1.0每次请求都要创建连接的缺点**。
- 提供了**范围请求功能（宽带优化）**
  **在HTTP/1.0中，存在一些浪费带宽的现象**，例如**客户端只是需要某个对象的一部分，而服务器却将整个对象送过来了，并且不支持断点续传功能**，**HTTP/1.1则在请求头引入了range头域，它允许值请求资源的某个部分，即返回码是2.6（Partial Content），这样就方便了开发者自由的选择以便于充分利用带宽和连接。这是支持文件断点续传的基础**。
- 提供了**虚拟主机的功能（HOST域）**
  **在HTTP/1.0中认为每台服务器都绑定一个唯一的IP地址**，因此，**请求消息中的URL并没有传递主机名（hostname）**。但随着虚拟主机技术的发展，每一台物理服务器上可以存在多个虚拟主机（Multi-homed Web Servers），并且它们共享一个IP地址。HTTP/1.1请求消息和响应消息都应支持Host头域，且请求消息中如果没有Host头域会报告一个错误（400 Bad Request）。
- **缓存处理字段**
  HTTP/1.1在1.0的基础上**加入了一些cache的新特性，引入了实体标签，一般被称为e-tags，新增更为强大的Cache-Control头**。
- **错误通知的管理**
  在HTTP/1.1中**新增了24个错误状态响应码**，如**409（Conflict）表示请求的资源与资源的当前状态发生冲突**；**410（Gone）表示服务器上的某个资源被永久性的删除**。

------

### HTTP/2.0

HTTP协议的**第四个版本**，**相对于HTTP/1.1新增了以下内容**：

- **二进制分帧**，HTTP/2.0的**所有帧都采用二进制编码**。
  - 帧：客户端与服务器通过交换帧来通信，帧是基于这个新协议通信的最小单位。
  - 消息：指逻辑上的HTTP消息，比如请求、响应等，由一或多个帧组成。
  - 流：流是连接中的一个虚拟信道，可以承载双向的消息；每个流都有一个唯一的整数标识符（1，2…N）。
- **多路复用**
  多路复用允许**同时通过单一的HTTP/2.0连接发起多重的请求-响应消息**。**有了新的分帧机制后，HTTP/2.0不再依赖多个TCP连接去处理更多并发的请求**。**每个数据流都拆分成很多互不依赖的帧，而这些帧可以交错（乱序发送），还可以分优先级。最后再在另一端根据每个帧首部的流标识符把它们重新组合起来**。**HTTP/2.0连接都是持久化的，而且客户端与服务器之间也只需要一个连接（每个域名一个连接）即可**。
- **头部压缩**
  **HTTP/1.1的首部带有大量信息，而且每次都要重复发送**。**HTTP/2.0要求通讯双方各自缓存一份首部字段表，从而避免了重复传输**。
- **请求优先级**
  **浏览器可以在发现资源时立即分派请求，指定每个流的优先级，让服务器决定最优的响应次序**。这样请求就不必排队了，既节省了时间，也最大限度地利用了每个连接。
- **服务端推送**
  服务端推送能**把客户端所需要的资源伴随着index.html一起发送到客户端**，**省去了客户端重复请求的步骤**。正因为没有发起请求，建立连接等操作，所以静态资源通过服务端推送的方式可以极大地提升速度。



### 1、Http1.0 、Http1.1主要区别

#### 1）长连接

**Http1.0**	默认不支持长连接的，每一次请求就会重新连接服务器。如果需要长连接，需要在http头加入"Connection: Keep-Alive"来建立长连接。

**Http1.1**	通过默认是长连接的。只需要请求一次，就可长时间与服务器建立连接，请求该服务器其他信息，也不会重新建立连接，除非手动断开了于服务器的连接。可通过"Connection: close"来关闭长连接。

> HTTP是基于TCP/IP协议的，创建一个TCP连接是需要经过三次握手的,有一定的开销，如果每次通讯都要重新建立连接的话，对性能有影响。因此最好能维持一个长连接，可以用个长连接来发多个请求。



#### 2）节约宽带

HTTP 1.1支持只发送header信息(不带任何body信息)，如果服务器认为客户端有权限请求服务器，则返回100，否则返回401。客户端如果接受到100，才开始把请求body发送到服务器。这样当服务器返回401的时候，客户端就可以不用发送请求body了，节约了带宽。

另外HTTP1.1还支持传送内容的一部分。这样当客户端已经有一部分的资源后，只需要跟服务器请求另外的部分资源即可。这是支持文件断点续传的基础。



#### 3）Host域

现在可以web server例如tomat，设置虚拟站点是非常常见的，也即是说，web server上的多个虚拟站点可以共享同一个ip和端口。

HTTP1.0是没有host域的，HTTP1.1才支持这个参数。



### 2、Http1.1、Http2.0 主要区别

#### 1）多路复用

HTTP2.0使用了多路复用的技术，做到同一个连接并发处理多个请求，而且并发请求的数量比HTTP1.1大了好几个数量级。

当然HTTP1.1也可以多建立几个TCP连接，来支持处理更多并发的请求，但是创建TCP连接本身也是有开销的。<u>TCP连接有一个预热和保护的过程</u>，先检查数据是否传送成功，一旦成功过，则慢慢加大传输速度。因此对应瞬时并发的连接，服务器的响应就会变慢。所以最好能使用一个建立好的连接，并且这个连接可以支持瞬时并发的请求。



#### 2）数据压缩

HTTP1.1不支持header数据的压缩，HTTP2.0使用HPACK算法对header的数据进行压缩，这样数据体积小了，在网络上传输就会更快。



#### 3）服务器推送

当我们对支持HTTP2.0的web server请求数据的时候，服务器会顺便把一些客户端需要的资源一起推送到客户端，免得客户端再次创建连接发送请求到服务器端获取。这种方式非常合适加载静态资源。

服务器端推送的这些资源其实存在客户端的某处地方，客户端直接从本地加载这些资源就可以了，不用走网络，速度自然是快很多的。



### 3、http请求

#### 请求行

```
Request URL: https://www.baidu.com/
Request Method: GET
Status Code: 200 OK
Remote Address: 39.156.66.18:443
Referrer Policy: no-referrer-when-downgrade
```

请求行中有请求连接，请求方式，请求状态码等信息

请求方式：GET, POST等

**GET**：请求能够携带的参数长度有限制，会在浏览器URL地址栏显示请求参数的内容，不安全，但高效

**POST**：请求能够携带的参数长度无限制，不会再浏览器URL地址栏显示请求参数的内容，安全，但相对于			  GET不高效



#### 请求头

```
Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
Accept-Encoding: gzip, deflate, br
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: max-age=0
Connection: keep-alive
...
```



### 4、http响应

#### 响应头

```java
Bdpagetype: 1
Bdqid: 0xae4e331d00306773
Cache-Control: private
Connection: keep-alive
Content-Encoding: gzip
Content-Type: text/html;charset=utf-8
Date: Fri, 20 Mar 2020 05:19:50 GMT
Expires: Fri, 20 Mar 2020 05:19:22 GMT
Server: BWS/1.1
Set-Cookie: BDSVRTM=0; path=/
Set-Cookie: BD_HOME=1; path=/
Set-Cookie: H_PS_PSSID=30969_1428_21117_30823_30717; path=/; domain=.baidu.com
Strict-Transport-Security: max-age=172800
Traceid: 1584681590277118234612560032610433132403
Transfer-Encoding: chunked
X-Ua-Compatible: IE=Edge,chrome=1
```



#### 响应状态码(重点)

1xx：信息，请求收到，继续处理；

2xx：成功；200

3xx：重定向；

4xx：客户端错误；404

5xx：服务端错误；500内部服务器错误，502网关错误

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForWeb/http状态码.png)



### 5、http是无连接、无状态的

**一.无连接**

1. 每一个访问都是无连接，服务器挨个处理访问队列里的访问，处理完一个就关闭连接，这事儿就完了，然后处理下一个新的
2. 无连接的含义是限制每次连接只处理一个请求。服务器处理完客户的请求，并收到客户的应答后，即断开连接

**二.无状态** 

   1.协议对于事务处理没有记忆能力

   2.对同一个url请求没有上下文关系

   3.每次的请求都是独立的，它的执行情况和结果与前面的请求和之后的请求是无直接关系的，它不会受前面的请求应答情况直接影响，也不会直接影响后面的请求应答情况

   4.服务器中没有保存客户端的状态，客户端必须每次带上自己的状态去请求服务器



### 6、窗口大小

窗口大小只是形象的解释了，每次发送的数据大小对方接受的数据大小，对方接受的数据大小，就是窗口大小。





### <u>关于http协议面试</u>

**1，http1.0，http1.1的区别；http1.1，http2.0的区别**

> 见第四大点的1，2小节



**2，当在浏览器输入url地址回车之后，到页面加载完成，经历了什么？**

>  答：



## 五、Maven

### 1、配置环境变量

在环境变量中添加：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForWeb/maven环境变量1.png)

在Path中添加：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForWeb/maven环境变量2.png)

此时在，cmd中输入：`mvn -version` 有版本信息，环境变量就配置好了



### 2、setting.xml

- 设置下载的jar包，存储本地的位置：

```xml
<localRepository>E:\RepMaven</localRepository>
```

- 设置镜像地址(由于maven是国外的，如果直接从maven官网去下载，下载速度极慢)：

```xml
<mirrors>
    <mirror>
        <id>alimaven</id>
        <name>aliyun maven</name>
        <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
        <mirrorOf>central</mirrorOf>        
    </mirror>
</mirrors>
```



### 3、Idea设置maven

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForWeb/idea设置maven.png)



### 4、解决Maven导出资源配置问题 (重点)

标准的Maven项目都会有一个resources目录来存放我们所有的资源配置文件，但是我们往往在项目中不仅仅会把所有的资源配置文件都放在resources中，同时我们也有可能放在项目中的其他位置，那么默认的maven项目构建编译时就不会把我们其他目录下的资源配置文件导出到target目录中，就会导致我们的资源配置文件读取失败，从而导致我们的项目报错出现异常，比如说尤其我们在使用MyBatis框架时，往往Mapper.xml配置文件都会放在dao包中和dao接口类放在一起的,那么执行程序的时候，其中的xml配置文件就一定会读取失败，不会生成到maven的target目录中，所以我们要在项目的pom.xml文件中进行设置，并且我建议大家，每新建一个maven项目，就把该设置导入pom.xml文件中，以防不测！！！

```xml
<build>
    <resources>
        
        <resource>
        	<directory>src/main/resources</directory>
            <includes>
                <include>**/*.properties</include>
                <include>**/*.xml</include>
            </includes>
            <filtering>true</filtering>
        </resource>
        
        <resource>
            <directory>src/main/java</directory>
            <includes>
                <include>**/*.properties</include>
                <include>**/*.xml</include>
            </includes>
            <filtering>true</filtering>
        </resource>
    
    </resources>
</build>
```



