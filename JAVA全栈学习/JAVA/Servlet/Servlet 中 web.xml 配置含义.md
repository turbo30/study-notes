# Servlet 中 web.xml 配置含义

代码如下：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
  
    <servlet>
        <!-- 描述性的文字说明，没实际意义，一般情况下直接删除掉 -->
        <description>servletDemo</description>
        <display-name>servletDemo</display-name>
        <!--自定义，一般为类名-->
        <servlet-name>servletDemo</servlet-name>
        <!--一定是package + .类名-->
        <servlet-class>day08_servlet.ServletDemo</servlet-class>
    </servlet>
    <!--给Servlet提供（映射)一个可供客户端访问的URI-->
    <servlet-mapping>
        <!--和servlet中的name必须相同-->
        <servlet-name>servletDemo</servlet-name>
        <!-- servlet的映射路径 -->
        <url-pattern>/servlet</url-pattern>
    </servlet-mapping>
    
</web-app>
```

这里要注意的是servlet-mapping的url-pattern中的这个映射路径 / 一定不能忘记！！！

补充：url-pattern: *.xxx  以*.字符串的请求都可以访问 注：不要加/  　　　　   

url-pattern: /*  任意字符串都可以访问  　　　   　

url-pattern： /xxx/* 以/xxx开头的请求都可以访问  　　

优先级从高到低：绝对匹配、/开头匹配 、扩展名方式匹配