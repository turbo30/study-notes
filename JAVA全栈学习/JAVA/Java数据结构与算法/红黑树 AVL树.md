旋转本身是O(1)操作，因为只需要调整指针的指向。



# 红黑树(理解原理)

[红黑树在线演示网址][https://rbtree.phpisfuture.com/]

[红黑树的插入、删除][https://blog.csdn.net/qq_36610462/article/details/83304175]

红黑树是自平衡的二叉查找树，非完全平衡树。

能保证在最坏情况下，基本的动态几何操作的时间均为**O（lgn）**

**在treeset类和treemap类用到了红黑树，并且java8加入的hashmap加入了红黑树，hashset、linkedhashset也是调用了hashmap，因此在java8也用到了红黑树。**

由于二叉树，可能会存在极端情况，如果每次插入的数都是比上一个数大，那么会呈现

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二叉树缺点.png)

如上情况，该情况会使得链表深度增加，并且查询起来很消耗时间。为了弥补该缺点，因此衍生出了，红黑树。



## 一、<u>红黑树规则</u>

- 节点是红色或是黑色

- 根节点必须为黑色
- 两个红色节点不能相邻（红色节点的子节点一定是黑色）
- 从任意节点到叶子节点的每条路径包含的黑色节点数目相同（黑色高度）
- 每个叶子节点(NULL节点，空节点)是黑色

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/红黑树.gif)



**当前节点与个节点的关系：**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/关系.png)

## 二、<u>红黑树特点</u>

- 红黑树从根节点到叶子节点的最长路径不会超过最短路径的2倍。





## 二、红黑树插入

一个新值插入**必须默认为红色**，因为默认为红色时，如果破坏了红黑树的规则，调整范围较。



### 1、叔叔节点为红色，调整颜色

插入[200，100，300，50]

- **首先以此插入，200，100，300**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二101.png" style="float:left;" />

很简单，插入200时，因为为根节点，所以为黑色，再插入100，因为100比200小所以走左子树，再插入300，因为比200大，所以走右子树。同时因为新插入的值为红色，所以根节点的两个子节点都为红色。

- **现在插入50**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二102.png" style="float:left;" />

此时就需要进行调整了，因为违反了红黑树**两个红色节点不能相邻**的规则。再观察当前节点的**叔叔节点为红色**，因此只是**调整节点的颜色**。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二103.png" style="float:left;" />

颜色调整好了，但是发现根节点变成了红色，这里又违反了红黑树根节点必须为黑色的规则，因此需要将根节点设置成黑色。

- **插入完成的最终结果**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二104.png" style="float:left;" />



#### 总结

==两个红色节点相邻，叔叔节点为红色，只调整颜色。==



### 2、叔叔节点为黑色，旋转

旋转都是以父节点为中心，进行旋转。



####  1）形为“/”，右旋转

在之前的红黑树中，再插入一个45的记录

- 插入45

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二201.png" style="float:left;" />

此时发现，有**两个红色节点相邻**，违反红黑树的规则。再观察**叔叔节点为黑色**(叔叔节点为null叶子节点，null叶子节点都是黑色)，因此需要**旋转**做调整。

- 因为当前节点，父节点，祖父节点形成“/”形状，做右旋转

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二202.png" style="float:left;" />

这里进行了右旋转，向右进行选择，此时当前节点的父节点，带着当前节点，直接向上晋升一个等级

- 此时旋转完之后，调整颜色。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二203.png" style="float:left;" />



##### 补充案例

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二204.png" style="float:left;" />

- 向该红色树中，添加35

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二205.png" style="float:left;" />

观察到，两个红色节点相邻，叔叔为红色节点，因此只需要调整颜色即可。**(注意：只针对当前节点，父节点，祖父节点，叔叔节点进行调整)**

- 调整之后

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二206.png" style="float:left;" />

调整完当前节点关系的节点的颜色之后，再次判断43与父节点是否是同一个颜色，发现是两个红色节点相邻，这里又违反了红黑树的规则。就以43元素为当前节点，它的叔叔节点为黑色，因此需要旋转，又因为当前节点，父节点，祖父节点形成“/”，因此需要右旋转。

- 旋转之后

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二207.png" style="float:left;" />

父节点带着子节点晋升一级。父节点成了根节点，又因为根节点必须为黑色，因此要将根节点设置成黑色。

- 最终结果

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二208.png" style="float:left;" />



##### 总结

==两个红色相邻，叔叔节点为黑色，并且当前节点，父节点，祖父节点形成“/”形状，做右旋转，再调整颜色==

**形为“\”，左旋转。与右旋转雷同。**



#### 2）形为“<”，左旋转再右旋转

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二209.png" style="float:left;" />

- 插入47

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二210.png" style="float:left;" />

插入47之后，发现两个红色相邻，违反红黑树规则，并且叔叔节点为黑色，因此需要旋转调整。应为当前节点，父节点，祖父节点形成“<”，因此需要先左旋转，再右旋转。

- 左旋转

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二211.png" style="float:left;" />

左旋转之后，形成了很熟悉的“/”情况。以父节点为中心进行右旋转。

- 右旋转

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二212.png" style="float:left;" />

旋转之后，进行颜色调整。

- 颜色调整，形成最终结果

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二213.png" style="float:left;" />



##### 总结

==两个红色节点相邻，并当前节点，父节点，祖父节点形成“<”，叔叔节点为黑色，那么就进行左旋转，再右旋转，调整颜色==

前节点，父节点，祖父节点形成“>”，且叔叔节点为黑色，那么就进行右旋转，再左旋转，调整颜色



## 三、红黑树删除

### 1、删除情况

1. 当被删除元素为红时，对红黑树规则没什么影响，直接删除。 
2. 当被删除元素为黑且为根节点时，直接删除。 



#### 补充

- 删除53节点

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/三101.png" style="float:left;" />

在删除53节点时，会去找53节点左子树中最大的数，最大的数是52，会将52节点替换掉53节点。

- 替换删除操作

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/三102.png" style="float:left;" />



<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/三103.png" style="float:left;" />

为了保证从任意节点到叶子节点(Null节点)的每条路径包含的黑色节点数目相同，因此52替换掉删除53的时候，只是互换值，并没有互换颜色，最终将红色节点删除。



- 继续删除52节点

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/三104.png" style="float:left;" />

为了保证从任意节点到叶子节点(Null节点)的每条路径包含的黑色节点数目相同，只是删除的节点的值，然后将左子树最大的值替换过来，然后将左子树最大的值的节点删除，这里最终删除的就是黑色的节点。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/三105.png" style="float:left;" />

这里删除完之后，发现不符合红黑树的从任意节点到叶子节点(Null节点)的每条路径包含的黑色节点数目相同的规则。因为从`100->51->50->叶子节点 `只经历了2个黑色节点，而其他路径都是经历了3个黑色节点，因此50红色节点要与40黑色节点进行颜色更换。

- 最终结果

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/三106.png" style="float:left;" />



#### 总结

其实红黑树的删除操作与二叉树的删除操作一样。删除哪个节点，就让该节点的左子树最大的节点替换该节点。只不过红黑树需要调整平衡而已。

只有当==最终消失的那个节点的颜色是黑色==时需要==平衡调整==（删除最终消失的节点颜色为红色就不需要平衡调整，没破坏红黑树规则），因消失黑色节点后，会违反从任意节点到叶子节点的每条路径包含的黑色节点数目相同的规则。

==被删除元素和后继元素互换只是值得互换，并不互换颜色，这个要注意==



## 四、红黑树调整规律总结

插入一个新值，必须为红色，插入之后进行前后颜色判断，总之每次只对当前节点、父节点、祖父节点、叔叔节点进行旋转调整或者颜色调整，叔叔节点的颜色决定了是否旋转，叔叔为红色不旋转调整颜色，叔叔为黑色旋转再调整颜色。

每次调整完颜色之后，就必须再次判断调整之前的父节点在调整之后，与它的父节点是否有红色碰撞，如果有，那么它就成为当前节点，进行判断它的叔叔颜色，进一步调整。



### 插入总结

- 两个红色节点相邻，叔叔节点为红色，只调整颜色
- 两个红色节点相邻，叔叔节点为黑色，并且当前节点、父节点、祖父节点形成`/`形状，做右旋转，再调整颜色；如果为`\`形状，做左旋转，再调整颜色
- 两个红色节点相邻，叔叔节点为黑色，当前节点、父节点、祖父节点形成`<`，那么就进行左旋转，再右旋转，调整颜色



### 删除总结

只有当==最终消失的那个节点的颜色是黑色==时需要==平衡调整==（删除最终消失的节点颜色为红色就不需要平衡调整，没破坏红黑树规则），因消失黑色节点后，会违反从任意节点到叶子节点的每条路径包含的黑色节点数目相同的规则。在调整颜色平衡的时候，可能会牵扯到旋转。

==被删除元素和后继元素互换只是值得互换，并不互换颜色，这个要注意==



### 问题

1，红黑树的性质？路径特点？

2，红黑树什么时候旋转，什么时候变色？

3，红黑树的时间复杂度？旋转的时间复杂度？



# AVL树(理解原理)

## 一、AVL树原理

AVL树是最先发明的自平衡二叉查找树。在AVL树中**任何节点的两个子树的高度最大差别为1**，所以它也被称为**高度平衡树**。**增加和删除可能需要通过一次或多次树旋转来重新平衡这个树**。AVL树得名于它的发明者G. M. Adelson-Velsky和E. M. Landis，他们在1962年的论文《An algorithm for the organization of information》中发表了它。



## 二、AVL树性质

- 任何节点的两个子树的高度差最大差别为1
- 增加和删除可能需要一次或多次树旋转来重新得到一个平衡树





