#  树 数据结构

**为什么要引入树？**

- 数组存储方式：由于数组访问元素速度快，开可以使用二分查找提高速度，但是在按一定顺序插入新元素时，当数组满了再插入新元素时，效率低速度慢。
- 链式存储方式：链式存储插入很方便，删除效率也高。但是在检索时，效率也比较低，每次都需要从头遍历到尾。
- 树结构存储方式：能提高数据存储，读取效率。既可以保证检索速度，也可以保证数据的插入、删除、修改的速度。



## 一、二叉树



### 1、概念



#### 1）二叉树

- 每个节点**最多**只能有两个子节点的一种形式称为二叉树
- 二叉树的子节点分为左节点和右节点



#### 2）满二叉树、完全二叉树

**满二叉树：**所有叶子节点都在最后一层，并且总节点数为2^n^-1(n为层数)

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/满二叉树.png" style="float:left" />



**完全二叉树：**叶子节点在最后一层和倒数第二层，并且最后一层的叶子节点在左边连续，倒数第二层叶子节点在右边连续

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/完全二叉树.png" style="float:left" />





#### 3）前序遍历、中序遍历、后序遍历

前序遍历：先输出**父节点**、再输出左子树、右子树

中序遍历：先遍历左子树、再遍历**父节点**、再遍历右子树

后序遍历：先遍历左子树、再遍历右子树、再遍历**父节点**

tips：遍历的顺序根据父节点的顺序而定



### 2、前中后序遍历

#### 代码实现

```java
public class BinaryTreeDemo {

    public static void main(String[] args) {
        // 创建节点
        EmpNode node1 = new EmpNode(1,"heroc");
        EmpNode node2 = new EmpNode(2,"lucy");
        EmpNode node3 = new EmpNode(3,"smith");
        EmpNode node4 = new EmpNode(4,"tom");
        EmpNode node5 = new EmpNode(5,"jack");

        // 创建树
        node1.setLeft(node2);
        node2.setLeft(node3);
        node1.setRight(node4);
        node4.setLeft(node5);

        // 确定根节点，遍历树
        BinaryTree rootNode = new BinaryTree(node1);
//        System.out.println("前序：");
//        rootNode.perOrder();
//        System.out.println("中序：");
//        rootNode.centerOrder();
//        System.out.println("后序：");
//        rootNode.afterOrder();

        // 前序遍历查询
        EmpNode empNode = rootNode.perOrderFind(5);
        if (empNode!=null){
            System.out.println(empNode.toString());
        }

        // 中序遍历查询
        empNode = rootNode.perOrderFind(2);
        if (empNode!=null){
            System.out.println(empNode.toString());
        }

        // 后序遍历查询
        empNode = rootNode.afterOrderFind(4);
        if (empNode!=null){
            System.out.println(empNode.toString());
        }
    }
}

// 创建节点
class EmpNode{
    private int id;
    private String name;
    private EmpNode left;
    private EmpNode right;

    public EmpNode(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EmpNode getLeft() {
        return left;
    }

    public void setLeft(EmpNode left) {
        this.left = left;
    }

    public EmpNode getRight() {
        return right;
    }

    public void setRight(EmpNode right) {
        this.right = right;
    }

    @Override
    public String toString() {
        return "EmpNode{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    // 前序遍历
    public void perOrder(){
        System.out.println(this.toString());
        if (this.left!=null){
            this.left.perOrder();
        }
        if (this.right!=null){
            this.right.perOrder();
        }
    }

    // 中序遍历
    public void centerOrder(){
        if (this.left!=null){
            this.left.centerOrder();
        }
        System.out.println(this.toString());
        if (this.right!=null){
            this.right.centerOrder();
        }
    }

    // 后续遍历
    public void afterOrder(){
        if (this.left!=null){
            this.left.afterOrder();
        }
        if (this.right!=null){
            this.right.afterOrder();
        }
        System.out.println(this.toString());
    }

    // 前序遍历查询
    public EmpNode perOrderFind(int id){
        if (this.id == id){
            return this;
        }

        EmpNode empNode = null;
        if (this.left!=null){
            empNode = this.left.perOrderFind(id);
        }
        if (empNode!=null){
            return empNode;
        }

        if (this.right!=null){
            empNode = this.right.perOrderFind(id);
        }
        if (empNode!=null){
            return empNode;
        }

        return null;
    }

    // 中序遍历查询
    public EmpNode centerOrderFind(int id){
        EmpNode empNode = null;
        // 向左查找
        if (this.left!=null){
            empNode = this.left.centerOrderFind(id);
        }
        if (empNode!=null){ // 如果empNode不等null，说明找到了，就返回结果
            return empNode;
        }

        // 当前节点
        if (this.id == id){
            return this;
        }

        // 向右查找
        if (this.right != null){
            empNode = this.right.centerOrderFind(id);
        }
        if (empNode!=null){
            return empNode;
        }

        // 都没找到返回null
        return empNode;
    }

    // 后序遍历查询
    public EmpNode afterOrderFind(int id){
        EmpNode empNode = null;
        // 向左查找
        if (this.left!=null){
            empNode = this.left.centerOrderFind(id);
        }
        if (empNode!=null){ // 如果empNode不等null，说明找到了，就返回结果
            return empNode;
        }

        // 向右查找
        if (this.right != null){
            empNode = this.right.centerOrderFind(id);
        }
        if (empNode!=null){
            return empNode;
        }

        // 当前节点
        if (this.id == id){
            return this;
        }

        return empNode;
    }

}

// 创建根节点
class BinaryTree{
    private EmpNode root;

    public BinaryTree(EmpNode root) {
        this.root = root;
    }

    public void perOrder(){
        if (root!=null){
            root.perOrder();
        }else {
            System.out.println("树为空");
        }
    }
    public void centerOrder(){
        if (root!=null){
            root.centerOrder();
        }else {
            System.out.println("树为空");
        }
    }
    public void afterOrder(){
        if (root!=null){
            root.afterOrder();
        }else {
            System.out.println("树为空");
        }
    }
    public EmpNode perOrderFind(int id){
        if (root!=null){
            return root.perOrderFind(id);
        }else {
            System.out.println("树为空");
            return null;
        }
    }
    public EmpNode centerOrderFind(int id){
        if (root!=null){
            return root.centerOrderFind(id);
        }else {
            System.out.println("树为空");
            return null;
        }
    }
    public EmpNode afterOrderFind(int id){
        if (root!=null){
            return root.afterOrderFind(id);
        }else {
            System.out.println("树为空");
            return null;
        }
    }
}
```



### 3、顺序存储二叉树

顺序存储二叉树，是将二叉树顺序存储到数组中，并在遍历数组的时候依旧按照前序遍历、中序遍历、后序遍历。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/顺序存储二叉树.png" style="float:left" />

**顺序存储二叉树的特点：**

- 顺序二叉树通常只考虑完全二叉树
- 从0开始，第n个元素的**左子节点**为第**2*n+1**个元素
- 从0开始，第n个元素的**右子节点**为第**2*n+2**个元素
- 从0开始，第n个元素的**父节点**为第**(n-1)/2**个元素

从0开始计算，为了与数组索引保持一致。



#### 代码实现

```java
public class ArrayBinaryTreeDemo {
    public static void main(String[] args) {
        int arr[] = {1,2,3,4,5,6,7}; // 这是一个二叉树从根节点依次顺序存储的数组
        ArrayBinaryTree arrayBinaryTree = new ArrayBinaryTree(arr);
        System.out.println("前序遍历：");
        arrayBinaryTree.perOrder();
        System.out.println("\n中序遍历：");
        arrayBinaryTree.infixOrder();
        System.out.println("\n后序遍历：");
        arrayBinaryTree.suffixOrder();
    }
}

class ArrayBinaryTree{
    int arr[];

    public ArrayBinaryTree(int[] arr) {
        this.arr = arr;
    }

    // 前序遍历顺序存储的二叉树数组
    public void perOrder(){
        perOrder(0);
    }
    public void perOrder(int index){
        System.out.print(arr[index]+" ");

        if ((2*index+1) < arr.length){
            perOrder(2*index+1);
        }

        if ((2*index+2) < arr.length){
            perOrder(2*index+2);
        }
    }

    // 中序遍历顺序存储的二叉树数组
    public void infixOrder(){
        infixOrder(0);
    }
    public void infixOrder(int index){
        if ((2*index+1) < arr.length){
            infixOrder(2*index+1);
        }

        System.out.print(arr[index]+" ");

        if ((2*index+2) < arr.length){
            infixOrder(2*index+2);
        }
    }

    // 后序遍历顺序存储的二叉树数组
    public void suffixOrder(){
        suffixOrder(0);
    }
    public void suffixOrder(int index){
        if ((2*index+1) < arr.length){
            suffixOrder(2*index+1);
        }

        if ((2*index+2) < arr.length){
            suffixOrder(2*index+2);
        }

        System.out.print(arr[index]+" ");
    }
}
```



### 3、线索化二叉树

由于树在最后的叶子节点都有左右指针是空的，即总共有n个节点的二叉链表中含有**n+1**个空指针域。利用空指针域存放指向当前节点在某种遍历下的前驱或后继节点。这种附加的指针指向的操作就是线索。

根据线索的不同，线索二叉树又可分为**前序线索二叉树**、**中序线索二叉树**、**后序线索二叉树**。

> 一个节点的前一个节点，称为前驱节点
>
> 一个节点的后一个节点，称为后继节点



<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/中序线索二叉树.png" style="float:left" />

**中序线索二叉树遍历顺序，infixThreadList()实现：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/线索二叉树遍历顺序.png" style="float:left" />

```java
public class ThreadBinaryTreeDemo {
    public static void main(String[] args) {
        Node node1 = new Node(1);
        Node node2 = new Node(3);
        Node node3 = new Node(6);
        Node node4 = new Node(8);
        Node node5 = new Node(10);
        Node node6 = new Node(14);

        node1.setLeft(node2);
        node1.setRight(node3);
        node2.setLeft(node4);
        node2.setRight(node5);
        node3.setLeft(node6);

        ThreadBinaryTree threadBinaryTree = new ThreadBinaryTree(node1);
//        threadBinaryTree.perThread(); // 前序线索化二叉树
//        threadBinaryTree.perOrder(); // 通过前序遍历

        threadBinaryTree.infixThread(); // 中序线索化二叉树
//        threadBinaryTree.infixOrder(); // 通过中序遍历
        threadBinaryTree.infixThreadList();
        System.out.println("id为8节点的后继节点为："+node4.getRight());
        System.out.println("id为10节点的前驱节点为："+node5.getLeft());
        System.out.println("id为10节点的后继节点为："+node5.getRight());
        System.out.println("id为14节点的前驱节点为："+node6.getLeft());
        System.out.println("id为14节点的后继节点为："+node6.getRight());

    }
}

class Node{
    private int id;
    private Node left;
    private Node right;
    // 设置左右节点的类型，0为左节点或右节点，1为前驱节点或后继节点
    private int leftType;
    private int rightType;

    public Node(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public int getLeftType() {
        return leftType;
    }

    public void setLeftType(int leftType) {
        this.leftType = leftType;
    }

    public int getRightType() {
        return rightType;
    }

    public void setRightType(int rightType) {
        this.rightType = rightType;
    }

    @Override
    public String toString() {
        return "Node{" +
                "id=" + id +
                ", leftType=" + leftType +
                ", rightType=" + rightType +
                '}';
    }

    // 前序遍历：只遍历左子节点和右子节点，不遍历前驱节点和后继节点
    public void perOrder(){
        System.out.println(this.toString());

        if (this.left!=null && this.getLeftType()==0){
            this.left.perOrder();
        }

        if (this.right!=null && this.getRightType()==0){
            this.right.perOrder();
        }
    }

    // 中序遍历：只遍历左子节点和右子节点，不遍历前驱节点和后继节点
    public void infixOrder(){
        if (this.left!=null && this.leftType == 0){
            this.left.infixOrder();
        }

        System.out.println(this.toString());

        if (this.right!=null && this.rightType == 0){
            this.right.infixOrder();
        }
    }

    // 后序遍历：只遍历左子节点和右子节点，不遍历前驱节点和后继节点
    public void suffixOrder(){
        if (this.left!=null && this.leftType == 0){
            this.left.suffixOrder();
        }
        if (this.right!=null && this.rightType == 0){
            this.right.suffixOrder();
        }
        System.out.println(this.toString());
    }
}

class ThreadBinaryTree{
    private Node root;
    // 用于记录前驱节点，每要遍历下一个节点时，
    // 就需要将当前节点记录下来，作为下一个节点的前驱节点
    // 如果前驱节点的右子节点为空，那么就可以设置前驱节点的后继节点
    // 后继节点也就是当前节点
    private Node pre;

    public ThreadBinaryTree(Node node) {
        this.root = node;
    }

    // 创建一个前序索引
    public void perThread(){
        perThread(root);
    }
    public void perThread(Node node){
        if (node == null){
            return;
        }

        // 处理当前节点
        // 先处理左节点，如果左子节点为空，那么就将上一个节点设置为左节点的前驱节点，
        // 这里不判断pre是否为空，是没有关系的，如果pre为空，说明该节点是线索化的第一个节点
        // 有利于根据线索指针进行遍历
        if (node.getLeft() == null){
            node.setLeft(pre);
            node.setLeftType(1);
        }
        // 由于后继节点需要遍历到下一个节点才能确定，所以，遍历到下一个节点时
        // 才能设置前驱节点的右子节点指向当前的node
        if (pre!=null && pre.getRight() == null){
            pre.setRight(node);
            pre.setRightType(1);
        }
        // 用于记录前驱节点，每要遍历下一个节点时，
        // 就需要将当前节点记录下来，作为下一个节点的前驱节点
        pre = node;

        // 获取左节点，一定要判断是否是原节点，否则会陷入死循环
        if (node.getLeftType()==0){
            perThread(node.getLeft());
        }

        // 获取右节点，一定要判断是否是原节点，否则会陷入死循环
        if (node.getRightType()==0){
            perThread(node.getRight());
        }
    }

    // 创建一个中序序列化二叉树
    public void infixThread(){
        infixThread(root);
    }
    public void infixThread(Node node){
        if (node==null){
            return;
        }

        if (node.getLeftType() == 0){
            infixThread(node.getLeft());
        }

        if (node.getLeft() == null){
            node.setLeft(pre);
            node.setLeftType(1);
        }
        if (pre!=null && pre.getRight() == null){
            pre.setRight(node);
            pre.setRightType(1);
        }
        pre = node;

        if (node.getRightType()==0){
            infixThread(node.getRight());
        }
    }

    // 创建一个后序线索化
    public void suffixThread(){
        suffixThread(root);
    }
    public void suffixThread(Node node){
        if (node==null){
            return;
        }
        if (node.getLeftType() == 0){
            suffixThread(node.getLeft());
        }
        if (node.getRightType() == 0){
            suffixThread(node.getRight());
        }

        if (node.getLeft() == null){
            node.setLeft(pre);
            node.setLeftType(1);
        }
        if (pre!=null && pre.getRight() == null){
            pre.setRight(node);
            pre.setRightType(1);
        }
        pre = node;
    }

    // 根据线索指针进行前序线索二叉树遍历
    public void perThreadList(){
        Node node = root;
        while (node!=null){
            while (node.getLeftType() == 0){
                System.out.println(node.toString());
                node = node.getLeft();
            }
            System.out.println(node.toString());
            while (node.getRightType()==1){
                node = node.getRight();
                System.out.println(node.toString());
            }
            node = node.getRight();
        }
    }

    // 根据线索指针进行中序线索二叉树遍历
    public void infixThreadList(){
        // 定义一个变量，存储当前节点
        Node node = root;
        while (node != null){
            // 第一次是要找到左节点为空，并且左节点类型为1，也就是中序线索化的二叉树第一个节点
            while (node.getLeftType() == 0){
                node = node.getLeft();
            }
            // 找到了就输出
            System.out.println(node.toString());

            while (node.getRightType() == 1){
                node = node.getRight();
                System.out.println(node.toString());
            }
            node = node.getRight();
        }
    }

    // 根据线索指针进行后序线索二叉树遍历
    public void suffixThreadList(){

    }

    // 前序遍历形式遍历线索二叉树
    public void perOrder(){
        if (root!=null){
            root.perOrder();
        }else {
            System.out.println("二叉树为空");
        }
    }

    // 中序遍历形式遍历线索二叉树
    public void infixOrder(){
        if (root!=null){
            root.infixOrder();
        }else {
            System.out.println("二叉树为空");
        }
    }

    // 后序遍历形式遍历线索二叉树
    public void suffixOrder(){
        if (root!=null){
            root.suffixOrder();
        }else {
            System.out.println("二叉树为空");
        }
    }
}
```

结果：

```
Node{id=8, leftType=0, rightType=1}
Node{id=3, leftType=0, rightType=0}
Node{id=10, leftType=1, rightType=1}
Node{id=1, leftType=0, rightType=0}
Node{id=14, leftType=1, rightType=1}
Node{id=6, leftType=0, rightType=0}
id为8节点的后继节点为：Node{id=3, leftType=0, rightType=0}
id为10节点的前驱节点为：Node{id=3, leftType=0, rightType=0}
id为10节点的后继节点为：Node{id=1, leftType=0, rightType=0}
id为14节点的前驱节点为：Node{id=1, leftType=0, rightType=0}
id为14节点的后继节点为：Node{id=6, leftType=0, rightType=0}
```



## 二、树结构的实际应用

### 1、堆排序

<见排序算法>



## 三、哈夫曼树

哈夫曼树又称最优二叉树，是一种带权路径长度最短的二叉树。所谓树的带权路径长度，就是树中所有的叶结点的权值乘上其到根结点的路径长度（若根结点为0层，叶结点到根结点的路径长度为叶结点的层数）。

==带权路径总和最小的就是哈夫曼树==。即 **WPL=叶子节点权\*路径+...+叶子节点权*路径**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/哈夫曼树.png" style="float:left" />

中间的树就是哈夫曼树。



### 1、创建哈夫曼树

**思路分析：**

- 将数列从小到大排序，此时每个数据就是一个节点
- 取出前两个节点，作为子节点，计算出父节点的权值(就是两个节点的权值和)
- 下一步就是将计算出的新父节点的权值放入数列中，重新排序，返回第二步
- 往复，最终会得到一个哈夫曼树

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/创建哈夫曼树思路.png" style="float:left" />

```java
public class HuffmanTree {
    public static void main(String[] args) {
        int arr[] ={3,6,15,20};
        Node huffmanTree = createHuffmanTree(arr);
        if (huffmanTree!=null){
            huffmanTree.preOrder();
        }
    }

    public static Node createHuffmanTree(int[] arr){
        // 创建一个集合，存入创建的节点
        List<Node> nodeList = new ArrayList<>();
        for (int item : arr) {
            nodeList.add(new Node(item));
        }

        // 因为每次都会remove一些节点，最终会在list中剩下一个节点，这个节点就是根节点
        while (nodeList.size() > 1){
            // 从小到达排序list
            Collections.sort(nodeList);

            // 取出前两个最小的，第一个作为左节点，第二个作为右节点
            Node leftNode = nodeList.get(0);
            Node rightNode = nodeList.get(1);

            // 将权重+路径和赋值给父节点，将父节点的左右节点挂上
            Node parentNode = new Node(leftNode.getValue()+rightNode.getValue());
            parentNode.setLeft(leftNode);
            parentNode.setRight(rightNode);

            // 移除最小的两个节点，将父节点放入list集合中，进行下一轮
            nodeList.remove(leftNode);
            nodeList.remove(rightNode);
            nodeList.add(parentNode);
        }
        // 返回最终的根节点
        return nodeList.get(0);
    }
}

class Node implements Comparable<Node>{
    private int value;
    private Node left;
    private Node right;

    public Node(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                '}';
    }

    // 从小到大排序
    @Override
    public int compareTo(Node node) {
        return this.value - node.value;
    }

    // 前序遍历
    public void preOrder(){
        System.out.println(this.toString());
        if (this.left!=null){
            this.left.preOrder();
        }
        if (this.right!=null){
            this.right.preOrder();
        }
    }
}
```

结果：

```
Node{value=44}
Node{value=20}
Node{value=24}
Node{value=9}
Node{value=3}
Node{value=6}
Node{value=15}
```



### 2、哈夫曼编码

<见算法>



## 四、二叉排序树 BST

二叉排序树：BTS(Binary Sort Tree)，对于二叉排序树的任何一个非叶子节点，要求左子节点的值比当前节点的值小，右子节点的值比当前节点的值大。如果有相同的值，可以将该节点放在左子节点或右子节点。

==二叉排序树的中序遍历，结果就是从小到大的排列。==

二叉排序树的特点：

- 左子树所有的值一定比根节点小，右子树所有的值一定比根节点大



### 1、二叉排序树 添加

#### 1）思路分析

- 添加的node的值小于当前节点的值，那么就判断当前节点的左节点是否为空；为空就直接挂在当前节点的左边，不为空就继续往左节点往下找
- 添加的node的值大于等于当前节点的值，那么就判断当前节点的右节点是否为空；为空就直接挂在当前节点的右边，不为空就继续往右节点往下找



#### 2）代码实现

```java
public class BinarySortTreeDemo {
    public static void main(String[] args) {
        int arr[] = {7,3,10,12,5,1,9};
        BinarySortTree binarySortTree = new BinarySortTree();
        for (int i = 0; i < arr.length; i++) {
            binarySortTree.add(new BstNode(arr[i]));
        }
        binarySortTree.infixOrder();
    }
}

// 二叉排序树
class BinarySortTree{
    private BstNode root;

    public BinarySortTree() {
    }

    public BinarySortTree(BstNode root) {
        this.root = root;
    }

    public void infixOrder(){
        if (root==null){
            return;
        }else {
            root.infixOrder();
        }
    }

    public void add(BstNode node){
        if (root==null){
            root = node;
        }else {
            root.add(node);
        }
    }
}

// 节点
class BstNode{
    int value;
    BstNode left;
    BstNode right;

    public BstNode(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BstNode{" +
                "value=" + value +
                '}';
    }

    public void infixOrder(){
        if (this.left!=null){
            this.left.infixOrder();
        }
        System.out.println(this.toString());
        if (this.right!=null){
            this.right.infixOrder();
        }
    }

    // 二叉排序树添加
    public void add(BstNode node){
        if (node == null){
            return;
        }
        // 添加的node的值小于当前节点的值，那么就判断当前节点的左节点是否为空
        // 为空就直接挂在当前节点的左边，不为空就继续往左节点往下找
        if (node.value < this.value){
            if (this.left!=null){
                this.left.add(node);
            }else {
                this.left = node;
            }
        }else {// 添加的node的值大于等于当前节点的值，那么就判断当前节点的右节点是否为空
               // 为空就直接挂在当前节点的右边，不为空就继续往右节点往下找
            if (this.right!=null){
                this.right.add(node);
            }else {
                this.right = node;
            }
        }
    }
}
```

结果：

```
BstNode{value=1}
BstNode{value=3}
BstNode{value=5}
BstNode{value=7}
BstNode{value=9}
BstNode{value=10}
BstNode{value=12}
```





### 2、二叉排序树 删除

#### 1）思路分析

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/二叉排序树.png" style="float:left" />

删除有三种情况：

1. **目标节点没有任何子节点**

   定位到目标节点的父节点，如果目标节点是其父节点的右子节点，那么将其父节点的右子节点置为空即可；如果目标节点是父节点的左子节点，那么将其父节点的左子节点置为空即可。

2. **目标节点有一个子节点**

   定位目标节点的父节点，如果目标节点是其父节点的左子节点，那么将其父节点的左子节点指向目标节点的子节点；如果目标节点是其父节点的右子节点，那么将其父节点的右子节点指向目标节点的子节点；如果目标节点的父节点为空，说明删除的目标节点是根节点，那么直接将根节点指向目标节点的子节点即可；。

3. **目标节点有两个子节点**

   定位目标节点，找到目标节点的右子节点中最小的节点，将该最小的节点临时保存下来，并删除，将目标节点重新赋值为该最小的节点。



#### 2）代码实现

```java
public class BinarySortTreeDemo {
    public static void main(String[] args) {
        int arr[] = {7,3,10,12,5,1,9,2};
        BinarySortTree binarySortTree = new BinarySortTree();
        for (int i = 0; i < arr.length; i++) {
            binarySortTree.add(new BstNode(arr[i]));
        }
        System.out.println("原二叉排序树中序遍历结果：");
        binarySortTree.infixOrder();
        System.out.println("删除有一个子节点的目标节点：1");
        binarySortTree.del(1);
        System.out.println("删除有两个子节点的目标节点：10");
        binarySortTree.del(10);
        System.out.println("删除没有子节点的目标节点：2");
        binarySortTree.del(2);
        System.out.println("最终中序遍历结果：");
        binarySortTree.infixOrder();
    }
}

// 二叉排序树
class BinarySortTree{
    private BstNode root;

    public BinarySortTree() {
    }

    public BinarySortTree(BstNode root) {
        this.root = root;
    }

    public void infixOrder(){
        if (root==null){
            return;
        }else {
            root.infixOrder();
        }
    }

    public void add(BstNode node){
        if (root==null){
            root = node;
        }else {
            root.add(node);
        }
    }

    // 获得需要删除的目标节点
    public BstNode getDelTargetNode(int value){
        if (root==null){
            return null;
        }else {
            return root.getDelTargetNode(value);
        }
    }

    // 获得需要删除的目标节点的父节点
    public BstNode getDelTargetParentNode(int value){
        if (root==null){
            return null;
        }else {
            return root.getDelTargetParentNode(value);
        }
    }

    // 找最小值节点
    public BstNode getRightMinNode(BstNode node){
        if (node==null){
            return null;
        }
        while (node.left!=null){
            node = node.left;
        }
        del(node.value); // 删除这个节点
        return node;
    }

    // 删除节点操作
    public void del(int value){
        BstNode targetNode = getDelTargetNode(value);
        if (targetNode == null){
            return;
        }
        // 如果targetNode不为空，并且root节点没有左右节点肯定就是删除root节点，直接置空即可
        if (root.left == null && root.right == null){
            root = null;
            return;
        }
        BstNode parentNode = getDelTargetParentNode(value);
        if (targetNode.left==null && targetNode.right==null){
            // 左右节点都为空，说明是叶子节点
            if (parentNode.left!=null && parentNode.left.value == value){
                parentNode.left = null;
            }else {
                parentNode.right = null;
            }
        }else if (targetNode.left!=null && targetNode.right!=null){
            // 左右节点都不为空，说明删除的是有左右子树的节点
            BstNode rightMinNode = getRightMinNode(targetNode.right);
            targetNode.value = rightMinNode.value; // 把值赋给目标节点
        }else {
            // 说明目标节点有一个子节点
            if (targetNode.left!=null){
                if (parentNode!=null){
                    if (parentNode.left!=null && parentNode.left.value == value){
                        parentNode.left = targetNode.left;
                    }else {
                        parentNode.right = targetNode.left;
                    }
                }else {
                    root = targetNode.left;
                }
            }else {
                if (parentNode!=null){
                    if (parentNode.left!=null && parentNode.left.value == value){
                        parentNode.left = targetNode.right;
                    }else {
                        parentNode.right = targetNode.right;
                    }
                }else {
                    root = targetNode.right;
                }
            }
        }
    }
}

// 节点
class BstNode{
    int value;
    BstNode left;
    BstNode right;

    public BstNode(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BstNode{" +
                "value=" + value +
                '}';
    }

    public void infixOrder(){
        if (this.left!=null){
            this.left.infixOrder();
        }
        System.out.println(this.toString());
        if (this.right!=null){
            this.right.infixOrder();
        }
    }

    // 二叉排序树添加
    public void add(BstNode node){
        if (node == null){
            return;
        }
        // 添加的node的值小于当前节点的值，那么就判断当前节点的左节点是否为空
        // 为空就直接挂在当前节点的左边，不为空就继续往左节点往下找
        if (node.value < this.value){
            if (this.left!=null){
                this.left.add(node);
            }else {
                this.left = node;
            }
        }else {// 添加的node的值大于等于当前节点的值，那么就判断当前节点的右节点是否为空
               // 为空就直接挂在当前节点的右边，不为空就继续往右节点往下找
            if (this.right!=null){
                this.right.add(node);
            }else {
                this.right = node;
            }
        }
    }

    // 找到需要删除的目标节点
    public BstNode getDelTargetNode(int value){
        if (this.value == value){
            return this;
        }
        if (value < this.value){
            if (this.left!=null){
                return this.left.getDelTargetNode(value);
            }else {
                return null;
            }
        }else {
            if (this.right!=null){
                return this.right.getDelTargetNode(value);
            }else {
                return null;
            }
        }
    }

    // 找到需要删除的目标节点的父节点
    public BstNode getDelTargetParentNode(int value){
        // 如果当前节点的左右节点中有一个的值为value，就返回该节点，就是value值节点的父节点
        if (this.left!=null && this.left.value == value || this.right!=null && this.right.value == value){
            return this;
        }
        // 不满足以上要求，说明需要继续找，
        // 如果value小于当前节点的value，就在当前节点的左节点找，
        // 如果value大于等于当前节点的value，就在当前节点的右节点找，
        if (value < this.value){
            if (this.left!=null){
                return this.left.getDelTargetParentNode(value);
            }else {
                return null;
            }
        }else {
            if (this.right!=null){
                return this.right.getDelTargetParentNode(value);
            }else {
                return null;
            }
        }
    }
}
```

结果：

```
原二叉排序树中序遍历结果：
BstNode{value=1}
BstNode{value=2}
BstNode{value=3}
BstNode{value=5}
BstNode{value=7}
BstNode{value=9}
BstNode{value=10}
BstNode{value=12}
删除有一个子节点的目标节点：1
删除有两个子节点的目标节点：10
删除没有子节点的目标节点：2
最终中序遍历结果：
BstNode{value=3}
BstNode{value=5}
BstNode{value=7}
BstNode{value=9}
BstNode{value=12}
```



## 五、平衡二叉树 AVL

平衡二叉树是对二叉排序树的增强。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/平衡二叉树引入.png" style="float:left" />



AVL树是最先发明的自平衡二叉查找树。在AVL树中**任何节点的两个子树的高度最大差别为1**，所以它也被称为**高度平衡树**。**增加和删除可能需要通过一次或多次树旋转来重新平衡这个树**。AVL树得名于它的发明者G. M. Adelson-Velsky和E. M. Landis，他们在1962年的论文《An algorithm for the organization of information》中发表了它。

**性质：**

- 任何节点的两个子树的高度差最大差别为1
- 增加和删除可能需要一次或多次树旋转来重新得到一个平衡树



#### . 向左旋转

当右子树的高度与左子树的高度差大于1时，就应该进行左旋转，降低右子树的高度，以达到平衡状态。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/AVL左旋转.png" style="float:left" />

#### . 向右旋转

当左子树的高度与右子树的高度差大于1时，就应该进行右旋转，降低左子树的高度，以达到平衡状态。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/AVL右旋转.png" style="float:left" />

#### . 双向旋转

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/AVL双旋转.png" style="float:left" />

如果不进行调整，直接向右旋转，得到的结果依旧不是AVL树

如果是向右旋转：

- 那么就需要判断当前节点的左子节点的右子树的高度是否大于当前节点的左子节点的左子树，如果大于，就需要将左子节点的这个树，进行向左旋转的调整，调整完之后，再将当前节点向右旋转。

如果是向左旋转：

- 那么就需要判断当前节点的右子节点的左子树的高度是否大于当前节点的右子节点的右子树，如果大于，就需要将左子节点的这个树，进行向右旋转的调整，调整完之后，再将当前节点向左旋转。



#### . 代码实现

```java
public class AVLTreeDemo {
    public static void main(String[] args) {
        //int[] arr = {4,3,6,5,7,8}; // 左旋转测试
        //int[] arr = {10,12,8,9,7,6}; // 右旋转测试
        int[] arr = {10,11,7,6,8,9}; // 双旋转测试
        AvlTree avlTree = new AvlTree();
        for (int i = 0; i < arr.length; i++) {
            avlTree.add(new AvlNode(arr[i]));
        }
        avlTree.infixOrder();
        System.out.println("高："+avlTree.getRoot().getTreeHeight());
        System.out.println("右："+avlTree.getRoot().getRightTreeHeight());
        System.out.println("左："+avlTree.getRoot().getLeftTreeHeight());
        System.out.println(avlTree.getRoot().value);
    }
}

class AvlTree{
    private AvlNode root;

    public AvlTree() {
    }

    public AvlTree(AvlNode root) {
        this.root = root;
    }

    public AvlNode getRoot() {
        return root;
    }

    public void add(AvlNode node){
        if (root==null){
            root=node;
        }else {
            root.add(node);
        }
    }

    public void infixOrder(){
        if (root==null){
            return;
        }else {
            root.infixOrder();
        }
    }

}

class AvlNode{
    int value;
    AvlNode left;
    AvlNode right;

    public AvlNode(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "AvlNode{" +
                "value=" + value +
                '}';
    }

    public void infixOrder(){
        if (this.left!=null){
            this.left.infixOrder();
        }
        System.out.println(this.toString());
        if (this.right!=null){
            this.right.infixOrder();
        }
    }

    // 二叉排序树添加节点操作
    public void add(AvlNode node){
        if (node==null){
            return;
        }
        if (node.value < this.value){
            if (this.left!=null){
                this.left.add(node);
            }else {
                this.left = node;
            }
        }else {
            if (this.right!=null){
                this.right.add(node);
            }else {
                this.right = node;
            }
        }

        // 在添加的过程中，就动态调整树的高度
        // 如果当前节点的右子树与左子树的高度差大于1，则向左旋转
        if (this.getRightTreeHeight()-this.getLeftTreeHeight() > 1){
            // 在向左旋转的时候，需要判断当前节点的右子树的左子树与右子树的高度差，
            // 如果当前节点的右子树的左子树高于当前节点的右子树的右子树，那么就需要将
            // 当前节点的右子树的子树向右旋转做一个平衡调整，再让当前节点向左旋转，才能保证平衡
            if (this.right.getLeftTreeHeight() > this.right.getRightTreeHeight()){
                this.right.rightRotate();
                this.leftRotate();
            }else {
                this.leftRotate(); // 向左旋转
            }
            return; // 一定要记得return，否则会出现严重错误
        }

        // 如果当前节点的左子树与右子树的高度差大于1，则向右旋转
        if (this.getLeftTreeHeight() - this.getRightTreeHeight() > 1){
            if (this.left.getRightTreeHeight() > this.left.getLeftTreeHeight()){
                this.left.leftRotate();
                this.rightRotate();
            }else {
                this.rightRotate(); // 向右旋转
            }
        }
    }

    // 获得高度
    public int getTreeHeight(){
        return Math.max(this.left==null ? 0 : this.left.getTreeHeight(), this.right==null ? 0 : this.right.getTreeHeight())+1;
    }

    // 获取当前节点的右子树的高度
    public int getRightTreeHeight(){
        if (this.right!=null){
            return this.right.getTreeHeight();
        }
        return 0;
    }

    // 获取当前节点的左子树的高度
    public int getLeftTreeHeight(){
        if (this.left!=null){
            return this.left.getTreeHeight();
        }
        return 0;
    }

    // 向左旋转
    public void leftRotate(){
        AvlNode newNode = new AvlNode(this.value);
        newNode.left = this.left;
        newNode.right = this.right.left;
        this.value = this.right.value;
        this.right = this.right.right;
        this.left = newNode;
    }

    // 向右旋转
    public void rightRotate(){
        AvlNode newNode = new AvlNode(this.value);
        newNode.right = this.right;
        newNode.left = this.left.right;
        this.value = this.left.value;
        this.left = this.left.left;
        this.right = newNode;
    }
}
```



## 六、多路查找树 简单介绍

二叉树操作效率较高，但也存在一些问题。

- 在构建二叉树时，需要多次进行i/o操作(因为数据很可能是通过数据库或本地文件中获取的)，节点很多，造成二叉树的层数也多，会降低操作速度。

因此就提出了多路树。允许每个节点可以有更多的数据项和更多的子节点，就是多叉树。比如2-3树，2-3-4树就是多叉树，多叉树通过重新组织节点，减少树的高度，能对二叉树进行优化。



### 1、2-3树

**2-3树：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/2-3树.png" style="float:left" />

2-3树是最简单的B树。需要保证顺序。

**性质：**

- 2-3树所有的叶子节点都在同一层 (只要是B树都满足这个性质)
- 有两个子节点的节点叫做二节点，二节点要么有两个子节点，要么没有子节点
- 有三个子节点的节点叫做三节点，三节点要么有三个子节点，要么没有子节点
- 2-3树是由二节点和三节点构成的树



==二节点的左子节点的值小于二节点的值，右子节点的值大于二节点的值；三节点的左子节点的值小于三节点中最小值，中子节点的值在三节点最小值与最大值之间，右子节点的值大于三节点中最大值。==



> [2-3树原理](https://www.bilibili.com/video/BV1E4411H73v?p=143)



### 2、B树介绍

B树(B-tree)

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/B树.png" style="float:left" />

B树的阶：节点的最多子节点的个数。比如2-3树的阶是3，2-3-4树的阶是4

B树搜索性能等价于在关键字全集内做一次二分查找。



> **mysql数据库的查找，索引的数据结构是B树或B+树**



### 3、B+树介绍

B+树是在B树上进行变化得来

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/B+树.png" style="float:left" />

**B+树说明：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/B+树说明.png" style="float:left" />



### 4、B*树介绍

B*树是在B+树基础上变化得来。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForAlgorithm/B星树.png" style="float:left" />

B*树比B+树的空间使用率高。





























