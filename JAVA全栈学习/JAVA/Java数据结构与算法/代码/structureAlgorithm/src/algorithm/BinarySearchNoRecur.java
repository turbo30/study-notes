package algorithm;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/10
 * Time: 21:52
 * Description: 二分查找非递归算法
 * Version: V1.0
 */
public class BinarySearchNoRecur {
    public static void main(String[] args) {
        int arr[] = {1,2,3,4,5,6,7};
        System.out.println(search(arr, 7));
    }

    public static int search(int[] arr, int target){
        int left = 0;
        int right = arr.length - 1;
        while (left<=right){
            int mid = (left+right)/2;
            if (arr[mid] == target){
                return mid;
            }else if (arr[mid] < target){
                left = mid+1;
            }else if(arr[mid] > target){
                right = mid-1;
            }
        }
        return -1;
    }
}
