package algorithm;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/12
 * Time: 20:26
 * Description: 克鲁斯卡尔算法，最小生成树
 * 1，创建图的顶点、边的矩阵、获取边的个数
 * 2，创建一个边的类，存储两个顶点和边的权
 * 3，将所有边存储到数组中，然后根据边的权进行排序
 * 4，创建一个记录终点的数组，以及返回顶点重点的方法
 * 5，遍历边的数组，根据每一个边的顶点，获取每个顶点的终点，以及设置顶点的终点
 * 6，终点相同，说明该边加入会形成回路，因此需抛弃该边
 * 7，最终得到一个完全符合要求的最小生成树
 * Version: V1.0
 */
public class Kruskal {
    public static void main(String[] args) {
        // 顶点信息
        char vertexes[] = {'A','B','C','D','E','F','G'};
        // 顶点关系权矩阵
        int[][] weight = {
                {0,12,FIN,FIN,FIN,16,14},
                {12,0,10,FIN,FIN,7,FIN},
                {FIN,10,0,3,5,6,FIN},
                {FIN,FIN,3,0,4,FIN,FIN},
                {FIN,FIN,5,4,0,2,8},
                {16,7,6,FIN,2,0,9},
                {14,FIN,FIN,FIN,8,9,0}
        };
        // 创建一个图
        Kruskal kruskal = new Kruskal(vertexes, weight);
        // 克鲁斯卡尔算法
        kruskal.kruskalAlgorithm();

    }

    int edgesCount; // 记录边的条数
    char[] vertexData; // 记录顶点信息
    int[][] weight; // 权矩阵
    static final int FIN = Integer.MAX_VALUE; // 将没有边的两点之间定义为FIN，表示不连通

    // 构造器，创建一个图
    public Kruskal(char[] vertexData, int[][] weight) {
        this.edgesCount = 0;
        // 通过拷贝复制的方式，为了不改变原来数据
        int len = vertexData.length;
        this.vertexData = new char[len];
        for (int i = 0; i < vertexData.length; i++) {
            this.vertexData[i] = vertexData[i];
        }

        this.weight = new int[len][len];
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len; j++) {
                this.weight[i][j] = weight[i][j];
            }
        }
    }

    /**
     * 遍历权矩阵，获取有效边数
     * @return
     */
    public int getEdgesCount(){
        for (int i = 0; i < weight.length; i++) {
            for (int j = i+1; j < weight[0].length; j++) {
                if (weight[i][j]!= FIN){
                    edgesCount++;
                }
            }
        }
        return edgesCount;
    }

    /**
     * 获得关于边的数组对象
     * @return
     */
    public Edge[] getEdgeArray(){
        Edge edges[] = new Edge[edgesCount];
        int index = 0;
        for (int i = 0; i < weight.length; i++) {
            for (int j = i+1; j < weight[0].length; j++) {
                if (weight[i][j]!= FIN){
                    edges[index++] = new Edge(vertexData[i],vertexData[j],weight[i][j]);
                }
            }
        }
        return edges;
    }

    /**
     * 对边对象，进行升序排序，有利于依次选取最小权的边
     * @param edges 原始边数组
     * @return 返回排序完成的边数组
     */
    public Edge[] sortEdge(Edge[] edges){
        for (int i = 0; i < edges.length - 1; i++) {
            for (int j = 0; j < edges.length - 1 - i; j++) {
                if (edges[j].weight > edges[j+1].weight){
                    Edge temp = edges[j];
                    edges[j] = edges[j+1];
                    edges[j+1] = temp;
                }
            }
        }
        return edges;
    }

    /**
     * 根据传入的顶点数据，获取顶点对应的索引
     * @param vertex 顶点
     * @return 返回其索引
     */
    public int getVertexIndex(char vertex){
        for (int i = 0; i < vertexData.length; i++) {
            if (vertexData[i] == vertex){
                return i;
            }
        }
        return -1;
    }

    /**
     * 获取终点顶点的索引
     * @param end 该数组是动态变化的数组，根据不同的边，会赋值新的终点给end数组
     * @param i 需要获取该顶点的终点索引的顶点索引
     * @return 返回该顶点对应的终点索引
     */
    public int getEnd(int[] end, int i){
        while (end[i] != 0){
            i = end[i];
        }
        return i;
    }

    /**
     * 克鲁斯卡尔算法
     */
    public void kruskalAlgorithm(){
        // 获取边的条数
        getEdgesCount();
        // 根据边条数，获取边的素组
        Edge[] edges = getEdgeArray();
        // 对边进行排序
        sortEdge(edges);
        // 用于存储最终最小生成树的结果
        ArrayList<Edge> result = new ArrayList<>();
        // end数组，用于存储已加入边的终点
        int[] end = new int[edgesCount];

        // 遍历已排序好的边数组
        for (int i = 0; i < edgesCount; i++) {
            // 获取该边的起始顶点和终止顶点
            int startIndex = getVertexIndex(edges[i].startVertex);
            int endIndex = getVertexIndex(edges[i].endVertex);

            // 获取起始顶点和终止顶点的终点
            int m = getEnd(end, startIndex);
            int n = getEnd(end, endIndex);

            // 如果终点不同，说明不形成回路，那么就将该边加入到结果list集合中
            // 并设置终点，如果终点相同，说明会形成回路，那么就遍历下一条边
            if (m!=n){
                result.add(edges[i]);
                end[m] = n;
            }
        }

        // 打印最小生成树的结果
        for (Edge edge : result) {
            System.out.println(edge);
        }
    }
}


// 边的类
class Edge{
    char startVertex; // 边的起始顶点
    char endVertex; // 边的终止顶点
    int weight; // 该边的权

    public Edge(char startVertex, char endVertex, int weight) {
        this.startVertex = startVertex;
        this.endVertex = endVertex;
        this.weight = weight;
    }



    @Override
    public String toString() {
        return "Edge{" +
                "startVertex=" + startVertex +
                ", endVertex=" + endVertex +
                ", weight=" + weight +
                '}';
    }
}