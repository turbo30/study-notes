package algorithm;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/12
 * Time: 17:47
 * Description: 普里姆算法，修路问题（最小生成树）
 * Version: V1.0
 */
public class Prim {
    public static void main(String[] args) {
        // 创建一个图对象
        Graph graph = new Graph(7);
        // 顶点信息
        char[] data = new char[]{'A','B','C','D','E','F','G'};
        // 权重关系矩阵，10000表示路不通
        int[][] weight = new int[][]{
                {10000, 5, 7, 10000, 10000, 10000, 2},
                {5, 10000, 10000, 9, 10000, 10000, 3},
                {7, 10000, 10000, 10000, 8, 10000, 10000},
                {10000, 10000, 10000, 4, 5, 10000, 6},
                {10000, 10000, 8, 10000, 10000, 5, 4},
                {10000, 10000, 10000, 4, 5, 10000, 6},
                {2, 3, 10000, 10000, 4, 6, 10000}
        };

        // 创建一个最小生成树类
        MinTree minTree = new MinTree();
        // 创建具体的图
        minTree.createGraph(graph,data,weight);
        //minTree.showMinTree(graph);

        // 获取最短路径(最小生成树)，起始索引为0，即从A点开始
        minTree.getMinRoad(graph,0);

    }

}

class MinTree{
    // 构建一个具体的图
    public void createGraph(Graph graph, char[] data, int[][] weight){
        for (int i = 0; i < graph.vertexesData.length; i++) {
            graph.vertexesData[i] = data[i];
            for (int j = 0; j < graph.vertexesData.length; j++) {
                graph.weightEdge[i][j] = weight[i][j];
            }
        }
    }

    // 遍历权重的关系矩阵
    public void showMinTree(Graph graph){
        for (int[] weight : graph.weightEdge) {
            System.out.println(Arrays.toString(weight));
        }
    }

    // 获取最短路径
    public void getMinRoad(Graph graph, int v){
        // 用于记录是否被访问，1则表示该顶点已被访问
        int[] isVisited = new int[graph.vertexCount];
        // 将起始顶点标记已访问
        isVisited[v] = 1;
        // 用于记录最小权重
        int minWeight = 10000;
        // 以下用于记录最小权重的两个顶点的下表索引
        int index1 = -1;
        int index2 = -1;

        // 第一层for循环，确定N个顶点，只能由N-1条边
        for (int k = 1; k < graph.vertexCount; k++) {
            // 第二层和第三层for循环，用于遍历权重与顶点之间的关系
            for (int i = 0; i < graph.vertexCount; i++) {
                for (int j = 0; j < graph.vertexCount; j++) {
                    // 两个顶点之间，一定是满足一个顶点已被访问，另一个顶点没有被访问
                    // 并且如果这个两个顶点之间的权重小于记录的最小权重，最小权重变量就应该重新赋值
                    if (isVisited[i]==1 && isVisited[j]==0 && graph.weightEdge[i][j] < minWeight){
                        minWeight = graph.weightEdge[i][j];
                        index1 = i;
                        index2 = j;
                    }
                }
            }
            // 一轮遍历完了，就获得了每轮的最小权重和两个顶点，输出，之后继续下一轮遍历
            // 将另一个顶点标记为已被访问
            isVisited[index2] = 1;
            System.out.println(graph.vertexesData[index1]+"->"+graph.vertexesData[index2]+": "+graph.weightEdge[index1][index2]);
            // 最小值重新赋值，为下一轮做准备
            minWeight = 10000;
        }
    }
}

class Graph{
    int vertexCount; // 顶点总个数
    char[] vertexesData; // 每个顶点的数据
    int[][] weightEdge; // 边的权值

    public Graph(int vertexCount) {
        this.vertexCount = vertexCount;
        this.vertexesData = new char[vertexCount];
        this.weightEdge = new int[vertexCount][vertexCount];
    }
}