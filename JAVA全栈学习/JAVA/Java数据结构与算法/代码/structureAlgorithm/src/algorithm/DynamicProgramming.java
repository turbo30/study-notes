package algorithm;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/11
 * Time: 14:21
 * Description: 动态规划之01背包
 * Version: V1.0
 */
public class DynamicProgramming {
    public static void main(String[] args) {
        int p[] = {1500,3000,2000}; // 物品价格
        int w[] = {1,4,3}; //物品重量
        int packWeight = 4;
        int v[][] = new int[w.length+1][packWeight+1]; // 动态规划价格表
        int putIn[][] = new int[w.length+1][packWeight+1]; // 用于记录物品是否被放入，放入为1

        // 第一行，第一列都为0
        for (int i = 0; i < v[0].length; i++) {
            v[0][i] = 0;
        }
        for (int i = 0; i < v.length; i++) {
            v[i][0] = 0;
        }

        // 从索引为1的行开始，从索引为1的列开始
        for (int i = 1; i < v.length; i++) {
            for (int j = 1; j < v[0].length; j++) {
                // 如果第i-1个物品的重量大于此时背包重量j，那么这个物品就不能放入背包中
                // 表就应该填入上一个满足要求的价格
                if (w[i-1] > j){
                    v[i][j] = v[i-1][j];
                }
                // 如果第i-1个物品的重量小于或等于此时背包的重量j，那么就判断这个物品的价格是否
                // 大于上一个满足要求的价格，如果小于就填入将上一个满足要求的价格，否则就填入该
                // 物品的价格加上剩余重量满足的最大价格
                if (w[i-1] <= j){
                    //v[i][j] = Math.max(v[i-1][j], v[i-1][j-w[i-1]]+p[i-1]);

                    if (v[i-1][j] < v[i-1][j-w[i-1]]+p[i-1]){
                        v[i][j] = v[i-1][j-w[i-1]]+p[i-1];
                        putIn[i][j] = 1; // 记录物品被放入
                    }else {
                        v[i][j] = v[i-1][j];
                    }
                }
            }
        }

        // 输出最大价格放入的物品
        int i = putIn.length-1;
        int j = putIn[0].length-1;
        int Price = 0;
        while (i>0 && j>0){
            if (putIn[i][j] == 1){
                System.out.println("第"+i+"个物品放入背包中");
                Price += p[i-1]; // 求价格总和
                j -= w[i-1];
                // 每输出一个商品，就减去该商品的重量，求出书包剩下的重量，对应价格表中的索引
            }
            i--;
        }

        System.out.println("最大价格: "+Price+"元");

//        for (int[] ints : v) {
//            System.out.println(Arrays.toString(ints));
//        }
//
//        for (int[] ints : putIn) {
//            System.out.println(Arrays.toString(ints));
//        }
    }
}
