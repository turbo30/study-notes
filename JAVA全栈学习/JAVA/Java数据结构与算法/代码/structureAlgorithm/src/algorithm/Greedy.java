package algorithm;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/12
 * Time: 14:02
 * Description: 贪心算法
 * Version: V1.0
 */
public class Greedy {
    public static void main(String[] args) {
        // 存储需要覆盖的所有城市
        ArrayList<String> areaList = new ArrayList<>();
        areaList.add("北京");
        areaList.add("上海");
        areaList.add("天津");
        areaList.add("广州");
        areaList.add("深圳");
        areaList.add("成都");
        areaList.add("杭州");
        areaList.add("大连");

        // 存储每个电台覆盖的城市
        HashSet<String> broadcastArea1 = new HashSet<>();
        broadcastArea1.add("北京");
        broadcastArea1.add("上海");
        broadcastArea1.add("天津");
        HashSet<String> broadcastArea2 = new HashSet<>();
        broadcastArea2.add("广州");
        broadcastArea2.add("北京");
        broadcastArea2.add("深圳");
        HashSet<String> broadcastArea3 = new HashSet<>();
        broadcastArea3.add("成都");
        broadcastArea3.add("上海");
        broadcastArea3.add("杭州");
        HashSet<String> broadcastArea4 = new HashSet<>();
        broadcastArea4.add("上海");
        broadcastArea4.add("天津");
        HashSet<String> broadcastArea5 = new HashSet<>();
        broadcastArea5.add("杭州");
        broadcastArea5.add("大连");

        // 将电台与其所覆盖的城市关联起来，成为键值对
        HashMap<String, HashSet<String>> broadcasts = new HashMap<>();
        broadcasts.put("K1",broadcastArea1);
        broadcasts.put("K2",broadcastArea2);
        broadcasts.put("K3",broadcastArea3);
        broadcasts.put("K4",broadcastArea4);
        broadcasts.put("K5",broadcastArea5);

        // keyList用于存储符合要求的电台
        ArrayList<String> keyList = new ArrayList<>();
        // key用于存储每一轮遍历与areaList交集最多的电台
        String key = null;
        // 用于临时存储电台覆盖的城市
        HashSet<String> temp = new HashSet<>();

        // 结束条件是areaList没有任何数据，即说明每个城市都被覆盖了
        while (!areaList.isEmpty()){
            // 遍历broadcasts
            for (String broadcastKey : broadcasts.keySet()) {
                temp.clear(); // 一定要清空，temp是临时存储的变量
                // 将当前电台覆盖的城市添加到临时存储的变量中
                temp.addAll(broadcasts.get(broadcastKey));
                // temp集合与areaList集合取交集，交集结果赋值给temp
                temp.retainAll(areaList);
                // 判断temp的大小是否大于0，以及key值是否为空，
                // 如果不为空就判断本轮的交集大小是否大于上一次存储的key值对应的电台覆盖的城市，大于就重新赋值新的最优的结果
                if (temp.size() > 0 && (key==null || temp.size() > broadcasts.get(key).size())){
                    key = broadcastKey;
                }
            }
            // 一轮遍历完之后，判断key是否为空，不为空，就将key存入到keyList集合中，表示符合要求的电台
            // 最后将符合要求的电台所覆盖的城市，从areaList集合中移除掉，说明这些城市已被覆盖
            if (key!=null){
                keyList.add(key);
                areaList.removeAll(broadcasts.get(key));
                key = null;
            }
        }

        // 输出结果
        System.out.println(keyList.toString());
    }

}
