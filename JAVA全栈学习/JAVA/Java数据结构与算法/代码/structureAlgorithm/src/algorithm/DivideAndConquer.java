package algorithm;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/11
 * Time: 13:28
 * Description: 汉诺塔
 * 思想：将n个盘看成两个盘，最下面一个盘为一个盘和之上的所有盘为一个盘
 * 如果只有一个盘，很明显就是从A-C
 * 如果有n个盘，分为第n个盘为一组，和n-1个盘为一组
 * 首先将n-1个盘移动到B，即A-B 过度为C
 * 然后再将第n个盘移动到C，即A-C
 * 最后将在B塔的n-1个盘移动到C，即B-C 过度为A
 * Version: V1.0
 */
public class DivideAndConquer {
    public static void main(String[] args) {
        hanoiTower(3,'A','B','C');
    }

    public static void hanoiTower(int num, char a, char b, char c){
        // 当num等于1时，直接输出A-C
        if (num==1){
            System.out.println("第1个塔 "+a+" -> "+c);
        }else {
            // 如果有多个盘，将num-1个盘从A移到B，过渡C
            hanoiTower(num-1, a, c, b);
            // 输出结果第num盘，从变量a到变量c的结果，即A-B
            System.out.println("第"+num+"个塔 "+a+" -> "+c);
            // 最后将B移到C，过渡为A
            hanoiTower(num-1, b, a, c);
        }
    }
}
