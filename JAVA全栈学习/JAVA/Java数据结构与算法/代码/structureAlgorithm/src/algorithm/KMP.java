package algorithm;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/11
 * Time: 22:05
 * Description: kmp算法
 * Version: V1.0
 */
public class KMP {
    public static void main(String[] args) {
        String str01 = "ABCAAB CAB DAABCAB";
        String str02 = "ABCAB";
        int index = kmpAlgo(str01, str02);
        System.out.println(index);
        int[] abaabcacs = getNextArray("abaabbabaab");
        System.out.println(Arrays.toString(abaabcacs));
    }

    public static int[] getNextArray(String dest){
        int next[] = new int[dest.length()]; // 创建一个和数组长度一样的部分匹配值表
        next[0] = 0; // 表的第一个长度永远是0
        // i=1 开始索引的可以看成是后缀字符串
        // j=0 开始索引的可以看成事前缀字符串
        for (int i = 1, j=0; i < next.length; i++) {
            // 如果两个字符不相等，并且j大于0，那么j就需要回溯到next[j-1]的位置
            // 建议画图理解
            while (j>0 && dest.charAt(i)!=dest.charAt(j)){
                // 回溯到cartAt(j-1)字符的索引对应的next表中的值，将其作为下一个回溯的索引
                j = next[j-1];
            }
            // 如果相等，那么最大匹配值就是j++的值
            if (dest.charAt(i) == dest.charAt(j)){
                j++;
            }
            next[i] = j;
        }
        return next;
    }

    public static int kmpAlgo(String source, String target){
        int[] next = getNextArray(target);
        for (int i = 0, j=0; i < source.length(); i++) {
            while (j>0 && source.charAt(i) != target.charAt(j)){
                j = next[j-1];
            }
            if (source.charAt(i)==target.charAt(j)){
                j++;
            }
            if (j == target.length()){
                return i-j+1;
            }
        }
        return -1;
    }
}
