package Aoj;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/9
 * Time: 19:36
 * Description: 算数问题
 *    809
 *   -------
 * xx）xxxx
 *     xx
 *   -------
 *      xxx
 *      xxx
 *   -------
 *        1
 * 输入：
 * 输出共五行，每行对应图中算式从上到下从左到右的一个数。
 * 具体来说：
 * 输出的第一行对应图中算式中左上角的那个未知的两位数；
 * 输出的第二行对应图中的那个未知的四位数；
 * 输出的第三行对应图中的另外一个未知的两位数；
 * 输出的第四行对应图中的位置靠上的那个未知的三位数；
 * 输出的第五行对应图中的位置靠下的那个未知的三位数。
 * Version: V1.0
 *    809
 *   -------
 * a）b(4)
 *    c(2)
 *   -------
 *      d(3)
 *      e(3)
 *   -------
 *        1
 */
public class Formula {
    public static void main(String[] args) {
        int a; // 除数
        int b; // 被除数
        int c; // 第一个8*除数的结果
        int d; // 被除数-d树的结果
        int e; // 最后9*除数的结果
        for (a = 10; a < 13; a++) {
            c = 8 * a;
            d = 9 * a + 1;
            e = 9 * a;
            b = 100 * c + d;
            if (b >= 1000 && b < 10000 && c < 100 && e >= 100 && e < 1000) {
                System.out.println(a); // 除数
                System.out.println(b); // 被除数
                System.out.println(c); // 第一个8*除数的结果
                System.out.println(d); // 被除数-d树的结果
                System.out.println(e); // 最后9*除数的结果
            }
        }
    }
}
