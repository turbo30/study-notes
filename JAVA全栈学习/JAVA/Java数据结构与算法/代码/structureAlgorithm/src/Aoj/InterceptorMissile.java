package Aoj;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/9
 * Time: 20:02
 * Description: 拦截导弹
 * 对于每组输入数据，第一行输出这套系统最多能拦截多少导弹，第二行输出如果要拦截所有导弹最少要配备多少套这种导弹拦截系统。
 * 输入：
 * 389 207 155 300 299 170 158 65
 * 输出：
 * 6
 * 2
 * 提示：
 * 对于样例中的数据
 * 第一行输出这套系统最多能拦截多少导弹，
 * 这里为389，300，299，170，158，65这六个导弹
 * 第二行输出如果要拦截所有导弹最少要配备多少套这种导弹拦截系统。
 * 两套设备分别拦截：389，300，299，170，158，65 和 207， 155导弹
 * Version: V1.0
 */
public class InterceptorMissile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String[] split = str.split(" ");
        int size = split.length;
        int[] num = new int[size];
        for (int i = 0; i < size; i++) {
            num[i] = Integer.parseInt(split[i]);
        }

        int[] intercept1 = new int[size];
        int[] intercept2 = new int[size];
        for(int i = 0; i < size; i++) {
            intercept1[i] = 1;
            intercept2[i] = 1;
        }

        for(int i = 0; i < size; i++) {
            for(int j = 0; j < i; j++) {
                if(num[i] <= num[j]) {
                    intercept1[i] = Math.max(intercept1[i], intercept1[j]+1);
                }else {
                    intercept2[i] = Math.max(intercept2[i], intercept2[j]+1);
                }
            }
        }

        int count = 0;
        int equipmentCount = 0;
        for(int i = 0; i < size; i++) {
            count = Math.max(count, intercept1[i]);
            equipmentCount = Math.max(equipmentCount, intercept2[i]);
        }
        System.out.println(count);
        System.out.println(equipmentCount);
    }
}
