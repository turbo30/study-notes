package Aoj;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/9
 * Time: 19:28
 * Description:
 * 门外长度为L的公路上有一排树，每两棵相邻的树之间的间隔都是1米。把马路看成一个数轴，
 * 一端在数轴0的位置，另一端在L的位置；数轴上的每个整数点，即 0,1,2……L都种有一棵树。
 * 由于部分区域占用要修工程。这些区域用它们在数轴上的起始点和终止点表示。已知任一区
 * 域的起始点和终止点的坐标都是整数，区域之间可能有重合的部分。现在要把这些区域中的
 * 树（包括区域端点处的两棵树）移走。你的任务是计算将这些树都移走后，马路上还有多少棵树。
 *
 * 输入：
 * 第一行有两个整数，分别表示马路的长度l和区域的数目 m。
 * 接下来m行，每行两个整数 u,v，表示一个区域的起始点和终止点的坐标
 * 输出：
 * 输出一行一个整数，表示将这些树都移走后，马路上剩余的树木数量。
 * 输入：
 * 500 3
 * 150 300
 * 100 200
 * 470 471
 * 输出：
 * 298
 * 提示：
 * 对于 20% 的数据，保证区域之间没有重合的部分。
 * 对于 100% 的数据，保证 1<=l<=10, 1<=m<=100, 0<=u<=v<=l
 * Version: V1.0
 */
public class DoorTree {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        /** 马路上的树（没有移走之前），总共有L+1棵。 */
        byte[] trees = new byte[scanner.nextInt() + 1];

        /** 区域个数。用于确定循环次数。 */
        int time = scanner.nextInt();

        // 循环time次，读取所有“区域”。
        // 如果某区域的终点和另一个区域的起点重叠，这种方法可以自动合并两个区域。
        for (int t = 0; t < time; t++) {
            trees[scanner.nextInt()]++;    // 这里输入的int是该区域的起点位置。
            trees[scanner.nextInt()]--;    // 这里输入的int是区域的终点位置。
        }

        scanner.close();

        /** 某一位置的“树”身处的区域的个数。 */
        int stack = 0;

        /** 当前未被移走的树的数量 */
        int quantity = 0;

        // 遍历trees以统计剩余树的数量。
        for (int i = 0; i < trees.length; i++) {
            // stack != 0表示第i棵树身处stack个叠加的区域中。
            // trees[i] != 0表示第i棵树不在区域的两个端点上。
            if (trees[i] != 0 || stack != 0) {
                stack += trees[i];
            } else {
                quantity++;
            }
        }
        System.out.print(quantity);
    }
}
