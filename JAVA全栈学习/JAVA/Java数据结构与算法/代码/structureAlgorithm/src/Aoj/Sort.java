package Aoj;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/6
 * Time: 15:29
 * Description:
 * Version: V1.0
 */
public class Sort {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        scanner.close();
        String[] strs = str.split(" ");
        int arr[] = new int[10];
        for (int i = 0; i < strs.length; i++) {
            arr[i] = Integer.parseInt(strs[i]);
        }

        int index = 0;
        int min;

        for (int i = 0; i < arr.length-1; i++) {
            min = arr[i];
            for (int j = i+1; j < arr.length; j++) {
                if (min > arr[j]){
                    min = arr[j];
                    index = j;
                }
            }
            if (min!=arr[i]){
                arr[index] = arr[i];
                arr[i] = min;
            }
        }

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");
        }
    }
}
