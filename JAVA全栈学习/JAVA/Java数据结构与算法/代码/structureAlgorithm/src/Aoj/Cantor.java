package Aoj;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/9
 * Time: 19:21
 * Description:
 * Cantor表
 * 1/1 1/2 1/3 1/4 1/5 ...
 * 2/1 2/2 2/3 2/4 2/5 ...
 * 3/1 3/2 3/3 3/4 3/5 ...
 * ...
 * 走Z字形
 * 输入：
 * 每个测试文件只包含一组测试数据，每组输入一个正整数N（1≤N≤10000000）。
 * 输出：
 * 对于每组输入数据，输出表中的第N项。
 * 输入：
 * 7
 * 输出1/4
 * Version: V1.0
 */
public class Cantor {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
//        int n = 7;
        int arr[]=new int[10000000];
        arr[1]=1;
        int num=1;
        // 存入的数以第一个大于或等于输入的数为截至，并把索引赋给num
        for(int i=2;i<arr.length;i++)
        {
            arr[i]=arr[i-1]+i;
            if(arr[i]>=n){
                num=i;
                break;
            }
        }
        /*for (int i = 0; i < 10; i++) {
            System.out.print(arr[i]+" "); // 输入的是7这个位置，数组中存入的数为0 1 3 6 10
        }*/
        if(num%2==0)
        {
            System.out.println(n-arr[num-1]+"/"+((num+1)-(n-arr[num-1])));
        }
        if(num%2==1)
        {
            System.out.println(((num+1)-(n-arr[num-1]))+"/"+(n-arr[num-1]));
        }
    }
}
