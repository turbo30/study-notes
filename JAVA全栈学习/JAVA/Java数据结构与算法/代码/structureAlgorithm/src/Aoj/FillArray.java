package Aoj;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/6
 * Time: 12:37
 * Description: 填充数组
 * 输入6：
 * 16	17	18	19	20	1
 * 15	30	31	32	21	2
 * 14	29	36	33	22	3
 * 13	28	35	34	23	4
 * 12	27	26	25	24	5
 * 11	10	9	8	7	6
 * Version: V1.0
 */
public class FillArray {
    public static void main(String[] args) {
        int i, j;
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] arr = new int[n][n];
        i = 0;
        j = n-1;
        int num = 1;
        arr[i][j] = 1;
        // 控制从 1----n*n 的 数字
        while (num < n * n) {
            // 下走
            while (i + 1 < n && arr[i + 1][j] == 0) {
                arr[++i][j] = ++num;
            }
            // 左走
            while (j - 1 >= 0 && arr[i][j - 1] == 0) {
                arr[i][--j] = ++num;
            }
            // 上走
            while (i - 1 >= 0 && arr[i - 1][j] == 0) {
                arr[--i][j] = ++num;
            }
            // 右走
            while (j + 1 < n && arr[i][j + 1] == 0) {
                arr[i][++j] = ++num;
            }
        }
        // 输出这个矩阵
        for (int[] ints : arr) {
            for (int k = 0; k < ints.length; k++) {
                System.out.print(ints[k]+"\t");
            }
            System.out.println();
        }
    }
}
