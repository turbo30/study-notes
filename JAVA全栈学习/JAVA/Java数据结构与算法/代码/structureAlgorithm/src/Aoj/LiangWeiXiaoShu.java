package Aoj;

import java.text.DecimalFormat;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/6
 * Time: 15:34
 * Description: 保留两位小数
 * Version: V1.0
 */
public class LiangWeiXiaoShu {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double num1 = Double.parseDouble(scanner.nextLine());
        double num2 = Double.parseDouble(scanner.nextLine());
        DecimalFormat df = new DecimalFormat("0.00");
        System.out.println(df.format(num1*num2));
    }
}
