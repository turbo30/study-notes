package Aoj;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/6
 * Time: 15:53
 * Description: 回文数
 * 12321
 * 123321
 * Version: V1.0
 */
public class PalindromeNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String[] split = s.split("");
        int mid = (split.length-1)/2;
        int left = 0;
        int right = split.length-1;
        boolean flag = true;
        while (left <= mid && right >= mid){
            if (!split[left].equals(split[right])){
                flag = false;
                break;
            }else {
                left++;
                right--;
            }
        }
        if (flag){
            System.out.println("yes");
        }else {
            System.out.println("no");
        }
    }
}
