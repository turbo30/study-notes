package test;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * User : phy
 * Date :  2020-09-11 14:48
 * Description :
 * Version : V1.0
 *
 * @author yue*/
public class SingleLinkedListDemo {
    public static void main(String[] args) {
        //测试
        //先创建节点
        HeroNode hero1 = new HeroNode(1, "送江", "及时雨");
        HeroNode hero2 = new HeroNode(2, "卢俊义", "玉麒麟");
        HeroNode hero3 = new HeroNode(3, "吴用", "智多星");
        HeroNode hero4 = new HeroNode(4, "林冲", "豹子头");
        HeroNode hero6 = new HeroNode(6, "送江111", "及时雨");
        HeroNode hero5 = new HeroNode(5, "卢俊义222", "玉麒麟");
        HeroNode hero7 = new HeroNode(7, "吴用333", "智多星");
        HeroNode hero8 = new HeroNode(8, "林冲444", "豹子头");

        //创建要给链表
        SingleLinkedList singleLinkedList = new SingleLinkedList();
        SingleLinkedList singleLinkedList1 = new SingleLinkedList();
        //加入
        /**
         *
         singleLinkedList.add(hero1);
         singleLinkedList.add(hero4);
         singleLinkedList.add(hero2);
         singleLinkedList.add(hero3);
         */

        //加人按照编号
        singleLinkedList.addByOrder(hero1);
        singleLinkedList.addByOrder(hero4);
        singleLinkedList.addByOrder(hero2);
        singleLinkedList.addByOrder(hero3);
        singleLinkedList1.addByOrder(hero5);
        singleLinkedList1.addByOrder(hero6);
        singleLinkedList1.addByOrder(hero7);
        singleLinkedList1.addByOrder(hero8);
//        singleLinkedList.list();
//        //测试修改的代码
//        HeroNode heroNode=new HeroNode(2,"小卢","猪猪猪");
//        singleLinkedList.update(heroNode);
        //删除节点
//        singleLinkedList.delete(4);
//        singleLinkedList.delete(1);
//        singleLinkedList.delete(2);
        //显示
//        System.out.println("修改后的链表：");
//        singleLinkedList.list();
//        System.out.println(getLength(singleLinkedList.getHead()));
//        System.out.println("节点是："+findNode(singleLinkedList.getHead(),4));


//        reverseList(singleLinkedList.getHead());
//        System.out.println("反转后的链表：");
//        singleLinkedList.list();

//        System.out.println("逆序打印链表：");
//        reversePrint(singleLinkedList.getHead());
        twoList(singleLinkedList.getHead(), singleLinkedList1.getHead());


    }

    /**
     * 方法：获取到单链表的节点个数（不包括头结点）
     * @param head
     * @return 节点有效个数
     */
    public static int getLength(HeroNode head){
        if(head.next==null){
            return 0;
        }
        int length=0;
        HeroNode temp=head.next;
        while (temp!=null){
                length++;
                temp=temp.next;
        }
        return length;
    }

    /**
     * 查找倒数第index个节点
     * @param index
     * @return  HeroNode
     */
    public static HeroNode findNode(HeroNode head,int index){
        if(head.next==null){
            return null;
        }
        int size=getLength(head);
        if(index<=0||index>size){
            return null;
        }
        HeroNode temp=head.next;
        for (int i=0;i<size-index;i++){
            temp=temp.next;
        }
        return temp;
    }

    /**
     * 用栈的方式反向打印链表{百度}
     * @param head
     */
    private static void reversePrint(HeroNode head){
        if(head.next==null){
            return;
        }
        Stack<HeroNode> nodes = new Stack<HeroNode>();
        HeroNode cur=head.next;
        while (cur!=null){
            nodes.push(cur);
            cur=cur.next;
        }
        while (nodes.size()>0){
            System.out.println(nodes.pop());
        }
    }

    private static void twoList(HeroNode head,HeroNode head1){
        if(head.next==null||head1.next==null){
            return;
        }
        HeroNode cur1=new HeroNode(0,"","");
        HeroNode cur = cur1;
        HeroNode temp1 = head.next;
        HeroNode temp2 = head1.next;
        while (true){
            if (temp1==null && temp2==null){
                break;
            }else if (temp1==null){
                cur.next = temp2;
                break;
            }else if (temp2 == null){
                cur.next = temp1;
                break;
            }

            if (temp1.no < temp2.no){
                cur.next = temp1;
                temp1 = temp1.next;
            }else {
                cur.next = temp2;
                temp2 = temp2.next;
            }
            cur = cur.next;
        }
        while (true){
            //判断是否到链表最后
            if(cur1==null){
                break;
            }
            //输出节点的信息
            System.out.println(cur1);
            //将temp后移
            cur1=cur1.next;
        }
    }
    /**
     * 单链表反转{腾讯面试题}
     * @param head
     */
    public static void reverseList(HeroNode head){
        //如果为空或者只有一个节点，就不用处理
        if(head.next==null||head.next.next==null){
            return;
        }
        //定义一个辅助指针，帮助我们遍历原来的链表
        HeroNode cur=head.next;
        //指向当前节点【cur】的下一个节点
        HeroNode next=null;
        HeroNode reverseHead=new HeroNode(0,"","");
        //遍历原来的链表，
        //每遍历一个，就取出放在reverseHead最前端
        while (cur!=null){
            //保存当前节点的下一个节点
            next=cur.next;
            //将cur的下一个节点指向新的链表的最前端
            cur.next=reverseHead.next;
            //将cur链接到新的链表上
            reverseHead.next=cur;
            //后移
            cur=next;
        }
        head.next=reverseHead.next;
    }
}
/**
 * 定义一个SingleLinkedList来管理我们的英雄
 */
class SingleLinkedList{
    /**
     * 先初始化一个头结点
     */
    private HeroNode head=new HeroNode(0,"","");

    public HeroNode getHead() {
        return head;
    }

    public void setHead(HeroNode head) {
        this.head = head;
    }

    /**
     * 添加一个新的节点
     *  1.找到当前链表的最后一个节点
     *  2.将最后这个节点的next指向新的节点
     * @param heroNode
     */
    public void add(HeroNode heroNode) {
        //head节点不能动，因此我们需要一个辅助节点
        HeroNode temp=head;
        //遍历链表，找到最后
        while (true){
            if(temp.next==null){
                break;
            }
            //如果没有找到最后,就将temp后移
            temp=temp.next;
        }
        //当退出while循环时，temp就指向了最后
        temp.next=heroNode;
    }
    /**
     *根据排名将英雄插入到指定位置
     * 如果有这个排名，则添加失败，并提示
     */
    public void addByOrder(HeroNode heroNode){
        //因为头结点不能动，我们还是需要辅助指针来帮助找到添加的位置
        //因为是单链表，因此我们找到的temp是位于要添加的前一个位置，否则加入不了
        HeroNode temp=head;
        //表示添加的编号是否存在，默认为false
        boolean flag=false;
        while (true){
            //说明已经在链表的最后了
            if (temp.next==null){
                break;
            }
            if (temp.next.no>heroNode.no){
                //位置找到了，就在temp后面插入
                break;
            }else if (temp.next.no==heroNode.no){
                //说明希望添加的编号已经存在
                flag=true;
                break;
            }
            //后移
            temp=temp.next;
        }
        //判断flag的值
        if (flag){
            //不能添加，已经存在了
            System.out.println("准备添加的英雄编号:"+heroNode.no+"号，已经存在，不能加人");
        }
        else {
            heroNode.next=temp.next;
            temp.next=heroNode;

        }
    }

    /**
     * 更加新的节点修改信息
     * @param heroNode
     */
    public void update(HeroNode heroNode){
        //判断链表是否为空
        if (head.next==null){
            System.out.println("链表为空！！！");
            return;
        }
        //需要找到要修改的节点，更加no号
        HeroNode temp=head.next;
        //是否找到该节点
        boolean flag=false;
        while (true){
            if (temp==null){
                break;
            }
            if(temp.no==heroNode.no){
                //找到了节点
                flag=true;
                break;
            }
            temp=temp.next;
        }
        if (flag){
            temp.name=heroNode.name;
            temp.nickname=heroNode.nickname;
        }else {
            System.out.println("找不到编号:"+heroNode.no+"号");
        }
    }

    /**
     * 删除指定节点
     * @param no
     */
    public void delete(int no){
        HeroNode temp=head;
        //表示是否找到了
        boolean flag=false;
        while (true){
            if (temp.next==null){
                break;
            }
            if(temp.next.no==no){
                //找到了要删除的节点的前一个节点
                flag=true;
                break;
            }
            //后移
            temp=temp.next;
        }
        if (flag){
            temp.next=temp.next.next;
        }else {
            System.out.println("要删除的"+no+"不存在");
        }
    }
    /**
     * 显示链表
     * 需要一个辅助节点遍历
     */
    public void list(){
        //判断链表是否为空
        if (head.next==null){
            System.out.println("链表为空");
            return;
        }
        //头结点不能动
        HeroNode temp=head.next;
        while (true){
            //判断是否到链表最后
            if(temp==null){
                break;
            }
            //输出节点的信息
            System.out.println(temp);
            //将temp后移
            temp=temp.next;
        }
    }
}
/**
 * 定义HeroNode，每一个HeroNode对象就是一个节点
 */
class HeroNode{
    /**
     * next指向下一个节点
     */
    public int no;
    public String name;
    public String nickname;
    public HeroNode next;
    public HeroNode(int hNo,String hName,String hNickname) {
        this.no=hNo;
        this.name=hName;
        this.nickname=hNickname;
    }

    @Override
    public String toString() {
        return "HeroNode{" +
                "no=" + no +
                ", name='" + name + '\'' +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}
