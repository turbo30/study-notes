package recursion;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/2
 * Time: 11:02
 * Description: 八皇后问题，切入点是第几个皇后
 * Version: V1.0
 */
public class EightQueens {
    public static void main(String[] args) {
        EightQueens eightQueens = new EightQueens();
        eightQueens.runEightQueens(0);
        System.out.println("一共"+eightQueens.count+"种走法");

    }

    // 定义一维数组，
    // map[3] 的意义是：第4行第map[3]列可放皇后
    int maxSize = 8;
    int map[] = new int[maxSize];
    int count = 0;

    // 第n个皇后，判断前面的皇后与它是否冲突
    public boolean judge(int n){
        for (int i = 0; i < n; i++) {
            // map[i] == map[n] 同一列
            // Math.abs(n-i) == Math.abs(map[n]-map[i]) 同一斜线
            if ( map[i] == map[n] || Math.abs(n-i) == Math.abs(map[n]-map[i]) ){
                return false;
            }
        }
        return true;
    }

    public void runEightQueens(int n){
        // n == maxSize 就说明已经走完了
        if(n == maxSize){
            count++;
            System.out.println(Arrays.toString(map));
            return;
        }

        // 第n行，也就是第n个皇后，i表示在第n行的所在位置
        for (int i = 1; i <= maxSize; i++) {
            map[n] = i;
            if ( judge(n) ){
                // 如果与前面所放的皇后都不冲突，就进入下一行放皇后
                // 如果冲突，在当前就继续往下一个位置放皇后，再判断
                runEightQueens(n+1);
            }
        }
    }
}
