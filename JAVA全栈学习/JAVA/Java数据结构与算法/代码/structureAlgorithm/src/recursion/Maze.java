package recursion;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/2
 * Time: 10:34
 * Description: 递归回溯解决简单迷宫
 * Version: V1.0
 */
public class Maze {
    public static void main(String[] args) {
        // 创建一个8*8的二维数组，用1表示围墙和阻碍物，用2表示已走可通的路，用3表示已走不可通的路
        int maxSize = 8;
        int map[][] = new int[maxSize][maxSize];

        // 建立如图所示的围墙和阻碍物
        for (int i = 0; i < maxSize; i++) {
            map[0][i] = 1;
            map[7][i] = 1;
            map[i][0] = 1;
            map[i][7] = 1;
        }
        map[2][1] = 1;
        map[2][2] = 1;
        map[2][3] = 1;
        map[5][3] = 1;
        map[5][4] = 1;

        runMaze(map,1,1);

        for (int[] ints : map) {
            System.out.println(Arrays.toString(ints));
        }
    }

    // 结束条件map[6][6]的位置为2即可
    public static boolean runMaze(int[][] map,int i, int j){
        if(map[6][6] == 2){
            return true;
        }
        // 走的策略：右、下、左、上
        if(map[i][j] == 0){
            map[i][j] = 2; // 假定该点为2
            if( runMaze(map, i, j+1) ){
                // 判断右是否可以走通，走得通就返回true
                return true;
            }else if ( runMaze(map, i+1, j) ){
                // 判断下是否可以走通，走得通就返回true
                return true;
            }else if ( runMaze(map, i, j-1) ){
                // 判断左是否可以走通，走得通就返回true
                return true;
            }else if ( runMaze(map, i-1, j) ){
                // 判断上是否可以走通，走得通就返回true
                return true;
            }else {
                // 如果右下左上都做不通，就把该点设置为3，并返回false
                map[i][j] = 3;
                return false;
            }
        }else { // 不等于0，就是1 2 3的情况，直接返回false退出即可
            return false;
        }
    }
}
