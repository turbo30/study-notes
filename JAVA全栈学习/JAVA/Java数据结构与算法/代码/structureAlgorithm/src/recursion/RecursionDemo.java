package recursion;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/1
 * Time: 12:14
 * Description: 递归测试
 * Version: V1.0
 */
public class RecursionDemo {
    public static void main(String[] args) {
        // 递归打印
        print(4);
        // 阶乘 10! = 3628800
        System.out.println(factorial(10));

    }

    public static void print(int n){
        if(n > 2){
            print(n-1);
        }
        System.out.println(n);
    }

    public static int factorial(int n){
        if (n == 1){
            return 1;
        }
        return factorial(n-1)*n;
    }
}
