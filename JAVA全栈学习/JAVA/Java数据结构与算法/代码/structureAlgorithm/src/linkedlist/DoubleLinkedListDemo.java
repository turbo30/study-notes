package linkedlist;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/5/30
 * Time: 17:43
 * Description: 双向链表
 * Version: V1.0
 */
public class DoubleLinkedListDemo {
    public static void main(String[] args) {
        DoubleLinkedList doubleLinkedList = new DoubleLinkedList();
        doubleLinkedList.addNodeById(new DouNode(1,"1"));
        doubleLinkedList.addNodeById(new DouNode(4,"4"));
        doubleLinkedList.addNodeById(new DouNode(2,"2"));
        doubleLinkedList.addNodeById(new DouNode(3,"3"));
        doubleLinkedList.addNodeById(new DouNode(2,"2"));
        doubleLinkedList.showNode();

        System.out.println("删除节点4：");
        doubleLinkedList.delNode(4);
        doubleLinkedList.showNode();
        System.out.println("删除节点2：");
        doubleLinkedList.delNode(2);
        doubleLinkedList.showNode();

        System.out.println("修改节点1：");
        doubleLinkedList.updateNode(new DouNode(1,"11111111111"));
        doubleLinkedList.showNode();
    }
}

class DoubleLinkedList{
    private DouNode head = new DouNode();

    // 依次添加节点
    public void addNode(DouNode node){
        DouNode temp = head;
        while (true){
            if(temp.next == null){
                temp.next = node;
                node.pre = temp;
                break;
            }
            temp = temp.next;
        }
    }

    // 根据id大小，添加节点
    public void addNodeById(DouNode node){
        DouNode temp = head;
        while (true){
            if(temp.next == null){
                temp.next = node;
                node.pre = temp;
                break;
            }
            if(temp.next.no == node.no){
                System.out.println("有重复节点添加...");
                break;
            }
            if (temp.next.no > node.no){
                temp.next.pre = node;
                node.next = temp.next;
                temp.next = node;
                node.pre = temp;
                break;
            }
            temp = temp.next;
        }
    }

    // 删除节点
    public void delNode(int no){
        DouNode temp = head.next;
        if(temp == null){
            System.out.println("链表为空，无法删除...");
            return;
        }
        while (true){
           if(temp == null){
               System.out.println("没有要删除的节点...");
               break;
           }
           if(temp.no == no){
               temp.pre.next = temp.next;
               if(temp.next!=null){
                   temp.next.pre = temp.pre;
               }
               break;
           }
           temp = temp.next;
        }
    }

    // 修改
    public void updateNode(DouNode node){
        DouNode temp = head.next;
        if(temp == null){
            System.out.println("链表为空，无法修改...");
            return;
        }
        while (true){
            if(temp == null){
                System.out.println("没有要修改的节点...");
                break;
            }
            if(temp.no == node.no){
                temp.name = node.name;
                break;
            }
            temp = temp.next;
        }
    }

    // 遍历链表
    public void showNode(){
        if(head.next == null){
            System.out.println("链表为空，无法遍历...");
            return;
        }
        DouNode temp = head.next;
        while (true){
            if(temp == null){
                break;
            }
            System.out.println(temp);
            temp = temp.next;
        }
    }
}

class DouNode{
    public int no;
    public String name;
    public DouNode pre;
    public DouNode next;

    public DouNode() {
    }

    public DouNode(int no, String name) {
        this.no = no;
        this.name = name;
    }

    @Override
    public String toString() {
        return "linkedlist.DouNode{" +
                "no=" + no +
                ", name='" + name + '\'' +
                '}';
    }
}