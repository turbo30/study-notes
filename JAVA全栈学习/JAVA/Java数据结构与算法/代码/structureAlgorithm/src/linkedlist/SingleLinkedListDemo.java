package linkedlist;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/5/29
 * Time: 16:57
 * Description:
 * Version: V1.0
 */
public class SingleLinkedListDemo {
    public static void main(String[] args) {
        SingleLinkedList singleLinkedList = new SingleLinkedList();
//        singleLinkedList.addNode(new linkedlist.Node(1, "heroC","20"));
//        singleLinkedList.addNode(new linkedlist.Node(3, "yikeX","21"));
//        singleLinkedList.addNode(new linkedlist.Node(2, "wenxC","18"));
        singleLinkedList.addNodeById(new Node(1, "heroC","20"));
        singleLinkedList.addNodeById(new Node(3, "yikeX","21"));
        singleLinkedList.addNodeById(new Node(2, "wenxC","18"));
        singleLinkedList.addNodeById(new Node(2, "wenxC","18"));
        singleLinkedList.showNode();

        singleLinkedList.updateNode(new Node(4, "wenxC~~","20"));
        // 修改id为2的节点
        System.out.println("修改id为2的节点");
        singleLinkedList.updateNode(new Node(2, "wenxC~~","20"));

        singleLinkedList.delNode(3);

        singleLinkedList.showNode();
    }
}

class SingleLinkedList{
    private final Node head = new Node(); // 创建一个头节点

    // 向链表中添加节点，直接在尾节点后添加新节点
    public void addNode(Node node){
        Node temp = head; // 获取头节点
        while (true){
            // 判断当前节点的next是否指向节点对象，如果为null，说明当前节点是尾节点
            if(temp.next == null){
                // 向尾节点的next添加新节点
                temp.next = node;
                break;
            }
            // 如果不是尾节点，那么就next到指向到下一个节点对象
            temp = temp.next;
        }
    }

    // 优化添加方式，根据id大小添加元素到指定位置
    public void addNodeById(Node node){
        Node temp = head;
        boolean flag = false;
        while (true){
            if(temp.next == null){
                // 说明是temp是尾节点，退出此时的temp就是需要往后添加node的节点
                break;
            }
            if(temp.id == node.id){
                // id一样，说明链表中已添加了同id的节点对象
                // flag为true，说明已存在同样id的节点
                flag = true;
                break;
            }else if(temp.next.id > node.id){
                // 如果temp下一个节点对象的id大于添加节点的id，那么这个添加的节点
                // 就应该插入到temp节点的后面，temp节点的下一个节点的前面
                // 退出此时的temp就是需要往后添加node的节点
                break;
            }
            temp = temp.next; // 以上都不满足，就继续判断下一个节点
        }
        if(flag){
            System.out.println("编号："+ node.id + " 已在链表中存在，无法添加到链表中...");
        }else {
            // 插入节点操作
            node.next = temp.next;
            temp.next = node;
        }
    }

    // 删除节点
    public void delNode(int id){
        Node temp = head;
        boolean flag = false;
        while (true){
            if(temp.next == null){
                flag = true;
                break;
            }
            if(temp.next.id == id){
                break;
            }
            temp = temp.next;
        }
        if(flag){
            System.out.println("链表中无id为"+id+"的节点...");
        }else {
            System.out.println("已删除id为"+temp.next.id+"的节点");
            temp.next = temp.next.next;
        }
    }

    // 修改节点内容
    public void updateNode(Node node){
        Node temp = head.next;
        while (true){
            if(temp == null){
                System.out.println("链表中无id为"+node.id+"的节点，无法修改...");
                break;
            }
            if(temp.id == node.id){
                temp.name = node.name;
                temp.age = node.age;
                break;
            }
            temp = temp.next;
        }
    }

    // 遍历链表中的所有节点
    public void showNode(){
        Node temp = head.next; // 获取头节点的下一个节点
        if(temp == null){ // 如果头节点的下一个节点为null，说明是空链表
            System.out.println("链表为空...");
            return;
        }
        while (true){
            System.out.println(temp.toString());
            temp = temp.next;
            if(temp == null){
                break;
            }
        }
    }


}

// 创建一个节点对象
class Node{
    // data域 ，在真实开发中应封装为一个类
    public int id;
    public String name;
    public String age;
    // next域
    public Node next; // 用于存储下一个节点对象(指向下一个节点)

    public Node() {
    }

    public Node(int id, String name, String age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "linkedlist.Node{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }
}
