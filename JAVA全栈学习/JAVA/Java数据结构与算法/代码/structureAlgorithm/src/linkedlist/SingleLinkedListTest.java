package linkedlist;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/5/29
 * Time: 21:20
 * Description: 单向链表的练习题
 * Version: V1.0
 */
public class SingleLinkedListTest {

    public static void main(String[] args) {
        SinLinkedList sinLinkedList = new SinLinkedList();
        sinLinkedList.addNode(new SinNode(3));
        sinLinkedList.addNode(new SinNode(1));
        sinLinkedList.addNode(new SinNode(4));
        sinLinkedList.addNode(new SinNode(2));
        sinLinkedList.showNode();


        SingleLinkedListTest test = new SingleLinkedListTest();
        // 有效个数
        System.out.println("单链表中有效个数："+test.getLinkedListLength(sinLinkedList.getHead()));
        // 返回倒数第2个节点
        System.out.println("倒数第2个节点："+test.getSinNode(sinLinkedList.getHead(),2));
        // 反转
        test.reverseSinNode(sinLinkedList.getHead());
        System.out.println("链表反转：");
        sinLinkedList.showNode();
        // 倒叙打印
        System.out.println("倒叙打印链表：");
        test.reversePrintNode(sinLinkedList.getHead());
        // 合并有序列表
        System.out.println("1：");
        SinLinkedList sinLinkedList1 = new SinLinkedList();
        sinLinkedList1.addNode(new SinNode(2));
        sinLinkedList1.addNode(new SinNode(5));
        sinLinkedList1.addNode(new SinNode(6));
        sinLinkedList1.addNode(new SinNode(9));
        sinLinkedList1.showNode();

        System.out.println("2：");
        SinLinkedList sinLinkedList2 = new SinLinkedList();
        sinLinkedList2.addNode(new SinNode(1));
        sinLinkedList2.addNode(new SinNode(3));
        sinLinkedList2.addNode(new SinNode(7));
        sinLinkedList2.addNode(new SinNode(12));
        sinLinkedList2.showNode();

        System.out.println("合并：");
        SinNode sinNode = test.merge(sinLinkedList1.getHead(), sinLinkedList2.getHead());
        if(sinNode!=null){
           while (sinNode.next!=null){
               System.out.println(sinNode.next);
               sinNode = sinNode.next;
           }
        }
    }

    // 获得链表有效节点个数
    public int getLinkedListLength(SinNode head){
        SinNode temp = head;
        int length = 0;
        while (true){
            if(temp.next == null){
                return length;
            }
            length++;
            temp = temp.next;
        }
    }

    // 获得倒数第k个节点
    public SinNode getSinNode(SinNode head, int k){
        SinNode temp = head.next;
        int size = getLinkedListLength(head);
        if(temp == null){
            return null;
        }
        if(k<=0 || k>size){
            return null;
        }
        for (int i = 0; i < size-k; i++) {
            temp = temp.next;
        }
        return temp;
    }

    // 链表反转
     public SinNode reverseSinNode(SinNode head){
        // 当链表为空或者链表中只有一个节点的时候，直接返回
        if(head.next == null || head.next.next == null){
            return head;
        }

        SinNode node = new SinNode();
        SinNode reverseNode = node;
        SinNode currentNode = head.next;
        SinNode curNextNode;
        // curNextNode用于记录当前节点的下一个节点，为了保证链表不断裂，能够持续遍历。
        // 因为当前节点加入到新链表中，要想将原链表的当前节点的下一个节点继续添加到新链表的头部，就必须记录下来

        while (currentNode != null){
            // 先把当前节点的下一个节点记录下来
            curNextNode = currentNode.next;
            // 把当前节点的下一个节点连接到反转链表头部的下一个节点，这样反转链表除了头部其余节点都在当前节点后面了
            currentNode.next = reverseNode.next;
            // 将反转链表的头部指向当前节点，形成新的反转链表
            reverseNode.next = currentNode;
            // 最后将，之前记录的原链表的当前节点的下一个节点赋值给当前节点，成为需要添加到反转链表的节点
            currentNode = curNextNode;
        }
        head.next = node.next; // 将原链表指向反转后的链表
        return head;
     }

     // 从尾到头打印链表
    public void reversePrintNode(SinNode head){
        if(head.next == null){
            return;
        }
        SinNode temp = head.next;
        Stack<SinNode> stack = new Stack<>();
        while (temp!=null){
            stack.push(temp);
            temp = temp.next;
        }
        while (!stack.isEmpty()){
            System.out.println(stack.pop());
        }
    }

    // 合并两个有序链表并返回这个合并的链表
    public SinNode merge(SinNode head1, SinNode head2){
        if (head1.next == null){
            return head2;
        }else if (head2.next == null){
            return head1;
        }

        SinNode merge = new SinNode();
        SinNode tempMerge = merge;
        SinNode temp1 = head1.next;
        SinNode temp2 = head2.next;
        SinNode next;

        while (true){
            if(temp1==null && temp2==null){
                break;
            }else if(temp1==null){
                tempMerge.next = temp2;
                break;
            }else if (temp2 == null){
                tempMerge.next = temp1;
                break;
            }

            if(temp1.id < temp2.id){
                tempMerge.next = temp1;
                temp1 = temp1.next;
            }else{
                tempMerge.next = temp2;
                temp2 = temp2.next;
            }
            tempMerge = tempMerge.next;
        }
        return merge;
    }
}

class SinLinkedList{
    private final SinNode head = new SinNode();

    public SinNode getHead() {
        return head;
    }

    public void addNode(SinNode node){
        SinNode temp = head;
        boolean flag = false;
        while (true){
            if(temp.next == null){
                break;
            }
            if(temp.next.id > node.id){
                break;
            }else if(temp.next.id == node.id){
                flag = true;
                break;
            }
            temp = temp.next;
        }
        if(flag){
            System.out.println("链表中已有该节点...");
        }else {
            node.next = temp.next;
            temp.next = node;
        }
    }

    public void showNode(){
        SinNode temp = head.next;
        while (true){
            if(temp==null){
                break;
            }else {
                System.out.println(temp);
            }
            temp = temp.next;
        }
    }

    public void showNode(SinNode node){
        SinNode temp = node.next;
        while (true){
            if(temp==null){
                break;
            }else {
                System.out.println(temp);
            }
            temp = temp.next;
        }
    }
}


class SinNode{
    public int id;
    public SinNode next;

    public SinNode() {
        this.id = 0;
        this.next = null;
    }

    public SinNode(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "linkedlist.SinNode{" +
                "id=" + id +
                '}';
    }
}
