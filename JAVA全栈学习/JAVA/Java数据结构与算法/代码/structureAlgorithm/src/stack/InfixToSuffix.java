package stack;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/5/31
 * Time: 16:03
 * Description: 中缀表达式转后缀表达式
 * 前缀表达式：15+((120+6)*2)-5
 * 后缀表达式：15 120 6 + 2 * + 5 -
 * Version: V1.0
 */
public class InfixToSuffix {
    public static void main(String[] args) {
        String infixExpression = "15+((120+6)*2)-5";
        // 将中缀表达式转换成list集合存储，方便对数据遍历和操作
        List<String> infixListExpression = toInfixListExpression(infixExpression);

        // 根据中缀转后缀规则进行转换
        Stack<String> operStack = new Stack<>(); // 符号栈
        List<String> suffixExpression = new ArrayList<>(); // 存储后缀表达式

        // 遍历list集合中的每一个元素
        for(String item : infixListExpression){
            if(item.matches("\\d+")){ // 如果为数字，直接加入到suffixExpression中
                suffixExpression.add(item);
            }else if (item.equals("(")){ // 如果为左括号，直接加入到suffixExpression中
                operStack.push(item);
            }else if (item.equals(")")){ // 如果为右括号，将左括号之前的符号都加入到suffixExpression中
                if(!operStack.peek().equals("(")){
                    suffixExpression.add(operStack.pop());
                }
                operStack.pop(); // 遇到左括号后，将其pop出
            }else if (!operStack.empty() && operStack.peek().equals("(")){
                // 如果栈不为空，并且栈顶是左括号，运算符号直接入栈
               operStack.push(item);
            }else {
                while (true){
                    // 如果符号栈不为空，并且栈顶不是左括号，就比较运算符优先级，
                    // 如果小于等于栈顶优先级，就将符号取出，再次判断栈是否为空，
                    // 新的栈顶是否为左括号
                    if(!operStack.empty()){
                        if (operStack.peek().equals("(")){
                            break;
                        }else if(operationPriority(item) <= operationPriority(operStack.peek()) ){
                            suffixExpression.add(operStack.pop());
                        }else {
                            break;
                        }
                    }else {
                        break;
                    }

                }
                operStack.push(item);
            }
        }

        // 将栈中剩下的符号依次取出，并加入到suffixExpression中
        while (true){
            if(!operStack.empty()){
                suffixExpression.add(operStack.pop());
            }else {
                break;
            }
        }

        System.out.println("中缀表达式："+infixExpression);
        System.out.println("后缀表达式："+suffixExpression);
    }

    public static List<String> toInfixListExpression(String infix){
        List<String> list = new ArrayList<>();
        int index = 0;
        String val = "";

        while (index < infix.length()){
            val = infix.substring(index,index+1);
            // 如果num是数字，那么就判断下一位是否还是数字，是就继续追加，不是就退出
            while (val.matches("\\d+") && index+1 < infix.length()){
                if(infix.substring(index+1,index+2).matches("\\d+")){
                    val += infix.substring(index+1,index+2);
                    index++;
                }else {
                    break;
                }
            }
            list.add(val);
            index++;
        }

        return list;
    }

    public static int operationPriority(String oper){
        int result;
        switch (oper){
            case "+":
            case "-":
                result = 1;
                break;
            case "*":
            case "/":
                result = 2;
                break;
            default:
                throw new RuntimeException("运算符有误...");
        }
        return result;
    }
}
