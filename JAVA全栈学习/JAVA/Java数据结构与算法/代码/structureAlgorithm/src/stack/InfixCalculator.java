package stack;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/5/31
 * Time: 11:56
 * Description: 中缀表达式 运算结果
 * Version: V1.0
 */
public class InfixCalculator {
    public static void main(String[] args) {
        String expression = "60+2*50-3";

        if(!expression.isEmpty()){
            InfixStack numStack = new InfixStack(expression.length()); // 数栈
            InfixStack operStack = new InfixStack(expression.length()); // 符号栈

            int index = 0; // 用于遍历表达式
            String num = ""; // 用于存储数字
            int oper = 0; // 用于存储符号
            while (true){
                // 获取数字
                char oneChar = expression.charAt(index);
                num += oneChar;
                // 判断下一个字符是不是数字，是数字就加入到num中，不是就跳出
                while (true){
                    if(index < expression.length()-1){ // 判断是否越界
                        if(numStack.isNumber(expression.charAt(index+1))){
                            num += expression.charAt(index+1);
                            index++;
                        }else {
                            index++;
                            break;
                        }
                    }else {
                        break;
                    }
                }
                // 将数字结果放入数栈中
                numStack.push(Integer.parseInt(num));
                // 记录数字的字符串置空
                num = "";

                // 运算符获取
                if(index < expression.length()-1){
                    oper = expression.charAt(index);
                    if(!operStack.isEmpty()){
                        // 判断符号栈中的符号与要入栈符号的优先级别，优先级别小于或等于，就进行运算操作
                        // 把运算结果放入数栈中
                        if(operStack.operPriority(oper) <= operStack.operPriority(operStack.peek())){
                            int num1 = numStack.pop();
                            int num2 = numStack.pop();
                            char operation = (char) operStack.pop();
                            int res = operStack.calculator(num1, num2, operation);
                            numStack.push(res);
                        }
                    }
                    // 如果符号栈为空或者优先级别大于栈顶符号，就直接入栈
                    operStack.push(oper);
                }else {
                    // 如果字符串已经遍历完了，就执行最后的运算，
                    // 符号栈中已经为空，说明已运算完
                    while (!operStack.isEmpty()){
                        int num1 = numStack.pop();
                        int num2 = numStack.pop();
                        char operation = (char) operStack.pop();
                        int res = operStack.calculator(num1, num2, operation);
                        numStack.push(res);
                    }
                    break;
                }

                index++;
            }

            System.out.println(expression+"="+numStack.pop());

        }else {
            System.out.println("表达式无效...");
        }

    }
}

class InfixStack{
    private int maxSize;
    private int top = -1;
    private int arr[];

    public InfixStack(int maxSize) {
        this.maxSize = maxSize;
        this.arr = new int[maxSize];
    }

    public boolean isFull(){
        return top == maxSize-1;
    }

    public boolean isEmpty(){
        return top == -1;
    }

    public int pop(){
        if(!isEmpty()){
            int value = arr[top];
            top--;
            return value;
        }else {
            throw new RuntimeException("栈空...");
        }
    }

    public boolean push(int val){
        if(!isFull()){
            top++;
            arr[top] = val;
            return true;
        }else {
            throw new RuntimeException("栈满...");
        }
    }

    public int peek(){
        if(!isEmpty()){
            return arr[top];
        }else {
            throw new RuntimeException("栈空...");
        }
    }

    public boolean isNumber(int num){
        if( (0<=num && num<=9) || ('0'<=num && num<='9')){
            return true;
        }else {
            return false;
        }
    }

    public int operPriority(int oper){
        if(oper == '*' || oper == '/'){
            return 1;
        }else if (oper == '+' || oper == '-'){
            return 0;
        }else {
            throw new RuntimeException("运算符有误...");
        }
    }

    public int calculator(int num1, int num2, char oper){
        int result = 0;
        switch (oper){
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num2 - num1;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                result = num2 / num1;
                break;
            default:
                break;
        }
        return result;
    }
}
