package stack;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/5/30
 * Time: 21:03
 * Description: 数组模拟栈操作
 * Version: V1.0
 */
public class ArrayStackDemo {
    public static void main(String[] args) {
        ArrayStack arrayStack = new ArrayStack(3);
        System.out.println("栈是否为空："+ arrayStack.isEmpty());

        arrayStack.push(2);
        arrayStack.push(3);
        arrayStack.push(4);

        System.out.println("栈是否满："+ arrayStack.isFull());

        arrayStack.push(5);

        System.out.println("遍历栈元素：");
        arrayStack.showArr();

        System.out.println("移除元素："+arrayStack.pop());
        System.out.println("遍历栈元素：");
        arrayStack.showArr();
        System.out.println("移除元素："+arrayStack.pop());
        System.out.println("移除元素："+arrayStack.pop());
        System.out.println("移除元素："+arrayStack.pop());
        arrayStack.showArr();
    }
}

class ArrayStack{
    private int top = -1;
    private int maxSize;
    private int arr[];

    public ArrayStack(int maxSize) {
        this.maxSize = maxSize;
        arr = new int[maxSize];
    }

    // 判断是否为满
    public boolean isFull(){
        return top == maxSize-1;
    }

    // 判断是否为空
    public boolean isEmpty(){
        return top == -1;
    }

    // 添加元素
    public void push(int num){
        if(!isFull()){
            top++;
            arr[top] = num;
        }else {
            System.out.println("已满，不能再往栈中添加新元素...");
        }
    }

    // 取出元素
    public int pop(){
        if (!isEmpty()){
            int value = arr[top];
            top--;
            return value;
        }else {
            return -1;
        }
    }

    // 遍历
    public void showArr(){
        if(!isEmpty()){
            for (int i = top; i >= 0; i--) {
                System.out.println(arr[i]);
            }
        }else {
            System.out.println("栈中无元素，不可遍历...");
        }
    }
}
