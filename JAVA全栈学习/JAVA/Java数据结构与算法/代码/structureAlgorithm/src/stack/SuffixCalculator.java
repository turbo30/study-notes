package stack;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/5/31
 * Time: 14:09
 * Description: 后缀表达式 运算，逆波兰RPN计算器
 * Version: V1.0
 */
public class SuffixCalculator {
    public static void main(String[] args) {
        // 计算(3+20)*6-5 转为后缀表达式 3 20 + 6 * 5 -
        String suffixExpression = "15 120 6 + 2 * + 5 -";
        String[] rpnExpression = suffixExpression.split(" ");
        int result = calculator(rpnExpression);
        System.out.println("计算结果为："+result);
    }

    public static int calculator(String[] rpn){
        int res = 0;
        Stack<String> stack = new Stack<>();
        // 遍历数组
        for (String item : rpn) {
            // 如果为数字直接入栈，如果为运算符，直接取出栈中元素进行运算
            if(item.matches("\\d+")){
                stack.push(item);
            }else {
                int num1 = Integer.parseInt(stack.pop());
                int num2 = Integer.parseInt(stack.pop());
                switch (item){
                    case "+":
                        res = num1 + num2;
                        break;
                    case "-":
                        res = num2 - num1; // 注意先出栈为被减数
                        break;
                    case "*":
                        res = num1 * num2;
                        break;
                    case "/":
                        res = num2 / num1; // 注意先出栈为被除数
                        break;
                    default:
                        throw new RuntimeException("运算符有误...");
                }
                stack.push(String.valueOf(res));
            }
        }
        return Integer.parseInt(stack.pop());
    }
}
