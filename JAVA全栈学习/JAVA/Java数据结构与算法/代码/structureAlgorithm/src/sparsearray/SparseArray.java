package sparsearray;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/5/28
 * Time: 17:02
 * Description: 数据结构之稀疏数组
 * Version: V1.0
 */
public class SparseArray {
    public static void main(String[] args) throws IOException {
        // 以五子棋为例，定义一个数组，该棋盘9*9，有2个棋子
        int chessArray[][] = new int[9][9];
        chessArray[1][3] = 2;
        chessArray[2][2] = 1;
        // 输出打印，看一下该原始数组
        System.out.println("原始数组：");
        for (int[] ints : chessArray) {
            for (int item : ints) {
                System.out.print(item+"\t");
            }
            System.out.println();
        }

        // 将原始数组转为稀疏数组，遍历原始数组，并将有效值存入到list集合中
        List<Integer> list = new ArrayList<>();
        int sum = 0;
        for (int i = 0; i < chessArray.length; i++) {
            for (int j = 0; j < chessArray[i].length; j++) {
                if(chessArray[i][j] != 0){
                    sum++;
                    list.add(i);
                    list.add(j);
                    list.add(chessArray[i][j]);
                }
            }
        }
        // 创建稀疏数组
        int sparseArray[][] = new int[sum+1][3];
        sparseArray[0][0] = chessArray.length;
        sparseArray[0][1] = chessArray[0].length;
        sparseArray[0][2] = sum;
        int row = 1;
        for (int i = 0; i < list.size() ; i+=3) {
            sparseArray[row][0] = list.get(i);
            sparseArray[row][1] = list.get(i+1);
            sparseArray[row][2] = list.get(i+2);
            row++;
        }
        // 遍历稀疏数组
        System.out.println("稀疏数组：");
        for (int[] ints : sparseArray) {
            System.out.println(ints[0]+"\t"+ints[1]+"\t"+ints[2]);
        }


        // 稀疏数组转为原始数组
        int chessArray2[][] = new int[sparseArray[0][0]][sparseArray[0][1]];
        for (int i = 1; i < sparseArray.length; i++) {
            chessArray2[sparseArray[i][0]][sparseArray[i][1]] = sparseArray[i][2];
        }
        System.out.println("恢复到原始数组：");
        for (int[] ints : chessArray2) {
            for (int item : ints) {
                System.out.print(item+"\t");
            }
            System.out.println();
        }


        /*// 通过IO存入到本地
        File file = new File("sparseArray.data");
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Writer out = new FileWriter("sparseArray.data");
        for (int i = 0; i < sparseArray.length; i++) {
            for (int j = 0; j < sparseArray[i].length; j++) {
                out.write(sparseArray[i][j]+" ");
            }
        }
        out.close();

        Reader in = new FileReader("sparseArray.data");
        char[] chars = new char[1024];
        int len = 0;
        while ((len = in.read(chars)) != -1){
            System.out.println(new String(chars, 0, len));
        }*/
    }
}
