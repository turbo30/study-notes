package queue;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/5/28
 * Time: 18:41
 * Description: 数组模拟队列一次性
 * Version: V1.0
 */
public class ArrayQueueDisposable {

    public static void main(String[] args) {
        ArrayQueueDis arrayQueueDis = new ArrayQueueDis(3);
        System.out.println("队列是否为空："+arrayQueueDis.isEmpty());
        System.out.println();

        System.out.println("添加元素...");
        arrayQueueDis.addQueue(3);
        arrayQueueDis.addQueue(4);
        System.out.println("队列是否已满："+arrayQueueDis.isFull());
        System.out.println("此时头元素："+arrayQueueDis.getHead());
        arrayQueueDis.addQueue(5);
        System.out.println("队列是否已满："+arrayQueueDis.isFull());
        System.out.println("队列遍历：");
        arrayQueueDis.showQueue();

        System.out.println();
        System.out.println("取出第一个元素:");
        System.out.println(arrayQueueDis.getQueue());
        System.out.println("队列遍历：");
        arrayQueueDis.showQueue();

        System.out.println();
        System.out.println("取出第二个元素:");
        System.out.println(arrayQueueDis.getQueue());
        System.out.println("此时头元素："+arrayQueueDis.getHead());
        System.out.println("队列遍历：");
        arrayQueueDis.showQueue();

        System.out.println();
        System.out.println("取出第三个元素:");
        System.out.println(arrayQueueDis.getQueue());
        System.out.println("队列是否为空："+arrayQueueDis.isEmpty());
        System.out.println("队列遍历：");
        arrayQueueDis.showQueue();
    }
}

class ArrayQueueDis{
    private int maxSize; // 数组最大长度
    private int front; // 指向头部元素的前一个位置
    private int rear; // 指向尾部元素的位置
    private int arr[]; // 数组

    public ArrayQueueDis(int maxSize) {
        this.maxSize = maxSize;
        this.rear = -1;
        this.front = -1;
        this.arr = new int[maxSize];
    }

    // 判断是否已满
    public boolean isFull(){
        return rear == maxSize-1;
    }

    // 判断是否为空
    public boolean isEmpty(){
        return rear == front;
    }

    // 向数组队列加入元素
    public boolean addQueue(int num){
        if(isFull()){
            System.out.println("数组已满！");
            return false;
        }
        rear++;
        arr[rear] = num;
        return true;
    }

    // 从数组队列获取元素
    public int getQueue(){
        if(isEmpty()){
            throw new RuntimeException("数组为空！");
        }
        front++;
        int num = arr[front];
        arr[front] = 0;
        return num;
    }

    // 从数组队列中获取头元素
    public int getHead(){
        if(isEmpty()){
            throw new RuntimeException("数组为空！");
        }
        return arr[front+1];
    }

    // 展示数组队列中的所有元素
    public void showQueue(){
        for (int i = 0; i < arr.length; i++) {
            System.out.println("arr["+i+"] "+arr[i]+"\t");
        }
    }
}