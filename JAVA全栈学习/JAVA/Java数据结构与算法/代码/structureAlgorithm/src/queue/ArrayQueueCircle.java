package queue;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/5/29
 * Time: 10:33
 * Description: 数组模拟环形队列
 * Version: V1.0
 */
public class ArrayQueueCircle {
    public static void main(String[] args) {
        ArrayQueueCir arrayQueueCir = new ArrayQueueCir(3);
        System.out.println("队列是否为空："+arrayQueueCir.isEmpty());
        System.out.println();

        System.out.println("添加元素...");
        arrayQueueCir.addQueue(3);
        arrayQueueCir.addQueue(4);
        System.out.println("队列是否已满："+arrayQueueCir.isFull());
        System.out.println("此时头元素："+arrayQueueCir.getHead());
        arrayQueueCir.addQueue(5);
        System.out.println("队列是否已满："+arrayQueueCir.isFull());
        System.out.println("队列遍历：");
        arrayQueueCir.showQueue();

        System.out.println();
        System.out.println("取出第一个元素:");
        System.out.println(arrayQueueCir.getQueue());
        System.out.println("队列遍历：");
        arrayQueueCir.showQueue();

        System.out.println();
        System.out.println("再添加一个新元素...");
        arrayQueueCir.addQueue(5);
        System.out.println("队列遍历：");
        arrayQueueCir.showQueue();

        System.out.println();
        System.out.println("取出第二个元素:");
        System.out.println(arrayQueueCir.getQueue());
        System.out.println("队列是否为空："+arrayQueueCir.isEmpty());
        System.out.println("队列遍历：");
        arrayQueueCir.showQueue();

        System.out.println();
        System.out.println("再添加一个新元素...");
        arrayQueueCir.addQueue(6);
        System.out.println("队列遍历：");
        arrayQueueCir.showQueue();
    }
}

class ArrayQueueCir{
    private int maxSize; // 数组最大长度
    private int front; // 指向头部元素的前一个位置，初始值0
    private int rear; // 指向尾部元素的位置，初始值0
    private int arr[]; // 数组

    public ArrayQueueCir(int arrSize) {
        this.maxSize = arrSize;
        this.arr = new int[arrSize];
    }

    // 判断是否已满
    public boolean isFull(){
        return (rear + 1) % maxSize == front;
    }

    // 判断是否为空
    public boolean isEmpty(){
        return rear == front;
    }

    // 向数组队列加入元素
    public boolean addQueue(int num){
        if(isFull()){
            System.out.println("数组已满，不能再添加元素！");
            return false;
        }
        arr[rear] = num;
        rear = (rear+1)%maxSize;
        return true;
    }

    // 从数组队列获取元素，并删除该元素
    public int getQueue(){
        if(isEmpty()){
            throw new RuntimeException("数组为空！");
        }
        int num = arr[front];
        front = (front+1)%maxSize;
        return num;
    }

    // 从数组队列中获取头元素
    public int getHead(){
        if(isEmpty()){
            throw new RuntimeException("数组为空！");
        }
        return arr[front];
    }

    // 展示数组队列中的所有元素
    public void showQueue(){
        if(!isEmpty()){
            int size = size();
            for (int i = front; i < front+size; i++) {
                System.out.println(arr[(i)%maxSize]+" ");
            }
        }else {
            System.out.println("队列中无元素");
        }
    }

    public int size(){
        return (rear - front + maxSize)%maxSize;
    }
}
