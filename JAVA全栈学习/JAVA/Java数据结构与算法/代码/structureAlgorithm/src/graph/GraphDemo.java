package graph;

import org.omg.PortableInterceptor.INACTIVE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/10
 * Time: 16:29
 * Description: 无向图的创建：邻接矩阵表，深度优先搜索DFS，广度优先搜索BFS
 * Version: V1.0
 */
public class GraphDemo {
    public static void main(String[] args) {
        String[] vertexes = {"A","B","C","D","E"};
        // 构建图，存入顶点
        Graph graph = new Graph(vertexes.length);
        for (String vertex : vertexes) {
            graph.addVertex(vertex);
        }

        // 创建边 A-D B-A B-D B-C D-E
        graph.addEdge(0,3,1);
        graph.addEdge(1,0,1);
        graph.addEdge(1,3,1);
        graph.addEdge(1,2,1);
        graph.addEdge(3,4,1);

        // 创建边 A-B A-C B-C B-D B-E
//        graph.addEdge(0,1,1);
//        graph.addEdge(0,2,1);
//        graph.addEdge(1,2,1);
//        graph.addEdge(1,3,1);
//        graph.addEdge(1,4,1);


        // 深度优先遍历
//        graph.dfs();

        // 广度优先遍历
        graph.bfs();
    }
}

class Graph{
    private ArrayList<String> vertexList; // 存储顶点
    private int edgeCount; // 记录边的个数
    private int[][] edgesTable; // 存储边的矩阵
    private int vertexNum; // 存储图顶点总个数
    private boolean[] isVisited; // 记录每个顶点是否被访问

    public Graph(int num) {
        this.vertexList = new ArrayList<>(num);
        this.edgesTable = new int[num][num];
        this.edgeCount = 0;
        this.vertexNum = num;
        this.isVisited = new boolean[num];
    }

    // 添加顶点
    public void addVertex(String vertex){
        if (vertexList.size() == vertexNum){
            System.out.println("图顶点已满...");
            return;
        }else {
            vertexList.add(vertex);
        }
    }

    // 无向图创建边
    public void addEdge(int v1, int v2, int weight){
        edgesTable[v1][v2] = weight;
        edgesTable[v2][v1] = weight;
        edgeCount++;
    }

    // 返回边的数目
    public int getEdgeCount(){
        return edgeCount;
    }

    // 通过索引，返回顶点值
    public String getVertex(int index){
        if (index < vertexList.size()){
            return vertexList.get(index);
        }else {
            throw new IndexOutOfBoundsException("索引超过存储顶点的长度...");
        }
    }

    // 打印邻接矩阵表
    public void showEdges(){
        for (int[] ints : edgesTable) {
            System.out.println(Arrays.toString(ints));
        }
    }

    // 从头遍历获取当前顶点的第一个邻接节点
    public int getFirstNeighbor(int index){
        for (int i = 0; i < vertexNum; i++) {
            if (edgesTable[index][i] > 0){
                return i;
            }
        }
        return -1;
    }

    // 获取当前顶点的下一个邻接节点，
    // 由于第一个邻接节点已经被访问过了，所以需要访问下一个邻接节点
    public int getNextNeighbor(int index, int nextIndex){
        for (int i = nextIndex+1; i < vertexNum; i++) {
            if (edgesTable[index][i] > 0){
                return i;
            }
        }
        return -1;
    }

    // 对一个节点的深度优先搜索操作
    public void dfs(boolean[] isVisited, int index){
        System.out.print(vertexList.get(index)+"->");
        isVisited[index] = true;
        int w = getFirstNeighbor(index);
        while (w != -1){
            if (!isVisited[w]){
                dfs(isVisited, w);
            }
            // 如果当前节点的第一个邻接节点已经被访问了，
            // 就访问当前节点的下一个邻接节点
            w = getNextNeighbor(index, w);
        }
    }

    // 深度优先搜索遍历
    public void dfs(){
        for (int i = 0; i < vertexNum; i++) {
            // 如果没有被访问就dfs
            if (!isVisited[i]){
                dfs(isVisited, i);
            }
        }
    }

    // 对一个节点的广度优先搜索操作
    public void bfs(boolean[] isVisited, int index){
        int u; // 用于存储从队列中取出的索引
        int w; // 用于存储索引u这一行没有被访问的顶点的索引
        LinkedList<Integer> queue = new LinkedList<>(); // 队列
        System.out.print(vertexList.get(index)+" -> ");
        isVisited[index] = true; // 设置已被访问
        queue.addLast(index); // 索引加入到队列中
        while(!queue.isEmpty()){
            u = queue.removeFirst(); // 从队列中取出第一个索引
            w = getFirstNeighbor(u); // 通过u这一行的索引，找到这一行第一个邻接顶点
            while (w != -1){
                if (!isVisited[w]){
                    System.out.print(vertexList.get(w)+" -> ");
                    queue.addLast(w);
                    isVisited[w] = true;
                }
                w = getNextNeighbor(u, w); // 继续找u这一行的下一个顶点
            }
        }
    }

    // 广度优先搜索遍历
    public void bfs(){
        for (int i = 0; i < vertexNum; i++) {
            if (!isVisited[i]){
                bfs(isVisited,i);
            }
        }
    }
}
