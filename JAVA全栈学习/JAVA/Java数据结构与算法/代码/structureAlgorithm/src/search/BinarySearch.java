package search;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/4
 * Time: 21:34
 * Description: 二分查找法，数组必须有序
 * Version: V1.0
 */
public class BinarySearch {
    public static void main(String[] args) {
        int[] arr = {1,5,11,21,21,21,30};
        //int index = binarySearch(arr, 0, arr.length - 1, 21);
        List<Integer> list = binarySearchAll(arr, 0, arr.length - 1, 0);
        for (Integer integer : list) {
            System.out.println(integer);
        }
        //System.out.println(index);
    }

    public static int binarySearch(int arr[],int left, int right, int value){
        // 没找到的情况
        if (left > right){
            return -1;
        }

        // 获取此范围的中间索引
        int mid = (left+right)/2;

        if (value > arr[mid]){
            // 向右部分查找
            return binarySearch(arr, mid+1, right , value);
        }else if (value < arr[mid]){
            // 向左部分查找
            return binarySearch(arr, left, mid-1, value);
        }else {
            // 找到返回索引
            return mid;
        }
    }

    public static List<Integer> binarySearchAll(int[] arr, int left, int right, int value){
        if (left>right){
            return new ArrayList<>();
        }

        int mid = (left+right)/2;

        if (value > arr[mid]){
            return binarySearchAll(arr, mid+1, right, value);
        }else if (arr[mid] > value){
            return binarySearchAll(arr, left, mid-1, value);
        }else {
            List<Integer> list = new ArrayList<>();
            list.add(mid);
            int index = mid;
            while (true){
                index--;
                if (index < 0 || arr[index] != value){
                    break;
                }
                list.add(index);
            }
            index = mid;
            while (true){
                index++;
                if (index > arr.length || arr[index] != value){
                    break;
                }
                list.add(index);
            }

            return list;
        }
    }
}
