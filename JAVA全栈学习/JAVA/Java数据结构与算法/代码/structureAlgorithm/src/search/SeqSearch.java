package search;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/4
 * Time: 21:17
 * Description: 线性查找算法
 * Version: V1.0
 */
public class SeqSearch {
    public static void main(String[] args) {
        int arr[] = {3,9,-1,-2,20,6};
        int index = seqSearch(arr, -1);
        System.out.println("该数的索引为"+ index);
    }

    public static int seqSearch(int arr[], int value){
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value){
                return i;
            }
        }
        return -1;
    }
}
