package search;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/5
 * Time: 16:54
 * Description: 斐波那契查找算法
 * Version: V1.0
 */
public class FibonacciSearch {
    public static void main(String[] args) {
        int[] arr = {1,5,11,21,121,230};
        int index = fibonacci(arr, 230);
        System.out.println(index);
    }

    public static int fibonacci(int[] arr, int value){
        // 创建一个斐波那契数组
        int f[] = new int[20];
        f[0] = 1;
        f[1] = 1;
        for (int i = 2; i < f.length; i++) {
            f[i] = f[i-1]+f[i-2];
        }

        // 找到斐波那契数组中等于或略大于arr数组长度的值
        int high = arr.length-1;
        int low = 0;
        int k = 0;
        while (high > f[k]-1){
            k++;
        }

        // 创建一个长度为f[k]的临时数组，并将多出的空间，赋值为原数组最后的数值
        int temp[] = Arrays.copyOf(arr,f[k]);
        for (int i = high+1; i < temp.length; i++) {
            temp[i] = arr[high];
        }

        while (low <= high){
            // 中间索引
            int mid = low+f[k-1]-1;
            // 如果value值大于中值，就往右边查找
            // 如果value值小于中值，就往左边查找
            if (temp[mid] < value){
                low = mid+1;
                k-=2; // 减2，是因为右边的步长为2
            }else if (temp[mid] > value){
                high = mid-1;
                k--;
            }else {
                // 返回最小索引，因为temp数组可能是扩大了的，后面扩充的数字是与
                // 原数组最后一个元素是一样的，所以，当mid大于原数组的最大索引是，
                // 这个数一定是原数组的最后一个，所以返回high，否则返回mid
                if (mid <= high){
                    return mid;
                }else {
                    return high;
                }
            }

        }
        return -1;
    }
}
