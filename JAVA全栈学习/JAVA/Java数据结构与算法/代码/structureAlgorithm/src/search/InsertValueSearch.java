package search;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/4
 * Time: 22:45
 * Description:
 * Version: V1.0
 */
public class InsertValueSearch {
    public static void main(String[] args) {
        int arr[] = new int[100];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i+1;
        }
        int index = insertValueSearch(arr, 0, arr.length - 1, 1);
        System.out.println(index);
    }

    public static int insertValueSearch(int[] arr, int low, int high, int value){
        // 一定要写value < arr[0] || value > arr[arr.length-1]，否则会出现越界情况
        // 一定要把小于最小值和大于最大值的数据排除掉
        if (low>high || value < arr[0] || value > arr[arr.length-1]){
            return -1;
        }

        int mid = low+(high-low)*(value-arr[low])/(arr[high]-arr[low]);

        if (value < arr[mid]){
            return insertValueSearch(arr, low, mid-1, value);
        }else if (value > arr[mid]){
            return insertValueSearch(arr, mid+1, high , value);
        }else {
            return mid;
        }
    }
}
