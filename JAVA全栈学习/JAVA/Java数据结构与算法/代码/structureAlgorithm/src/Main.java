import java.util.ArrayList;
import java.util.Collections;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/20
 * Time: 20:09
 * Description:
 * Version: V1.0
 */
public class Main {
    public static void main(String[] args) {
        int arr[] = {4,23,1,6};
        H h = new H();
        h.Huffman(arr);
    }

}

class H{
     public void Huffman(int arr[]){
         ArrayList<HNode> list = new ArrayList<>();
         for (int i = 0; i < arr.length; i++) {
             list.add(new HNode(arr[i]));
         }

         while (list.size() > 1){
             Collections.sort(list);

             HNode left = list.get(0);
             HNode right = list.get(1);
             HNode node = new HNode(left.id + right.id);
             node.left = left;
             node.right = right;

             list.remove(left);
             list.remove(right);
             list.add(node);
         }

         list.get(0).infixOrder();
     }
}

class HNode implements Comparable<HNode>{
    int id;
    HNode left;
    HNode right;

    public HNode(int id) {
        this.id = id;
    }

    public void infixOrder(){
        if (this.left!=null){
            this.left.infixOrder();
        }
        System.out.println(this);
        if (this.right!=null){
            this.right.infixOrder();
        }
    }

    @Override
    public String toString() {
        return "HNode{" +
                "id=" + id +
                '}';
    }

    @Override
    public int compareTo(HNode o) {
        return this.id - o.id;
    }
}
