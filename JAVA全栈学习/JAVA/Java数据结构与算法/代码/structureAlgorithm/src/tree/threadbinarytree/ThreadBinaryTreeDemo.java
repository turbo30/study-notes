package tree.threadbinarytree;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/6
 * Time: 19:31
 * Description: 线索化二叉树
 * Version: V1.0
 */
public class ThreadBinaryTreeDemo {
    public static void main(String[] args) {
        Node node1 = new Node(1);
        Node node2 = new Node(3);
        Node node3 = new Node(6);
        Node node4 = new Node(8);
        Node node5 = new Node(10);
        Node node6 = new Node(14);

        node1.setLeft(node2);
        node1.setRight(node3);
        node2.setLeft(node4);
        node2.setRight(node5);
        node3.setLeft(node6);

        ThreadBinaryTree threadBinaryTree = new ThreadBinaryTree(node1);
//        threadBinaryTree.perThread(); // 前序线索化二叉树
//        threadBinaryTree.perOrder(); // 通过前序遍历

        threadBinaryTree.infixThread(); // 中序线索化二叉树
//        threadBinaryTree.infixOrder(); // 通过中序遍历
        threadBinaryTree.infixThreadList();
        System.out.println("id为8节点的后继节点为："+node4.getRight());
        System.out.println("id为10节点的前驱节点为："+node5.getLeft());
        System.out.println("id为10节点的后继节点为："+node5.getRight());
        System.out.println("id为14节点的前驱节点为："+node6.getLeft());
        System.out.println("id为14节点的后继节点为："+node6.getRight());

    }
}

class Node{
    private int id;
    private Node left;
    private Node right;
    // 设置左右节点的类型，0为左节点或右节点，1为前驱节点或后继节点
    private int leftType;
    private int rightType;

    public Node(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public int getLeftType() {
        return leftType;
    }

    public void setLeftType(int leftType) {
        this.leftType = leftType;
    }

    public int getRightType() {
        return rightType;
    }

    public void setRightType(int rightType) {
        this.rightType = rightType;
    }

    @Override
    public String toString() {
        return "Node{" +
                "id=" + id +
                ", leftType=" + leftType +
                ", rightType=" + rightType +
                '}';
    }

    // 前序遍历：只遍历左子节点和右子节点，不遍历前驱节点和后继节点
    public void perOrder(){
        System.out.println(this.toString());

        if (this.left!=null && this.getLeftType()==0){
            this.left.perOrder();
        }

        if (this.right!=null && this.getRightType()==0){
            this.right.perOrder();
        }
    }

    // 中序遍历：只遍历左子节点和右子节点，不遍历前驱节点和后继节点
    public void infixOrder(){
        if (this.left!=null && this.leftType == 0){
            this.left.infixOrder();
        }

        System.out.println(this.toString());

        if (this.right!=null && this.rightType == 0){
            this.right.infixOrder();
        }
    }

    // 后序遍历：只遍历左子节点和右子节点，不遍历前驱节点和后继节点
    public void suffixOrder(){
        if (this.left!=null && this.leftType == 0){
            this.left.suffixOrder();
        }
        if (this.right!=null && this.rightType == 0){
            this.right.suffixOrder();
        }
        System.out.println(this.toString());
    }
}

class ThreadBinaryTree{
    private Node root;
    // 用于记录前驱节点，每要遍历下一个节点时，
    // 就需要将当前节点记录下来，作为下一个节点的前驱节点
    // 如果前驱节点的右子节点为空，那么就可以设置前驱节点的后继节点
    // 后继节点也就是当前节点
    private Node pre;

    public ThreadBinaryTree(Node node) {
        this.root = node;
    }

    // 创建一个前序索引
    public void perThread(){
        perThread(root);
    }
    public void perThread(Node node){
        if (node == null){
            return;
        }

        // 处理当前节点
        // 先处理左节点，如果左子节点为空，那么就将上一个节点设置为左节点的前驱节点，
        // 这里不判断pre是否为空，是没有关系的，如果pre为空，说明该节点是线索化的第一个节点
        // 有利于根据线索指针进行遍历
        if (node.getLeft() == null){
            node.setLeft(pre);
            node.setLeftType(1);
        }
        // 由于后继节点需要遍历到下一个节点才能确定，所以，遍历到下一个节点时
        // 才能设置前驱节点的右子节点指向当前的node
        if (pre!=null && pre.getRight() == null){
            pre.setRight(node);
            pre.setRightType(1);
        }
        // 用于记录前驱节点，每要遍历下一个节点时，
        // 就需要将当前节点记录下来，作为下一个节点的前驱节点
        pre = node;

        // 获取左节点，一定要判断是否是原节点，否则会陷入死循环
        if (node.getLeftType()==0){
            perThread(node.getLeft());
        }

        // 获取右节点，一定要判断是否是原节点，否则会陷入死循环
        if (node.getRightType()==0){
            perThread(node.getRight());
        }
    }

    // 创建一个中序序列化二叉树
    public void infixThread(){
        infixThread(root);
    }
    public void infixThread(Node node){
        if (node==null){
            return;
        }

        if (node.getLeftType() == 0){
            infixThread(node.getLeft());
        }

        if (node.getLeft() == null){
            node.setLeft(pre);
            node.setLeftType(1);
        }
        if (pre!=null && pre.getRight() == null){
            pre.setRight(node);
            pre.setRightType(1);
        }
        pre = node;

        if (node.getRightType()==0){
            infixThread(node.getRight());
        }
    }

    // 创建一个后序线索化
    public void suffixThread(){
        suffixThread(root);
    }
    public void suffixThread(Node node){
        if (node==null){
            return;
        }
        if (node.getLeftType() == 0){
            suffixThread(node.getLeft());
        }
        if (node.getRightType() == 0){
            suffixThread(node.getRight());
        }

        if (node.getLeft() == null){
            node.setLeft(pre);
            node.setLeftType(1);
        }
        if (pre!=null && pre.getRight() == null){
            pre.setRight(node);
            pre.setRightType(1);
        }
        pre = node;
    }

    // 根据线索指针进行前序线索二叉树遍历
    public void perThreadList(){
        Node node = root;
        while (node!=null){
            while (node.getLeftType() == 0){
                System.out.println(node.toString());
                node = node.getLeft();
            }
            System.out.println(node.toString());
            while (node.getRightType()==1){
                node = node.getRight();
                System.out.println(node.toString());
            }
            node = node.getRight();
        }
    }

    // 根据线索指针进行中序线索二叉树遍历
    public void infixThreadList(){
        // 定义一个变量，存储当前节点
        Node node = root;
        while (node != null){
            // 第一次是要找到左节点为空，并且左节点类型为1，也就是中序线索化的二叉树第一个节点
            while (node.getLeftType() == 0){
                node = node.getLeft();
            }
            // 找到了就输出
            System.out.println(node.toString());

            while (node.getRightType() == 1){
                node = node.getRight();
                System.out.println(node.toString());
            }
            node = node.getRight();
        }
    }

    // 根据线索指针进行后序线索二叉树遍历
    public void suffixThreadList(){

    }

    // 前序遍历形式遍历线索二叉树
    public void perOrder(){
        if (root!=null){
            root.perOrder();
        }else {
            System.out.println("二叉树为空");
        }
    }

    // 中序遍历形式遍历线索二叉树
    public void infixOrder(){
        if (root!=null){
            root.infixOrder();
        }else {
            System.out.println("二叉树为空");
        }
    }

    // 后序遍历形式遍历线索二叉树
    public void suffixOrder(){
        if (root!=null){
            root.suffixOrder();
        }else {
            System.out.println("二叉树为空");
        }
    }
}