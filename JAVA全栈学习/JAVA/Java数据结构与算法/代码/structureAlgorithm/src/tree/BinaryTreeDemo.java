package tree;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/6
 * Time: 13:00
 * Description: 二叉树，前中后序遍历
 * Version: V1.0
 */
public class BinaryTreeDemo {

    public static void main(String[] args) {
        // 创建节点
        EmpNode node1 = new EmpNode(1,"heroc");
        EmpNode node2 = new EmpNode(2,"lucy");
        EmpNode node3 = new EmpNode(3,"smith");
        EmpNode node4 = new EmpNode(4,"tom");
        EmpNode node5 = new EmpNode(5,"jack");

        // 创建树
        node1.setLeft(node2);
        node2.setLeft(node3);
        node1.setRight(node4);
        node4.setLeft(node5);

        // 确定根节点，遍历树
        BinaryTree rootNode = new BinaryTree(node1);
//        System.out.println("前序：");
//        rootNode.perOrder();
//        System.out.println("中序：");
//        rootNode.centerOrder();
//        System.out.println("后序：");
//        rootNode.afterOrder();

        // 前序遍历查询
        EmpNode empNode = rootNode.perOrderFind(5);
        if (empNode!=null){
            System.out.println(empNode.toString());
        }

        // 中序遍历查询
        empNode = rootNode.perOrderFind(2);
        if (empNode!=null){
            System.out.println(empNode.toString());
        }

        // 后序遍历查询
        empNode = rootNode.afterOrderFind(4);
        if (empNode!=null){
            System.out.println(empNode.toString());
        }
    }
}

// 创建节点
class EmpNode{
    private int id;
    private String name;
    private EmpNode left;
    private EmpNode right;

    public EmpNode(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EmpNode getLeft() {
        return left;
    }

    public void setLeft(EmpNode left) {
        this.left = left;
    }

    public EmpNode getRight() {
        return right;
    }

    public void setRight(EmpNode right) {
        this.right = right;
    }

    @Override
    public String toString() {
        return "EmpNode{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    // 前序遍历
    public void perOrder(){
        System.out.println(this.toString());
        if (this.left!=null){
            this.left.perOrder();
        }
        if (this.right!=null){
            this.right.perOrder();
        }
    }

    // 中序遍历
    public void centerOrder(){
        if (this.left!=null){
            this.left.centerOrder();
        }
        System.out.println(this.toString());
        if (this.right!=null){
            this.right.centerOrder();
        }
    }

    // 后续遍历
    public void afterOrder(){
        if (this.left!=null){
            this.left.afterOrder();
        }
        if (this.right!=null){
            this.right.afterOrder();
        }
        System.out.println(this.toString());
    }

    // 前序遍历查询
    public EmpNode perOrderFind(int id){
        if (this.id == id){
            return this;
        }

        EmpNode empNode = null;
        if (this.left!=null){
            empNode = this.left.perOrderFind(id);
        }
        if (empNode!=null){
            return empNode;
        }

        if (this.right!=null){
            empNode = this.right.perOrderFind(id);
        }
        if (empNode!=null){
            return empNode;
        }

        return null;
    }

    // 中序遍历查询
    public EmpNode centerOrderFind(int id){
        EmpNode empNode = null;
        // 向左查找
        if (this.left!=null){
            empNode = this.left.centerOrderFind(id);
        }
        if (empNode!=null){ // 如果empNode不等null，说明找到了，就返回结果
            return empNode;
        }

        // 当前节点
        if (this.id == id){
            return this;
        }

        // 向右查找
        if (this.right != null){
            empNode = this.right.centerOrderFind(id);
        }
        if (empNode!=null){
            return empNode;
        }

        // 都没找到返回null
        return empNode;
    }

    // 后序遍历查询
    public EmpNode afterOrderFind(int id){
        EmpNode empNode = null;
        // 向左查找
        if (this.left!=null){
            empNode = this.left.centerOrderFind(id);
        }
        if (empNode!=null){ // 如果empNode不等null，说明找到了，就返回结果
            return empNode;
        }

        // 向右查找
        if (this.right != null){
            empNode = this.right.centerOrderFind(id);
        }
        if (empNode!=null){
            return empNode;
        }

        // 当前节点
        if (this.id == id){
            return this;
        }

        return empNode;
    }

}

// 创建根节点
class BinaryTree{
    private EmpNode root;

    public BinaryTree(EmpNode root) {
        this.root = root;
    }

    public void perOrder(){
        if (root!=null){
            root.perOrder();
        }else {
            System.out.println("树为空");
        }
    }
    public void centerOrder(){
        if (root!=null){
            root.centerOrder();
        }else {
            System.out.println("树为空");
        }
    }
    public void afterOrder(){
        if (root!=null){
            root.afterOrder();
        }else {
            System.out.println("树为空");
        }
    }
    public EmpNode perOrderFind(int id){
        if (root!=null){
            return root.perOrderFind(id);
        }else {
            System.out.println("树为空");
            return null;
        }
    }
    public EmpNode centerOrderFind(int id){
        if (root!=null){
            return root.centerOrderFind(id);
        }else {
            System.out.println("树为空");
            return null;
        }
    }
    public EmpNode afterOrderFind(int id){
        if (root!=null){
            return root.afterOrderFind(id);
        }else {
            System.out.println("树为空");
            return null;
        }
    }
}