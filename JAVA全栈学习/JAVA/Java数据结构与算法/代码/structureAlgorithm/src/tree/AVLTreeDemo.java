package tree;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/9
 * Time: 15:23
 * Description: 平衡二叉树
 * 每个节点的左右子树高度差不能超过1
 * Version: V1.0
 */
public class AVLTreeDemo {
    public static void main(String[] args) {
        //int[] arr = {4,3,6,5,7,8}; // 左旋转测试
        //int[] arr = {10,12,8,9,7,6}; // 右旋转测试
        int[] arr = {10,11,7,6,8,9}; // 双旋转测试
        AvlTree avlTree = new AvlTree();
        for (int i = 0; i < arr.length; i++) {
            avlTree.add(new AvlNode(arr[i]));
        }

        avlTree.infixOrder();
        System.out.println("高："+avlTree.getRoot().getTreeHeight());
        System.out.println("右："+avlTree.getRoot().getRightTreeHeight());
        System.out.println("左："+avlTree.getRoot().getLeftTreeHeight());
        System.out.println(avlTree.getRoot().value);
    }
}

class AvlTree{
    private AvlNode root;

    public void setRoot(AvlNode root) {
        this.root = root;
    }

    public AvlTree() {
    }

    public AvlTree(AvlNode root) {
        this.root = root;
    }

    public AvlNode getRoot() {
        return root;
    }

    public void add(AvlNode node){
        if (root==null){
            root=node;
        }else {
            root.add(node);
        }
    }

    public void infixOrder(){
        if (root==null){
            return;
        }else {
            root.infixOrder();
        }
    }

}

class AvlNode{
    int value;
    AvlNode left;
    AvlNode right;

    public AvlNode(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "AvlNode{" +
                "value=" + value +
                '}';
    }

    public void infixOrder(){
        if (this.left!=null){
            this.left.infixOrder();
        }
        System.out.println(this.toString());
        if (this.right!=null){
            this.right.infixOrder();
        }
    }

    // 二叉排序树添加节点操作
    public void add(AvlNode node){
        if (node==null){
            return;
        }
        if (node.value < this.value){
            if (this.left!=null){
                this.left.add(node);
            }else {
                this.left = node;
            }
        }else {
            if (this.right!=null){
                this.right.add(node);
            }else {
                this.right = node;
            }
        }

        // 在添加的过程中，就动态调整树的高度
        // 如果当前节点的右子树与左子树的高度差大于1，则向左旋转
        if (this.getRightTreeHeight()-this.getLeftTreeHeight() > 1){
            // 在向左旋转的时候，需要判断当前节点的右子树的左子树与右子树的高度差，
            // 如果当前节点的右子树的左子树高于当前节点的右子树的右子树，那么就需要将
            // 当前节点的右子树的子树向右旋转做一个平衡调整，再让当前节点向左旋转，才能保证平衡
            if (this.right.getLeftTreeHeight() > this.right.getRightTreeHeight()){
                this.right.rightRotate();
                this.leftRotate();
            }else {
                this.leftRotate(); // 向左旋转
            }
            return; // 一定要记得return，否则会出现严重错误
        }

        // 如果当前节点的左子树与右子树的高度差大于1，则向右旋转
        if (this.getLeftTreeHeight() - this.getRightTreeHeight() > 1){
            if (this.left.getRightTreeHeight() > this.left.getLeftTreeHeight()){
                this.left.leftRotate();
                this.rightRotate();
            }else {
                this.rightRotate(); // 向右旋转
            }
        }
    }

    // 获得高度
    public int getTreeHeight(){
        return Math.max(this.left==null ? 0 : this.left.getTreeHeight(), this.right==null ? 0 : this.right.getTreeHeight())+1;
    }

    // 获取当前节点的右子树的高度
    public int getRightTreeHeight(){
        if (this.right!=null){
            return this.right.getTreeHeight();
        }
        return 0;
    }

    // 获取当前节点的左子树的高度
    public int getLeftTreeHeight(){
        if (this.left!=null){
            return this.left.getTreeHeight();
        }
        return 0;
    }

    // 向左旋转
    public void leftRotate(){
        AvlNode newNode = new AvlNode(this.value);
        newNode.left = this.left;
        newNode.right = this.right.left;
        this.value = this.right.value;
        this.right = this.right.right;
        this.left = newNode;
    }

    // 向右旋转
    public void rightRotate(){
        AvlNode newNode = new AvlNode(this.value);
        newNode.right = this.right;
        newNode.left = this.left.right;
        this.value = this.left.value;
        this.left = this.left.left;
        this.right = newNode;
    }
}