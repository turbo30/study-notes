package tree;

import java.util.*;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/7
 * Time: 19:26
 * Description: 哈夫曼编码与解码
 * Version: V1.0
 */
public class HuffmanCode {
    public static void main(String[] args) {
        String content = "i love love love java you";
        System.out.println("字符串："+content);
        byte[] encode = encode(content);
        System.out.println("压缩后得到的哈夫曼编码字节数组："+Arrays.toString(encode));
        byte[] decode = decode(encode);
        System.out.println("解码后："+new String(decode));
    }

    // 创建一个哈夫曼树
    public static CodeNode getHuffmanTree(byte[] contentBytes){
        // 将字符串转换成字节，统计每个字节出现的次数存储到map中
        Map<Byte, Integer> contentMap = new HashMap<>();
        for (byte contentByte : contentBytes) {
            if (contentMap.get(contentByte) == null){
                contentMap.put(contentByte,1);
            }else {
                contentMap.put(contentByte,contentMap.get(contentByte)+1);
            }
        }

        // 遍历map，每个entry创建一个codeNode节点，存储到list中便于创建哈夫曼树
        List<CodeNode> codeNodeList = new ArrayList<>();
        for(Map.Entry<Byte,Integer> mapEntry: contentMap.entrySet()){
            codeNodeList.add(new CodeNode(mapEntry.getKey(),mapEntry.getValue()));
        }

        // 创建哈夫曼树
        while (codeNodeList.size() > 1){
            Collections.sort(codeNodeList);

            CodeNode leftNode = codeNodeList.get(0);
            CodeNode rightNode = codeNodeList.get(1);
            CodeNode parentNode = new CodeNode(null,leftNode.getWeight()+rightNode.getWeight());
            parentNode.setLeft(leftNode);
            parentNode.setRight(rightNode);

            codeNodeList.remove(leftNode);
            codeNodeList.remove(rightNode);
            codeNodeList.add(parentNode);
        }
        return codeNodeList.get(0);
    }

    // 根据哈夫曼树，左路为0，右路为1，获得编码表
    static Map<Byte, String> codeMap = new HashMap<>();
    public static Map<Byte,String> getCodeMap(CodeNode node, String code, StringBuilder stringBuilder){
        StringBuilder strBuilder = new StringBuilder(stringBuilder);
        strBuilder.append(code);
        if (node != null){
            if (node.getData() == null){
                // 向左
                getCodeMap(node.getLeft(),"0",strBuilder);
                // 向右
                getCodeMap(node.getRight(),"1",strBuilder);
            }else {
                codeMap.put(node.getData(),strBuilder.toString());
            }
        }
        return codeMap;
    }

    // codeSize记录编码字符串的长度，用于解决编码后最后一个数存在01110这种类似情况的编码
    // 如果存在这种01110编码，在转为byte时存入14这个数字
    // 而在解码的时候，因为14是数组的最后一个，在转为二进制字符串如果直接追加1110，在对照编码表的时候
    // 会出错，因此需要判断在追加1110后，解析后的编码长度是不是加密时的编码长度一样，如果不一样就需要在
    // 1110前面追加0，直到与加密时的长度一致，才可以解析成功
    static int codeSize = 0;
    // 获取字符串的最终编码
    public static byte[] encode(String content){
        byte[] contentBytes = content.getBytes();
        // 创建哈夫曼树
        CodeNode huffmanTree = getHuffmanTree(contentBytes);

        // 获得哈夫曼树的编码表
        Map<Byte, String> codeMap = getCodeMap(huffmanTree, "", new StringBuilder());

        // 获得字符串的编码字符串
        StringBuilder encodeBuilder = new StringBuilder();
        for (byte contentByte : contentBytes) {
            encodeBuilder.append(codeMap.get(contentByte));
        }

        // 将编码字符串8位一组存入byte数组中
        String encodeString = encodeBuilder.toString();
        codeSize = encodeString.length(); // 存储编码有多少位
        // System.out.println(encodeString);
        // 输出：101010001011111111001000101111111100...
        // 获取前8位，10101000字符串转换成字节
        // Integer.parseInt("10101000",2)：将10101000以2进制为基准解析成十进制的int类型，
        // 这里的10101000为补码，反码为10101000-1=10100111，正码为11011000，最高位为符号位得出-88
        int size = (encodeString.length()+7)/8;
        byte[] codeBytes = new byte[size];
        int count = 0;
        for (int i = 0; i < codeBytes.length; i++) {
            if (count+8 < encodeString.length()){
                codeBytes[i] = (byte) Integer.parseInt(encodeString.substring(count,count+8),2);
                count+=8;
            }else {
                codeBytes[i] = (byte) Integer.parseInt(encodeString.substring(count),2);
            }
        }
        return codeBytes;
    }

    // 反转编码表，用于解析使用
    public static Map<String,Byte> getReCodeMap(){
        Map<String, Byte> reCodeMap = new HashMap<>();
        for (Map.Entry<Byte, String> entry : codeMap.entrySet()) {
            reCodeMap.put(entry.getValue(),entry.getKey());
        }
        return reCodeMap;
    }

    public static byte[] decode(byte[] encode){
        StringBuilder decodeBuilder = new StringBuilder();
        Map<String, Byte> reCodeMap = getReCodeMap();
        // 获取哈夫曼编码字符串
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] < 0){
                String str = Integer.toBinaryString(encode[i]);
                decodeBuilder.append(str.substring(str.length()-8));
            }else if (i != encode.length-1){
                // 如果是正数并且不是数组最后一位数，需要给正数补位，用256与运算补位即可
                int temp = encode[i] | 256;
                // 256的二进制是1 0000 0000与正数与运算
                // 比如256与77二进制100 1101与运算 得到1 0100 1101，截取8位即可
                String str = Integer.toBinaryString(temp);
                decodeBuilder.append(str.substring(str.length()-8));
            }else {
                // 最后一位不需要转换成8位数，直接添加到末尾即可
                String lastCode = Integer.toBinaryString(encode[i]);
                // 判断最后一个数转成二进制代码后，解析二进制编码总长度是否与加密时候一致
                if (codeSize != (i*8+lastCode.length())){
                    // 不一致，就在最后一个数前面加差数个的0字符串，然后重置最后一个编码
                    int gap = codeSize- (i*8+lastCode.length());
                    String str = "";
                    for (int j = 0; j < gap; j++) {
                        str+="0";
                    }
                    lastCode = str + lastCode;
                }
                decodeBuilder.append(lastCode);
            }
        }
        // System.out.println(decodeBuilder.toString());

        // 扫描字符串进行解析
        String decodeStr = decodeBuilder.toString();
        List<Byte> decodeList = new ArrayList<>();
        for (int i = 0; i < decodeStr.length(); ) {
            int count = 1;
            while (true){
                String sub = decodeStr.substring(i, i + count);
                Byte aByte = reCodeMap.get(sub);
                if (aByte!=null){
                    decodeList.add(aByte);
                    break;
                }else {
                    count++;
                }
            }
            i = i+count;
        }

        byte[] decode = new byte[decodeList.size()];
        int index = 0;
        for (Byte aByte : decodeList) {
            decode[index++] = aByte;
        }

        return decode;
    }

}

// 创建节点
class CodeNode implements Comparable<CodeNode>{
    private Byte data;  // 数据
    private int weight;  // 数据在content中出现的次数，权重
    private CodeNode left;
    private CodeNode right;

    public CodeNode(Byte data, int weight) {
        this.data = data;
        this.weight = weight;
    }

    public Byte getData() {
        return data;
    }

    public void setData(Byte data) {
        this.data = data;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public CodeNode getLeft() {
        return left;
    }

    public void setLeft(CodeNode left) {
        this.left = left;
    }

    public CodeNode getRight() {
        return right;
    }

    public void setRight(CodeNode right) {
        this.right = right;
    }

    @Override
    public String toString() {
        return "CodeNode{" +
                "data=" + data +
                ", weight=" + weight +
                '}';
    }

    @Override
    public int compareTo(CodeNode o) {
        return this.weight - o.weight;
    }

    public void preOrder(){
        System.out.println(this.toString());
        if (this.left!=null){
            this.left.preOrder();
        }
        if (this.right!=null){
            this.right.preOrder();
        }
    }
}
