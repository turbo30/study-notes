package tree;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/6
 * Time: 18:43
 * Description: 顺序存储的二叉树数组
 * Version: V1.0
 */
public class ArrayBinaryTreeDemo {
    public static void main(String[] args) {
        int arr[] = {1,2,3,4,5,6,7}; // 这是一个二叉树从根节点依次顺序存储的数组
        ArrayBinaryTree arrayBinaryTree = new ArrayBinaryTree(arr);
        System.out.println("前序遍历：");
        arrayBinaryTree.perOrder();
        System.out.println("\n中序遍历：");
        arrayBinaryTree.infixOrder();
        System.out.println("\n后序遍历：");
        arrayBinaryTree.suffixOrder();
    }
}

class ArrayBinaryTree{
    int arr[];

    public ArrayBinaryTree(int[] arr) {
        this.arr = arr;
    }

    // 前序遍历顺序存储的二叉树数组
    public void perOrder(){
        perOrder(0);
    }
    public void perOrder(int index){
        System.out.print(arr[index]+" ");

        if ((2*index+1) < arr.length){
            perOrder(2*index+1);
        }

        if ((2*index+2) < arr.length){
            perOrder(2*index+2);
        }
    }

    // 中序遍历顺序存储的二叉树数组
    public void infixOrder(){
        infixOrder(0);
    }
    public void infixOrder(int index){
        if ((2*index+1) < arr.length){
            infixOrder(2*index+1);
        }

        System.out.print(arr[index]+" ");

        if ((2*index+2) < arr.length){
            infixOrder(2*index+2);
        }
    }

    // 后序遍历顺序存储的二叉树数组
    public void suffixOrder(){
        suffixOrder(0);
    }
    public void suffixOrder(int index){
        if ((2*index+1) < arr.length){
            suffixOrder(2*index+1);
        }

        if ((2*index+2) < arr.length){
            suffixOrder(2*index+2);
        }

        System.out.print(arr[index]+" ");
    }
}
