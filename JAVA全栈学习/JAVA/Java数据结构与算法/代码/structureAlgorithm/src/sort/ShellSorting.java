package sort;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/2
 * Time: 17:48
 * Description: 希尔排序法
 * 是直接插入排序的增强版，但不稳定
 * Version: V1.0
 */
public class ShellSorting {
    public static void main(String[] args) {
        int arr[] = {3,9,-1,10,20,6};
        System.out.println("排序前："+ Arrays.toString(arr));
        sort(arr);
        System.out.println("排序后："+Arrays.toString(arr));
    }

    public static void sort(int[] arr){

        // 确定每次循环的间隔，步长gap，初始步长一般为数组长度的一般，
        // 之后步长是上一次步长的一半
        for (int gap = arr.length/2 ; gap > 0 ; gap /= 2) {
            // 从第一个步长开始向前插入操作。加入步长为2，分组为0，2，4...
            // 那么就从索引为2开始，向前比较插值
            for (int i = gap; i < arr.length; i++) {
                int value = arr[i];
                int index = i-gap;
                while (index >= 0 && arr[index] > value){
                    arr[index+gap] = arr[index];
                    index-=gap;
                }
                if(index+gap != i){
                    arr[index+gap] = value;
                }
            }
        }
    }
}
