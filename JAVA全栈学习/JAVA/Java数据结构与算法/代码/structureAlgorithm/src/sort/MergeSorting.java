package sort;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/3
 * Time: 13:07
 * Description: 归并排序法
 * Version: V1.0
 */
public class MergeSorting {
    public static void main(String[] args) {
        int arr[] = {3,9,-1,10,20,6};
        int temp[] = new int[arr.length];
        System.out.println("排序前："+ Arrays.toString(arr));
        sort(arr,0,arr.length-1,temp);
        System.out.println("排序后："+Arrays.toString(arr));
    }

    /**
     * 拆分后合并操作
     * @param arr 原数组
     * @param left 原数组的左索引
     * @param right 原数组的右索引
     * @param temp 临时数组
     */
    public static void sort(int[] arr, int left, int right, int[] temp){
        // 如果左索引小于右索引 就继续拆分，先左拆分，拆分到不能拆分为止，
        // 再右拆分到不能拆分为止，最后将最后拆分的结果合并
        if (left < right){
            int mid = (left+right)/2;
            // 往左拆分
            sort(arr, left, mid, temp);
            // 往右拆分
            sort(arr, mid+1, right, temp);
            // 合并
            merge(arr, left, mid, right, temp);
        }
    }


    /**
     * 合并操作
     * @param arr 分割的原数组
     * @param left 最左边的索引
     * @param mid 中间索引
     * @param right 最右边的索引
     * @param temp 临时存储的数组
     */
    public static void merge(int[] arr, int left, int mid, int right, int[] temp){
        int i = left; // 从最左边开始的索引
        int j = mid+1; // 从中间索引的下一个索引开始的索引
        int t = 0; // 临时数组的起始索引

        while ( i <= mid && j <= right){
            // 左右两部分进行比较，小的往临时数组里放
            if (arr[i] <= arr[j]){
                temp[t] = arr[i];
                t++;
                i++;
            }else {
                temp[t] = arr[j];
                j++;
                t++;
            }
        }

        // 总有一边的数据先放完，另一边的数据就一次放到临时数组
        while (i <= mid){
            temp[t] = arr[i];
            t++;
            i++;
        }
        while (j <= right){
            temp[t] = arr[j];
            t++;
            j++;
        }

        // 组后将临时数组的数据全部拷贝到原数组对应位置
        t = 0;
        int tempLeft = left; // 原数组拆分后的起始索引位置
        while (tempLeft <= right){
            arr[tempLeft] = temp[t];
            tempLeft++;
            t++;
        }
    }
}
