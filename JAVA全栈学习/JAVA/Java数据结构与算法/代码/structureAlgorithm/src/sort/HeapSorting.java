package sort;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/7
 * Time: 11:42
 * Description: 堆排序(树的应用)
 * 升序使用大顶堆
 * 降序使用小顶堆
 * Version: V1.0
 */
public class HeapSorting {
    public static void main(String[] args) {
        int arr[] = {4,6,8,5,9};
        System.out.println("通过堆排序得到升序：");
        heapSort(arr);
    }

    // 核心部分
    public static void adjustHeap(int[] arr, int i, int length){
        int temp = arr[i]; // 记录非叶子节点(父节点)
        // j为非叶子节点的左子节点
        for (int j = 2*i+1; j < length; j = j*2+1) {
            // 如果有右子节点并且左子节点小于右子节点，将j指向右子节点
            // 否子依旧指向左子节点
            if (j+1<length && arr[j]<arr[j+1]){
               j++;
            }
            // 如果右子节点(左子节点)大于父节点，那么就将子节点赋值给父节点的位置
            // 并将父节点的i指向当前节点，目的为了下一次比较以及值得交换
            if (arr[j] > temp){
                arr[i] = arr[j];
                i=j;
            }else {
                break;
            }
        }
        // 此时的i已经指向了当前节点，即将当前节点赋值为原先的父节点
        arr[i] = temp;
    }

    public static void heapSort(int[] arr){
        int temp;

        // 从最后一个非叶子节点开始依次往后遍历，创建一个大顶堆
        for (int i = arr.length/2-1; i >= 0 ; i--) {
            adjustHeap(arr, i, arr.length);
        }

        // 大顶堆创建完了，就开始排序了
        for (int j = arr.length-1; j >=0 ; j--) {
            // 交换，因为每次大顶堆调整完之后，最大的数都在根节点，
            // 因此需要将最大的数放在数组的后面，进行交换
            temp = arr[j];
            arr[j] = arr[0];
            arr[0] = temp;
            // 交换完之后，大顶堆被打乱，因此有需要调整成为大顶堆
            // 因为每次交换都是讲0索引的值与当前数组最后一个值进行交换，
            // 被打乱的是索引为0的根节点，所以就从根节点开始调整，长度变成了j
            adjustHeap(arr, 0, j);
        }

        System.out.println(Arrays.toString(arr));
    }
}
