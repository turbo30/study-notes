package sort;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/2
 * Time: 16:25
 * Description: 插入排序法
 * Version: V1.0
 */
public class InsertSorting {
    public static void main(String[] args) {
        int arr[] = {3,9,-1,10,20};
        System.out.println("排序前："+ Arrays.toString(arr));
        sort(arr);
        System.out.println("排序后："+Arrays.toString(arr));
    }

    public static void sort(int[] arr){
        int index; // 记录被比较值的索引
        int value; // 记录要去比较‘有序队列’中所有值的值

        for (int i = 1; i < arr.length; i++) {
            value = arr[i]; // 将需要插入的值记录下来，以便进行插入操作
            index = i-1; // 表示从当前索引的前一个位置的索引，用于确定比较起始位置
            // 如果当前值比前一个位置值小，就将前一个位置的值往后挪
            while (index >= 0 && arr[index] > value){
                arr[index + 1] = arr[index]; // 往后挪
                index--; // 进行--操作，比较再前一个位置的值
            }
            // 如果index索引对应的值比value小，就将value的值插入到该值的后面
            if (index+1 != i){ // 判断index+1是否有改变，没改变说明没有进行过交换，就没有比较赋值一次了
                arr[index+1] = value;
            }
        }
    }
}
