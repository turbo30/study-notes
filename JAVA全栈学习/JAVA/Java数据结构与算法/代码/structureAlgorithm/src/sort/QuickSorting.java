package sort;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/2
 * Time: 19:43
 * Description: 快速排序法
 * Version: V1.0
 */
public class QuickSorting {
    public static void main(String[] args) {
        int arr[] = {3,9,-1,-2,20,6};
        System.out.println("排序前："+ Arrays.toString(arr));
        sort(arr,0,arr.length/2,arr.length-1);
        System.out.println("排序后："+Arrays.toString(arr));
    }

    public static void sort(int[] arr, int start,int mid, int end){
        int left = start; // 创建一个left指针，指向第一个索引开始遍历
        int right = end; // 创建一个right指针，指向最后一个往左开始遍历
        int standard = arr[mid]; // 指定一个标准值，用于参考
        int temp; // 用于交换的临时变量

        // 循环结束条件时left必须小于right
        while (left < right){
            // 先让left索引，找到比标准值大的值
            while (arr[left] < standard){
                left++;
            }
            // 再让right索引，找到比标准值小的值
            while (arr[right] > standard){
                right--;
            }

            // 如果两个索引相等，说明本轮循环已结束，
            // 满足标准值左边小于标准值，右边都大于标准值
            if (left >= right){
                break;
            }

            // 进行值交换
            temp = arr[left];
            arr[left] = arr[right];
            arr[right] = temp;

            // 判断是否有指向了标准值，有就让对方索引进行响应的变化
            if (arr[left] == standard){
                right-=1;
            }
            if (arr[right] == standard){
                left+=1;
            }
        }

        // 这一步很重要，一定要判断索引是否一样，一样就调整
        // 不然容易栈溢出，死循环
        if (left == right){
            left++;
            right--;
        }
        // 判断进行下一组的循环
        if (start < right){
            sort(arr,start,(start+right)/2,right);
        }
        if (left < end){
            sort(arr,left,(left+end)/2 ,end);
        }
    }
}
