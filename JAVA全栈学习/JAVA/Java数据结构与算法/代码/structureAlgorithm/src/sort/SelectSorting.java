package sort;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/2
 * Time: 15:22
 * Description: 选择排序
 * 第一趟遍历时，找到最小的值，与arr[0]做交换，第二趟遍历时就从arr[1]开始，
 * 找到最小的值与arr[1]交换，依次类推，最终获得有序的排序。从大到小，反之。
 * Version: V1.0
 */
public class SelectSorting {
    public static void main(String[] args) {
        int arr[] = {3,9,-1,-2,20};
        System.out.println("排序前："+Arrays.toString(arr));
        sort(arr);
        System.out.println("排序后："+Arrays.toString(arr));
    }

    public static void sort(int[] arr){
        int minIndex = 0; // 记录最小值的索引
        int min; // 记录最小值，方便做数据交换

        // 第一个for循环，用于确定起始值，为了与后面元素做比较
        // 每循环一次，确定一个最小的值放在前面
        for (int i = 0; i < arr.length-1; i++) {
            min = arr[i];
            // 第二个for循环，从起始位置的下一位开始的每一位元素与其做比较
            for (int j = i; j < arr.length-1; j++) {
                // 遇到小的数据，就记录索引和值
                if(min > arr[j+1]){
                    minIndex = j+1;
                    min = arr[minIndex];
                }
            }
            // 在与后面的数据比较完之后，发现最小的还是本轮起始位置的值，那么就没必要做交换
            if (minIndex != i){
                arr[minIndex] = arr[i];
                arr[i] = min;
            }
        }
    }
}
