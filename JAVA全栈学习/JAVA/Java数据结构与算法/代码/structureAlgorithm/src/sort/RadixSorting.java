package sort;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/3
 * Time: 23:01
 * Description: 基数排序法，只支持正数
 * 如果需要排序负数和正数混合的数组，就得将正数和负数分出来，到各自的数组
 * 然后对各自的数组进行装桶即可。负数判断与正数刚好相反
 * Version: V1.0
 */
public class RadixSorting {
    public static void main(String[] args) {
        int arr[] = {3,9,1,10,20,6};
        System.out.println("排序前："+ Arrays.toString(arr));
        sort(arr);
        System.out.println("排序后："+Arrays.toString(arr));
    }

    public static void sort(int[] arr){
        // 桶，第一个索引记录是第几个桶，第二个索引记录桶中存放的值
        // 每个桶的大小都是arr.length，而不是每个桶都有数据，所以消耗空间内存
        int bucket[][] = new int[10][arr.length];
        // 这个数组用于记录每个桶中的元素个数
        int bucketElementsCount[] = new int[10];

        // 获取数组中最大的数，计算有多少位
        int max = arr[0];
        for (int i : arr) {
            if (i>max){
                max = i;
            }
        }
        int digit = (max+"").length();

        // 最大数的位数，决定了基数排序要轮回多少次，即digit-1次
        for (int i = 0, n = 1; i < digit; i++, n *= 10) {
            // 每一轮的基数都有变化，第一轮为个位，第二轮位十位，第三轮为百位，以此类推
            for (int j = 0; j < arr.length; j++) {
                //int bucketNumber = arr[j] / (int)Math.pow(10,i) % 10;
                int bucketNumber = arr[j] / n % 10;
                // 将数装到bucketNumber的桶中索引为bucketElementsCount[bucketNumber]的位置
                bucket[bucketNumber][bucketElementsCount[bucketNumber]] = arr[j];
                // 自增，桶中的元素在增加
                bucketElementsCount[bucketNumber]++;
            }

            // 每个数都到了自己对应的桶中之后，需要将桶中的数据都重新填到arr中
            int index = 0;
            for (int c = 0; c < bucketElementsCount.length; c++) {
                // 如果不等于0，所以记录的该桶中是有数据的
                if (bucketElementsCount[c] != 0){
                    for (int k = 0; k < bucketElementsCount[c]; k++) {
                        arr[index++] = bucket[c][k];
                    }
                }
            }

            // 重置记录桶中元素的个数为0，为下一次记录做准备
            for (int j = 0; j < bucketElementsCount.length; j++) {
                bucketElementsCount[j] = 0;
            }
        }
    }
}
