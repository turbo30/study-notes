package sort;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/6/1
 * Time: 21:43
 * Description: 冒泡排序法以及优化，时间复杂度O(n^2)
 * Version: V1.0
 */
public class BubbleSorting {
    public static void main(String[] args) {
        int arr[] = {3,9,-1,10,20};
        System.out.println("排序前："+Arrays.toString(arr));
        bubbleSort(arr);
        System.out.println("排序后："+Arrays.toString(arr));
    }

    public static void bubbleSort(int[] arr){
        boolean flag = true; // 该布尔值，如果出现有一趟循环一次都没有交换过，就说明排序完成，直接退出即可
        int temp;
        for (int i = 1; i < arr.length; i++) { // 趟数的循环，每循环一趟，就确定一个最大值到数组最后一位
            for (int j = 0; j < arr.length-i; j++) { // 从索引0开始比较到上一趟确定位置的前一个位置为止
                if(arr[j] > arr[j+1]){ // 大的往后挪
                    temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                    flag = false;
                }
            }
            if (flag){
                // System.out.println("循环比较了"+i+"趟");
                break;
            }else {
                flag = true;
            }
        }
    }

}
