# IDEA开发工具设置的VM options参数，项目上线后怎么在tomcat进行设置

![](https://gitee.com/herocheung/study_pic/raw/master/pictureForIdea/tomcat启动项VMoptions.png)

项目上线就没办法用idea，这个时候需要修改tomcat的配置。  

找到tomcat的bin目录，找到catalina.bat文件（catalina.sh是Linux操作系统需要修改的文件，具体怎么修改另行百度）  添加以下代码：

![](https://gitee.com/herocheung/study_pic/raw/master/pictureForIdea/tomcat配置VMoptions.png)

```bat
set JAVA_OPTS=%JAVA_OPTS% -Dfile.encoding=UTF-8
```



# IDEA环境解决html网页乱码

在tomcat的配置中的 VM options 上加上。

`-Dfile.encoding=UTF-8 `，重新运行容器，清除浏览器缓存后，网页乱码解决了，

但是容器控制台中文信息就已经乱码了。

淇℃伅 [原本是‘信息’]

于是修改idea64.exe.vmoptions 信息，在最后加上：

```bat
-Dfile.encoding=UTF-8
-Dsun.jnu.encoding=UTF-8
```

重启idea ，解决问题。