# MySql (Linux)



## 一、通过宝塔安装mysql

[宝塔面板][https://www.bt.cn/]



## 二、Linux下启动mysql

### 1、启动
1. **使用linux命令service 启动（常用）：**

   ```bash
   service mysqld start
   ```

2. **使用 mysqld 脚本启动：**

   ```bash
   /etc/inint.d/mysqld start
   ```

3. **使用 safe_mysqld 启动：**

   ```bash
   safe_mysqld&
   ```

   

### 2、停止
1. **使用 service 启动（常用）：**

   ```bash
   service mysqld stop
   ```

2. **使用 mysqld 脚本启动：**

   ```bash
   /etc/inint.d/mysqld stop
   ```

3.  

   ```bash
   mysqladmin shutdown
   ```

   

### 3、重启
1. 使用 service 启动：

   ```bash
   service mysqld restart
   ```

2. 使用 mysqld 脚本启动：

   ```bash
   /etc/inint.d/mysqld restart
   ```

   

备注：查看mysql端口是否已经使用，使用`netstat -anp` 命令查看服务器端口使用情况。或者使用`ps -ef | grep mysql`命令



## 二、mysql远程连接



### 1、最重要的两步

第一步：在服务器上打开3306端口

第二步：给MySQL设置一个能在任意IP地址登录的用户

==这两者缺一不可==



### 2、防火墙开启端口3306

```bash
# 检查防火墙是否在运行 绿色为正在运行
systemctl status firewalld 

# 查看防火墙是否放行了3306端口
firewall-cmd --list-ports

# 没有就在防火墙放行端口3306
firewall-cmd --zone=public --add-port=3306/tcp --permanent

# 重启防火墙
systemctl restart firewall
```

==注意：==

如果你的防火墙没有开启，可以不用管端口的问题

如果你是用的阿里云的Linux主机，一定还要到==安全策略==里面打开端口！！！！



### 3、mysql添加用户、对用户设置

```bash
# 看看MySQL启动没有
systemctl status mysqld
# 没有就启动一下
systemctl start mysqld  
 
#已经启动了
#那就进入 MySQL monitor设置用户
mysql -u root -p
```

1。进入mysql之后，设置root用户的host为%就可以远程连接了

```sql
-- 使用mysql
use mysql;

-- 更新root的host地址为%
update user set host="%" where user="root";

-- 刷新
flush privileges
```

2。也可以新建用户并设置权限，也可指定ip地址，不指定ip地址就用%

```sql
-- 使用mysql
use mysql

-- 创建新用户 指定用户只能登录的ip地址
insert into user (host,user,password) values('223.85.182.166','heroc',password('123'));

-- 查询用户
select host, user from user;

-- 给用户增删改的权限
-- *.* 表示对于所有库所有表，也可以指定使用哪个数据库比如：test.*，该权限只能用于test库
GRANT select，insert，update，delete ON *.* TO heroc@"223.85.182.166" IDENTIFIED BY "123456";

-- 刷新权限
flush privileges

-- 撤销所有权限
REVOKE ALL ON *.* FROM 'heroc'@'223.85.182.166';

-- 删除用户
drop user 'heroc'@'223.85.182.166';

-- 注意：每次修改了权限之后都要手动刷新权限
------------------------------------------------------------------------------------

-- 这时给用户所有权限的语句
-- WITH GRANT OPTION 该用户可以授予其他用户权限
GRANT ALL PRIVILEGES ON *.* TO heroc@"223.85.182.166" IDENTIFIED BY "123456" WITH GRANT OPTION;

-- ip段
update user set host="223.85.182.%" where user="root";
```



### 4、远程连接

以上步骤完成之后，就可以远程连接了。

如果使用的是阿里云服务器，那么就通过公网ip连接即可！



## 三、查看连接mysql的ip地址

```sql
SELECT SUBSTRING_INDEX(HOST,':',1) AS ip , COUNT(*) FROM information_schema.processlist GROUP BY ip;
```



## 四、尽量不用%来设置访问mysql数据库的地址

```sql
USE mysql;


UPDATE USER SET HOST = <new_host> WHERE HOST = '%'


SELECT USER,HOST FROM USER
```



## 五、登录免密

在配置文件中的mysqld下加入以下：

```
[mysqld]
skip-grant-tables
```



# sql

```sql
USE mysql;

INSERT INTO USER (HOST,USER,PASSWORD) 
VALUES('223.85.182.166','heroc',PASSWORD('123'));

SELECT HOST, USER FROM USER;

SELECT * FROM USER

GRANT SELECT ON test.* TO 'heroc'@'223.85.182.166';

REVOKE ALL ON *.* FROM 'heroc'@'223.85.182.166';

DROP USER 'heroc'@'223.85.182.166';

FLUSH PRIVILEGES

SHOW GRANTS FOR 'heroc'@'223.85.182.166'
```



[https://www.bt.cn/]: 
