# SQL 基础



## 一、MySQL概念

配置文件 ：my.ini

```ini
[mysqld]
# 在该位置添加下面这句话，可设置时区，不添加该句话，默认SYSTEM时区
default-time-zone='+08:00';
```



几个概念：

- 数据库：文件夹
- 表：文件
- 数据：存储的数据



**关系型数据库：**

​		通过表与表之间，行与列之间的关系进行数据的存储。(MySQL、Oracle等)

**非关系型数据库：**

​		对象存储，通过对象的自身的属性来决定。(Redis、MongDB等)



==DB(数据库)==  作用存储数据

==DBMS(数据库管理系统)==  作用管理和操作数据



## 二、SQL

### 1、SQL基本概念

SQL (Structured Query Language)：结构化查询语言

其实是定义了操作所有关系型数据库的规则，一种语法。(MySQL、Oracle等都是关系型数据库)每一个种数据库软件操作的方式有细微不一样的地方，称之为“方言”。	



### 2、SQL通用语法

- SQL以分号结尾。
- 可使用空格和缩进来增强语言的可读性。
- SQL语句不区分大小写，建议关键字使用大写。
- 注释：
  + 单行注释 `-- ` 或者 `#` ：注意前者书写完之后要加空格，后者不用加空格
  + 多行注释`/* */`



### 3、SQL分类

1. **DDL(Data Definition Language)**数据定义语言

   用于操作数据库、表、列等。

2. **DML(Data Manipulation Language)**数据操作语言

   用于对数据的增删改的操作。

3. **DQL(Data Query Language)**数据查询语言

   用于对数据查询的操作。

4. **DCL(Data Control Language)**数据控制语言 (了解)

   用于定义数据库的访问权限和安全级别，以及创建用户。



### 4、==重要==MyISAM与InnoDB的区别

|              | MyISAM           | InnoDB            |
| ------------ | ---------------- | ----------------- |
| 事务         | 不支持           | 支持              |
| 数据行锁     | 不支持(支持表锁) | 支持              |
| 外键约束     | 不支持           | 支持              |
| 全文索引     | 支持             | 不支持            |
| 存储空间大小 | 较小             | 大约是MyISAM的2倍 |

常用使用操作：

- MyISAM   空间小，速度快
- InnoDB    安全性高，事务处理，多表多用户操作



## 三、DDL



### 1、操作数据库：CRUD

**C：create 创建**

+ `create database <数据库名>`  创建数据库

+ `create database if not exists <数据库名>` 创建数据库时检查所创建的数据库是否存在，如果存在则不会创建也不会报错，如果不存在则会创建该数据库

+ `create database <数据库名> character set <字符集>` 设置数据库的字符编码

+ `create database if not exists <数据库名> character set <字符集>` 

  

**R：retrieve 查询**

+ `show databases` 查询所有数据库的名称

+ `show create database <表名> ` 查询某个数据库使用的字符集(查询某个数据库的查询语句)，查看数据库创建的SQL语句

  

**U：update 修改** 

- `alter database <数据库名> character set <字符集>` 修改数据库的字符编码



**D：delete 删除**

- `drop database <数据库名>` 删除数据库
- `drop database if exists <数据库名>` 如果数据库存在，则删除该数据库



### 2、使用数据库

- `select database()` 查询正在使用的数据库
- `use <数据库名>` 使用指定的数据库



### 3、操作表：CRUD

##### 常用类型

tinyint：十分小的数据	(1字节)

samllint：较小的数据	(2字节)

mediumint：中等的数据	(3字节)

**int：标准存储	(4字节)     常用**

bigint：较大的数据	(8字节)



float：单精度浮点数	(4字节)

double(有多少个数,小数精确位)：double(5,2) 	则可表示为 999.99	(8字节)

decimal(有多少个数,小数精确位)：字符串形式的浮点数



**char：字符串固定大小	 0~255**

**varchar：可变字符串 	0~65535**

tinytext：微型文本	2^8-1

**text：文本串	2^16-1** (用于保存大文本)



date：年月日，yyyy-MM-dd

time：时分秒，HH:mm:ss

**datatime：年月日时分秒，yyyy-MM-dd HH:mm:ss** (最常用的)

**timestamp：时间戳类型，年月日时分秒，yyyy-MM-dd HH:mm:ss** (不用给定值，它会自动给定一个时间戳。计算机时间原点1970.1.1)

```sql
insert_time timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;
-- 在创建时间戳时，最好加上该句话，以免数据库不添加。这位是为了添加数据时自动给定时间戳。
```

year：年份表示



<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/decimal.png" style="float:left;" />



##### <u>字段属性(重点)</u>

**Unsigned：**

- 无符号的整数
- 使用了该属性则声明了该列不能为负数

**zerofill：**

- 零填充
- 比如：int(3) 定义了3个长度的int，你输入的是1，那么会用0将其他位给填充上，结果为001

**auto_increment：**

- 自增，默认+1
- 通常用来设计唯一的主键，index，必须是整数类型
- 可自定义自增的起始值以及步长

**not null：**

- 非空，该字段必须存入值

**null：**

- 不设定，默认为null
- 该字段可为空

**default：**

- 默认值，如果不指定该字段的值，则会自动填入默认值

**comment：**

- 给字段一个描述，备注



##### <u>必须字段设置 (重点)</u>

> **阿里巴巴手册**
>
> 【强制】每一个表中必须得包含以下字段

- id： 主键
- version：每一个表都需要一个version的字段，用于记录该版本，多线程的乐观锁会使用到
- is_delete：伪删除，如果执行了“删除”操作，就将该字段的值设置一个删除标识，而不是真正的删除该条记录，为了用于开发人员可以看到用户删除了的信息，或其他操作。
- create_time：记录创建时间
- update_time：记录修改时间



#### 1）创建表

**C：create 创建**

```sql
-- 标准创建表的语句
CREATE TABLE IF NOT EXISTS `student`(
    `id` INT(3) ZEROFILL UNSIGNED AUTO_INCREMENT COMMENT 'ID', -- 主键,零填充,无符号,自增1
    `name` VARCHAR(32) COMMENT '姓名', 
    `age` INT COMMENT '年龄', 
    `sex` VARCHAR(2) DEFAULT '男' COMMENT '性别',
    `version` INT NOT NULL DEFAULT 1 COMMENT '版本号',
    `gmt_create` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
    `is_delete` INT NOT NULL DEFAULT 0 COMMENT '删除记录标识',
    `gmt_update` INT NOT NULL DEFAULT 0 COMMENT '更新记录时间',
    `course_id` INT COMMENT '课程表外键',
    PRIMARY KEY(`id`),
    CONSTRAINT `FK_student_cources` FOREIGN KEY (`course_id`) REFERENCES course(`id`)
)ENGINE INNODB DEFAULT CHARSET utf8 COMMENT '对表的注释';
```



#### 5）复制表

- `create table <表A> like <表B>` 将表B复制到新创建表A



#### 3）查询表

**R：retrieve 查询**

+ `show tables()` 查询数据库中的所有表
+ `desc <表名>` 查询表结构   desc=describe
+ `show create table <表名>` 查询表信息



#### 4）修改表 (对表中列的增删改查)

**U：update 修改** 

- `alter table <表名A> rename to(as) <表名B>` 修改表名，将表名A改成表名B
- `alter table <表名> character set <字符集>` 修改表的字符集
- `alter table <表名> add <列名> <数据类型>` 修改表，添加列
- `alter table <表名> change <旧列名> <新列名> <数据类型或约束>` 对字段重命名，并可修改字段的类型和约束
- `alter table <表名> modify <列名> <新数据类型>`  修改表的列的数据类型
- `alter table <表名> drop <列名>` 修改表，删除指定列



##### <u>change与modify的区别</u>

change：用于字段的重命名，并可修改字段的类型和约束

modify：只能用于修改字段的类型和约束



#### 5）删除表

**D：delete 删除**

- `drop table <表名>` 删除指定的表
- `drop table if exists <表名>` 删除存在的指定的表



## 四、DML

对数据的增删改查。

### 1、添加数据

`insert into 表名(列名1,列名2...列名n) values(值1,值2...值n)` 向表中插入值

```sql
insert into student(id,name,age,gmt_create) values(1,'张还行',18,current_time);
```

- 列名类型与值要一一对应；
- 如果不指定列名，那么就默认给所有列插入数据，该插入也要一一对应；
- 除了数字类型，其他类型需要用引号。日期默认格式`'yyyy-MM-dd'`



### 2、删除数据

**删除表中符合条件数据：**

`delete from <表名> where <条件>` 从表名中删除满足条件的数据

```sql
delete from student where id = 2;
```

---

**删除表中所有数据：**

`delete from <表名>` 删除表中所有的数据，表中有多少条数据，就会执行该语句多少次，效率低，并且自增不会清空

`truncate table <表名>` 这句话，会删除整张表，并会创建一个一模一样的空表，效率高==推荐==



#### <u>delete与truncate的区别</u>

相同的：

- 都可以清空表中的所有数据

不同点：

- delete清空表，不会清除自增；truncate会删除这个表，并重新创建一个一模一样的表
- delete支持事务，truncate不支持事务



使用delete删除问题：

- InnoDB 重启数据库现象，自增会从1开始 (自增数据存在内存中，断电即失)
- MyISAM 重启数据库现象，继续从上一个自增量开始 (存在文件中，不会丢失)



### 3、修改数据

`update <表名> set <列名> = <新值> , ... where <条件>` 修改数据

```sql
update student set score = 100.0 , name = 'heroC' where id = 1;
```

---

**修改表中某列的所有数据值：**

`update <表名> set <列名> = <新值> , ... ` 不加where语句，则会更改该列的所有数据





## 五、DQL (最重要)

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/select完整语法.png)



### 1、DQL查询基础

**多个字段查询：**

`select * from 表名` 查询表中所有数据

**去重复查询：**

`select distinct <列名> from <表名> ` 查询列名下不重复的数据

**拼接字符串：**

`select concat(a,b) from <表名>`  a为追加的数据，b为需要被追加的字段 

```sql
select concat('姓名：',name) from student;
-- name字段存储的是 heroC
-- 经过concat加工，查询结果：
-- 姓名：heroC
```

**查询自增的步长：**

`select @@auto_increment_increment` 

**查询系统版本：**

`select version()`

**ifnull(expr1，expr2)、取别名、计算列：**

```sql
select name,math,english,ifnull(math,0)+ifnull(english,0) as 'score' from stu;
-- 查询姓名，数学，英语，数学与英语之和的成绩。
-- 其中ifnull(expr1，expr2) 表示判断expr1是否为null，如果为null，则替换为0进行计算。
```



```sql
SELECT VERSION(); -- 查询当前版本
SELECT CURRENT_DATE(); -- 查询当前日期 yyyy-MM-dd
SELECT CURRENT_TIME(); -- 查询当前时间 HH:mm:ss
SELECT NOW(); -- 查询当前日期和时间 yyyy-MM-dd HH:mm:ss
```



### 2、条件查询

where 语句之后使用

- `>` 	大于		`>=`          大于等于
- `<`     小于		`<=`          小于等于
- `=`     等于        `!=`、`<>`         不等于
- `and`     并且         `between A and B`     [A,B]包含A包含B，以及之间的数据
- `or`       或者
- `in(A,B,C)` 只要包含A,B,C的数据就可
- `is null` 查询是null值的数据
- `is not null` 查询不是null值的数据



### 3、模糊查询

关键字 `like`

占位符：`_` 单个任意字符；`%` 多个任意字符

```sql
select * from stu where name like '张%';
-- 查询姓 张 的记录
select * from stu where name like '%还%';
-- 查询含有 还 的记录
select * from stu where name like '___';
-- 通过3个 _ ，查询姓名是3个字的人
select * from stu where name like '_里%';
-- 查询第二个字为 里 的人
```



#### <u>禁止使用左模糊和全模糊查询</u>

> **阿里巴巴手册**
>
> 强制规定不能使用左模糊和全模糊进行查询，因为使用左模糊和全模糊查询不会使用索引，大大降低了查询效率。
>
> 解决：让给搜索引擎来解决筛选查询。
>
> 测试：
>
> ```sql
> -- 没有创建索引，通过右模糊查询
> EXPLAIN SELECT * FROM sal WHERE `name` LIKE ('e%');
> -- 结果：查询了8行，用的where查询
> ```
>
> <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/模糊与索引1.png" style="float:left" />
>
> ```sql
> -- 给sal表的name字段创建一个名为idx_name索引
> CREATE INDEX idx_name ON sal(`name`);
> EXPLAIN SELECT * FROM sal WHERE `name` LIKE ('e%');
> -- 结果：查询了1行，用的index查询
> ```
>
> <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/模糊与索引2.png" style="float:left" />
>
> ```sql
> -- 在创建了index索引之后，使用左模糊查询
> EXPLAIN SELECT * FROM sal WHERE REVERSE(`name`) LIKE ('%e');
> EXPLAIN SELECT * FROM sal WHERE REVERSE(`name`) LIKE ('_y%');
> EXPLAIN SELECT * FROM sal WHERE REVERSE(`name`) LIKE REVERSE('%y');
> -- 结果：无论使用左模糊、全模糊、左模糊反转，查询了8行，用的where查询，不会使用索引去查询
> ```
>
> <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/模糊与索引3.png" style="float:left" />
>
> 因此，得出结论，左模糊和全模糊查询不会使用索引，大大降低查询效率。



### 4、排序查询

升序为**ASC**（默认）

降序为**DESC**

`select * from <表名> order by 字段1 排序方式1, 字段2 排序方式2`

```sql
select * from stu oreder by math desc, english desc;
-- 查询stu表中的所有数据，按照数学成绩降序排列，如果数学成绩一样，则按照英语成绩降序排列
```



### 5、聚合查询 (常用)

将一列数据作为一个整体，进行纵向计算。==**注意**：聚合函数的计算会自动排除null值==

1. count(列名)：计算个数

   ```sql
   select COUNT(name) from stu;
   -- 计算stu表中name列中有多少个数据，如果name列中有null值的记录，就不会算入其中
   select COUNT(*) from stu;
   -- 计算stu表中有多少条数据，只要一行数据中有值，那么就会算入
   select COUNT(1) from stu;
   -- 计算stu表中有多少条数据，每一条记录的字段不会全部走完，直接计算行数
   ```

2. max(列名)：计算最大值

   ```sql
   select MAX(math) from stu;
   -- 查找math列表中最大值
   ```

3. min()：计算最小值

   ```sql
   select MIN(math) from stu;
   -- 查找math列表中最小值
   ```

4. sum()：求和

   ```sql
   select SUM(math) from stu;
   -- 计算math列中所有数据之和，排除null值
   ```

5. avg()：计算平均值

   ```sql
   select AVG(math) from stu;
   -- 计算math列表中数据的平均数，也会排除null值
   ```

   

### 6、分组查询

`select <被分组的列,聚合函数> from <表名> group by <需要分组的列> having <条件>`

```sql
select sex, AVG(math) from stu group by sex;
-- 以性别分组，查询性别，以及每个性别中数学平均成绩
select sex, avg(math),count(id) from stu where math > 70 group by sex;
-- 以性别分组，查询性别，以及每个性别大于70分的数学平均成绩，并查询大于70分的人数
select sex, avg(math),count(id)'num' from stu where math > 70 group by sex having num>1;
-- 在查询到最终结果之后，通过having在对查询之后的结果进行限定，num要大于1
```



#### <u>where与having的区别</u>

- where在分组之前对表的数据进行限定，而having在分组之后对产生的新表进行限定。
- where不可与聚合函数连用，而having可以与聚合函数连用。





### 7、分页查询

`select * from stu limit 开始索引, 需要查询的条数`

```sql
select * from stu limit 0,3;
-- 查询stu表中从 索引0 开始的3条记录   (第几页-1)*每页需要显示的条数
```



## 六、约束

对表中的数据进行限定，从而保证数据的正确性、有效性和完整性。

- 主键约束：primary key

- 非空约束：not null

- 唯一约束(唯一索引)：unique
- 外键约束：foreign key



### 1、主键约束

`primary key`

主键约束：非空且唯一，一张表只能有一个字段为主键(如同身份证号)

**添加主键约束：**

- 在创建表的时候添加主键约束

  ```sql
  create table stu(id int primary key, name varchar(32) not null);
  ```

- 在创建表之后添加主键约束

  ```sql
  alter table stu modify id int primary key;
  ```



**==删除主键约束：==**

```sql
alter table stu drop primary key;
```



####  .  自动增长

`auto_increment` 关键字可使得值自动增长，**一般配合主键一起使用**。自动增长只与上一条数据有关系，根据上一条数据实现自动增长。

**添加自动增长：**

```sql
-- 在创建表的时候添加自动增长
create table stu(id int primary key auto_increment, name varchar(32) not null);
-- 在创建之后添加自动增长
alter table stu modify id int auto_increment;
```

**删除自动增长：**

```sql
alter table stu modify id int;
```



#### . 联合主键

多对多的情况下，会产生中间表，中间表有两个字段分为两个表的主键的外键，为了保证唯一性，该两个字段可联合主键

`primary key(字段1, 字段2);`



### 2、非空约束

`not null`

**添加非空约束：**

- 在创建表的时候添加非空约束

  ```sql
  create table stu(id int not null, name varchar(32) not null);
  ```

- 创建表完之后添加非空约束

  ```sql
  alter table stu modify name varchar(32) not null;
  ```



**删除非空约束：**

```sql
alter table stu modify name varchar(32);
```



### 3、唯一约束(唯一索引)

`unique`

**添加唯一约束：**

- 在创建表的时候添加唯一约束

  ```sql
  create table stu(id int not null, number varchar(32) unique);
  ```

- 在创建表之后添加唯一约束

  ```sql
  alter table stu modify number varchar(32) unique;
  ```



**==删除唯一约束：==**

```sql
alter table stu drop index number;
```



### 4、外键约束 (了解)

`constraint <给外键取一个名称> foreign key <本表已存在的列名> references <外键的表>(被设置为外键的列)` 

外键一般是另一个表的主键。

注意： 外键不删除完，主表相关记录不可能删除。比如：emp中的外键dep_id，不把1号部门删除完，那么dep表中的主键1号记录，就不能删除。



**添加外键约束：**

- 在创建表的时候添加外键

  ```sql
  -- 创建dep
  create table dep(id int primary key, name varchar(32) not null);
  -- 创建emp
  create table emp(
      		`id` int primary key, 
      		`name` varchar(32) not null, 
      		`age` int not null,
      		-- emp为员工表，dep为部门表，dep表的主键是id列
      		`dep_id` int, 
      		constraint `FK_emp_dep` foreign key (`dep_id`) references dep(`id`)
  );
  ```

  <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/外键.png" style="float:left" />

- 在创建表之后添加外键约束

  `constraint <给外键取一个名称> foreign key <本表已存在的列名> references <外键的表>(被设置为外键的列)` 

  ```sql
  alter table emp add constraint `FK_emp_dep` foreign key (`dep_id`) references dep(`id`);
  ```

  

**==删除外键约束：==**

```sql
alter table emp drop foreign key `FK_emp_dep`;
-- drop foreign key <外键名称>
alter table emp drop index `FK_emp_dep`;
-- 删除索引
```



#### . 级联操作

`on update cascade` **级联更新**

级联就是外键会跟着外表的被设置成外键的键进行统一更新变化。

```sql
constraint `FK_emp_dep` foreign key (`dep_id`) references dep(`id`) on update cascade;
```



`on delete cascade` **级联删除**

如果主表的信息记录删除了，对应的外键绑定的记录，也会统一全部删除。

```sql
constraint `FK_emp_dep` foreign key (`dep_id`) references dep(`id`) on delete cascade;
```



同时使用

```sql
constraint `FK_emp_dep` foreign key (`dep_id`) references dep(`id`) on update cascade on delete cascade;
```



### <u>强制不使用外键 (重点)</u>

> **阿里巴巴开发手册**
>
> 【强制】不得使用外键与级联，一切外键概念必须在应用层面解决。

外键是物理外键，数据库级别的外键，不建议使用，会使得表之间的关系复杂，后期难以删除表。

==最佳实现：==

- 数据库只存放表，只有行与字段的关系
- 通过java编程实现外键同等功能的操作



## 七、多表设计



### 1、多表之间的关系

#### 1）一对一

可以实现在任意一张表上，并使外键唯一

如：人和身份证

#### 2）一对多 / 多对一

如：部门和员工

一个部门有很多员工，一个员工只能有一个部门

#### 3）多对多

如：学生和课程

一个学生可以选多个课程，一个课程可以有多个学生

多对多的情况，需要借助第三张表。

中间表的两个外键可以形成**联合主键**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/多对多.png)



## 八、数据库范式

范式的作用，为了除去冗余的信息，避免增删改数据时造成数据不一致的错误。

目前关系数据库有六种范式：第一范式（1NF）、第二范式（2NF）、第三范式（3NF）、巴斯-科德范式（BCNF）、第四范式（4NF）和第五范式（5NF，又称完美范式）。



### 1、三大范式

**第一范式（1NF）**：对于添加的一个规范要求，所有的域都应该是原子性的，即数据库表的每一列都是不可分割的原子数据项。（保证每一列不可再分）

**第二范式（2NF）**：在1NF的基础上，非码属性必须完全依赖于候选码（在1NF基础上消除非主属性对主码的部分函数依赖）。码就是主键。（保证每张表只描述一件事情）

**第三范式（3NF）**：在2NF基础上，任何非主属性不依赖于其它非主属性（在2NF基础上消除传递依赖）。（保证每一列与主键直接相关，不能间接相关）



### 2、<u>范式与性能的问题</u>

> **阿里巴巴手册**
>
> 关联查询的表不能超过3张表，为了提高查询效率，增加用户体验

- 在设计表的时候，需要考虑实际项目来决定怎么使用范式去设计表
- 有些时候为了使得查询效率高一些，而特意去冗余一些字段，考虑用户体验
- 故意增加一些计算列，索引





## 九、数据库的备份和还原

### 1、命令行方式 

**命令行备份：**

`mysqldump -u<用户名> -t<密码> <需要备份的数据库名> > 保存的路径 `

```sql
mysqldump -uroot -t11 db1 > d:/beifen.sql
```



**还原：**

1. 登录数据库

2. 创建新数据库

3. 使用新数据库

4. 执行文件`source 文件路径`

   ```sql
   source d:/beifen.sql
   ```




```
//数据库的导入导出
一、MYSQL的命令行模式的设置： 
桌面->我的电脑->属性->环境变量->新建-> PATH=“；path\mysql\bin;”其中path为MYSQL的安装路径。(找到mysqldump执行文件的文件夹)

二、导出数据库：
1】将数据库condimentssys导出到d:\condimentssys.sql文件中：
win+R输入cmd在命令窗口输入
C:\\>mysqldump -h localhost -u root -p condimentssys >d:\condimentssys.sql
然后输入密码，等待一会导出就成功了，可以到目标文件中检查是否成功。
2】 将数据库condimentssys中的goods表导出到d:\goods.sql文件中： 
C:\\>mysqldump -h localhost -u root -p condimentssys goods >d:\goods.sql

三、导入数据库
1】在mysql中创建一个空数据库condimentssys
2】在cmd命令窗口输入口令
C:\\>mysql -u root -p condimentssys < d:\condimentssys.sql
则可将数据全部导入到数据库中
```



### 2、图形化工具

通过native for sql软件，选中数据库，单击右键，转存数据库

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/备份.png)



# SQL 提升



## 一、多表查询

多表查询，其实就是分析查询字段属于哪个表，知道需要查哪些表，之后查看这些表有哪些关联，关联就作为条件语句。

**笛卡尔积：**

```sql
select *
from dep t1, emp t2;
```

将两个表的信息都查询出来，组合成一张表，但是会出现`dep表记录*emp表记录`条数据。

为了保证数据的错误，还要对组合成的一张表进行筛洗。



**多表查询思路：**

1. 分析需要查的字段属于哪个表；
2. 找到表与表之间的交点；
3. 利用join on进行表与表之间的查询
4. 查询后的表数据，还可以通过where来筛选最终结果



### 1、内连接查询

- **隐式内连接**

  ```sql
  select 
  	t2.name 员工, 
  	t2.age 年龄, 
      t1.name 部门 
  from 
  	dep t1, 
      emp t2 
  where 
  	t1.id = t2.dep_id;
  -- 该查出的表为部门表与员工表的指定信息，指定了只显示符合员工表外键等于部门表主键的数据
  ```

- **显式内连接**

  ```sql
  select
  	t1.name `员工姓名`,
      t1.age `年龄`,
      t2.name `部门`
  from
  	emp t1
  -- [inner 可选]
  join
  	dep t2
  on
  	t1.dep_id = t2.id;
  -- 查询dep上满足on之后条件的数据连接到emp上，查询组合成的表的信息
  ```

  

### 2、外连接查询

- **左外连接**

  ```sql
  select
  	t1.name `员工姓名`,
      t1.age `年龄`,
      t2.name `部门`
  from
  	emp t1
  left join
  	dep t2
  on
  	t1.dep_id = t2.id;
  -- 查询左边表的所有信息，以及与另一个表的交集部分的信息
  ```

- **右外连接**

  ```sql
  select
  	t1.name `员工姓名`,
      t1.age `年龄`,
      t2.name `部门`
  from
  	emp t1
  right join
  	dep t2
  on
  	t1.dep_id = t2.id;
  -- 查询右边表的所有信息，以及与另一个表的交集部分的信息
  ```

  

### 3、子查询

嵌套查询称为子查询。

- 子查询是单行单列

  ```sql
  select * from emp where salary < (select AVG(salary) from emp);
  -- 查询小于平均工资的员工信息
  ```

- 子查询是多行单列(多行结果决定一条记录)

  ```sql
  select * 
  from emp 
  where dep_id 
  in (select id from dep where name = '财务部' or name = '销售部');
  -- 从emp表中查询dep_id与dep表中财务部和销售部id一样的员工信息。
  -- 也就是查询在财务部和销售部的员工信息
  ```

- 子查询是多行多列

  ```sql
  select *
  from dep t1, (select * from emp where emp.'join_date' > '2011-11-11') t2
  where t1.id = t2.dep_id;
  -- (select * from emp where emp.'join_date' > '2011-11-11') 满足该条件的虚拟表
  ```



### 4、自联表查询

```sql
CREATE TABLE category(
`categoryid` INT,
`pid` INT,
`categoryname` VARCHAR(20)
)ENGINE=INNODB CHARSET utf8;

DESC category;

INSERT INTO `category` 
VALUES(2,1,'信息技术'),(3,1,'软件开发'),(4,3,'数据库'),(5,1,'美术设计'),(6,3,'web开发'),(7,5,'ps技术'),(8,2,'办公信息');

SELECT * FROM `category`;
```

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/自联表.png" style="float:left;" />

**查询思路：**

1. 将一张表分为两张表（pid与categoryid的关系，pid为1，则为顶级父）

   | categoryid | pid  | categoryname |
   | ---------- | ---- | ------------ |
   | 2          | 1    | 信息技术     |
   | 3          | 1    | 软件开发     |
   | 5          | 1    | 美术设计     |

   | categoryid | pid  | categoryname |
   | ---------- | ---- | ------------ |
   | 4          | 3    | 数据库       |
   | 6          | 3    | web开发      |
   | 7          | 5    | ps技术       |
   | 8          | 2    | 办公信息     |

2. 查看交点关系

3. 然后进行多表查询

```sql
-- 查询顶级部门与子部门的信息
SELECT c2.`categoryname`,c1.`categoryname`
FROM category AS c1
INNER JOIN category AS c2
ON c1.`pid` = c2.`categoryid`
```

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/自联表查询结果.png" style="float:left;" />



## 二、事务

### 1、事务概念

事务管理就是，当几个语句对数据进行操作时，只能同时成功或者同时失败。

- 关闭自动提交：`set autocommit=0`

- 开启事务：`start transaction`
- 提交事务：`commit`
- 回滚事务：`rollback`
- 开启自动提交：`set autocommit=1`

> 保存点（了解）
>
> savepoint <保存点名字> ：设置保存点
>
> rollback to savepoint <保存点名字> ：回滚到保存点位置
>
> release savepoint <保存点名字> ：撤销保存点



在sql语句中，一条语句默认会自动提交事务。

**自动提交事务查询**：`@@autocommit` 有俩值，如果等于0，则是手动提交，需要手动写commit命令，如果等于1，则是自动提交。

> 设置事务开启与关闭
>
> set autocommit = 0; //关闭
>
> set autocommit = 1; //开启

==注意==：mysql默认自动提交事务，Oracle默认手动提交事务



### 2、==重要==事务的四大特征 ACID

- 原子性 Atomicity
- 一致性 Consistency
- 隔离性 Isolation
- 持久性 Durability



### 3、==重要==事务的隔离级别 <u>脏读 不可重复读 幻读</u>

概念：多个事务之间隔离的，相互独立的。但是如果多个事务操作同一批数据，则会引发一些问题，设置不同的隔离级别可以解决这些问题。



**存在的问题：**

1. **脏读**：一个事务，读取到另一个事务中还未提交的数据
2. **丢失修改**：一个事务A对数据进行了修改，修改之后要对其读取，在读取之前，而另一个事务B在它修改之后又对其进行了修改，事务A读取到的数据，是事务B修改的数据，因此事务A已产生丢失修改
3. **不可重复读**：在同一个事务中，该事务还未结束，进行了两次读取，读到的数据条数一样，但数据发生了变化
4. **幻读**：一个事务操作表中所有数据，而另一个事务向表中添加了一条数据，第一个事务再次读的时候，惊奇的发现了之前不存在的一条数据，就认为第一次幻读了。(是在同一事务下，连续执行两次同样的SQL语句第二次的SQL语句返回之前不存在的记录条数。第一次读了10条数据，第二次读了11条数据，发生幻读)



**隔离级别：**

1. read uncommitted：读未提交 
   - 产生的问题：脏读、不可重复读、幻读
2. read committed：读已提交 (Oracle 默认级别)
   - 产生的问题：不可重复读、幻读
3. repeatable read：重复读 (MySQL 默认级别，innodb引擎在重复读隔离级别已是串行化隔离级别)
   - 产生的问题：幻读
4. serializable：串行化 (类似锁，事务锁，一个事务进行操作的时候，其他事务都不能进行任何操作)
   - 可以解决所有问题

>  注意：隔离级别越高，效率越低
>
>  查询隔离级别： select @@tx_isolation;
>
>  设置隔离级别：set global transaction isolation level <隔离级别>;
>
>  ==**TIPS**==
>
>  与 SQL 标准不同的地方在于 InnoDB 存储引擎在 REPEATABLE-READ（可重读）事务隔离级别下使用的是**Next-Key Lock** 锁算法，因此可以避免幻读的产生，这与其他数据库系统(如SQL Server) 是不同的。所以说InnoDB 存储引擎的默认支持的隔离级别是 REPEATABLE-READ（可重读） 已经可以完全保证事务的隔离性要求，即达到了 SQL标准的 SERIALIZABLE(可串行化) 隔离级别。因为隔离级别越低，事务请求的锁越少，所以大部分数据库系统的隔离级别都是 READCOMMITTED(读取提交内容) ，但是你要知道的是InnoDB 存储引擎默认使用 REPEAaTABLEREAD（可重读） 并不会有任何性能损失。
>
>  InnoDB 存储引擎在 分布式事务 的情况下一般会用到 SERIALIZABLE(可串行化) 隔离级别。





## 三、索引

> 索引(index)：是帮助MySQL高效获取数据
>
> 索引在小数据量的时候用处不大，但是在大数据之下，可以提高查询效率



### 1、主要索引

#### 主键索引 (primary key)

- 唯一的标识，主键不可重复，一个表中只能有一个主键的字段

#### 唯一索引 (unique)

- 一个表中可有多个唯一索引的字段

#### 常规索引 (key / index)

- 默认的，可用index关键字，也可以用key关键字

#### 全文索引 (fulltext)

- 在特定的数据库引擎下才有
- 快速定位数据



#### 创建索引

```sql
CREATE INDEX idx_name ON sal(`name`); -- 给sal表的name字段创建一个名为idx_name索引

ALTER TABLE sal DROP INDEX idx_name;

CREATE TABLE test(
id INT AUTO_INCREMENT COMMENT 'ID',
`name` VARCHAR(10) COMMENT '姓名',
`age` INT COMMENT '年龄',
PRIMARY KEY (`id`) COMMENT '主键',
UNIQUE INDEX uk_test_name (`name`) COMMENT '唯一索引',
KEY idx_test_age(`age`) USING BTREE COMMENT '索引'
);
```



### 2、测试索引

通过`explain` 可以查看查找数据的过程信息

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/index.png)



### 3、<u>添加索引规则 原则</u>

- 索引不是越多越好
- 小数据量不需要加索引
- 不要对进程变动数据添加索引
- 索引一般加在常用于查询的字段上

> **阿里巴巴开发手册**
>
> 强制规定命名规则：主键 `pk_`、索引 `idx_`、唯一索引 `uk_`





## 四、DCL

管理用户，授予权限

MySQL有专门的用户管理数据库`mysql` ，用户表`user`



### 1、管理用户

**添加用户：**

- `create user '用户名'@'主机名' identified by '密码';`



**删除用户：**

- `drop user'用户名'@'主机名' ` 



**修改用户名：**

- `rename user <旧名> to <新名>`



**修改用户密码：**

- ```sql
  -- 1、
  -- 修改当前登录的用户密码
  set authentication_string=''
  
  -- 2、
  -- 先将user表的用户的密码置为空
  update user set authentication_string='' where user='用户名';
  -- 在重置密码
  alter user '用户名'@'主机名' identified by '设置的新密码';
  -- 刷新权限表
  flush privileges;
  ```
  
- 如果使用native连接数据库报2059。原因是mysql8 之前的版本中加密规则是mysql_native_password,而在mysql8之后,加密规则是caching_sha2_password，我们在这里修改一下加密规则就好了

  ```sql
  mysql -h localhost -u root -p -- 登录
  use mysql; --选择数据库
  ALTER USER 'root'@'localhost' IDENTIFIED BY 'password' PASSWORD EXPIRE NEVER; --更改加密方式
  ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '你的密码'; --更新用户密码
  FLUSH PRIVILEGES; --刷新权限
  ```

- **如果root密码忘记，可以无验证登录数据库，更改密码**

  ```bash
  // 1、首先以管理员身份使用cmd输入
  net stop mysql // 停止服务器
  // 2、接着输入(也可以在配置文件中，添加skip-grant-tables，也可以实现无密码登录)
  mysqld --skip-grant-tables
  // 3、以管理员身份打开新的cmd，直接输入mysql回车。就可登录数据，即可访问数据库，用户表修改密码
  ```

  

**查询用户：**

- `select * from user;`



### 2、权限管理

**查询用户权限：**

- `show grants for '用户名'@'主机名';`



**授予权限：**

- `grant <权限列表> on <有权限操作的表> to '用户名'@'主机名';`

  ```sql
  grant select,delete,update on db1.stu to 'heroC'@'localhost';
  -- 将heroC用户授予查询db1数据stu表的权限
  ```

- `grant all [privileges] on *.* to '用户名'@'主机名';`

  给用户授予所有的权限，以及可以操作所有的数据库与表。除了给别人授权没有权限其余与root权限相同



**撤销权限：**

- `revoke <权限列表> on <有权限操作的表> from '用户名'@'主机名'; ` 

  撤销用户的指定表的指定权限

- `revoke all on *.* from '用户名'@'主机名';`

  撤销用户的所有权限



## 五、函数

案例：定义一个重复插入10万条数据的函数

```sql
DELIMITER $$ -- 定义函数必写的开头
CREATE FUNCTION insert_million()
RETURNS INT
BEGIN
   DECLARE num INT DEFAULT 100000;
   DECLARE i INT DEFAULT 0;
   WHILE i<num DO
        -- SQL插入语句
        SET i = i + 1;
   END WHILE;
   RETURN i;
END;

-- 执行函数
SELECT insert_million();
```



## 六、排名函数

https://www.cnblogs.com/scwbky/p/9558203.html



### 1、ROW_NUMBER() over

这个函数不需要考虑是否并列，哪怕根据条件查询出来的数值相同也会进行连续排名

![](https://images2018.cnblogs.com/blog/1476885/201808/1476885-20180830090746251-1156974345.png)



### 2、RANK() over

**定义**：查出指定条件后的进行排名。特点是，加入是对学生排名，使用这个函数，成绩相同的两名是并列，下一位同学空出所占的名次。

使用rank() over的时候，空值是最大的，如果排序字段为null,可能造成null字段排在最前面，影响排序结果。可以这样：`rank() over(partition by course order by score desc nulls last)`来规避这个问题。

![](https://images2018.cnblogs.com/blog/1476885/201808/1476885-20180830090537924-678260036.png)

### 3、DENSE_RANK() over

与ran() over的区别是，两名学生的成绩并列以后，下一位同学并不空出所占的名次。

```sql
select Score, dense_rank() over (order by Score desc)  as `Rank` from Scores;
```

![](https://images2018.cnblogs.com/blog/1476885/201808/1476885-20180830090650526-424335453.png)



## 七、视图

视图(VIEW)也被称作虚表，即虚拟的表，是一组数据的逻辑表示,其本质是对应于一条SELECT语句，结果集被赋予一个名字，即视图名字。
视图本身并不包含任何数据，它只包含映射到基表的一个查询语句，当基表数据发生变化，视图数据也随之变化。



应用场景：

- 多个地方用到同样的查询结果
- 该查询结果使用的SQL语句较复杂

实例：

```sql
create view my_v1
as
select name,age
from student s
inner join major m
on s.mid = m.id;
```

使用：

```sql
select * from my_v1 where name like '张%';
```



优点：

- SQL语句重用
- 简化复杂SQL操作
- 保护数据防止表结构暴露，提高安全性



修改视图：

```sql
# 方式一
create or replace view 视图名
as
查询语句；

# 方式二
alter view 视图名
as
查询语句；
```

删除

```sql
drop view 视图名，视图名....
```

查看视图结构

```sql
# 方式一
desc 视图名;

# 方式二
show create view 视图名;
```



具备以下特点的视图不能执行更新操作：

- 包含分组函数、distinct、group by、having、union
- 常量视图
- select中包含子查询
- join
- from一个不能更新的视图
- where子句的子查询引用了from子句中的表



## 八、变量

系统变量：

- 全局变量
- 会话变量

自定义变量：

- 用户变量
- 局部变量



### 1、系统变量

查看系统变量`show global|[session] variables;`

查看满足条件的系统变量 `show global|[session] variables like '%char%'`

查看指定的某个系统变量 `select @@global|[session].系统变量名`

为某个系统变量赋值`set global|[session] 系统变量名 = 值`   `set @@global|[session].系统变量名 = 值`

tips: 修改全局变量之后，重启之后恢复为默认值



### 2、自定义变量

使用步骤：声明、赋值、使用

#### 用户变量

作用域：针对当前会话（连接）有效，同于会话变量作用域

```sql
# 声明初始化
SET @用户变量名=值；
SET @用户变量名:=值；
SELECT @用户变量名:=值； # select只能用:=

# 赋值更新
# 方式一 ： 同声明初始化
# 方式二
SELECT 字段 INTO @用户变量名 FROM 表； # 将表中的某个字段值赋值给变量

# 使用
SELECT @用户变量名
```



#### 局部变量

仅定义在begin end中有效

应用在begin end中的第一句话

```sql
# 声明
DECLARE 变量名 类型 DEFAULT 值;

# 赋值
SET 局部变量名=值；
SET 局部变量名:=值；
SELECT @局部变量名:=值； # select只能用:=
```



## 九、存储过程

一组预先编译号的SQL语句的集合，理解成批量处理语句

优点：

- 提高代码重用性
- 简化操作
- 减少了编译次数、减少了与数据库的连接次数，提高效率



**创建**

```sql
CREATE PROCEDURE 存储过程名(参数列表)
BEGIN
	一组合法的SQL语句（如只有一句，则 BEGIN END 可省略）
END
```

**参数列表：**

参数模式 参数名 参数类型

eg: IN params VARCHAR(20)

**参数模式：**

`IN` : 该参数可以作为输入

`OUT` : 该参数可以作为输出，也就是可以作为返回值

`INOUT` : 该参数既可以作为输入也可以作为输出



存储过程中的每条SQL语句的结尾要求必须加分号

存储过程的结尾可以使用 DELIMITER 重写设置

```sql
DELIMITER 结束标记
```



**调用**

```sql
CALL 存储过程名
```



**实例**

```sql
# 1、空参
DELIMITER $
CREATE PROCEDURE myp1()
BEGIN
	INSERT INTO user(username,password)
	VALUES('vince','1000'),('turbo','1130'),('hero','65535');
END $
# 调用
CALL myp1()$

# 2、带in参
CREATE PROCEDURE myp2(IN stuName VARCHAR(20))
BEGIN
	select c.name
	from classes c
	right join students s
	on c.s_id = s.id
	where s.name = stuName;
END $
# 调用
CALL myp2('vince')$

# 3、用户是否登录成功
CREATE PROCEDURE myp3(IN username VARCHAR(20), IN password VARCHAR(20))
BEGIN
	DECLARE res INT DEFAULT 0
	SELECT COUTN(*) INTO res
	FROM user
	where user.username = username
	and admin.password = password;
	select if(res=1,'成功','失败') as res;
END $
# 调用
CALL myp3('vince','1000')$


# 4、out参数
CREATE PROCEDURE myp4(IN stuName VARCHAR(20), OUT className VARCHAR(20))
BEGIN
	select c.name into className
	from classes c
	right join students s on c.s_id = s.id
	where s.name = stuName;
END $
# 调用
CALL myp5('vince', @res)$
SELECT @res $


# 5、inout参数
CREATE PROCEDURE myp5(INOUT a INT, INOUT b INT)
BEGIN
	SET a = a + b;
	SET b = a - b;
	SET a = a -b;
END $
# 调用
SET @x = 2 $
SET @y = 8 $
CALL myp5(@x,@y) $


# 删除存储过程
DROP PROCEDURE 存储过程名

# 查看存储过程结构信息
SHOW CREATE PROCEDURE 存储过程名
```



## 十、函数

函数与存储过程的区别：

存储过程：可以有0个，也可以有多个返回值；适合做批量插入、批量更新

函数：有且仅有一个返回值；适合做处理数据后返回一个结果



```sql
CREATE FUNCTION 函数名(参数列表) RETURNS 返回类型
BEGIN
	函数体（当函数体只有一句话可省略 BEGIN END）
END
```

**参数列表：**

参数名 参数类型

eg: params INT

**函数体：**肯定有return语句

**调用**

```sql
SELECT 函数名(参数)
```



**实例**

```sql
DELIMITER $

# 1、无参
CREATE FUNCTION myf1() RETURNS INT
BEGIN
	DECLARE res INT DEFAULT 0;
	SELECT COUNT(*) INTO res
	FROM employees;
	RETURN res;
END
# 调用
SELECT myf1() $

# 2、根据员工名返回工资
CREATE FUNCTION myf2(name VARCHAR(20)) RETURNS INT
BEGIN
	declare res int default 0
	select salary into res
	from employees e
	where e.name = name;
	return res;
END $
# 调用
SELECT myf2('vince') $


# 查看函数结构信息
SHOW CREATE FUNCTION 函数名;

# 删除函数
DROP FUNCTION 函数名;
```



## 十一、触发器

![image-20230223004155445](https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/image-20230223004155445.png)



```sql
# 创建
create trigger trigger_name
before|[after] insert|[update]|[delete]
on table_name for each row # 行级触发器
begin
	trigger_stmt;
end;

# 查看
show triggers;

# 删除
drop trigger [schema_name.]trigger_name; # 没有shema_name默认为当前数据库
```



**实例**

通过触发器记录操作日志

```sql
# 当向user表插入新数据时，则会触发该触发器，添加向user_logs表中插入日志
create trigger tri_user_insert
after insert on tb_user for each row
begin
	insert into user_logs(operation, operate_time, operate_id, operate_params) values('insert', now(), new.id, concat('insert data is: id=', new.id, ', name=',new.name,', phone=',new.phone, ', email=',new,email));
end;

# 当向user表更新数据时，则会触发该触发器，添加向user_logs表中插入日志
create trigger tri_user_update
after insert on tb_user for each row
begin
	insert into user_logs(operation, operate_time, operate_id, operate_params) values('update', now(), new.id, concat('update before data is: id=', old.id, ', name=',old.name,', phone=',old.phone, ', email=',old,email,' | update after data is: id=', new.id, ', name=',new.name,', phone=',new.phone, ', email=',new,email));
end;

# 当向user表删除数据时，则会触发该触发器，添加向user_logs表中插入日志
create trigger tri_user_delete
after delete on tb_user for each row
begin
	insert into user_logs(operation, operate_time, operate_id, operate_params) values('insert', now(), old.id, concat('delete data is: id=', old.id, ', name=',old.name,', phone=',old.phone, ', email=',old,email));
end;
```



![image-20230223011341269](https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/image-20230223011341269.png)

## 十二、流程控制

### 1、IF 函数

```sql
IF(表达式1, 表达式2, 表达式3)
# 如果表达式1成立，则if函数返回表达式2的值，否则返回表达式3的值
```



### 2、CASE

```sql
# 情况1
CASE 变量|表达式|字段
WHEN 要判断的值 THEN 返回的值1
WHEN 要判断的值 THEN 返回的值2
...
ELSE 要返回的值n
END;

# 情况2
CASE
WHEN 要判断的条件1 THEN 返回的值1
WHEN 要判断的条件2 THEN 返回的值2
...
ELSE 要返回的值n
END;
```



**case作为表达式**

![image-20230222220719520](https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/image-20230222220719520.png)



**case作为独立语句**

![image-20230222220822546](https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/image-20230222220822546.png)



**实例**

```sql
create procedure pro_gradeLeval(IN score int, out res VARCHAR(20))
BEGIN
	case
	when score >= 90 then select 'A' into res;
	when score >= 80 then select 'B' into res;
	when score >= 60 then select 'C' into res;
	else select 'D' into res;
	end case;
END;

set @r;
call pro_gradeLeval(50, @r);

select @r;
```



### 3、IF

```sql
if 条件1 then 语句1;
elseif 条件2 then 语句2;
...
else 语句n;
end if;
```



**实例**

```sql
create function fun_gradeLevel(score int) returns char(1)
BEGIN
	declare res char(1) default '';
	if score >= 90 and score <= 100 then set res = 'A';
	elseif score >= 80 then set res = 'B';
	elseif score >= 60 then set res = 'C';
	else set res = 'D';
	end if;
	return res;
END;

select fun_gradeLevel(85);
```



### 4、循环结构

分类：while、loop、repeat

循环控制：

`iterate` 跳过本次循环，继续下一次

`leave` 跳出循环，结束循环



```sql
# while
[名字：] while 循环条件 do
			循环体;
end while [名字];

# loop
[名字：] loop 循环条件
			循环体;
end loop [名字];

# repeat
[名字：] repeat
			循环体;
until 结束循环的条件
end repeat [名字];
```



实例

```sql
create procedure pro_genUserExample(IN count INT)
begin
	declare i int default 1;
	a: while i < count do
		insert into `user`(username, password) values(concat('vince',i), '000000');
		set i = i + 1;
		if i > 20 then leave a;
		end if;
	end while a;
end


call pro_genUserExample(100);
```











# SQLYog  连接出现2058错误

当连接时出现该错误提示，是因为mysql8.0版本，密码的加密方式改变了，而导致sqlyog数据库管理系统无法识别而造成登录失败。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/sqlyog错误号码2058.png)

==解决办法==

进入数据库后使用以下命令修改root用户的密码即可：

```sql
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
```

命令最后的'password'改为你需要的密码然后输入即可。



# MD5安全加密

MD5加密，只能正向加密，不可解密，不可逆

sql通过函数`MD5()`进行加密

```sql
CREATE TABLE `info`(
`id` INT,
`pwd` VARCHAR(50),
PRIMARY KEY (`id`)
)ENGINE=INNODB DEFAULT CHARSET utf8;

DROP TABLE info;

INSERT INTO info(id,pwd) VALUES(1,MD5('123')),(2,MD5('213'));

SELECT * FROM info WHERE pwd = MD5('123');
```

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMySQL/md5加密.png)



# ZEROFILL

一定要在写在最前面，否则会报错

```sql
`empno` INT(5) ZEROFILL AUTO_INCREMENT COMMENT '雇员编码',
```



# mysql服务器无法启动



### 1、Mysql服务读取描述失败，错误代码：2的解决办法

启动cmd，在`Mysql Server8.0\bin` 目录下执行： `SC DELETE (无法启动的服务器名)`
删除无法启动的服务器；
然后依然在目录下执行：`mysqld.exe -install`
然后测试连接服务器：`net start mysql`

### 2、如果出现mysql服务无法启动 服务没有报告任何错误 解决方法
如果在mysql的安装路径（`C:\Program Files\MySQL\MySQL Server 8.0`）中没有配置文件，则自己手动创建一个，文件名为：`my.ini`，添加内容如下：

```ini
[mysql]
default-character-set=utf8
[mysqld]
port=3306
basedir=C:\Program Files\MySQL\MySQL Server 8.0
datadir=C:\Program Files\MySQL\MySQL Server 8.0\datadir
max_connections=200
character-set-server=utf8
default-storage-engine=INNODB
```

==注意==：

datadir属性后的data文件夹，一定不要自己手动创建，手动创建会有问题，如果自己创建了可以删除掉，再执行后面的操作。

不要自己创建data文件夹，配置完`my.ini`后在该目录下cmd执行`mysqld --initialize-insecure`会自己生成data文件，然后执行`net start mysql`就可以了 