# Dubbo+Zookeeper



## 一、分布式系统

### 1）简介

在《分布式系统原理与范型》一书中有如下定义：“分布式系统是若干独立计算机的集合，这些计算机对于用户来说就像单个相关系统”；

分布式系统是由一组通过网络进行通信、为了完成共同的任务而协调工作的计算机节点组成的系统。分布式系统的出现是为了用廉价的、普通的机器完成单个计算机无法完成的计算、存储任务。其目的是**利用更多的机器，处理更多的数据**。

分布式系统（distributed system）是建立在网络之上的软件系统。

首先需要明确的是，只有当单个节点的处理能力无法满足日益增长的计算、存储任务的时候，且硬件的提升（加内存、加磁盘、使用更好的CPU）高昂到得不偿失的时候，应用程序也不能进一步优化的时候，我们才需要考虑分布式系统。因为，分布式系统要解决的问题本身就是和单机系统一样的，而由于分布式系统多节点、通过网络通信的拓扑结构，会引入很多单机系统没有的问题，为了解决这些问题又会引入更多的机制、协议，带来更多的问题。



### 2）背景

随着互联网的发展，网站应用的规模不断扩大，常规的垂直应用架构已无法应对，分布式服务架构以及流动计算架构势在必行，亟需一个治理系统确保架构有条不紊的演进。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDubboZookeeper/dubbo-architecture-roadmap.jpg" style="float:left" />



#### 单一应用架构

当网站流量很小时，只需一个应用，将所有功能都部署在一起，以减少部署节点和成本。此时，用于简化增删改查工作量的数据访问框架(ORM)是关键。

适用于小型网站，小型管理系统，将所有功能都部署到一个功能里，简单易用。

**缺点：**

1、性能扩展比较难

2、协同开发问题

3、不利于升级维护



#### 垂直应用架构

当访问量逐渐增大，单一应用增加机器带来的加速度越来越小，提升效率的方法之一是将应用拆成互不相干的几个应用，以提升效率。此时，用于加速前端页面开发的Web框架(MVC)是关键。

通过切分业务来实现各个模块独立部署，降低了维护和部署的难度，团队各司其职更易管理，性能扩展也更方便，更有针对性。

**缺点：**公用模块无法重复利用，开发性的浪费



#### 分布式服务架构

当垂直应用越来越多，应用之间交互不可避免，将核心业务抽取出来，作为独立的服务，逐渐形成稳定的服务中心，使前端应用能更快速的响应多变的市场需求。此时，用于提高业务复用及整合的分布式服务框架(RPC)是关键。



#### 流动计算架构

当服务越来越多，容量的评估，小服务资源的浪费等问题逐渐显现，此时需增加一个调度中心基于访问压力实时管理集群容量，提高集群利用率。此时，用于提高机器利用率的资源调度和治理中心(SOA)是关键。



## 二、RPC

**RPC**【Remote Procedure Call】是**指远程过程调用**，是一种进程间通信方式，他是一种技术的思想，而不是规范。它允许程序调用另一个地址空间（通常是共享网络的另一台机器上）的过程或函数，而不用程序员显式编码这个远程调用的细节。即程序员无论是调用本地的还是远程的函数，本质上编写的调用代码基本相同。

也就是说两台服务器A，B，一个应用部署在A服务器上，想要调用B服务器上应用提供的函数/方法，由于不在一个内存空间，不能直接调用，需要通过网络来表达调用的语义和传达调用的数据。为什么要用RPC呢？就是无法在一个进程内，甚至一个计算机内通过本地调用的方式完成的需求，比如不同的系统间的通讯，甚至不同的组织间的通讯，由于计算能力需要横向扩展，需要在多台机器组成的集群上部署应用。RPC就是要像调用本地的函数一样去调远程函数；

推荐阅读文章：https://www.jianshu.com/p/2accc2840a1b

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDubboZookeeper/rpc基本原理.png" style="float:left" />

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDubboZookeeper/rpc步骤分析.png" style="float:left" />

RPC两个核心模块：通讯，序列化。



## 三、Dubbo

### 1）简介

Apache Dubbo |ˈdʌbəʊ| 是一款高性能、轻量级的开源Java RPC框架，它提供了

**三大核心能力：面向接口的远程方法调用，智能容错和负载均衡，以及服务自动注册和发现。**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDubboZookeeper/dubbo.png" style="float:left" />

**服务提供者**（Provider）：暴露服务的服务提供方，服务提供者在启动时，向注册中心注册自己提供的服务。

**服务消费者**（Consumer）：调用远程服务的服务消费方，服务消费者在启动时，向注册中心订阅自己所需的服务，服务消费者，从提供者地址列表中，基于软负载均衡算法，选一台提供者进行调用，如果调用失败，再选另一台调用。

**注册中心**（Registry）：注册中心返回服务提供者地址列表给消费者，如果有变更，注册中心将基于长连接推送变更数据给消费者

**监控中心**（Monitor）：服务消费者和提供者，在内存中累计调用次数和调用时间，定时每分钟发送一次统计数据到监控中心

**调用关系说明**

l 服务容器负责启动，加载，运行服务提供者。

l 服务提供者在启动时，向注册中心注册自己提供的服务。

l 服务消费者在启动时，向注册中心订阅自己所需的服务。

l 注册中心返回服务提供者地址列表给消费者，如果有变更，注册中心将基于长连接推送变更数据给消费者。

l 服务消费者，从提供者地址列表中，基于软负载均衡算法，选一台提供者进行调用，如果调用失败，再选另一台调用。

l 服务消费者和提供者，在内存中累计调用次数和调用时间，定时每分钟发送一次统计数据到监控中心。



### 2）Win Linux 安装zookeeper和dubbo

> 安装zookeeper

[狂神安装zookeeper](https://mp.weixin.qq.com/s?__biz=Mzg2NTAzMTExNg==&mid=2247483947&idx=1&sn=0c8efabbaf9b8ca835d862e6e0a2254f&chksm=ce610488f9168d9eee180472c9e225c737ed56075370c1174eb29ae214326a5f8e49147c2d65&mpshare=1&scene=23&srcid=0728sEPmGxnSi3gUyQnX4uXM&sharer_sharetime=1595923227732&sharer_shareid=75cd2c6be206ecff0fb62dcc956c1e9d#rd)

下载地址：https://zookeeper.apache.org/releases.html

运行/bin/zkServer.cmd ，初次运行会报错，没有zoo.cfg配置文件；

可能遇到问题：闪退 !

解决方案：编辑zkServer.cmd文件末尾添加pause 。这样运行出错就不会退出，会提示错误信息，方便找到原因。



将conf文件夹下面的zoo_sample.cfg复制一份改名为zoo.cfg即可。



> Linux 下安装Zookeeper

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDubboZookeeper/zookeeper安装linux.png" style="float:left" />



```bash
# 以日志的形式启动
./zkServer.sh start-foreground

# Zookeeper AdminServer，默认使用8080端口，它的配置属性如下
# 所以需要在zoo.cfg中配置
admin.serverPort=8888
```







> windows 下安装dubbo

dubbo本身并不是一个服务软件。它其实就是一个jar包，能够帮你的java程序连接到zookeeper，并利用zookeeper消费、提供服务。

但是为了让用户更好的管理监控众多的dubbo服务，官方提供了一个可视化的监控程序dubbo-admin，不过这个监控即使不装也不影响使用。

我们这里来安装一下：

**1、下载dubbo-admin**

地址 ：https://github.com/apache/dubbo-admin/tree/master

**2、解压进入目录**

修改 dubbo-admin\src\main\resources \application.properties 指定zookeeper地址

```
server.port=7001
spring.velocity.cache=false
spring.velocity.charset=UTF-8
spring.velocity.layout-url=/templates/default.vm
spring.messages.fallback-to-system-locale=false
spring.messages.basename=i18n/message
spring.root.password=root
spring.guest.password=guest

dubbo.registry.address=zookeeper://127.0.0.1:2181
```

**3、在项目目录下**打包dubbo-admin

```
mvn clean package -Dmaven.test.skip=true
```

打包完成会生成一个jar包，使用`java -jar`命令执行该jar包，就可以登录`localhost:7001`进入dubbo控制中心了。**默认账户密码都是root**

**<u>dubbo的注册中心用的是zookeeper，所以在使用dubbo时，需要开启zookeeper服务</u>**



## 四、Dubbo配置

### 1、配置

```yaml
# 服务方：
dubbo:
  # 暴露提供程序的应用名称，自取
  application:
    name: provider-server
  # 连接注册中心
  registry:
    address: zookeeper://127.0.0.1:2181
  # 传输协议有：dubbo、rmi、hessian、webserver、rest、redis等
  # 默认传输协议为dubbo，传输端口号默认的是20880.服务方指定就可以了
  protocal:
    name: dubbo
    port: 20880
  # 通过dubbo的@service注解扫描到的包中方法被注册到zookeeper中
  scan:
    base-packages: com.heroc.service

# 消费方：
dubbo:
  # 暴露提供程序的应用名称，自取
  application:
    name: consumer-server
  # 连接注册中心
  registry:
    address: zookeeper://127.0.0.1:2181
  # 通过dubbo的@service注解扫描到的包中方法被注册到zookeeper中
  scan:
    base-packages: com.heroc.controller
```



### 2、协议配置 多协议

传输协议有：dubbo、rmi、hessian、webserver、rest、redis等



<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDubboZookeeper/dubbo协议.png" style="float:left" />



在springboot使用注解指定的方式，指定该接口使用指定的协议传输：

```java
@DubboService(protocol = "dubbo")
```



### 3、启动检查

duboo配置，启动检查配置在服务消费者一方。

```yaml
# 开发环境下设置为false，生产环境下设置为true
  consumer:
    check: false
```

消费者项目启动的时候，会去检查使用的接口是否都已经被注册到注册中心了，如果没有就会报异常，停止运行，这个检查时保证项目上线时能否正常运行。

但是在开发环境下，不能确保你使用的所有接口都已经开发完成注册到注册中心了，所以就没必要进行启动检查操作。所以设置为false，可以避免不能运行项目。

**注意：生产环境下一定要设置为true**



### 4、负载均衡

dubbo的负载均衡配置属性为loadbalance。为了确保服务的运行量是均衡的。默认为random策略。



> 策略

**random,  roundrobin,  leastactive, hash**

1. RandomLoadBalance: 随机负载均衡。随机的选择一个。是Dubbo的**默认**负载均衡策略。
2. RoundRobinLoadBalance: 轮询负载均衡。轮询选择一个。
3. LeastActiveLoadBalance: 最少活跃调用数，相同活跃数的随机。活跃数指调用前后计数差。使慢的 Provider 收到更少请求，因为越慢的 Provider 的调用前后计数差会越大。
4. ConsistentHashLoadBalance: 一致性哈希负载均衡。相同参数的请求总是落在同一台机器上。



> 配置

在服务者一方的配置

```java
@DubboService(protocol = "dubbo", loadbalance = "random", weight = 10)
```



在消费者一方的配置

```java
@DubboReference(check = false, loadbalance = "random")
```







## 五、分布式开发demo

使用dubbo的PRC通信的时候，都是借助zookeeper这个注册中心，所以zookeeper的服务必须打开。

在zookeeper的bin目录下开启了服务之后，可以使用`java -jar`命令执行dubbo的admin的jar包，就可以登录`localhost:7001`进入dubbo控制中心，查看注册中心所有挂起的服务器。**默认账户密码都是root**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDubboZookeeper/dubbo控制中心.png" style="float:left" />



### 1）需要的依赖包

```xml
<!--dubbo-->
<!-- Dubbo Spring Boot Starter -->
<dependency>
    <groupId>org.apache.dubbo</groupId>
    <artifactId>dubbo-spring-boot-starter</artifactId>
    <version>2.7.7</version>
</dependency>
<!--zookeeper-->
<dependency>
    <groupId>com.github.sgroschupf</groupId>
    <artifactId>zkclient</artifactId>
    <version>0.1</version>
</dependency>
<!-- 引入zookeeper -->
<dependency>
    <groupId>org.apache.curator</groupId>
    <artifactId>curator-framework</artifactId>
    <version>2.12.0</version>
</dependency>
<dependency>
    <groupId>org.apache.curator</groupId>
    <artifactId>curator-recipes</artifactId>
    <version>2.12.0</version>
</dependency>
<dependency>
    <groupId>org.apache.zookeeper</groupId>
    <artifactId>zookeeper</artifactId>
    <version>3.4.14</version>
    <!--排除这个slf4j-log4j12 因为会与spring的冲突-->
    <exclusions>
        <exclusion>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
        </exclusion>
    </exclusions>
</dependency>
```



### 2）provider-server

> yaml 配置

```yaml
server:
  port: 8081

dubbo:
  # 暴露提供程序的应用名称，自取
  application:
    name: provider-server
  # 连接注册中心
  registry:
    address: zookeeper://127.0.0.1:2181
  # 传输协议有：dubbo、rmi、hessian、webserver、rest、redis等
  # 默认传输协议为dubbo，传输端口号默认的是20880.服务方指定就可以了
  protocal:
    name: dubbo
    port: 20880
  # 通过dubbo的@service注解扫描到的包中方法被注册到zookeeper中
  scan:
    base-packages: com.heroc.service
```



> OrderService.java

```java
public interface OrderService {
    String getOrder();
}
```

提供者写一个订单接口



> OrderServiceImpl

```java
/**
 * 这个注解 @DubboService，是将该实例注册到注册中心，供其他微服务进行调用
 * 低版本的dubbo的这个注解与spring的@Service注解重名.
 * 因此使用低版本的时候，最好使用dubbo的@Service和spring的@Component
 * */
@DubboService
@Service
public class OrderServiceImpl implements OrderService {

    @Override
    public String getOrder() {
        return "来自provider-server的订单";
    }
}
```

接口的具体业务逻辑实现



### 3）consumer-server

> yaml 配置

```yaml
server:
  port: 8080

dubbo:
  # 暴露提供程序的应用名称，自取
  application:
    name: consumer-server
  # 连接注册中心
  registry:
    address: zookeeper://127.0.0.1:2181
  # 开发环境下设置为false，生产环境下设置为true
  consumer:
    check: false
```



> OrderService.java

```java
public interface OrderService {
    String getOrder();
}
```

提供者写的一个订单接口



> UserServiceImpl

```java
@Service
public class UserServiceImpl implements UserService {

    /**
     * 这里引入了OrderService接口的作用是起到一个约束作用，只能调用哪些方法
     *  @DubboReference 这个注解是从注册中心引入这个实例，就可以调用注册中心
     *  这个实例的方法了。
     * */
    @DubboReference
    private OrderService orderService;

    public void userOrder(){
        System.out.println("消费者下了一个：" + orderService.getOrder());
    }

}
```



> 测试

```java
@SpringBootTest
class ConsumerServerApplicationTests {

    @Autowired
    private UserService userService;

    @Test
    void contextLoads() {
        userService.userOrder();
    }

}
```

结果：

```
消费者下了一个：来自provider-server的订单
```



就此，完成了简单的分布式开发



## 五、Dubbo思考

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForDubboZookeeper/dubbo思考.png" style="float:left" />



## 六、解决Dubbo无法发布被事务代理的Service问题

由于注册到注册中心的service是被指定了扫描的包，而为这个类加了@Transactional这个事务注解，spring就会使用aop动态代理这个类。此时这个事务是com.sun.proxy包下的，导致dubbo在发布前，进行包匹配时无法完成匹配，进而发布失败。



> 解决办法

传统的事务代理使用的jdk动态代理，所以就会造成动态代理的包名是`com.sun.proxy`开头的，所以就会发布失败。而springboot默认使用的是cglib动态代理，而cglib动态代理生成的包名是当前类包名开始的，所以不会产生发布失败的问题。但是会产生发布的接口被springboot改变。因此还需要在注解中加入`interfaceClass`将包名特别指明以下，防止改动：

```java
@DubboService(protocol = "dubbo", interfaceClass = OrderService.class, loadbalance = "random", weight = 10)
@Service
public class OrderServiceImpl implements OrderService {

    @Override
    public String getOrder() {
        return "来自provider-server的订单";
    }
}
```



1. 将jdk动态代理改成cglib动态代理(pringboot默认cglib代理)
2. 指定需要注册的类的interfaceClass





## 七、分布式遇到的4个问题

1. 这么多服务，客户端该如何去访问？
2. 这么多服务，服务之间如何通信？
3. 这么多服务，如何治理？
4. 服务挂了，怎么办？



- API网关，服务路由
- HTTP、PRC框架，异步调用
- 服务注册与发现、高可用
- 熔断机制，服务降级 (解决服务器宕机问题)



解决方案：

> **一、Spring Cloud NetFlix**

一站式解决方案。

​	Api网关 ---> zuul组件

​	Feign --->  HttpClient --->   HTTP的通信方式，同步并阻塞

​	服务注册与发现，Eureka

​	熔断机制 Hystrix

(NetFlix 宣布无期限停止维护。生态不在维护，造成脱节)



> **二、Apache Dubbo Zookeeper**

​	Api网关 没有，要么找第三方，要么自己实现。

​	Dubbo是一个高性能基于Java实现的RPC通信框架

​	服务注册与发现 Zookeeper

​	熔断机制 没有，借助Hystrix



> **一、Spring Cloud Alibaba**

一站式解决方案。



