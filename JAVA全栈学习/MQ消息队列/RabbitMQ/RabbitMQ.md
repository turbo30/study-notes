# 	RabbitMQ



## 一、MQ引言



### 1、什么是MQ

​	`MQ`(Message Quene)：翻译为`消息队列`，通过典型的`生产者`和`消费者`模型，生产者不断向消息队列中产生消息，消费者不断从队列中获取消息。因为消息的生产和消费都是异步的，而且只关心消息的发送和接收，没有业务逻辑的侵入，轻松实现系统间的解耦。别名为`消息中间件` 通过利用高效可靠的消息传递机制进行平台无关的数据交流，并基于数据通信来进行分布式系统集成。



### 2、MQ有哪些

​	主流的消息中间件很多，老牌的`ActiveMQ`、`RabbitMQ`，炙手可热的`Kafka`，阿里巴巴自主开发的`RocketMQ`等。



### 3、不同MQ的特点

> **ActiveMQ**

​	ActiveMQ 是Apache出品，最流行、能力强劲的开源消息总线。它是一个完全支持JMS规范的消息中间件。丰富的API，多种集群框架模式让其在业界成为老牌的消息中间件，在中小型企业颇受欢迎（因为吞吐量不是很高）。



> **Kafka**

​	Kafka是LinkedIn开源的分布式发布-订阅消息系统，目前归属于Apache顶级项目。Kafka主要特点是基于Pull的模式来处理消息消费，<u>追求高吞吐量</u>，一开始的目的就是用于日志收集和传输。0.8版本开始支持复制，不支持事务，对消息的重复、丢失、错误没有严格要求，适合产量大数据的互联网服务数据收集业务。



> **RocketMQ**

​	RocketMQ是阿里巴巴开源的消息中间件，它是纯Java开发，<u>具有高吞吐量、高可用性、适合大规模分布式系统应用的特点</u>。RocketMQ思路起源于Kafka，但并不是Kafka的一个复制品。他对消息的可靠性传输以及事务性做了优化，目前在阿里集团被广泛用于交易、充值、流计算、消息推送、日志流式处理、binglog分发等场景。



> **RabbitMQ**

​	RabbitMQ是使用Erlang语言开发的开源消息队列系统，基于AMQP协议来实现。AMQP主要特征是面向消息、队列、路由（包括点对点和发布/订阅）、可靠性、安全。AMQP协议更多用于企业系统内对数据<u>一致性、稳定性和可靠性要求很高的场景</u>，对性能和吞吐量的要求其次。



> RabbitMQ比Kafka可靠，Kafka更适合IO吞吐量大的场景，一般应用于大数据日志处理或对实时性（少量延迟）、可靠性（少量丢失数据）要求稍低的场景使用，比如ELK日志收集。



### 4、引入MQ的缺点

- **提高复杂度**

  会引出一大堆问题

- **降低可用性**

  MQ一旦挂了，整套系统就玩完了

- **一致性问题**

  所有跨VM的一致性问题，从技术的角度讲通用的解决方案是：

  1. 强一致性，分布式事务，但落地太难且成本太高
  2. 最终一致性，主要是用“记录”和“补偿”的方式。在做所有的不确定的事情之前，先把事情记录下来，然后去做不确定的事情，结果可能是：成功、失败或是不确定，“不确定”（例如超时等）可以等价为失败。成功就可以把记录的东西清理掉了，对于失败和不确定，可以依靠定时任务等方式把所有失败的事情重新搞一遍，直到成功为止。
     回到刚才的例子，系统在A扣钱成功的情况下，把要给B“通知”这件事记录在库里（为了保证最高的可靠性可以把通知B系统加钱和扣钱成功这两件事维护在一个本地事务里），通知成功则删除这条记录，通知失败或不确定则依靠定时任务补偿性地通知我们，直到我们把状态更新成正确的为止。



## 二、RabbitMQ

默认端口号 15672 

默认账户密码都是guest

已修改密码为guest1130



### 1、RabbitMQ

> 基于**AMQP**协议，**erlang**语言开发，是部署最广泛的开源消息中间件，是最受欢迎的开源消息中间之一



#### 1）AMQP协议

​	AMQP（advanced message queuing protocol）在2003年时被提出，最早用于解决金融领域不同平台之间的消息传递交互问题。顾名思义，AMQP是一种协议，更准确的说是一种binary wirelevel protocol（链接协议）。这是其和JMS的本质差别，**AMQP不从API层进行限定，而是直接定义网络交换的数据格式**。这使得实现了AMQP的provider天然性就是跨平台的。



![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/AMQP.png)

有流程图可以看到，生产者将消息发送到RabbitMQ的服务器server，然后通过交换机exchange对消息进行处理，加入到消息队列中，也可以不直接放入到消息队列中，根据业务来决定。消费者从RabbitMQ的服务器server中的消息队列中获取消息。



#### 2）消息基于什么传输？

由于 TCP 连接的创建和销毁开销较大，且并发数受系统资源限制，会造成性能瓶颈。RabbitMQ 使用==信道==的方式来传输数据。==信道是建立在真实的 TCP 连接内的虚拟连接，且每条 TCP 连接上的信道数量没有限制==。



### 2、RabbitMQ 安装



#### Linux环境下安装

**1）安装软件**：

rpm安装

```bash
yum -y install socat
rpm -ivh esl-erlang_22.0.7-1_centos_7_amd64.rpm
rpm -ivh rabbitmq-server-3.7.18-1.el7.noarch.rpm
```



yum安装

```bash
############ 通过yum安装，可以先查看以下存在的版本
yum list <软件名>

yum -y install rabbitmq-server

##  存在的依赖软件
yum -y install erlang
yum -y install socat
```



**2）查看软件安装位置**：

```bash
rpm -ql <软件名>
```



**3）复制配置文件**：

因为rabbitmq读取配置文件会去`/etc/rabbitmq/`文件下读取配置文件，所以需要将rabbitmq安装目录下的配置文件拷贝到该目录下，并命名为**rabbitmq.config**

```bash
cp /usr/share/doc/rabbitmq-server-3.7.18/rabbitmq.config.example /etc/rabbitmq/rabbitmq.config
```



**4）修改配置文件**：

```bash
vim /etc/rabbitmq/rabbitmq.config
```

==**一定要删除后面的逗号，被坑过一次**==

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/修改配置文件.png)

打开之后，就可以以任何地方都可以访问rabbitmq的管理界面



**5）启动rabbitmq的插件管理**：

方便web的配置界面

```bash
rabbitmq-plugins enable rabbitmq_management
```



**6）启动rabbitmq**：

由于rabbitmq在安装的时候，就会在系统中添加一个`rabbitmq-server`的启动脚本

```bash
# 查看服务状态
systemctl status rabbitmq-server
# 启动服务
systemctl start rabbitmq-server
# 停止服务
systemctl stop rabbitmq-server
```



#### 访问RabbitMQ web界面

rabbitmq的默认端口号是15672，默认账户密码都是guest

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/rabbit管理界面.png)



## 三、RabbitMQ配置



### 1、RabbitMQ 管理命令行

```bash
# 1.服务启动相关
systemctl start|restart|status|stop rabbitmq-server

# 2.管理命令行 用来在不使用web管理界面情况下命令操作RabbitMQ
rabbitmqctl help

# 3.插件管理命令行
abbitmq-plugins enable rabbitmq_management
```



```bash
# 查看用户，以及权限
rabbitmqctl list_users

# 展示插件
rabbitmq-plugins list

# 启用插件
rabbitmq-plugins enable <插件名>
rabbitmq-plugins disable <插件名>
```



## 四、RabbitMQ 模型

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/rabbitmq模式01.png)



![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/rabbitmq模式02.png)



### 1、准备



#### 1）创建新用户、新虚拟主机

虚拟主机就相当于数据库中的库的概念



> **添加新虚拟主机**

登录rabbitMQ管理页面，

在Admin->Virtual Hosts中添加虚拟主机host为`/ems`

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/添加虚拟主机host.png)



> **添加新用户**

**添加一个用户名为ems，密码为123的新用户，角色为admin**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/添加ems新用户.png" style="float:left" />



**设置该用户可访问的虚拟主机，以及对该虚拟主机的权限**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/添加ems新用户设置可访问的虚拟主机.png" style="float:left" />



**添加虚拟主机新用户最终结果**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/添加虚拟主机新用户最终结果.png" style="float:left" />



#### 2）创建项目

> 依赖

```xml
<dependency>
    <groupId>com.rabbitmq</groupId>
    <artifactId>amqp-client</artifactId>
    <version>5.7.2</version>
</dependency>
```



### 2、RabbitMQ 直连模式 消息持久化

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/直连.png" style="float:left" />



可以运用的场景：用户注册之后等，需要加积分的操作。可以通过发送消息到积分服务器进行操作。



#### 1）生产者 发布消息

```java
public class ProviderSendMsgTest {

    @Test
    public void sendMsg() throws IOException, TimeoutException {
        // 创建连接mq的连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 设置连接mq的主机
        connectionFactory.setHost("127.0.0.1");
        // 设置端口号
        connectionFactory.setPort(5672);
        // 设置需要连接的虚拟主机
        connectionFactory.setVirtualHost("/ems");
        // 设置用户名、密码
        connectionFactory.setUsername("ems");
        connectionFactory.setPassword("123");

        // 获取一个连接对象
        Connection connection = connectionFactory.newConnection();
        // 连接对象获取一个通道
        Channel channel = connection.createChannel();

        /**
         * 通道绑定一个消息队列
         * 参数1：队列名称，没有会自动创建一个
         * 参数2：是否持久化消息队列，true为当mq服务器宕机了只会将消息队列持久化到硬盘中，存储的消息并不会，false则不会
         * 参数3：是否为独占式队列，true表示只有当前能使用该队列，false则不是
         * 参数4：是否在消息消费完成后自动删除队列，true表示会删除，false表示不会
         * 参数5：额外附加参数
         */
        channel.queueDeclare("hello",false,false,false,null);

        /**
         * 发布消息
         * 参数1：交换机名称，没有就空着
         * 参数2：消息队列名称
         * 参数3：额外附加参数，没有null
         *		额外参数值：MessageProperties.PERSISTENT_TEXT_PLAIN 队列中的消息持久化
         * 参数4：需要发布的内容
         */
        channel.basicPublish("","hello",null,"hello rabbitMQ".getBytes());

        // 关闭io流
        channel.close();
        connection.close();
    }
}
```



#### 2）消费者 消费消息

```java
public class ConsumerConsumptionMsg {

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("127.0.0.1");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/ems");
        connectionFactory.setUsername("ems");
        connectionFactory.setPassword("123");

        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();

        // 绑定的消息队列，参数一定要与发布者绑定的消息队列参数一样
        channel.queueDeclare("hello",false,false,false,null);

        /**
         * 消费者消费消息
         * 参数1：消息队列名称
         * 参数2：开启消息的自动确认机制
         * 参数3：消费时的回调接口
         */
        channel.basicConsume("hello",true, new DefaultConsumer(channel){
            // 最后一个参数是从消息队列中取出的消息
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("消息......:  "+new String(body));
            }
        });

        // 消费者不关闭通道和连接，就会一直监听消息队列消费消息。
        // 不建议关闭，因为要一直监听，拿取消息队列中的消息
        // channel.close();
        // connection.close();
    }
}
```



#### 3）提取公共代码 创建工具类

```java
@Slf4j
public class RabbitMqUtils {

    private static ConnectionFactory connectionFactory;

    static {
        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("127.0.0.1");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/ems");
        connectionFactory.setUsername("ems");
        connectionFactory.setPassword("123");
    }

    public static Connection getConnection(){
        try{
            Connection connection = connectionFactory.newConnection();
            return connection;
        } catch (TimeoutException e) {
            log.warn("获取RabbitMQ连接超时异常....");
            e.printStackTrace();
        } catch (IOException e) {
            log.warn("获取RabbitMQ连接IO流异常....");
            e.printStackTrace();
        }
        return null;
    }

    public static void close(Connection connection, Channel channel){
        try {
            if (channel != null) {
                channel.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (TimeoutException e) {
            log.warn("获取RabbitMQ关闭超时异常....");
            e.printStackTrace();
        } catch (IOException e) {
            log.warn("获取RabbitMQ关闭IO流异常....");
            e.printStackTrace();
        }
    }

}
```



### 3、RabbitMQ 任务模式

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/work模型.png" style="float:left" />

多个消费者去处理消息队列中的消息，消息队列中的消息不会被重复执行，**默认将消息平均分配到每个消费者**。

#### 1）生产者

```java
public class ProviderSendMsgTest {

    @Test
    public void sendMsg() throws IOException, TimeoutException {

        // 获取连接
        Connection connection = RabbitMqUtils.getConnection();

        // 连接对象获取一个通道
        Channel channel = connection.createChannel();

        // 通道绑定队列
        channel.queueDeclare("work",true,false,false,null);

        // 通道发布消息
        for (int i = 0; i < 10; i++) {
            channel.basicPublish("","hello", MessageProperties.PERSISTENT_TEXT_PLAIN,(i+" work rabbitMQ").getBytes());
        }

        // 关闭io流
        RabbitMqUtils.close(connection,channel);
    }
}
```



#### 2）消费者1 消费者2

消费者1和消费者2的代码是一毛一样的

```java
public static void main(String[] args) throws IOException {
    Connection connection = RabbitMqUtils.getConnection();
    Channel channel = connection.createChannel();

    channel.queueDeclare("work",true,false,false,null);

    channel.basicConsume("work",true,new DefaultConsumer(channel){
        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
            System.out.println("consumer 1: "+new String(body));
        }
    });

}
```



#### 3）默认平均分配存在的问题 消息确认机制



##### . 存在的问题

默认情况下是消息自动确认：

以下方法的第二个参数是autoAck 为true表示自动确认。

```java
channel.basicConsume("hello",true, new DefaultConsumer(channel){
    ...
});
```



>官网表述(https://www.rabbitmq.com/tutorials/tutorial-two-java.html)：
>
>Doing a task can take a few seconds. You may wonder what happens if one of the consumers starts a long task and dies with it only partly done. With our current code, once RabbitMQ delivers a message to the consumer it immediately marks it for deletion. In this case, if you kill a worker we will lose the message it was just processing. We'll also lose all the messages that were dispatched to this particular worker but were not yet handled.

自动确认的致命问题是，如果为自动确认消息时，消费者一拿到消息就会自动向RabbitMQ服务器发送一个确认消息，这个确认消息代表该消费者拿到了消息，服务器收到确认消息就会从消息队列将该消息删除，服务器会再发一个消息给消费者，一次类推，这样不管消费者对消息是否处理完成，因发送了确认消息，消费者就会接收收到大量的消息。如果消费者拿到消息并还没有将所有消息都处理完就宕机了，那么这些未处理的消息就会消失在世界上。这样会导致致命的问题。



##### . 解决问题

为了保证处理快的机器多处理消息，处理慢的机器少处理消息。还保证消息不会随意丢失。那么就不能使用自动确认机制。

消费者代码更改为：

```java
public class Consumer1 {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare("work",true,false,false,null);

        // 每次接收消息的条数
        channel.basicQos(1);
        
        /**
         * 参数1：0表示对消息的大小无限制,单位为(B-字节)
         * 参数2：会告诉RabbitMQ不要同时给一个消费者推送多于N个消息，即一旦有N个消息还没有ack，则该consumer将 阻塞 掉，直到有消息ack。0为无上限
         * 参数3：true表示该设置应用于channel层面，false表示应用于consumer层面
         */
        //channel.basicQos(0,1,false);

        channel.basicConsume("work",false,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("consumer 1: "+new String(body));
                
                /**
                * 消息处理完发送确认信息，不发送确认信息，RabbitMQ服务器中会对该消息标注为unAcked
                * 参数1：当前消息的唯一标识，用于告诉RabbitMQ服务器确认哪条消息，做消息删除处理
                * 参数2：是否发送堕多条确认消息 
                */
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        });
    }
}
```



### 4、RabbitMQ 广播模式(fanout)

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/广播模型.png" style="float:left" />

在广播模式下，消费发送流程：

- 可以有多个消费者
- 每个消费者有自己的消息队列
- 每个队列都要绑定Exchange交换机
- 生产者发送的消息，只能发送给交换机，交换机决定将消息发送给哪个队列，生产者无法决定
- 交换机把消息发送给绑定的所有队列
- 队列的消息者都能拿到该消息。实现一个条消息被多个消费者消费



#### 1）生产者

```java
public class ProviderSendMsgTest {
    @Test
    public void providerSendMsg() throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();

        /**
         * 声明一个交换机，没有会自动创建一个，fanout类型是广播
         * 参数1：交换机名称
         * 参数2：交换机类型
         */
        channel.exchangeDeclare("logs","fanout");

        // 发布消息到交换机，参数2没有意义
        channel.basicPublish("logs","",null,"fanout Rabbit".getBytes());

        RabbitMqUtils.close(channel,connection);
    }
}
```



#### 2）消费者1 消费者2

消费者代码都一样

```java
public class Consumer1 {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();

        // 消费者声明一个交换机，与发布者一致
        channel.exchangeDeclare("logs","fanout");

        // 创建一个临时消息队列，当消费者通道和连接关闭时，该消息队列会自动删除，返回消息队列名称
        String queueName = channel.queueDeclare().getQueue();

        /**
         * 将消息队列与交换机绑定
         * 参数1：消息队列名称
         * 参数2：交换机名称
         * 参数3：路由key值，在这里没有意义
         */
        channel.queueBind(queueName,"logs","");

        // 消费消息
        channel.basicConsume(queueName,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(new String(body));
            }
        });
    }
}
```

消费者都会获取到交换机中的消息。



### 5、RabbitMQ 路由模式



#### 1）Routing 之订阅模型 - Direct

`在Fanout模式中，一条消息会被所有订阅者的消息队列都消费。但是，在某些场景下，我们希望不同的消息被不同的队列消费。这时就要用到Direct类型的Exchange`



在Direct模型下：

- 队列与交换机的绑定，不能是任意绑定的，而是需要指定一个RoutingKey
- 消息的发送方在向Exchange发送消息时，需要指定RoutingKey
- Exchange不再把消息交给每一个绑定的队列，而是根据消息的RoutingKey进行判断，只有RouringKey一致的才会被接收到消息。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/路由模式.png" style="float: left; zoom: 80%;" />



##### 生产者

```java
public class Provider {
    @Test
    public void sendMsg() throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("logs_direct","direct");
        // 确定发送的消息路由
        String routingKey = "info";
        channel.basicPublish("logs_direct",routingKey,null,("direct rabbit "+routingKey).getBytes());
        RabbitMqUtils.close(channel,connection);
    }
}
```



##### 消费者info  消费者all

ConsumerInfo

```java
public class ConsumerInfo {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("logs_direct","direct");
        // 获取临时队列
        String queueName = channel.queueDeclare().getQueue();
        // 消费路由为info的消息
        channel.queueBind(queueName,"logs_direct","info");
        channel.basicConsume(queueName,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(new String(body));
            }
        });
    }
}
```

ConsumerAll

```java
public class ConsumerAll {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("logs_direct","direct");
        String queueName = channel.queueDeclare().getQueue();

        // 消费路由为info和error的消息
        channel.queueBind(queueName,"logs_direct","info");
        channel.queueBind(queueName,"logs_direct","error");

        channel.basicConsume(queueName,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(new String(body));
            }
        });
    }
}
```



#### 2）Routing 之订阅模型 - Topic

`Topic`类型的`Exchange`与`Direct`相比，都是可以根据`RoutingKey`把消息路由到不同的队列。只不过`Topic`类型`Exchage`可以让队列在绑定`RoutingKey`的时候使用通配符！这种模型`RoutingKey`一般都是由一个或者多个单词组成，多个单词之间以"."分割，例如：`item.insert`

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/topics模式.png" style="float: left; zoom: 80%;" />

通配符

- `*` (star) can substitute for exactly one word.  只匹配一个单词
- `#` (hash) can substitute for zero or more words.  可匹配多个或0个单词

例如：

​	audit.*  可匹配 audit.cor 或者 audit.irs 等

​	audit.# 可匹配 audit.irs.cor 或者 audit.irs 或者 audit 等

​	*.audit.#



##### 生产者

```java
public class Provider {
    @Test
    public void sendMsg() throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("logs_topic","topic");
        // 确定发送的消息路由
        String routingKey = "user.item.save";
        channel.basicPublish("logs_topic",routingKey,null,("topic rabbit "+routingKey).getBytes());
        RabbitMqUtils.close(channel,connection);
    }
}
```



##### 消费者

```java
public class Consumer3 {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("logs_topic","topic");
        String queue = channel.queueDeclare().getQueue();

        // 匹配user.#的消息
        channel.queueBind(queue,"logs_topic","user.#");

        channel.basicConsume(queue,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(new String(body));
            }
        });
    }
}
```



```java
public class Consumer8 {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("logs_topic","topic");
        String queue = channel.queueDeclare().getQueue();

        // 匹配user.*的消息
        channel.queueBind(queue,"logs_topic","user.*");

        channel.basicConsume(queue,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(new String(body));
            }
        });
    }
}
```

所以，Consumer3类能接收到消息。



## 五、SpringBoot 整合 RabbitMQ 高级进阶

> 依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-amqp</artifactId>
</dependency>
```

通过RabbitTempalte来使用。如果只写生产者，是不会创建队列的。



> 配置文件
>
> (https://www.cnblogs.com/qts-hope/p/11242559.html)

```yaml
spring:
  application:
    name: sb-rabbitmq
  rabbitmq:
    host: 127.0.0.1
    port: 5672
    virtual-host: /ems
    username: ems
    password: 123
    publisher-confirm-type: correlated # 须配置这个才会确认回调
    publisher-returns: true # 开启发送失败退回
    listener:
      direct:
        acknowledge-mode: manual # 开启消费者手动ack
      simple:
        acknowledge-mode: manual # 开启消费者手动ack
        # concurrency: 1 # 最小消费数量
        # max-concurrency: 1 # 最大消费数量
        prefetch: 1 # 指定一个请求能处理多少个消息，如果有事务的话，必须大于等于transaction数量.
        retry: # 重试是否可用
          enabled: true
```



### 1、直连模式

生产者

```java
@SpringBootTest(classes = RabbitMqSpring.class)
@RunWith(SpringRunner.class)
public class RabbitMqTest {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void sendMsgHello(){
        // 发送消息到名为hello消息队列
        rabbitTemplate.convertAndSend("hello","springboot send msg hello");
    }
}
```



消费者

```java
@Component
// 消费者监听，消息队列声明：名称、持久化、独占、自动删除
@RabbitListener(queuesToDeclare = @Queue(name = "hello", durable = "false", exclusive = "false", autoDelete = "false"))
public class ConsumerHello {

    // 获取消息队列中的消息
    @RabbitHandler
    public void receive(String msg){
        System.out.println("accept：----> "+msg);
    }
}
```



### 2、任务模式  能者多劳  消费者手动ACK

配置消费者手动提交ack，以及一个请求处理多少个消息

```yaml
spring:
  application:
    name: sb-rabbitmq
  rabbitmq:
    host: 127.0.0.1
    port: 5672
    virtual-host: /ems
    username: ems
    password: 123
    listener:
      direct:
        acknowledge-mode: manual # 开启消费者手动ack
      simple:
        acknowledge-mode: manual # 开启消费者手动ack
        prefetch: 1 # 指定一个请求能处理多少个消息，如果有事务的话，必须大于等于transaction数量.重点
        retry: # 重试是否可用
          enabled: true
```



生产者

```java
@Test
public String sendMsg(){
    for (int i = 0; i < 20; i++) {
        CorrelationData correlationId = new CorrelationData(UUID.randomUUID().toString());
        /**
         * 参数1：exchange
         * 参数2：routingKey 任务模式就是队列名
         * 参数3：发布内容
         * 参数4：关联数据，该消息的id
         */
        rabbitTemplate.convertAndSend("","work","springboot send msg work "+ i,correlationId);
    }
}
```



消费者

**实现能者多劳。**Message和Headers都可以

```java
@Component
@Slf4j
public class ConsumerWork {

    @RabbitListener(queuesToDeclare = @Queue(name = "work"))
    public void receiveOne(String msg, Channel channel, Message message){
        try {
            System.out.println("consumer1: ---> " + msg);
            if (message.getBody().toString()!=null){
                // 返送ack
                channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
            }else {
                // 拒绝
                // 参数1：消息标识 参数2：是否重新入队列，true表示重新入队列，false表示丢弃
                channel.basicReject(message.getMessageProperties().getDeliveryTag(),false);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RabbitListener(queuesToDeclare = @Queue(name = "work"))
    public void receiveTwo(String msg, Channel channel,Message message, @Headers Map<String,Object> headers){
        try {
            TimeUnit.MILLISECONDS.sleep(1000);
            System.out.println("consumer2: ---> " + msg);
            if (message.getBody().toString()!=null){
                channel.basicAck((Long) headers.get(AmqpHeaders.DELIVERY_TAG),false);
            }else {
                // 参数1：消息标识 参数2：是否重新入队列，true表示重新入队列，false表示丢弃
                channel.basicReject(message.getMessageProperties().getDeliveryTag(),false);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```





### 3、广播模式

生产者

```java
// fanout
@Test
public void sendMsgFanout(){
    rabbitTemplate.convertAndSend("log_fanout","","springboot send msg fanout");
}
```



消费者

```java
@Component
public class ConsumerFanout {

    /**
     * 广播，绑定队列和交换机
     * 注解 @Queue不写参数是产生一个临时队列； @Exchange 是交换机
     * @param msg
     */
    @RabbitListener(bindings = {
        @QueueBinding(
            value = @Queue,
            exchange = @Exchange(value = "log_fanout",type = "fanout"))
    })
    public void receiveOne(String msg){
        System.out.println("consumer1 ---> "+msg);
    }

    @RabbitListener(bindings = {
        @QueueBinding(
            value = @Queue,
            exchange = @Exchange(value = "log_fanout",type = "fanout"))
    })
    public void receiveTwo(String msg){
        System.out.println("consumer2 ---> "+msg);
    }
}
```



### 4、静态路由 direct

生产者

```java
// direct
@Test
public void sendMsgDirect(){
    rabbitTemplate.convertAndSend("log_direct","error","springboot send msg direct error");
}
```

消费者

```java
@Component
public class ConsumerDirect {

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "log_direct",type = "direct"),
                    key = "info"
            )
    })
    public void receiveInfo(String msg){
        System.out.println("consumer info ---> "+msg);
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "log_direct",type = "direct"),
                    key = {"info","error"}
            )
    })
    public void receiveAll(String msg){
        System.out.println("consumer info/error ---> "+msg);
    }
}
```



### 5、动态路由 topic

生产者

```java
// topic
@Test
public void sendMsgTopic(){
    rabbitTemplate.convertAndSend("log_topic","user.item.save","springboot send msg topic");
}
```

消费者

```java
@Component
public class ConsumerTopic {

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "log_topic",type = "topic"),
                    key = "user.*"
            )
    })
    public void receiveOne(String msg){
        System.out.println("consumer user.* ---> " + msg);
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "log_topic",type = "topic"),
                    key = "user.#"
            )
    })
    public void receiveTwo(String msg){
        System.out.println("consumer user.# ---> " + msg);
    }
}
```



### 6、确认回调、返回回调

确认回调、返回回调的作用是，当发布者发布消息到服务器，根据情况就执行回调函数。



```yaml
spring:
  application:
    name: sb-rabbitmq
  rabbitmq:
    host: 127.0.0.1
    port: 5672
    virtual-host: /ems
    username: ems
    password: 123
    publisher-confirm-type: correlated # 须配置这个才会确认回调（重点）
    publisher-returns: true # 开启发送失败退回（重点）
    listener:
      direct:
        acknowledge-mode: manual # 开启消费者手动ack
      simple:
        acknowledge-mode: manual # 开启消费者手动ack
        #concurrency: 1 # 最小消费数量
        #max-concurrency: 1 # 最大消费数量
        prefetch: 1 # 指定一个请求能处理多少个消息，如果有事务的话，必须大于等于transaction数量.
        retry: # 重试是否可用
          enabled: true
```



发送者

```java
@RestController
@Slf4j
public class ProviderController implements RabbitTemplate.ConfirmCallback,RabbitTemplate.ReturnCallback{

    private RabbitTemplate rabbitTemplate;

    /**
     * 构造方法注入
     * @param rabbitTemplate
     */
    @Autowired
    public ProviderController(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
        //这是是设置回调能收到发送到响应
        rabbitTemplate.setConfirmCallback(this);
        //如果设置备份队列则不起作用
        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setReturnCallback(this);
    }

    @GetMapping("/send")
    public String sendMsg(){
        for (int i = 0; i < 20; i++) {
            CorrelationData correlationId = new CorrelationData(UUID.randomUUID().toString());
            /**
             * 参数1：exchange
             * 参数2：routingKey 任务模式就是队列名
             * 参数3：发布内容
             * 参数4：关联数据，该消息的id
             */
            rabbitTemplate.convertAndSend("","work","springboot send msg work "+ i,correlationId);
        }
        return "已发送";
    }

    /**
     * 确认回调
     * @param correlationData
     * @param ack
     * @param cause
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if (ack){
            log.info("消息发送成功：correlationData({}),ack({}),cause({})",correlationData,ack,cause);
        }else {
            log.info("消息发送失败：correlationData({}),ack({}),cause({})",correlationData,ack,cause);
        }
    }

    /**
     * 失败回调
     * @param message
     * @param replyCode
     * @param replyText
     * @param exchange
     * @param routingKey
     */
    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        // 消息发送到转换器的时候没有对列,配置了备份对列该回调则不生效
        log.info("消息丢失:exchange({}),route({}),replyCode({}),replyText({}),message:{}",exchange,routingKey,replyCode,replyText,message);
    }
}
```



### 7、消息过期时间TTL

这里只是设置该条消息具有过期时间

代码方式1：

```java
// hello word
@Test
public void sendMsgHello(){
    // 参数3: MessagePostProcessor函数式接口，可以设置消息的一些属性
    rabbitTemplate.convertAndSend("hello","springboot send msg hello",message->{
        // 如果在消息队列中5s没有被消费，自动删除该消息
        message.getMessageProperties().setExpiration(String.valueOf(5000));
        return message;
    });
}
```



注解方式：

```java
@RabbitListener(queuesToDeclare = @Queue(
        name = "hello",
        durable = "true",
        exclusive = "false",
        autoDelete = "false",
        arguments = {
                @Argument(name = "x-max-length", value = "2", type = "java.lang.Integer"),
                @Argument(name = "x-message-ttl", value = "10000", type = "java.lang.Integer")
        })
)
```

参数介绍：
1、**name**: 队列的名称；
2、**actualName**: 队列的真实名称，默认用name参数，如果name为空，则根据规则生成一个；
3、**durable**: 是否持久化；
4、**exclusive**: 是否独享、排外的；
5、**autoDelete**: 是否自动删除；
6、**arguments**：队列的其他属性参数，有如下可选项：
（1）x-message-ttl：消息的过期时间，单位：毫秒；
（2）x-expires：队列过期时间，队列在多长时间未被访问将被删除，单位：毫秒；
（3）x-max-length：队列最大长度，超过该最大值，则将从队列头部开始删除消息；
（4）x-max-length-bytes：队列消息内容占用最大空间，受限于内存大小，超过该阈值则从队列头部开始删除消息；
（5）x-overflow：设置队列溢出行为。这决定了当达到队列的最大长度时消息会发生什么。有效值是drop-head、reject-publish或reject-publish-dlx。仲裁队列类型仅支持drop-head；
（6）x-dead-letter-exchange：死信交换器名称，过期或被删除（因队列长度超长或因空间超出阈值）的消息可指定发送到该交换器中；
（7）x-dead-letter-routing-key：死信消息路由键，在消息发送到死信交换器时会使用该路由键，如果不设置，则使用消息的原来的路由键值
（8）x-single-active-consumer：表示队列是否是单一活动消费者，true时，注册的消费组内只有一个消费者消费消息，其他被忽略，false时消息循环分发给所有消费者(默认false)
（9）x-max-priority：队列要支持的最大优先级数;如果未设置，队列将不支持消息优先级；
（10）x-queue-mode（Lazy mode）：将队列设置为延迟模式，在磁盘上保留尽可能多的消息，以减少RAM的使用;如果未设置，队列将保留内存缓存以尽可能快地传递消息；
（11）x-queue-master-locator：在集群模式下设置镜像队列的主节点信息。



### 8、死信队列

`DLX (Dead-Letter-Exchange)`可被称之为死信交换机，也有人称死信邮箱。当消息在一个队列中变成死信`(Dead message)`之后，它能被重新发送到另一个交换机中，这个交换机就是DLX，绑定DLX的队列就称之为死信队列。

消息变成死信，可能由于以下的原因：

- 消息被拒绝
- 消息过期
- 队列达到最大长度

DLX也是一个正常的交换机。当这个队列中存在死信时，Rabbitmq就会自动地将这个消息重新发布到设置的DLX上去，进而被路由到另一个队列，即死信队列。



### 9、延迟队列

延迟队列存储的对象时对应的延迟消息；所谓“延迟消息”是指当消息被发送以后，并不想让消费者立刻拿到消息，而是等待一定时间后，消费者才能拿到这个消息进行消费。

在RabbitMQ中延迟队列可以通过`过期时间ttl`+`死信队列`来实现。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/延迟队列原理.png" style="float: left; zoom: 80%;" />



> 注解方式

- 延迟队列声明，并设置消息过期时间、死信交换机、死信路由
- 业务队列声明，将死信交换机与业务队列绑定，设置路由

消息存入延迟队列，10s后从延迟队列过期，并重新路由到业务队列，消费者获取消费消息

```java
@Component
@Slf4j
public class ConsumerDelay {

    @RabbitListener(queuesToDeclare = @Queue(name = "delay_queue", durable = "true",arguments = {
            @Argument(name = "x-message-ttl", value = "10000", type = "java.lang.Integer"),
            @Argument(name = "x-dead-letter-exchange", value = "delay_exchange"),
            @Argument(name = "x-dead-letter-routing-key", value = "dlx.key")
    }))
    public void delay(){
        log.info("消息成功加入延迟队列，延迟时间10s");
    }

    @RabbitListener(bindings = {
            @QueueBinding(value = @Queue(name = "process_queue", durable = "true"),
                          exchange = @Exchange(name = "delay_exchange", type = "direct"),
                          key = "dlx.key")
    })
    public void receive(String msg, Channel channel, Message message) throws IOException {
        System.out.println("accept process_queue msg ---> " + msg);
        // 由于开启了手动确认，所以需要以下代码手动确认
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);
    }
}
```



生产者

```java
// delayQueue
@Test
public void sendMsgDelay(){
    rabbitTemplate.convertAndSend("delay_queue","delay msg");
}
```





> 代码方式

- 延迟队列
- 交换机
- 业务队列

**创建对应队列**

```java
/**
 * @Author: heroC
 * Create: 2020/8/14 10:45
 * Description: 创建队列，创建交换机，队列绑定交换机
 * 延迟队列：延迟队列就是将消息存入队列，消息到期了成了死信就通过交换机从该队列移到另一个业务队列，
 * 然后用户再从业务队列中获取信息消费。中间这个队列就起到了一个延迟的效果，所以叫做延迟队列。
 * Version: V1.0
 */
@Configuration
public class DelayQueueConfig {
    /**为了更贴合业务，参数名不使用DeadQueue之类的*/
    /**延迟队列名*/
    public static final String DELAY_QUEUE = "delay.queue";
    /**延迟队列(死信队列)交换器名*/
    private static final String DELAY_EXCHANGE = "delay.exchange";
    /**处理业务的队列(死信队列)*/
    public static final String PROCESS_QUEUE = "process.queue";
    /**ttl(10秒)*/
    private static int DELAY_EXPIRATION = 10000;

    /**
     * 创建延迟队列
     * "x-dead-letter-exchange"参数定义死信队列交换机
     * "x-dead-letter-routing-key"定义死信队列中的消息重定向时的routing-key
     * "x-message-ttl"定义消息的过期时间
     * "x-max-length"定义该队列存储的最大的消息数量
     */
    @Bean
    public Queue delayQueue(){
        return QueueBuilder.durable(DELAY_QUEUE)
                .withArgument("x-dead-letter-exchange", DELAY_EXCHANGE)
                .withArgument("x-dead-letter-routing-key", PROCESS_QUEUE)
                .withArgument("x-message-ttl", DELAY_EXPIRATION)
                .withArgument("x-max-length",2)
                .build();
    }

    /**创建用于业务的队列*/
    @Bean
    public Queue processQueue(){
        return QueueBuilder.durable(PROCESS_QUEUE).build();
    }

    /**创建一个DirectExchange*/
    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange(DELAY_EXCHANGE);
    }

    /**绑定Exchange和queue，把消息重定向到业务queue*/
    @Bean
    Binding dlxBinding(DirectExchange directExchange, Queue processQueue){
        return BindingBuilder.bind(processQueue)
                .to(directExchange)
                .with(PROCESS_QUEUE);
        //绑定，以PROCESS_QUEUE为routing key
    }
}
```

生产者：

```java
 // delayQueue
@Test
public void sendMsgDelay(){
    // 将消息发送给延迟队列，消息到期后，成为死信，会路由到业务队列
    rabbitTemplate.convertAndSend(DelayQueueConfig.DELAY_QUEUE,"delay msg");
}
```

消费者：

```java
@Component
public class ConsumerDelay {

    // 消费者消费业务上的消息
    @RabbitListener(queues = DelayQueueConfig.PROCESS_QUEUE)
    public void receive(String msg){
        System.out.println(msg);
    }
}
```



### 10、事务

**事务与确认回调、返回回调不能并存。**

配置类

```java
/**
 * 配置启动rabbitmq事务，到ioc容器
 * @param connectionFactory
 * @return
 */
@Bean("rabbitTransactionManager")
public RabbitTransactionManager rabbitTransactionManager(CachingConnectionFactory connectionFactory){
    return new RabbitTransactionManager(connectionFactory);
}
```



发送者

```java
@GetMapping("/send")
// 遇到异常回滚，事务代理者是rabbitTransactionManager
@Transactional(rollbackFor = Exception.class, transactionManager = "rabbitTransactionManager")
public String sendMsg(){
    for (int i = 0; i < 20; i++) {
        CorrelationData correlationId = new CorrelationData(UUID.randomUUID().toString());
        /**
             * 参数1：exchange
             * 参数2：routingKey 任务模式就是队列名
             * 参数3：发布内容
             * 参数4：关联数据，该消息的id
             */
        rabbitTemplate.convertAndSend("","work","springboot send msg work "+ i,correlationId);
    }
    return "已发送";
}
```



> 如果在test环境下测试事务需要关闭回滚
>
> ```java
> @Test
> @Transactional
> @Rollback(false)
> public void sendMsgDirect(){
>  ...
> }
> ```
>
> @Rollback 文档解释：
>
> ```java
> /**
>  * Whether the <em>test-managed transaction</em> should be rolled back
>  * after the test method has completed.
>  * <p>If {@code true}, the transaction will be rolled back; otherwise,
>  * the transaction will be committed.
>  * <p>Defaults to {@code true}.
>  */
> ```



## 六、消息追踪

消息中心的消息追踪需要使用Trace实现，Trace是RabbitMQ用于记录每一次发送的消息，方便使用RabbitMQ的开发者调试、排错。可通过插件形式提供可视化界面。Trace启动后会自动创建系统Exchange：amq.rabbitmq.trace，每个队列会自动绑定该Exchange，绑定后发送到队列的消息都会记录到Trace日志



### 1、消息追踪启动与查看

命令：

```bash
# 查看插件列表
rabbitmq-plugins list

# rabbitmq启动trace插件
rabbitmq-plugins enable rabbitmq_tracing

# 打开trace的开关
rabbitmqctl trace_on

# 打开trace的开关（vhost为启动需要日志追踪的虚拟主机）
rabbitmqctl trace_on -p vhost

# 关闭trace开关
rabbitmqctl trace_off

# rabbitmq关闭trace插件
rabbitmq-plugins disable rabbitmq_tracing

# 只有administrator的角色才能查看日志界面
rabbitma set_user_tags <用户名> administrator
```

安装插件并启动trace_on之后，会发现多个exchange: amq.rabbitmq.trace，类型为topic



## 七、消息队列应用场景 优点

### 1、异步处理

场景：一个用户注册账号，需要发送邮件和短信

`串行`

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/串行.png" style="float: left; zoom: 80%;" />

通过串行方式，即将用户信息写入数据库(50ms)之后，通过发送邮件(50ms)，邮件发送成功，通知发送短信(50ms)，成功之后，返回界面提示信息给用户。这样消耗的时间150ms

`并行`

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/并行.png" style="float: left; zoom: 80%;" />

通过并行方式，即将用户信息写入数据库(50ms)之后，同时发送邮件和短信(50ms)，成功之后，返回界面提示信息给用户。这样耗时100ms

`消息队列`

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/消息队列.png" style="float: left; zoom: 80%;" />

通过消息队列方式，即即将用户信息写入数据库(50ms)之后，发送消息给mq服务器(5ms)，然后就不管了，短息和邮件的发送相对不影响用户的使用，所以这时就直接返回。这样耗时55ms



由此看来，异步处理大大节省时间。提高效率。



### 2、应用解耦

场景：双11

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/解耦01.png" style="float: left; zoom: 80%;" />

如果订单系统和库存系统直接交互，库存系统崩掉了，整个系统就崩掉了。耦合度很高。



<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/解耦02.png" style="float: left; zoom: 80%;" />

如果使用消息队列。订单系统的订单信息，直接发送到消息队列，库存系统从消息队列中获取消息及逆行处理，这样即使任意一方崩了，都不会互相影响。解耦的作用，也提高了效率。



### 3、流量削峰

场景：秒杀

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/流量削峰.png" style="float: left; zoom: 80%;" />

将消息队列放在客户端与服务端之间。给消息队列设置一个阈值，消息数量达到阈值，就不让用户的信息存入进来，直接返回错误页面等提示信息。秒杀服务系统从消息队列中逐一获取消息，进行处理。这样减轻了服务端的压力，也降低了服务因突然的压力而崩坏的风险。



## 七、RabbitMQ 集群

集群官方文档：https://www.rabbitmq.com/clustering.html



### 1、普通集群 (副本集群)



#### 1）架构图

> All data/state required for the operation of a RabbitMQ broker is replicated across all nodes. An exception to this are message queues, which by default reside on one node, though they are visible and reachable from all nodes. To replicate queues across nodes in a cluster, use a queue type that supports replication. This topic is covered in the [Quorum Queues](https://www.rabbitmq.com/quorum-queues.html) and [Classic Mirrored Queues](https://www.rabbitmq.com/ha.html) guides.   --- 摘自官网

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/普通集群架构图.png" style="float: left; zoom: 80%;" />

有唯一一个master节点，多个slave节点，所有slave节点都复制了master节点的数据信息(消息队列中的信息不会被复制)。消息队列中的信息只存储在master节点上。但是消费者可以通过访问slave节点拿到master节点上的消息，也可以直接从master节点上获取消息。**master宕机了，salve节点不允许对外提供服务。**



#### 2）集群搭建

node1、node2、node3表示服务器

```bash
# 0.集群规划
node1: 10.15.0.3 mq1 master 主节点
node2: 10.15.0.4 mq2 repl1  副节点
node3: 10.15.0.5 mq3 repl2  副节点

# 1.克隆三台机器主机名和ip映射
vim /etc/hosts 加入：
	10.15.0.3 mq1
	10.15.0.4 mq2
	10.15.0.5 mq3
node1：vim /etc/hostname 修改主机名为mq1
node2：vim /etc/hostname 修改主机名为mq2
node3：vim /etc/hostname 修改主机名为mq3

# 2.三个机器安装rabbitmq，并同步cookie文件，在node1上执行
scp /var/lib/rabbitmq/.erlang.cookie root@mq2:/var/lib/rabbitmq
scp /var/lib/rabbitmq/.erlang.cookie root@mq3:/var/lib/rabbitmq

# 3.查看cookie是否一致
node1: cat /var/lib/rabbitmq/.erlang.cookie
node2: cat /var/lib/rabbitmq/.erlang.cookie
node3: cat /var/lib/rabbitmq/.erlang.cookie

# 4.后台启动rabbitmq所有节点 执行如下命令，启动成功访问管理界面
rabbitmq-server -detached

# 5.在node2和node3执行如下命令加入集群：
1.关闭 rabbitmqctl stop_app
2.加入集群 rabbitmqctl join_cluster rabbit@mq1
3.启动服务 rabbitmqctl start_app

# 6.查看集群状态，任意节点执行：
rabbitmqctl cluster_status

# 7.如果出现如下显示，集群搭建成功：
Cluster status of node rabbit@mq3 ...
[{nodes,[{disc,[rabbit@mq1,rabbit@mq2,rabbit@mq3]}]},
{running_name,[rabbit@mq1,rabbit@mq2,rabbit@mq3]},
{cluster_name,<<"rabbit@mq1">>},
{partitions,[]},
{alarms,[{rabbit@mq1,[]},{rabbit@mq2,[]},{rabbit@mq3,[]}]}]
```

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/集群结果.png" style="float: left; zoom: 80%;" />



### 2、镜像集群

镜像集群基于普通集群，搭建的

镜像集群官方文档：https://www.rabbitmq.com/ha.html

> This guide covers mirroring (queue contents replication) of classic queues. [Quorum queues](https://www.rabbitmq.com/quorum-queues.html) is an alternative, more modern queue type that offers high availability via replication and focuses on data safety.  --- 摘自官网
>
> By default, contents of a queue within a RabbitMQ cluster are located on a single node (the node on which the queue was declared). This is in contrast to exchanges and bindings, which can always be considered to be on all nodes. Queues can optionally be made *mirrored* across multiple nodes.  --- 摘自官网

​	`镜像队列机制就是将队列在三个节点之间设置主从关系，消息会在三个节点之间进行自动同步，且如果其中一个节点不可用，并不会导致消息丢失或服务不可用的情况，提升MQ集群的整体高可用性。`



#### 1）架构图

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/镜像集群.png" style="float: left; zoom: 80%;" />



#### 2）配置集群

```bash
# 0.策略说明
rabbitmqctl set_policy [-p <vhost>] [--priority <priority>] [--apply-to <apply-to>] <name> <pattern> <definition>

-p Vhost: 可选参数，针对指定vhost下的queue进行设置
Name： policy名称
Pattern： 匹配queue的模式（正则表达式）
Definition： 镜像意义，包括三个部分ha-mode，ha-params，ha-sync-mode
			 ha-mode：指明镜像队列的模式，有效值为all/exactly/nodes
			 		  all：表示在集群中所有的节点上进行镜像
			 		  exactly：表示在指定个数的节点上进行镜像，节点的个数由ha-params指定
			 		  nodes：表示在指定的节点上进行镜像，节点名称通过ha-params指定
			 ha-params：ha-mode模式需要的参数
			 ha-sync-mode：进行队列中消息的同步方式，有效值为automatic和manual
			 priority：可选参数，policy的优先级，数越大优先级越高

# 1.查看当前策略
rabbitmqctl list_policies

# 2.添加策略 ha-all
rabbitmqctl set_policy ha-all '^hello' '{"ha-mode":"all","ha-sync-mode":"automatic"}'
说明：正则表达式 ^ 表示所有匹配 ^hello：匹配所有hello开头的队列

# 3.删除策略 ha-all
rabbitmqctl clear_policy ha-all
```

配置完成，就是镜像队列了。



## 八、常见问题

### 1、消息堆积

当消息上产的速度长时间，远远大于消费的速度时。就会曹诚消息堆积。



- **消息堆积的影响**
  + 可能导致新消息无法入队列
  + 可能导致就消息无法丢失
  + 消息等待消费的时间过长，超出了业务容忍范围
- **产生堆积的情况**
  + 生产者突然大量发布消息
  + 消费者消费失败
  + 消费者出现性能瓶颈
  + 消费者挂掉
- **解决办法**
  + 排查消费者的消费性能瓶颈
  + 增加消费者的多线程处理
  + 部署增加多个消费者



### 2、消息丢失

因某种原因，消息被丢失。丢失分三种情况：生产者消息丢失、MQ消息丢失、消费者消息丢失



> **生产者消息丢失**

由于生产者在发送消息到MQ的过程中因某种原因丢失了消息。为了防止发生。可以采用确认机制。确认回调、返回回调。

配置文件

```yaml
publisher-confirm-type: correlated # 须配置这个才会确认回调
publisher-returns: true # 开启发送失败退回
```

java类

```java
@RestController
@Slf4j
public class ProviderController implements RabbitTemplate.ConfirmCallback,RabbitTemplate.ReturnCallback{

    private RabbitTemplate rabbitTemplate;

    /**
     * 构造方法注入
     * @param rabbitTemplate
     */
    @Autowired
    public ProviderController(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
        //这是是设置回调能收到发送到响应
        rabbitTemplate.setConfirmCallback(this);
        //如果设置备份队列则不起作用
        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setReturnCallback(this);
        // 开启事务
        rabbitTemplate.setChannelTransacted(true);
    }

    @GetMapping("/send")
    //@Transactional(rollbackFor = Exception.class, transactionManager = "rabbitTransactionManager")
    public String sendMsg(){
        for (int i = 0; i < 20; i++) {
            CorrelationData correlationId = new CorrelationData(UUID.randomUUID().toString());
            /**
             * 参数1：exchange
             * 参数2：routingKey 任务模式就是队列名
             * 参数3：发布内容
             * 参数4：关联数据，该消息的id
             */
            rabbitTemplate.convertAndSend("","work","springboot send msg work "+ i,correlationId);
        }
        return "已发送";
    }

    /**
     * 确认回调
     * @param correlationData
     * @param ack
     * @param cause
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if (ack){
            log.info("消息发送成功：correlationData({}),ack({}),cause({})",correlationData,ack,cause);
        }else {
            log.info("消息发送失败：correlationData({}),ack({}),cause({})",correlationData,ack,cause);
            // 重发
        }
    }

    /**
     * 失败回调
     * @param message
     * @param replyCode
     * @param replyText
     * @param exchange
     * @param routingKey
     */
    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        // 消息发送到转换器的时候没有对列,配置了备份对列该回调则不生效
        log.info("消息丢失:exchange({}),route({}),replyCode({}),replyText({}),message:{}",exchange,routingKey,replyCode,replyText,message);
    }
}
```

失败了，可以重发。



> **MQ消息丢失**

由于MQ服务器的宕机等原因，消息丢失。可通过设置队列的持久化操作，而防止MQ服务器消息的丢失。当MQ服务器宕机了，会将消息队列中的数据持久化到内存中。spring整合rabbitmq，声明的队列默认都是持久化的，为了以防万一，最好是显式声明。



> **消费者消息丢失**

在MQ服务器发送消息到消费者的过程中，发生了消息丢失。

解决办法，开启手动ACK机制。消费者收到消息之后，消费完了消息，就发送ACK确认信息给MQ服务器。MQ服务器接收到了ACK确认，才删除该条消息。



### 3、有序消费消息

> 场景1

当RabbitMQ采用work Queue模式，此时只会有一个Queue但是会有多个Consumer,同时多个Consumer直接是竞争关系，此时就会出现MQ消息乱序的问题。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/有序消费场景1.jpg" style="float: left;" />

**解决：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/有序消费场景1解决.jpg" style="float: left;" />



> 场景2

当RabbitMQ采用简单队列模式的时候,如果消费者采用多线程的方式来加速消息的处理,此时也会出现消息乱序的问题。

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/有序消费场景2.jpg" style="float: left;" />

**解决**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMQ/有序消费场景2解决.jpg" style="float: left;" />

### 4、重复消费

​	为了防止消息在消费者端丢失，会采用手动回复MQ的方式来解决，同时也引出了一个问题，消费者处理消息成功，手动回复MQ时由于网络不稳定，连接断开，导致MQ没有收到消费者回复的消息，那么该条消息还会保存在MQ的消息队列，由于MQ的消息重发机制，会重新把该条消息发给和该队列绑定的消息者处理，这样就会导致消息重复消费。而有些操作是不允许重复消费的，比如下单，减库存，扣款等操作。  

​	MQ重发消息场景：

​	1.消费者未响应ACK，主动关闭频道或者连接

​	2.消费者未响应ACK，消费者服务挂掉



**解决：**

​	如果消费消息的业务是幂等性操作（同一个操作执行多次，结果不变）就算重复消费也没问题，可以不做处理，如果不支持幂等性操作，如：下单，减库存，扣款等，那么可以在消费者端每次消费成功后将该条消息id保存到数据库，每次消费前查询该消息id，如果该条消息id已经存在那么表示已经消费过就不再消费否则就消费。本方案采用redis存储消息id，因为redis是单线程的，并且性能也非常好，提供了很多原子性的命令，本方案使用setnx命令存储消息id。

