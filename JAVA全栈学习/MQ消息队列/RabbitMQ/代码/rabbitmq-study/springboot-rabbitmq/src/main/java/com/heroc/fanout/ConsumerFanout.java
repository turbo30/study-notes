package com.heroc.fanout;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author: heroC
 * Create: 2020/8/13 19:20
 * Description:
 * Version: V1.0
 */
@Component
public class ConsumerFanout {

    /**
     * 广播，绑定队列和交换机
     * 注解 @Queue不写参数是产生一个临时队列； @Exchange 是交换机
     * @param msg
     */
    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "log_fanout",type = "fanout"))
    })
    public void receiveOne(String msg){
        System.out.println("consumer1 ---> "+msg);
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "log_fanout",type = "fanout"))
    })
    public void receiveTwo(String msg){
        System.out.println("consumer2 ---> "+msg);
    }
}
