package com.heroc.delay;

import com.heroc.config.DelayQueueConfig;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @Author: heroC
 * Create: 2020/8/14 11:09
 * Description:
 * Version: V1.0
 */
@Component
@Slf4j
public class ConsumerDelay {

    /*
    // 代码方式的消费者
    @RabbitListener(queues = DelayQueueConfig.PROCESS_QUEUE)
    public void receive(String msg){
        System.out.println(msg);
    }*/

    @RabbitListener(queuesToDeclare = @Queue(name = "delay_queue", durable = "true",arguments = {
            @Argument(name = "x-message-ttl", value = "10000", type = "java.lang.Integer"),
            @Argument(name = "x-dead-letter-exchange", value = "delay_exchange"),
            @Argument(name = "x-dead-letter-routing-key", value = "dlx.key")
    }))
    public void delay(){
        log.info("消息成功加入延迟队列，延迟时间10s");
    }

    @RabbitListener(bindings = {
            @QueueBinding(value = @Queue(name = "process_queue", durable = "true"),
                          exchange = @Exchange(name = "delay_exchange", type = "direct"),
                          key = "dlx.key")
    })
    public void receive(String msg, Channel channel, Message message) throws IOException {
        System.out.println("accept process_queue msg ---> " + msg);
        // 由于开启了手动确认，所以需要以下代码手动确认
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);
    }
}
