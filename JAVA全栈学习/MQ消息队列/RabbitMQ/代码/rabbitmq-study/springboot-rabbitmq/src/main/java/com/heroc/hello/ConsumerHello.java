package com.heroc.hello;

import org.springframework.amqp.rabbit.annotation.Argument;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author: heroC
 * Create: 2020/8/13 16:44
 * Description:
 * Version: V1.0
 */
@Component
@RabbitListener(queuesToDeclare = @Queue(
        name = "hello",
        durable = "true",
        exclusive = "false",
        autoDelete = "false",
        arguments = {
                @Argument(name = "x-max-length", value = "2", type = "java.lang.Integer"),
                @Argument(name = "x-message-ttl", value = "10000", type = "java.lang.Integer")
        }))
public class ConsumerHello {

    @RabbitHandler
    public void receive(String msg) {
        System.out.println("accept：----> " + msg);
    }
}
