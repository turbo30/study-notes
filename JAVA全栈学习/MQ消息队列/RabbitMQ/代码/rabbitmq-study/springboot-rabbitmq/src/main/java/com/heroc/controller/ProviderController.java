package com.heroc.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @Author: heroC
 * Create: 2020/8/13 17:51
 * Description:
 * Version: V1.0
 */
@RestController
@Slf4j
public class ProviderController implements RabbitTemplate.ConfirmCallback,RabbitTemplate.ReturnCallback{

    private RabbitTemplate rabbitTemplate;

    /**
     * 构造方法注入
     * @param rabbitTemplate
     */
    @Autowired
    public ProviderController(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
        //这是是设置回调能收到发送到响应
        rabbitTemplate.setConfirmCallback(this);
        //如果设置备份队列则不起作用
        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setReturnCallback(this);
        // 开启事务
        // rabbitTemplate.setChannelTransacted(true);
    }

    @GetMapping("/send")
    //@Transactional(rollbackFor = Exception.class, transactionManager = "rabbitTransactionManager")
    public String sendMsg(){
        for (int i = 0; i < 20; i++) {
            CorrelationData correlationId = new CorrelationData(UUID.randomUUID().toString());
            /**
             * 参数1：exchange
             * 参数2：routingKey 任务模式就是队列名
             * 参数3：发布内容
             * 参数4：关联数据，该消息的id
             */
            rabbitTemplate.convertAndSend("","work","springboot send msg work "+ i,correlationId);
        }
        return "已发送";
    }

    /**
     * 确认回调
     * @param correlationData
     * @param ack
     * @param cause
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if (ack){
            log.info("消息发送成功：correlationData({}),ack({}),cause({})",correlationData,ack,cause);
        }else {
            log.info("消息发送失败：correlationData({}),ack({}),cause({})",correlationData,ack,cause);
            // 重发
        }
    }

    /**
     * 失败回调
     * @param message
     * @param replyCode
     * @param replyText
     * @param exchange
     * @param routingKey
     */
    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        // 消息发送到转换器的时候没有对列,配置了备份对列该回调则不生效
        log.info("消息丢失:exchange({}),route({}),replyCode({}),replyText({}),message:{}",exchange,routingKey,replyCode,replyText,message);
    }
}
