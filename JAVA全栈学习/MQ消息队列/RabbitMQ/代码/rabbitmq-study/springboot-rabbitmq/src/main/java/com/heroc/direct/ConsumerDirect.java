package com.heroc.direct;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author: heroC
 * Create: 2020/8/13 19:30
 * Description:
 * Version: V1.0
 */
@Component
public class ConsumerDirect {

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "log_direct",type = "direct"),
                    key = "info"
            )
    })
    public void receiveInfo(String msg){
        System.out.println("consumer info ---> "+msg);
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "log_direct",type = "direct"),
                    key = {"info","error"}
            )
    })
    public void receiveAll(String msg){
        System.out.println("consumer info/error ---> "+msg);
    }
}
