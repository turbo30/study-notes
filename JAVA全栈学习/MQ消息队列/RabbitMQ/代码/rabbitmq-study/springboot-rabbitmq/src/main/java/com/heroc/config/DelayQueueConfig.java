package com.heroc.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.transaction.RabbitTransactionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: heroC
 * Create: 2020/8/14 10:45
 * Description: 创建队列，创建交换机，队列绑定交换机
 * 延迟队列：延迟队列就是将消息存入队列，消息到期了成了死信就通过交换机从该队列移到另一个业务队列，
 * 然后用户再从业务队列中获取信息消费。中间这个队列就起到了一个延迟的效果，所以叫做延迟队列。
 * Version: V1.0
 */
@Configuration
public class DelayQueueConfig {
    /**为了更贴合业务，参数名不使用DeadQueue之类的*/
    /**延迟队列名*/
    public static final String DELAY_QUEUE = "delay.queue";
    /**延迟队列(死信队列)交换器名*/
    private static final String DELAY_EXCHANGE = "delay.exchange";
    /**处理业务的队列(死信队列)*/
    public static final String PROCESS_QUEUE = "process.queue";
    /**ttl(10秒)*/
    private static int DELAY_EXPIRATION = 10000;

    /**
     * 创建延迟队列
     * "x-dead-letter-exchange"参数定义死信队列交换机
     * "x-dead-letter-routing-key"定义死信队列中的消息重定向时的routing-key
     * "x-message-ttl"定义消息的过期时间
     * "x-max-length"定义该队列存储的最大的消息数量
     */
    @Bean
    public Queue delayQueue(){
        return QueueBuilder.durable(DELAY_QUEUE)
                .withArgument("x-dead-letter-exchange", DELAY_EXCHANGE)
                .withArgument("x-dead-letter-routing-key", PROCESS_QUEUE)
                .withArgument("x-message-ttl", DELAY_EXPIRATION)
                .withArgument("x-max-length",2)
                .build();
    }

    /**创建用于业务的队列*/
    @Bean
    public Queue processQueue(){
        return QueueBuilder.durable(PROCESS_QUEUE).build();
    }

    /**创建一个DirectExchange*/
    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange(DELAY_EXCHANGE);
    }

    /**绑定Exchange和queue，把消息重定向到业务queue*/
    @Bean
    Binding dlxBinding(DirectExchange directExchange, Queue processQueue){
        return BindingBuilder.bind(processQueue)
                .to(directExchange)
                .with(PROCESS_QUEUE);
        //绑定，以PROCESS_QUEUE为routing key
    }

    /**
     * 配置启动rabbitmq事务，到ioc容器
     * @param connectionFactory
     * @return
     */
    /*@Bean("rabbitTransactionManager")
    public RabbitTransactionManager rabbitTransactionManager(CachingConnectionFactory connectionFactory){
        return new RabbitTransactionManager(connectionFactory);
    }*/
}
