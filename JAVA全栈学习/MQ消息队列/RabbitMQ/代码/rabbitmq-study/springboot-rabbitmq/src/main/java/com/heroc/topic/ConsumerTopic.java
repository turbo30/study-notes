package com.heroc.topic;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author: heroC
 * Create: 2020/8/13 19:37
 * Description:
 * Version: V1.0
 */
@Component
public class ConsumerTopic {

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "log_topic",type = "topic"),
                    key = "user.*"
            )
    })
    public void receiveOne(String msg){
        System.out.println("consumer user.* ---> " + msg);
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(value = "log_topic",type = "topic"),
                    key = "user.#"
            )
    })
    public void receiveTwo(String msg){
        System.out.println("consumer user.# ---> " + msg);
    }
}
