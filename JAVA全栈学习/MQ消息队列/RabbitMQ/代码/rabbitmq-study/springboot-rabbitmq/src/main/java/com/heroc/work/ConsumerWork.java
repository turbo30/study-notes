package com.heroc.work;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.Time;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author: heroC
 * Create: 2020/8/13 17:25
 * Description:
 * Version: V1.0
 */
@Component
@Slf4j
public class ConsumerWork {

    @RabbitListener(queuesToDeclare = @Queue(name = "work"))
    public void receiveOne(String msg, Channel channel, Message message){
        try {
            System.out.println("consumer1: ---> " + msg);
            if (message.getBody().toString()!=null){
                channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
            }else {
                // 参数1：消息标识 参数2：是否重新入队列，true表示重新入队列，false表示丢弃
                channel.basicReject(message.getMessageProperties().getDeliveryTag(),false);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RabbitListener(queuesToDeclare = @Queue(name = "work"))
    public void receiveTwo(String msg, Channel channel,Message message, @Headers Map<String,Object> headers){
        try {
            TimeUnit.MILLISECONDS.sleep(1000);
            System.out.println("consumer2: ---> " + msg);
            if (message.getBody().toString()!=null){
                channel.basicAck((Long) headers.get(AmqpHeaders.DELIVERY_TAG),false);
            }else {
                // 参数1：消息标识 参数2：是否重新入队列，true表示重新入队列，false表示丢弃
                channel.basicReject(message.getMessageProperties().getDeliveryTag(),false);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
