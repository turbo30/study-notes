package com.heroc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: heroC
 * Create: 2020/8/13 16:20
 * Description:
 * Version: V1.0
 */

@SpringBootApplication
public class RabbitMqSpring {
    public static void main(String[] args) {
        SpringApplication.run(RabbitMqSpring.class,args);
    }
}
