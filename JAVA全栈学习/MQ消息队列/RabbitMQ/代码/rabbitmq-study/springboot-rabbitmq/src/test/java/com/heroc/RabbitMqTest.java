package com.heroc;

import com.heroc.config.DelayQueueConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author: heroC
 * Create: 2020/8/13 16:38
 * Description:
 * Version: V1.0
 */
@SpringBootTest(classes = RabbitMqSpring.class)
@RunWith(SpringRunner.class)
public class RabbitMqTest {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    // hello word
    @Test
    public void sendMsgHello(){
        // 参数3: MessagePostProcessor函数式接口，可以设置消息的一些属性
        rabbitTemplate.convertAndSend("hello","springboot send msg hello");
    }

    // work
    @Test
    public void sendMsgWork(){
        for (int i = 0; i < 50; i++) {
            rabbitTemplate.convertAndSend("work","springboot send msg work "+ i);
        }
    }

    // fanout
    @Test
    public void sendMsgFanout(){
        rabbitTemplate.convertAndSend("log_fanout","","springboot send msg fanout");
    }

    // direct
    @Test
    public void sendMsgDirect(){
        rabbitTemplate.convertAndSend("log_direct","error","springboot send msg direct error");
    }

    // topic
    @Test
    public void sendMsgTopic(){
        rabbitTemplate.convertAndSend("log_topic","user.item.save","springboot send msg topic");
    }

    // delayQueue
    @Test
    public void sendMsgDelay(){
        rabbitTemplate.convertAndSend("delay_queue","delay msg");
    }
}
