package com.heroc.hello;

import com.heroc.utils.RabbitMqUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.MessageProperties;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @Author: heroC
 * Create: 2020/8/12 20:44
 * Description:
 * Version: V1.0
 */
public class ProviderSendMsgTest {

    @Test
    public void sendMsg() throws IOException, TimeoutException {
        /*// 创建连接mq的连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 设置连接mq的主机
        connectionFactory.setHost("47.95.2.239");
        // 设置端口号
        connectionFactory.setPort(5672);
        // 设置需要连接的虚拟主机
        connectionFactory.setVirtualHost("/ems");
        // 设置用户名、密码
        connectionFactory.setUsername("ems");
        connectionFactory.setPassword("123");

        // 获取一个连接对象
        Connection connection = connectionFactory.newConnection();*/

        Connection connection = RabbitMqUtils.getConnection();

        // 连接对象获取一个通道
        Channel channel = connection.createChannel();

        /**
         * 通道绑定一个消息队列
         * 参数1：队列名称，没有会自动创建一个
         * 参数2：参数2：是否持久化消息队列，true为当mq服务器宕机了只会将消息队列持久化到硬盘中，存储的消息并不会，false则不会
         * 参数3：是否为独占式队列，true表示只有当前能使用该队列，false则不是
         * 参数4：是否在消息消费完成后自动删除队列，true表示会删除，false表示不会
         * 参数5：额外附加参数
         */
        channel.queueDeclare("hello",false,false,false,null);

        /**
         * 发布消息
         * 参数1：交换机名称，没有就空着
         * 参数2：消息队列名称
         * 参数3：额外附加参数，没有null
         *        额外参数值：MessageProperties.PERSISTENT_TEXT_PLAIN 队列中的消息持久化
         * 参数4：需要发布的内容
         */
        channel.basicPublish("","hello", MessageProperties.PERSISTENT_TEXT_PLAIN,"hello rabbitMQ".getBytes());

        // 关闭io流
        /*channel.close();
        connection.close();*/

        RabbitMqUtils.close(channel,connection);
    }
}
