package com.heroc.topic;

import com.heroc.utils.RabbitMqUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.junit.Test;

import java.io.IOException;

/**
 * @Author: heroC
 * Create: 2020/8/13 16:08
 * Description:
 * Version: V1.0
 */
public class Provider {
    @Test
    public void sendMsg() throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("logs_topic","topic");
        // 确定发送的消息路由
        String routingKey = "user.item.save";
        channel.basicPublish("logs_topic",routingKey,null,("topic rabbit "+routingKey).getBytes());
        RabbitMqUtils.close(channel,connection);
    }
}
