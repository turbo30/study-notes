package com.heroc.fanout;

import com.heroc.utils.RabbitMqUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.junit.Test;

import java.io.IOException;

/**
 * @Author: heroC
 * Create: 2020/8/13 11:45
 * Description:
 * Version: V1.0
 */
public class ProviderSendMsgTest {
    @Test
    public void providerSendMsg() throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();

        /**
         * 声明一个交换机，没有会自动创建一个，fanout类型是广播
         * 参数1：交换机名称
         * 参数2：交换机类型
         */
        channel.exchangeDeclare("logs","fanout");

        // 发布消息，发布到交换机，第二参数没有意义
        channel.basicPublish("logs","",null,"fanout Rabbit".getBytes());

        RabbitMqUtils.close(channel,connection);
    }
}
