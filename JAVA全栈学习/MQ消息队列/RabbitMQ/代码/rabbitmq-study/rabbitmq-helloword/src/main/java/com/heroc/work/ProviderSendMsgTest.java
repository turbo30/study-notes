package com.heroc.work;

import com.heroc.utils.RabbitMqUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.MessageProperties;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @Author: heroC
 * Create: 2020/8/12 20:44
 * Description: 任务模型 生产者
 * Version: V1.0
 */
public class ProviderSendMsgTest {

    @Test
    public void sendMsg() throws IOException, TimeoutException {

        // 获取连接
        Connection connection = RabbitMqUtils.getConnection();

        // 连接对象获取一个通道
        Channel channel = connection.createChannel();

        // 通道绑定队列
        channel.queueDeclare("work",true,false,false,null);

        // 通道发布消息
        for (int i = 0; i < 10; i++) {
            channel.basicPublish("","hello", MessageProperties.PERSISTENT_TEXT_PLAIN,(i+" work rabbitMQ").getBytes());
        }

        // 关闭io流
        RabbitMqUtils.close(channel,connection);
    }
}
