package com.heroc.utils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @Author: heroC
 * Create: 2020/8/13 10:07
 * Description:
 * Version: V1.0
 */
@Slf4j
public class RabbitMqUtils {

    private static ConnectionFactory connectionFactory;

    static {
        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("47.95.2.239");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/ems");
        connectionFactory.setUsername("ems");
        connectionFactory.setPassword("123");
    }

    public static Connection getConnection(){
        try{
            Connection connection = connectionFactory.newConnection();
            return connection;
        } catch (TimeoutException e) {
            log.warn("获取RabbitMQ连接超时异常....");
            e.printStackTrace();
        } catch (IOException e) {
            log.warn("获取RabbitMQ连接IO流异常....");
            e.printStackTrace();
        }
        return null;
    }

    public static void close(Channel channel, Connection connection){
        try {
            if (channel != null) {
                channel.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (TimeoutException e) {
            log.warn("获取RabbitMQ关闭超时异常....");
            e.printStackTrace();
        } catch (IOException e) {
            log.warn("获取RabbitMQ关闭IO流异常....");
            e.printStackTrace();
        }
    }

}
