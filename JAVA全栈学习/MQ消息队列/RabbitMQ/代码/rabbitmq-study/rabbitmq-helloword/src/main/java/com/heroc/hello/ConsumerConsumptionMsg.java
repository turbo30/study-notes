package com.heroc.hello;

import com.heroc.utils.RabbitMqUtils;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @Author: heroC
 * Create: 2020/8/12 21:07
 * Description:
 * Version: V1.0
 */
public class ConsumerConsumptionMsg {

    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = RabbitMqUtils.getConnection();

        Channel channel = connection.createChannel();

        // 绑定的消息队列，参数一定要与发布者绑定的消息队列参数一样
        channel.queueDeclare("hello",false,false,false,null);

        /**
         * 消费者消费消息
         * 参数1：消息队列名称
         * 参数2：开启消息的自动确认机制
         * 参数3：消费时的回调接口
         */
        channel.basicConsume("hello",true, new DefaultConsumer(channel){
            // 最后一个参数是从消息队列中取出的消息
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("消息......:  "+new String(body));
            }
        });

        // 消费者不关闭通道和连接，就会一直监听消息队列消费消息
        // 不建议关闭，因为要一直监听
        /*channel.close();
        connection.close();*/
    }
}
