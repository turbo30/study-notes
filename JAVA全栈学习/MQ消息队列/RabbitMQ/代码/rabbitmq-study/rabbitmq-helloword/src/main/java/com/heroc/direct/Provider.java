package com.heroc.direct;

import com.heroc.utils.RabbitMqUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.junit.Test;

import java.io.IOException;

/**
 * @Author: heroC
 * Create: 2020/8/13 15:35
 * Description:
 * Version: V1.0
 */
public class Provider {
    @Test
    public void sendMsg() throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("logs_direct","direct");
        // 确定发送的消息路由
        String routingKey = "info";
        channel.basicPublish("logs_direct",routingKey,null,("direct rabbit "+routingKey).getBytes());
        RabbitMqUtils.close(channel,connection);
    }
}
