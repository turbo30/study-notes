package com.heroc.fanout;

import com.heroc.utils.RabbitMqUtils;
import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @Author: heroC
 * Create: 2020/8/13 12:00
 * Description:
 * Version: V1.0
 */
public class Consumer1 {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();

        // 消费者声明一个交换机，与发布者一致
        channel.exchangeDeclare("logs","fanout");

        // 创建一个临时消息队列，当消费者通道和连接关闭时，该消息队列会自动删除，返回消息队列名称
        String queueName = channel.queueDeclare().getQueue();

        /**
         * 将消息队列与交换机绑定
         * 参数1：消息队列名称
         * 参数2：交换机名称
         * 参数3：路由key值，在这里没有意义
         */
        channel.queueBind(queueName,"logs","");

        // 消费消息
        channel.basicConsume(queueName,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(new String(body));
            }
        });
    }
}
