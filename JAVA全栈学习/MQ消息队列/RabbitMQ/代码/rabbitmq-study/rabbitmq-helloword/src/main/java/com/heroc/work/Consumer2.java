package com.heroc.work;

import com.heroc.utils.RabbitMqUtils;
import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @Author: heroC
 * Create: 2020/8/13 11:04
 * Description:
 * Version: V1.0
 */
public class Consumer2 {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();

        channel.basicQos(1);

        channel.queueDeclare("work",true,false,false,null);

        channel.basicConsume("work",false, new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("consumer 2: "+new String(body));
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        });

    }
}
