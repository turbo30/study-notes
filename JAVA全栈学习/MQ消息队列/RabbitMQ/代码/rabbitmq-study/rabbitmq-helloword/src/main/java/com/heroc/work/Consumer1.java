package com.heroc.work;

import com.heroc.utils.RabbitMqUtils;
import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @Author: heroC
 * Create: 2020/8/13 11:01
 * Description:
 * Version: V1.0
 */
public class Consumer1 {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare("work",true,false,false,null);

        // 每次接收消息的条数
        channel.basicQos(1);

        channel.basicConsume("work",false,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("consumer 1: "+new String(body));
                /**
                 * 消息处理完成发送确认信息，不发送确认信息，RabbitMQ服务器中会对该消息标注为unAcked
                 * 参数1：当前消息的唯一标识，用于告诉RabbitMQ服务器确认哪条消息，做消息删除处理
                 * 参数2：是否发送堕多条确认消息
                 */
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        });

    }
}
