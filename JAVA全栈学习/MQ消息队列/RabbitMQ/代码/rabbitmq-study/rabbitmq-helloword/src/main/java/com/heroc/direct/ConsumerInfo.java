package com.heroc.direct;

import com.heroc.utils.RabbitMqUtils;
import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @Author: heroC
 * Create: 2020/8/13 15:39
 * Description:
 * Version: V1.0
 */
public class ConsumerInfo {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("logs_direct","direct");
        String queueName = channel.queueDeclare().getQueue();
        // 消费路由为info的消息
        channel.queueBind(queueName,"logs_direct","info");
        channel.basicConsume(queueName,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(new String(body));
            }
        });
    }
}
