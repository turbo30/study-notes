package com.heroc.topic;

import com.heroc.utils.RabbitMqUtils;
import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @Author: heroC
 * Create: 2020/8/13 16:09
 * Description:
 * Version: V1.0
 */
public class Consumer8 {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("logs_topic","topic");
        String queue = channel.queueDeclare().getQueue();

        // 匹配user.*的消息
        channel.queueBind(queue,"logs_topic","user.*");

        channel.basicConsume(queue,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(new String(body));
            }
        });
    }
}
