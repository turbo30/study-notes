## _ 开发过程中

![](.\picture\空指针异常.png)

![](.\picture\空指针异常01.png)

如果使用serviceImpl中的方法，去调用另一个serviceImpl的方法，就会报空指针异常。



## _ IDEA开发工具设置的VM options参数，项目上线后怎么在tomcat进行设置

![](.\picture\tomcat启动项VMoptions.png)

项目上线就没办法用idea，这个时候需要修改tomcat的配置。  

找到tomcat的bin目录，找到catalina.bat文件（catalina.sh是Linux操作系统需要修改的文件，具体怎么修改另行百度）  添加以下代码：

![](.\picture\tomcat配置VMoptions.png)

```bat
set JAVA_OPTS=%JAVA_OPTS% -Dfile.encoding=UTF-8
```



## _ IDEA环境解决html网页乱码

在tomcat的配置中的 VM options 上加上。

`-Dfile.encoding=UTF-8 `，重新运行容器，清除浏览器缓存后，网页乱码解决了，

但是容器控制台中文信息就已经乱码了。

淇℃伅 [原本是‘信息’]

于是修改idea64.exe.vmoptions 信息，在最后加上：

```bat
-Dfile.encoding=UTF-8
-Dsun.jnu.encoding=UTF-8
```

重启idea ，解决问题。



## _ 解决tomcat部署vue项目后，刷新页面问题

在tomcat的web.xml中加入该配置即可：

```xml
<error-page>
    <error-code>404</error-code>
    <location>/index.html</location>
</error-page>
```



## _ IDEA 提交JavaWeb项目到gitee



### 创建本地git仓库

![](.\picture\创建git仓库.png)

### 添加忽略配置文件

```
# Built application files and Maven
target/
pom.xml.tag
pom.xml.releaseBackup
pom.xml.versionsBackup
pom.xml.next
release.properties
dependency-reduced-pom.xml
buildNumber.properties
.mvn/timing.properties

# Compiled class files
*.class

# Log Files
*.log

# About IntelliJ
*.iml
/.idea/
/out/

# BlueJ files
*.ctxt

# Mobile Tools for Java (J2ME)
.mtj.tmp/

# macOS
.DS_Store

# Package Files
*.jar
*.war
*.ear
*.zip
*.tar.gz
*.rar

# CMake
cmake-build-debug/

# File-based project format
*.iws

# mpeltonen/sbt-idea plugin
.idea_modules/

# JIRA plugin
atlassian-ide-plugin.xml

# Crashlytics plugin (for Android Studio and IntelliJ)
com_crashlytics_export_strings.xml
crashlytics.properties
crashlytics-build.properties
fabric.properties

# virtual machine crash logs, see http://www.java.com/en/download/help/error_hotspot.xml
hs_err_pid*
```



### 添加到本地仓库

![](.\picture\git的add.png)

先add，再commit



### 添加远程仓库连接

![](.\picture\添加remotes.png)



### 如果远程仓库中有新文件，需要将其pull到本地仓库

点击pull，等待。。。 出现refusing to merge unrelated histories 拒绝合并不相关的历史，  因为本地的内容确实是没和远程的文件接触过。 打开IDEA-Terminal，输入下面命令：

```
git pull origin master --allow-unrelated-histories
```

简单的git pull不能把远程仓库的文件拉下来，需要用到上边的git命令。这个命令是告诉系统，我允许合并不相关历史的内容。  然后直接push就可以成功了。如果提示git不是内部命令，说明没有配置环境变量。

