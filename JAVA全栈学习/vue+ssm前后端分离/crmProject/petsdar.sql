/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 8.0.19 : Database - petsdar
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`petsdar` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `petsdar`;

/*Table structure for table `account` */

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '账户ID',
  `user_name` varchar(20) NOT NULL COMMENT '账户登录名',
  `password` varchar(35) NOT NULL COMMENT '账户密码',
  `create_time` datetime NOT NULL COMMENT '账户信息创建时间',
  `update_time` datetime NOT NULL COMMENT '账户信息更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=202006281845002 DEFAULT CHARSET=utf8;

/*Data for the table `account` */

insert  into `account`(`id`,`user_name`,`password`,`create_time`,`update_time`) values (202006281845001,'heroc','34A213D372422BDCAD8E7300E202E7CAA74','2020-06-28 20:07:52','2020-06-28 20:07:52');

/*Table structure for table `emp` */

DROP TABLE IF EXISTS `emp`;

CREATE TABLE `emp` (
  `emp_id` bigint NOT NULL AUTO_INCREMENT COMMENT '员工ID',
  `emp_name` varchar(20) NOT NULL COMMENT '员工姓名',
  `emp_sex` varchar(2) NOT NULL DEFAULT '男' COMMENT '员工性别',
  `emp_hiredate` datetime NOT NULL COMMENT '员工入职日期',
  `emp_shop` varchar(32) NOT NULL COMMENT '员工所在店铺名',
  `emp_city` varchar(20) NOT NULL COMMENT '员工所在城市',
  `emp_position` varchar(32) NOT NULL COMMENT '员工职位',
  `emp_tel` varchar(11) NOT NULL COMMENT '员工联系电话',
  `emp_create_time` datetime NOT NULL COMMENT '员工信息创建时间',
  `emp_update_time` datetime NOT NULL COMMENT '员工信息更新时间',
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20200705155557708 DEFAULT CHARSET=utf8;

/*Data for the table `emp` */

insert  into `emp`(`emp_id`,`emp_name`,`emp_sex`,`emp_hiredate`,`emp_shop`,`emp_city`,`emp_position`,`emp_tel`,`emp_create_time`,`emp_update_time`) values (202007050083,'ttt','男','2020-07-04 00:00:00','宠达眉山仁寿分店','眉山市','店长','13456789012','2020-07-05 16:36:00','2020-07-05 16:36:00'),(202007051383,'agfgafgaf','男','2020-07-05 00:00:00','宠达成都环球中心分店','成都市','店长','13456789012','2020-07-05 16:42:13','2020-07-05 16:42:13'),(202007054997,'张还行','男','2020-07-05 00:00:00','宠达总部','总部','董事长','13456789012','2020-07-05 16:32:49','2020-07-05 16:32:49'),(202007055292,'还行行','男','2020-07-05 00:00:00','宠达成都环球中心分店','成都市','店长','13456789011','2020-07-05 16:40:53','2020-07-05 16:43:36'),(202007072233,'测试总部','男','2020-07-07 00:00:00','宠达总部','总部','财务部门','13018277623','2020-07-07 10:43:22','2020-07-07 10:43:22');

/*Table structure for table `keeper_account` */

DROP TABLE IF EXISTS `keeper_account`;

CREATE TABLE `keeper_account` (
  `keeper_account_id` varchar(32) NOT NULL COMMENT '店长账户ID[DZ+时间戳+随机数]',
  `keeper_account_login_name` varchar(20) NOT NULL COMMENT '店长登录账户名',
  `keeper_account_login_pwd` varchar(35) NOT NULL COMMENT '店长登录账户密码',
  `keeper_account_shop` varchar(32) NOT NULL COMMENT '店长管理店铺名',
  `keeper_account_user` varchar(20) NOT NULL COMMENT '店长姓名',
  `kt_create_time` datetime NOT NULL COMMENT '店长信息创建时间',
  `kt_update_time` datetime NOT NULL COMMENT '店长信息更新时间',
  PRIMARY KEY (`keeper_account_id`),
  UNIQUE KEY `uk_kt_login_name` (`keeper_account_login_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `keeper_account` */

insert  into `keeper_account`(`keeper_account_id`,`keeper_account_login_name`,`keeper_account_login_pwd`,`keeper_account_shop`,`keeper_account_user`,`kt_create_time`,`kt_update_time`) values ('DZ159409460893898','DZ-hero','24A213D372420BDCAD8E7300E202E7CAAa4','宠达成都大悦城分店','张还行','2020-07-07 12:03:29','2020-07-07 12:03:29');

/*Table structure for table `position` */

DROP TABLE IF EXISTS `position`;

CREATE TABLE `position` (
  `position_id` varchar(32) NOT NULL COMMENT '职位ID[HQRC+时间戳+随机数]',
  `position_name` varchar(20) NOT NULL COMMENT '职位名称',
  `position_create_time` date NOT NULL COMMENT '职位信息创建时间',
  `position_update_time` datetime NOT NULL COMMENT '职位信息更新时间',
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `position` */

insert  into `position`(`position_id`,`position_name`,`position_create_time`,`position_update_time`) values ('HQ159392082548757','董事长','2020-07-05','2020-07-05 11:50:33'),('HQ159392102287441','CEO','2020-07-05','2020-07-05 11:50:23'),('HQ159408974281471','财务部门','2020-07-07','2020-07-07 10:42:23'),('RC159394214705721','店长','2020-07-05','2020-07-05 17:44:20'),('RC159394222088441','经理','2020-07-05','2020-07-05 17:43:41'),('RC159402478311826','美容师','2020-07-06','2020-07-06 16:39:43'),('RC159408975908553','销售员','2020-07-07','2020-07-07 10:42:51');

/*Table structure for table `power` */

DROP TABLE IF EXISTS `power`;

CREATE TABLE `power` (
  `pr_id` varchar(2) NOT NULL COMMENT '权限ID',
  `is_add` tinyint unsigned NOT NULL COMMENT '新增权限',
  `is_del` tinyint unsigned NOT NULL COMMENT '删除权限',
  `is_update` tinyint unsigned NOT NULL COMMENT '修改权限',
  `is_query` tinyint unsigned NOT NULL COMMENT '查询权限',
  `is_shop_update` tinyint unsigned NOT NULL COMMENT '修改店铺权限',
  `is_shop_query` tinyint unsigned NOT NULL COMMENT '查询店铺权限',
  `pr_create_time` datetime NOT NULL COMMENT '权限信息创建时间',
  `pr_update_time` datetime NOT NULL COMMENT '权限信息更新时间',
  PRIMARY KEY (`pr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `power` */

insert  into `power`(`pr_id`,`is_add`,`is_del`,`is_update`,`is_query`,`is_shop_update`,`is_shop_query`,`pr_create_time`,`pr_update_time`) values ('DZ',1,1,1,1,1,1,'2020-07-04 17:18:33','2020-07-07 10:54:34');

/*Table structure for table `shop` */

DROP TABLE IF EXISTS `shop`;

CREATE TABLE `shop` (
  `shop_id` bigint NOT NULL AUTO_INCREMENT COMMENT '店铺ID',
  `chain` varchar(32) NOT NULL COMMENT '店铺名称',
  `shop_tel` varchar(13) NOT NULL COMMENT '店铺电话',
  `shop_address` varchar(100) NOT NULL COMMENT '店铺地址',
  `shop_keeper` varchar(20) NOT NULL COMMENT '店铺店长',
  `sp_create_time` datetime NOT NULL COMMENT '店铺信息创建时间',
  `sp_update_time` datetime NOT NULL COMMENT '店铺信息更新时间',
  PRIMARY KEY (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20200707103054208 DEFAULT CHARSET=utf8;

/*Data for the table `shop` */

insert  into `shop`(`shop_id`,`chain`,`shop_tel`,`shop_address`,`shop_keeper`,`sp_create_time`,`sp_update_time`) values (202007070143,'宠达成都大悦城分店','028-12345678','四川省成都市大悦城F1-101号','张还行','2020-07-07 12:02:01','2020-07-07 12:02:01'),(202007072544,'宠达眉山仁寿分店','028-12345678','四川省眉山市仁寿县仁和春天广场10-101号','张还行','2020-07-03 15:00:21','2020-07-03 15:38:48'),(202007075845,'宠达成都环球中心分店','028-12345678','四川省成都市武侯区大悦路518号大悦城1层-J01号','张还行','2020-07-03 15:06:57','2020-07-03 15:39:27');

/*Table structure for table `task` */

DROP TABLE IF EXISTS `task`;

CREATE TABLE `task` (
  `task_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `task` tinytext NOT NULL COMMENT '任务内容',
  `tk_create_time` datetime NOT NULL COMMENT '任务信息创建时间',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=202007021929006 DEFAULT CHARSET=utf8;

/*Data for the table `task` */

insert  into `task`(`task_id`,`task`,`tk_create_time`) values (1593694278808,'争对新产品做好宣传活动','2020-07-02 20:56:28'),(1593694278809,'销售额达到下发的文件要求','2020-07-02 20:56:28'),(1593694278810,'做好疫情防御工作','2020-07-02 20:56:28');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
