# login-Vue

需要用到的后端提供的接口

| 请求方式 | 路径                               | 功能           | 返回                         |
| -------- | ---------------------------------- | -------------- | ---------------------------- |
| post     | http://127.0.0.1:8080/api/finduser | 登录验证用户   | 成功返回200；失败返回404     |
| get      | http://127.0.0.1:8080/api/allbook  | 所有的书籍信息 | 返回书籍信息的json格式的数据 |

前后端进行数据交换时，一定要注意变量名一致，否则会得不到数据。



## 一、创建项目

在目标文件夹中以管理员身份打开cmd窗口，输入以下命令

1. 创建一个login-vue项目

   `vue init webpack login-vue`

2. 进入login-vue工程目录下，安装依赖

   `npm install vue-router --save-dev` 安装路由

   `npm install element-ui -S` 安装elementUI

   `npm install` 安装依赖

   `cnpm install sass-loader node-sass --save-dev` 安装sass加载器

   `npm run dev` 启动项目，测试是否正常启动

3. 用idea打开login-vue项目工程，删掉没必要的文件(components目录下的，assets目录下的，创建router目录)

4. 找到package.json文件，将sass版本改一下，否则会引起打包不成功

   ```json
    "node-sass": "^4.13.1",
   "sass-loader": "^7.0.3",
   ```



## 二、开发项目

因为要与后端进行数据交互，通信。所以需要安装Axios依赖

`npm install axios`

#### qs 与 json 的区别

> qs依赖，`npm install qs` 该依赖是可以在给后台传递数据时，以路径传递参数的格式传递数据
>
> qs.stringify({username:'heroC', password:'1130'});
>
> 传递数据格式是：.../...?username=heroC&password=1130
>
> JSON.stringify({username:'heroC', password:'1130'});
>
> 传递数据格式是：{"username":"heroC","password":"1130"}



### 1、main.js

main.js是该项目的启动入口，使用的资源，都在这里引入

```js
import Vue from 'vue'

// 导入router文件下的index.js文件的router相关的配置
import Router from './router'
// 导入axios
import axios from 'axios'
import VueAxios from 'vue-axios'
// 导入qs
import qs from 'qs'
// 导入element-ui
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import App from './App'

Vue.config.productionTip = false;

Vue.use(VueAxios, axios);
Vue.use(ElementUI);
Vue.use(qs);

new Vue({
  el: '#app',
  router: Router,
  components: {App},
  template: '<App/>',
  render: h => h(App)
})
```



### 2、config\index.js

这时该项目的配置文件。由于要跨域访问数据，进行数据交互。

因此，需要解决跨域请求的问题。该方法，只能在开发阶段有用，在实际上线的项目中，使用nginx。

```js
module.exports = {
  dev: {

    // Paths
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    // proxyTable是解决跨域的配置
    proxyTable: {
      '/api':{
        target:'http://127.0.0.1:8080', // 请求的地址，会在/api前面添加请求目的
        changeOrigin:true, // 开启允许跨域请求
        /*pathRewrite: {
          '^/api': '/' //如果请求路径中本身就有/api该属性不用配置。该属性的意思是将/api替换成/
        }*/
      }
    },
    
    // Various Dev Server settings
    host: 'localhost', // can be overwritten by process.env.HOST
    port: 8030, // can be overwritten by process.env.PORT, if port is in use, a free one will be determined
    autoOpenBrowser: false,
    errorOverlay: true,
    notifyOnErrors: true,
    poll: false, // https://webpack.js.org/configuration/dev-server/#devserver-watchoptions-
    ...
  }
  ...
}
```



### 3、App.vue

app.vue是全局的视图组件，所有组件的视图展示都在app.vue这个组件中

```vue
<template>
  <div id="app">
    <!--router指向的组件展示区域-->
    <router-view></router-view>
  </div>
</template>

<script>
export default {
  name: 'App'
}
</script>

<style>
</style>
```



### 4、Login.vue

在Component文件夹中，创建一个Login.vue组件。

使用了ElementUI，展示了一个登录页面，并对数据做了与后台通信。

功能：

- 用户名和密码不能为空
- 验证用户名和密码是否存在
- 验证成功，跳入Main.vue组件展示内容页

效果：

![](.\picture\登录页面.png)

![](.\picture\登录加载.png)

![](.\picture\登录失败.png)

![](.\picture\登录输入不正确.png)

```vue
<template>
  <div v-loading="loading" element-loading-text="登录中..." element-loading-spinner="el-icon-loading" element-loading-background="rgba(0, 0, 0, 0)">
    <el-form ref="loginForm" :model="form" :rules="rules" label-width="80px" class="login-box">
      <h3 class="login-title">欢迎登录</h3>
      <el-form-item label="账号" prop="username">
        <el-input type="text" placeholder="请输入账号" v-model="form.username"/>
      </el-form-item>
      <el-form-item label="密码" prop="password">
        <el-input type="password" placeholder="请输入密码" v-model="form.password"/>
      </el-form-item>
      <el-form-item>
        <el-button type="primary" v-on:click="onSubmit('loginForm')">登录</el-button>
      </el-form-item>
    </el-form>

    <el-dialog
      title="温馨提示"
      :visible.sync="dialogVisible"
      width="30%">
      <span>请输入账号和密码</span>
      <span slot="footer" class="dialog-footer">
        <el-button type="primary" @click="dialogVisible = false">确 定</el-button>
      </span>
    </el-dialog>

    <el-dialog
      title="温馨提示"
      :visible.sync="dialogVisibleError"
      width="30%">
      <span style="color: red">请输入正确的账号和密码</span>
      <span slot="footer" class="dialog-footer">
        <el-button type="primary" @click="dialogVisibleError = false">确 定</el-button>
      </span>
    </el-dialog>
  </div>
</template>

<script>
  export default {
    name: "Login",
    data() {
      return {
        form: {
          username: '',
          password: ''
        },

        // 表单验证，需要在 el-form-item 元素中增加 prop 属性
        rules: {
          username: [
            {required: true, message: '账号不可为空', trigger: 'blur'}
          ],
          password: [
            {required: true, message: '密码不可为空', trigger: 'blur'}
          ]
        },

        // 对话框显示和隐藏
        dialogVisible: false,
        dialogVisibleError: false,
        loading:false
      }
    },
    methods: {
      onSubmit(formName) {
        // 将数据包装到param中
        var param = {
          'userid':this.form.username,
          'pwd':this.form.password
        }
        // 为表单绑定验证功能
        this.$refs[formName].validate((valid) => {
          if (valid) {
            this.loading = true,
            // 使用 vue-router 路由到指定页面，该方式称之为编程式导航
            //
            // console.log(param.userid)
            /*localStorage.setItem("username",param.userid);
            this.$router.push({
              name:'main',
              params: {
                userID: param.userid
              }
            })*/
                
            // 使用axios，发起post方式的Ajax请求
            this.axios({
              url: '/api/finduser', // 请求地址，在confg\index.js中允许了跨域请求
              method: "post",
              data: JSON.stringify({
                userName: param.userid,
                pwd: param.pwd
              }), // 将数据转换为JSON
              headers: {
                'Content-Type': 'application/json; charset=utf-8'
              } // 设置数据的格式，以避免后端接收到的json出现乱码
            }).then((response)=>{ // 后端返回的响应
              if(response.data == 200){
                // sessionStorage.setItem("username",this.form.username);
                localStorage.setItem("username",this.form.username);
                this.$router.push("/main"); // 通过路由跳转到的组件请求
              }else {
                this.loading = false,
                this.dialogVisibleError = true;
                return false;
              }
            })
          } else {
            this.dialogVisible = true;
            return false;
          }
        });
      }
    }
  }
</script>

<style lang="scss" scoped>
  .login-box {
    border: 1px solid #DCDFE6;
    width: 350px;
    margin: 180px auto;
    padding: 35px 35px 15px 35px;
    border-radius: 5px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    box-shadow: 0 0 25px #909399;
  }

  .login-title {
    text-align: center;
    margin: 0 auto 40px auto;
    color: #303133;
  }
</style>
```



#### sessionStorage 与 localStorage 的区别

localstorage（本地存储）则以文件的方式存储在本地,永久保存（不主动删除，则一直存在）；

sessionstorage( 会话存储 ) ，临时保存，关掉该网站后，ssessionstorage就会被清除掉。

localStorage和sessionStorage只能存储字符串类型，对于复杂的对象可以使用ECMAScript提供的JSON对象的stringify和parse来处理



### 5、Main.vue

登录成功进入的页面。该页面将展示书籍信息，以及登录的用户名，退出登录按钮。

![](.\picture\内容展示页面.png)

![](.\picture\数据刷新加载.png)

```vue
<template>
  <div>
      <div style="float: left; margin-left: 5%">
        <div><strong>用户名：</strong><span v-text="userid"></span></div>
      </div>
      <div style="float: right; margin-right: 5%"><a href="javascript:;" @click="out">退出</a></div>

    <div style="clear: both; margin-left: 20%; margin-right: 20%; padding-top:1%">
      <div><h3>书籍列表</h3></div>
      <div v-loading="loading">
        <el-table :data="tableData" style="width: 100%">
          <el-table-column prop="bookID" label="ID" width="180"></el-table-column>
          <el-table-column prop="bookName" label="书名" width="180"></el-table-column>
          <el-table-column prop="bookCounts" label="数量" width="180"></el-table-column>
          <el-table-column prop="detail" label="描述"></el-table-column>
        </el-table>
      </div>
    </div>

  </div>
</template>

<script>
  export default {
    name: "Main",
    data() {
      return {
        userid: null,
        tableData: null,
        loading: false
      }
    },
    // 路由跳转到该页面会率先加载此方法
    beforeRouteEnter(to, form, next){
      next(vm => {
        vm.userid = localStorage.getItem('username')
        //vm.userid = sessionStorage.getItem('username')
        vm.getData();
        // 当数据请求过程中，会进行1秒的加载动画
        setTimeout(function () {
          vm.loading = false;
        },1000)
      });
    },
    methods: {
      getData: function () {
        this.loading = true,
        this.axios({
          url: '/api/allbook',
          method: "get"
        }).then((response) => {
          this.tableData = response.data});
      },
      out: function () {
        // sessionStorage.removeItem("username");
        localStorage.removeItem('username')
        this.$router.push('/')
      }
    }
  }
</script>

<style scoped>

</style>
```



## 三、router路由配置 登录拦截

```js
import VueRouter from "vue-router";
import Vue from "vue";
import Login from "../components/Login";
import Main from "../components/Main";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history', // 设置为history，就不会在路由路径中出现#符号
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },{
      path: '/main',
      name: 'main',
      props: true,
      meta:{
        requireAuth: true
      }, // 在需要拦截的请求中加入该meta属性，可以用于判断是否拦截该路由
      component: Main
    }
  ]
})

// 判断是否需要登录权限 以及是否登录
router.beforeEach((to, from, next) => {
  if (to.matched.some(res => res.meta.requireAuth)) {// 判断是否需要登录权限
    if (localStorage.getItem('username')) {// 判断是否登录
      next()
    } else {// 没登录则跳转到登录界面
      next({
        path: '/',
        query: {redirect: to.fullPath}
      })
    }
  } else {
    next()
  }
})

export default router
```



#### 路由在组件之间传递参数

>```js
>routes: [
>    {
>        path: '/main/:id', // /:是声明接收参数的变量，id就是用于接收参数的变量
>        name: 'main',
>        props: true, // 将变量存储在跳转的Main组件的props:['id']属性中，Main组件就可以从props属性中获取到变量值
>        meta:{
>            requireAuth: true
>        }, // 在需要拦截的请求中加入该meta属性，可以用于判断是否拦截该路由
>        component: Main
>    }
>]
>```
>
>传递参数：
>
>`this.$router.push({name:'路由名', params:{id:133} })`
>
>通过路由接收参数：
>
>`this.$route.params.id`



## 四、前端开发中遇见的问题

#### 登录拦截问题

> 登录拦截问题

```js
const router = new VueRouter({
  mode: 'history', // 设置为history，就不会在路由路径中出现#符号
  routes: [
    {
      path: '/main',
      name: 'main',
      props: true,
      meta:{
        requireAuth: true
      }, // 在需要拦截的请求中加入该meta属性，可以用于判断是否拦截该路由
      component: Main
    }
  ]
})

// 判断是否需要登录权限 以及是否登录
router.beforeEach((to, from, next) => {
  if (to.matched.some(res => res.meta.requireAuth)) {// 判断是否需要登录权限
    if (localStorage.getItem('username')) {// 判断是否登录
      next()
    } else {// 没登录则跳转到登录界面
      next({
        path: '/',
        query: {redirect: to.fullPath}
      })
    }
  } else {
    next()
  }
})
```



#### 跨域请求访问问题

> 解决跨域问题

```js
module.exports = {
  dev: {

    // Paths
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    // proxyTable是解决跨域的配置
    proxyTable: {
      '/api':{
        target:'http://127.0.0.1:8080', // 请求的地址，会在/api前面添加请求目的
        changeOrigin:true, // 开启允许跨域请求
        /*pathRewrite: {
          '^/api': '/' //如果请求路径中本身就有/api该属性不用配置。该属性的意思是将/api替换成/
        }*/
      }
    },
    
    // Various Dev Server settings
    host: 'localhost', // can be overwritten by process.env.HOST
    port: 8030, // can be overwritten by process.env.PORT, if port is in use, a free one will be determined
    autoOpenBrowser: false,
    errorOverlay: true,
    notifyOnErrors: true,
    poll: false, // https://webpack.js.org/configuration/dev-server/#devserver-watchoptions-
    ...
  }
  ...
}
```



#### 前端发送json数据给后端乱码问题

> 解决前端发送数据乱码问题

```js
this.axios({
    ...
    headers: {
        'Content-Type': 'application/json; charset=utf-8'
    } // 设置数据的格式，以避免后端接收到的json出现乱码
}
```



#### 全局存储登录用户名问题

> localstorage和sessionstorage的区别

```js
sessionStorage.setItem("username",this.form.username);
或者
localStorage.setItem("username",this.form.username);
```

