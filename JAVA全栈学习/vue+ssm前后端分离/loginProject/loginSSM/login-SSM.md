# login-SSM

需要提供给前端的接口

| 请求方式 | 路径                               | 功能           | 返回                         |
| -------- | ---------------------------------- | -------------- | ---------------------------- |
| post     | http://127.0.0.1:8080/api/finduser | 登录验证用户   | 成功返回200；失败返回404     |
| get      | http://127.0.0.1:8080/api/allbook  | 所有的书籍信息 | 返回书籍信息的json格式的数据 |

前后端进行数据交换时，一定要注意变量名一致，否则会得不到数据。



## 项目结构

<img src=".\picture\项目结构.png" style="float:left;" />



## 一、MySQL 建数据库 建表

```sql
CREATE DATABASE IF NOT EXISTS `ssmbuild`;

CREATE TABLE `books`(
  `bookID` INT(10) NOT NULL AUTO_INCREMENT COMMENT '书ID',
  `bookName` VARCHAR(100) NOT NULL COMMENT '书名',
  `bookCounts` INT(11) NOT NULL COMMENT '数量',
  `detail` VARCHAR(200) NOT NULL COMMENT '描述',
  PRIMARY KEY (`bookID`)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

DESC `books`;
DESC `user`;

INSERT INTO `books` VALUES
(1,'Java',1,'从入门到放弃'),
(2,'MySQL',10,'从删库到跑路'),
(3,'Linux',5,'从进门到出门');

SELECT * FROM `books`;

CREATE TABLE `user`(
  `userID` INT(10) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `userName` VARCHAR(30) NOT NULL UNIQUE COMMENT '用户名',
  `pwd` VARCHAR(100) NOT NULL COMMENT '用户密码',
  PRIMARY KEY (`userID`)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

INSERT INTO `user` VALUES(1,'heroC','4A213D37242BDCAD8E7300E202E7CAA4')

SELECT * FROM `user`;
```



## 二、创建项目 Maven配置

1. 通过idea创建一个名为login-SSM的空maven项目

2. 导入依赖包 pom.xml

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <project xmlns="http://maven.apache.org/POM/4.0.0"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
       <modelVersion>4.0.0</modelVersion>
   
       <groupId>org.example</groupId>
       <artifactId>login-SSM</artifactId>
       <version>1.0-SNAPSHOT</version>
   
       <dependencies>
   
           <!--junit测试-->
           <dependency>
               <groupId>junit</groupId>
               <artifactId>junit</artifactId>
               <version>4.12</version>
               <!--作用域是test，只能在test属性的包下使用 不写作用域则任何包下都可以使用-->
               <scope>test</scope>
           </dependency>
   
           <!--mysql连接驱动  连接池c3p0-->
           <dependency>
               <groupId>mysql</groupId>
               <artifactId>mysql-connector-java</artifactId>
               <version>8.0.15</version>
           </dependency>
           <dependency>
               <groupId>com.mchange</groupId>
               <artifactId>c3p0</artifactId>
               <version>0.9.5.2</version>
           </dependency>
   
           <!--servlet-->
           <dependency>
               <groupId>javax.servlet</groupId>
               <artifactId>jstl</artifactId>
               <version>1.2</version>
           </dependency>
   
           <!--mybatis-->
           <dependency>
               <groupId>org.mybatis</groupId>
               <artifactId>mybatis</artifactId>
               <version>3.5.0</version>
           </dependency>
           <!--mybatis-spring-->
           <dependency>
               <groupId>org.mybatis</groupId>
               <artifactId>mybatis-spring</artifactId>
               <version>2.0.0</version>
           </dependency>
   
           <!--spring-->
           <dependency>
               <groupId>org.springframework</groupId>
               <artifactId>spring-jdbc</artifactId>
               <version>5.2.0.RELEASE</version>
           </dependency>
   
           <dependency>
               <groupId>org.springframework</groupId>
               <artifactId>spring-webmvc</artifactId>
               <version>5.2.0.RELEASE</version>
           </dependency>
   
           <dependency>
               <groupId>org.aspectj</groupId>
               <artifactId>aspectjweaver</artifactId>
               <version>1.9.4</version>
           </dependency>
   
           <!--json-->
           <dependency>
               <groupId>com.alibaba</groupId>
               <artifactId>fastjson</artifactId>
               <version>1.2.68</version>
           </dependency>
   
       </dependencies>
   
       <build>
           <resources>
               <resource>
                   <directory>src/main/resources</directory>
                   <includes>
                       <include>**/*.properties</include>
                       <include>**/*.xml</include>
                   </includes>
                   <filtering>true</filtering>
               </resource>
   
               <resource>
                   <directory>src/main/java</directory>
                   <includes>
                       <include>**/*.properties</include>
                       <include>**/*.xml</include>
                   </includes>
                   <filtering>true</filtering>
               </resource>
           </resources>
       </build>
   
   </project>
   ```

3. 项目右键**Add framework support** 添加web app框架

4. 配置 tomcat

   ![](.\picture\tomcat.png)

5. 创建文件夹

   ![](.\picture\创建文件夹.png)



## 三、utils工具类

#### MD5.java

```java
package com.heroc.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created with IntelliJ IDEA.
 * User: heroC
 * Date: 2019/01/10 9:53
 * Description: MD5加密，给密码加密之后的结果进行数据持久化
 * Version: V1.0
 */
public class MD5 {
        public static StringBuffer getMD5(String source) throws NoSuchAlgorithmException {
            //将明文转换成byte[]
            byte [] bytes = source.getBytes();
            //将byte字节数组通过MessageDigest进行MD5运算，得到一个新的字节数组。
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte [] newBytes = md.digest(bytes);
            //将新的字节数组转换成密文
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < newBytes.length; i++) {
                sb.append(String.format("%02X", newBytes[i]));
            }
            return sb;
        }

}
```



## 三、Mybatis 配置

#### mybatis-config.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>

    <settings>
        <setting name="logImpl" value="STDOUT_LOGGING"/>
    </settings>
    
    <!--环境 数据源 整合到了spring-dao.xml中-->

    <typeAliases>
        <package name="com.heroc.pojo"/>
    </typeAliases>
    
</configuration>
```



## 四、Spring配置

### 1、pojo层

因为ssmbuild数据库中有两个表，一个books表，一个user表

因此pojo层需要book类和user类

注意数据库的字段一定要与pojo中的类的变量名一致



#### Book.java

```java
package com.heroc.pojo;

import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/4/14
 * Time: 20:02
 * Description: 对应books表
 * Version: V1.0
 */
@Repository
public class Book {
    private int bookID;
    private String bookName;
    private int bookCounts;
    private String detail;

    public Book() {
    }

    public Book(int bookID, String bookName, int bookCounts, String detail) {
        this.bookID = bookID;
        this.bookName = bookName;
        this.bookCounts = bookCounts;
        this.detail = detail;
    }

    public int getBookID() {
        return bookID;
    }

    public void setBookID(int bookID) {
        this.bookID = bookID;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public int getBookCounts() {
        return bookCounts;
    }

    public void setBookCounts(int bookCounts) {
        this.bookCounts = bookCounts;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookID=" + bookID +
                ", bookName='" + bookName + '\'' +
                ", bookCounts=" + bookCounts +
                ", detail='" + detail + '\'' +
                '}';
    }
}
```



#### User.java

```java
package com.heroc.pojo;

import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/4/14
 * Time: 20:02
 * Description: 对应user表
 * Version: V1.0
 */
@Repository
public class User {
    private int userID;
    private String userName;
    private String pwd;

    public User() {
    }

    public User(int userID, String userName, String pwd) {
        this.userID = userID;
        this.userName = userName;
        this.pwd = pwd;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public String toString() {
        return "User{" +
                "userID=" + userID +
                ", userName='" + userName + '\'' +
                ", pwd='" + pwd + '\'' +
                '}';
    }
}
```



### 2、dao层

因为dao层是数据持久化层，与mybatis仅仅联系，所以先配置dao层



#### ...Mapper.java/xml

> 在dao层创建mapper接口，以及mapper配置文件

**BookMapper.java**

```java
package com.heroc.dao;

import com.heroc.pojo.Book;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/4/14
 * Time: 20:04
 * Description: 对book实体类的数据进行数据持久化
 * Version: V1.0
 */
public interface BookMapper {
    // 查询所有书籍
    List<Book> selectAllBook();
}
```

**BookMapper.xml**

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.dao.BookMapper">

    <select id="selectAllBook" resultType="book">
        select * from `books`
    </select>

</mapper>
```



**UserMapper.java**

```java
package com.heroc.dao;

import com.heroc.pojo.User;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/4/14
 * Time: 20:04
 * Description: 对user实体类的数据进行数据持久化
 * Version: V1.0
 */
public interface UserMapper {
    // 通过姓名查询用户信息
    User selectUserByName(String name);
}
```

**UserMapper.xml**

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.dao.UserMapper">

    <select id="selectUserByName" resultType="user" parameterType="String">
        select * from `user` where `userName` = #{name}
    </select>

</mapper>
```



### resource层

#### mysql-db.properties

> 在resource文件夹下，创建mysql-db.properties

```properties
jdbc.driver=com.mysql.cj.jdbc.Driver
# Mysql8.0+ 数据库没有设置时区时，连接时还需要加一个 serverTimezone=Asia/Shanghai
jdbc.url=jdbc:mysql://localhost:3306/ssmbuild?useSSL=true&useUnicode=true&characterEncoding=UTF-8
jdbc.username=root
jdbc.password=113011
# 不能直接使用username、password作为key值，这样配置的话在运行时会找系统的username
```



#### spring-dao.xml

> 在resource文件夹下，创建spring-dao.xml

**spring-dao.xml**

- 加载properties文件
- 注册数据源datasource：此项目用的c3p0
- 注册SqlSessionFactoryBean工厂：引入数据源，导入mybaits-config.xml，扫描mapper的xml文件
- 注册SqlSessionTemplate
- 注册MapperScannerConfigurer，用于扫描dao层的接口

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd
			http://www.springframework.org/schema/context
			https://www.springframework.org/schema/context/spring-context.xsd">

    <context:property-placeholder location="classpath:mysql-db.properties"/>

    <bean class="com.mchange.v2.c3p0.ComboPooledDataSource" id="dataSource">
        <property name="driverClass" value="${jdbc.driver}"/>
        <property name="jdbcUrl" value="${jdbc.url}"/>
        <property name="user" value="${jdbc.username}"/>
        <property name="password" value="${jdbc.password}"/>
        <property name="maxPoolSize" value="30"/>
        <property name="minPoolSize" value="10"/>
        <!--不自动提交commit-->
        <property name="autoCommitOnClose" value="false"/>
        <!--获取连接超时时间-->
        <property name="checkoutTimeout" value="10000"/>
        <!--获取失败，尝试连接次数-->
        <property name="acquireRetryAttempts" value="2"/>
    </bean>

    <bean class="org.mybatis.spring.SqlSessionFactoryBean" id="sqlSessionFactory">
        <property name="configLocation" value="classpath:mybatis-config.xml"/>
        <property name="dataSource" ref="dataSource"/>
        <property name="mapperLocations" value="classpath:com/heroc/dao/*.xml"/>
    </bean>

    <bean class="org.mybatis.spring.SqlSessionTemplate" id="sqlSession">
        <constructor-arg index="0" ref="sqlSessionFactory"/>
    </bean>

    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer" id="mapperScannerConfigurer">
        <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
        <property name="basePackage" value="com.heroc.dao"/>
    </bean>

</beans>
```



### 3、service层

service层是通过调用dao层对数据进行逻辑处理，以及业务处理，将结果返回给controller层



#### BookService.java

```java
package com.heroc.service;

import com.heroc.pojo.Book;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/4/14
 * Time: 21:11
 * Description: BookService接口，是提供对book有关的业务逻辑处理的接口
 * Version: V1.0
 */
public interface BookService {
    // 获取全部书籍的信息
    List<Book> selectAllBook();
}
```



#### BookServiceImpl.java

```java
package com.heroc.service;

import com.heroc.dao.BookMapper;
import com.heroc.pojo.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/4/14
 * Time: 21:12
 * Description: BookService接口的实现类，对方法进行实现，数据处理等
 * Version: V1.0
 */
@Service
public class BookServiceImpl implements BookService {

    private BookMapper bookMapper;

    @Autowired
    /* 因为对最终处理完的数据要进行数据持久化操作，
       或者需要获取数据库的信息等，因此会调用dao层的相关类。
       因为dao层的xml文件和接口，在spring-dao.xml中注册，
       因此可以通过set方法，对变量进行自动注入*/
    public void setBookMapper(BookMapper bookMapper) {
        this.bookMapper = bookMapper;
    }

    // 查询书籍所有信息，所以调用dao层进行书籍查询获取结果之后，将其返回
    @Override
    public List<Book> selectAllBook() {
        return bookMapper.selectAllBook();
    }
}

```



#### UserService.java

```java
package com.heroc.service;

import com.heroc.pojo.User;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/4/14
 * Time: 21:11
 * Description: UserService接口，是提供对user有关的业务逻辑处理的接口
 * Version: V1.0
 */
public interface UserService {
    // 通过用户名验证用户是否存在
    Boolean checkUserByName(User user);
}
```



#### UserServiceImpl.java

```java
package com.heroc.service;

import com.heroc.dao.UserMapper;
import com.heroc.pojo.User;
import com.heroc.utils.MD5;
import org.springframework.beans.factory.annotation.Autowired;

import java.security.NoSuchAlgorithmException;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/4/14
 * Time: 21:14
 * Description: UserService接口的实现类，对方法进行实现，数据处理等
 * Version: V1.0
 */
public class UserServiceImpl implements UserService {
    private UserMapper userMapper;

    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    // 验证user信息
    @Override
    public Boolean checkUserByName(User user){

        String userName = user.getUserName();
        String pwd = user.getPwd();
        User userResult = userMapper.selectUserByName(userName);

        try {
            // 验证从数据库中查到的密码与请求传递过来的user信息密码是否一致
            if(userResult != null){
                String userResultPwd = userResult.getPwd();
                String userMD5 = MD5.getMD5(pwd).toString();
                if(userMD5.equals(userResultPwd)){
                    return true;
                }
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return false;
    }
}
```



### resource层



#### spring-service.xml

该配置文件，对service层的类和接口进行配置，注册

- 扫描service层使用注解的类，进行注解配置
- 如果有类没有使用注解方式的注入或注册，那么就需要配置bean，手动注册和变量注入。比如：BookServiceImpl.java就是用来注解；UserServiceImpl.java就没有使用注解，需要手动注册到IOC容器中
- 注册DataSourceTransactionManager事务
- 使用aop方式，对service层中的所有方法都配置事务回滚

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd
			http://www.springframework.org/schema/tx
			https://www.springframework.org/schema/tx/spring-tx.xsd
			http://www.springframework.org/schema/aop
			https://www.springframework.org/schema/aop/spring-aop.xsd
			http://www.springframework.org/schema/context
			https://www.springframework.org/schema/context/spring-context.xsd">

    <context:component-scan base-package="com.heroc.service"/>

    <bean class="com.heroc.service.UserServiceImpl" id="userService">
        <property name="userMapper" ref="userMapper"/>
    </bean>

    <bean class="org.springframework.jdbc.datasource.DataSourceTransactionManager" id="transactionManager">
        <property name="dataSource" ref="dataSource"/>
    </bean>

    <tx:advice transaction-manager="transactionManager" id="transaction">
        <tx:attributes>
            <tx:method name="*"/>
        </tx:attributes>
    </tx:advice>
    <aop:config>
        <aop:pointcut id="transactionPointCut" expression="execution(* com.heroc.service.*.*(..))"/>
        <aop:advisor advice-ref="transaction" pointcut-ref="transactionPointCut"/>
    </aop:config>

</beans>
```



### 4、controller层



#### BookController.java

前端请求的路径为 localhost:8080/api/allbook

```java
package com.heroc.controller;

import com.alibaba.fastjson.JSON;
import com.heroc.pojo.Book;
import com.heroc.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/4/14
 * Time: 21:22
 * Description: 对映射路径，进行处理。@RestController表示返回字符串数据，不会经过视图解析器
 * Version: V1.0
 */
@RestController
@RequestMapping("/api")
public class BookController {

    private BookService bookService;
    @Autowired
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(value = "/allbook", produces = "application/json;charset=utf-8")
    // 本项目使用的是fastjson依赖包，进行json的转换，
    // 为保证前端收到的json数据不是乱码的，
    // 因此需要增加produces = "application/json;charset=utf-8"保证不乱码
    public String allBook(){
        List<Book> books = bookService.selectAllBook();
        // 将所有书籍的转换成json格式数据，传递给前端
        String bookJson = JSON.toJSONString(books);
        return bookJson;
    }
}
```



#### UserController.java

前端请求的路径为 localhost:8080/api/finduser

```java
package com.heroc.controller;

import com.alibaba.fastjson.JSON;
import com.heroc.pojo.User;
import com.heroc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/4/14
 * Time: 21:35
 * Description: 对映射路径，进行处理。@RestController表示返回字符串数据，不会经过视图解析器
 * Version: V1.0
 */
@RestController
@RequestMapping("/api")
public class UserController {
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/finduser")
    public String findUserByNameJson(@RequestBody String userJson){
        // 验证user信息，在数据库中的user信息是否一致
        User user = JSON.parseObject(userJson,User.class);
        //System.out.println(user.toString());
        Boolean checkResult = userService.checkUserByName(user);
        if(checkResult){
            return "200";
        }
        return "404";
    }
}
```



### web.xml

> SpringMVC，四大剑客：DispatcherServlet、映射器处理器、控制器处理器、视图解析器

配置controller层，即springmvc，需要在web目录下的WEB-INF目录下的web.xml

- servlet 配置DispatcherServlet
- filter 统一编码为UTF-8

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    
    <servlet>
        <servlet-name>dispatcher</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:applicationContext.xml</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>dispatcher</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>

    <filter>
        <filter-name>encoding</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>UTF-8</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>encoding</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    
</web-app>
```



### resource层

> 在spring-mvc.xml中配置springmvc的映射器处理器、控制器处理器、视图解析器

#### spring-mvc.xml

- 开启注解扫描controller层的包
- 配置2个处理器
- 过滤静态资源
- 注册视图解析器InternalResourceViewResolver

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd
			http://www.springframework.org/schema/context
			https://www.springframework.org/schema/context/spring-context.xsd http://www.springframework.org/schema/mvc https://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <context:component-scan base-package="com.heroc.controller"/>

    <mvc:annotation-driven/>
    <mvc:default-servlet-handler/>

    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver" id="internalResourceViewResolver">
        <property name="prefix" value="/WEB-INF/jsp/"/>
        <property name="suffix" value=".jsp"/>
    </bean>

</beans>
```



## 五、后台开发问题

### 解决接收前端数据问题

> 解决接收前端数据问题

使用@RequestBody注解可以接收前端发送的Json字符串数据

```java
@RequestMapping("/finduser")
public String findUserByNameJson(@RequestBody String userJson){
    // 验证user信息，在数据库中的user信息是否一致
    User user = JSON.parseObject(userJson,User.class);
    //System.out.println(user.toString());
    Boolean checkResult = userService.checkUserByName(user);
    if(checkResult){
        return "200";
    }
    return "404";
}
```

如果前端使用的是qs.stringify()传递的数据，那么接收的参数名就必须与前端发送数据的key值一样

> qs依赖，`npm install qs` 该依赖是可以在给后台传递数据时，以路径传递参数的格式传递数据
>
> **qs**.stringify({username:'heroC', password:'1130'});
>
> 传递数据格式是：.../...?username=heroC&password=1130
>
> **JSON**.stringify({username:'heroC', password:'1130'});
>
> 传递数据格式是：{"username":"heroC","password":"1130"}