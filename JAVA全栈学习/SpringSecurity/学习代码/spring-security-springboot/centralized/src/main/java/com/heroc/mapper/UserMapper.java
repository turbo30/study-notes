package com.heroc.mapper;

import com.heroc.entity.SysUser;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author: heroC
 * @time: 2020/9/6 21:25
 */
@Mapper
public interface UserMapper {

    @Select("select * from sys_user where username = #{username}")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "roles", column = "id", javaType = List.class,
            many = @Many(select = "com.heroc.mapper.RoleMapper.findByUid"))
    })
    SysUser findByName(String username);
}
