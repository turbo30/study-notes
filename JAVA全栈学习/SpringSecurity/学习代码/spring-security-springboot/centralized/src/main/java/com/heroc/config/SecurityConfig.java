package com.heroc.config;

import com.heroc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author: heroC
 * @time: 2020/9/6 20:40
 */
@Configuration
// 开启security
@EnableWebSecurity
// 开启权限注解
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /**
     * 认证用户
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 在配置中，权限ROLE_USER，不要加前缀，直接USER
        // 使用内存认证
/*        auth.inMemoryAuthentication()
                .withUser("user")
                .password("{noop}123")
                .roles("USER");*/

        /**
         * 通过数据库查询数据，进行认证
         * 参数是 UserDetailsService
         * 因为userService实现了UserDetailsService
         */
        auth.userDetailsService(userService).passwordEncoder(bCryptPasswordEncoder());
    }

    /**
     * 配置springSecurity相关信息
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 释放静态资源，指定拦截规则，指定自定义页面等
        http.authorizeRequests()
                // 释放静态资源
                .antMatchers("/login.jsp","/failer.jsp","/css/**","/img/**","/plugins/**")
                .permitAll()
                // /**下的所有资源需要指定角色访问
                .antMatchers("/**")
                .hasAnyRole("USER","ADMIN")
                // 其他请求需要认证
                .anyRequest()
                .authenticated()
                // 添加其他规则用and()方法连接 登录页面
                .and()
                .formLogin()
                .loginPage("/login.jsp")
                .loginProcessingUrl("/login")
                .successForwardUrl("/index.jsp")
                .failureForwardUrl("/failer.jsp")
                .permitAll()
                // 退出登录页面
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login.jsp")
                .invalidateHttpSession(true)
                .permitAll()
                // 不使用csrf防攻击机制
                .and()
                .csrf()
                .disable();
    }
}
