package com.heroc.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author: heroC
 * @time: 2020/9/6 20:25
 */
@Controller
@RequestMapping("/product")
public class ProductController {

    @Secured({"ROLE_PRODUCT","ROLE_ADMIN"})
    @RequestMapping("/findAll")
    public String findAll(){
        return "product-list";
    }
}
