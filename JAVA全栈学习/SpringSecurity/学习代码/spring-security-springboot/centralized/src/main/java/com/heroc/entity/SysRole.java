package com.heroc.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

/**
 * GrantedAuthority是security的权限管理的接口，实现了该类，
 * 则会直接获得权限名称。将权限名称返回出去
 * @author: heroC
 * @time: 2020/9/6 21:18
 */
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class  SysRole implements GrantedAuthority {

    private Integer id;
    private String roleName;
    private String roleDesc;

    @Override
    public String getAuthority() {
        return roleName;
    }
}
