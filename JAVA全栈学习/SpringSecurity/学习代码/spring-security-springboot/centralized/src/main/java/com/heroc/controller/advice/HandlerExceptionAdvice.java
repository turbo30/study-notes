package com.heroc.controller.advice;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author: heroC
 * @time: 2020/9/6 23:14
 */
@ControllerAdvice
public class HandlerExceptionAdvice {

    @ExceptionHandler(RuntimeException.class)
    public String exception403(Exception e){
        if (e instanceof AccessDeniedException){
            return "redirect:/403.jsp";
        }
        return "redirect:/500.jsp";
    }
}
