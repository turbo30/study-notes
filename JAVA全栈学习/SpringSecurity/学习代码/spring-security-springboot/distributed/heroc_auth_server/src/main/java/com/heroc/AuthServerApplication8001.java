package com.heroc;

import com.heroc.config.RsaKeyProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author: heroC
 * @time: 2020/9/9 9:29
 */
@SpringBootApplication
@MapperScan("com.heroc.mapper")
// 将RsaKeyProperties.class注入到ioc容器中
@EnableConfigurationProperties(RsaKeyProperties.class)
public class AuthServerApplication8001 {
    public static void main(String[] args) {
        SpringApplication.run(AuthServerApplication8001.class,args);
    }
}
