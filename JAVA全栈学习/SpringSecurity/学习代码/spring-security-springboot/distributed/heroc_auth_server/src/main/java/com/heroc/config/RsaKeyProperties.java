package com.heroc.config;

import com.heroc.utils.RsaUtils;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * 读取配置文件
 * @author: heroC
 * @time: 2020/9/9 9:32
 */
@ConfigurationProperties("heroc.key")
@Data
public class RsaKeyProperties {

    private String pubKeyPath;
    private String priKeyPath;

    private PublicKey pubKey;
    private PrivateKey priKey;

    /**
     * 在构造器完成之后
     * 获取公钥和私钥
     */
    @PostConstruct
    public void createRsaKey() throws Exception {
        pubKey = RsaUtils.getPublicKey(pubKeyPath);
        priKey = RsaUtils.getPrivateKey(priKeyPath);
    }


}
