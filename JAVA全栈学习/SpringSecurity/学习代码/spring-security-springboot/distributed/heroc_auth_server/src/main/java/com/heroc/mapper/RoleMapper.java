package com.heroc.mapper;

import com.heroc.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author: heroC
 * @time: 2020/9/9 9:43
 */
@Mapper
public interface RoleMapper {
    @Select("select r.id, r.role_name roleName, r.role_desc roleDesc " +
            "from sys_role r, sys_user_role ur " +
            "where r.id = ur.rid and ur.uid = #{uid}")
    List<SysRole> findByUid(Integer uid);
}
