package com.heroc.filter;

import com.heroc.config.RsaKeyProperties;
import com.heroc.domain.Payload;
import com.heroc.entity.SysUser;
import com.heroc.utils.JsonUtils;
import com.heroc.utils.JwtUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * 检验前端传递过来的token信息
 * @author: heroC
 * @time: 2020/9/9 10:52
 */
public class TokenVerifyFilter extends BasicAuthenticationFilter {

    private RsaKeyProperties prop;

    public TokenVerifyFilter(AuthenticationManager authenticationManager, RsaKeyProperties prop) {
        super(authenticationManager);
        this.prop = prop;
    }

    /**
     * 请求过滤，验证请求头中的token
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader("Authorization");
        if (header == null && !header.startsWith("Bearer ")) {
            // 未登录，没有token
            chain.doFilter(request, response);
            try{
                response.setContentType("application/json;charset=utf-8");
                PrintWriter out = response.getWriter();
                HashMap<String, String> map = new HashMap<>(2);
                map.put("code",HttpServletResponse.SC_FORBIDDEN+"");
                map.put("msg","请登录！");
                out.println(JsonUtils.toString(map));
                out.flush();
                out.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        } else {
            // 携带了正确格式的token
            String token = header.replace("Bearer ", "");
            // 验证token是否正确，通过公钥去解析私钥，获取载荷中的用户信息
            Payload<SysUser> payload = JwtUtils.getInfoFromToken(token, prop.getPubKey(), SysUser.class);
            SysUser userInfo = payload.getUserInfo();
            // 如果用户信息不为空，那么就将其封装为UsernamePasswordAuthenticationToken进行验证
            if (userInfo!=null){
                UsernamePasswordAuthenticationToken authResult = new UsernamePasswordAuthenticationToken(userInfo.getUsername(), null, userInfo.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authResult);
                chain.doFilter(request, response);
            }
        }
    }
}
