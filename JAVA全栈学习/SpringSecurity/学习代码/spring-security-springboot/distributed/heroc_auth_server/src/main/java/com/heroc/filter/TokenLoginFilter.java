package com.heroc.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.heroc.config.RsaKeyProperties;
import com.heroc.entity.SysRole;
import com.heroc.entity.SysUser;
import com.heroc.utils.JsonUtils;
import com.heroc.utils.JwtUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

/**
 * 登录过滤
 * 1，重写attemptAuthentication，尝试认证操作。获取到前端传递过来的用户名和密码，进一步认证操作
 * 2，重写successfulAuthentication，认证成功之后，将jwt的token设置到头请求中，返回给前端
 * @author: heroC
 * @time: 2020/9/9 10:34
 */
public class TokenLoginFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    private RsaKeyProperties prop;

    public TokenLoginFilter(AuthenticationManager authenticationManager, RsaKeyProperties prop) {
        this.authenticationManager = authenticationManager;
        this.prop = prop;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try{
            SysUser sysUser = new ObjectMapper().readValue(request.getInputStream(), SysUser.class);
            UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(sysUser.getUsername(), sysUser.getPassword());
            return authenticationManager.authenticate(authRequest);
        }catch (Exception e){
            try{
                response.setContentType("application/json;charset=utf-8");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                PrintWriter out = response.getWriter();
                HashMap<String, String> map = new HashMap<>(2);
                map.put("code",HttpServletResponse.SC_UNAUTHORIZED+"");
                map.put("msg","username or password error");
                out.println(JsonUtils.toString(map));
                out.flush();
                out.close();
            }catch (Exception outEx){
                outEx.printStackTrace();
            }
            throw new RuntimeException(e);
        }
    }

    @Override
    public void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        SysUser sysUser = new SysUser();
        sysUser.setUsername(authResult.getName());
        sysUser.setRoles((List<SysRole>) authResult.getAuthorities());
        // jwt，用户信息，通过rsa加密的私钥，过期时间
        String token = JwtUtils.generateTokenExpireInMinutes(sysUser, prop.getPriKey(), 60);
        response.addHeader("Authorization","Bearer "+token);
        try{
            response.setContentType("application/json;charset=utf-8");
            PrintWriter out = response.getWriter();
            HashMap<String, String> map = new HashMap<>(2);
            map.put("code",HttpServletResponse.SC_OK+"");
            map.put("msg","ok");
            out.println(JsonUtils.toString(map));
            out.flush();
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
