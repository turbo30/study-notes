package com.heroc.controller.advice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.heroc.utils.JsonUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

/**
 * @author: heroC
 * @time: 2020/9/6 23:14
 */
@RestControllerAdvice
public class HandlerExceptionAdvice {

    @ExceptionHandler(RuntimeException.class)
    public String exception403(Exception e){
        if (e instanceof AccessDeniedException){
            HashMap<String, String> map = new HashMap<>(2);
            map.put("code", HttpServletResponse.SC_FORBIDDEN+"");
            map.put("msg","无权访问！");
            return JsonUtils.toString(map);
        }
        return "{'code':'500','msg':'服务繁忙！'}";
    }
}
