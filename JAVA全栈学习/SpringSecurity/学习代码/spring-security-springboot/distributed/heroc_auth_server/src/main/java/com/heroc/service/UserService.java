package com.heroc.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author: heroC
 * @time: 2020/9/6 21:34
 */
public interface UserService extends UserDetailsService {
}
