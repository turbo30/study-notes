package com.heroc.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: heroC
 * @time: 2020/9/9 13:35
 */
@RestController
@RequestMapping("/product")
public class ProductController {

    @Secured({"ROLE_PRODUCT","ROLE_ADMIN"})
    @RequestMapping("/findAll")
    public String productInfo(){
        return "商品列表访问成功！";
    }
}
