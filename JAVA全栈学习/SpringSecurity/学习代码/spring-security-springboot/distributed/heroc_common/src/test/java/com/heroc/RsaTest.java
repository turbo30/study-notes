package com.heroc;

import com.heroc.utils.JwtUtils;
import com.heroc.utils.RsaUtils;
import org.junit.Test;

/**
 * @author: heroC
 * @time: 2020/9/9 9:08
 */
public class RsaTest {

    private static String dir;
    private static String privateFilePath;
    private static String publicFilePath;
    static {
        dir = System.getProperty("user.dir");
        privateFilePath = dir+"/src/main/resources/key/id_key_rsa";
        publicFilePath = dir+"/src/main/resources/key/id_key_rsa.pub";
    }

    /**
     * 生产rsa公钥和私钥
     * @throws Exception
     */
    @Test
    public void rsa() throws Exception {
        RsaUtils.generateKey(publicFilePath,privateFilePath,"heroc",2048);
    }

    /**
     * 获得私钥
     * @throws Exception
     */
    @Test
    public void getPrivateRsa() throws Exception {
        System.out.println(RsaUtils.getPrivateKey(privateFilePath));
    }

    /**
     * 获得公钥
     * @throws Exception
     */
    @Test
    public void getPublicRsa() throws Exception {
        System.out.println(RsaUtils.getPublicKey(publicFilePath));
    }
}
