package com.heroc.domain;

import lombok.Data;

import java.util.Date;

/**
 * jwt生成的token有3部分组成：
 * 头部：主要设置一些规范信息，签名部分的编码格式就在头部中声明
 * 载荷：中存放有效信息的部分，比如用户名，用户角色，过期时间等，但是不要放密码，会泄露！
 * 签名：将头部与载荷分别采用base64编码后，用“.”相连，再加入盐，最后使用头部声明的编码类型进行编
 *      码，就得到了签名。用于校验，防止被篡改。
 *
 * 为了方便后期获取token中的用户信息，将token中载荷部分单独封装成一个对象
 * 该类就是存放载荷中的信息
 * @author: heroC
 * @time: 2020/9/7 22:10
 */
@Data
public class Payload<T> {
    /** id */
    private String id;
    /** 指定泛型的用户信息 */
    private T userInfo;
    /** 过期时间 */
    private Date expiration;
}
