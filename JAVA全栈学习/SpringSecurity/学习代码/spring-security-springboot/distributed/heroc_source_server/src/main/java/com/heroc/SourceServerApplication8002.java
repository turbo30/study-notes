package com.heroc;

import com.heroc.config.RsaKeyProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author: heroC
 * @time: 2020/9/9 13:12
 */
@SpringBootApplication
@MapperScan("com.heroc.mapper")
@EnableConfigurationProperties(RsaKeyProperties.class)
public class SourceServerApplication8002 {
    public static void main(String[] args) {
        SpringApplication.run(SourceServerApplication8002.class,args);
    }
}
