package com.heroc.filter;

import com.heroc.config.RsaKeyProperties;
import com.heroc.domain.Payload;
import com.heroc.entity.SysUser;
import com.heroc.utils.JsonUtils;
import com.heroc.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * @author: heroC
 * @time: 2020/9/9 13:23
 */
public class TokenVerifyFilter extends BasicAuthenticationFilter {

    @Autowired
    private RsaKeyProperties porp;

    public TokenVerifyFilter(AuthenticationManager authenticationManager, RsaKeyProperties porp) {
        super(authenticationManager);
        this.porp = porp;
    }

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader("Authorization");
        if (header == null && !header.startsWith("Bearer ")){
            // 未登录，没有token
            chain.doFilter(request, response);
            try{
                response.setContentType("application/json;charset=utf-8");
                PrintWriter out = response.getWriter();
                HashMap<String, String> map = new HashMap<>(2);
                map.put("code",HttpServletResponse.SC_FORBIDDEN+"");
                map.put("msg","请登录！");
                out.println(JsonUtils.toString(map));
                out.flush();
                out.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            String token = header.replace("Bearer ", "");
            Payload<SysUser> payload = JwtUtils.getInfoFromToken(token, porp.getPubKey(), SysUser.class);
            UsernamePasswordAuthenticationToken authResult = new UsernamePasswordAuthenticationToken(payload.getUserInfo().getUsername(), null, payload.getUserInfo().getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authResult);
            chain.doFilter(request,response);
        }
    }
}
