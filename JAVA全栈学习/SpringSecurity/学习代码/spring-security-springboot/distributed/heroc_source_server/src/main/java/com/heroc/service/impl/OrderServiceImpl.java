package com.heroc.service.impl;

import com.heroc.service.OrderService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

/**
 * @author: heroC
 * @time: 2020/9/9 13:42
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Secured({"ROLE_ORDER","ROLE_ADMIN"})
    @Override
    public String orderInfo() {
        return "service 订单 访问成功";
    }
}
