package com.heroc.config;

import com.heroc.utils.RsaUtils;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import java.security.PublicKey;

/**
 * @author: heroC
 * @time: 2020/9/9 13:24
 */
@Data
@ConfigurationProperties("heroc.key")
public class RsaKeyProperties {

    private String pubKeyPath;
    private PublicKey pubKey;

    /**
     * 在构造器完成之后
     * 获取公钥和私钥
     */
    @PostConstruct
    public void createRsaKey() throws Exception {
        pubKey = RsaUtils.getPublicKey(pubKeyPath);
    }

}
