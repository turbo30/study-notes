package com.heroc.service;

/**
 * @author: heroC
 * @time: 2020/9/9 13:41
 */
public interface OrderService {
    public String orderInfo();
}
