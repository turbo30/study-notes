# Spring Security



# ==SSM 使用 Spring Security==

## 一、环境搭建

### 1、数据

```sql
/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 8.0.16 : Database - security_authority
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `sys_permission` */

DROP TABLE IF EXISTS `sys_permission`;

CREATE TABLE `sys_permission` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `permission_NAME` VARCHAR(30) DEFAULT NULL COMMENT '菜单名称',
  `permission_url` VARCHAR(100) DEFAULT NULL COMMENT '菜单地址',
  `parent_id` INT(11) NOT NULL DEFAULT '0' COMMENT '父菜单id',
  PRIMARY KEY (`ID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

/*Data for the table `sys_permission` */

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `ROLE_NAME` VARCHAR(30) DEFAULT NULL COMMENT '角色名称',
  `ROLE_DESC` VARCHAR(60) DEFAULT NULL COMMENT '角色描述',
  PRIMARY KEY (`ID`)
) ENGINE=INNODB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `sys_role` */

/*Table structure for table `sys_role_permission` */

DROP TABLE IF EXISTS `sys_role_permission`;

CREATE TABLE `sys_role_permission` (
  `RID` INT(11) NOT NULL COMMENT '角色编号',
  `PID` INT(11) NOT NULL COMMENT '权限编号',
  PRIMARY KEY (`RID`,`PID`),
  KEY `FK_Reference_12` (`PID`),
  CONSTRAINT `FK_Reference_11` FOREIGN KEY (`RID`) REFERENCES `sys_role` (`ID`),
  CONSTRAINT `FK_Reference_12` FOREIGN KEY (`PID`) REFERENCES `sys_permission` (`ID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

/*Data for the table `sys_role_permission` */

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(32) NOT NULL COMMENT '用户名称',
  `password` VARCHAR(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `status` INT(1) DEFAULT '1' COMMENT '1开启0关闭',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `sys_user` */

/*Table structure for table `sys_user_role` */

DROP TABLE IF EXISTS `sys_user_role`;

CREATE TABLE `sys_user_role` (
  `UID` INT(11) NOT NULL COMMENT '用户编号',
  `RID` INT(11) NOT NULL COMMENT '角色编号',
  PRIMARY KEY (`UID`,`RID`),
  KEY `FK_Reference_10` (`RID`),
  CONSTRAINT `FK_Reference_10` FOREIGN KEY (`RID`) REFERENCES `sys_role` (`ID`),
  CONSTRAINT `FK_Reference_9` FOREIGN KEY (`UID`) REFERENCES `sys_user` (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

/*Data for the table `sys_user_role` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

```



### 2、依赖

```xml
<dependency>
    <groupId>org.springframework.security</groupId>
    <artifactId>spring-security-config</artifactId>
    <version>5.1.5.RELEASE</version>
</dependency>
<dependency>
    <groupId>org.springframework.security</groupId>
    <artifactId>spring-security-taglibs</artifactId>
    <version>5.1.5.RELEASE</version>
</dependency>
```



### 3、WEB.xml 配置

web.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">

    <servlet>
        <servlet-name>dispatcher</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:spring-mvc.xml</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>dispatcher</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>

    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>classpath:applicationContext.xml</param-value>
    </context-param>

    <filter>
        <filter-name>encodingFilter</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>UTF-8</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>encodingFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

    <!--spring security 过滤器链 名字必须叫springSecurityFilterChain-->
    <filter>
        <filter-name>springSecurityFilterChain</filter-name>
        <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>springSecurityFilterChain</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

</web-app>
```



## 二、常用过滤器链



### 1、常用过滤器介绍



- **org.springframework.security.web.context.SecurityContextPersistenceFilter**
  首当其冲的一个过滤器，作用之重要，自不必多言。
  SecurityContextPersistenceFilter主要是使用SecurityContextRepository在session中保存或更新一个
  SecurityContext，并将SecurityContext给以后的过滤器使用，来为后续filter建立所需的上下文。
  SecurityContext中存储了当前用户的认证以及权限信息。



- **org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter**
  此过滤器用于集成SecurityContext到Spring异步执行机制中的WebAsyncManager



- **org.springframework.security.web.header.HeaderWriterFilter**
  向请求的Header中添加相应的信息,可在http标签内部使用security:headers来控制



- **org.springframework.security.web.csrf.CsrfFilter**
  csrf又称跨域请求伪造，SpringSecurity会对所有post请求验证是否包含系统生成的csrf的token信息，
  如果不包含，则报错。起到防止csrf攻击的效果。



- **org.springframework.security.web.authentication.logout.LogoutFilter**

  匹配URL为/logout的请求，实现用户退出,清除认证信息。

  

- **org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter**
  认证操作全靠这个过滤器，默认匹配URL为/login且必须为POST请求。



- **org.springframework.security.web.authentication.ui.DefaultLoginPageGeneratingFilter**
  如果没有在配置文件中指定认证页面，则由该过滤器生成一个默认认证页面。



- **org.springframework.security.web.authentication.ui.DefaultLogoutPageGeneratingFilter**
  由此过滤器可以生产一个默认的退出登录页面



- **org.springframework.security.web.authentication.www.BasicAuthenticationFilter**
  此过滤器会自动解析HTTP请求中头部名字为Authentication，且以Basic开头的头信息。



- **org.springframework.security.web.savedrequest.RequestCacheAwareFilter**
  通过HttpSessionRequestCache内部维护了一个RequestCache，用于缓存HttpServletRequest



- **org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter**
  针对ServletRequest进行了一次包装，使得request具有更加丰富的API



- **org.springframework.security.web.authentication.AnonymousAuthenticationFilter**
  当SecurityContextHolder中认证信息为空,则会创建一个匿名用户存入到SecurityContextHolder中。
  spring security为了兼容未登录的访问，也走了一套认证流程，只不过是一个匿名的身份。



- **org.springframework.security.web.session.SessionManagementFilter**
  SecurityContextRepository限制同一用户开启多个会话的数量



- **org.springframework.security.web.access.ExceptionTranslationFilter**
  异常转换过滤器位于整个springSecurityFilterChain的后方，用来转换整个链路中出现的异常



- **org.springframework.security.web.access.intercept.FilterSecurityInterceptor**
  获取所配置资源访问的授权信息，根据SecurityContextHolder中存储的用户信息来决定其是否有权
  限。



> TIP
>
> 远不止这些过滤器



### 2、过滤器加载原理



#### 1）DelegatingFilterProxy

Delegating 授权	【授权代理过滤器】

我们在web.xml中配置了一个名称为springSecurityFilterChain的过滤器DelegatingFilterProxy，接下我直接对
DelegatingFilterProxy源码里重要代码进行说明，其中删减掉了一些不重要的代码，大家注意我写的注释就行了！

```java
public class DelegatingFilterProxy extends GenericFilterBean {
    @Nullable
    private String contextAttribute;
    @Nullable
    private WebApplicationContext webApplicationContext;
    @Nullable
    private String targetBeanName;
    private boolean targetFilterLifecycle;
    @Nullable
    private volatile Filter delegate;//注：这个过滤器才是真正加载的过滤器
    private final Object delegateMonitor;

    //注：doFilter才是过滤器的入口，直接从这看！
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain
            filterChain) throws ServletException, IOException {
        Filter delegateToUse = this.delegate;
        if (delegateToUse == null) {
            synchronized (this.delegateMonitor) {
                delegateToUse = this.delegate;
                if (delegateToUse == null) {
                    WebApplicationContext wac = this.findWebApplicationContext();
                    if (wac == null) {
                        throw new IllegalStateException("No WebApplicationContext found: no
                                ContextLoaderListener or DispatcherServlet registered ? ");
                    }
                    //第一步：doFilter中最重要的一步，初始化上面私有过滤器属性delegate
                    delegateToUse = this.initDelegate(wac);
                }
                this.delegate = delegateToUse;
            }
        }
        //第三步：执行FilterChainProxy过滤器
        this.invokeDelegate(delegateToUse, request, response, filterChain);
    }

    //第二步：直接看最终加载的过滤器到底是谁
    protected Filter initDelegate(WebApplicationContext wac) throws ServletException {
        //debug得知targetBeanName为：springSecurityFilterChain
        String targetBeanName = this.getTargetBeanName();
        Assert.state(targetBeanName != null, "No target bean name set");
        //debug得知delegate对象为：FilterChainProxy
        Filter delegate = (Filter) wac.getBean(targetBeanName, Filter.class);
        if (this.isTargetFilterLifecycle()) {
            delegate.init(this.getFilterConfig());
        }
        return delegate;
    }

    protected void invokeDelegate(Filter delegate, ServletRequest request, ServletResponse
            response, FilterChain filterChain) throws ServletException, IOException {
        delegate.doFilter(request, response, filterChain);
    }
}
```

第二步debug结果如下：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringSecurity/过滤器01.png)

由此可知，DelegatingFilterProxy通过springSecurityFilterChain这个名称，得到了一个FilterChainProxy过滤器，
最终在第三步执行了这个过滤器。



#### 2）FilterChainProxy

【过滤链代理】

```java
public class FilterChainProxy extends GenericFilterBean {
    private static final Log logger = LogFactory.getLog(FilterChainProxy.class);
    private static final String FILTER_APPLIED =
            FilterChainProxy.class.getName().concat(".APPLIED");
    private List<SecurityFilterChain> filterChains;
    private FilterChainProxy.FilterChainValidator filterChainValidator;
    private HttpFirewall firewall;

    //咿！？可以通过一个叫SecurityFilterChain的对象实例化出一个FilterChainProxy对象
    //这FilterChainProxy又是何方神圣？会不会是真正的过滤器链对象呢？先留着这个疑问！
    public FilterChainProxy(SecurityFilterChain chain) {
        this(Arrays.asList(chain));
    }

    //又是SecurityFilterChain这家伙！嫌疑更大了！
    public FilterChainProxy(List<SecurityFilterChain> filterChains) {
        this.filterChainValidator = new FilterChainProxy.NullFilterChainValidator();
        this.firewall = new StrictHttpFirewall();
        this.filterChains = filterChains;
    }

    //注：直接从doFilter看
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        boolean clearContext = request.getAttribute(FILTER_APPLIED) == null;
        if (clearContext) {
            try {
                request.setAttribute(FILTER_APPLIED, Boolean.TRUE);
                this.doFilterInternal(request, response, chain);
            } finally {
                SecurityContextHolder.clearContext();
                request.removeAttribute(FILTER_APPLIED);
            }
        } else {
            //第一步：具体操作调用下面的doFilterInternal方法了
            this.doFilterInternal(request, response, chain);
        }
    }

    private void doFilterInternal(ServletRequest request, ServletResponse response, FilterChain
            chain) throws IOException, ServletException {
        FirewalledRequest fwRequest =
                this.firewall.getFirewalledRequest((HttpServletRequest) request);
        HttpServletResponse fwResponse =
                this.firewall.getFirewalledResponse((HttpServletResponse) response);
        //第二步：封装要执行的过滤器链，那么多过滤器就在这里被封装进去了！
        List<Filter> filters = this.getFilters((HttpServletRequest) fwRequest);
        if (filters != null && filters.size() != 0) {
            FilterChainProxy.VirtualFilterChain vfc = new
                    FilterChainProxy.VirtualFilterChain(fwRequest, chain, filters);
            //第四步：加载过滤器链
            vfc.doFilter(fwRequest, fwResponse);
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug(UrlUtils.buildRequestUrl(fwRequest) + (filters == null ? " has no
                        matching filters" : " has an empty filter list"));
            }
            fwRequest.reset();
            chain.doFilter(fwRequest, fwResponse);
        }
    }

    private List<Filter> getFilters(HttpServletRequest request) {
        Iterator var2 = this.filterChains.iterator();
        //第三步：封装过滤器链到SecurityFilterChain中！
        SecurityFilterChain chain;
        do {
            if (!var2.hasNext()) {
                return null;
            }
            chain = (SecurityFilterChain) var2.next();
        } while (!chain.matches(request));
        return chain.getFilters();
    }
}
```

第二步debug结果如下图所示，惊不惊喜？十五个过滤器都在这里了！

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringSecurity/过滤器02.png)

再看第三步，怀疑这么久！原来这些过滤器还真是都被封装进SecurityFilterChain中了。



#### 3）SecurityFilterChain

【安全过滤链】

最后看SecurityFilterChain，这是个接口，实现类也只有一个，这才是web.xml中配置的过滤器链对象！

```java
//接口
public interface SecurityFilterChain {
    boolean matches(HttpServletRequest var1);

    List<Filter> getFilters();
}

//实现类
public final class DefaultSecurityFilterChain implements SecurityFilterChain {
    private static final Log logger = LogFactory.getLog(DefaultSecurityFilterChain.class);
    private final RequestMatcher requestMatcher;
    private final List<Filter> filters;

    public DefaultSecurityFilterChain(RequestMatcher requestMatcher, Filter... filters) {
        this(requestMatcher, Arrays.asList(filters));
    }

    public DefaultSecurityFilterChain(RequestMatcher requestMatcher, List<Filter> filters) {
        logger.info("Creating filter chain: " + requestMatcher + ", " + filters);
        this.requestMatcher = requestMatcher;
        this.filters = new ArrayList(filters);
    }

    public RequestMatcher getRequestMatcher() {
        return this.requestMatcher;
    }

    public List<Filter> getFilters() {
        return this.filters;
    }

    public boolean matches(HttpServletRequest request) {
        return this.requestMatcher.matches(request);
    }

    public String toString() {
        return "[ " + this.requestMatcher + ", " + this.filters + "]";
    }
}
```

总结：通过此章节，我们对SpringSecurity工作原理有了一定的认识。但理论千万条，功能第一条，探寻底层，是
为了更好的使用框架。
那么，言归正传！到底如何使用自己的页面来实现SpringSecurity的认证操作呢？要完成此功能，首先要有一套
自己的页面！



## 三、自定义认证页面

### 1、配置：权限、角色、登录退出

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:context="http://www.springframework.org/schema/context"
        xmlns:aop="http://www.springframework.org/schema/aop"
        xmlns:tx="http://www.springframework.org/schema/tx"
        xmlns:mvc="http://www.springframework.org/schema/mvc"
        xmlns:security="http://www.springframework.org/schema/security"
        xsi:schemaLocation="http://www.springframework.org/schema/beans
			    http://www.springframework.org/schema/beans/spring-beans.xsd
			    http://www.springframework.org/schema/context
			    http://www.springframework.org/schema/context/spring-context.xsd
			    http://www.springframework.org/schema/aop
			    http://www.springframework.org/schema/aop/spring-aop.xsd
			    http://www.springframework.org/schema/tx
			    http://www.springframework.org/schema/tx/spring-tx.xsd
			    http://www.springframework.org/schema/mvc
			    http://www.springframework.org/schema/mvc/spring-mvc.xsd
                http://www.springframework.org/schema/security
			    http://www.springframework.org/schema/security/spring-security.xsd">

    <!--释放静态资源-->
    <security:http pattern="/css/**" security="none"/>
    <security:http pattern="/img/**" security="none"/>
    <security:http pattern="/plugins/**" security="none"/>
    <security:http pattern="/failer.jsp" security="none"/>
    <!--配置springSecurity-->
    <!--
    auto-config="true"  表示自动加载springsecurity的配置文件
    use-expressions="true" 表示使用spring的el表达式来配置springsecurity
    -->
    <security:http auto-config="true" use-expressions="true">
        <!--让认证页面可以匿名访问-->
        <security:intercept-url pattern="/login.jsp" access="permitAll()"/>
        <!--拦截资源-->
        <!--
        pattern="/**" 表示拦截所有资源
        access="hasAnyRole('ROLE_USER')" 表示只有ROLE_USER角色才能访问资源
        -->
        <security:intercept-url pattern="/**" access="hasAnyRole('ROLE_USER')"/>
        <!--配置认证信息-->
        <security:form-login login-page="/login.jsp"
                             login-processing-url="/login"
                             default-target-url="/index.jsp"
                             authentication-failure-url="/failer.jsp"/>
        <!--配置退出登录信息-->
        <security:logout logout-url="/logout"
                         logout-success-url="/login.jsp"/>
        <!--去掉csrf拦截的过滤器-->
        <!--<security:csrf disabled="true"/>-->
    </security:http>

    <!--把加密对象放入的IOC容器中-->
    <bean id="passwordEncoder" class="org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder"/>

    <!--设置Spring Security认证用户信息的来源-->
    <!--
    springsecurity默认的认证必须是加密的，加上{noop}表示不加密认证。
    -->
    <security:authentication-manager>
        <security:authentication-provider user-service-ref="userServiceImpl">
            <security:password-encoder ref="passwordEncoder"/>
        </security:authentication-provider>
    </security:authentication-manager>

</beans>
```

注意：一旦开启了csrf防护功能，logout处理器便只支持POST请求方式了！



### 2、CSRF防护机制

CSRF（Cross-site request forgery）跨站请求伪造，是一种难以防范的网络攻击方式。



#### 1）CsrfFilter 过滤器

```java
public final class CsrfFilter extends OncePerRequestFilter {
    public static final RequestMatcher DEFAULT_CSRF_MATCHER = new
            CsrfFilter.DefaultRequiresCsrfMatcher();
    private final Log logger = LogFactory.getLog(this.getClass());
    private final CsrfTokenRepository tokenRepository;
    private RequestMatcher requireCsrfProtectionMatcher;
    private AccessDeniedHandler accessDeniedHandler;

    public CsrfFilter(CsrfTokenRepository csrfTokenRepository) {
        this.requireCsrfProtectionMatcher = DEFAULT_CSRF_MATCHER;
        this.accessDeniedHandler = new AccessDeniedHandlerImpl();
        Assert.notNull(csrfTokenRepository, "csrfTokenRepository cannot be null");
        this.tokenRepository = csrfTokenRepository;
    }

    //通过这里可以看出SpringSecurity的csrf机制把请求方式分成两类来处理
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        request.setAttribute(HttpServletResponse.class.getName(), response);
        CsrfToken csrfToken = this.tokenRepository.loadToken(request);
        boolean missingToken = csrfToken == null;
        if (missingToken) {
            csrfToken = this.tokenRepository.generateToken(request);
            this.tokenRepository.saveToken(csrfToken, request, response);
        }
        request.setAttribute(CsrfToken.class.getName(), csrfToken);
        request.setAttribute(csrfToken.getParameterName(), csrfToken);
        //第一类："GET", "HEAD", "TRACE", "OPTIONS"四类请求可以直接通过
        if (!this.requireCsrfProtectionMatcher.matches(request)) {
            filterChain.doFilter(request, response);
        } else {
            //第二类：除去上面四类，包括POST都要被验证携带token才能通过
            String actualToken = request.getHeader(csrfToken.getHeaderName());
            if (actualToken == null) {
                actualToken = request.getParameter(csrfToken.getParameterName());
            }
            if (!csrfToken.getToken().equals(actualToken)) {
                if (this.logger.isDebugEnabled()) {
                    this.logger.debug("Invalid CSRF token found for " +
                            UrlUtils.buildFullRequestUrl(request));
                }
                if (missingToken) {
                    this.accessDeniedHandler.handle(request, response, new
                            MissingCsrfTokenException(actualToken));
                } else {
                    this.accessDeniedHandler.handle(request, response, new
                            InvalidCsrfTokenException(csrfToken, actualToken));
                }
            } else {
                filterChain.doFilter(request, response);
            }
        }
    }

    public void setRequireCsrfProtectionMatcher(RequestMatcher requireCsrfProtectionMatcher) {
        Assert.notNull(requireCsrfProtectionMatcher, "requireCsrfProtectionMatcher cannot be
        null ");
        this.requireCsrfProtectionMatcher = requireCsrfProtectionMatcher;
    }

    public void setAccessDeniedHandler(AccessDeniedHandler accessDeniedHandler) {
        Assert.notNull(accessDeniedHandler, "accessDeniedHandler cannot be null");
        this.accessDeniedHandler = accessDeniedHandler;
    }

    private static final class DefaultRequiresCsrfMatcher implements RequestMatcher {
        private final HashSet<String> allowedMethods;

        private DefaultRequiresCsrfMatcher() {
            this.allowedMethods = new HashSet(Arrays.asList("GET", "HEAD", "TRACE", "OPTIONS"));
        }
    }

    public boolean matches(HttpServletRequest request) {
        return !this.allowedMethods.contains(request.getMethod());
    }
}
```

通过源码分析，我们明白了，自己的认证页面，请求方式为POST，但却没有携带token，所以才出现了403权限不
足的异常。那么如何处理这个问题呢？
方式一：直接禁用csrf，不推荐。
方式二：在认证页面携带token请求。



#### 2）认证页面携带token 解决Csrf拦截

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringSecurity/认证页面携带token.png)

注：HttpSessionCsrfTokenRepository对象负责生成token并放入session域中。



## 四、Spring Security 实现数据库认证

账户表、角色表、账户角色关系表



### 1、认证功能（连接数据库）

有一个`UserDetailsService`接口，该接口是Spring Security操作用户信息的接口，我们项目都有一个用户实体类，单实Spring Securtiy不知道用户的用户实体类是什么，所以自己就确定了一个规范`UserDetails`是登录用户实体类，`UserDetailsService`是逻辑实现接口。我们要自定义自己认证功能，就需要将自己的用户接口继承它。



```java

// 继承UserDetailsService
public interface UserService extends UserDetailsService {

    public void save(SysUser user);

    public List<SysUser> findAll();

    public Map<String, Object> toAddRolePage(Integer id);

    public void addRoleToUser(Integer userId, Integer[] ids);
}

```

实现该接口的类，实现UserDetilsService中的方法：

```java
@Autowired
private BCryptPasswordEncoder bCryptPasswordEncoder;

/**
 * 添加新用户
 */
@Override
public void save(SysUser user) {
    // 使用 spring security 加密
    user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    userDao.save(user);
}

...

/**
 * 认证业务
 * @param username 用户在浏览器输入的登录名
 * @return UserDetails是Spring Security自己的用户对象
 * @throws UsernameNotFoundException
 */
@Override
public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    // 1、根据用户名做查询
    SysUser sysUser = userDao.findByName(username);
    if (sysUser == null){
        return null;
    }
    // 获取该用户的角色信息
    List<SimpleGrantedAuthority> authorities = new ArrayList<>();
    List<SysRole> roles = sysUser.getRoles();
    for (SysRole role : roles) {
        authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
    }
    // 密码前面加了 {noop} 字符串表示该密码是以原文匹配，如果设置了加密，该失效
    // 将userDetails示例返回，Spring Security 会判断该用户的密码与该用户输入的密码是否一致，
    // 一致则根据角色信息释放对应的权限
    UserDetails userDetails = new User(sysUser.getUsername(),sysUser.getPassword(),authorities);
    return userDetails;
}
```

在 spring-security.xml中 加密配置：

```xml
<!--把加密对象放入的IOC容器中-->
<bean id="passwordEncoder" class="org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder"/>

<!--设置Spring Security认证用户信息的来源-->
<security:authentication-manager>
    <security:authentication-provider user-service-ref="userServiceImpl">
		<security:password-encoder ref="passwordEncoder"/>
    </security:authentication-provider>
</security:authentication-manager>
```



### 2、用户状态

enabled： true表示可用，false表示不可用
accountNonExpired: true表示账户不失效，false表示账户失效
credentialsNonExpired：true表示密码不失效，false表示密码失效
accountNonLocked：true表示账户不锁定，false表示账户锁定

```java
// Use对象的构造器，
public User(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
    if (username != null && !"".equals(username) && password != null) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.accountNonExpired = accountNonExpired;
        this.credentialsNonExpired = credentialsNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.authorities = Collections.unmodifiableSet(sortAuthorities(authorities));
    } else {
        throw new IllegalArgumentException("Cannot pass null or empty values to constructor");
    }
}
```

==这四个参数必须同时为true认证才可以登录成功==，为了节省时间，我只用第一个布尔值做个测试，修改认证业务代码：

```java
@Override
public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    SysUser sysUser = userDao.findByName(username);
    if (sysUser == null){
        return null;
    }

    List<SimpleGrantedAuthority> authorities = new ArrayList<>();
    List<SysRole> roles = sysUser.getRoles();
    for (SysRole role : roles) {
        authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
    }
    // 密码加了 {noop} 表示改密码是原文
    // 将userDetails示例返回，Spring Security 会判断该用户的密码与该用户输入的密码是否一致，
    // 一致则根据角色信息释放对应的权限
    /**
     * enabled： true表示可用，false表示不可用
     * accountNonExpired: true表示账户不失效，false表示账户失效
     * credentialsNonExpired：true表示密码不失效，false表示密码失效
     * accountNonLocked：true表示账户不锁定，false表示账户锁定
     */
    UserDetails userDetails = new User(sysUser.getUsername(),
                                       sysUser.getPassword(),
                                       sysUser.getStatus()==1,
                                       true,
                                       true,
                                       true,
                                       authorities);
    return userDetails;
}
```



## 五、Remember me

### 1、记住我功能 源码分析

进入`UsernamePasswordAuthenticationFilter`的父类`AbstractAuthenticationProcessingFilter`的方法`successfulAuthentication`

跟踪找到AbstractRememberMeServices对象的loginSuccess方法：

```java
public abstract class AbstractRememberMeServices implements RememberMeServices,
InitializingBean, LogoutHandler {
	public final void loginSuccess(HttpServletRequest request, HttpServletResponse response,
Authentication successfulAuthentication) {
		// 判断是否勾选记住我
		// 注意：这里this.parameter点进去是上面的private String parameter = "remember-me";
		if (!this.rememberMeRequested(request, this.parameter)) {
			this.logger.debug("Remember-me login not requested.");
		} else {
			//若勾选就调用onLoginSuccess方法
			this.onLoginSuccess(request, response, successfulAuthentication);
		}
	}
}
```

再点进去上面if判断中的rememberMeRequested方法，还在当前类中：

```java
protected boolean rememberMeRequested(HttpServletRequest request, String parameter) {
    if (this.alwaysRemember) {
        return true;
    } else {
    // 从上面的字parameter的值为"remember-me"
    // 也就是说，此功能提交的属性名必须为"remember-me"
    String paramValue = request.getParameter(parameter);
    // 这里我们看到属性值可以为：true，on，yes，1。
    if (paramValue != null && (paramValue.equalsIgnoreCase("true") ||
    paramValue.equalsIgnoreCase("on") || paramValue.equalsIgnoreCase("yes") ||
    paramValue.equals("1"))) {
        //满足上面条件才能返回true
        return true;
        } else {
            if (this.logger.isDebugEnabled()) {
                this.logger.debug("Did not send remember-me cookie (principal did not set
                parameter '" + parameter + "')");
            }
            return false;
        }
    }
}
```

如果上面方法返回true，就表示页面勾选了记住我选项了。
继续顺着调用的方法找到PersistentTokenBasedRememberMeServices的onLoginSuccess方法：

```java
public class PersistentTokenBasedRememberMeServices extends AbstractRememberMeServices {
protected void onLoginSuccess(HttpServletRequest request, HttpServletResponse response,
Authentication successfulAuthentication) {
    // 获取用户名
    String username = successfulAuthentication.getName();
    this.logger.debug("Creating new persistent login for user " + username);
    //创建记住我的token
    PersistentRememberMeToken persistentToken = new PersistentRememberMeToken(username,
    this.generateSeriesData(), this.generateTokenData(), new Date());
    try {
        //将token持久化到数据库
        this.tokenRepository.createNewToken(persistentToken);
        //将token写入到浏览器的Cookie中
        this.addCookie(persistentToken, request, response);
    } catch (Exception var7) {
    	this.logger.error("Failed to save persistent token ", var7);
    }
}
```

> 根据源码分析，记住我功能参数名必须为`remember-me` 值必须为`true` 、`on`、`yes`、`1`这四之一。且会生成记住我的token，存放在cookie中。请求带上这个token



### 2、开启 Remember me过滤器

```xml
<!--设置可以用spring的el表达式配置Spring Security并自动生成对应配置组件（过滤器）-->
<security:http auto-config="true" use-expressions="true">
    <!--省略其余配置-->
    <!--开启remember me过滤器，设置token存储时间为60秒-->
    <security:remember-me token-validity-seconds="60"/>
</security:http>
```

说明：RememberMeAuthenticationFilter中功能非常简单，会在打开浏览器时，自动判断是否认证，如果没有则
调用autoLogin进行自动认证。



### 3、安全分析

记住我功能方便是大家看得见的，但是安全性却令人担忧。因为Cookie毕竟是保存在客户端的，很容易盗取，而且
cookie的值还与用户名、密码这些敏感数据相关，虽然加密了，但是将敏感信息存在客户端，还是不太安全。那么
这就要提醒喜欢使用此功能的，用完网站要及时手动退出登录，清空认证信息。
此外，SpringSecurity还提供了remember me的另一种相对更安全的实现机制 :在客户端的cookie中，仅保存一个
无意义的加密串（与用户名、密码等敏感数据无关），然后在db中保存该加密串-用户信息的对应关系，自动登录
时，用cookie中的加密串，到db中验证，如果通过，自动登录才算通过。



### 4、持久化Remember me信息

创建一张表，注意这张表的名称和字段都是固定的，不要修改。

```sql
CREATE TABLE `persistent_logins` (
    `username` varchar(64) NOT NULL,
    `series` varchar(64) NOT NULL,
    `token` varchar(64) NOT NULL,
    `last_used` timestamp NOT NULL,
    PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
```

然后将spring-security.xml中 改为：

```xml
<!--
    开启remember me过滤器，
    data-source-ref="dataSource" 指定数据库连接池
    token-validity-seconds="60" 设置token存储时间为60秒 可省略
    remember-me-parameter="remember-me" 指定记住的参数名(也可以自定义，与页面保持一致即可) 可省略
-->
<security:remember-me data-source-ref="dataSource"
                      token-validity-seconds="60"
                	  remember-me-parameter="remember-me"/>
```



## 六、显示当前用户名

```jsp
<span class="hidden-xs">
<security:authentication property="principal.username" />
</span>
或者
<span class="hidden-xs">
<security:authentication property="name" />
</span>
```



## 七、权限

### 1、页面根据权限展示对应的内容

`security:authorize`

```jsp
<%-- hasAnyRole 拥有角色才能展示该菜单 --%>
<security:authorize access="hasAnyRole('ROLE_PRODUCT','ROLE_ADMIN')">
    <li id="system-setting"><a
         href="${pageContext.request.contextPath}/product/findAll">
        <i class="fa fa-circle-o"></i> 产品管理
        </a></li>
</security:authorize>
<security:authorize access="hasAnyRole('ROLE_ORDER','ROLE_ADMIN')">
    <li id="system-setting"><a
        href="${pageContext.request.contextPath}/order/findAll">
        <i class="fa fa-circle-o"></i> 订单管理
        </a></li>
</security:authorize>
```



### 2、IOC容器结构

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringSecurity/ioc结构.png)

父容器是不能被http请求访问的类(service层、dao层)，子容器是能被http访问(controller层)



### 3、开启权限注解 一定要配套

> 本案例由于将注解放在controller层中使用，即在子容器中，以下配置文件就必须放在子容器的配置文件中，即spring-mvc.xml中，才能被在子容器上添加的注解访问
>
> 如果注解在service层中使用，那么以下配置文件需要放在父容器的配置文件中，即applicationContext.xml中，才能被父容器的service层访问

```xml
<!--
        开启权限控制注解支持
        jsr250-annotations="enabled"        开启java250注解
        secured-annotations="enabled"       开启springSecurity内部权限注解开关
        pre-post-annotations="enabled"      开启spring权限注解开关
-->
<security:global-method-security
          jsr250-annotations="enabled"
          secured-annotations="enabled"
          pre-post-annotations="enabled"/>
```



> @Secured 根据安全性考虑，该注解一般用在service层或dao层
>
> 这里测试需要，在controller层使用

```java
@Controller
@RequestMapping("/order")
public class OrderController {

    // @Secured 根据安全性考虑，该注解一般用在service层或dao层
    // 只有角色为ROLE_ORDER和ROLE_ADMIN才能被访问
    @Secured({"ROLE_ORDER","ROLE_USER"})
    @RequestMapping("/findAll")
    public String findAll(){
        return "order-list";
    }
}
```



`本实例，使用在子容器controller层的注解，因此子容器配置文件放在spring-mvc.xml中`

**如果在service或者dao层使用该注解，配置文件就放在父容器中即applicationContext.xml或者引用在父容器的spring-security.xml中**

```java
@Secured({"ROLE_ORDER","ROLE_ADMIN"}) // springSecurity内部制定的注解
@RolesAllowed({"ROLE_ORDER","ROLE_ADMIN"}) // jsr520注解
@PreAuthorize("hasAnyAuthority('ROLE_ORDER','ROLE_ADMIN')") // spring注解使用el表达式
```



## 八、异常处理 跳转页面

### 1、方式一：实现 HandlerExceptionResolver 接口

```java
package com.heroc.controller.advice;


import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author: heroC
 * @time: 2020/9/6 12:52
 */
@Component
public class HandlerControllerException implements HandlerExceptionResolver {

    /**
     * 出现异常跳转到对应的页面
     * @param httpServletRequest
     * @param httpServletResponse
     * @param o 出现异常的对象
     * @param e 出现异常的信息
     * @return
     */
    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("errorMsg",e.getMessage());

        if (e instanceof AccessDeniedException){
            mv.setViewName("redirect:/403.jsp");
        }else {
            mv.setViewName("redirect:/500.jsp");
        }

        return mv;
    }
}

```



### 2、方式二：注解方式

```java
@ControllerAdvice
public class HandlerControllerAdvice {

    @ExceptionHandler(AccessDeniedException.class)
    public String handlerException403(){
        return "redirect:/403.jsp";
    }

    @ExceptionHandler(RuntimeException.class)
    public String handlerRuntimeException(){
        return "redirect:/500.jsp";
    }

}
```



# ==SpringBoot 使用 Spring Security==



## 一、SpringBoot 集中式整合

项目：spring-security-springboot 之 centralized

运行：spring-boot:run



### 1、技术选型

SpringBoot2.1.3，SpringSecurity，MySQL，mybatis，jsp

> springboot不推荐使用jsp，由于为了这里使用方便而使用jsp



### 2、环境准备

由于使用jsp，所以，需要建立一个`webapp` 然后在pom.xml中设置打包方式

```xml
    <packaging>war</packaging>
```

然后`webapp`的文件夹上就有蓝色的点点



### 3、导入依赖

```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.1.3.RELEASE</version>
    <relativePath/>
</parent>

<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-security</artifactId>
    </dependency>
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>5.1.47</version>
    </dependency>
    <dependency>
        <groupId>tk.mybatis</groupId>
        <artifactId>mapper-spring-boot-starter</artifactId>
        <version>2.1.5</version>
    </dependency>
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <version>1.18.8</version>
    </dependency>


    <!-- springboot不推荐使用jsp，但是为了方便演示，所以使用jsp -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-tomcat</artifactId>
    </dependency>
    <dependency>
        <groupId>org.apache.tomcat.embed</groupId>
        <artifactId>tomcat-embed-jasper</artifactId>
    </dependency>
</dependencies>
```



### 4、yaml 配置

```yaml
server:
  port: 8080
spring:
  mvc:
    view:
      prefix: /pages/
      suffix: .jsp
  datasource:
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql://localhost:3306/security?useSSL=false&characterEncoding=UTF-8&useUnicode=true&serverTimezone=GMT%2B8
    username: root
    password: 12345

mybatis:
  type-aliases-package: com.heroc.entity
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
    map-underscore-to-camel-case: true
  mapper-locations: classpath:mapper/*.xml

```



### 5、SecurityConfig 配置

```java
@Configuration
// 开启security
@EnableWebSecurity
// 开启权限注解
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /**
     * 认证用户
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 在配置中，权限ROLE_USER，不要加前缀，直接USER
        // 使用内存认证
/*        auth.inMemoryAuthentication()
                .withUser("user")
                .password("{noop}123")
                .roles("USER");*/

        /**
         * 通过数据库查询数据，进行认证
         * 参数是 UserDetailsService
         * 因为userService实现了UserDetailsService
         */
        auth.userDetailsService(userService).passwordEncoder(bCryptPasswordEncoder());
    }

    /**
     * 配置springSecurity相关信息
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 释放静态资源，指定拦截规则，指定自定义页面等
        http.authorizeRequests()
                // 释放静态资源
                .antMatchers("/login.jsp","/failer.jsp","/css/**","/img/**","/plugins/**")
                .permitAll()
                // /**下的所有资源需要指定角色访问 、会把设置的角色名自动加上 ‘ ROLE_ ’ 前缀。
                .antMatchers("/**")
                .hasAnyRole("USER","ADMIN")
                // 其他请求需要认证
                .anyRequest()
                .authenticated()
                // 添加其他规则用and()方法连接 登录页面
                .and()
                .formLogin()
                .loginPage("/login.jsp")
                .loginProcessingUrl("/login")
                .successForwardUrl("/index.jsp")
                .failureForwardUrl("/failer.jsp")
                .permitAll()
                // 退出登录页面
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login.jsp")
                .invalidateHttpSession(true)
                .permitAll()
                // 不使用csrf防攻击机制
                .and()
                .csrf()
                .disable();
    }
}
```



### 6、entity

SysUser.java

```java
/**
 * 本方式实现了UserDetails，那么SpringSecurity就会接管该实体，
 * 也可以在security配置类中将sysuser这个实体转换成userdetails实体
 * 供给security接管使用判断认证给予权限。@JsonIgnore是忽略转换为json
 * @author: heroC
 * @time: 2020/9/6 21:05
 */
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class SysUser implements UserDetails {
    private Integer id;
    private String username;
    private String password;
    private Integer status;
    private List<SysRole> roles;

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return status==1;
    }
}
```



SysRole.java

```java
/**
 * GrantedAuthority是security的权限管理的接口，实现了该类，
 * 则会直接获得权限名称。将权限名称返回出去
 * @author: heroC
 * @time: 2020/9/6 21:18
 */
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysRole implements GrantedAuthority {

    private Integer id;
    private String roleName;
    private String roleDesc;

    @Override
    public String getAuthority() {
        return roleName;
    }
}
```



### 7、service

UserService.java 继承 UserDetailsService

```java
public interface UserService extends UserDetailsService {
}
```



UserServiceImpl.java 实现 UserService 

```java
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    /**
     * 通过用户名查询用户信息，返回一个UserDetails对象
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userMapper.findByName(username);
    }
}
```



### 8、controller

```java
@Controller
@RequestMapping("/product")
public class ProductController {

    @Secured({"ROLE_PRODUCT","ROLE_ADMIN"})
    @RequestMapping("/findAll")
    public String findAll(){
        return "product-list";
    }
}
```



### 9、统一处理异常

```java
@ControllerAdvice
public class HandlerExceptionAdvice {

    @ExceptionHandler(RuntimeException.class)
    public String exception403(Exception e){
        if (e instanceof AccessDeniedException){
            return "redirect:/403.jsp";
        }
        return "redirect:/500.jsp";
    }
}
```



## 二、SpringBoot 分布式整合

项目：spring-security-springboot 之 distributed





首先，我们要明确，在分布式项目中，每台服务器都有各自独立的session，而这些session之间是无法直接共享资
源的，所以，session通常不能被作为单点登录的技术方案。



### 1、分布式认证流程图

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringSecurity/分布式认证流程图.png)

**总结一下，单点登录的实现分两大环节：**

- 用户认证：这一环节主要是用户向认证服务器发起认证请求，认证服务器给用户返回一个成功的令牌token，
  主要在认证服务器中完成，即图中的A系统，注意A系统只能有一个。
- 身份校验：这一环节是用户携带token去访问其他服务器时，在其他服务器中要对token的真伪进行检验，主
  要在资源服务器中完成，即图中的B系统，这里B系统可以有很多个。



### 2、JWT 介绍

从分布式认证流程中，我们不难发现，这中间起最关键作用的就是token，token的安全与否，直接关系到系统的
健壮性，这里我们选择使用JWT来实现token的生成和校验。

JWT，全称JSON Web Token，官网地址https://jwt.io，是一款出色的分布式身份校验方案。可以生成token，也可
以解析检验token。

**JWT生成的token由三部分组成：**

- 头部：主要设置一些规范信息，签名部分的编码格式就在头部中声明。
- 载荷：token中存放有效信息的部分，比如用户名，用户角色，过期时间等，但是不要放密码，会泄露！
- 签名：将头部与载荷分别采用base64编码后，用“.”相连，再加入盐，最后使用头部声明的编码类型进行编
  码，就得到了签名。



#### 1）JWT 安全分析

从JWT生成的token组成上来看，要想避免token被伪造，主要就得看签名部分了，而签名部分又有三部分组成，其
中头部和载荷的base64编码，几乎是透明的，毫无安全性可言，那么最终守护token安全的重担就落在了加入的盐
上面了！

试想：如果生成token所用的盐与解析token时加入的盐是一样的。岂不是类似于中国人民银行把人民币防伪技术
公开了？大家可以用这个盐来解析token，就能用来伪造token。

这时，我们就需要对盐采用非对称加密的方式进行加密，以达到生成token与校验token方所用的盐不一致的安全
效果！



#### 2）非对称加密RSA介绍

- 基本原理：同时生成两把密钥：私钥和公钥，私钥隐秘保存，公钥可以下发给信任客户端
  + 私钥加密，持有私钥或公钥才可以解密（**推荐使用**）
  + 公钥加密，持有私钥才可解密
- 优点：安全，难以破解
- 缺点：算法比较耗时，为了安全，可以接受
- 历史：三位数学家Rivest、Shamir 和 Adleman 设计了一种算法，可以实现非对称加密。这种算法用他们三
  个人的名字缩写：RSA



#### 3）依赖

```xml
<dependency>
    <groupId>io.jsonwebtoken</groupId>
    <artifactId>jjwt-api</artifactId>
    <version>0.10.7</version>
</dependency>
<dependency>
    <groupId>io.jsonwebtoken</groupId>
    <artifactId>jjwt-impl</artifactId>
    <version>0.10.7</version>
    <scope>runtime</scope>
</dependency>
<dependency>
    <groupId>io.jsonwebtoken</groupId>
    <artifactId>jjwt-jackson</artifactId>
    <version>0.10.7</version>
    <scope>runtime</scope>
</dependency>
```



### 3、SpringSecurity+JWT+RSA分布式认证思路分析



#### 1）分布式认证流程

- **用户认证**：
  由于，分布式项目，多数是前后端分离的架构设计，我们要满足可以接受异步post的认证请求参数，需要修改UsernamePasswordAuthenticationFilter过滤器中attemptAuthentication方法，让其能够接收请求体。另外，默认successfulAuthentication方法在认证通过后，是把用户信息直接放入session就完事了，现在我	们需要修改这个方法，在认证通过后生成token并返回给用户。

- **身份校验**：
  原来BasicAuthenticationFilter过滤器中doFilterInternal方法校验用户是否登录，就是看session中是否有用户信息，我们要修改为，验证用户携带的token是否合法，并解析出用户信息，交给SpringSecurity，以便于后续的授权功能可以正常使用。



### 4、SpringSecurity+JWT+RSA分布式认证实现

项目：distributed

jwt+rsa：认证中心将私钥封装到token中，给前端。其他服务器只需要配置公钥即可，前端将token中封装的私钥等数据解析出来，公钥可以解密私钥



#### 1）认证服务器

> 创建一个：common项目

utils：`JsonUtils`、`JwtUtils`、`RsaUtils`

Jwt的载荷数据

```java
// domain层
/**
 * jwt生成的token有3部分组成：
 * 头部：主要设置一些规范信息，签名部分的编码格式就在头部中声明
 * 载荷：中存放有效信息的部分，比如用户名，用户角色，过期时间等，但是不要放密码，会泄露！
 * 签名：将头部与载荷分别采用base64编码后，用“.”相连，再加入盐，最后使用头部声明的编码类型进行编
 *      码，就得到了签名。用于校验，防止被篡改。
 *
 * 为了方便后期获取token中的用户信息，将token中载荷部分单独封装成一个对象
 * 该类就是存放载荷中的信息
 * @author: heroC
 * @time: 2020/9/7 22:10
 */
@Data
public class Payload<T> {
    /** id */
    private String id;
    /** 指定泛型的用户信息 */
    private T userInfo;
    /** 过期时间 */
    private Date expiration;
}
```

通过RsaUtils生成私钥和公钥用于设置token的签名。登录成之后将用户信息，私钥，以及过期时间信息集中在一起生成token，返回给前端。



> 创建一个认证服务器：auth_server

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringSecurity/认证服务器.png" style="float:left;" />

**entity**：

​			`SysUser`需要继承Security的`UserDetails`，然后重写里面的方法，这些方法需要`@JsonIgnore`

​			`SysRole`需要继承`GrantedAuthority`，重写里面的方法，获取该用户的角色名也需要`@JsonIgnore`这样该用户的信息和用户角色，都给Security托管了



**service**：

​			既然实体类都继承了UserDetails，那么service也需要继承`UserDetailsService`，实现`loadUserByUsername`方法，通过数据库获取正确的用户信息，与前端用户密码进行匹配认证。



**filter**：

​			`TokenLoginFilter` 继承`UsernamePasswordAuthenticationFilter`登录时，需要进行的过滤操作，主要重写两个方法`attemptAuthentication`认证操作，将前端的用户名和密码封装起来，然后进行与数据库信息匹配。`successfulAuthentication`认证成功之后执行的方法，生成jwt+rsa私钥的token，封装到请求头中，返回给前端，即登录成功。

​			`TokenVerifyFilter`继承`BasicAuthenticationFilter`验证操纵。重写`doFilterInternal`方法，验证请求头中的token。



**config**：

​				`RsaKeyProperties`是读取配置文件根据私钥公钥的路径获取私钥公钥

​			   `WebSecurityConfig`认证配置，指定service，以及加密类；设置各种过滤配置



#### 2）资源服务器

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringSecurity/资源服务器.png" style="float:left;" />

只需要对token进行验证即可。配置文件中只需要引入公钥。RsaKeyProperties也只需要从配置文件中获取公钥路径，从而得到公钥。



# ==OAuth2.0==

## 一、介绍

>先说OAuth，OAuth是Open Authorization的简写。
>
>OAuth协议为用户资源的授权提供了一个安全的、开放而又简易的标准。与以往的授权方式不同之处是OAuth的授权不会使第三方触及到用户的帐号信息（如用户名与密码），即第三方无需使用用户的用户名与
>密码就可以申请获得该用户资源的授权，因此OAuth是安全的。
>
>OAuth2.0是OAuth协议的延续版本，但不向前兼容(即完全废止了OAuth1.0)。



### 1、使用场景

假设，A网站是一个打印照片的网站，B网站是一个存储照片的网站，二者原本毫无关联。

如果一个用户想使用A网站打印自己存储在B网站的照片，那么A网站就需要使用B网站的照片资源才行。

按照传统的思考模式，我们需要A网站具有登录B网站的用户名和密码才行，但是，现在有了OAuth2，只需要A网
站获取到使用B网站照片资源的一个通行令牌即可！这个令牌无需具备操作B网站所有资源的权限，也无需永久有
效，只要满足A网站打印照片需求即可。

这么听来，是不是有点像单点登录？NONONO！千万不要混淆概念！单点登录是用户一次登录，自己可以操作其
他关联的服务资源。OAuth2则是用户给一个系统授权，可以直接操作其他系统资源的一种方式。

但SpringSecurity的OAuth2也是可以实现单点登录的！

总结一句：SpringSecurity的OAuth2可以做服务之间资源共享，也可以实现单点登录！



