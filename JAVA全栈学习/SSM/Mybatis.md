Mybatis

[Mybatis中文开发文档][https://mybatis.org/mybatis-3/zh/index.html]



## 一、简介



### 1、什么是Mybatis

MyBatis 是一款优秀的**持久层框架**，它支持自定义 SQL、存储过程以及高级映射。MyBatis 免除了几乎所有的 JDBC 代码以及设置参数和获取结果集的工作。MyBatis 可以通过简单的 XML 或注解来配置和映射原始类型、接口和 Java POJO（Plain Old Java Objects，普通老式 Java 对象）为数据库中的记录。

MyBatis 本是apache的一个开源项目iBatis, 2010年这个项目由apache software foundation 迁移到了google code，并且改名为MyBatis 。2013年11月迁移到Github。iBATIS提供的持久层框架包括SQL Maps和Data Access Objects（DAOs 数据访问对象）



### 2、为什么使用Mybatis

- 方便
- 简化JDBC繁琐的操作，冗余的代码
- 自动化帮你操作很多操作



#### . 优点

- 简单易学
- 灵活
- 解除sql与程序代码的耦合
- 提供映射标签，支持对象与数据库的orm字段关系映射
- 提供对象关系映射标签，支持对象关系组建维护
- 提供xml标签，支持编写动态sql



## 二、第一个Mybatis程序

**注意**

如果出现中文乱码，无法执行程序的情况，可能是idea设置中没有设置好编码格式：

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/编码问题.png" style="float:left;" />

**整个程序结构**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/第一个mybatis工程.png" style="float:left;" />



### 1、创建一个数据库，表，插入数据

```sql
CREATE DATABASE `mybatis`;

USE mybatis;

CREATE TABLE `user`(
  `id` INT(3) ZEROFILL AUTO_INCREMENT COMMENT 'ID',
  `name` VARCHAR(30) DEFAULT NULL COMMENT '用户姓名',
  `pwd` VARCHAR(30) DEFAULT NULL COMMENT '用户密码',
   PRIMARY KEY(`id`) COMMENT 'ID为主键'
)ENGINE=INNODB DEFAULT CHARSET=utf8;

DESC `user`

INSERT INTO `user` VALUES
(1,'张还行','1130'),
(2,'heroC','123'),
(3,'十一','456')

SELECT * FROM `user`
```



### 2、搭建环境，Maven、导入mybatis

```xml
<dependency>
    <groupId>org.mybatis</groupId>
    <artifactId>mybatis</artifactId>
    <version>3.4.6</version>
</dependency>

<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.15</version>
</dependency>

<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.12</version>
    <scope>test</scope>
</dependency>

<!--避免有些xml不打包到运行目录中而导致报错-->
<build>
    <resources>
        <resource>
            <directory>src/main/resources</directory>
            <includes>
                <include>**/*.properties</include>
                <include>**/*.xml</include>
            </includes>
            <filtering>true</filtering>
        </resource>

        <resource>
            <directory>src/main/java</directory>
            <includes>
                <include>**/*.properties</include>
                <include>**/*.xml</include>
            </includes>
            <filtering>true</filtering>
        </resource>
    </resources>
</build>
```



### 3、XML配置文件以及utils

mybatis-config.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">

<configuration>
    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC"/>
            <dataSource type="POOLED">
                <property name="driver" value="com.mysql.cj.jdbc.Driver"/>
                <property name="url" value="jdbc:mysql://localhost:3306/mybatis?useSSL=true&amp;useUnicode=true&amp;characterEncoding=utf-8"/>
                <property name="username" value="root"/>
                <property name="password" value="113011"/>
            </dataSource>
        </environment>
    </environments>
    
    <!--每一个mapper配置文件都需要注册到mybatis库中-->
    <!--不注册会报错，Type interface xxxx is not known to the MapperRegistry-->
    <mappers>
        <mapper resource="com/heroc/dao/UserMapper.xml"/>
    </mappers>
</configuration>
```

MybatisUtils.java

```java
/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/3/28
 * Time: 17:11
 * Description:
 * 通过获取配置文件，加载驱动，连接数据库
 * Resources.getResourceAsStream(resource) 加载配置文件
 * SqlSessionFactoryBuilder().build(resourceIn) 获取sqlSessionFactory实例
 * 通过sqlSessionFactory对象获取sqlSession实例，
 * sqlSession实例就如同jdbc中的connection对象，里面有很多与connection对象相同的方法
 * Version: V1.0
 */
public class MybatisUtils {
    private static SqlSessionFactory sqlSessionFactory;

    static {
        try {
            // 通过加载配置文件，获取sqlSessionFactory对象
            String resource = "mybatis-config.xml";
            InputStream resourceIn = Resources.getResourceAsStream(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 获取sqlSession实例，该对象就如同jdbc中的connection对象，里面有很多与connection对象相同的方法
    public static SqlSession getSqlSession(){
        return sqlSessionFactory.openSession();
    }
}
```



### 4、编写代码

**pojo层**

```java
/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/3/28
 * Time: 17:34
 * Description: User实体类，该实体类必须与数据库的字段一致，数据类型一致
 * Version: V1.0
 */
public class User implements Serializable {
    private static final long serialVersionUID = 1130435674L;

    private int id;
    private String name;
    private String pwd;

    public User() {
    }

    public User(int id, String name, String pwd) {
        this.id = id;
        this.name = name;
        this.pwd = pwd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pwd='" + pwd + '\'' +
                '}';
    }
}
```

**dao层 (mapper层)**

UserDao.java

```java
/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/3/28
 * Time: 17:36
 * Description:
 * User实体类的dao层(也是mapper层)，数据持久化层
 * 用于将User相关的操作与数据库进行操作
 * 对应一个mapper.xml
 * Version: V1.0
 */
public interface UserDao {
    List<User> getUserList();
}
```

UserMapper.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<!--由一个UserDao接口的UserDaoImpl实现类，转换为一个mapper配置文件-->
<!--namespace 绑定一个持久层中需要实现持久化操作的接口-->
<mapper namespace="com.heroc.dao.UserDao">

    <!--id 绑定对应的接口中需要实现的抽象方法名 意思就是去实现这个方法-->
    <!--resultType 接口中id绑定的抽象方法需要返回的结果类型-->
    <!--select中的sql语句类似于在id绑定的抽象方法中定义需要执行的sql语句-->
    <!--作用实现指定接口中的抽象方法-->
    <select id="getUserList" resultType="com.heroc.pojo.User">
        select * from user;
    </select>

</mapper>
```



### 5、测试

使用junit测试

```java
/**
 * Created with IntelliJ IDEA
 * User: heroC
 * Date: 2020/3/28
 * Time: 17:54
 * Description: 测试UserDao接口中getUserList抽象方法
 * Version: V1.0
 */
public class UserDaoTest {

    @Test
    public void getUserListTest(){
        // 获得sqlSession对象
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        // 通过反射获取到被mapper配置了的接口的实现类(向上转型)
        UserDao userDao = sqlSession.getMapper(UserDao.class);
        // 通过接口实现类，调用方法，后去从数据库获得的数据，并遍历
        List<User> userList = userDao.getUserList();
        for (User user : userList) {
            System.out.println(user);
        }
        // 关闭sqlSession对象
        sqlSession.close();
    }
}
```

**结果：**

```
User{id=1, name='张还行', pwd='1130'}
User{id=2, name='heroC', pwd='123'}
User{id=3, name='十一', pwd='456'}
```



### 6、作用域和生命周期 ==重要==

> 来自官方文档

理解我们之前讨论过的不同作用域和生命周期类别是至关重要的，因为**错误的使用会导致非常严重的并发问题**。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/mybatis流程.png)

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/sqlsession与mapper.png)



#### . SqlSessionFactoryBuilder

这个类可以被实例化、使用和丢弃，**一旦创建了 SqlSessionFactory，就不再需要它了**。 因此 SqlSessionFactoryBuilder 实例的最佳作用域是**方法作用域**（也就是局部方法变量）。 你可以重用 SqlSessionFactoryBuilder 来创建多个 SqlSessionFactory 实例，但最好还是不要一直保留着它，以保证所有的 XML 解析资源可以被释放给更重要的事情。



#### . SqlSessionFactory

SqlSessionFactory **一旦被创建就应该在应用的运行期间一直存在**，没有任何理由丢弃它或重新创建另一个实例。 使用 SqlSessionFactory 的最佳实践是在应用运行期间不要重复创建多次，多次重建 SqlSessionFactory 被视为一种代码“坏习惯”。因此 SqlSessionFactory 的最佳作用域是**应用作用域**（全局唯一，全局作用域）。 有很多方法可以做到，最简单的就是**使用单例模式或者静态单例模式创建SqlSessionFactory **。



#### . SqlSession

每个线程都应该有它自己的 SqlSession 实例。SqlSession 的实例**不是线程安全的，因此是不能被共享的**，所以它的最佳的作用域是请求或**方法作用域**。 绝对不能将 SqlSession 实例的引用放在一个类的静态域，甚至一个类的实例变量也不行。 也绝不能将 SqlSession 实例的引用放在任何类型的托管作用域中，比如 Servlet 框架中的 HttpSession。 如果你现在正在使用一种 Web 框架，考虑将 SqlSession 放在一个和 HTTP 请求相似的作用域中。 换句话说，每次收到 HTTP 请求，就可以打开一个 SqlSession，**返回一个响应后，就关闭它**。 这个关闭操作很重要，为了确保每次都能执行关闭操作，你应该把这个关闭操作放到 finally 块中。



#### . 映射器实例

映射器是一些绑定映射语句的接口。映射器接口的实例是从 SqlSession 中获得的。虽然从技术层面上来讲，任何映射器实例的最大作用域与请求它们的 SqlSession 相同。但方法作用域才是映射器实例的最合适的作用域。 也就是说，映射器实例应该在调用它们的方法中被获取，使用完毕之后即可丢弃。 映射器实例并不需要被显式地关闭。尽管在整个请求作用域保留映射器实例不会有什么问题，但是你很快会发现，在这个作用域上管理太多像 SqlSession 的资源会让你忙不过来。 因此，最好将映射器放在方法作用域内。就像下面的例子一样：

```java
try (SqlSession session = sqlSessionFactory.openSession()) {
  BlogMapper mapper = session.getMapper(BlogMapper.class);
  // 你的应用逻辑代码
}
```



## 三、CRUD ==事务==

**注意：**

==增删改 一定要提交事务，否则数据库中记录不会受影响。==

```java
// MybatisUtils.java 获取sqlSession，传入Boolean值true会自动提交数据；默认为false，需要手动提交数据
public static SqlSession getSqlSession(){
	return sqlSessionFactory.openSession();
}
// ---> 源码
public SqlSession openSession(boolean autoCommit);
```



```xml
<!--通过mapper.xml就可以知道接口是啥，接口中的抽象方法是啥-->
<?xml version="1.0" encoding="GBK" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.heroc.dao.UserMapper">
    
    <select id="getUserList" resultType="com.heroc.pojo.User">
        select * from mybatis.user;
    </select>

    <select id="getUserByID" parameterType="int" resultType="com.heroc.pojo.User">
        select * from mybatis.user where id = #{id}
    </select>

    <insert id="insertUser" parameterType="com.heroc.pojo.User">
        <!--占位符中的变量一定要与User类的变量名一致-->
        insert into mybatis.user(id,`name`,pwd) values (#{id},#{name},#{pwd})
    </insert>

    <update id="updateUser" parameterType="com.heroc.pojo.User">
        update mybatis.user set `name`=#{name}, pwd=#{pwd} where id=#{id}
    </update>

    <delete id="deleteUser" parameterType="int">
        delete from mybatis.user where id=#{id}
    </delete>

</mapper>
```



测试类

```java
public class UserMapperTest {

    @Test
    public void getUserListTest(){
        // 获得sqlSession对象
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        // 通过反射获取到被mapper配置了的接口的实现类(向上转型)
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        // 通过接口实现类，调用方法，后去从数据库获得的数据，并遍历
        List<User> userList = userMapper.getUserList();
        for (User user : userList) {
            System.out.println(user);
        }
        // 关闭sqlSession对象
        sqlSession.close();
    }

    @Test
    public void getUserByIDTest(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        System.out.println(mapper.getUserByID(1));
        sqlSession.close();
    }

    @Test
    public void insertUserTest(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        int count = mapper.insertUser(new User(4, "yikeX", "7890"));
        // 增删改一定要提交事务，否则数据不会写入到数据库的表中
        sqlSession.commit();
        if(count>0){
            System.out.println("数据插入成功");
        }

        sqlSession.close();
    }

    @Test
    public void updateUserTest(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        int count = mapper.updateUser(new User(4, "X", "66888"));
        // 增删改一定要提交事务，否则数据不会写入到数据库的表中
        sqlSession.commit();
        if(count>0){
            System.out.println("数据更新成功");
        }

        sqlSession.close();
    }

    @Test
    public void deleteUserTest(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        int count = mapper.deleteUser(4);
        // 增删改一定要提交事务，否则数据不会写入到数据库的表中
        sqlSession.commit();
        if(count>0){
            System.out.println("数据删除成功");
        }

        sqlSession.close();
    }
}
```



## 四、Map

map参数可以随意插入值，比pojo层中的类更加灵活

```java
// 插入用户
int insertUserMap(Map<String,Object> map);
```

```xml
<insert id="insertUserMap" parameterType="map">
    <!--占位符中的变量是map中的键key-->
    insert into user(`name`,pwd) values (#{userName},#{userPwd})
</insert>
```

```java
@Test
public void insertUserMapTest(){
    SqlSession sqlSession = MybatisUtils.getSqlSession();

    UserMapper mapper = sqlSession.getMapper(UserMapper.class);
    Map<String,Object> map = new HashMap<>();
    map.put("userName","yikeX");
    map.put("userPwd","23443");
    mapper.insertUserMap(map);
    // 增删改一定要提交事务，否则数据不会写入到数据库的表中
    sqlSession.commit();

    sqlSession.close();
}
```



## 五、配置解析

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/mybatis配置结构.png)



### 1、properties

这些属性可以在外部进行配置，并可以进行动态替换。你既可以在典型的 Java 属性文件中配置这些属性，也可以在 properties 元素的子元素中设置。例如：

**db.properties**

```properties
driver=com.mysql.cj.jdbc.Driver
url=jdbc:mysql://localhost:3306/mybatis?useUnicode=true&useSSL=true&characterEncoding=UTF-8
username=root
password=113011
```

在mybatis-config.xml中可以通过properties属性引用该配置，例如：

```xml
<?xml version="1.0" encoding="GBK" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <!--属性之间的导入是有规定顺序的-->
    <properties resource="db.properties">
        <property name="username" value="root"/>
        <property name="password" value="111111"/>
    </properties>
    
    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC"/>
            <dataSource type="POOLED">
                <property name="driver" value="${driver}"/>
                <property name="url" value="${url}"/>
                <property name="username" value="${username}"/>
                <property name="password" value="${password}"/>
            </dataSource>
        </environment>
    </environments>
</configuration>
```

导入了properties配置文件之后，可以使用`${key}`使用properties中的对应值

==注意==  在通过properties属性引入了properties配置文件之后，还可以通过property属性对properties配置文件进行追加，如果键值对出现重复，例如：username和password与properties配置文件中的username和password重复配置了，在程序执行中，**properties配置文件中的配置是优先使用的**，会忽略在mybatis-config.xml中property属性追加的重复项。



### 2、setting

[mybatis配置setting属性][https://mybatis.org/mybatis-3/zh/configuration.html#settings]

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/setting属性logImpl.png)

一个配置比较完整的 settings 元素的示例如下：

```xml
<settings>
  <setting name="cacheEnabled" value="true"/>
  <setting name="lazyLoadingEnabled" value="true"/>
  <setting name="multipleResultSetsEnabled" value="true"/>
  <setting name="useColumnLabel" value="true"/>
  <setting name="useGeneratedKeys" value="false"/>
  <setting name="autoMappingBehavior" value="PARTIAL"/>
  <setting name="autoMappingUnknownColumnBehavior" value="WARNING"/>
  <setting name="defaultExecutorType" value="SIMPLE"/>
  <setting name="defaultStatementTimeout" value="25"/>
  <setting name="defaultFetchSize" value="100"/>
  <setting name="safeRowBoundsEnabled" value="false"/>
  <setting name="mapUnderscoreToCamelCase" value="false"/>
  <setting name="localCacheScope" value="SESSION"/>
  <setting name="jdbcTypeForNull" value="OTHER"/>
  <setting name="lazyLoadTriggerMethods" value="equals,clone,hashCode,toString"/>
</settings>
```



### 3、typeAliases

设置别名，为了方便使用pojo层的java bean，设置别名之后，就不用使用全限定名



**第一种方式：**

在mybatis-config.xml中

```xml
<typeAliases>
    <typeAlias type="com.heroc.pojo.User" alias="user"/>
</typeAliases>
```

将pojo包中的User类，设定一个别名为user，在mapper.xml中就可以直接使用别名

```xml
<select id="getUserList" resultType="user">
    select * from `user`
</select>
```



**第二种方式：**

在mybatis-config.xml中

```xml
<typeAliases>
    <package name="com.heroc.pojo"/>
</typeAliases>
```

将pojo包中的所有java bean都扫描，自动设置别名，别名为**该类以首字母小写开始的类名**



**第三种方式：**

由于第二种方式，扫描自动设置别名，有些类名过于长，在使用该类的别名时很长很麻烦，所以引入注解，注解优先级别比第二种方式高。因此在使用第二种方式时，有些类名过长，可以使用注解取指定的别名。

```java
@Alias("hero")
public class User {
    private int id;
    private String name;
    private String pwd;

    public User() {
    }
    ...
}
```

==注意== **别名优先级：指定java bean取别名 > 注解取别名 > 包扫描批量取别名**



### 4、enviroments

```xml
<!--default 可以指定使用哪个环境，只能指定一个-->
<environments default="development">
    <!--id 是这个环境的身份，名称-->
    <environment id="development">
        <!--transactionManager中type 是指定该环境使用哪个事务管理器 [JDBC|MANAGED] -->
        <transactionManager type="JDBC"/>
        <!--dataSource中type 是指定使用什么类型的数据源 [UNPOOLED|POOLED|JNDI] -->
        <dataSource type="POOLED">
            <property name="driver" value="${driver}"/>
            <property name="url" value="${url}"/>
            <property name="username" value="${username}"/>
            <property name="password" value="${password}"/>
        </dataSource>
    </environment>

    <environment id="test">
        <transactionManager type="JDBC"/>
        <dataSource type="POOLED">
            <property name="driver" value="${driver}"/>
            <property name="url" value="${url}"/>
            <property name="username" value="${username}"/>
            <property name="password" value="${password}"/>
        </dataSource>
    </environment>
</environments>
```



**事务管理器（transactionManager）**

在 MyBatis 中有两种类型的事务管理器（也就是 type="[JDBC|MANAGED]"）：

- JDBC – 这个配置直接使用了 JDBC 的提交和回滚设施，它依赖从数据源获得的连接来管理事务作用域。

- MANAGED – 这个配置几乎没做什么。它从不提交或回滚一个连接，而是让容器来管理事务的整个生命周期（比如 JEE 应用服务器的上下文）。 默认情况下它会关闭连接。然而一些容器并不希望连接被关闭，因此需要将 closeConnection 属性设置为 false 来阻止默认的关闭行为。

  ```xml
  <transactionManager type="MANAGED">
    <property name="closeConnection" value="false"/>
  </transactionManager>
  ```

> 如果你正在使用 Spring + MyBatis，则没有必要配置事务管理器，因为 Spring 模块会使用自带的管理器来覆盖前面的配置。



**数据源（dataSource）**

dataSource 元素使用标准的 JDBC 数据源接口来配置 JDBC 连接对象的资源。

- 大多数 MyBatis 应用程序会按示例中的例子来配置数据源。虽然数据源配置是可选的，但如果要启用延迟加载特性，就必须配置数据源。

有三种内建的数据源类型（也就是 type="[UNPOOLED|POOLED|JNDI]"）：

**UNPOOLED**– 这个数据源的实现会每次请求时打开和关闭连接。虽然有点慢，但对那些数据库连接可用性要求不高的简单应用程序来说，是一个很好的选择。 性能表现则依赖于使用的数据库，对某些数据库来说，使用连接池并不重要，这个配置就很适合这种情形。UNPOOLED 类型的数据源仅仅需要配置以下 5 种属性：

- `driver` – 这是 JDBC 驱动的 Java 类全限定名（并不是 JDBC 驱动中可能包含的数据源类）。
- `url` – 这是数据库的 JDBC URL 地址。
- `username` – 登录数据库的用户名。
- `password` – 登录数据库的密码。
- `defaultTransactionIsolationLevel` – 默认的连接事务隔离级别。
- `defaultNetworkTimeout` – 等待数据库操作完成的默认网络超时时间（单位：毫秒）。查看 `java.sql.Connection#setNetworkTimeout()` 的 API 文档以获取更多信息。

作为可选项，你也可以传递属性给数据库驱动。只需在属性名加上“driver.”前缀即可，例如：

- `driver.encoding=UTF8`

这将通过 DriverManager.getConnection(url, driverProperties) 方法传递值为 `UTF8` 的 `encoding` 属性给数据库驱动。

**POOLED**– 这种数据源的实现利用“池”的概念将 JDBC 连接对象组织起来，避免了创建新的连接实例时所必需的初始化和认证时间。 这种处理方式很流行，能使并发 Web 应用快速响应请求。

除了上述提到 UNPOOLED 下的属性外，还有更多属性用来配置 POOLED 的数据源：

- `poolMaximumActiveConnections` – 在任意时间可存在的活动（正在使用）连接数量，默认值：10
- `poolMaximumIdleConnections` – 任意时间可能存在的空闲连接数。
- `poolMaximumCheckoutTime` – 在被强制返回之前，池中连接被检出（checked out）时间，默认值：20000 毫秒（即 20 秒）
- `poolTimeToWait` – 这是一个底层设置，如果获取连接花费了相当长的时间，连接池会打印状态日志并重新尝试获取一个连接（避免在误配置的情况下一直失败且不打印日志），默认值：20000 毫秒（即 20 秒）。
- `poolMaximumLocalBadConnectionTolerance` – 这是一个关于坏连接容忍度的底层设置， 作用于每一个尝试从缓存池获取连接的线程。 如果这个线程获取到的是一个坏的连接，那么这个数据源允许这个线程尝试重新获取一个新的连接，但是这个重新尝试的次数不应该超过 `poolMaximumIdleConnections` 与 `poolMaximumLocalBadConnectionTolerance` 之和。 默认值：3（新增于 3.4.5）
- `poolPingQuery` – 发送到数据库的侦测查询，用来检验连接是否正常工作并准备接受请求。默认是“NO PING QUERY SET”，这会导致多数数据库驱动出错时返回恰当的错误消息。
- `poolPingEnabled` – 是否启用侦测查询。若开启，需要设置 `poolPingQuery` 属性为一个可执行的 SQL 语句（最好是一个速度非常快的 SQL 语句），默认值：false。
- `poolPingConnectionsNotUsedFor` – 配置 poolPingQuery 的频率。可以被设置为和数据库连接超时时间一样，来避免不必要的侦测，默认值：0（即所有连接每一时刻都被侦测 — 当然仅当 poolPingEnabled 为 true 时适用）。

**JNDI** – 这个数据源实现是为了能在如 EJB 或应用服务器这类容器中使用，容器可以集中或在外部配置数据源，然后放置一个 JNDI 上下文的数据源引用。这种数据源配置只需要两个属性：

- `initial_context` – 这个属性用来在 InitialContext 中寻找上下文（即，initialContext.lookup(initial_context)）。这是个可选属性，如果忽略，那么将会直接从 InitialContext 中寻找 data_source 属性。
- `data_source` – 这是引用数据源实例位置的上下文路径。提供了 initial_context 配置时会在其返回的上下文中进行查找，没有提供时则直接在 InitialContext 中查找。

和其他数据源配置类似，可以通过添加前缀“env.”直接把属性传递给 InitialContext。比如：

- `env.encoding=UTF8`

这就会在 InitialContext 实例化时往它的构造方法传递值为 `UTF8` 的 `encoding` 属性。



### 5、mappers

既然 MyBatis 的行为已经由上述元素配置完了，我们现在就要来定义 SQL 映射语句了。 但首先，我们需要告诉 MyBatis 到哪里去找到这些语句。 在自动查找资源方面，Java 并没有提供一个很好的解决方案，所以最好的办法是直接告诉 MyBatis 到哪里去找映射文件。 你可以使用相对于类路径的资源引用，或完全限定资源定位符（包括 `file:///` 形式的 URL），或类名和包名等。例如：

```xml
<!-- 使用相对于类路径的资源引用 推荐使用-->
<mappers>
  <mapper resource="org/mybatis/builder/AuthorMapper.xml"/>
  <mapper resource="org/mybatis/builder/BlogMapper.xml"/>
  <mapper resource="org/mybatis/builder/PostMapper.xml"/>
</mappers>

<!-- 
以下两种方式的使用需要注意：
1，mapper配置文件与接口名必须同名！！！
2，必须在同一个包下 ！！！
-->
<!-- 使用映射器接口实现类的完全限定类名 -->
<mappers>
  <mapper class="org.mybatis.builder.AuthorMapper"/>
  <mapper class="org.mybatis.builder.BlogMapper"/>
  <mapper class="org.mybatis.builder.PostMapper"/>
</mappers>

<!-- 将包内的映射器接口实现全部注册为映射器 -->
<mappers>
  <package name="org.mybatis.builder"/>
</mappers>


<!-- 使用完全限定资源定位符（URL） 强制不推荐-->
<mappers>
  <mapper url="file:///var/mappers/AuthorMapper.xml"/>
  <mapper url="file:///var/mappers/BlogMapper.xml"/>
  <mapper url="file:///var/mappers/PostMapper.xml"/>
</mappers>
```

这些配置会告诉 MyBatis 去哪里找映射文件，剩下的细节就应该是每个 SQL 映射文件了，也就是接下来我们要讨论的。



## 六、ResultMap 结果映射

类型别名是你的好帮手。使用它们，你就可以不用输入类的全限定名了。比如：

```xml
<!-- mybatis-config.xml 中 -->
<typeAlias type="com.someapp.model.User" alias="User"/>

<!-- SQL 映射 XML 中 -->
<select id="selectUsers" resultType="User">
  select id, username, hashedPassword
  from some_table
  where id = #{id}
</select>
```

在这些情况下，MyBatis 会在幕后自动创建一个 `ResultMap`，再根据属性名来映射列到 JavaBean 的属性上。

**如果属性名与表中的字段名不一致，那么就会造成映射失败！！！**



**解决方法一：**

通过在xml中的sql语句，每一个字段取一个别名与JavaBean的属性名一致

```xml
<select id="selectUsers" resultType="User">
  select
    user_id             as "id",
    user_name           as "userName",
    hashed_password     as "hashedPassword"
  from some_table
  where id = #{id}
</select>
```

这种方法的使用有一定局限性



**解决方法二 (推荐使用):**

如果表中pwd字段与javaBean中password属性名不一样，就得通过 resultMap结果映射 来实现。

如果字段与属性名一致，在resultMap结果映射配置中，就不用写出来。

需要注意的是sql语句标签中resultMap属性要指对resultMap标签的id属性。

```xml
<resultMap id="userMap" type="User">
    <!--column 对应的是表中字段； property 对应的是User类中的属性-->
    <!--<result column="id" property="id"/>
    <result column="name" property="name"/>-->
    <result column="pwd" property="password"/>
</resultMap>

<select id="getUserList" resultMap="userMap">
    select * from `user`
</select>
```



## 七、日志工厂

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/setting属性logImpl.png)

**日志模式：**

- SLF4J
- LOG4J (掌握)
- LOG4J2
- JDK_LOGGING
- COMMONS_LOGGING
- STDOUT_LOGGING (标准日志 掌握)
- NO_LOGGING



### STDOUT_LOGGING

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/stdout_logging.png)



### LOG4J

导入依赖包：

```xml
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.17</version>
</dependency>
```

**log4j.properties**

```properties
#将等级为DEBUG的日志信息输出到console和file这两个目的地，console和file的定义在下面的代码
log4j.rootLogger=DEBUG,console,file

#控制台输出的相关设置
log4j.appender.console=org.apache.log4j.ConsoleAppender
log4j.appender.console.Target=System.out
log4j.appender.console.Threshold=DEBUG
log4j.appender.console.layout=org.apache.log4j.PatternLayout
log4j.appender.console.layout.ConversionPattern=[%c]-%m%n

#文件输出的相关设置
log4j.appender.file=org.apache.log4j.RollingFileAppender
log4j.appender.file.File=./log/heroc.log
log4j.appender.file.MaxFileSize=10mb
log4j.appender.file.Threshold=DEBUG
log4j.appender.file.layout=org.apache.log4j.PatternLayout
log4j.appender.file.layout.ConversionPattern=[%p][%d{yyyy-MM-dd}][%c]%m%n

#日志输出级别
log4j.logger.org.mybatis=DEBUG
log4j.logger.java.sql=DEBUG
log4j.logger.java.sql.Statement=DEBUG
log4j.logger.java.sql.ResultSet=DEBUG
log4j.logger.java.sql.PreparedStatement=DEBUG
```

测试：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/log4j.png)



**log4j 的其他使用**

可通过获取log4j的logger对象，在必要输出日志时，使用不同等级的输出

```java
// 获取log4j的logger对象
static Logger logger = Logger.getLogger(UserMapperTest.class);

@Test
public void Log4jTest(){
    logger.debug("log4j这是一条debug类型的测试日志");
    logger.info("log4j这是一条info类型的测试日志");
    logger.error("log4j这是一条error类型的测试日志");
}
```



## 八、分页

### 1、limit 实现分页 (推荐)

UserMapper.java

```java
public interface UserMapper {
    // 分页查询
    List<User> getUserByLimit(HashMap<String,Integer> map);
}
```

UserMapper.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.dao.UserMapper">
    <resultMap id="UserMap" type="user">
        <result column="pwd" property="password"/>
    </resultMap>

    <select id="getUserByLimit" resultMap="UserMap" parameterType="map">
        select * from user limit #{startPage},#{pageContextNumber}
    </select>
</mapper>
```

UserMapperTest.java

```java
public class UserMapperTest {
    @Test
    public void getUserByLimitTest(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        HashMap<String, Integer> map = new HashMap<>();
        map.put("startPage",0);
        map.put("pageContextNumber",2);
        List<User> userList = mapper.getUserByLimit(map);
        for (User user : userList) {
            System.out.println(user);
        }
        
        sqlSession.close();
    }
}
```



### 2、RowBunds类 实现分页

通过RowBunds类，也可以实现分页查询。RowBunds类，是通过返回的结果集，进行内存区域分页。



UserMapper.java

```java
public interface UserMapper {
    // rowBounds分页查询
    List<User> getUserByRowBounds();
}
```

UserMapper.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.dao.UserMapper">
    <resultMap id="UserMap" type="user">
        <result column="pwd" property="password"/>
    </resultMap>
    <select id="getUserByRowBounds" resultMap="UserMap">
        select * from user
    </select>
</mapper>
```

UserMapperTest.java

```java
public class UserMapperTest {
	@Test
    public void getUserByRowBoundsTest(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        RowBounds rowBounds = new RowBounds(0,2);
        List<User> userList = sqlSession.selectList(
            "com.heroc.dao.UserMapper.getUserByRowBounds",
            null, 
            rowBounds);
        for (User user : userList) {
            System.out.println(user);
        }

        sqlSession.close();
    }
}
```



## 九、Mybatis执行流程

 ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/mybatis执行流程.png)



## 十、关于@Param()注解

```java
public interface UserMapper {
    User getUserById(@Param("Uid") int id, @Param("Uname") String name);
}
```

**使用范围：**

- 在多个参数时使用@Param()，里面指定该变量的名字，在mapper.xml文件中的sql语句就使用@Param()设置的名字；
- 一个参数不用使用该注解，但是建议加上
- 参数类型是引用类型就不用使用@Param()



### . 占位符#{} 和 ${} 的区别

`#{}` 占位符，可以防止注入，类似parastement ( 使用`#{}` 强制)

`${}` 字符拼接，不能防止注入，类似statement



## 十一、Lombok框架

了解有这个框架即可，不推荐使用。

Project Lombok is a java library that automatically plugs into your editor and build tools, spicing up your java.
Never write another getter or equals method again, with one annotation your class has a fully featured builder, Automate your logging variables, and much more.



**pom.xml** 导入依赖包

```xml
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.18.12</version>
    <scope>provided</scope>
</dependency>
```



注解

```
@Getter and @Setter
@FieldNameConstants
@ToString
@EqualsAndHashCode
@AllArgsConstructor, @RequiredArgsConstructor and @NoArgsConstructor
@Log, @Log4j, @Log4j2, @Slf4j, @XSlf4j, @CommonsLog, @JBossLog, @Flogger, @CustomLog
@Data
@Builder
@SuperBuilder
@Singular
@Delegate
@Value
@Accessors
@Wither
@With
@SneakyThrows
@val
@var
experimental @var
@UtilityClass
```



<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/@Data注解.png" style="float:left" />



在POJO包中的类上添加对应注解，就可以自动写构造器、getter/setter等方法



## 十二、复杂查询

### 1、创建表

创建了teacher表和student表，使得老师与学生实现1对多的情况

```sql
CREATE TABLE `teacher`(
  `id` INT(10) NOT NULL,
  `name` VARCHAR(30) DEFAULT NULL,
  PRIMARY KEY(`id`)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

INSERT INTO teacher VALUES(1,'张老师');`teacher`

CREATE TABLE `student`(
  `id` INT(10) NOT NULL,
  `name` VARCHAR(30) DEFAULT NULL,
  `tid` INT(10),
  PRIMARY KEY(`id`),
  CONSTRAINT `fk_tid` FOREIGN KEY (`tid`) REFERENCES `teacher`(`id`)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

INSERT INTO `student` VALUES
(1,'小明',1),(2,'小红',1),(3,'小张',1),(4,'小李',1),(5,'小王',1),(6,'小侯',1);
```

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/多表结构.png)

### 2、配置环境

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/复杂查询01.png" style="float:left" />

在resource包中创建com/heroc/dao与java包中com/heroc/dao，在编译时，会合并在一起。

**db.properties**

```properties
driver=com.mysql.cj.jdbc.Driver
url=jdbc:mysql://localhost:3306/mybatis?useUnicode=true&useSSL=true&characterEncoding=UTF-8
username=root
password=113011
```

**mybatis-config.xml**

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <properties resource="db.properties"/>
    
    <settings>
        <setting name="logImpl" value="STDOUT_LOGGING"/>
    </settings>

    <typeAliases>
        <typeAlias type="com.heroc.pojo.Teacher" alias="teacher"/>
    </typeAliases>
    
    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC"/>
            <dataSource type="POOLED">
                <property name="driver" value="${driver}"/>
                <property name="url" value="${url}"/>
                <property name="username" value="${username}"/>
                <property name="password" value="${password}"/>
            </dataSource>
        </environment>
    </environments>

    <!--注册mapper配置-->
    <mappers>
        <mapper resource="com/heroc/dao/TeacherMapper.xml"/>
        <mapper resource="com/heroc/dao/StudentMapper.xml"/>
    </mappers>
</configuration>
```

**MybatisUtils.java**

```java
public class MybatisUtils {
    private static SqlSessionFactory sqlSessionFactory;
    static {
        try {
            InputStream resourceAsStream = Resources.getResourceAsStream("mybatis-config.xml");
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static SqlSession getSqlSession(){
        return sqlSessionFactory.openSession();
    }
}
```



### 3、多对一 查询处理

> mybatis04工程

查询学生id，学生name，老师name

**Student.java**

```java
public class Student implements Serializable {
    private static final long serialVersionUID = 1130345459L;
    private int id;
    private String name;
    // 学生需要关联一个老师
    private Teacher teacher;


    public Student() {
    }

    public Student(int id, String name, Teacher teacher) {
        this.id = id;
        this.name = name;
        this.teacher = teacher;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", teacher=" + teacher +
                '}';
    }
}
```

**Teacher.java**

```java
public class Teacher implements Serializable {
    private static final long serialVersionUID = 1130876954L;
    private int id;
    private String name;

    public Teacher() {
    }

    public Teacher(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
```



#### 1）根据查询结果

```java
public interface StudentMapper {
    // 查询学生id,name,老师name
    List<Student> getStudentTeacher();
}
```

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.dao.StudentMapper">

    <select id="getStudentTeacher" resultMap="StudentTeacher">
        select s.id 'sid', s.name 'student', t.name 'teacher'
        from student s,teacher t
        where s.tid = t.id
    </select>
    <resultMap id="StudentTeacher" type="Student">
        <result column="sid" property="id"/>
        <result column="student" property="name"/>
        <!--association 处理单独的一个对象-->
        <!--javaType 是属性的类型-->
        <association property="teacher" javaType="Teacher">
            <result column="teacher" property="name"/>
        </association>
    </resultMap>

</mapper>
```

在Sutdent这个类中，有3个私有属性，类型分别是int，String，Teacher

在resultMap标签中，整个结果需要返回的类型是Student，基本类型和String都以`<result column="查询结果的字段名" property="javabean中的属性名"/>`来映射结果，而复杂类型Teacher类，需要使用association标签`<association property="javabean中的属性名" javaType="这个属性名的类型">` ，内标签依然使用result标签`<result column="查询结果的字段名" property="复杂类型中对应的属性名"/>` 来映射嵌套结果

```java
public class StudentMapperTest {
    @Test
    public void getStudentTeacherTest(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        List<Student> studentTeacher = mapper.getStudentTeacher();
        for (Student student : studentTeacher) {
            System.out.println(student);
        }
        sqlSession.close();
    }
}
```

输出结果：

```
Student{id=1, name='小明', teacher=Teacher{id=0, name='张老师'}}
Student{id=2, name='小红', teacher=Teacher{id=0, name='张老师'}}
Student{id=3, name='小张', teacher=Teacher{id=0, name='张老师'}}
Student{id=4, name='小李', teacher=Teacher{id=0, name='张老师'}}
Student{id=5, name='小王', teacher=Teacher{id=0, name='张老师'}}
Student{id=6, name='小侯', teacher=Teacher{id=0, name='张老师'}}
```



#### 2）根据查询过程

```java
public interface StudentMapper {
    // 查询学生id,name,老师name
    List<Student> getStudentTeacher2();
}
```

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.dao.StudentMapper">

    <select id="getStudentTeacher2" resultMap="StudentTeacher2">
        select * from student
    </select>
    <resultMap id="StudentTeacher2" type="Student">
        <!--column 对应表的字段名； property 对应javabean的属性名-->
        <result column="id" property="id"/>
        <result column="name" property="name"/>
        <!--association 处理单独的一个对象-->
        <!--javaType 是属性的类型-->
        <association column="tid" property="teacher" javaType="Teacher" select="getTeacher"/>
    </resultMap>
    <select id="getTeacher" resultType="Teacher">
        <!--通过student表的tid，查询到teacher表的所有信息，
			并映射到对应到Stuent类中teacher对象属性中-->
        select * from teacher where id = #{tid}
    </select>
    
</mapper>
```

```java
public class StudentMapperTest {
    @Test
    public void getStudentTeacherTest2(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        List<Student> studentTeacher = mapper.getStudentTeacher2();
        for (Student student : studentTeacher) {
            System.out.println(student);
        }
        sqlSession.close();
    }
}
```

输出结果：

```
Student{id=1, name='小明', teacher=Teacher{id=1, name='张老师'}}
Student{id=2, name='小红', teacher=Teacher{id=1, name='张老师'}}
Student{id=3, name='小张', teacher=Teacher{id=1, name='张老师'}}
Student{id=4, name='小李', teacher=Teacher{id=1, name='张老师'}}
Student{id=5, name='小王', teacher=Teacher{id=1, name='张老师'}}
Student{id=6, name='小侯', teacher=Teacher{id=1, name='张老师'}}
```



### 4、一对多 查询处理

> mybatis05工程

查询某个老师的所有学生

**Teacher.java**

```java
public class Teacher {
    private int id;
    private String name;
    private List<Student> students;

    public Teacher() {
    }

    public Teacher(int id, String name, List<Student> students) {
        this.id = id;
        this.name = name;
        this.students = students;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", students=" + students +
                '}';
    }
}
```

**Student.java**

```java
public class Student {
    private int id;
    private String name;
    private int tid;

    public Student() {
    }

    public Student(int id, String name, int tid) {
        this.id = id;
        this.name = name;
        this.tid = tid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", tid=" + tid +
                '}';
    }
}
```



#### 1）根据查询结果

```java
public interface TeacherMapper {
    // 查询某个老师的所有学生
    Teacher getTeacherStudents(int id);
}
```

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.dao.TeacherMapper">
    <select id="getTeacherStudents" parameterType="_int" resultMap="TeacherStudent">
        select s.id 'sid', s.name 'sname', s.tid 'ttid', t.name 'tname', t.id 'tid'
        from student s,teacher t
        where s.tid = t.id and s.tid = #{id}
    </select>
    <resultMap id="TeacherStudent" type="Teacher">
        <result column="tid" property="id"/>
        <result column="tname" property="name"/>
        <!--collection 是针对集合 ofType 是集合泛型的类型-->
        <collection property="students" ofType="Student" javaType="list">
            <result column="sid" property="id"/>
            <result column="sname" property="name"/>
            <result column="ttid" property="tid"/>
        </collection>
    </resultMap>
</mapper>
```

```java
public class TeacherMapperTest {
    @Test
    public void getTeacherStudentsTest(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        Teacher teacherStudents = sqlSession
                                .getMapper(TeacherMapper.class)
                                .getTeacherStudents(1);
        System.out.println(teacherStudents);
        sqlSession.close();
    }
}
```

**输出结果：**

```
Teacher{id=1, 
		name='张老师', 
		students=[Student{id=1, name='小明', tid=1}, 
				  Student{id=2, name='小红', tid=1}, 
				  Student{id=3, name='小张', tid=1}, 
				  Student{id=4, name='小李', tid=1}, 
				  Student{id=5, name='小王', tid=1}, 
				  Student{id=6, name='小侯', tid=1}]
		}
```



#### 2）根据查询过程

```java
public interface TeacherMapper {
    // 查询某个老师的所有学生
    Teacher getTeacherStudents(int id);
}
```

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.dao.TeacherMapper">
    <select id="getTeacherStudents" parameterType="_int" resultMap="TeacherStudent">
        select * from teacher
    </select>
    <resultMap id="TeacherStudent" type="Teacher">
        <id column="id" property="id"/>
        <!--如果是通过子查询，那么collection中多一个javaType属性，用于指定是哪个集合-->
        <collection property="students" javaType="ArrayList" ofType="Student" column="id" select="getStudent"/>
    </resultMap>
    <select id="getStudent" resultType="Student">
        select * from student where tid = #{id}
    </select>
</mapper>
```

```java
public class TeacherMapperTest {
    @Test
    public void getTeacherStudentsTest(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        Teacher teacherStudents = sqlSession
                                .getMapper(TeacherMapper.class)
                                .getTeacherStudents(1);
        System.out.println(teacherStudents);
        sqlSession.close();
    }
}
```

**输出结果：**

```
Teacher{id=1, 
		name='张老师', 
		students=[Student{id=1, name='小明', tid=1}, 
				  Student{id=2, name='小红', tid=1}, 
				  Student{id=3, name='小张', tid=1}, 
				  Student{id=4, name='小李', tid=1}, 
				  Student{id=5, name='小王', tid=1}, 
				  Student{id=6, name='小侯', tid=1}]
		}
```



### 小结

association 标签，当这个属性的类型为一个自定义类的类型时使用

collection 标签，当该属性的类型是一个集合时使用

javaType 属性，指定这个属性是什么类型

ofType 属性，指定集合中的泛型是什么类型



本小结只是以简单的例子提供一种解决思路，在实际开发过程中，不要使用慢SQL，尽量使用索引去查询表



## 十三、动态SQL

**动态SQL：根据不同的条件生成不同的SQL语句**

- if
- choose (when, otherwise)
- trim (where, set)
- foreach



**示例目录结构：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/动态sql项目结构.png" style="float:left;" />

### 1、创建表

```sql
CREATE TABLE `blog`(
  `id` VARCHAR(50) NOT NULL COMMENT '博客ID',
  `title` VARCHAR(100) NOT NULL COMMENT '博客标题',
  `author` VARCHAR(30) NOT NULL COMMENT '博客作者',
  `create_time` DATETIME NOT NULL COMMENT '创建时间',
  `views` INT(30) NOT NULL COMMENT '浏览量',
  PRIMARY KEY(`id`)
)ENGINE=INNODB DEFAULT CHARSET=utf8;
```



### 2、配置环境

db.properties

```properties
driver=com.mysql.cj.jdbc.Driver
url=jdbc:mysql://localhost:3306/mybatis?useSSL=true&useUnicode=true&characterEncoding=UTF-8
username=root
password=113011
```

mybatis-config.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <properties resource="db.properties"/>

    <settings>
        <setting name="logImpl" value="STDOUT_LOGGING"/>
        <!--是否开启驼峰命名自动映射，即从经典数据库列名 A_COLUMN 映射到经典 Java 属性名 aColumn 默认false-->
        <setting name="mapUnderscoreToCamelCase" value="true"/>
    </settings>

    <typeAliases>
        <typeAlias type="com.heroc.pojo.Blog" alias="Blog"/>
    </typeAliases>

    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC"></transactionManager>
            <dataSource type="POOLED">
                <property name="driver" value="${driver}"/>
                <property name="url" value="${url}"/>
                <property name="username" value="${username}"/>
                <property name="password" value="${password}"/>
            </dataSource>
        </environment>
    </environments>

    <mappers>
        <mapper resource="com/heroc/dao/BlogMapper.xml"/>
    </mappers>
</configuration>
```

MybatisUtils.java

```java
public class MybatisUtils {
    private static SqlSessionFactory sqlSessionFactory;
    static {
        try {
            InputStream resourceAsStream = Resources.getResourceAsStream("mybatis-config.xml");
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static SqlSession getSqlSession(){
        return sqlSessionFactory.openSession();
    }
}
```

IDUtils.java

```java
public class IDUtils {
    // 用于表中插入id，使用的uuid，UUID可以保证唯一性
    public static String getID(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }
}
```

Blog.java

```java
public class Blog implements Serializable {
    private static final long serialVersionUID = 1130876495L;
    private String id;
    private String title;
    private String author;
    private Date createTime;
    private int views;

    public Blog() {
    }

    public Blog(String id, String title, String author, Date createTime, int views) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.createTime = createTime;
        this.views = views;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    @Override
    public String toString() {
        return "Blog{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", createTime=" + createTime +
                ", views=" + views +
                '}';
    }
}
```



### 3、向表中插入信息

```java
public interface BlogMapper {
    // 向bolg表中插入值
    int insertBlog(Blog blog);
}
```

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.dao.BlogMapper">
    <insert id="insertBlog" parameterType="Blog">
        insert into blog
        values (#{id},#{title},#{author},#{createTime},#{views})
    </insert>
</mapper>
```

InsertBlogTest.java

```java
public class InsertBlogTest {
    @Test
    public void insertBlogTest(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);

        Blog blog = new Blog(IDUtils.getID(), "Mybatis如此简单", "heroC", new Date(), 6666);
        mapper.insertBlog(blog); // 插入第一条信息

        blog.setId(IDUtils.getID());
        blog.setTitle("Java如此简单");
        mapper.insertBlog(blog); // 插入第二条信息

        blog.setId(IDUtils.getID());
        blog.setTitle("Spring如此简单");
        blog.setViews(1000);
        mapper.insertBlog(blog); // 插入第三条信息

        blog.setId(IDUtils.getID());
        blog.setTitle("微服务如此简单");
        blog.setViews(30000);
        mapper.insertBlog(blog); // 插入第四条信息

        sqlSession.commit();
        sqlSession.close();
    }
}
```

插入结果：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/动态sql案列插入信息结果.png)



### 4、If

```java
public interface BlogMapper {
    // 通过if判断查询条件
    List<Blog> selectBlogIf(Map map);
}
```

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.dao.BlogMapper">
    <select id="selectBlogIf" parameterType="map" resultType="Blog">
        select * from blog
        <where>
            <if test="title != null">
                title = #{title}
            </if>
            <if test="views != null">
                and views = #{views}
            </if>
        </where>
    </select>
</mapper>
```

> *where* 元素只会在子元素返回至少一个条件的情况下才插入 “WHERE” 子句。而且，若子句的开头为 “AND” 或 “OR”，*where* 元素也会将它们去除，以保证一个正确SQL语句。

测试类：

```java
public class InsertBlogTest {
    @Test
    public void selectBlogIfTest(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
        Map<String, Object> map = new HashMap<>();
        map.put("title","Java如此简单");
        map.put("views",6666);
        List<Blog> blogs = mapper.selectBlogIf(map);
        for (Blog blog : blogs) {
            System.out.println(blog);
        }
        sqlSession.close();
    }
}
```

**输出结果：**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/if查询.png)



### 5、choose (when、otherwise)

有时候，我们不想使用所有的条件，而只是想从多个条件中选择一个使用。针对这种情况，MyBatis 提供了 choose 元素，它有点像 Java 中的 switch 语句。

```xml
<select id="selectBlogIf" parameterType="map" resultType="Blog">
    select * from blog
    <where>
        <choose>
            <when test="title != null">
                title = #{title}
            </when>
            <when test="views != null">
                and views = #{views}
            </when>
            <otherwise>
                author = "heroC"
            </otherwise>
        </choose>
    </where>
</select>
```

chooes 只走一个结果，when元素中从上之下，第一个满足条件就走第一个去执行，后面的都不执行，如果when元素都没有，就走otherwise元素的语句



### 6、trim (where, set)

**WHERE**

```xml
<select id="selectBlogIf" parameterType="map" resultType="Blog">
    select * from blog
    <where>
        <if test="title != null">
            title = #{title}
        </if>
        <if test="views != null">
            and views = #{views}
        </if>
    </where>
</select>
```

***where*** 元素只会在子元素返回至少一个条件的情况下才插入 “WHERE” 子句。而且，若子句的开头为 “AND” 或 “OR”，*where* 元素也会将它们去除，以保证一个正确SQL语句。

如果 *where* 元素与你期望的不太一样，你也可以通过自定义 trim 元素来定制 *where* 元素的功能。比如，和 *where* 元素等价的自定义 trim 元素为：

```xml
<trim prefix="WHERE" prefixOverrides="AND |OR ">
  ...
</trim>
```

*prefixOverrides* 属性会忽略通过管道符分隔的文本序列（注意此例中的空格是必要的）



**SET**

```xml
<update id="selectBlogIf" parameterType="map">
    update blog
    <set>
        <if test="title != null">
            title = #{title},
        </if>
        <if test="views != null">
            views = #{views}
        </if>
    </set>
    where id = #{id}
</update>
```

***set*** 元素会动态地在行首插入 SET 关键字，并会删掉额外的逗号

来看看与 *set* 元素等价的自定义 *trim* 元素吧：

```xml
<trim prefix="SET" suffixOverrides=",">
  ...
</trim>
```

注意，我们覆盖了后缀值设置，并且自定义了前缀值。



### 7、foreach

如以下的sql语句，

```sql
select * from blog where author = 'heroC' and (title='微服务如此简单' or title='Mybatis如此简单');
select * from blog where title in('微服务如此简单', 'Mybatis如此简单');
```

and关键字之后有多个条件要满足的情况

in关键字括号中多个条件需要满足的情况

这种就可以使用 foreach



**该案例使用的查询语句是：**

```sql
select * from blog where author = 'heroC' and (title='微服务如此简单' or title='Mybatis如此简单');
```



```java
public interface BlogMapper {
    // 通过foreach完成查询
    List<Blog> selectBlogForeach(Map map);
}
```

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.dao.BlogMapper">
    <select id="selectBlogForeach" parameterType="map" resultType="blog">
        select * from blog
        <where>
            <if test="author != null">
                author = #{author}
            </if>
            <foreach collection="titles" item="title" open="and (" separator="or" close=")">
                title = #{title}
            </foreach>
        </where>
    </select>
</mapper>
```

> foreach 的属性解释：
>
> ```xml
> <foreach collection="titles" item="title" open="and (" separator="or" close=")">
>     title = #{title}
> </foreach>
> ```
>
> collection 对应的是map集合中存储list集合的key值，通过map的key值，找到了value，需要遍历的list集合
>
> item 是给遍历出来的元素取得名字，用于sql语句的占位符拼接
>
> open 是语句的前封闭符
>
> separator 是设置每一个条件之间的分割符
>
> close 是语句的后封闭符
>
> foreach 元素中间就写需要拼接的sql语句
>
> ```sql
> -- 该foreach拼接的sql语句就是：
> and (title='微服务如此简单' or title='Mybatis如此简单')
> ```



**测试代码：**

```java
@Test
public void selectBlogForeachTest(){
    SqlSession sqlSession = MybatisUtils.getSqlSession();
    BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
    
    // 定义一个map，里面存储键值对元素
    Map map = new HashMap();
    
    // put一个author的key值
    map.put("author","heroC");

    ArrayList<String> titlesArray = new ArrayList<>();
    titlesArray.add("微服务如此简单");
    titlesArray.add("Mybatis如此简单");
    map.put("titles",titlesArray); // 这个put了一个list集合，list集合中存储了多条件查询的内容

    List<Blog> blogs = mapper.selectBlogForeach(map);
    for (Blog blog : blogs) {
        System.out.println(blog);
    }

    sqlSession.close();
}
```

**输出结果：**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/foreach结果.png)



### 8、SQL片段

SQL片段，用于将重复使用的查询语句提取出来，直接引用即可。

用`sql`元素封装，用`include`元素引用

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.dao.BlogMapper">

    <!--提取重复代码-->
    <sql id="if-blog-title-views">
        <if test="title != null">
            title = #{title}
        </if>
        <if test="views != null">
            and views = #{views}
        </if>
    </sql>
    <select id="selectBlogIf" parameterType="map" resultType="Blog">
        select * from blog
        <where>
            <!--引用重复代码-->
            <include refid="if-blog-title-views"></include>
        </where>
    </select>
</mapper>
```

> tips:
>
> 最好基于单表来定义SQL片段，增加可重用性



## 十四、缓存

由于每次查询都会去连接数据库，这样会消耗资源，效率也低。为了解决这个问题，就引入了缓存机制，它的作用就是将一次查询结果，给它暂时存储在内存中，等下一次再次查询的时候就可以直接走缓存获取结果，解决了高并发的系统性能问题。

> **==tips==**
>
> 1. **缓存是放在内存中的临时数据**
> 2. **缓存可以减少与数据库的交互次数，在一定程度上节约了时间，降低了系统开销，提高了效率**
> 3. **经常查询并且不经常改变的数据可以缓存**



### 1、Mybatis缓存

Mybatis默认定义了两级缓存

- 一级缓存：默认开启（sqlSession级别的缓存，也称为本地缓存）
- 二级缓存：需要手动开启和配置，基于namespace级别的缓存。为了提高扩展性，Mybatis提供了Cache接口，可通过实现该接口自定义二级缓存



#### 一级缓存

也是本地缓存，在于数据库会话的期间查询到的数据会放在本地缓存中。**从sqlSession开启到sqlSession关闭期间，该缓存都会一直存在**，查询相同数据直接会从本地缓存中获取，不会再次连接数据库。

```java
@Test
public void selectBlogForeachTest(){
    SqlSession sqlSession = MybatisUtils.getSqlSession();
       /*
        这中间的代码，查询的结果会本地缓存，如果sqlSession被关闭，缓存清除
        */
    sqlSession.close();
}
```



##### 一级缓存测试

```java
@Test
public void selectBlogIfTest(){
    SqlSession sqlSession = MybatisUtils.getSqlSession();
    BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
    Map<String, Object> map = new HashMap<>();
    map.put("title","Java如此简单");
    map.put("views",6666);
    
    List<Blog> blogs = mapper.selectBlogIf(map); // 第一次查询
    for (Blog blog : blogs) {
        System.out.println(blog);
    }
    System.out.println("=====================");
    
    List<Blog> blogss = mapper.selectBlogIf(map); // 第二次查询
    for (Blog blog : blogss) {
        System.out.println(blog);
    }
    sqlSession.close();
}
```

**输出结果：**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/一级缓存01.png)

根据结果可以知道，因为没有再次执行sql语句，所以第二次查询是从本地缓存中获取的结果



##### 一级缓存刷新测试

当执行查询之后，再执行更新操作，本地缓存会自动刷新，所以第二次查询会再次连接数据库获取结果

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForMybatis/一级缓存02.png" style="float:left;" />



**一级缓存被刷新的情况：**

- 映射语句文件中的所有 select 语句的结果将会被缓存。
- 映射语句文件中的所有 insert、update 和 delete 语句会刷新缓存。
- 缓存会使用最近最少使用算法（LRU, Least Recently Used）算法来清除不需要的缓存。



> tips
>
> ```java
> sqlSession.clearCache(); // 清除缓存
> ```



#### 二级缓存

这些属性可以通过 cache 元素的属性来修改（在对应的mapper中设置，它的作用域是namespace）。比如：

```xml
<cache
       eviction="FIFO"
       flushInterval="60000"
       size="512"
       readOnly="true"/>
```

***eviction*** 可用的清除策略有：

- `LRU` – 最近最少使用：移除最长时间不被使用的对象。
- `FIFO` – 先进先出：按对象进入缓存的顺序来移除它们。
- `SOFT` – 软引用：基于垃圾回收器状态和软引用规则移除对象。
- `WEAK` – 弱引用：更积极地基于垃圾收集器状态和弱引用规则移除对象。

默认的清除策略是 LRU。

***flushInterval***（刷新间隔）属性可以被设置为任意的正整数，设置的值应该是一个以毫秒为单位的合理时间量。 默认情况是不设置，也就是没有刷新间隔，缓存仅仅会在调用语句时刷新。

***size***（引用数目）属性可以被设置为任意正整数，要注意欲缓存对象的大小和运行环境中可用的内存资源。默认值是 1024。

***readOnly***（只读）属性可以被设置为 true 或 false。只读的缓存会给所有调用者返回缓存对象的相同实例。 因此这些对象不能被修改。这就提供了可观的性能提升。而可读写的缓存会（通过序列化）返回缓存对象的拷贝。 速度上会慢一些，但是更安全，因此默认值是 false。

***type*** （类型）属性可以设置使用哪个缓存驱动(常见的是redis、ehcache)

> `tips` 二级缓存是事务性的。这意味着，当 SqlSession 完成并提交时，或是完成并回滚，但没有执行 flushCache=true 的 insert/delete/update 语句时，缓存会获得更新。





##### 二级缓存的使用

mybatis-config.xml

```xml
<settings>
    <!--显示开启二级缓存-->
    <setting name="cacheEnabled" value="true"/>
</settings>
```

BlogMapper.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.dao.BlogMapper"> <!--作用域namespace-->
    <!--
	<cache/> 
	这种写法就是使用mybatis的默认缓存配置，javabean需要序列化实现Serializable接口
	-->
    <cache eviction="FIFO" flushInterval="60000" size="2048" readOnly="true"/>
    
    <!--useCache 用于配置该sql操作需不需要缓存-->
    <select id="selectBlogIf" parameterType="map" resultType="Blog" useCache="true">
        select * from blog
    </select>
</mapper>
```

当一级缓存关闭之后，会将一级缓存的缓存内容交给二级缓存，此时其他sqlSession会话就可以从二级缓存中获取结果。

> **tips**：java bean需要序列化！！！



### 2、缓存原理

sqlSession执行一个查询语句，

1. 先看二级缓存中有没有该结果的缓存，有则直接获取结果；
2. 没有则查看自己的一级缓存，有则直接获取结果；
3. 没有则连接数据库，查询结果。



## 十五、注解

简单增删改查可以通过注解的形式实现，不用写mapper.xml文件，优点是==解耦==。对于复杂的sql语句，java力不从心。





## 十六、编写脱敏插件

### 1. 前言

在日常开发中，身份证号、手机号、卡号、客户号等个人信息都需要进行数据脱敏。否则容易造成个人隐私泄露，客户资料泄露，给不法分子可乘之机。但是数据脱敏不是把敏感信息隐藏起来，而是看起来像真的一样，实际上不能是真的。我以前的公司就因为不重视脱敏，一名员工在离职的时候通过后台的导出功能导出了核心的客户资料卖给了竞品，给公司造成了重大的损失。当然这里有数据管理的原因，但是脱敏仍旧是不可忽略的一环，脱敏可以从一定程度上保证数据的合规使用。下面就是一份经过脱敏的数据：

![脱敏之后的数据](https://img2020.cnblogs.com/other/1739473/202008/1739473-20200811095651549-1758778114.png)

### 2. Mybatis 脱敏插件

最近在研究**Mybatis**的插件，所以考虑能不能在**ORM**中搞一搞脱敏，所以就尝试了一下，这里分享一下思路。借此也分享一下**Mybatis**插件开发的思路。

#### 2.1 Mybatis 插件接口

**Mybatis**中使用插件，需要实现接口`org.apache.ibatis.plugin.Interceptor`，如下所示：

```java
public interface Interceptor {

  Object intercept(Invocation invocation) throws Throwable;

  default Object plugin(Object target) {
    return Plugin.wrap(target, this);
  }

  default void setProperties(Properties properties) {
    // NOP
  }

}
```

这里其实最核心的是`Object intercept(Invocation invocation)`方法，这是我们需要实现的方法。

#### 2.2 Invocation 对象

那么核心方法中的`Invocation`是个什么概念呢？

```java
public class Invocation {

  private final Object target;
  private final Method method;
  private final Object[] args;

  public Invocation(Object target, Method method, Object[] args) {
    this.target = target;
    this.method = method;
    this.args = args;
  }

  public Object getTarget() {
    return target;
  }

  public Method getMethod() {
    return method;
  }

  public Object[] getArgs() {
    return args;
  }

  public Object proceed() throws InvocationTargetException, IllegalAccessException {
    return method.invoke(target, args);
  }

}
```

这个东西包含了四个概念：

- **target** 拦截的对象
- **method** 拦截**target**中的具体方法，也就是说**Mybatis**插件的粒度是精确到方法级别的。
- **args** 拦截到的参数。
- **proceed** 执行被拦截到的方法，你可以在执行的前后做一些事情。

#### 2.3 拦截签名

既然我们知道了**Mybatis**插件的粒度是精确到方法级别的，那么疑问来了，插件如何知道轮到它工作了呢？

所以**Mybatis**设计了签名机制来解决这个问题，通过在插件接口上使用注解`@Intercepts`标注来解决这个问题。

```java
@Intercepts(@Signature(type = ResultSetHandler.class,
        method = "handleResultSets",
        args = {Statement.class}))
```

就像上面一样，事实上就等于配置了一个`Invocation`。

#### 2.4 插件的作用域

那么问题又来了，**Mybatis**插件能拦截哪些对象,或者说插件能在哪个生命周期阶段起作用呢？它可以拦截以下四大对象：

- **Executor** 是**SQL**执行器，包含了组装参数，组装结果集到返回值以及执行**SQL**的过程，粒度比较粗。
- **StatementHandler** 用来处理SQL的执行过程，我们可以在这里重写**SQL**非常常用。
- **ParameterHandler** 用来处理传入**SQL**的参数，我们可以重写参数的处理规则。
- **ResultSetHandler** 用于处理结果集，我们可以重写结果集的组装规则。

你需要做的就是明确的你的业务需要在上面四个对象的哪个处理阶段拦截处理即可。

#### 2.5 MetaObject

**Mybatis**提供了一个工具类`org.apache.ibatis.reflection.MetaObject`。它通过反射来读取和修改一些重要对象的属性。我们可以利用它来处理四大对象的一些属性，这是**Mybatis**插件开发的一个常用工具类。

- **Object getValue(String name)** 根据名称获取对象的属性值，支持**OGNL**表达式。
- **void setValue(String name, Object value)** 设置某个属性的值。
- **Class getSetterType(String name)** 获取**setter**方法的入参类型。
- **Class getGetterType(String name)** 获取**getter**方法的返回值类型。

通常我们使用`SystemMetaObject.forObject(Object object)`来实例化`MetaObject`对象。你会在接下来的实战DEMO中看到我使用它。

### 3. Mybatis 脱敏插件实战

接下来我就把开头的脱敏需求实现一下。首先需要对脱敏字段进行标记并确定使用的脱敏策略。

编写脱敏函数：

```java
/**
 * 具体策略的函数
 * @author felord.cn
 * @since 11:24
 **/
public interface Desensitizer  extends Function<String,String>  {

}
```

编写脱敏策略枚举：

```java
/**
 * 脱敏策略.
 *
 * @author felord.cn
 * @since 11 :25
 */
public enum SensitiveStrategy {
    /**
     * Username sensitive strategy.
     */
    USERNAME(s -> s.replaceAll("(\\S)\\S(\\S*)", "$1*$2")),
    /**
     * Id card sensitive type.
     */
    ID_CARD(s -> s.replaceAll("(\\d{4})\\d{10}(\\w{4})", "$1****$2")),
    /**
     * Phone sensitive type.
     */
    PHONE(s -> s.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2")),

    /**
     * Address sensitive type.
     */
    ADDRESS(s -> s.replaceAll("(\\S{8})\\S{4}(\\S*)\\S{4}", "$1****$2****"));


    private final Desensitizer desensitizer;

    SensitiveStrategy(Desensitizer desensitizer) {
        this.desensitizer = desensitizer;
    }

    /**
     * Gets desensitizer.
     *
     * @return the desensitizer
     */
    public Desensitizer getDesensitizer() {
        return desensitizer;
    }
}
```

编写脱敏字段的标记注解：

```java
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Sensitive {
    SensitiveStrategy strategy();
}
```

我们的返回对象中如果某个字段需要脱敏，只需要通过标记就可以了。例如下面这样：

```java
@Data
public class UserInfo {

    private static final long serialVersionUID = -8938650956516110149L;
    private Long userId;
    @Sensitive(strategy = SensitiveStrategy.USERNAME)
    private String name;
    private Integer age;
}
```

然后就是编写插件了，我可以确定的是需要拦截的是`ResultSetHandler`对象的`handleResultSets`方法，我们只需要实现插件接口`Interceptor`并添加签名就可以了。全部逻辑如下：

```java
@Slf4j
@Intercepts(@Signature(type = ResultSetHandler.class,
        method = "handleResultSets",
        args = {Statement.class}))
public class SensitivePlugin implements Interceptor {
    @SuppressWarnings("unchecked")
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        List<Object> records = (List<Object>) invocation.proceed();
        // 对结果集脱敏
        records.forEach(this::sensitive);
        return records;
    }


    private void sensitive(Object source) {
        // 拿到返回值类型
        Class<?> sourceClass = source.getClass();
        // 初始化返回值类型的 MetaObject
        MetaObject metaObject = SystemMetaObject.forObject(source);
        // 捕捉到属性上的标记注解 @Sensitive 并进行对应的脱敏处理
        Stream.of(sourceClass.getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(Sensitive.class))
                .forEach(field -> doSensitive(metaObject, field));
    }


    private void doSensitive(MetaObject metaObject, Field field) {
        // 拿到属性名
        String name = field.getName();
        // 获取属性值
        Object value = metaObject.getValue(name);
        // 只有字符串类型才能脱敏  而且不能为null
        if (String.class == metaObject.getGetterType(name) && value != null) {
            Sensitive annotation = field.getAnnotation(Sensitive.class);
            // 获取对应的脱敏策略 并进行脱敏
            SensitiveStrategy type = annotation.strategy();
            Object o = type.getDesensitizer().apply((String) value);
            // 把脱敏后的值塞回去
            metaObject.setValue(name, o);
        }
    }
}
```

然后配置脱敏插件使之生效：

```java
@Bean
public SensitivePlugin sensitivePlugin(){
    return new SensitivePlugin();
}
```

操作查询获得结果 `UserInfo(userId=123123, name=李*龙, age=28)` ，成功将指定字段进行了脱敏。

> 补充一句，其实脱敏也可以在**JSON**序列化的时候进行。

### 4. 总结

今天对编写**Mybatis**插件的一些要点进行了说明，同时根据说明实现了一个脱敏插件。但是请注意一定要熟悉四大对象的生命周期，否则自写插件可能会造成意想不到的结果。