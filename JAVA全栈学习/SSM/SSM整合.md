# SSM 整合



## 一、环境要求

- IDEA
- MySQL 8.0 或 MySQL 5.0+
- Tomcat 8.5.51
- Maven 3.6.3



## 二、数据库环境

```sql
CREATE DATABASE IF NOT EXISTS `ssmbuild`;

CREATE TABLE `books`(
  `bookID` INT(10) NOT NULL AUTO_INCREMENT COMMENT '书ID',
  `bookName` VARCHAR(100) NOT NULL COMMENT '书名',
  `bookCounts` INT(11) NOT NULL COMMENT '数量',
  `detail` VARCHAR(200) NOT NULL COMMENT '描述',
  PRIMARY KEY (`bookID`)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

DESC `books`;

INSERT INTO `books` VALUES
(1,'Java',1,'从入门到放弃'),
(2,'MySQL',10,'从删库到跑路'),
(3,'Linux',5,'从进门到出门');

SELECT * FROM `books`;
```



## 三、基本环境的搭建

1. 建立一个Maven项目，添加web支持，导入相关pom依赖

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.example</groupId>
    <artifactId>ssmbuild</artifactId>
    <version>1.0-SNAPSHOT</version>

    <dependencies>
        <!--junit测试-->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <!--作用域是test，只能在test属性的包下使用 不写作用域则任何包下都可以使用-->
            <scope>test</scope>
        </dependency>

        <!--mysql连接驱动  连接池c3p0-->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.15</version>
        </dependency>
        <dependency>
            <groupId>com.mchange</groupId>
            <artifactId>c3p0</artifactId>
            <version>0.9.5.2</version>
        </dependency>

        <!--servlet JSP-->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>3.1.0</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet.jsp</groupId>
            <artifactId>jsp-api</artifactId>
            <version>2.2</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
            <version>1.2</version>
        </dependency>

        <!--mybatis-->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.5.0</version>
        </dependency>
        <!--mybatis-spring-->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-spring</artifactId>
            <version>2.0.0</version>
        </dependency>

        <!--spring-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>5.2.0.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>5.2.0.RELEASE</version>
        </dependency>
        
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjweaver</artifactId>
            <version>1.9.4</version>
		</dependency>
    </dependencies>

    <!--避免有些xml不打包到运行目录中而导致报错-->
    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>true</filtering>
            </resource>

            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>true</filtering>
            </resource>
        </resources>
    </build>

</project>
```



2. idea连接数据库

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringSSM/连接数据库.png" style="float:left;" />

3. 建立基本结构和配置框架

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringSSM/基本结构.png" style="float:left;" />

   **mybatis-config.xml**

   ```xml
   <?xml version="1.0" encoding="UTF-8" ?>
   <!DOCTYPE configuration
           PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
           "http://mybatis.org/dtd/mybatis-3-config.dtd">
   <configuration>
   
   </configuration>
   ```

   **applicationContext.xml**

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns:p="http://www.springframework.org/schema/p"
          xmlns:c="http://www.springframework.org/schema/c"
          xmlns:tx="http://www.springframework.org/schema/tx"
          xmlns:aop="http://www.springframework.org/schema/aop"
          xmlns:context="http://www.springframework.org/schema/context"
          xsi:schemaLocation="http://www.springframework.org/schema/beans
   			https://www.springframework.org/schema/beans/spring-beans.xsd
   			http://www.springframework.org/schema/tx
   			https://www.springframework.org/schema/tx/spring-tx.xsd
   			http://www.springframework.org/schema/aop
   			https://www.springframework.org/schema/aop/spring-aop.xsd
   			http://www.springframework.org/schema/context
   			https://www.springframework.org/schema/context/spring-context.xsd">
       
   
   </beans>
   ```



## 四、Mybatis相关搭建

1. 数据库配置文件 **database.properties**

   ```properties
   jdbc.driver=com.mysql.cj.jdbc.Driver
   # Mysql8.0+ 数据库没有设置时区时，连接时还需要加一个 serverTimezone=Asia/Shanghai
   jdbc.url=jdbc:mysql://localhost:3306/ssmbuild?useSSL=true&useUnicode=true&characterEncoding=UTF-8
   jdbc.username=root
   jdbc.password=113011
   ```

2. 建立一个实体类 **Books.java**

   ```java
   public class Books implements Serializable {
       private static final long serialVersionUID = 1130202004101806L;
       private int bookID;
       private String bookName;
       private int bookCounts;
       private String detail;
   
       public Books() {
       }
   
       public Books(int bookID, String bookName, int bookCounts, String detail) {
           this.bookID = bookID;
           this.bookName = bookName;
           this.bookCounts = bookCounts;
           this.detail = detail;
       }
   
       public int getBookID() {
           return bookID;
       }
   
       public void setBookID(int bookID) {
           this.bookID = bookID;
       }
   
       public String getBookName() {
           return bookName;
       }
   
       public void setBookName(String bookName) {
           this.bookName = bookName;
       }
   
       public int getBookCounts() {
           return bookCounts;
       }
   
       public void setBookCounts(int bookCounts) {
           this.bookCounts = bookCounts;
       }
   
       public String getDetail() {
           return detail;
       }
   
       public void setDetail(String detail) {
           this.detail = detail;
       }
   
       @Override
       public String toString() {
           return "Books{" +
                   "bookID=" + bookID +
                   ", bookName='" + bookName + '\'' +
                   ", bookCounts=" + bookCounts +
                   ", detail='" + detail + '\'' +
                   '}';
       }
   }
   ```

3. 写一个Books接口，dao层：**BookMapper.java**

   ```java
   public interface BookMapper {
       // 增加一本书
       int addBook(Books book);
   
       // 删除一本数
       int deleteBookByID(@Param("bookID") int bookID);
   
       // 更新一本书
       int updateBook(Books book);
   
       // 查询一本书
       Books selectBookByID(@Param("bookID")int bookID);
   
       // 查询全部书记录
       List<Books> selectBooks();
   }
   ```

   接着写一个**BookMapper.xml**

   ```xml
   <?xml version="1.0" encoding="UTF-8" ?>
   <!DOCTYPE mapper
           PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
           "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
   <mapper namespace="com.heroc.dao.BookMapper">
       
       <insert id="addBook" parameterType="books">
           insert into `books`(bookName,bookCounts,detail)
           values (#{bookName},#{bookCounts},#{detail})
       </insert>
       
       <delete id="deleteBookByID" parameterType="_int">
           delete form `books`
           where bookID = #{bookID}
       </delete>
       
       <update id="updateBook" parameterType="books">
           update `books`
           set bookName = #{bookName}, bookCounts = #{bookCounts}, detail = #{detail}
           where bookID = #{bookID}
       </update>
       
       <select id="selectBookByID" parameterType="_int" resultType="books">
           select * from `books`
           where bookID = #{bookID}
       </select>
   
       <select id="selectBooks" resultType="books">
           select * from `books`
       </select>
       
   </mapper>
   ```

4. 紧接着，在**mybatis-config.xml**中将mapper注册到配置文件中

   ```xml
   <?xml version="1.0" encoding="UTF-8" ?>
   <!DOCTYPE configuration
           PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
           "http://mybatis.org/dtd/mybatis-3-config.dtd">
   <configuration>
   
       <!--mybatis连接池已交给spring配置-->
   
       <typeAliases>
           <package name="com.heroc.pojo"/>
       </typeAliases>
   
       <!--配置mapper-->
       <mappers>
           <mapper resource="com/heroc/dao/BookMapper.xml"/>
       </mappers>
   
   </configuration>
   ```

5. 写service层，用于业务处理。**BookService.java**

   ```java
   public interface BookService {
       // 增加一本书
       int addBook(Books book);
   
       // 删除一本数
       int deleteBookByID(int bookID);
   
       // 更新一本书
       int updateBook(Books book);
   
       // 查询一本书
       Books selectBookByID(int bookID);
   
       // 查询全部书记录
       List<Books> selectBooks();
   
       // List<Books> selectBooks(@Param("bookID")int bookID);
   }
   ```

   紧接着写实现类，**BookServiceImpl.java**

   ```java
   public class BookServiceImpl implements BookService {
       // service层调dao层
       private BookMapper bookMapper;
       // 创建set方法，方便由spring的IOC容器管理
       public void setBookMapper(BookMapper bookMapper) {
           this.bookMapper = bookMapper;
       }
   
       @Override
       public int addBook(Books book) {
           return bookMapper.addBook(book);
       }
   
       @Override
       public int deleteBookByID(int bookID) {
           return bookMapper.deleteBookByID(bookID);
       }
   
       @Override
       public int updateBook(Books book) {
           return bookMapper.updateBook(book);
       }
   
       @Override
       public Books selectBookByID(int bookID) {
           return bookMapper.selectBookByID(bookID);
       }
   
       @Override
       public List<Books> selectBooks() {
           return bookMapper.selectBooks();
       }
   }
   ```



**创建完之后的目录结构：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringSSM/mybatis.png" style="float:left;" />



## 五、Spring相关搭建

1. **spring-dao.xml** 对数据持久化mybatis数据库的一些配置

   - 关联连接数据库的配置文件
   - 配置数据源
   - sqlSessionFactory配置
   - 自动扫描mapper类，dao层
   
```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns:context="http://www.springframework.org/schema/context"
          xsi:schemaLocation="http://www.springframework.org/schema/beans
   			https://www.springframework.org/schema/beans/spring-beans.xsd
   			http://www.springframework.org/schema/context
   			https://www.springframework.org/schema/context/spring-context.xsd">
   
       <!--1、关联数据库配置文件-->
       <context:property-placeholder location="classpath:database.properties"/>
   
       <!--2、连接池，这里使用的c3p0数据源-->
       <bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource">
           <property name="driverClass" value="${jdbc.driver}"/>
           <property name="jdbcUrl" value="${jdbc.url}"/>
           <property name="user" value="${jdbc.username}"/>
           <property name="password" value="${jdbc.password}"/>
           <property name="maxPoolSize" value="30"/>
           <property name="minPoolSize" value="10"/>
           <!--不自动提交commit-->
           <property name="autoCommitOnClose" value="false"/>
           <!--获取连接超时时间-->
           <property name="checkoutTimeout" value="10000"/>
           <!--获取失败，尝试连接次数-->
           <property name="acquireRetryAttempts" value="2"/>
       </bean>
   
       <!--3、sqlSessionFactory-->
       <bean class="org.mybatis.spring.SqlSessionFactoryBean" id="sqlSessionFactory">
           <property name="dataSource" ref="dataSource"/>
           <!--配置mapper-->
           <property name="mapperLocations" value="classpath:com/heroc/dao/*.xml"/>
           <!--绑定mybatis-config.xml-->
           <property name="configLocation" value="classpath:mybatis-config.xml"/>
       </bean>
       <!--注册sqlSession-->
       <!--<bean class="org.mybatis.spring.SqlSessionTemplate" id="sqlSession">
           <constructor-arg index="0" ref="sqlSessionFactory"/>
       </bean>-->
   
       <!--4、配置dao层的接口扫描包，将接口自动注册到IOC容器中-->
       <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer" id="mapperScannerConfigurer">
           <!--注入sqlSessionFactory-->
           <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
           <!--要扫描的dao包-->
           <property name="basePackage" value="com.heroc.dao"/>
       </bean>
   
   </beans>
```

2. **spring-service.xml** 对service层的相关进行配置

   - 注册所有业务类
- 对service层开启事务
  
   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns:context="http://www.springframework.org/schema/context"
          xsi:schemaLocation="http://www.springframework.org/schema/beans
   			https://www.springframework.org/schema/beans/spring-beans.xsd
   			http://www.springframework.org/schema/context
   			https://www.springframework.org/schema/context/spring-context.xsd">
   
       <!--1、扫描Service层使用了注解的类，将其注入到IOC容器中-->
       <context:component-scan base-package="com.heroc.service"/>
   
       <!--2、注册所有业务类-->
       <bean class="com.heroc.service.BookServiceImpl" id="bookService">
           <!--由于在spring-dao.xml实现了自动扫描mapper接口，dao层的接口自动注册到IOC容器中，
           又因为这两个配置文件结构目录在一个文件夹下，所以可以直接引用spring-dao.xml配置文件
           注册的bean；如果不在一个文件目录下，可通过import标签导入spring-dao.xml配置文件-->
           <property name="bookMapper" ref="bookMapper"/>
       </bean>
       
        <!--3、声明式事务配置，这样配置开启事务，所有方法都会被纳入事务管理-->
       <bean class="org.springframework.jdbc.datasource.DataSourceTransactionManager" id="transactionManager">
           <property name="dataSource" ref="dataSource"/>
       </bean>
   
       <!--4、可以继续使用aop进行对指定方法开启事务支持-->
       <tx:advice id="transaction" transaction-manager="transactionManager">
           <tx:attributes>
               <tx:method name="*" propagation="REQUIRED"/>
           </tx:attributes>
       </tx:advice>
       <aop:config>
           <!--对service层开启事务管理-->
           <aop:pointcut id="transactionPointcut" expression="execution(* com.heroc.service.*.*(..))"/>
           <aop:advisor advice-ref="transaction" pointcut-ref="transactionPointcut"/>
       </aop:config>
   
   </beans>
   ```



## 六、Spring MVC相关搭建

1. **web.xml** 中配置DispatcherServlet和乱码过滤

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
            version="4.0">
   
       <!--注册DispatcherServlet-->
       <servlet>
           <servlet-name>dispatcherServlet</servlet-name>
           <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
           <init-param>
               <param-name>contextConfigLocation</param-name>
               <param-value>classpath:applicationContext.xml</param-value>
           </init-param>
           <load-on-startup>1</load-on-startup>
       </servlet>
       <servlet-mapping>
           <servlet-name>dispatcherServlet</servlet-name>
           <url-pattern>/</url-pattern>
       </servlet-mapping>
   
       <!--mvc过滤器，解决乱码-->
       <filter>
           <filter-name>encoding</filter-name>
           <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
           <init-param>
               <param-name>encoding</param-name>
               <param-value>utf-8</param-value>
           </init-param>
       </filter>
       <filter-mapping>
           <filter-name>encoding</filter-name>
           <url-pattern>/*</url-pattern>
       </filter-mapping>
       
       <!--session过期时间为15分钟-->
       <session-config>
           <session-timeout>15</session-timeout>
       </session-config>
   
   </web-app>
   ```



2. **spring-mvc.xml** 配置mvc

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns:context="http://www.springframework.org/schema/context"
          xmlns:mvc="http://www.springframework.org/schema/mvc"
          xsi:schemaLocation="http://www.springframework.org/schema/beans
   			https://www.springframework.org/schema/beans/spring-beans.xsd
   			http://www.springframework.org/schema/context
   			https://www.springframework.org/schema/context/spring-context.xsd http://www.springframework.org/schema/mvc https://www.springframework.org/schema/mvc/spring-mvc.xsd">
   
       <!--扫描包-->
       <context:component-scan base-package="com.heroc.controller"/>
       <!--不处理静态文件-->
       <mvc:default-servlet-handler/>
       <!--自动注册映射器，适配器-->
       <mvc:annotation-driven/>
   
       <!--视图解析器-->
       <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver" id="internalResourceViewResolver">
           <property name="prefix" value="/jsp/"/>
           <property name="suffix" value=".jsp"/>
       </bean>
   
   </beans>
   ```

   

**目前目录结构图：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringSSM/springmvc目录结构.png" style="float:left;" />



## 七、查询所有书籍信息并显示在页面

1. 在controller层，**BookController.java**

   ```java
   @Controller
   @RequestMapping("/books")
   public class BookController {
   
       private BookService bookService;
   
       // 自动注入到IOC容器中，在spring4.0及以上推荐在set方法上自动注入和构造器上自动注入
       @Autowired
       @Qualifier("bookServiceImpl")
       public void setBookService(BookService bookService) {
           this.bookService = bookService;
       }
   
       @GetMapping("/allbooks")
       public String allBooks(Model model){
           // 调用service层，service层再调用dao层，dao层从数据库查询数据并返回
           List<Books> books = bookService.selectBooks();
           // 将数据信息，添加到模型中，进行视图解析
           model.addAttribute("books",books);
           // 返回需要跳转的页面
           return "allBooks";
       }
   }
   ```

2. **index.jsp**页面的编写

   ```jsp
   <%@ page contentType="text/html;charset=UTF-8" language="java" %>
   <html>
     <head>
       <title>首页</title>
       <style>
         a{
           text-decoration: none;
           color: black;
         }
         div{
           width: 200px;
           height: 50px;
           line-height: 50px;
           background-color: cornflowerblue;
           border-radius: 5px;
           margin: 200px auto;
           text-align: center;
         }
       </style>
     </head>
     <body>
     <div>
       <h3><a href="${pageContext.request.contextPath}/books/allbooks">进入书籍页面</a></h3>
     </div>
     </body>
   </html>
   ```

3. **allBooks.jsp**的编写

   ```jsp
   <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
   <%@ page contentType="text/html;charset=UTF-8" language="java" %>
   <html>
   <head>
       <title>展示全部书籍</title>
       <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
       <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
       <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
   </head>
   <body>
   <div class="container">
       <div class="row clearfix">
           <div class="row-md-12">
               <h1 class="page-header">
                   <small>书籍列表 —— 显示所有书籍</small>
               </h1>
           </div>
       </div>
       <div class="row clearfix" style="margin-top: 20px">
           <div class="column col-md-12">
               <table class="table table-hover">
                   <thead>
                   <tr>
                       <th>ID</th>
                       <th>书名</th>
                       <th>数量</th>
                       <th>描述</th>
                   </tr>
                   </thead>
                   <tbody>
                   <%--${books} 从model中获取的数据--%>
                   <c:forEach var="book" items="${books}">
                       <tr>
                           <td>${book.bookID}</td>
                           <td>${book.bookName}</td>
                           <td>${book.bookCounts}</td>
                           <td>${book.detail}</td>
                       </tr>
                   </c:forEach>
                   </tbody>
               </table>
           </div>
       </div>
   </div>
   </body>
   </html>
   ```

4. 结果：

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringSSM/首页.png" style="float:left;" />

   <img src=".\pictureForSSM\全部书籍展示.png" style="float:left;" />



**目前目录结构图：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringSSM/controller01.png" style="float:left;" />



## 八、添加新书籍

1. 在**allBooks.jps**页面中新增一个[新增书籍按钮]

   ```jsp
   <div style="height: 20px">
       <a href="${pageContext.request.contextPath}/books/addbook">新增书籍</a>
   </div>
   ```

2. 写一个**addBook.jsp**页面，新增书籍的表单

   ```jsp
   <%@ page contentType="text/html;charset=UTF-8" language="java" %>
   <html>
   <head>
       <title>展示全部书籍</title>
       <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
       <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
       <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
   </head>
   <style>
       a{
           text-decoration: none;
           font-size: 15px;
       }
       #page-info>a:hover{
           text-decoration: none;
           font-size: 16px;
           color: cornflowerblue;
       }
   </style>
   <body>
   <div class="container">
       <div class="row clearfix">
           <div class="row-md-12">
               <h1 class="page-header">
                   <small>书籍列表 —— 新增书籍</small>
               </h1>
           </div>
       </div>
       <div id="page-info" style="height: 20px">
           <a href="${pageContext.request.contextPath}/books/allbooks">返回所有书籍页面</a>
       </div>
       <div class="row clearfix" style="margin-top: 20px">
           <div class="row-md-4 col-md-offset-4">
               <form action="${pageContext.request.contextPath}/books/addbook" method="post">
                   <div class="form-group" style="width: 400px">
                       <label for="name">书名</label>
                       <input type="text" id="name" name="bookName" class="form-control" placeholder="请输入书名" required>
                       <label for="counts">数量</label>
                       <input type="number" id="counts" name="bookCounts" class="form-control" placeholder="请输入数量" required>
                       <label for="detail">描述</label>
                       <input type="text" id="detail" name="detail" class="form-control" placeholder="请输入描述" required>
                   </div>
                   <div style="padding-left: 180px">
                       <button class="btn btn-primary" type="submit">提交</button>
                   </div>
               </form>
           </div>
       </div>
   </div>
   </body>
   </html>
   ```

3. 在**BookController.java**中新增两个处理请求的处理方法

   ```java
   // 跳转新增书籍页面
   @GetMapping("/addbook")
   public String addBookPage(){
       return "addBook";
   }
   
   // 新增书籍
   @PostMapping("/addbook")
   public String addBook(@NotNull Books book,Model model){
       bookService.addBook(book);
       return "redirect:/books/allbooks";  // 重定向
   }
   ```



**实现结果：**

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringSSM/controller03.png" style="float:left;" />
点击提交按钮，返回到页面，展示所有书籍信息，既可以查看到刚添加的数据
<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringSSM/controller02.png" style="float:left;" />















## 九、修改删除书籍

1. 修改一下**allBook.jsp**的表单

   ```jsp
   <div class="row clearfix" style="margin-top: 20px">
           <div class="column col-md-12">
               <table class="table table-hover">
                   <thead>
                   <tr>
                       <th>ID</th>
                       <th>书名</th>
                       <th>数量</th>
                       <th>描述</th>
                       <th>操作</th>
                   </tr>
                   </thead>
                   <tbody>
                   <%--${books} 从model中获取的数据--%>
                   <c:forEach var="book" items="${books}">
                       <tr>
                           <td>${book.bookID}</td>
                           <td>${book.bookName}</td>
                           <td>${book.bookCounts}</td>
                           <td>${book.detail}</td>
                           <td>
                               <a href="${pageContext.request.contextPath}/books/updatebook/${book.bookID}">修改</a>
                               &nbsp; | &nbsp;
                               <a href="${pageContext.request.contextPath}/books/deletebook/${book.bookID}">删除</a>
                           </td>
                       </tr>
                   </c:forEach>
                   </tbody>
               </table>
           </div>
       </div>
   ```

2. 在**BookController.jsp**添加修改修改操作

   ```java
   // 跳转修改书籍页面
   @GetMapping("/updatebook/{bookID}")
   public String updateBookPage(@PathVariable("bookID")int bookID, Model model){
       Books book = bookService.selectBookByID(bookID);
       model.addAttribute("book",book);
       return "updateBook";
   }
   // 修改书籍
   @PostMapping("/updatebook")
   public String updateBook(@NotNull Books book){
       bookService.updateBook(book);
       return "redirect:/books/allbooks"; // 返回首页
   }
   
   // 删除书籍
   @GetMapping("/deletebook/{bookID}")
   public String deleteBook(@PathVariable("bookID")int bookID){
       bookService.deleteBookByID(bookID);
       return "redirect:/books/allbooks";
   }
   ```

3. 添加一个**updateBook.jsp**的页面

   ```jsp
   <%@ page contentType="text/html;charset=UTF-8" language="java" %>
   <html>
   <head>
       <title>修改书籍</title>
       <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
       <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
       <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
   </head>
   <style>
       a{
           text-decoration: none;
           font-size: 15px;
       }
       #page-info>a:hover{
           text-decoration: none;
           font-size: 16px;
           color: cornflowerblue;
       }
   </style>
   <body>
   <div class="container">
       <div class="row clearfix">
           <div class="row-md-12">
               <h1 class="page-header">
                   <small>书籍列表 —— 修改书籍</small>
               </h1>
           </div>
       </div>
       <div id="page-info" style="height: 20px">
           <a href="${pageContext.request.contextPath}/books/allbooks">返回所有书籍页面</a>
       </div>
       <div class="row clearfix" style="margin-top: 20px">
           <div class="row-md-4 col-md-offset-4">
               <form action="${pageContext.request.contextPath}/books/updatebook" method="post">
                   <input type="hidden" value="${book.bookID}" name="bookID">
                   <div class="form-group" style="width: 400px">
                       <label for="name">书名</label>
                       <input type="text" id="name" name="bookName" value="${book.bookName}" class="form-control" required>
                       <label for="counts">数量</label>
                       <input type="number" id="counts" name="bookCounts" value="${book.bookCounts}" class="form-control" required>
                       <label for="detail">描述</label>
                       <input type="text" id="detail" name="detail" value="${book.detail}" class="form-control" required>
                   </div>
                   <div style="padding-left: 180px">
                       <button class="btn btn-primary" type="submit">提交</button>
                   </div>
               </form>
           </div>
       </div>
   </div>
   </body>
   </html>
   ```



## 十、整合thymeleaf

spring-mvc.xml

```xml
<!-- 使用thymeleaf解析 -->
<bean id="templateResolver" class="org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver">
    <property name="prefix" value="/"/>
    <property name="suffix" value=".html"/>
    <property name="templateMode" value="HTML"/>
    <property name="cacheable" value="false"/>
    <property name="characterEncoding" value="UTF-8"/>
</bean>

<bean id="templateEngine" class="org.thymeleaf.spring4.SpringTemplateEngine">
    <property name="templateResolver" ref="templateResolver"/>
</bean>

<bean id="viewResolver" class="org.thymeleaf.spring4.view.ThymeleafViewResolver">
    <property name="templateEngine" ref="templateEngine"/>
    <property name="characterEncoding" value="UTF-8"/>
</bean>
```



```xml
<dependency>
    <groupId>org.thymeleaf</groupId>
    <artifactId>thymeleaf-spring4</artifactId>
    <version>3.0.9.RELEASE</version>
</dependency>
```



```html
<html lang="en" xmlns:th="http://www.thymeleaf.org">
```





