# Spring

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpring/spring-overview.png)

==Spring是一个轻量级控制反转(IoC)和面向切面(AOP)的容器框架。==

- **目的**：解决企业应用开发的复杂性
- **功能**：使用基本的JavaBean代替EJB，并提供了更多的企业应用功能
- **范围**：任何Java应用
- **优点**：
  + 是一个开源框架
  + 轻量级、非入侵式框架(在项目中引入spring不会对之前的代码有影响)
  + <u>控制反转(IoC)、面向切面(AOP)</u>
  + 支持事务处理
  + 对框架整合的支持

Spring是一个开源框架，它由**Rod Johnson**创建。它是为了解决企业应用开发的复杂性而创建的。

> [Rod Johnson](https://baike.baidu.com/item/Rod%20Johnson)  Rod在悉尼大学不仅获得了计算机学位，同时还获得了音乐学位

spring框架的最初版本interface21在2002年推出，并在2004年3月24日发布了spring1.0正式版。

> Spring官方网站所有版本下载地址：https://repo.spring.io/release/org/springframework/spring/



## 一、Spring组成

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpring/spring七大模块.png)



## 二、IoC 控制反转 ==重点==

### 1、理论推导

**dao层，有1个接口，3个接口的实现类**

```java
/*UserDao.java*/
public interface UserDao {
    void getUser();
}

/*UserDaoImpl.java*/
public class UserDaoImpl implements UserDao {
    @Override
    public void getUser() {
        System.out.println("默认dao层");
    }
}

/*UserDaoMysqlImpl.java*/
public class UserDaoMysqlImpl implements UserDao {
    @Override
    public void getUser() {
        System.out.println("Mysql的dao层");
    }
}

/*UserDaoOracleImpl.java*/
public class UserDaoOracleImpl implements UserDao {
    @Override
    public void getUser() {
        System.out.println("Oracle的dao层");
    }
}
```

**service层，1个接口，1个实现类**

```java
/*UserService.java*/
public interface UserService {
    void getUser();
}

/*UserServiceImpl.java*/
public class UserServiceImpl implements UserService {
    // 由于这里定死了dao层的实现类的实例，想调用其他实现类的实例，就会更改代码，或者重复写很多类
    // 这个就会存在很多不灵活的问题
    private UserDao userDao = new UserDaoImpl();
    @Override
    public void getUser() {
        userDao.getUser();
    }
}
```

由于service层实现了业务逻辑之后，需要将数据持久化，service层的类需要得到dao层类的实例，如果service层中的类使用的是私有成员new的方式获取的dao层实例，客户需求发生变化，dao层的类有增加实现，service层也会更改获取dao层的实例，那么就会引起大量代码改变。



**改变后的service层**

```java
/*UserService.java*/
public interface UserService {
    void getUser();
}

/*UserServiceImpl.java*/
public class UserServiceImpl implements UserService {
    private UserDao userDao;

    // 通过使用set注入，可传入不同的dao层接口实现不同的功能
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void getUser() {
        userDao.getUser();
    }
}
```

**测试类**

```java
/*UserTest.java*/
public class UserTest {
    public static void main(String[] args) {
        UserServiceImpl userService = new UserServiceImpl();
        // 通过传入dao层的实现类实例，实现不同的功能，减少了代码量，增加了灵活性
        // 降低了耦合度
        userService.setUserDao(new UserDaoMysqlImpl());
        userService.getUser();
    }
}
```

为了解决该问题，就在service层中定义私有成员，使用set方法，进行注入。将程序的主动权掌握在用户手上，用户通过传入不同的接口，实现不同的方法。**通过传入dao层的实现类实例，实现不同的功能，减少了代码量，增加了灵活性，降低了耦合。**这是IOC的原型。



**总结：**

- 第一个，主动权在程序员手上，用户需求功能改变，程序员需要手动更改大量代码
- 第二个，主动权在用户手上，用户通过传入不同的接口实例，就可实现不同的功能。这个也就是IOC的原型



### 2、本质

**IOC(Inversion Of Control)控制反转，是一种设计思想**。没有IoC的程序中，我们使用面向对象编程，对象的创建于对象间的依赖关系完全硬编码在程序中，对象的创建由程序自己控制；有了IoC之后将对象的创建转移给第三方，个人认为所谓控制反转就是：获得依赖对象的方式反转了。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpring/IoC思想.png)

**控制反转是一种通过描述（XML或注解）并通过第三方去生产或获取特定对象的方式。在Spring中实现控制反转的是IoC容器，其实现方法是依赖注入（Dependency Injection, DI）**

IoC是一种思想，DI是IoC的一种实现。



### 3、举例理解IoC

Hello.java

```java
public class Hello {
    private String str;

    public Hello() {
    }

    public Hello(String str) {
        this.str = str;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    @Override
    public String toString() {
        return "Hello{" +
                "str='" + str + '\'' +
                '}';
    }
}
```

beans.xml（applicationContext.xml）

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        https://www.springframework.org/schema/beans/spring-beans.xsd">

    <!--
        bean 就等于 对象，每一个bean就是一个实体类
        Hello hello = new Hello();
        id 就是变量； class 就是new的pojo
        property 就是通过类中的set方法对属性进行设置值
    -->
    <bean id="hello" class="com.heroc.pojo.Hello">
        <property name="str" value="HelloSpring"/>
    </bean>
    <!--
        value 具体的值，基本数据类型
        ref 引用spring容器中建好的对象
    -->
</beans>
```

HelloTest.java

```java
public class HelloTest {
    public static void main(String[] args) {
        // 获取spring的上下文
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        // 从容器中获取实例对象
        Hello hello = (Hello) context.getBean("hello");
        System.out.println(hello.toString());
    }
}
```



> Hello 对象是谁创建的？		Hello对象是由Spring创建的
>
> Hello 对象的属性是怎么设置的？		Hello对象的属性是由Spring容器设置的

控制：传统应用程序的对象是由程序本身控制创建的，使用Spring后，对象是由Spring来创建的

反转：程序本身不创建对象，而变成被动的接收对象

依赖注入：就是利用set方法来进行注入的

IOC是一种编程思想，由主动的编程变成被动的接收。IoC容器来配置对象，由spring来创建，管理，装配。



#### ApplicationContext 接口

ApplicationContext 接口是，spring的上下文，即容器

Servlet的上下文(容器) 是：ServletContext



### 4、IoC创建对象方式

**无参构造器**

```java
public class User {
    private String name;
    public User() {
        System.out.println("User无参构造函数被创建");
    }
    ...
}
```

- 默认使用无参构造器，以这种方式配置bean没有无参构造器会报错

```xml
<!--默认使用无参构造器，实例化对象，使用set方法注入值-->
<bean id="user" class="com.heroc.pojo.User">
    <property name="name" value="heroC"/>
</bean>
```

---

**有参构造器**

```java
public class User {
    private String name;
    public User(String name) {
        System.out.println("User有参构造函数被创建");
        this.name = name;
    }
    ...
}
```

- 使用有参构造器，实例化对象 通过`index`

```xml
<!--方式一：使用有参构造器，实例化对象-->
<bean id="user" class="com.heroc.pojo.User">
    <constructor-arg index="0" value="heroC"/>
</bean>
```

- 使用有参构造器，实例化对象 通过`type` 如果有多个String类型的变量，就不能被正确赋值

```xml
<!--方式二：使用有参构造器，实例化对象-->
<bean id="user" class="com.heroc.pojo.User">
    <constructor-arg type="java.lang.String" value="heroC"/>
</bean>
```

- 使用有参构造器，实例化对象 通过`name`  **推荐**

```xml
<bean id="user" class="com.heroc.pojo.User">
    <constructor-arg name="name" value="heroC"/>
</bean>
```

- 使用有参构造器，实例化对象 通过`ref` 

```xml
<!--通过ref去引用该容器中，配置的其他对象-->
<bean id="user" class="com.heroc.pojo.User">
    <constructor-arg ref=""/>
</bean>
```



**UserTest.java**

```java
public class UserTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        System.out.println(context.getBean("user").equals(context.getBean("user")));
    }
}

/*
输出结果：true
*/
```

**总结：**

当配置文件被加载的时候，容器中管理的对象就已经初始化了。因此从容器中获取的对象，都是同一个对象。



## 三、Spring配置

```xml
<!--
    bean 就等于 对象，每一个bean就是一个实体类
    Hello hello = new Hello();
    id 就是变量
	class 就是该变量的类型
    property 就是通过类中的set方法对属性进行设置值
	name bean标签上的那么属性 可以给该bean取很多个别名，可使用空格、逗号、分号分割
	scope 可以设置该实例的模式
-->
<bean id="hello" class="com.heroc.pojo.Hello" name="he,he1;he3 he4">
    <property name="str" value="HelloSpring"/>
</bean>
<!--
    value 具体的值，基本数据类型
    ref 引用spring容器中建好的对象
-->

<!--
	import 用于团体开发时，将团队中的所有配置文件汇总到一个配置文件中
-->
<import resource="beans1.xml"/>
```



## 四、DI 依赖注入

Dependency Injection (DI)

### 1、构造器注入

> 见：二、4、IoC创建对象方式



### 2、set方式注入 各种集合的注入（重点）

Address.java

```java
public class Address {
    private String address;

    public Address() {
    }
    ...
}
```

Student.java

```java
public class Student {
    private String name;
    private Address address;
    private String[] books;
    private List<String> hobbies;
    private Map<String,String> card;
    private Set<String> games;
    private String wife;
    private Properties info;

    public Student() {
    }

    public Student(String name, Address address, String[] books, List<String> hobbies, Map<String, String> card, Set<String> games, String wife, Properties info) {
        this.name = name;
        this.address = address;
        this.books = books;
        this.hobbies = hobbies;
        this.card = card;
        this.games = games;
        this.wife = wife;
        this.info = info;
    }
    ...
}
```

beans.xml

```java
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean class="com.heroc.pojo.Student" id="student">
        <property name="name" value="heroC"/>
    </bean>

</beans>
```

测试类

```java
public class StudentTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        Student student = (Student) context.getBean("student");
        System.out.println(student.getName());
    }
}
```



完善注入信息：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd">
    <bean id="address" class="com.heroc.pojo.Address">
        <property name="address" value="成都"/>
    </bean>

    <bean class="com.heroc.pojo.Student" id="student">
        <!--普通注入 value-->
        <property name="name" value="heroC"/>
        
        <!--引用类型注入 ref-->
        <property name="address" ref="address"/>
        
        <!--数组注入 array-->
        <property name="books">
            <array>
                <value>红楼梦</value>
                <value>西游记</value>
                <value>水浒传</value>
                <value>三国演义</value>
            </array>
        </property>
        
        <!--List集合注入 list-->
        <property name="hobbies">
            <list>
                <value>摄影</value>
                <value>听歌</value>
                <value>旅游</value>
            </list>
        </property>
        
        <!--Map集合注入 map-->
        <property name="card">
            <map>
                <entry key="一卡通" value="6000000765754"/>
                <entry key="天府通" value="1130868644668"/>
            </map>
        </property>
        
        <!--Set集合注入 set-->
        <property name="games">
            <set>
                <value>LOL</value>
                <value>部落冲突</value>
            </set>
        </property>
        
        <!--null-->
        <property name="wife">
            <null/>
        </property>
        
        <!--properties注入 props-->
        <property name="info">
            <props>
                <prop key="学号">2017108152</prop>
            </props>
        </property>
    </bean>

</beans>
```

测试类：

```java
public class StudentTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        Student student = (Student) context.getBean("student");
        System.out.println(student.toString());

        /*
        * Student{
        *          name='heroC',
        *          address=Address{address='成都'},
        *          books=[红楼梦, 西游记, 水浒传, 三国演义],
        *          hobbies=[摄影, 听歌, 旅游],
        *          card={一卡通=6000000765754, 天府通=1130868644668},
        *          games=[LOL, 部落冲突],
        *          wife='null',
        *          info={学号=2017108152}
        * }
         * */
    }
}
```



### 3、拓展方式注入

#### p命名空间

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:p="http://www.springframework.org/schema/p"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd">

    <!--p命名可直接获取属性注入值，通过set注入-->
    <bean id="userP" class="com.heroc.pojo.User" p:userName="heroC" p:userNum="2017108152"/>

</beans>
```

测试：

```java
@Test
public void userPTest(){
    ApplicationContext context = new ClassPathXmlApplicationContext("userBeans.xml");
    User userP = context.getBean("userP", User.class);
    System.out.println(userP.toString());
}
/*
User{userName='heroC', userNum='2017108152'}
*/
```



#### c命名空间

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:c="http://www.springframework.org/schema/c"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd">

    <!--c命名可直接获取构造器中属性注入值，通过有参构造器注入-->
    <bean id="userC" class="com.heroc.pojo.User" c:userName="heroC" c:userNum="2017108152"/>

</beans>
```

测试：

```java
@Test
public void userPTest(){
    ApplicationContext context = new ClassPathXmlApplicationContext("userBeans.xml");
    User userP = context.getBean("userC", User.class);
    System.out.println(userP.toString());
}
/*
User{userName='heroC', userNum='2017108152'}
*/
```



注意：`p`命名和`c`命名不能直接使用，需要导入xml约束！



## 五、bean

### 1、bean的作用域

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpring/beanScopes.png)

1. ***singleton***

   (单例模式) 全局唯一，**默认**就是该作用域。在xml中配置的bean，全局只有一个这个实例。

   ```xml
   <bean id="user" class="com.heroc.pojo.User" scope="singleton"/>
   ```

2. ***prototype***

   (原型模式) 每一次去获取bean时，都会产生一个新的实例对象。

   ```xml
   <bean id="user" class="com.heroc.pojo.User" scope="prototype"/>
   ```

3. ***request*** 、***session***  、***application***  、***websocket*** 

   这些作用域，只能在web开发下使用



### 2、bean的自动装配

bean装配3种方式

- 通过xml中显示方式进行属性的注入赋值
- 通过java中显示方式进行属性的注入赋值
- 通过xml中隐式方式进行属性的自动注入赋值



```java
/*Cat.java*/
public class Cat {
    public void shut(){
        System.out.println("miao~");
    }
}
/*Dog.java*/
public class Dog {
    public void shut(){
        System.out.println("wang~");
    }
}
/*Person.java*/
public class Person {
    private String name;
    private Cat cat;
    private Dog dog;

    public Person(String name, Cat cat, Dog dog) {
        this.name = name;
        this.cat = cat;
        this.dog = dog;
    }

    public Person() {
    }
    ...
}
```



#### 1）byName 方式自动装配

byName方式自动装配，会在person类中找到属性的set方法，通过set方法后面的名字在xml配置文件中的上下去寻找该bean，有则自动装配，无则报错。<u>所以必须于set后面的名字一样才能装配成功！</u>

如：setDog()方法，会通过set后面的名字dog，在xml中找id为dog的bean

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="cat" class="com.heroc.pojo.Cat"/>
    <bean id="dog" class="com.heroc.pojo.Dog"/>

    <bean id="person" class="com.heroc.pojo.Person" autowire="byName">
        <property name="name" value="heroC"/>
        <!--因为person类中有cat和dog属性，通过byName在上下文查找而自动装配了-->
    </bean>
</beans>
```



#### 2）byType 方式自动装配

byType方式自动装配，是通过获取对象中的成员属性的类型，在xml中上下文查找是否有该类型的bean，有则自动装配，无则报错。<u>如果该类型的bean有很多个，则也会报错！</u>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="cat" class="com.heroc.pojo.Cat"/>
    <bean class="com.heroc.pojo.Dog"/>
    <!-- 有多个一样的类型，通过byType自动装配时会报错
		<bean id="dog1" class="com.heroc.pojo.Dog"/>
	-->

    <bean id="person" class="com.heroc.pojo.Person" autowire="byType">
        <property name="name" value="heroC"/>
    </bean>
</beans>
```



#### 3）注解实现自动装配

The introduction of annotation-based configuration raised the question of whether this approach is “better” than XML.

1. 导入约束：context约束
2. 配置注解支持：==context:annotation-config==

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:context="http://www.springframework.org/schema/context"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        https://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        https://www.springframework.org/schema/context/spring-context.xsd">

    <!--开启注解支持-->
    <context:annotation-config/>

</beans>
```



***@Autowired***

直接在成员属性上使用(可以不需要set方法)，也可以在set方法上使用！

Autowired注解，通过变量的类型在IoC容器中查找是否有该类型的bean，有则自动装配注入，没有则报错。

==先按照(byType)后按照(byName)在IoC容器中上下文查找！！！==

```java
public class Person {
    private String name;
    @Autowired
    private Cat cat;
    @Autowired
    private Dog dog;
    ...
}
```

```xml
<bean id="cat234" class="com.heroc.pojo.Cat"/>
<bean id="dog" class="com.heroc.pojo.Dog"/>
```

> 如果xml中配置的bean是唯一的，与注解的属性类型一致，名字不一致或者没有id，也能自动装配成功。如果cat类型的bean有多个，那么就先按照byType找，再按照byName找。



```java
@Autowired(required = false)
private Cat cat;
// xml 没有配置Cat类型的bean
System.out.println(person.getCat());
// 输出 null
```

> `@Autowired(required = false)` 
>
> required默认为true，如果设置为false时，当xml中没有该类型的bean，就会给该属性赋值为null



***@Qualifier(value=“ ”)***

```java
@Autowired
@Qualifier(value="dog123")
private Dog dog;
```

自动装配，先在xml中通过byType找到bean，再通过byName所指定名找到id为dog123的bean

@autowired是先通过byType在上下文中找bean，如果有且仅有一个这个bean，即使id没有或者不一致也会执行成功；如果存在多个这样的bean，同时没有使用@Qualifier来指定name时，则会报异常。如果@autowired和@Qualifier一起使用，则会通过byType和byName去找到唯一一个bean，没有则会报异常。所以@autowired是先是通过byType寻找，如果存在多个一样类型的bean，再通过byName找到指定的bean



***@Nullable***

```java
public Person(@Nullable String name, Cat cat, Dog dog) {
    this.name = name;
    this.cat = cat;
    this.dog = dog;
}
```

说明name这个属性可以为null



---

***@Resource***

@Resource 是java自带的注解，默认按照byName在xml中查找bean

@Resource的作用相当于@Autowired，只不过@Autowired按byType自动注入，而@Resource默认按 byName自动注入罢了。@Resource有两个属性是比较重要的，分是name和type，Spring将@Resource注解的name属性解析为bean的名字，而type属性则解析为bean的类型。所以如果使用name属性，则使用byName的自动注入策略，而使用type属性时则使用byType自动注入策略。如果既不指定name也不指定type属性，这时将通过反射机制使用byName自动注入策略。 　　

@Resource装配顺序 ：

1. 如果同时指定了name和type，则从Spring上下文中找到唯一匹配的bean进行装配，找不到则抛出异常 　　
2. 如果指定了name，则从上下文中查找名称（id）匹配的bean进行装配，找不到则抛出异常 　　
3. 如果指定了type，则从上下文中找到类型匹配的唯一bean进行装配，找不到或者找到多个，都会抛出异常 　
4.  如果既没有指定name，又没有指定type，则自动按照byName方式进行装配；如果没有匹配，则回退为一个原始类型进行匹配，如果匹配则自动装配；



---

##### @Resource与@Autowired的区别 ==重点==

1、 @Autowired与@Resource都可以用来装配bean. 都可以写在字段上,或写在setter方法上。

2、 @Autowired默认按类型装配（这个注解是属业spring的），默认情况下必须要求依赖对象必须存在，如果要允许null值，可以设置它的required属性为false，如：@Autowired(required=false) ，如果我们想使用名称装配可以结合@Qualifier注解进行使用

3、@Resource（这个注解属于J2EE的），默认按照名称进行装配，名称可以通过name属性进行指定，如果没有指定name属性，当注解写在字段上时，默认取字段名进行安装名称查找，如果注解写在setter方法上默认取属性名进行装配。当找不到与名称匹配的bean时才按照类型进行装配。但是需要注意的是，如果name属性一旦指定，就只会按照名称进行装配。



### 3、接口有多个实现类，注入时是如何选择使用指定实例 ==重点==

**自动注入set方法的参数名，用于指定使用哪个实现类来进行注入。**

**如果接口的实例只有一个，注册的bean也只有一个，那么注入时参数变量名可任意取，都可以获得到这个实例**

**如果被注入的实例是通过注解的方式在容器中注册的bean，那么通过@Autowired自动注入实例时，通过类名来命名参数名，这样就可以指定使用这个接口的哪个具体实现类。**

**如果被注入的实例是通过xml方式手动注册的bean，那么通过@Autowired自动注入该实例时，通过bean的id来命名参数名，这样就可以指定使用的这个接口的哪个具体实现类**



使用controller层和service层做解释：

#### 1）service层使用xml配置bean

service层有一个接口UserService.java

```java
public interface UserService {
    void checkUserByName();
}
```



有两个实现接口的类

UserServiceImpl01.java

```java
public class UserServiceImpl01 implements UserService {

    @Override
    public void checkUserByName(){
        System.out.println("UserServiceImpl01111111");
    }
}
```

UserServiceImpl02.java

```java
public class UserServiceImpl02 implements UserService {

    @Override
    public void checkUserByName(){
        System.out.println("UserServiceImpl0222222");
    }
}
```



xml注册bean

```xml
<bean class="com.heroc.service.UserServiceImpl01" id="userService01">
</bean>

<bean class="com.heroc.service.UserServiceImpl02" id="userService02">
</bean>
```



controller层UserController.java中使用

```java
@RestController
public class UserController {
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService01) {
        this.userService = userService01;
    }
    
    public void printInfo(){
        userService.checkUserByName(); // 会打印UserServiceImpl01111111
    }
}
```

> 这里通过@Autowired自动注入，setUserService方法的参数名就是xml中注册的bean的id，通过id就指定了使用的是哪个接口的实现类。

这里的变量为userService01，对应xml中的bean的id，所以可以知道这里注入的是UserServiceImpl01类



#### 2）service层使用@Service注解注册bean

service层有一个接口UserService.java

```java
public interface UserService {
    void checkUserByName();
}
```



有两个实现接口的类

UserServiceImpl01.java

```java
@Service
public class UserServiceImpl01 implements UserService {

    @Override
    public void checkUserByName(){
        System.out.println("UserServiceImpl01111111");
    }
}
```

UserServiceImpl02.java

```java
@Service
public class UserServiceImpl02 implements UserService {

    @Override
    public void checkUserByName(){
        System.out.println("UserServiceImpl0222222");
    }
}
```



controller层UserController.java中使用

```java
@RestController
public class UserController {
    private UserService userService;

    @Autowired
    public void setUserService(UserService userServiceImpl01) {
        this.userService = userService01;
    }
    
    public void printInfo(){
        userService.checkUserByName(); // 会打印UserServiceImpl01111111
    }
}
```

> 这里通过@Autowired自动注入，由于UserService接口的实现类都是通过注解注册的bean，因此setUserService方法的参数名需要使用实现类的名称，来指定使用哪个实现类。

这里的参数名为userServiceImpl01，又因为UserServiceImpl01.java通过@service注解在容器中注册了bean，所以通过上下文找到这里注入的是UserServiceImpl01类



## 六、Spring的注解

> 怎么选用注解开发，根据项目情况，选择使用注解还是xml方式开发。大多数情况使用xml进行开发

使用注解开发，需要使用aop的包依赖，并且xml需要导入context的约束，增加注解的支持！

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:p="http://www.springframework.org/schema/p"
       xmlns:c="http://www.springframework.org/schema/c"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd
			http://www.springframework.org/schema/context
			https://www.springframework.org/schema/context/spring-context.xsd">
    
    <!--context:component-scan 扫描注解-->
    <context:component-scan base-package="com.heroc"/>
    
    <!--context:annotation-config 开启注解-->
    <context:annotation-config/>

</beans>
```



- **bean注解**

  com.heroc.pojo.User

  ```java
  // @Component组件 @Scope("singleton")作用域
  // 等价于在xml中 <bean id="user" class="com.heroc.pojo.User" scope="singleton"/>
  @Component
  @Scope("singleton")
  public class User {
  
      // @Value("heroC") 等价于在xml中 <property name="name" value="heroC"/>
      @Value("heroC")
      private String name;
  
      @Override
      public String toString() {
          return "User{" +
                  "name='" + name + '\'' +
                  '}';
      }
  }
  ```

  

---

### 1、@Component  @Repository...

- pojo层：【@Component】

  dao层：【@Repository】

  service层：【@Service】

  controller层：【@Controller】

  > @Component、 @Repository、 @Service、 @Controller
  >
  > 这四个注解都是等价的，都是在xml中注册一个bean，只是用在不同的层中，用于分辨使用这个注解的类是属于哪个层的。

- **自动装配**

  @Autowired

  @Qualifier("name")

  @Nullabel

  @Resource

- **作用域**

  @Scope("singleton") 用于指定这个bean的作用域，值可以是：singleton、prototype等



### 2、@RequestParam

该注解功能：

1. **可以指定请求中的变量名**

   当前端请求中的变量名为username，而后端接收该参数的变量名与其不同时，可使用该注解指定

   ```java
   public String test(@RequestParam("username") String name){
       System.out.println(name);
   }
   ```

2. **必须有值的参数**

   name变量必须有值

   ```java
   public String test(@RequestParam(requried="true") String name){
       System.out.println(name);
   }
   ```

3. **设置参数的默认值**

   当前端没有传递参数过来时，可以使用默认值

   ```java
   public String test(@RequestParam(defaultValue='默认名字') String name){
       System.out.println(name);
   }
   ```

   



---

**小结**

- xml与注解：
  - xml更加万能，适用于任何场景！维护方便简单
  - 注解相对于xml比较局限，维护相对比较复杂

- 最佳实现：
  - xml用于管理bean，所有bean在xml中注册
  - 注解用于完成属性的注入



## 七、使用JavaConfig实现配置

BeansConfig.java

```java
// @Configuration 也是一个组件，等价于xml中的<beans></beans>
// @ComponentScan 开启注解扫描 
// @Import 用于导入其他的beans类，合并成一个beans类
@Configuration
@ComponentScan("com.heroc")
@Import(BeansConfig2.class)
public class BeansConfig {

    // @Bean 等价于xml中的<bean></bean>标签
    // 这个方法的名字就是bean标签的id属性
    // 这个方法的返回值就是bean标签的class属性
    @Bean
    public User getUser(){
        return new User();
    }

}
```

等价于xml

```xml
<beans>
    <context:component-scan base-package="com.heroc"/>
    <context:annotation-config/>
    <bean id="getUser" class="com.heroc.pojo.User"/>
</beans>
```



User.java

```java
// @Component 等价于 <bean id="user" class="com.heroc.pojo.User"/>
@Component
public class User {
    private String name;

    public String getName() {
        return name;
    }

    @Value("heroC")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                '}';
    }
}
```



测试类：

```java
public class UserTest {
    @Test
    public void user(){
        // 使用java来配置spring就使用AnnotationConfigApplicationContext来获取配置类的class对象
        ApplicationContext context = new AnnotationConfigApplicationContext(BeansConfig.class);
        
        // 获取的是BeansConfig.java中的bean注解的方法getUser的这个bean
        // User getUser = context.getBean("getUser", User.class);
        
        // 获取的是User.java中的component注解的这个bean
        User getUser = context.getBean("user", User.class);
        System.out.println(getUser.toString());
    }
}

/*输出： User{name='heroC'}*/
```



## 八、代理模式 ==重点==

spring的AOP底层，就是使用的代理模式！！！



### 1、静态代理

以房东、中介、客户的关系解释静态代理

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpring/静态代理.png)

这里的代理就是中介，中介可以联系房东，为房东租房子，客户找中介要租房子，这样中介就是中间的代理，房东和客户只需要单纯的做一件事情就可以了，一个租出去，一个要租房，中介可以为房东、客户做很多繁杂的事情。

Rent.java

```java
public interface Rent {
    void rent();
}
```

Householder.java

```java
public class Householder implements Rent{
    @Override
    public void rent() {
        System.out.println("房东要租房子!");
    }
}
```

Proxy.java

```java
public class Proxy implements Rent{
    // 中介与房东合作，把房东的房子租出去
    private Householder householder;

    public Proxy(Householder householder) {
        this.householder = householder;
    }

    // 代理帮房东租房子，并且还可以服务于要租房子的顾客
    @Override
    public void rent() {
        this.seeHouse();
        householder.rent();
        this.signAContract();
    }

    public void seeHouse(){
        System.out.println("代理带看房子");
    }

    public void signAContract(){
        System.out.println("代理签合同");
    }
}
```

Client.java

```java
public class Client {
    public static void main(String[] args) {
        // 要租房子的顾客，通过找到代理，代理找到合适的房东，租到了房子
        Proxy proxy = new Proxy(new Householder());
        proxy.rent();
    }
}
```

输出结果：

```
代理带看房子
房东要租房子!
代理签合同
```



个人观点的角色分析：

- 抽象角色：一般会使用接口或抽象类解决
- 真实角色(目标对象)：被代理的角色
- 代理角色(代理对象)：代理真实角色的角色，还可以做一些附属操作
- 访问角色：用于访问代理的角色



**总结：**

- 优点
  + 可以使得目标对象做的事情更加纯粹简单
  + 可以集中管理，方便扩展
- 缺点
  + 每一个目标对象都会有一个对应的代理对象，这样代码量翻倍，开发效率低



### 2、动态代理 (基于接口)

静态代理是代理类在代码运行前已经创建好，并生成class文件；动态代理类 是代理类在程序运行时创建的代理模式。  

动态代理类的代理类并不是在Java代码中定义的，而是在运行时根据我们在Java代码中的“指示”动态生成的。相比于静态代理， 动态代理的优势在于可以很方便的对代理类的函数进行统一的处理，而不用修改每个代理类中的方法。 想想你有100个静态代理类，现在有一个需求，每个代理类都需要新增一个处理逻辑，你需要打开100个代理类在每个代理方法里面新增处理逻辑吗？ 有或者代理类有5个方法，每个方法都需要新增一个处理逻辑， 你需要在每个方法都手动新增处理逻辑吗？ 想想就挺无趣的。并且，100个目标对象，就需要100个静态代理类，代码量不仅翻倍，开发效率也低，而动态代理只需要一个处理类，就可实现多个目标对象使用一个处理类就可以动态生成代理。



**动态代理：**

基于接口的动态代理 (基于接口---JDK动态代理)

基于类的动态代理 (基于类---cglib)



**Proxy 类**

`java.lang.reflect.Proxy`

**常用方法：**

- `public static InvocationHandler getInvocationHandler(Object proxy)throws IllegalArgumentException` 

- `public static Class<?> getProxyClass(ClassLoader loader,Class<?>... interfaces)` 

- `public static boolean isProxyClass(Class<?> cl)` 

  如果且仅当使用 getProxyClass方法或  newProxyInstance方法将指定的类动态生成为代理类时，则返回true。

- `public static Object newProxyInstance(ClassLoader loader,Class<?>[]interfaces,InvocationHandler h) ` 

  返回指定接口的代理类的实例，该接口将方法调用分派给指定的调用处理程序。



**InvocationHandler 接口**

`java.lang.reflect Interface InvocationHandler`

**抽象方法：**

- `public Object invoke(Object proxy, Method method, Object[] args) throws Throwable;`

  当在与之关联的代理实例上调用方法时，将在调用处理程序中调用此方法

  `proxy` - 调用该方法的代理实例 

  `method` -所述`方法`对应于调用代理实例上的接口方法的实例。  `方法`对象的声明类将是该方法声明的接口，它可以是代理类继承该方法的代理接口的超级接口。 

  `args`  -包含的方法调用传递代理实例的参数值的对象的阵列，或`null`如果接口方法没有参数。  原始类型的参数包含在适当的原始包装器类的实例中，例如`java.lang.Integer`或`java.lang.Boolean`  。 



**通过案例解释动态代理**

Rent.java

```java
public interface Rent {
    void rent();
}
```

RentImpl.java

```java
public class RentImpl implements Rent {
    @Override
    public void rent() {
        System.out.println("RentImpl要租房子!");
    }
}
```

ProxyInvocationHandler.java

```java
/*
 * ProxyInvocationHandler 类 实现InvocationHandler接口，这个类中持有一个被代理对象(委托类)的实例target。该类被JDK Proxy类回调
 * InvocationHandler 接口中有一个invoke方法：当一个代理实例的方法被调用时，代理方法将被编码并分发到 InvocationHandler接口的invoke方法执行。
 * */
public class ProxyInvocationHandler implements InvocationHandler {
    // 获取需要被代理的目标对象接口
    private Object target;
    // 该方法传入的是实现了接口的实例
    public void setTarget(Object target) {
        this.target = target;
    }

    // 通过Proxy.newProxyInstance()获取一个动态的Proxy类
    public Object getProxy(){
        /**
         * this.getClass().getClassLoader() 传入该类的类加载器
         * target.getClass().getInterfaces() 传入目标对象实现的接口
         * InvocationHandler 传入动态代理的处理器
         */
        return Proxy.newProxyInstance(
            this.getClass().getClassLoader(),
            target.getClass().getInterfaces(),
            this);
    }

    /**
     * 当一个代理实例的方法被调用时，代理方法将被编码并分发到 InvocationHandler接口的invoke方法执行，回调
     * @param proxy 代表动态生成的 动态代理 对象实例
     * @param method 代表被调用委托类的接口方法，和生成的代理类实例调用的接口方法是一致的，它对应的Method 实例
     * @param args 代表调用接口方法对应的Object参数数组，如果接口是无参，则为null； 对于原始数据类型返回的他的包装类型。
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        /*
        * 在转调具体目标对象的前后，可以执行一些功能处理，对代理的增强
        * */

        /*System.out.println(proxy.getClass().getInterfaces()[0]); 可知道proxy代理类也实现了target实例对象的接口
        Method[] methods = proxy.getClass().getMethods();
        for (Method method1 : methods) {
            System.out.println(method1.toString()); // 从遍历可知，proxy有target实例对象的接口方法
        }*/

        // 当代理对象的某个方法被调用时，则会执行该方法，method是代理类被调的方法，
        // 这个方法与target实例对象一致，则通过invoke去激活target实例对象的该方法，达到target实例对象中该方法被使用
        System.out.println("before invoke...");
        method.invoke(target, args); // 执行被代理类的方法
        System.out.println("after invoke...");
        return null;
    }
}
```

Client.java

```java
public class Client {
    public static void main(String[] args) {
        // 得到目标对象
        RentImpl rentImpl = new RentImpl();
        // 得到动态代理的处理器
        ProxyInvocationHandler pih = new ProxyInvocationHandler();
        // 给处理器传递一个目标对象，通过反射获取目标对象的信息
        pih.setTarget(rentImpl);
        // 通过目标对象，获取目标对象的实现接口，然后得到一个代理对象
        Rent poxy = (Rent) pih.getProxy();
        // proxy代理对象调用rent方法，则会回调MyInvocationHandler中的invoke方法，
        // method.invoke(target, args); 通过该句，去调用实例rentImpl的rent方法
        poxy.rent();
    }
}
```

输出：

```
before invoke...
RentImpl要租房子!
after invoke...
```





**总结：**

- 动态代理解决了静态代理的不足之处
  + 可以使得目标对象做的事情更加纯粹简单
  + 可以集中管理，方便扩展
  + 通过实现InvocationHandler接口，可以动态生成很多个代理
  + 解决了代码量，开发效率提升



## 九、AOP 面向切面编程 ==重点==

### 1、AOP简介

AOP为Aspect Oriented Programming的缩写，意为：面向切面编程，通过预编译方式和运行期间动态代理实现程序功能的统一维护的一种技术。AOP是OOP的延续，是软件开发中的一个热点，也是Spring框架中的一个重要内容，是函数式编程的一种衍生范型。利用AOP可以对业务逻辑的各个部分进行隔离，从而使得业务逻辑各部分之间的**耦合度降低**，提高程序的**可重用性**，同时提高了**开发的效率**。



AOP的底层就是动态代理，以代理的模式切面编程，不更改原有类的代码，而新增一个代理类，实现对原有类方法的增强。



通知(Advice)的类型：

- 前置通知（Before advice）：在某个连接点（Join point）之前执行的通知，但这个通知不能阻止连接点的执行（除非它抛出一个异常）。
- 返回后通知（After returning advice）：在某个连接点（Join point）正常完成后执行的通知。例如，一个方法没有抛出任何异常正常返回。
- 抛出异常后通知（After throwing advice）：在方法抛出异常后执行的通知。
- 后置通知（After（finally）advice）：当某个连接点（Join point）退出的时候执行的通知（不论是正常返回还是发生异常退出）。
- 环绕通知（Around advice）：包围一个连接点（Join point）的通知，如方法调用。这是最强大的一种通知类型。环绕通知可以在方法前后完成自定义的行为。它也会选择是否继续执行连接点或直接返回它们自己的返回值或抛出异常来结束执行。



依赖的包：

```xml
<!-- https://mvnrepository.com/artifact/org.aspectj/aspectjweaver -->
<dependency>
    <groupId>org.aspectj</groupId>
    <artifactId>aspectjweaver</artifactId>
    <version>1.9.4</version>
</dependency>
```



### 2、AOP实现方式

---

**excution表达式**

> *execution(\* 包名.\*.*(..))*

整个表达式可以分为五个部分：

1、execution(): 表达式主体。

2、第一个*号：方法返回类型， *号表示所有的类型。

3、包名：表示需要拦截的包名。

4、第二个*号：表示类名，\*号表示所有的类。

5、*(..):最后这个星号表示方法名，*号表示所有的方法，后面( )里面表示方法的参数，两个句点表示任何参数

---



以下案例使用的接口

```java
public interface UserService {
    void add();
    void update();
    void delete();
    void select();
}
```



#### 方式一：Spring的API 实现

UserServiceImp.java

```java
public class UserServiceImp implements UserService {
    @Override
    public void add() {
        System.out.println("增加了一个用户");
    }

    @Override
    public void delete() {
        System.out.println("删除了一个用户");
    }

    @Override
    public void update() {
        System.out.println("更新了一个用户");
    }

    @Override
    public void select() {
        System.out.println("查询了一个用户");
    }
}
```

BeforeLog.java

```java
// 需要实现Spring的接口
public class BeforeLog implements MethodBeforeAdvice {
    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println(target.getClass().getName()+"在方法之前执行了"+method.getName()+"方法");
    }
}
```

AfterLog.java

```java
// 需要实现Spring的接口
public class AfterLog implements AfterReturningAdvice {
    @Override
    public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
        System.out.println(method.getName()+"方法之后执行了"+this.getClass().getName());
    }
}
```

applicationContext.xml

```xml
<!--方式一：-->

<!--注册bean-->
<bean id="beforeLog" class="com.heroc.demo01.BeforeLog"/>
<bean id="userServiceImp" class="com.heroc.demo01.UserServiceImp"/>
<bean id="afterLog" class="com.heroc.demo01.AfterLog"/>

<!--AOP配置-->
<aop:config>
    <!--切点，需要被代理的对象 execution(第一个参数是返回的类型 第二个是需要设置为切点的类的方法(被代理))-->
    <!--com.heroc.demo01.RentImp.*(..) 表示com.heroc.demo01包下的RentImp类的所有方法不限参数-->
    <!--*(..) 第一个*表示所有方法，括号内的省略号是不限参数类型个数-->
    <aop:pointcut id="userServicePointCut" expression="execution(* com.heroc.demo01.UserServiceImp.*(..))"/>
    <!--环绕通知：即在切点方法的前后都通知-->
    <!--因为这些类都已经实现了在切点方法上执行顺序的接口，所以使用aop:advisor-->
    <aop:advisor advice-ref="beforeLog" pointcut-ref="userServicePointCut"/>
    <aop:advisor advice-ref="afterLog" pointcut-ref="userServicePointCut"/>
</aop:config>
```

测试类：

```java
@Test
public void demo01(){
    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    // 面向接口的动态代理，所以返回类型必须是接口，userServiceImp是被代理的对象
    UserService userServiceImp = context.getBean("userServiceImp", UserService.class);
    userServiceImp.add();
}
```

输出结果：

```java
com.heroc.demo01.UserServiceImp在方法之前执行了add方法
增加了一个用户
add方法之后执行了com.heroc.demo01.AfterLog
```



#### 方式二：自定义 实现 (推荐)

UserServiceImp02.java

```java
public class UserServiceImp02 implements UserService {
    @Override
    public void add() {
        System.out.println("增加了一个用户");
    }

    @Override
    public void delete() {
        System.out.println("删除了一个用户");
    }

    @Override
    public void update() {
        System.out.println("更新了一个用户");
    }

    @Override
    public void select() {
        System.out.println("查询了一个用户");
    }
}
```

DiyAspect.java

```java
public class DiyAspect {
    public void before(){
        System.out.println("---------之前执行---------");
    }

    public void after(){
        System.out.println("---------之后执行---------");
    }
}
```

applicationContext.xml

```xml
<!--方式二：-->

<!--注册bean-->
<bean id="diyAspect" class="com.heroc.demo02.DiyAspect"/>
<bean id="userServiceImp02" class="com.heroc.demo02.UserServiceImp02"/>

<!--AOP配置-->
<aop:config>
    <!--引用一个切面，切面就是一个类-->
    <aop:aspect ref="diyAspect">
        <!--确定切点-->
        <aop:pointcut id="userServicePointCut02" expression="execution(* com.heroc.demo02.UserServiceImp02.*(..))"/>
        <!--在切点方法之前，执行切面类中的before方法-->
        <aop:before method="before" pointcut-ref="userServicePointCut02"/>
        <!--在切点方法之后，执行切面类中的after方法-->
        <aop:after method="after" pointcut-ref="userServicePointCut02"/>
    </aop:aspect>
</aop:config>
```

测试类：

```java
    @Test
    public void demo01(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        // 面向接口的动态代理，所以返回类型必须是接口，userServiceImp02是被代理的对象
        UserService userServiceImp02 = context.getBean("userServiceImp02", UserService.class);
        userServiceImp02.add();
        userServiceImp02.update();
    }
```

输出结果：

```
---------之前执行---------
增加了一个用户
---------之后执行---------
---------之前执行---------
更新了一个用户
---------之后执行---------
```



#### 方式三：注解 实现

UserServiceImp03.java

```java
public class UserServiceImp03 implements UserService {
    @Override
    public void add() {
        System.out.println("增加了一个用户");
    }

    @Override
    public void delete() {
        System.out.println("删除了一个用户");
    }

    @Override
    public void update() {
        System.out.println("更新了一个用户");
    }

    @Override
    public void select() {
        System.out.println("查询了一个用户");
    }
}
```

AnnotationAspect.java

```java
@Aspect // 切面
public class AnnotationAspect {

    // 切点方法之前执行，绑定再目标方法之前
    @Before("execution(* com.heroc.demo03.UserServiceImp03.*(..))")
    public void before(){
        System.out.println("---------之前执行---------");
    }

    // 切点方法以及所有方法执行完之后再执行，这是一个终结方法
    @After("execution(* com.heroc.demo03.UserServiceImp03.*(..))")
    public void after(){
        System.out.println("---------之后执行---------");
    }

    // 环绕通知
    @Around("execution(* com.heroc.demo03.UserServiceImp03.*(..))")
    public void around(ProceedingJoinPoint joinPoint) throws Throwable {
        // 环绕通知的前置通知是最开始执行的方法
        System.out.println("环绕之前");
        /* 通过连接点的proceed()方法，会与目标对象的方法连接，
        去执行目标对象的方法，执行完之后，又回到该连接点位置继续往下执行*/
        joinPoint.proceed(); // 去调用目标方法，没有该语句，则不会去调用目标方法
        System.out.println("环绕之后");
    }
}
```

applicationContext.xml

```xml
<!--方式三：-->

<bean id="annotationAspect" class="com.heroc.demo03.AnnotationAspect"/>
<bean id="userServiceImp03" class="com.heroc.demo03.UserServiceImp03"/>

<!--开启aop面向切面注解的自动代理-->
<!--(JDK 方式代理(默认) proxy-target-class="false")-->
<!--(cglib 方式代理 proxy-target-class="true")-->
<aop:aspectj-autoproxy/>
```

测试类：

```java
@Test
public void demo01(){
    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    UserService userServiceImp03 = context.getBean("userServiceImp03", UserService.class);
    userServiceImp03.update();
}
```

输出结果

有joinPoint.proceed();该句话输出结果：

```
环绕之前
---------之前执行---------
更新了一个用户
环绕之后
---------之后执行---------
```

无joinPoint.proceed();该句话输出结果：

```
环绕之前
环绕之后
---------之后执行---------
```



### 3、AOP相对于切点的执行顺序

```
环绕之前
---------before之前---------
------切点目标方法
------目标方法返回结果后通知
环绕之后
---------after之后---------

////异常后通知 只要抛出异常则会通知
```



## 十、整合Mybatis



### 1、Spring整合Mybatis 方式一 ==重点掌握==

**pojo层**

User.java

```java
public class User {
    private int id;
    private String name;
    private String pwd;
    ...
}
```

**dao层**

UserMapper.java

```java
public interface UserMapper {
    List<User> selectAllUser();
}
```

UserMapper.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.dao.UserMapper">
    <select id="selectAllUser" resultType="user">
        select * from `user`
    </select>
</mapper>
```

UserMapperImpl.java

```java
// 该类的作用就是获取sqlSession，去执行UserMapper里的方法
public class UserMapperImpl implements UserMapper {
    // 该类需要在IoC容器中注册，以方便其他类使用； SqlSessionTemplate是同步安全的
    private SqlSessionTemplate sqlSession;

    public void setSqlSession(SqlSessionTemplate sqlSession) {
        this.sqlSession = sqlSession;
    }

    @Override
    public List<User> selectAllUser() {
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        return mapper.selectAllUser();
    }
}
```

**配置文件**

mybatis-config.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>

    <!--在与spring整合之后，即使spring配置文件可以完全替代mybatis的配置文件，
        但是建议mybatis配置文件用于管理mybatis的设置和别名-->

    <settings>
        <!--<setting name="logImpl" value="STDOUT_LOGGING"/>-->
        <setting name="mapUnderscoreToCamelCase" value="true"/>
    </settings>

    <typeAliases>
        <package name="com.heroc.pojo"/>
    </typeAliases>

</configuration>
```

spring-dao.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd">

    <!--代替mybatis-config.xml中的environments环境设置的dataSource-->
    <!--dataSource这里默认使用的spring的数据源，也可以使用c3p0、driud等数据源-->
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.cj.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://localhost:3306/mybatis?useUnicode=true&amp;useSSL=true&amp;characterEncoding=UTF-8"/>
        <property name="username" value="root"/>
        <property name="password" value="113011"/>
    </bean>

    <!--将SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream)配置成bean，以spring的方式实现-->
    <bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <property name="dataSource" ref="dataSource"/>
        <!--代替mybatis-config.xml中的mappers标签-->
        <property name="mapperLocations" value="classpath:com/heroc/dao/*.xml"/>
        <!--将mybatis-config.xml与该配置文件联合到一起-->
        <property name="configLocation" value="mybatis-config.xml"/>
    </bean>

    <!--将sqlSession添加到IoC容器中，代替了SqlSession session = factory.openSession();-->
    <bean id="sqlSesson" class="org.mybatis.spring.SqlSessionTemplate">
        <constructor-arg index="0" ref="sqlSessionFactory"/>
    </bean>

</beans>
```

ApplicationContext.xml

```xml
<!--该配置文件为spring的总配置文件，用于整合其他各个功能的spring配置文件-->
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:p="http://www.springframework.org/schema/p"
       xmlns:c="http://www.springframework.org/schema/c"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd
			http://www.springframework.org/schema/context
			https://www.springframework.org/schema/context/spring-context.xsd
			http://www.springframework.org/schema/aop
			https://www.springframework.org/schema/aop/spring-aop.xsd">

    <context:annotation-config/>
    <import resource="spring-dao.xml"/>
    
    <!--将实现类注册进来，并注入sqlSession，去管理方法执行sql语句-->
    <bean id="userMapperImpl" class="com.heroc.dao.UserMapperImpl">
        <property name="sqlSession" ref="sqlSesson"/>
    </bean>
</beans>
```



### 2、Spring整合Mybatis 方式二 (了解即可)

其他都与方式一是一样的，只是方式二中继承SqlSessionDaoSupport可以省去sqlSessionTemplate实例对象的获取，直接由SqlSessionDaoSupport去创建。使用继承方式，spring-dao.xml中就不用注册sqlSessionTemplate了。



UserMapperImpl2.java

```java
public class UserMapperImpl2 extends SqlSessionDaoSupport implements UserMapper {
    /*SqlSessionDaoSupport可以为我们创建sqlSessionTemplate实例对象，
      但是SqlSessionDaoSupport需要一个SqlSessionFactory，
      因此在IoC注册该类时需要注入SqlSessionFactory的bean*/
    @Override
    public List<User> selectAllUser() {
        return getSqlSession().getMapper(UserMapper.class).selectAllUser();
    }
}
```

ApplicationContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:p="http://www.springframework.org/schema/p"
       xmlns:c="http://www.springframework.org/schema/c"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd
			http://www.springframework.org/schema/context
			https://www.springframework.org/schema/context/spring-context.xsd
			http://www.springframework.org/schema/aop
			https://www.springframework.org/schema/aop/spring-aop.xsd">

    <context:annotation-config/>
    <import resource="spring-dao.xml"/>

    <bean id="userMapperImpl2" class="com.heroc.dao.UserMapperImpl2">
        <property name="sqlSessionFactory" ref="sqlSessionFactory"/>
    </bean>

</beans>
```



## 十一、事务 ==重点==

事务ACID原则：

- 原子性
- 一致性
- 隔离性
- 持久性



使用事务需要tx约束(需要引入mybatis-spring依赖才可以使用tx约束)

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                           https://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/tx
                           https://www.springframework.org/schema/tx/spring-tx.xsd">

</beans>
```



### 1、声明式事务：AOP

”交由容器管理事务“ 又称 “声明式事务”

声明式事务，是通过AOP来实现的。

**基于.xml的声明式事务；基于@Transaction注解的声明式事务**

**pojo层**

User.java

```java
public class User {
    private int id;
    private String name;
    private String pwd;
    ...
}
```

**dao层**

UserMapper.java

```java
public interface UserMapper {
    List<User> selectAllUser();
    int addUser(User user);
    int deleteUser(int id);
}
```

UserMapper.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.dao.UserMapper">
    <select id="selectAllUser" resultType="user">
        select * from `user`
    </select>

    <insert id="addUser" parameterType="user">
        insert into `user` values (#{id},#{name},#{pwd})
    </insert>

    <delete id="deleteUser" parameterType="_int">
        /*deletes 专门写错，为了测试事务开启是否有效*/
        deletes from `user` where id = #{id}
    </delete>
</mapper>
```

UserMapperImpl.java

```java
public class UserMapperImpl implements UserMapper {
    // 该类需要在IoC容器中注册，成员属性注入
    private SqlSessionTemplate sqlSession;

    public void setSqlSession(SqlSessionTemplate sqlSession) {
        this.sqlSession = sqlSession;
    }

    @Override
    public List<User> selectAllUser() {
        // 测试事务，deletUser方法的xml配置是有错的，开启事务，如果deleteUser方法执行不成功，
        // 那么addUser方法执行了，也会被撤销。失败则回滚
        User user = new User(4, "yikeX", "1234");
        addUser(user);
        deleteUser(4);
        return sqlSession.getMapper(UserMapper.class).selectAllUser();
    }

    @Override
    public int addUser(User user) {
        return sqlSession.getMapper(UserMapper.class).addUser(user);
    }

    @Override
    public int deleteUser(int id) {
        return sqlSession.getMapper(UserMapper.class).deleteUser(id);
    }
}
```

**配置文件**

mybatis-config.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>

    <!--在与spring整合之后，即使spring配置文件可以完全替代mybatis的配置文件，
        但是建议mybatis配置文件用于管理mybatis的设置和别名-->

    <settings>
        <!--<setting name="logImpl" value="STDOUT_LOGGING"/>-->
        <setting name="mapUnderscoreToCamelCase" value="true"/>
    </settings>

    <typeAliases>
        <package name="com.heroc.pojo"/>
    </typeAliases>

</configuration>
```

spring-dao.xml (配置事务，重点)

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd
            http://www.springframework.org/schema/tx
            https://www.springframework.org/schema/tx/spring-tx.xsd
            http://www.springframework.org/schema/aop
            https://www.springframework.org/schema/aop/spring-aop.xsd">

    <!--代替mybatis-config.xml中的环境设置dataSource-->
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.cj.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://localhost:3306/mybatis?useUnicode=true&amp;useSSL=true&amp;characterEncoding=UTF-8"/>
        <property name="username" value="root"/>
        <property name="password" value="113011"/>
    </bean>

    <!--将SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream)配置成bean-->
    <bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <property name="dataSource" ref="dataSource"/>
        <!--代替mybatis-config.xml中的mappers标签-->
        <property name="mapperLocations" value="classpath:com/heroc/dao/*.xml"/>
        <!--将mybatis-config.xml与该配置文件联合到一起-->
        <property name="configLocation" value="mybatis-config.xml"/>
    </bean>

    <!--将sqlSession添加到IoC容器中，代替了SqlSession session = factory.openSession();-->
    <bean id="sqlSesson" class="org.mybatis.spring.SqlSessionTemplate">
        <constructor-arg index="0" ref="sqlSessionFactory"/>
    </bean>

    <!--配置事务管理器，开启事务-->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <constructor-arg index="0" ref="dataSource"/>
    </bean>

    <!--tx是配置文件事务的约束，声明哪些方法需要开启事务通知-->
    <tx:advice id="txAdvice" transaction-manager="transactionManager">
        <tx:attributes>
            <!--name为需要开启事务通知的方法名，propagation为传播常用默认的值是REQUIRED-->
            <tx:method name="selectAllUser" propagation="REQUIRED"/>
            <tx:method name="addUser" propagation="REQUIRED"/>
            <tx:method name="deleteUser" propagation="REQUIRED"/>
        </tx:attributes>
    </tx:advice>

    <!--通过aop的方式，将UserMapperImpl类下的所有方法都设为切点，但只有被事务声明了的方法才添加事务通知-->
    <aop:config>
        <aop:pointcut id="txPointCut" expression="execution(* com.heroc.dao.UserMapperImpl.*(..))"/>
        <aop:advisor advice-ref="txAdvice" pointcut-ref="txPointCut"/>
    </aop:config>

</beans>
```

ApplicationContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:p="http://www.springframework.org/schema/p"
       xmlns:c="http://www.springframework.org/schema/c"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd
			http://www.springframework.org/schema/context
			https://www.springframework.org/schema/context/spring-context.xsd
			http://www.springframework.org/schema/aop
			https://www.springframework.org/schema/aop/spring-aop.xsd">

    <context:annotation-config/>
    <import resource="spring-dao.xml"/>

    <bean id="userMapperImpl" class="com.heroc.dao.UserMapperImpl">
        <property name="sqlSession" ref="sqlSesson"/>
    </bean>

</beans>
```

测试类

```java
public class MyTest {
    @Test
    public void userMapperTest(){
        ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
        UserMapper userMapperImpl = context.getBean("userMapperImpl", UserMapper.class);
        List<User> users = userMapperImpl.selectAllUser();
    }
}
/*最后是执行不成功的，事务开启成功，失败则回滚
  如果不开启事务，那么会添加成功，但不能删除，删除会报错*/
```



#### 事务传播

事务的传播行为，默认值为 Propagation.REQUIRED。可以手动指定其他的事务传播行为，如下：

- **Propagation.REQUIRED**

如果当前存在事务，则加入该事务，如果当前不存在事务，则创建一个新的事务。

- **Propagation.SUPPORTS**

如果当前存在事务，则加入该事务；如果当前不存在事务，则以非事务的方式继续运行。

- **Propagation.MANDATORY**

如果当前存在事务，则加入该事务；如果当前不存在事务，则抛出异常。

- **Propagation.REQUIRES_NEW**

重新创建一个新的事务，如果当前存在事务，延缓当前的事务。

- **Propagation.NOT_SUPPORTED**

以非事务的方式运行，如果当前存在事务，暂停当前的事务。

- **Propagation.NEVER**

以非事务的方式运行，如果当前存在事务，则抛出异常。

- **Propagation.NESTED**

如果没有，就新建一个事务；如果有，就在当前事务中嵌套其他事务。



### 2、编程式事务

[编程式事务](http://mybatis.org/spring/zh/transactions.html#programmatic)

编程式事务，需要从源代码中去实现。不推荐使用！

使用TransactionTemplate或者直接使用底层的PlatformTransactionManager。对于编程式事务管理，spring推荐使用TransactionTemplate。

```java
public class AccountServiceImpl_Program implements IAccountService {

    // 注入 accountDao 利用setter方法注入属性
    private IAccountDao accountDao;

    public void setAccountDao(IAccountDao accountDao) {
        this.accountDao = accountDao;
    }

    // 注入 transactionTemplate 利用setter方法注入属性
    private TransactionTemplate transactionTemplate;

    public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    @Override
    public void addAccount(Account account) {
        accountDao.add(account);
    }
    @Override
    public Account findAccountByName(String name) {
        Account account = accountDao.selectOneByName(name);
        return account;
    }

    @Override
    public void transfer(Account from, Account to, Integer money) {
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                accountDao.out(from, money);
                int d = 1 / 0;
                accountDao.in(to, money);
            }
        });
    }

}
```



```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:context="http://www.springframework.org/schema/context"
    xsi:schemaLocation="
http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

    <context:property-placeholder location="classpath:jdbc.properties" />
    <!-- 配置c3p0连接池 -->
    <bean id="dataSource"
        class="com.mchange.v2.c3p0.ComboPooledDataSource">
        <property name="driverClass" value="${jdbc.driver}" />
        <property name="jdbcUrl" value="${jdbc.url}" />
        <property name="user" value="${jdbc.user}" />
        <property name="password" value="${jdbc.password}" />
    </bean>
    
    <!-- 在容器中声明jdbc模版，这里需要注入数据源 -->
    <bean id="jdbcTemplate" class="org.springframework.jdbc.core.JdbcTemplate">
        <property name="dataSource" ref="dataSource"></property>
    </bean>

    <!-- 此处声明Dao层类，用注解的方式将jdbcTemplate注入到了accountDao中，也可用其他方式 -->
    <bean id="accountDao" class="com.hao.tx.dao.impl.AccountDaoImpl"></bean>

    <!-- (三)、在业务层注入模板类管理事务 -->
    <!-- 此处声明业务层类，用setter的方式注入了accountDao 和 管理事务的模版 -->
    <bean id="accountService" class="com.hao.tx.service.impl.AccountServiceImpl_Program">
        <property name="accountDao" ref="accountDao"></property>
        <property name="transactionTemplate" ref="transactionTemplate"></property>
    </bean>
    
    
    <!-- (一)、注册事务管理器: -->
    
    <!-- 配置事务管理器 这里DataSourceXXX...可以用做Spring自己的或者MyBatis的事务管理器 -->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <!-- 需要注入连接池,通过连接池获得连接 -->
        <property name="dataSource" ref="dataSource" />
    </bean>
    
    <!-- (二)、注册事务模板类: -->
    <!-- 事务管理的模板 -->
    <bean id="transactionTemplate" class="org.springframework.transaction.support.TransactionTemplate">
        <property name="transactionManager" ref="transactionManager" />
    </bean>
</beans>
```



```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext1.xml")
public class TxAcTest001 {

    @Autowired
    @Qualifier("accountService") 
    private IAccountService accountService;

    @Test
    public void test01() {
        System.out.println("转账之前....");
        Account a = accountService.findAccountByName("txa");
        if (a != null) {
            System.out.println(a);
        }

        Account b = accountService.findAccountByName("txb");
        if (b != null) {
            System.out.println(b);
        }
        
        try {// 这里捕获转账时出现的异常
            System.out.println("开始转账.....");
            accountService.transfer(a, b, 100); //会出现异常
        } catch (ArithmeticException e) {
            e.printStackTrace();
        } finally {
            System.out.println("转账之后....");
            Account a_trans = accountService.findAccountByName("txa");
            if (a != null) {
                System.out.println(a_trans);
            }
            Account b_trans = accountService.findAccountByName("txb");
            if (b != null) {
                System.out.println(b_trans);
            }
        }
    }
}
```

