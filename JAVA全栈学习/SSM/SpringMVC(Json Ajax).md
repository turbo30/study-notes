# Spring MVC

M(Model) 模型：数据模型，通常模型对象负责在数据库中存取数据。

V(View) 视图：是应用程序中处理数据显示的部分。通常视图是依据模型数据创建的。

C(Controller) 控制：是应用程序中处理用户交互的部分。通常控制器负责从视图读取数据，控制用户输入，并向模型发送数据。

MVC **是一种设计规范**；是将业务逻辑、数据、显示分离的方法来组织代码

MVC 主要作用是**降低了视图与业务逻辑间的双向耦合**

MVC 不是设计模式，**是一种架构模式**



**特点：**

- 轻量级，易学
- 高效，基于请求响应的MVC框架
- 与Spring兼容性好，无缝结合
- 约定优于配置
- 功能强大：RESTful风格、数据校验、格式化、本地化、主题等
- 简洁灵活



## 一、SpringMVC 工作原理



### 1、DispatcherServlet 中心控制器

Spring的web框架围绕DispatcherServlet设计。DispatcherServlet的作用就是将请求分发到不同的处理器(处理请求的类或方法)。从Spring 2.5开始，使用Java5或以上版本的用户采用基于注解的controller声明方式。

> 个人对DispatcherServlet 的简单理解：
>
> 由于在java web中，一个请求对应一个servlet，用户输入一个请求之后，就会去调用一个servlet，导致servlet会很多，因此就提供了DispatcherServlet这个类来调度管理所有的servlet。



Spring MVC框架像许多其他MVC框架一样，**已请求为驱动，围绕一个中心Servlet分派请求及提供其他功能，DispatcherServlet是一个实际的Servlet(它继承了HttpServlet基类)**

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/DispatcherServlet.png)



### 2、MVC原理图解 ==重点==

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/MVC原理.png)

当发起请求时被前置的控制器拦截到请求，根据请求参数生成代理请求，找到请求对应的实际控制器(也就是处理该请求对应的java程序java类)，控制器处理请求，创建数据模型，访问数据库，将模型响应给中心控制器，控制器使用模型与视图渲染视图结果，将结果返回给中心控制器，再将结果返回给请求者。



### 3、一个简单MVC项目

项目结构

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/mvc示例01.png" style="float:left" />

1. 写一个controller处理类

   ```java
   public class HelloController implements Controller {
       @Override
       public ModelAndView handleRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
           // modelAndView 模型和视图
           ModelAndView modelAndView = new ModelAndView();
   
           // 封装对象 ，放在ModelAndView中，Model
           modelAndView.addObject("msg","HelloSpringMVC!");
           // 封装要跳转的视图，放在ModelAndView中
           modelAndView.setViewName("hello"); // 会去springmvc-servlet.xml中配置的进行拼接 /WEB-INF/jsp/hello.jsp
   
           return modelAndView;
       }
   }
   ```

2. 创建一个spring配置文件springmvc-servlet.xml

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://www.springframework.org/schema/beans
   			https://www.springframework.org/schema/beans/spring-beans.xsd">
   
       <bean class="org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping"/>
       <bean class="org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter"/>
   
       <!--视图解析器DispatcherServlet给他的ModeAndView-->
       <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver" id="internalResourceViewResolver">
           <!--前缀-->
           <property name="prefix" value="/WEB-INF/jsp/"/>
           <!--后缀-->
           <property name="suffix" value=".jsp"/>
       </bean>
   
       <!--这里注册controller类，name就是请求路径-->
       <bean name="/hello" class="com.heroc.controller.HelloController"/>
   </beans>
   ```

3. 在web.xml中注册DispatcherServlet，用于管理所有的servlet

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
            version="4.0">
   
       <!--注册DispatcherServlet-->
       <servlet>
           <servlet-name>springmvc</servlet-name>
           <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
           <!--关联一个springmvc的配置文件【servlet-name】-servlet.xml-->
           <init-param>
               <param-name>contextConfigLocation</param-name>
               <param-value>classpath:springmvc-servlet.xml</param-value>
           </init-param>
           <!--启动级别为1-->
           <load-on-startup>1</load-on-startup>
       </servlet>
   
       <!--/ 匹配所有请求(不包括.jsp)-->
       <!--/* 匹配所有请求(包括.jsp)-->
       <servlet-mapping>
           <servlet-name>springmvc</servlet-name>
           <url-pattern>/</url-pattern>
       </servlet-mapping>
   </web-app>
   ```

4. jsp/hello.jsp

   ```jsp
   <%@ page contentType="text/html;charset=UTF-8" language="java" %>
   <html>
   <head>
       <title>HelloSpringMVC</title>
   </head>
   <body>
   ${msg} <%--获取ModeAndView中存储的数据msg--%>
   </body>
   </html>
   ```

5. 结果

   ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/mvc示例01结果.png)



####  / 和 /* 的区别

< url-pattern>/\</url-pattern>  会匹配到/login这样的路径型url，不会匹配到模式为*.jsp这样的后缀型url

< url-pattern>/\*\</url-pattern> 会匹配所有url：路径型的和后缀型的url(包括/login,*.jsp,*.js和*.html等)



### 4、MVC执行原理 ==重点==

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/MVC执行原理.png)

图为SpringMVC的一个较完整的流程图，实线表示SpringMVC框架提供的技术，不需要开发者实现，虚线表示需要开发者实现。

**简要分析执行流程**

1. DispatcherServlet表示前置控制器，是整个SpringMVC的控制中心。用户发出请求，DispatcherServlet接收请求并拦截请求。

   > 我们假设请求的url为 : `http://localhost:8080/hello`
   >
   > **如上url拆分成三部分：**
   >
   > `http://localhost:8080`服务器域名
   >
   > SpringMVC部署在服务器上的web站点
   >
   > hello表示控制器
   >
   > 通过分析，如上url表示为：请求位于服务器localhost:8080上的SpringMVC站点的hello控制器。

2. HandlerMapping为处理器映射。DispatcherServlet调用HandlerMapping,HandlerMapping根据请求url查找Handler。

3. HandlerExecution表示具体的Handler,其主要作用是根据url查找控制器，如上url被查找控制器为：hello。

4. HandlerExecution将解析后的信息传递给DispatcherServlet,如解析控制器映射等。

5. HandlerAdapter表示处理器适配器，其按照特定的规则去执行Handler。

6. Handler让具体的Controller执行。

7. Controller将具体的执行信息返回给HandlerAdapter,如ModelAndView。

8. HandlerAdapter将视图逻辑名或模型传递给DispatcherServlet。

9. DispatcherServlet调用视图解析器(ViewResolver)来解析HandlerAdapter传递的逻辑视图名。

10. 视图解析器将解析的逻辑视图名传给DispatcherServlet。

11. DispatcherServlet根据视图解析器解析的视图结果，调用具体的视图。

12. 最终视图呈现给用户。




#### SpringMVC 四剑客 ==重点==

> Spring MVC 重要的四剑客：
>
> - 调配管理Servlet：DispatcherServlet
>
> - 处理器映射器：...HandlerMapping (不止这一种) 用于匹配请求路径
> - 处理器适配器：...HandlerAdapter (不止这一种) 用于匹配controller类返回ModeAndView
> - 视图解析器：InternalResourceViewResolver (这时springmvc提供的视图解析器，也可使用模板引擎thymeleaf等) 用于将ModeAndView中的数据与视图进行解析合并



## 二、使用注解开发 （实际开发过程）

#### 注解开发示例

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/mvc示例02.png" style="float:left" />

1. 配置web.xml，将DispatcherServlet配置上

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
            version="4.0">
   
       <servlet>
           <servlet-name>springmvc</servlet-name>
           <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
           <init-param>
               <param-name>contextConfigLocation</param-name>
               <param-value>classpath:springmvc-servlet.xml</param-value>
           </init-param>
       </servlet>
       <servlet-mapping>
           <servlet-name>springmvc</servlet-name>
           <url-pattern>/</url-pattern>
       </servlet-mapping>
   
   </web-app>
   ```

2. 配置springmvc-servlet.xml文件

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns:context="http://www.springframework.org/schema/context"
          xmlns:mvc="http://www.springframework.org/schema/mvc"
          xsi:schemaLocation="http://www.springframework.org/schema/beans
   			https://www.springframework.org/schema/beans/spring-beans.xsd
   			http://www.springframework.org/schema/context
   			https://www.springframework.org/schema/context/spring-context.xsd
   			http://www.springframework.org/schema/mvc
   			https://www.springframework.org/schema/mvc/spring-mvc.xsd">
   
       <!--自动扫描包，让包下的注解生效，自动在IOC容器中注册-->
       <context:component-scan base-package="com.heroc.controller"/>
       <!--让SpringMVC不处理静态资源，比如.css .js .mp4 .mp3等-->
       <mvc:default-servlet-handler/>
       <!--该配置也是放行指定的静态资源-->
       <mvc:resources mapping="/jsp/**" location="/WEB-INF/jsp"/>
       <!--支持mvc注解驱动
           在spring中一般采用@RrequestMapping注解来完成映射关系
           要想使用该注解，必须向上下文注册
           DefaultAnnotationHandlerMapping和AnnotationMethodHandlerAdapter实例
           这两个实例分别在类级别和方法级别处理
           而annotation-driven配置会帮我们自动完成上两个实例的注册和注入-->
       <mvc:annotation-driven/>
   
       <!--视图解析器-->
       <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver" id="InternalResourceViewResolver">
           <property name="prefix" value="/WEB-INF/jsp/"/>
           <property name="suffix" value=".jsp"/>
       </bean>
   
   </beans>
   ```

3. 写一个处理类

   ```java
   @Controller // 将该方法注册到IOC容器中，是Controller
   // @RestController 直接返回数据，不会经过视图解析器
   // @RequestMapping("/03")
   public class AnnotationController {
   
       // 如果类上加了@RequestMapping("/03")，请求路径应该为 localhost:8080/03/annotation
       // 03/annotation有一个父子关系
       @RequestMapping("/annotation")
       public String annotationController(Model model){
           // ModelAndView中的model模型 封装数据
           model.addAttribute("msg","Hello,AnnotationController!"); 
           return "annotation"; // ModelAndView中的view模型，会走视图解析器，拼接找到视图文件
       }
   }
   ```

4. annotation.jsp

   ```jsp
   <%@ page contentType="text/html;charset=UTF-8" language="java" %>
   <html>
   <head>
       <title>AnnotationController</title>
   </head>
   <body>
   ${msg}
   </body>
   </html>
   ```

5. 结果

   <img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/mvc示例02结果.png" style="float:left" />

#### MVC 常用注解

```java
类作用域：
@Controller // 在ioc容器注册，是一个controller控制器，处理请求的
@RestController // 与controller类似，只不过该注解不走视图解析器，直接返回数据到页面
方法作用域：
@RequestMapping // 解析url映射
@GetMapping  @PostMapping
@ResponseBody // 不会走视图解析器，会直接返回一个字符串
变量作用域：
@PathVariable // restful风格，在路径中获取变量
```





## 三、RestFul 风格

### 1、概念

Restful就是一个资源定位及资源操作的风格。不是标准也不是协议，只是一种风格。基于这个风格设计的软件可以更简洁，更有层次，更易于实现缓存等机制。



### 2、功能

资源：互联网所有的事物都可以被抽象为资源

资源操作：使用POST、DELETE、PUT、GET，使用不同方法对资源进行操作。

分别对应 添加、 删除、修改、查询。

**传统方式操作资源**  ：通过不同的参数来实现不同的效果！方法单一，post 和 get

​	`http://127.0.0.1/item/queryItem.action?id=1` 查询,GET

​	`http://127.0.0.1/item/saveItem.action `新增,POST

​	`http://127.0.0.1/item/updateItem.action `更新,POST

​	`http://127.0.0.1/item/deleteItem.action?id=1` 删除,GET或POST

**使用RESTful操作资源** ：可以通过不同的请求方式来实现不同的效果！如下：请求地址一样，但是功能可以不同！

​	`http://127.0.0.1/item/1 `查询,GET

​	`http://127.0.0.1/item` 新增,POST

​	`http://127.0.0.1/item`更新,PUT

​	`http://127.0.0.1/item/1` 删除,DELETE



### 3、通过例子了解Restful风格

```java
@Controller
public class RestfulController {

    /*@RequestMapping中有很多属性，value为请求路径; method是一个枚举类型，指定请求方法*/
    @RequestMapping(value = "/restful/{a}/{b}", method = RequestMethod.GET)
    /*@PathVariable 是为了使用restful风格而用于获取路径中变量的注解，路径中用{}括起来的变量与参数变量名字要一一对应*/
    public String restful(@PathVariable int a, @PathVariable int b, Model model){
        int res = a + b;
        model.addAttribute("msg","结果为：" + res);
        return "restful";
    }
}
```

在Spring MVC中可以使用  @PathVariable 注解，让方法参数的值对应绑定到一个URI模板变量上。

结果：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/restful01.png)

> 使用Restful风格的优点
>
> - 使路径变得更加简洁；
> - 获得参数更加方便，框架会自动进行类型转换。
> - 通过路径变量的类型可以约束访问参数，如果类型不一样，则访问不到对应的请求方法，如这里访问是的路径是/restful/1/a，则路径与方法不匹配，而不会是参数转换失败。



**使用@PathVariable(路径变量)的method属性指定请求类型**

用于约束请求的类型，可以收窄请求范围。指定请求谓词的类型如GET, POST, HEAD, OPTIONS, PUT, PATCH, DELETE, TRACE等。默认为GET



方法级别的注解变体有如下几个：组合注解

```
@GetMapping
@PostMapping
@PutMapping
@DeleteMapping
@PatchMapping
```

@GetMapping 是一个组合注解，平时使用的会比较多！

它所扮演的是 @RequestMapping(method =RequestMethod.GET) 的一个快捷方式。



## 四、MVC 重定向与转发



### 1、没有视图解析器的情况

```java
@Controller
public class RestfulController {

    @GetMapping("/t1")
    public String test1(Model model){
        model.addAttribute("msg","结果"s);
        // 转发
        return "/restful.html";
    }
    
    @GetMapping("/t2")
    public String test2(){
        model.addAttribute("msg","结果");
        // 转发
        return "forward:/restful.html";
    }
    
    @GetMapping("/t3")
    public String test3(Model model){
        model.addAttribute("msg","结果");
        // 重定向
        return "redirect:/restful.html";
    }
}
```



### 2、有视图解析器的情况

```java
@Controller
public class RestfulController {

    @GetMapping("/t1")
    public String test1(Model model){
        model.addAttribute("msg","结果"s);
        // 转发
        return "restful";
    }
    
    @GetMapping("/t2")
    public String test2(Model model){
        model.addAttribute("msg","结果");
        // 重定向
        return "redirect:/restful.html";
    }
}
```



## 五、数据处理

### 1、数据处理

#### 1）请求中的变量名与参数一致

提交请求：`localhost:8080/t1?name=heroC`

处理：

```java
@GetMapping("/t1")
public String test01(String name, Model model){
    model.addAttribute("msg",name);
    return "index";
}
```

当请求中的变量名与参数一致，可以直接从请求中获取变量的值复制给方法的同名参数



#### 2）请求中的变量名与参数不一致

提交请求：`localhost:8080/t1?username=heroC`

处理：

```java
@GetMapping("/t1")
public String test01(@RequestParam("username") String name, Model model){
    model.addAttribute("msg",name);
    return "index";
}
```

当请求中的变量名与参数不一致，需要使用@RequestParam注解，指定请求中的参数名



#### 3）当方法参数是一个对象

User.java

```java
public class User{
    private int id;
    private String name;
    private int age;
    
    ...
}
```

提交请求：`localhost:8080/t1?id=001&name=heroC&age=21`

处理：

```java
@GetMapping("/t1")
public String test01(User user, Model model){
    model.addAttribute("msg",user.toString());
    return "index";
}
```

需要注意的是：提交的请求参数名必须与对象的属性名一致，否则无法给对象的属性赋值。



### 2、前端显示数据

#### 1）ModelAndView 类

通过是实现Controller接口

```JAVA
public class ControllerTest1 implements Controller {

   public ModelAndView handleRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
       //返回一个模型视图对象
       ModelAndView mv = new ModelAndView();
       mv.addObject("msg","ControllerTest1");
       mv.setViewName("test");
       return mv;
  }
}
```



#### 2）ModelMap 类

```java
@RequestMapping("/hello")
public String hello(@RequestParam("username") String name, ModelMap model){
   //封装要显示到视图中的数据
   //相当于req.setAttribute("name",name);
   model.addAttribute("name",name);
   System.out.println(name);
   return "hello";
}
```



#### 3）Model 类

```java
@RequestMapping("/ct2/hello")
public String hello(@RequestParam("username") String name, Model model){
   //封装要显示到视图中的数据
   //相当于req.setAttribute("name",name);
   model.addAttribute("msg",name);
   System.out.println(name);
   return "test";
}
```



#### 4）简单对比



- Model 只有寥寥几个方法只适合用于储存数据，简化了新手对于Model对象的操作和理解；
- ModelMap 继承了 LinkedMap ，除了实现了自身的一些方法，同样的继承 LinkedMap 的方法和特性
- ModelAndView 可以在储存数据的同时，可以进行设置返回的逻辑视图，进行控制展示层的跳转。



### 3、解决乱码问题

**第一种方式：**

在web.xml中配置：

```xml
<!-- springMVC指定的过滤器 -->
<filter>
   <filter-name>encoding</filter-name>
   <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
   <init-param>
       <param-name>encoding</param-name>
       <param-value>utf-8</param-value>
   </init-param>
</filter>
<filter-mapping>
   <filter-name>encoding</filter-name>
   <url-pattern>/*</url-pattern>
</filter-mapping>
```

有些极端情况下.这个过滤器对get的支持不好 .



**第二种方式：**

网上找到，比较全的去解决乱码的过滤器

```java
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
* 解决get和post请求 全部乱码的过滤器
*/
public class GenericEncodingFilter implements Filter {

   @Override
   public void destroy() {
  }

   @Override
   public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
       //处理response的字符编码
       HttpServletResponse myResponse=(HttpServletResponse) response;
       myResponse.setContentType("text/html;charset=UTF-8");

       // 转型为与协议相关对象
       HttpServletRequest httpServletRequest = (HttpServletRequest) request;
       // 对request包装增强
       HttpServletRequest myrequest = new MyRequest(httpServletRequest);
       chain.doFilter(myrequest, response);
  }

   @Override
   public void init(FilterConfig filterConfig) throws ServletException {
  }

}

//自定义request对象，HttpServletRequest的包装类
class MyRequest extends HttpServletRequestWrapper {

   private HttpServletRequest request;
   //是否编码的标记
   private boolean hasEncode;
   //定义一个可以传入HttpServletRequest对象的构造函数，以便对其进行装饰
   public MyRequest(HttpServletRequest request) {
       super(request);// super必须写
       this.request = request;
  }

   // 对需要增强方法 进行覆盖
   @Override
   public Map getParameterMap() {
       // 先获得请求方式
       String method = request.getMethod();
       if (method.equalsIgnoreCase("post")) {
           // post请求
           try {
               // 处理post乱码
               request.setCharacterEncoding("utf-8");
               return request.getParameterMap();
          } catch (UnsupportedEncodingException e) {
               e.printStackTrace();
          }
      } else if (method.equalsIgnoreCase("get")) {
           // get请求
           Map<String, String[]> parameterMap = request.getParameterMap();
           if (!hasEncode) { // 确保get手动编码逻辑只运行一次
               for (String parameterName : parameterMap.keySet()) {
                   String[] values = parameterMap.get(parameterName);
                   if (values != null) {
                       for (int i = 0; i < values.length; i++) {
                           try {
                               // 处理get乱码
                               values[i] = new String(values[i]
                                      .getBytes("ISO-8859-1"), "utf-8");
                          } catch (UnsupportedEncodingException e) {
                               e.printStackTrace();
                          }
                      }
                  }
              }
               hasEncode = true;
          }
           return parameterMap;
      }
       return super.getParameterMap();
  }

   //取一个值
   @Override
   public String getParameter(String name) {
       Map<String, String[]> parameterMap = getParameterMap();
       String[] values = parameterMap.get(name);
       if (values == null) {
           return null;
      }
       return values[0]; // 取回参数的第一个值
  }

   //取所有值
   @Override
   public String[] getParameterValues(String name) {
       Map<String, String[]> parameterMap = getParameterMap();
       String[] values = parameterMap.get(name);
       return values;
  }
}
```

web.xml

```xml
<filter>
   <filter-name>encoding</filter-name>
   <filter-class>XXXXXX.GenericEncodingFilter</filter-class>
</filter>
<filter-mapping>
   <filter-name>encoding</filter-name>
   <url-pattern>/*</url-pattern>
</filter-mapping>
```



## 六、JSON

JSON(JavaScript Object Notation, JS 对象简谱) 是一种轻量级的数据交换格式。它基于 ECMAScript (欧洲计算机协会制定的js规范)的一个子集

- 采用完全独立于编程语言的文本格式来存储和表示数据。
- 简洁和清晰的层次结构使得 JSON 成为理想的数据交换语言。
- 易于人阅读和编写，同时也易于机器解析和生成，并有效地提升网络传输效率。



JSON要求的语法格式：

- 对象表示为键值对，数据由逗号分隔
- 花括号保存对象
- 方括号保存数组



### 1、前端JSON对象的转换

```html
<script>
    // JS对象
    var user = {
        name:"heroC",
        age:21,
        sex:"男"
    };
    console.log(user);

    // 将JS对象转换为json
    var json = JSON.stringify(user);
    console.log(json);

    // 将json转换为js对象
    var parse = JSON.parse(json);
    console.log(parse);
</script>
```

浏览器控制台输出结果：

<img src="https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/前端json.png" style="float:left" />



### 2、Controller返回JSON数据

#### 1）使用Jackson依赖实现

- Jackson是目前比较好的json解析工具，还有阿里巴巴的fastjson

**导入Jackson的依赖包**

```xml
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>2.10.0</version>
</dependency>
```

**配置web.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">

    <servlet>
        <servlet-name>springmvc</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:springmvc-config.xml</param-value>
        </init-param>
    </servlet>
    <servlet-mapping>
        <servlet-name>springmvc</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>

    <filter>
        <filter-name>encoding</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>utf-8</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>encoding</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
</web-app>
```

配置springmvc-config.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd
			http://www.springframework.org/schema/context
			https://www.springframework.org/schema/context/spring-context.xsd
			http://www.springframework.org/schema/mvc
			https://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <context:component-scan base-package="com.heroc.controller"/>
    <mvc:annotation-driven/>
    <mvc:default-servlet-handler/>

    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver" id="internalResourceViewResolver">
        <property name="prefix" value="/WEB-INF/jsp/"/>
        <property name="suffix" value=".jsp"/>
    </bean>

</beans>
```

User.java

```java
public class User {
    private String name;
    private int age;
    private String sex;
    ...
}
```

JSONContoller.java

```java
@Controller
public class JSONController {

    @GetMapping("/json")
    @ResponseBody // 不会走视图解析器，会直接返回一个字符串,到前端
    public String jsonTest(Model model){
        User user = new User("heroC",21,"男");
        return user.toString();
    }
}
```

结果：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/json示例01.png)

已经成功输出了user对象，但是还不是jsong格式，接下来在Controller中使用jackson依赖

JSONController.java

```java
@Controller
public class JSONController {

    @GetMapping("/json")
    @ResponseBody // 不会走视图解析器，会直接返回一个字符串
    public String jsonTest(Model model) throws JsonProcessingException {
        User user = new User("heroC",21,"男");
        // 使用jackson中的ObjectMapper的writeValueAsString方法可以将其转换成json
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(user);
        return json;
    }
}
```

结果：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/json示例02.png)

已成功转换成了json格式，但是发现中文乱码了，这里解决乱码问题

##### 解决json乱码问题

**1、**使用@...Mapping注解解决，这些注解中有一个produces属性，可以解决json乱码

```java
@RequestMapping(value = "/json", produces = "application/json;charset=utf-8")
或者get请求：
@GetMapping(value = "/json", produces = "application/json;charset=utf-8")
或者post请求
@PostMapping(value = "/json", produces = "application/json;charset=utf-8")
```

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/json示例03.png)

**2、**由于直接在注解中设置编码，会很麻烦，这样每一个方法的注解都可能会写这样一句话，因此MVC也为我们想到了这个问题，直接在xml中配置即可，就不用直接在注解中配置produces属性了

```xml
<!--固定不变的-->
<mvc:annotation-driven>
    <mvc:message-converters register-defaults="true">
        <bean class="org.springframework.http.converter.StringHttpMessageConverter">
            <constructor-arg value="UTF-8"/>
        </bean>
        <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
            <property name="objectMapper">
                <bean class="org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean">
                    <property name="failOnEmptyBeans" value="false"/>
                </bean>
            </property>
        </bean>
    </mvc:message-converters>
</mvc:annotation-driven>
```



##### TPS: 提炼重复代码作为工具类

由于每个方法如果要返回一个json数据时，都要去重复写ObjectMapper这样的类，去转换成json格式。因此可以用一个类去封装成一个工具类。

```java
public class JsonUtils {
    // 将一个对象转换为json格式
    public static String getJson(Object object){
        return getJson(object,"yyyy-MM-dd HH:mm:ss");
    }

    // 可获得json格式的数据，也可以获得设置了格式的json格式的date时间数据
    public static String getJson(Object object, String dateFormat){
        ObjectMapper objectMapper = new ObjectMapper();
        // objectMapper传入date默认为时间戳显示，这里将时间戳设置为false，停用
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,false);
        // 创建一个日期格式
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        // 给objectMapper配置一个日期类型的格式
        objectMapper.setDateFormat(simpleDateFormat);
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
```

这只是，展示可以将重用代码归并到一个工具类中，要使用直接调用即可的思想。



#### 2）使用fastjson依赖实现

- fastjson是阿里巴巴开发的json格式转换的框架。导入fastjson依赖包

```xml
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
    <version>1.2.68</version>
</dependency>
```



fastjson 三个主要的类：

**JSONObject  代表 json 对象** 

- JSONObject实现了Map接口, 猜想 JSONObject底层操作是由Map实现的。
- JSONObject对应json对象，通过各种形式的get()方法可以获取json对象中的数据，也可利用诸如size()，isEmpty()等方法获取"键：值"对的个数和判断是否为空。其本质是通过实现Map接口并调用接口中的方法完成的。

**JSONArray  代表 json 对象数组**

- 内部是有List接口中的方法来完成操作的。

**JSON代表 JSONObject和JSONArray的转化**

- JSON类源码分析与使用
- 仔细观察这些方法，主要是实现json对象，json对象数组，javabean对象，json字符串之间的相互转化。

```java
public class FastJsonDemo {
   public static void main(String[] args) {
       //创建一个对象
       List<User> list = new ArrayList<>();
       userList.add(new User("heroC1",21,"男"));
       userList.add(new User("heroC2",21,"男"));
       userList.add(new User("heroC3",21,"男"));
       userList.add(new User("heroC4",21,"男"));
       User user2 = new User("heroC2",21,"男");

       System.out.println("*******Java对象 转 JSON字符串*******");
       String str1 = JSON.toJSONString(list);
       System.out.println("JSON.toJSONString(list)==>"+str1);

       System.out.println("\n****** JSON字符串 转 Java对象*******");
       User jp_user1=JSON.parseObject(str1,User.class);
       System.out.println("JSON.parseObject(str1,User.class)==>"+jp_user1);

       System.out.println("\n****** Java对象 转 JSON对象 ******");
       JSONObject jsonObject1 = (JSONObject) JSON.toJSON(user2);
       System.out.println("(JSONObject) JSON.toJSON(user2)==>"+jsonObject1.getString("name"));

       System.out.println("\n****** JSON对象 转 Java对象 ******");
       User to_java_user = JSON.toJavaObject(jsonObject1, User.class);
       System.out.println("JSON.toJavaObject(jsonObject1, User.class)==>"+to_java_user);
  }
}
```

这种工具类，我们只需要掌握使用就好了，在使用的时候在根据具体的业务去找对应的实现。和以前的commons-io那种工具包一样，拿来用就好了！



##### fastjson 处理中文乱码

```java
@RequestMapping(value = "/test/esQuery",produces = { "application/json;charset=UTF-8" })
```



### 3、给前端返回格式化的日期，解决返回时间戳问题

#### 方式一：注解

导入jackson依赖包，使用jackson的注释：

```java
@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
```



例如，pojo类中使用：

```java
@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
public Date getCreateTime() {
    return createTime;
}
```

这样在返回给前端的json对象中，时间就不是时间戳了，而是被格式化了的时间。



#### 方式二：自定以JsonUtils工具类

```java
public class JsonUtils {
    // 将一个对象转换为json格式
    public static String getJson(Object object){
        return getJson(object,"yyyy-MM-dd HH:mm:ss");
    }

    // 可获得json格式的数据，也可以获得设置了格式的json格式的date时间数据
    public static String getJson(Object object, String dateFormat){
        ObjectMapper objectMapper = new ObjectMapper();
        // objectMapper传入date默认为时间戳显示，这里将时间戳设置为false，停用
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,false);
        // 创建一个日期格式
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        // 给objectMapper配置一个日期类型的格式
        objectMapper.setDateFormat(simpleDateFormat);
        try {
            // 返回json对象
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
```





## 七、Ajax



### 1、简介

- **AJAX = Asynchronous JavaScript and XML（异步的 JavaScript 和 XML）。**
- AJAX 是一种在无需重新加载整个网页的情况下，能够更新部分网页的技术。
- **Ajax 不是一种新的编程语言，而是一种用于创建更好更快以及交互性更强的Web应用程序的技术。**



### 2、jQuery的Ajax

纯JS原生实现Ajax我们不去讲解，这里直接使用jquery提供的，方便学习和使用，避免重复造轮子，有兴趣的同学可以去了解下JS原生XMLHttpRequest ！

Ajax的核心是XMLHttpRequest对象(XHR)。XHR为向服务器发送请求和解析服务器响应提供了接口。能够以异步方式从服务器获取新数据。

jQuery 提供多个与 AJAX 有关的方法。

通过 jQuery AJAX 方法，您能够使用 HTTP Get 和 HTTP Post 从远程服务器上请求文本、HTML、XML 或 JSON – 同时您能够把这些外部数据直接载入网页的被选元素中。

jQuery 不是生产者，而是大自然搬运工。

jQuery Ajax本质就是 XMLHttpRequest，对他进行了封装，方便调用！

```
jQuery.ajax(...)
      部分参数：
            url：请求地址
            type：请求方式，GET、POST（1.9.0之后用method）
        headers：请求头
            data：要发送的数据
    contentType：即将发送信息至服务器的内容编码类型(默认: "application/x-www-form-urlencoded; charset=UTF-8")
          async：是否异步
        timeout：设置请求超时时间（毫秒）
      beforeSend：发送请求前执行的函数(全局)
        complete：完成之后执行的回调函数(全局)
        success：成功之后执行的回调函数(全局)
          error：失败之后执行的回调函数(全局)
        accepts：通过请求头发送给服务器，告诉服务器当前客户端可接受的数据类型
        dataType：将服务器端返回的数据转换成指定类型
          "xml": 将服务器端返回的内容转换成xml格式
          "text": 将服务器端返回的内容转换成普通文本格式
          "html": 将服务器端返回的内容转换成普通文本格式，在插入DOM中时，如果包含JavaScript标签，则会尝试去执行。
        "script": 尝试将返回值当作JavaScript去执行，然后再将服务器端返回的内容转换成普通文本格式
          "json": 将服务器端返回的内容转换成相应的JavaScript对象
        "jsonp": JSONP 格式使用 JSONP 形式调用函数时，如 "myurl?callback=?" jQuery 将自动替换 ? 为正确的函数名，以执行回调函数
```



### 3、Ajax简单示例

web.xml

```XML
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">

    <servlet>
        <servlet-name>dispatcher</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:spring-mvc.xml</param-value>
        </init-param>
    </servlet>
    <servlet-mapping>
        <servlet-name>dispatcher</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>

    <filter>
        <filter-name>encoding</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>UTF-8</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>encoding</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

</web-app>
```

spring-mvc.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
			https://www.springframework.org/schema/beans/spring-beans.xsd
			http://www.springframework.org/schema/context
			https://www.springframework.org/schema/context/spring-context.xsd
			http://www.springframework.org/schema/mvc
			https://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <context:component-scan base-package="com.heroc.controller"/>
    <mvc:default-servlet-handler/>
    <mvc:annotation-driven/>

    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver" id="internalResourceViewResolver">
        <property name="prefix" value="/jsp/"/>
        <property name="suffix" value=".jsp"/>
    </bean>
    
</beans>
```

Ajax.java

```java
@RestController
public class AjaxController {

    @RequestMapping("/test")
    public void ajaxTest(String name, HttpServletResponse response) throws IOException {
        if("heroC".equals(name)){
            response.getWriter().print("true");
        }else {
            response.getWriter().print("false");
        }
    }

}
```

index.jsp

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>AjaxTest</title>
  <script src="https://cdn.bootcss.com/jquery/3.4.1/jquery.min.js"></script>
</head>
<script>
  function aTest() {
    $.ajax({
      url:"${pageContext.request.contextPath}/test",
      data:{"name":$("#ajaxtest").val()},
      /*success里的data是接收的response响应对象里的数据*/
      success:function (data) {
        alert(data);
      }
    })
  }
</script>
<body>
<input id="ajaxtest" type="text" onblur="aTest()">
</body>
</html>
```

实现结果：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/ajax01.png)

Type为**xhr**(xmlHttpResquest)，就是ajax



### 4、Ajax加载数据示例

添加一个pojo层，**User.java**类 这里get/set等方法省略

```java
public class User {
    private String name;
    private int age;
    private String sex;
    ...
}
```

在**AjaxController.java**中添加：

```java
@RequestMapping("/ajaxshow")
public List<User> ajaxShow(){
    List<User> list = new ArrayList<>();
    list.add(new User("heroC",21,"男"));
    list.add(new User("yikeX",21,"女"));
    list.add(new User("张还行",21,"男"));
    return list;
}
```

**ajaxShow.jsp**

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>通过ajax展示后端传递的数据</title>
    <script src="https://cdn.bootcss.com/jquery/3.4.1/jquery.min.js"></script>
</head>
<script>
$(function () {
    $("#btn").click(function () {
        $.ajax({
            url:"${pageContext.request.contextPath}/ajaxshow",
            success:function (data) {
                var html;
                /*let关键字是js的新特性，变量作用域只在方法体内*/
                for (let i =  0; i < data.length; i++){
                    html += "<tr>"
                        +"<td>"+data[i].name+"</td>"
                        +"<td>"+data[i].age+"</td>"
                        +"<td>"+data[i].sex+"</td>"
                        +"</tr>"
                }
                $("#content").html(html);
            }
        });
    })
})
</script>
<body>
<button id="btn">加载数据</button>
<div>
    <table>
        <tr>
            <th>姓名</th>
            <th>年龄</th>
            <th>性别</th>
        </tr>
        <tbody id="content"></tbody>
    </table>
</div>
</body>
</html>
```

结果：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/ajax02.png)



### 5、Ajax验证用户体验

login.jsp

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录验证页面</title>
    <script src="https://cdn.bootcss.com/jquery/3.4.1/jquery.min.js"></script>
</head>
<script>
    function userName() {
        $.ajax({
            url:"${pageContext.request.contextPath}/username",
            data:{"name":$("#username").val()},
            /*success里的data是接收的response响应对象里的数据*/
            success:function (data) {
                if(data.toString()==="200"){
                    $("#nameinfo").css("color","green").html("√");
                }else {
                    $("#nameinfo").css("color","red").html("X");
                }
            }
        })
    }
    function pwd() {
        $.ajax({
            url:"${pageContext.request.contextPath}/pwd",
            data:{"pwd":$("#pwd").val()},
            /*success里的data是接收的response响应对象里的数据*/
            success:function (data) {
                if(data.toString()==="200"){
                    $("#pwdinfo").css("color","green").html("√");
                }else {
                    $("#pwdinfo").css("color","red").html("X");
                }
            }
        })
    }
</script>
<body>
<label for="username">用户名</label>
<p>
    <input id="username" type="text" onblur="userName()">
    <span id="nameinfo"></span>
</p>
<label for="pwd">密码</label>
<p>
    <input id="pwd" type="password" onblur="pwd()">
    <span id="pwdinfo"></span>
</p>
</body>
</html>
```

在AjaxController.java添加：

```java
@RequestMapping("/username")
public String loginName(String name){
    String msg;
    if(name.equals("heroC")){
        msg = "200";
    }else {
        msg = "404";
    }
    return msg;
}

@RequestMapping("/pwd")
public String loginPwd(String pwd){
    String msg;
    if (pwd != null && !pwd.equals("")){
        msg = "200";
    }else {
        msg = "404";
    }
    return msg;
}
```

如果使用中文json乱码，在spring-mvc.xml中添加以下配置：

```xml
<mvc:annotation-driven>
    <mvc:message-converters>
        <bean class="org.springframework.http.converter.StringHttpMessageConverter" id="httpMessageConverter">
            <constructor-arg index="0" value="UTF-8"/>
        </bean>
        <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
            <property name="objectMapper">
                <bean class="org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean">
                    <property name="failOnEmptyBeans" value="false"/>
                </bean>
            </property>
        </bean>
    </mvc:message-converters>
</mvc:annotation-driven>
```



## 八、拦截器



### 1、概述

SpringMVC的处理器拦截器类似于Servlet开发中的过滤器Filter,用于对处理器进行预处理和后处理。开发者可以自己定义一些拦截器来实现特定的功能。

**过滤器与拦截器的区别** ：mvc的拦截器是**AOP思想的具体应用**

**过滤器**

- servlet规范中的一部分，任何java web工程都可以使用
- 在url-pattern中配置了/*之后，可以对所有要访问的资源进行拦截

**拦截器** 

- 拦截器是SpringMVC框架自己的，只有使用了SpringMVC框架的工程才能使用
- 拦截器只会拦截访问的控制器方法(在控制器方法上AOP)， 如果访问的是jsp/html/css/image/js是不会进行拦截的



### 2、自定义拦截器

配置好环境之后，建立一个config层，用于放置一些配置类

继承HandlerInterceptor接口，重写方法。

**MyInterceptor.java**

```java
public class MyInterceptor implements HandlerInterceptor {
    // 一般重写第一个方法，就可以了
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // return true , 经过该方法进行处理之后，不会被拦截，放行
        // return false， 经过该方法处理之后，会被拦截，不会被放行
        System.out.println("拦截前。。。");
        return true;
    }

    // 以下两个方法，可用作日志处理
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("请求响应完成，拦截后被放行执行完之后，执行该方法");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("最终的拦截处理");
    }
}
```

在spring-mvc.xml配置拦截器

```xml
<mvc:interceptors>
    <mvc:interceptor>
        <!--/** 是拦截映射路径/*下的所有*路径，所有映射都会经过以下拦截器配置-->
        <mvc:mapping path="/**"/>
        <bean class="com.heroc.config.MyInterceptor"/>
    </mvc:interceptor>
</mvc:interceptors>
```



运行结果：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/interceptor01.png)

页面会返回一个ok的字符串。

如果自定义拦截器第一个方法`return false;` ，页面不会显示ok，并且运行结果为：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForSpringMVC/interceptor02.png)

因为第一个方法拦截了，所以，后面的都不会执行。



### 3、登录验证示例



**MyController.java**

```java
@Controller
public class MyController {
    // 进入登录页面
    @RequestMapping("/toLogin")
    public String toLogin(){
        return "login";
    }

    // 进入首页
    @RequestMapping("/main")
    public String toMain(HttpServletRequest request, Model model){
        HttpSession session = request.getSession();
        model.addAttribute("name",session.getAttribute("username"));
        return "main";
    }

    // 登录表单的请求
    @PostMapping("/login")
    public String login(String username, String pwd, HttpSession session, Model model){
        session.setAttribute("username",username);
        model.addAttribute("name",session.getAttribute("username"));
        return "main";
    }

    // 注销
    @RequestMapping("/out")
    public String out(HttpSession session){
        session.removeAttribute("username");
        return "login";
    }
}
```

**LoginInterceptor.java**

```java
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 进入登录页面的URI 放行
        if(request.getRequestURI().contains("toLogin")){
            return true;
        }
        // 提交登录表单的URI 放行
        if(request.getRequestURI().contains("login")){
            return true;
        }
        // session中有username节点信息的 放行
        if(request.getSession().getAttribute("username") != null){
            return true;
        }

        // 其余情况 拦截 不放行，并转发到index页面
        request.getRequestDispatcher("/toLogin").forward(request,response);
        return false;
    }
}
```

**spring-mvc.xml** 注册拦截器

```xml
<mvc:interceptors>
    
    <mvc:interceptor>
        <!--/** 是拦截映射路径/*下的所有*路径，所有映射都会经过以下拦截器配置-->
        <mvc:mapping path="/**"/>
        <bean class="com.heroc.config.LoginInterceptor"/>
    </mvc:interceptor>

</mvc:interceptors>
```



**index.jsp**

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>首页</title>
  </head>
  <body>
  <h1>
    <a href="${pageContext.request.contextPath}/toLogin">登录页面</a>
  </h1>
  <h1>
    <a href="${pageContext.request.contextPath}/main">首页</a>
  </h1>
  </body>
</html>
```

**login.jsp**

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录页面</title>
</head>
<body>
<h1>登录页面</h1>
<form action="${pageContext.request.contextPath}/login" method="post">
    <div>
        <label for="username">用户名：</label>
        <input id="username" name="username">
    </div>
    <div>
        <label for="pwd">密码：</label>
        <input id="pwd" name="pwd">
    </div>
    <button type="submit">登录</button>
</form>
</body>
</html>
```

**main.jsp**

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>首页</title>
</head>
<body>
<h1>首页</h1>
<p>
    ${name}
</p>
<a href="${pageContext.request.contextPath}/out">注销</a>
</body>
</html>
```



## 九、文件上传下载

文件上传是项目开发中最常见的功能之一 ,springMVC 可以很好的支持文件上传，但是SpringMVC上下文中默认没有装配MultipartResolver，因此默认情况下其不能处理文件上传工作。如果想使用Spring的文件上传功能，则需要在上下文中配置MultipartResolver。

前端表单要求：为了能上传文件，必须将表单的method设置为POST，并将enctype设置为multipart/form-data。只有在这样的情况下，浏览器才会把用户选择的文件以二进制数据发送给服务器；

**对表单中的 enctype 属性做个详细的说明：**

- application/x-www=form-urlencoded：默认方式，只处理表单域中的 value 属性值，采用这种编码方式的表单会将表单域中的值处理成 URL 编码方式。
- multipart/form-data：这种编码方式会以二进制流的方式来处理表单数据，这种编码方式会把文件域指定文件的内容也封装到请求参数中，不会对字符编码。
- text/plain：除了把空格转换为 "+" 号外，其他字符都不做编码处理，这种方式适用直接通过表单发送邮件。

```jsp
<form action="" enctype="multipart/form-data" method="post">
   <input type="file" name="file"/>
   <input type="submit">
</form>
```

一旦设置了enctype为multipart/form-data，浏览器即会采用二进制流的方式来处理表单数据，而对于文件上传的处理则涉及在服务器端解析原始的HTTP响应。在2003年，Apache Software Foundation发布了开源的Commons FileUpload组件，其很快成为Servlet/JSP程序员上传文件的最佳选择。

- Servlet3.0规范已经提供方法来处理文件上传，但这种上传需要在Servlet中完成。
- 而Spring MVC则提供了更简单的封装。
- Spring MVC为文件上传提供了直接的支持，这种支持是用即插即用的MultipartResolver实现的。
- Spring MVC使用Apache Commons FileUpload技术实现了一个MultipartResolver实现类：
- CommonsMultipartResolver。因此，SpringMVC的文件上传还需要依赖Apache Commons FileUpload的组件。



> 文件上传

1、导入文件上传的jar包，commons-fileupload ， Maven会自动帮我们导入他的依赖包 commons-io包；

```xml
<!--文件上传-->
<dependency>
   <groupId>commons-fileupload</groupId>
   <artifactId>commons-fileupload</artifactId>
   <version>1.3.3</version>
</dependency>
<!--servlet-api导入高版本的-->
<dependency>
   <groupId>javax.servlet</groupId>
   <artifactId>javax.servlet-api</artifactId>
   <version>4.0.1</version>
</dependency>
```

2、配置bean：multipartResolver

【**注意！！！这个bena的id必须为：multipartResolver ， 否则上传文件会报400的错误！在这里栽过坑,教训！**】

```xml
<!--文件上传配置-->
<bean id="multipartResolver" class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
   <!-- 请求的编码格式，必须和jSP的pageEncoding属性一致，以便正确读取表单的内容，默认为ISO-8859-1 -->
   <property name="defaultEncoding" value="utf-8"/>
   <!-- 上传文件大小上限，单位为字节（10485760=10M） -->
   <property name="maxUploadSize" value="10485760"/>
   <property name="maxInMemorySize" value="40960"/>
</bean>
```

CommonsMultipartFile 的 常用方法：

- **String getOriginalFilename()：获取上传文件的原名**
- **InputStream getInputStream()：获取文件流**
- **void transferTo(File dest)：将上传文件保存到一个目录文件中**

 我们去实际测试一下

3、编写前端页面

```jsp
<form action="/upload" enctype="multipart/form-data" method="post">
 <input type="file" name="file"/>
 <input type="submit" value="upload">
</form>
```

4、**Controller**

```java
package com.kuang.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;

@Controller
public class FileController {
   //@RequestParam("file") 将name=file控件得到的文件封装成CommonsMultipartFile 对象
   //批量上传CommonsMultipartFile则为数组即可
   @RequestMapping("/upload")
   public String fileUpload(@RequestParam("file") CommonsMultipartFile file ,HttpServletRequest request) throws IOException {

       //获取文件名 : file.getOriginalFilename();
       String uploadFileName = file.getOriginalFilename();

       //如果文件名为空，直接回到首页！
       if ("".equals(uploadFileName)){
           return "redirect:/index.jsp";
      }
       System.out.println("上传文件名 : "+uploadFileName);

       //上传路径保存设置
       String path = request.getServletContext().getRealPath("/upload");
       //如果路径不存在，创建一个
       File realPath = new File(path);
       if (!realPath.exists()){
           realPath.mkdir();
      }
       System.out.println("上传文件保存地址："+realPath);

       InputStream is = file.getInputStream(); //文件输入流
       OutputStream os = new FileOutputStream(new File(realPath,uploadFileName));//文件输出流

       //读取写出
       int len=0;
       byte[] buffer = new byte[1024];
       while ((len=is.read(buffer))!=-1){
           os.write(buffer,0,len);
           os.flush();
      }
       os.close();
       is.close();
       return "redirect:/index.jsp";
  }
}
```

5、测试上传文件，OK！



**采用file.Transto 来保存上传的文件**

1、编写Controller

```java
/*
* 采用file.Transto 来保存上传的文件
*/
@RequestMapping("/upload2")
public String  fileUpload2(@RequestParam("file") CommonsMultipartFile file,HttpServletRequest request) throws IOException {

   //上传路径保存设置
   String path = request.getServletContext().getRealPath("/upload");
   File realPath = new File(path);
   if (!realPath.exists()){
       realPath.mkdir();
  }
   //上传文件地址
   System.out.println("上传文件保存地址："+realPath);

   //通过CommonsMultipartFile的方法直接写文件（注意这个时候）
   file.transferTo(new File(realPath +"/"+ file.getOriginalFilename()));

   return "redirect:/index.jsp";
}
```

2、前端表单提交地址修改

3、访问提交测试，OK！



> 文件下载

**文件下载步骤：**

1、设置 response 响应头

2、读取文件 -- InputStream

3、写出文件 -- OutputStream

4、执行操作

5、关闭流 （先开后关）

**代码实现：**

```java
@RequestMapping(value="/download")
public String downloads(HttpServletResponse response ,HttpServletRequestrequest) throws Exception{
   //要下载的图片地址
   String  path = request.getServletContext().getRealPath("/upload");
   String  fileName = "基础语法.jpg";

   //1、设置response 响应头
   response.reset(); //设置页面不缓存,清空buffer
   response.setCharacterEncoding("UTF-8"); //字符编码
   response.setContentType("multipart/form-data"); //二进制传输数据
   //设置响应头
   response.setHeader("Content-Disposition",
           "attachment;fileName="+URLEncoder.encode(fileName, "UTF-8"));

   File file = new File(path,fileName);
   //2、 读取文件--输入流
   InputStream input=new FileInputStream(file);
   //3、 写出文件--输出流
   OutputStream out = response.getOutputStream();

   byte[] buff =new byte[1024];
   int index=0;
   //4、执行 写出操作
   while((index= input.read(buff))!= -1){
       out.write(buff, 0, index);
       out.flush();
  }
   out.close();
   input.close();
   return null;
}
```

前端

```jsp
<a href="/download">点击下载</a>
```

