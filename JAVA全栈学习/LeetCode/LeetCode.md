### 剑指Offer 47. 礼物最大价值

> 题目

在一个 m*n 的棋盘的每一格都放有一个礼物，每个礼物都有一定的价值（价值大于 0）。你可以从棋盘的左上角开始拿格子里的礼物，并每次向右或者向下移动一格、直到到达棋盘的右下角。给定一个棋盘及其上面的礼物的价值，请计算你最多能拿到多少价值的礼物？

示例 1:

```
输入: 
[
  [1,3,1],
  [1,5,1],
  [4,2,1]
]
输出: 12
解释: 路径 1→3→5→2→1 可以拿到最多价值的礼物
```



> 解析

使用动态规划，最重要的是每次 **向右** 或者 **向下** 移动一格。随意，第一行即每个价值只能是从右移动，用f(i,j)表示价格。那么第一行只能从右移动，动态规划的每个格子的价格为：`f(0,j)=f(0,j-1)+f(0,j)`；第一列只能从上往下移动，动态规划的每个格子的价格为：`f(i,0)=f(i-1,0)+f(i,0)`，其余的可以从上往下移动，也可以往右移动，每个格子的最新价格为：`f(i,j)=Math.max(f(i,j-1),f(i-1,j))+f(i,j)`所以

```
[
  [1,3,1],
  [1,5,1],
  [4,2,1]
]
动态规划之后为：
[
  [1,4,5],
  [2,9,10],
  [6,11,12]
]
```



> 解答：

```java
class Solution {
    public int maxValue(int[][] grid) {
        int m = grid.length; // 获取行数
        int n = grid[0].length; // 获取列数
        // 对第一列的数据进行动态规划
        for(int i=1; i < m; i++){
            grid[i][0] += grid[i-1][0];
        }
        // 对第一行的数据进行动态规划
        for(int j=1; j < n; j++){
            grid[0][j] += grid[0][j-1];
        }
        // 其余的数据进行动态规划
        for(int i=1; i < m; i++){
            for(int j=1; j < n; j++){
                grid[i][j] += Math.max(grid[i][j-1],grid[i-1][j]);
            }
        }
        // 动态规划后的最后一个，就是最大价值
        return grid[m-1][n-1];
    }
}
```



### 剑指Offer 48. 从上到下打印数组

> 题目：

从上到下打印出二叉树的每个节点，同一层的节点按照从左到右的顺序打印。

例如:
给定二叉树: [3,9,20,null,null,15,7],

      3 
    /   \
    9   20
       /  \
      15   7
返回：

```
[3,9,20,15,7]
```



> 解析：

思路很简单，用ArrayList集合层级遍历，首先存入根节点，然后从头开始遍历，根节点的左右节点依次存储到集合中，然后依次遍历下去，将遍历到的当前节点的左右节点一次存入集合中。

如题：存入3这个节点到集合中，然后集合的第一个元素为3节点，索引为index=0，然后将3节点的左右节点存入集合，此时集合中的数据为`3,9,20`，index++，遍历下去获取到的节点为9，又因9节点的左右节点都为null，不做处理，然后index++，此时获取到的是20这个节点，将20节点的左右节点存入到集合中为`3,9,20,15,7`，结束条件为index的大小与集合大小一致。



> 解答：

```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    List<TreeNode> list = new ArrayList<>();

    public int[] levelOrder(TreeNode root) {
        // 如果根节点不为null
        if(root!=null){
            // 将根节点存入集合中
            list.add(root);
            // 遍历集合的指针
            int i = 0;
            // 退出条件是i的值已经等于list集合的大小
            while(i != list.size()){
                // 获得i指针指向的节点，将该节点的左右子节点存入集合中
                TreeNode node = list.get(i);
                if(node.left!=null){
                    list.add(node.left);
                }
                if(node.right!=null){
                    list.add(node.right);
                }
                i++;
            }
        }
        // 将list集合中的节点转换为int类型的数组返回即结果
        int arr[] = new int[list.size()];
        int index = 0;
        for(TreeNode node : list){
            arr[index] = node.val;
            index++;
        }
        return arr;
    }
}
```



### 剑指Offer 49. 丑数

> 题目

我们把只包含质因子 2、3 和 5 的数称作丑数（Ugly Number）。求按从小到大的顺序的第 n 个丑数。

示例:

```
输入: n = 10
输出: 12
解释: 1, 2, 3, 4, 5, 6, 8, 9, 10, 12 是前 10 个丑数。
```

说明:  

1 是丑数。
n 不超过1690。



> 解析

每个丑数都是由 最小`丑数 = 某较小丑数 × 某因子`。

解析：

https://leetcode-cn.com/problems/chou-shu-lcof/solution/mian-shi-ti-49-chou-shu-dong-tai-gui-hua-qing-xi-t/

用3个指针，即a,b,c，首先都指向数组的第一位，a指针用于乘2因子，b指针用于乘3因子，c指针用于乘5因子。

每次从这几个指针乘以因子得出的数中选取最小的一个值，填入最新的数组位置中。然后等于这个最小值的指针进行加一操作。就可以得到下一个较小丑数。



> 解答

```java
class Solution {
    public int nthUglyNumber(int n) {
        int a = 0;
        int b = 0;
        int c = 0;
        int dp[] = new int[n];
        dp[0] = 1;
        for(int i=0; i<n-1; i++){
            int n2 = dp[a]*2;
            int n3 = dp[b]*3;
            int n5 = dp[c]*5;
            int min = Math.min(Math.min(n2,n3),n5);
            dp[i+1] = min;
            if(min == n2) a++;
            if(min == n3) b++;
            if(min == n5) c++;
        }
        return dp[n-1];
    }
}
```





### 75. 颜色分类

> 题目

给定一个包含红色、白色和蓝色，一共 n 个元素的数组，原地对它们进行排序，使得相同颜色的元素相邻，并按照红色、白色、蓝色顺序排列。

此题中，我们使用整数 0、 1 和 2 分别表示红色、白色和蓝色。

注意:
不能使用代码库中的排序函数来解决这道题。

示例：

```
输入: [2,0,2,1,1,0]
输出: [0,0,1,1,2,2]
```



进阶：

一个直观的解决方案是使用计数排序的两趟扫描算法。
首先，迭代计算出0、1 和 2 元素的个数，然后按照0、1、2的排序，重写当前数组。
你能想出一个仅使用常数空间的一趟扫描算法吗？



> 解析

使用双指针一趟扫描。

p0指针指向光

p2指针指向尾

使用i遍历整个数组。

当i小于等于p2时，并且i指针指向的数等于2，就与p2指针指向的数交换，就把2放到了最后，然后p2往前移动一位，再看i当前位置是否还是2，如果是就在进行交换。如果不是就看i当前位置是否是0，如果是就与p0指向的位置交换。

https://leetcode-cn.com/problems/sort-colors/solution/kuai-su-pai-xu-partition-guo-cheng-she-ji-xun-huan/



> 解答

```java
class Solution {
    public void sortColors(int[] nums) {
        int n = nums.length;
        int p0 = 0, p2 = n - 1;
        for (int i = 0; i <= p2; ++i) {
            while (i <= p2 && nums[i] == 2) {
                int temp = nums[i];
                nums[i] = nums[p2];
                nums[p2] = temp;
                --p2;
            }
            if (nums[i] == 0) {
                int temp = nums[i];
                nums[i] = nums[p0];
                nums[p0] = temp;
                ++p0;
            }
        }
    }
}
```



### 18. 四数之和

> 题目

给定一个包含 n 个整数的数组 nums 和一个目标值 target，判断 nums 中是否存在四个元素 a，b，c 和 d ，使得 a + b + c + d 的值与 target 相等？找出所有满足条件且不重复的四元组。

**注意：**

答案中不可以包含重复的四元组。

**示例：**

```
给定数组 nums = [1, 0, -1, 0, -2, 2]，和 target = 0。

满足要求的四元组集合为：
[
  [-1,  0, 0, 1],
  [-2, -1, 1, 2],
  [-2,  0, 0, 2]
]
```



> 解析





> 解答

```java
class Solution {
    public List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> result = new ArrayList<>();
        Arrays.sort(nums);
        int n = nums.length;
        for(int i = 0; i < n - 3; i++){
            // 去重，已经经历过的就没必要经历了
            if(i > 0 && nums[i] == nums[i-1]){
                continue;
            }
            for(int j = i+1; j < n - 2; j++){
                // 去重
                if(j > i+1 && nums[j] == nums[j-1]){
                    continue;
                }
                // 2Sum
                int start = j+1;
                int end = n-1;
                int t = target - nums[i] - nums[j];
                while(start < end){
                    int cur = nums[start] + nums[end];
                    if(cur < t){
                        start++;
                    }else if(cur > t){
                        end--;
                    }else{
                        List<Integer> item = new ArrayList<>();
                        item.add(nums[i]);
                        item.add(nums[j]);
                        item.add(nums[start]);
                        item.add(nums[end]);
                        result.add(item);

                        start++;
                        // 去重
                        while(start < end && nums[start] == nums[start-1]){
                            start++;
                        }
                        end--;
                        // 去重
                        while(start < end && nums[end] == nums[end+1]){
                            end--;
                        }
                    }
                }
            }
        }
        return result;
    }
}
```



### 剑指Offer 36.二叉搜索树与双向链表

> 题目

输入一棵二叉搜索树，将该二叉搜索树转换成一个排序的循环双向链表。要求不能创建任何新的节点，只能调整树中节点指针的指向。

 

为了让您更好地理解问题，以下面的二叉搜索树为例：

 ![](https://assets.leetcode.com/uploads/2018/10/12/bstdlloriginalbst.png)



 

我们希望将这个二叉搜索树转化为双向循环链表。链表中的每个节点都有一个前驱和后继指针。对于双向循环链表，第一个节点的前驱是最后一个节点，最后一个节点的后继是第一个节点。

下图展示了上面的二叉搜索树转化成的链表。“head” 表示指向链表中有最小元素的节点。

 ![](https://assets.leetcode.com/uploads/2018/10/12/bstdllreturndll.png)



 

特别地，我们希望可以就地完成转换操作。当转化完成以后，树中节点的左指针需要指向前驱，树中节点的右指针需要指向后继。还需要返回链表中的第一个节点的指针。



> 解析

最开始我是使用的`队列+中序遍历`的方式来完成二叉搜索树转换为双向链表。这个方式虽然可以完成操作，但是时间复杂度很低，击败了16.68%的用户。哈哈哈。



后来，直接用`中序遍历+pre指针+cur指针`来完成转换。因为直接用遍历转换，所以效率比较高。击败了100%



解释一下，思路：

利用`pre`、`cur`指针。`pre`指针是记录前一个节点，`cur`指针是记录当前节点。`head`指针是记录头节点，即第一个节点，方便形成环状。

还是利用中序遍历。在操作的时候分两种情况：

- `pre`为null，则表示`cur`指针指向的是第一个节点，此时就应该将`head`指针指向第一个节点，记录下来。
- `pre`不为null，则表示有前驱节点，直接按照双向链表规则，确定前驱和后继指针。然后将`pre`=`cur`，确定下一个前驱节点。

最佳理解为：画图。哈哈哈



> 解答

```java
/*
// Definition for a Node.
class Node {
    public int val;
    public Node left;
    public Node right;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val,Node _left,Node _right) {
        val = _val;
        left = _left;
        right = _right;
    }
};
*/
class Solution {
    Node pre, head;
    public Node treeToDoublyList(Node root) {
        if(root==null)return null;
        dfs(root);
        head.left = pre;
        pre.right = head;
        return head;
    }

    public void dfs(Node cur){
        if(cur == null) return;
        dfs(cur.left);
        if(pre!=null){
            pre.right = cur;
        }else{
            head = cur;
        }
        cur.left = pre;
        pre = cur;
        dfs(cur.right);
    }
}
```





### 剑指 Offer 32 从上到下打印二叉树 III

> 题目

请实现一个函数按照之字形顺序打印二叉树，即第一行按照从左到右的顺序打印，第二层按照从右到左的顺序打印，第三行再按照从左到右的顺序打印，其他行以此类推。

 

例如:
给定二叉树: [3,9,20,null,null,15,7],

      3
     /  \
    9    20
    	/  \
       15   7

返回其层次遍历结果：

```
[
  [3],
  [20,9],
  [15,7]
]
```


提示：

`节点总数 <= 1000`



> 解析

利用`双端队列+层序遍历`



> 解答

```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        if(root != null) queue.add(root);
        while(!queue.isEmpty()){
            LinkedList<Integer> item = new LinkedList<>();
            for(int i = queue.size(); i>0; i--){
                TreeNode node = queue.poll();
                // 看res队列的大小，决定如何加入到双端队列中
                if(res.size()%2 == 0){
                    item.addLast(node.val);
                }else{
                    item.addFirst(node.val);
                }
                if(node.left!=null) queue.add(node.left);
                if(node.right!=null) queue.add(node.right);
            }
            res.add(item);
        }
        return res;
    }
}
```



### 416. 分割等和子集 (动态规划)

> 题目

给定一个只包含正整数的非空数组。是否可以将这个数组分割成两个子集，使得两个子集的元素和相等。

注意:

每个数组中的元素不会超过 100
数组的大小不会超过 200
示例 1:

```
输入: [1, 5, 11, 5]

输出: true

解释: 数组可以分割成 [1, 5, 5] 和 [11].
```




示例 2:

```
输入: [1, 2, 3, 5]

输出: false

解释: 数组不能分割成两个元素和相等的子集.
```



> 解析

利用动态规划

https://leetcode-cn.com/problems/partition-equal-subset-sum/solution/0-1-bei-bao-wen-ti-xiang-jie-zhen-dui-ben-ti-de-yo/



> 解答

```java
class Solution {
    public boolean canPartition(int[] nums) {
        // 确定长度 数组长度一定大于2，才可分割
        int len = nums.length;
        if(len < 2) return false;

        // 计算总和
        int sum = 0;
        for(int num : nums){
            sum += num;
        }

        // 如果总和不是偶数，则不能分割两个子集
        if((sum & 1) == 1) return false;

        // 目标数 是其中一个子集的和，利用动态规划解决，如果数组中存在能够达到目标数，才能是true
        int target = sum/2;
        boolean[][] dp = new boolean[len][target+1];

        // 第一列为true，因为目标数为0，则不需要任何元素，所以肯定为true
        for(int i=0; i<len; i++){
            dp[i][0] = true;
        }

        for(int i = 1; i<len; i++){
            for(int j = 1; j<=target; j++){
                dp[i][j] = dp[i-1][j];

                if(nums[i] == j){
                    dp[i][j] = true;
                    continue;
                }

                if(nums[i] < j){
                    dp[i][j] = dp[i-1][j] || dp[i-1][j-nums[i]];
                }
            }

            if(dp[i][target]) return true;
        }

        return dp[len-1][target];
    }
}
```

空间复杂度O(NC)

时间复杂度O(NC)



**优化空间复杂度：**

```java
public class Solution {

    public boolean canPartition(int[] nums) {
        int len = nums.length;
        if (len == 0) {
            return false;
        }
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        if ((sum & 1) == 1) {
            return false;
        }

        int target = sum / 2;
        boolean[] dp = new boolean[target + 1];
        dp[0] = true;

        for (int i = 1; i < len; i++) {
            for (int j = target; nums[i] <= j; j--) {
                if (dp[target]) {
                    return true;
                }
                dp[j] = dp[j] || dp[j - nums[i]];
            }
        }
        return dp[target];
    }
}
```

空间复杂度O(C)

时间复杂度O(NC)





### 剑指 Offer 34 二叉树中和为某一值的路径 (回溯)

> 题目

输入一棵二叉树和一个整数，打印出二叉树中节点值的和为输入整数的所有路径。从树的根节点开始往下一直到叶节点所经过的节点形成一条路径。

 

例如:
给定如下二叉树，以及目标和 `sum = 22`，

                  5
                 / \
                4   8
               /   / \
              11  13  4
             /  \    / \
            7    2  5   1


返回其层次遍历结果：

```
[
   [5,4,11,2],
   [5,8,4,5]
]
```


提示：

`节点总数 <= 10000`



> 解析

回溯



> 解答

```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    List<List<Integer>> res = new ArrayList<>();
    LinkedList<Integer> path = new LinkedList<>();
    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        dfs(root,sum);
        return res;
    }

    public void dfs(TreeNode root, int tar){
        if(root==null) return;
        path.add(root.val);
        tar -= root.val;
        if(tar == 0 && root.left==null && root.right == null){
            res.add(new LinkedList(path));
        }
        dfs(root.left,tar);
        dfs(root.right,tar);
        path.removeLast();
    }
}

// 回溯方法，每一层递归方法都有对应的变量，
// 比如：第一个递归方法中tar为10，第二个递归方法tar的值变成了5，第一个递归中的tar的值不会变
```





### 剑指 Offer 38 字符串排列

> 题目

输入一个字符串，打印出该字符串中字符的所有排列。

 

你可以以任意顺序返回这个字符串数组，但里面不能有重复元素。

 

示例:

输入：s = "abc"
输出：["abc","acb","bac","bca","cab","cba"]


限制：

`1 <= s 的长度 <= 8`



> 解析

回溯

https://leetcode-cn.com/problems/zi-fu-chuan-de-pai-lie-lcof/solution/mian-shi-ti-38-zi-fu-chuan-de-pai-lie-hui-su-fa-by/



> 解答

```java
class Solution {
    List<String> res = new LinkedList<>();
    char[] c;
    public String[] permutation(String s) {
        c = s.toCharArray();
        dfs(0);
        return res.toArray(new String[res.size()]);
    }

    public void dfs(int x){
        if(x == c.length-1){
            res.add(String.valueOf(c)); // 添加排列方案
            return;
        }

        HashSet<Character> set = new HashSet<>();
        for(int i = x; i<c.length; i++){
            if(set.contains(c[i])) continue; // 重复，因此剪枝
            set.add(c[i]);

            swap(i, x); // 交换，将 c[i] 固定在第 x 位  
            dfs(x+1); // 固定下一位
            swap(i, x); // 恢复交换，
        }
    }

    public void swap(int a, int b){
        char temp = c[a];
        c[a] = c[b];
        c[b] = temp;
    }
}
```



### 剑指 Offer 33 二叉搜索树的后序遍历序列

> 题目

输入一个整数数组，判断该数组是不是某二叉搜索树的后序遍历结果。如果是则返回 true，否则返回 false。假设输入的数组的任意两个数字都互不相同。

 

参考以下这颗二叉搜索树：

        5
       / \
      2   6
     / \
    1   3

**示例 1：**

```
输入: [1,6,3,2,5]
输出: false
```

**示例 2：**

```
输入: [1,3,2,6,5]
输出: true
```

**提示：**

`数组长度 <= 1000`



> 解析

https://leetcode-cn.com/problems/er-cha-sou-suo-shu-de-hou-xu-bian-li-xu-lie-lcof/solution/di-gui-he-zhan-liang-chong-fang-shi-jie-jue-zui-ha/



> 解答

```java
class Solution {
    public boolean verifyPostorder(int[] postorder) {
        return helper(postorder, 0, postorder.length - 1);
    }

    boolean helper(int[] postorder, int left, int right) {
        //如果left==right，就一个节点不需要判断了，如果left>right说明没有节点，
        //也不用再看了,否则就要继续往下判断
        if (left >= right)
            return true;
        //因为数组中最后一个值postorder[right]是根节点，这里从左往右找出第一个比
        //根节点大的值，他后面的都是根节点的右子节点（包含当前值，不包含最后一个值，
        //因为最后一个是根节点），他前面的都是根节点的左子节点
        int mid = left;
        int root = postorder[right];
        while (postorder[mid] < root)
            mid++;
        int temp = mid;
        //因为postorder[mid]前面的值都是比根节点root小的，
        //我们还需要确定postorder[mid]后面的值都要比根节点root大，
        //如果后面有比根节点小的直接返回false
        while (temp < right) {
            if (postorder[temp++] < root)
                return false;
        }
        //然后对左右子节点进行递归调用
        return helper(postorder, left, mid - 1) && helper(postorder, mid, right - 1);
    }
}
```







