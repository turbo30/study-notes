package com.heroc;

import com.heroc.utils.ApplicationUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.junit.jupiter.api.Test;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@SpringBootTest
class ShiroSbootApplicationTests {
    @Autowired
    private JavaMailSender javaMailSender;

    @Test
    void contextLoads() {
        Md5Hash md5Hash = new Md5Hash("123456", "hero", 2);
        System.out.println(md5Hash.toString());
    }

    // 发送简单邮件
    @Test
    void sendSimpleMail(){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo("herocheung@foxmail.com");
        message.setFrom("herocheung@foxmail.com");
        message.setText("简单邮件发送测试....");
        message.setSubject("测试");
        javaMailSender.send(message);
    }

    // 发送复杂邮件
    @Test
    void sendMMail() throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setTo("herocheung@foxmail.com");
        helper.setFrom("herocheung@foxmail.com");
        helper.setSubject("复杂邮件测试");
        helper.setText("<h4>复杂邮件发送测试，本次验证码为：</h4><h2 style='color:red'>445670</h2>",true);
        // 添加附件：
        //helper.addAttachment("1.jpg",new File());

        javaMailSender.send(message);
    }

    @Scheduled(cron = "0 0/5 10,15 15 * ?")
    @Test
    void scheduled(){
        System.out.println("被执行了");
    }

    @Test
    public void getBean(){
        System.out.println(ApplicationUtils.getBean("userServiceImpl"));
    }
}
