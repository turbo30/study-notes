package com.heroc.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.HashMap;


/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/27
 * Time: 17:34
 * Description: druid数据源配置
 * Version: V1.0
 */
@Configuration
public class DruidConfig {

    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource druidDataSource(){
        return new DruidDataSource();
    }

    /**
     * ServletRegistrationBean 是spring中的类
     * 由于springboot集成了tomcat，如果需要将bean配置到web.xml中
     * 就得使用ServletRegistrationBean这个类去配置，并将返回的类型，注入到ioc容器中
     * @return
     */
    @Bean
    public ServletRegistrationBean statViewServlet(){
        // 注册servlet的bean，并给定固定访问的url
        // 如同 <servlet-class>StatViewServlet></servlet-class>
        // 如同 <servlet-mapping><url-pattern>/druid/*</url-pattern></servlet-mapping>
        ServletRegistrationBean<StatViewServlet> bean = new ServletRegistrationBean<>(new StatViewServlet(),"/druid/*");

        // setInitParameters 如同<servlet><init-param></init-param></servlet>

        HashMap<String, String> initParameters = new HashMap<>();
        // 给bean注入参数
        // 登录账户密码 key值是固定的
        initParameters.put("loginUsername","admin");
        initParameters.put("loginPassword","123456");
        // 允许谁可访问
        initParameters.put("allow","");
        // 禁止访问： initParameters.put("admin","127.0.0.1");

        bean.setInitParameters(initParameters);
        return bean;
    }

    /**
     * 配置druid的过滤器到web.xml中的filter过滤器
     * @return
     */
    @Bean
    public FilterRegistrationBean webStatFilter(){
        FilterRegistrationBean<WebStatFilter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new WebStatFilter());
        HashMap<String, String> initParameters = new HashMap<>();
        // 以下请求，不被统计到druid中
        initParameters.put("exclusions","*.js,*.css,/druid/*");
        bean.setInitParameters(initParameters);
        return bean;
    }

}
