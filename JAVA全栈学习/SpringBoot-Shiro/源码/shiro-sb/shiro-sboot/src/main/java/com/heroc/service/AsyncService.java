package com.heroc.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/28
 * Time: 10:20
 * Description:
 * Version: V1.0
 */
@Service
public class AsyncService {

    // 该注解，告诉spring，这是一个异步操作
    @Async
    public void hello(){
        try {
            TimeUnit.SECONDS.sleep(5L);
            System.out.println("执行完，异步操作...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
