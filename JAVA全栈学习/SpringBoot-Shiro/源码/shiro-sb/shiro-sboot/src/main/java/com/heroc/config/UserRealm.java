package com.heroc.config;

import com.alibaba.druid.sql.visitor.functions.Char;
import com.heroc.pojo.User;
import com.heroc.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/27
 * Time: 16:18
 * Description:
 * Version: V1.0
 */
public class UserRealm extends AuthorizingRealm {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    // 授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        // 获取身份信息，就是用户名
        String primaryPrincipal = (String)principalCollection.getPrimaryPrincipal();

        // 获取授权实例
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

        // 从数据库中获取用户的权限，按理应该返回权限实例
        User user = userService.getUser(primaryPrincipal);
        // 设置权限
        info.addStringPermission(user.getPerms());

        return info;
    }

    // 认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        // 可获取到身份信息，不包括密码，一般是用户输入的用户名
         String principal = (String)authenticationToken.getPrincipal();

        // 从数据库中获取用户信息
        User user = userService.getUser(principal);
        // 匹配用户名是否正确
        if (!StringUtils.isEmpty(principal) && !principal.equals(user.getName())){
            // 会抛出异常
            throw new UnknownAccountException("用户不存在");
        }

        // 密码认证的SimpleAuthenticationInfo实现类
        // SimpleAuthenticationInfo(principal传递数据库中正确的用户身份信息(用户名)，
        //                          传递数据库获取的密码用于与token中的密码进行比对，
        //                          加盐，
        //                          realm的名字(直接获取父类的就可以了，可以忽略不重要))
        return new SimpleAuthenticationInfo(user.getName(),
                                            user.getPwd(),
                                            ByteSource.Util.bytes(user.getName()),
                                            this.getName());
    }
}
