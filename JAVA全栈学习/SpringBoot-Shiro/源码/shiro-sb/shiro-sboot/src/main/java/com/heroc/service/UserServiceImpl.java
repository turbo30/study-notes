package com.heroc.service;

import com.heroc.mapper.UserMapper;
import com.heroc.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/27
 * Time: 17:48
 * Description:
 * Version: V1.0
 */
@Service
public class UserServiceImpl implements UserService {
    private UserMapper userMapper;

    @Autowired
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public User getUser(String name) {
        return userMapper.getUser(name);
    }
}
