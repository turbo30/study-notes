package com.heroc.service;

import com.heroc.pojo.User;
import org.apache.ibatis.annotations.Param;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/27
 * Time: 17:48
 * Description:
 * Version: V1.0
 */
public interface UserService {

    User getUser(String name);
}
