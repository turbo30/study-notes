package com.heroc.mapper;

import com.heroc.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/27
 * Time: 17:46
 * Description:
 * Version: V1.0
 */
@Repository
@Mapper
public interface UserMapper {

    User getUser(@Param("name") String name);

}
