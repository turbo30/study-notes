package com.heroc.controller;

import com.heroc.service.AsyncService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/27
 * Time: 16:11
 * Description:
 * Version: V1.0
 */
@Controller
public class IndexController {

    @Autowired
    private AsyncService asyncService;

    @RequestMapping({"/","/index"})
    public String index(){
        return "index";
    }

    @RequestMapping("/user/add")
    public String add(HttpServletRequest request){
        // System.out.println(request.getHeader("Cookie"));
        // shiro产生的token就是session会放入到头请求中的cookie中，返回回来进行验证
        return "add";
    }

    @RequestMapping("/user/update")
    public String update(){
        return "update";
    }

    @RequestMapping("/toLogin")
    public String toLogin(){
        return "login";
    }

    @RequestMapping("/login")
    public String login(@RequestParam("username") String username, @RequestParam("password") String pwd, Model model){
        Subject subject = SecurityUtils.getSubject();
        // shiro的md5Hash加密方式对密码加密
        //Md5Hash md5Hash = new Md5Hash(pwd, username, 2); md5Hash.toString()
        UsernamePasswordToken token = new UsernamePasswordToken(username, pwd);
        try {
            // 获取到shiro的token之后，就调用login，进行shiro的realm的一系列操作，比如认证，授权等
            subject.login(token);
            model.addAttribute("msg","true");
            return "index";
        }catch (UnknownAccountException e){
            model.addAttribute("msg","用户名不存在");
            return "login";
        }catch (LockedAccountException e) {
            model.addAttribute("msg", "登录失败，该用户已被冻结");
            return "login";
        }catch (IncorrectCredentialsException e){
            model.addAttribute("msg","密码不正确");
            return "login";
        }catch (Exception e){
            return "login";
        }
    }

    @RequestMapping("/unauth")
    @ResponseBody
    public String unauth(){
        return "未授权，无法访问";
    }

    @RequestMapping("/logout")
    public String logOut(Model model){
        Subject currentUser = SecurityUtils.getSubject();
        // 对当前用户进行退出登录操作
        currentUser.logout();
        model.addAttribute("msg","false");
        return "index";
    }

    @RequestMapping("/async")
    @ResponseBody
    public String async(){
        asyncService.hello();
        return "OK";
    }

}
