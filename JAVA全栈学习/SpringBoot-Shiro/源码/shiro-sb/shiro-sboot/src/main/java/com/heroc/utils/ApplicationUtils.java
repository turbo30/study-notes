package com.heroc.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/29
 * Time: 17:58
 * Description: 获取ioc容器中的bean
 * Version: V1.0
 */
@Component
public class ApplicationUtils implements ApplicationContextAware {

    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }


    public static Object getBean(String beanName){
        return context.getBean(beanName);
    }
}
