package com.heroc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisSbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisSbootApplication.class, args);
    }

}
