package com.heroc;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
class RedisSbootApplicationTests {

    @Autowired
    @Qualifier("redisTemplateCustom")
    private RedisTemplate redisTemplate;

    @Test
    void contextLoads() {
        LettuceConnectionFactory connectionFactory = (LettuceConnectionFactory)redisTemplate.getConnectionFactory();
        connectionFactory.setDatabase(0);
        connectionFactory.afterPropertiesSet();
        redisTemplate.opsForValue().set("key0","test");
    }

}
