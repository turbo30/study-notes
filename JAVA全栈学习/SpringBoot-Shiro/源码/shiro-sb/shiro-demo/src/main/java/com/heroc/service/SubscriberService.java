package com.heroc.service;

import com.heroc.pojo.Permission;
import com.heroc.pojo.Subscriber;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/30
 * Time: 11:25
 * Description:
 * Version: V1.0
 */
public interface SubscriberService {

    boolean register(Subscriber subscriber);

    Subscriber findSubByUsername(String username);

    Subscriber findRolesByUsername(String username);

    List<Permission> findPermissionByRoleId(String roleId);
}
