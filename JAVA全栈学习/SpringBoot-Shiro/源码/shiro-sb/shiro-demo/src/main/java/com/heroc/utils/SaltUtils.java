package com.heroc.utils;

import java.util.Random;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/30
 * Time: 11:36
 * Description:
 * Version: V1.0
 */
public class SaltUtils {
    private static String elements = "ABCDEFGHIJKLMNOPQRSTUVWSYZabcdefghijklmnopqrstuvwsyz1234567890!@#$%^&*()_+=~";

    public static String getSalt(){
        return getSalt(8);
    }

    public static String getSalt(int count){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < count; i++) {
            char c = elements.charAt(new Random().nextInt(elements.length()));
            sb.append(c);
        }
        return sb.toString();
    }
}
