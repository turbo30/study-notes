package com.heroc.config.shiro.cache;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/30
 * Time: 18:14
 * Description:
 * Version: V1.0
 */

public class RedisCacheManager implements CacheManager {

    /**
     *
     * @param s 认证缓存或者授权缓存的名称，在realm开启缓存的时候设置的
     * @param <K> key：一般是用户身份信息
     * @param <V> value： 用户的一些credentials等信息
     * @return
     * @throws CacheException
     */
    @Override
    public <K, V> Cache<K, V> getCache(String s) throws CacheException {
        return new RedisCache<>(s);
    }
}
