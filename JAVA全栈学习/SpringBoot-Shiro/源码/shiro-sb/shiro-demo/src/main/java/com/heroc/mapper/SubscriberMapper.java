package com.heroc.mapper;

import com.heroc.pojo.Permission;
import com.heroc.pojo.Role;
import com.heroc.pojo.Subscriber;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/30
 * Time: 11:26
 * Description:
 * Version: V1.0
 */
@Mapper
@Repository
public interface SubscriberMapper {

    int register(Subscriber subscriber);

    Subscriber findSubByUsername(@Param("username") String username);

    Subscriber findRolesByUsername(@Param("username") String username);

    List<Permission> findPermissionsByRoleId(@Param("id") String roleId);
}
