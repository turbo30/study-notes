package com.heroc.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/30
 * Time: 11:20
 * Description: 用户
 * Version: V1.0
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Component
public class Subscriber implements Serializable {
    private static final long serialVersionUID = 200730112008L;
    private int id;
    private String username;
    private String password;
    private String salt;
    private List<Role> roles;

}
