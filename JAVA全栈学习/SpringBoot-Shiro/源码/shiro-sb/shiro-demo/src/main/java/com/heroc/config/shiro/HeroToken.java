package com.heroc.config.shiro;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/31
 * Time: 14:49
 * Description: 自定义token，扩展，增加新的属性
 * loginType用户确定是使用哪个realm
 * Version: V1.0
 */
public class HeroToken extends UsernamePasswordToken {

    private String loginType;

    public HeroToken() {
    }

    public HeroToken(String username, String password, String loginType) {
        super(username, password);
        this.loginType = loginType;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }
}
