package com.heroc.controller;

import com.heroc.config.shiro.HeroToken;
import com.heroc.pojo.Subscriber;
import com.heroc.service.SubscriberService;
import com.heroc.utils.SaltUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/30
 * Time: 11:25
 * Description:
 * Version: V1.0
 */
@Controller
public class SubscriberController {

    private SubscriberService subscriberService;

    @Autowired
    public void setSubscriberService(SubscriberService subscriberService) {
        this.subscriberService = subscriberService;
    }

    @GetMapping("/login")
    public String toLogin(){
        Subject subject = SecurityUtils.getSubject();
        boolean authenticated = subject.isAuthenticated();
        if (authenticated){
            return "index";
        }
        return "login";
    }

    @GetMapping("/register")
    public String toRegister(){
        return "register";
    }

    @PostMapping("/user/register")
    public String register(Subscriber subscriber){
        boolean result = subscriberService.register(subscriber);
        if (result){
            return "redirect:/login";
        }
        return "redirect:/register";
    }

    @PostMapping("/user/login")
    public String login(@RequestParam("username") String username,
                           @RequestParam("password") String password){
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        // HeroToken token = new HeroToken(username, password, "Manager");
        Subject subject = SecurityUtils.getSubject();
        try{
            // 记住我功能
            // token.setRememberMe(true);
            subject.login(token);
            return "redirect:/login";
        }catch (UnknownAccountException e){
            System.out.println("用户名找不到");
            return "login";
        }catch (IncorrectCredentialsException e){
            System.out.println("密码不正确");
            return "login";
        }catch (Exception e){
            return "login";
        }
    }

    @GetMapping("/rm")
    public String getRm(){
        return "rm";
    }

}
