package com.heroc.config.shiro;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import com.heroc.config.shiro.cache.RedisCacheManager;
import com.heroc.config.shiro.realms.ManagerRealms;
import com.heroc.config.shiro.realms.UserRealms;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.RememberMeManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.session.mgt.eis.JavaUuidSessionIdGenerator;
import org.apache.shiro.session.mgt.eis.RandomSessionIdGenerator;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/30
 * Time: 10:31
 * Description:
 * Version: V1.0
 */
@Configuration
public class ShiroConfig {

    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(DefaultWebSecurityManager securityManager){
        ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
        shiroFilter.setSecurityManager(securityManager);

        HashMap<String, String> map = new HashMap<>();
        map.put("/register","anon");
        map.put("/login","anon");
        map.put("/user/**","anon");
        map.put("/rm","user");
        map.put("/logout","logout");
        shiroFilter.setFilterChainDefinitionMap(map);

        shiroFilter.setLoginUrl("/login");

        return shiroFilter;
    }

    @Bean
    public DefaultWebSecurityManager defaultWebSecurityManager(Realm userRealms){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();

        // 如果要添加多个reaml只能，用集合把所有realm封装起来，在setRealms
        /*ArrayList<Realm> realms = new ArrayList<>();
        realms.add(userRealms());
        realms.add(managerRealms());
        securityManager.setRealms(realms);*/
        securityManager.setRealm(userRealms);

        // 设置缓存管理器，实现的是本地缓存
        // securityManager.setCacheManager(new EhCacheManager());

        // 使用redis缓存用户信息
        securityManager.setCacheManager(new RedisCacheManager());

        // 设置自定义的sessionManager
        // securityManager.setSessionManager(heroHeaderWebSessionManager());

        // 设置自定义的rememberMeManager
        // securityManager.setRememberMeManager(heroRememberMeManager());

        // 获取扩展的使用realm规则。自定义认证器
        // securityManager.setAuthenticator(getHeroModularRealmAuthenticator());

        return securityManager;
    }

    @Bean
    public Realm userRealms(){
        UserRealms userRealms = new UserRealms();
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        hashedCredentialsMatcher.setHashAlgorithmName("md5");
        hashedCredentialsMatcher.setHashIterations(1024);
        userRealms.setCredentialsMatcher(hashedCredentialsMatcher);

        // 启动全局缓存，减少数据库的查询
        userRealms.setCachingEnabled(true);
        // 开启授权缓存，并取名字
        userRealms.setAuthorizationCachingEnabled(true);
        userRealms.setAuthorizationCacheName("authorizationCache");
        // 开启认证缓存，并取名字
        userRealms.setAuthenticationCachingEnabled(true);
        userRealms.setAuthenticationCacheName("authenticationCache");

        return userRealms;
    }

    @Bean
    public Realm managerRealms(){
        ManagerRealms managerRealms = new ManagerRealms();
        return managerRealms;
    }

    /**
     * session重写了一定要被securityManager接纳
     * @return
     */
    public SessionManager sessionManager(){
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        // 持久化sessionID，达到多态共享session，
        // 有该sessionID，就不用重新登录了。
        // 或者以防止服务器崩后重启，而导致会话中断
        sessionManager.setSessionDAO(heroWebSessionDAO());

        sessionManager.setGlobalSessionTimeout(15*1000);
        sessionManager.setSessionValidationInterval(20*1000);
        sessionManager.setDeleteInvalidSessions(true);
        // 禁止在url中追加sessionId信息
        sessionManager.setSessionIdUrlRewritingEnabled(false);
        return sessionManager;
    }

    public HeroWebSessionDAO heroWebSessionDAO(){
        HeroWebSessionDAO heroWebSessionDAO = new HeroWebSessionDAO();
        // 给当前有效的sessionID取名字，在数据库中存储的是hash类型
        heroWebSessionDAO.setActiveSessionsCacheName("shiro-activeSessionCache");
        // 设置生成sessionID的格式
        heroWebSessionDAO.setSessionIdGenerator(new JavaUuidSessionIdGenerator());
        return heroWebSessionDAO;
    }

    /**
     * 自定义session管理器，用于将session写到请求头中，或者从请求头中获取sessionID
     */
    /*@Bean
    public SessionManager heroHeaderWebSessionManager(){
        HeroHeaderWebSessionManager sessionManager = new HeroHeaderWebSessionManager();
        // 设置session过期时间
        sessionManager.setGlobalSessionTimeout(30*60*1000);
        // 删除所有无效的Session对象，此时的session被保存在了内存里面
        sessionManager.setDeleteInvalidSessions(true);
        // 扫描失效的sessionid，让无效的sessionid释放
        sessionManager.setSessionValidationInterval(1000*20);
        // 禁止在url中追加sessionId信息
        sessionManager.setSessionIdUrlRewritingEnabled(false);
        return sessionManager;
    }*/

    /**
     * 自定义记住我管理器，用于将RememberMe写到请求头中，或者从请求头中获取RememberMe
     */
    /*@Bean
    public RememberMeManager heroRememberMeManager(){
        HeroRememberMeManager rememberMeManager = new HeroRememberMeManager();
        return rememberMeManager;
    }*/

    /**
     * 设置认证器选择realm的规则
     * @return
     */
    /*@Bean
    public ModularRealmAuthenticator getHeroModularRealmAuthenticator(){
        return new HeroModularRealmAuthenticator();
    }*/


    @Bean
    public ShiroDialect shiroDialect(){
        return new ShiroDialect();
    }

}
