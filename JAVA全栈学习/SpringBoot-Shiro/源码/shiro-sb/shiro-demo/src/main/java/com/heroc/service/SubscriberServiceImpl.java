package com.heroc.service;

import com.heroc.mapper.SubscriberMapper;
import com.heroc.pojo.Permission;
import com.heroc.pojo.Subscriber;
import com.heroc.utils.SaltUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/30
 * Time: 11:25
 * Description:
 * Version: V1.0
 */
@Service
public class SubscriberServiceImpl implements SubscriberService {
    private SubscriberMapper subscriberMapper;

    @Autowired
    public void setSubscriberMapper(SubscriberMapper subscriberMapper) {
        this.subscriberMapper = subscriberMapper;
    }

    @Override
    public boolean register(Subscriber subscriber) {
        String salt = SaltUtils.getSalt();
        Md5Hash md5Hash = new Md5Hash(subscriber.getPassword(), salt, 1024);
        subscriber.setPassword(md5Hash.toHex()).setSalt(salt);
        int result = subscriberMapper.register(subscriber);
        if (result > 0){
            return true;
        }
        return false;
    }

    @Override
    public Subscriber findSubByUsername(String username) {
        return subscriberMapper.findSubByUsername(username);
    }

    @Override
    public Subscriber findRolesByUsername(String username) {
        return subscriberMapper.findRolesByUsername(username);
    }

    @Override
    public List<Permission> findPermissionByRoleId(String roleId) {
        return subscriberMapper.findPermissionsByRoleId(roleId);
    }
}
