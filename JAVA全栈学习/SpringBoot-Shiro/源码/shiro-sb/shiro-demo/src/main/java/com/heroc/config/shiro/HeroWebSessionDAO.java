package com.heroc.config.shiro;

import com.heroc.utils.ApplicationUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/31
 * Time: 23:37
 * Description:
 * 继承EnterpriseCacheSessionDAO 并重写方法，就可以实现session的持久化
 * Version: V1.0
 */
public class HeroWebSessionDAO extends EnterpriseCacheSessionDAO {

    public HeroWebSessionDAO() {
        super();
    }

    @Override
    protected Serializable doCreate(Session session) {
        Serializable sessionId = generateSessionId(session);
        assignSessionId(session, sessionId);
        getRedisTemplate().opsForValue().set(sessionId,session);
        return sessionId;
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
        return (Session) getRedisTemplate().opsForValue().get(sessionId);
    }

    @Override
    protected void doUpdate(Session session) {
        getRedisTemplate().opsForValue().set(session.getId(),session);
    }

    @Override
    protected void doDelete(Session session) {
        getRedisTemplate().delete(session.getId());
    }

    private RedisTemplate getRedisTemplate(){
        RedisTemplate redisTemplate = (RedisTemplate) ApplicationUtils.getContext("redisTemplate");
        return redisTemplate;
    }
}
