package com.heroc.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/30
 * Time: 13:26
 * Description: 角色
 * Version: V1.0
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Component
public class Role implements Serializable {
    private static final long serialVersionUID = 200730132605L;
    private int id;
    private String roleName;

    private List<Permission> permissions;
}
