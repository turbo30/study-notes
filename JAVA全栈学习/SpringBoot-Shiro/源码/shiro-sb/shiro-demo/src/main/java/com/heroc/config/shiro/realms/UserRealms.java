package com.heroc.config.shiro.realms;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.heroc.pojo.Permission;
import com.heroc.pojo.Role;
import com.heroc.pojo.Subscriber;
import com.heroc.service.SubscriberService;
import com.heroc.utils.ApplicationUtils;
import com.heroc.utils.HeroSimpleByteSource;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/30
 * Time: 10:31
 * Description:
 * Version: V1.0
 */

public class UserRealms extends AuthorizingRealm {

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        SubscriberService subscriberService = (SubscriberService) ApplicationUtils.getContext("subscriberServiceImpl");
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        String primaryPrincipal = (String)principals.getPrimaryPrincipal();
        Subscriber subscriber = subscriberService.findRolesByUsername(primaryPrincipal);
        List<Role> roles = subscriber.getRoles();
        if (!CollectionUtils.isEmpty(roles)){
            roles.forEach(role -> {
                simpleAuthorizationInfo.addRole(role.getRoleName());
                List<Permission> permissions = subscriberService.findPermissionByRoleId(role.getId()+"");
                if (!CollectionUtils.isEmpty(permissions)){
                    permissions.forEach(perms -> {
                        simpleAuthorizationInfo.addStringPermission(perms.getPermsName());
                    });
                }
            });
        }
        return simpleAuthorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String principal = (String)token.getPrincipal();
        SubscriberService subscriberService = (SubscriberService) ApplicationUtils.getContext("subscriberServiceImpl");
        Subscriber subscriber = subscriberService.findSubByUsername(principal);
        if (StringUtils.isEmpty(principal) || !subscriber.getUsername().equals(principal)){
            throw new UnknownAccountException();
        }

        return new SimpleAuthenticationInfo(subscriber.getUsername(),
                                            subscriber.getPassword(),
                                            new HeroSimpleByteSource(subscriber.getSalt()),
                                            this.getName());
    }
}
