package com.heroc.config.shiro;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.pam.ModularRealmAuthenticator;
import org.apache.shiro.realm.Realm;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA
 *
 * @Author: heroC
 * Date: 2020/7/31
 * Time: 14:39
 * Description:
 * 自定义认证规则，分支管理realm，更具要求执行对应的realm
 * 因为shiro默认认证规则会走ModularRealmAuthenticator的doAuthenticate方法。是链式执行realm
 * Version: V1.0
 */
public class HeroModularRealmAuthenticator extends ModularRealmAuthenticator {

    @Override
    protected AuthenticationInfo doAuthenticate(AuthenticationToken authenticationToken) throws AuthenticationException {
        // 获取token中的loginType
        HeroToken heroToken = (HeroToken) authenticationToken;
        String loginType = heroToken.getLoginType();

        assertRealmsConfigured();
        Collection<Realm> realms = getRealms();

        // 获取满足要求的realm，添加到集合中，这里就选择到了一个realm
        ArrayList<Realm> typeRealms = new ArrayList<>();
        realms.forEach(realm -> {
            if (realm.getName().startsWith(loginType)){
                typeRealms.add(realm);
            }
        });

        if (typeRealms.size() == 1) {
            return doSingleRealmAuthentication(typeRealms.iterator().next(), authenticationToken);
        } else {
            return doMultiRealmAuthentication(typeRealms, authenticationToken);
        }
    }
}
