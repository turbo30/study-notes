# Shiro

Shiro是一个功能强大且易于使用的Java安全框架。他执行身份验证、授权、加密和会话管理。



> 相关课程
>
> https://www.bilibili.com/video/BV1uz4y197Zm?p=1
>
> https://www.bilibili.com/video/BV1YW411M7S3?p=1



## 一、权限管理

权限管理包括，身份认证和授权两个部分。简称`认证授权`



### 1、身份认证

就是判断一个用户是否为合法用户的处理过程。最常用的简单身份认证方式是系统通过对用户输入的用户名和口令，看其是否与系统中存储的该用户的用户名和口令一致，来判断用户身份是否正确。对采用指纹等系统，则出示指纹；对于硬件key等刷卡系统，则需要刷卡。



### 2、授权

授权，即访问控制。控制谁能访问哪些资源。主题进行身份认证后需要分配权限方可访问系统的资源，对于某些资源没有权限是无法访问的。



## 二、Shiro 核心与架构

> 核心部分

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForShiro/ShiroBasicArchitecture.png)

核心的三个部分：Subject、SecurityManager、Realm



> 架构



![](https://gitee.com/turbo30/study_pic/raw/master/pictureForShiro/ShiroArchitecture.png)

**subject：**

一个一个的主体。图中表示，可以有很多种语言，使用shiro

**security Manager：**

shiro的安全管理部分。

- Authenticator部分是用于认证用户的；
- Authorizer部分是用于授权的；
- SessionManager是在web项目中能够使用到的模块，集成session，保持用户的会话；
- SessionDAO是将session持久化的模块；
- CacheManager是缓存模块，一个用户认证时会调用数据库查找该用户是否存在，为了避免每次都调用数据库，所以可以对其做缓存的操作；
- PluggableRealms是具体实现认证和授权的模块，会和数据库打交道。

**Cryptography：**

算法模块。主要对用户密码做加密处理的，大致有MD5等加密算法。



## 三、Shiro认证



### 1、关键对象 ==重要==

- **Subject：主题**

  访问系统的用户，主体可以是用户、程序等。进行认证的都是主体。

- **Principal：身份信息**

  是主体进行身份认证的标识，标识必须具有唯一性，如用户名、手机号、邮箱地址等，一个主体可以有多个身份，但是必须有一个主身份。

- **credential：凭证信息**

  是只有主体自己知道的安全信息，如密码、证书等。



### 2、认证流程

​	![](https://gitee.com/turbo30/study_pic/raw/master/pictureForShiro/认证流程.png)



### 3、追源码 (认证、授权) ==重要==

通过`subject.login(token);`打断点，找到在`SimpleAccountRealm`中完成了认证授权操作以及密码比对。这个类继承了`AuthorizingRealm`类，所以我们自定义Realm时，也继承`AuthorizingRealm`这个类，重写`doGetAuthenticationInfo`方法实现与数据库连接的认证操作，重写`doGetAuthorizationInfo`方法实现授权操作。





## 四、Shiro授权

授权，即访问控制。控制谁能访问哪些资源。主题进行身份认证后需要分配权限方可访问系统的资源，对于某些资源没有权限是无法访问的。



### 1、关键对象 ==重要==

授权可简单理解为who对what进行how操作：

**who：即主体(Subject)**，主体需要访问系统的资源；

**what：即资源(Resource)**，如系统菜单、页面、按钮、类方法、系统商品信息等。资源包括资源类型和资源实例。

**how：权限/许可(Permission)**，规定了主体对资源的操作许可，权限离开资源没有意义。



### 2、授权流程

​	![](https://gitee.com/turbo30/study_pic/raw/master/pictureForShiro/授权流程.png)



### 3、授权方式

- 基于角色的访问控制

  + RBAC基于角色的访问控制（Role-Based Access Control）是以角色为中心进行的访问控制

    ```java
    if(subject.hasRole("admin")){
        // 操作
    }
    ```

    

- 基于资源的访问控制

  + RBAC基于资源的访问控制（Resource-Based Access Control）是以资源为中心进行范文控制

    ```java
    if(subject.isPermission("user:create:*")){
        // 操作
    }
    ```

    

### 4、权限字符串

权限字符串规则：

**资源标识符:操作:资源实例标识符**

意思就是对哪个资源的哪个实例具有什么操作，权限字符串也可以使用*通配符

eg:

```
用户创建权限
user:create 或 user:create:*

用户修改实例001的权限
user:update:001

用户实例001的所有权限
user:*:001
```



### 5、授权编程方式

- 编程式

  ```java
  Subject subject = SecurityUtils.getSubject();
  if(subject.hasRole("admin")){
      // 具有admin权限的操作
  }else{
      // 无权限操作
  }
  ```

- 注解式

  ```java
  @RequiresRoles("admin") // 满足amdin的角色执行该方法
  @RequiresRoles(value = {"admin","user"}) // 满足amdin和user两个角色，执行该方法
  public void hello(){
      // 有权限操作
  }
  ```

- 标签式

  整合thymeleaf需要查看springBoot.md

  ```html
  <shiro:hasRole name="admin">
  	<!--有权限-->
  </shiro:hasRole>
  
  guest标签
  　　<shiro:guest>
  　　</shiro:guest>
  　　用户没有身份验证时显示相应信息，即游客访问信息。
  
  user标签
  　　<shiro:user>　　
  　　</shiro:user>
  　　用户已经身份验证/记住我登录后显示相应的信息。
  
  authenticated标签
  　　<shiro:authenticated>　　
  　　</shiro:authenticated>
  　　用户已经身份验证通过，即Subject.login登录成功，不是记住我登录的。
  
  notAuthenticated标签
  　　<shiro:notAuthenticated>
  　　
  　　</shiro:notAuthenticated>
  　　用户已经身份验证通过，即没有调用Subject.login进行登录，包括记住我自动登录的也属于未进行身份验证。
  
  principal标签
  　　<shiro: principal/>
  　　
  　　<shiro:principal property="username"/>
  　　相当于((User)Subject.getPrincipals()).getUsername()。
  
  lacksPermission标签
  　　<shiro:lacksPermission name="org:create">
  　
  　　</shiro:lacksPermission>
  　　如果当前Subject没有权限将显示body体内容。
  
  hasRole标签
  　　<shiro:hasRole name="admin">　　
  　　</shiro:hasRole>
  　　如果当前Subject有角色将显示body体内容。
  
  hasAnyRoles标签
  　　<shiro:hasAnyRoles name="admin,user">
  　　　
  　　</shiro:hasAnyRoles>
  　　如果当前Subject有任意一个角色（或的关系）将显示body体内容。
  
  lacksRole标签
  　　<shiro:lacksRole name="abc">　　
  　　</shiro:lacksRole>
  　　如果当前Subject没有角色将显示body体内容。
  
  hasPermission标签
  　　<shiro:hasPermission name="user:create">　　
  　　</shiro:hasPermission>
  　　如果当前Subject有权限将显示body体内容
  ```



## 五、过滤器缩写解释

| 配置缩写          | 对应的过滤器                  | 功能                                                         |
| ----------------- | ----------------------------- | ------------------------------------------------------------ |
| anon              | AnonymousFilter               | 任何用户都可访问                                             |
| authc             | FormAuthenticationFilter      | 指定url需要form表单登录，默认会请求中获取`username`、`password`、`rememberMe`等参数并尝试登录，如果登录不了就会跳转到loginUrl配置的路径。我们也可以用这个过滤器做默认的登录逻辑，但是一般都是我们自己在控制器写登录逻辑的，自己写的话出错返回的信息可以定制。 |
| authcBasic        | BasicHttpAuthenticationFilter | 指定url需要basic登录                                         |
| logout            | LogoutFilter                  | 登出过滤器，配置指定的url就可以实现退出功能，非常方便        |
| noSessionCreation | NoSessionCreationFilter       | 禁止创建会话                                                 |
| perms             | PermissionAuthorizationFilter | 需要指定权限才能访问                                         |
| port              | PortFilter                    | 需要指定端口才能访问                                         |
| rest              | HttpMethodPermssionFilter     | 将http请求方法转化成相应的动词来构造一个权限字符串，这个感觉意义不大 |
| roles             | RolesAuthorizationFilter      | 需要指定角色才能访问                                         |
| ssl               | SslFilter                     | 需要https请求才能访问                                        |
| user              | UserFilter                    | 需要“记住我”功能才能访问                                     |





## 六、整合springboot



### 1、流程图

​	![](https://gitee.com/turbo30/study_pic/raw/master/pictureForShiro/整合springboot流程图.png)

shiroFilter的作用就是拦截所有请求，通过核心模块SecurityManager去调用我们自定义的Realm对其进行身份验证和授权操作。然后根据结果执行对应的操作。



### 2、依赖

```xml
<dependency>
    <groupId>org.apache.shiro</groupId>
    <artifactId>shiro-spring</artifactId>
    <version>1.5.3</version>
</dependency>
```

连接数据库依赖

```xml
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>druid</artifactId>
    <version>1.1.23</version>
</dependency>
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.17</version>
</dependency>
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.1.3</version>
</dependency>
```



### 3、ShiroConfig配置

```java
@Configuration
public class ShiroConfig{

    /**
     * ShiroFilterFactoryBean，
     * shiro将所有请求全部拦截，做请求的处理和认证授权处理
      * @param securityManager
     * @return
     */
    @Bean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("securityManager") DefaultWebSecurityManager securityManager){
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        // 设置安全管理
        shiroFilterFactoryBean.setSecurityManager(securityManager);


        /**
         * shiro内置过滤器
         * 认证过滤器：
         * anon: 无需认证就可以访问
         * authc: 必须认证才能访问
         * user: 必须拥有 记住我 功能才能用
         * 授权过滤器：
         * perms：拥有对某个资源的权限才能访问
         * roles：拥有某个角色权限才能访问
         *
         * 通常可将这些过滤器分为两组
         * anon,authc,authcBasic,user是第一组认证过滤器
         * perms,port,rest,roles,ssl是第二组授权过滤器
         * 注意user和authc不同：当应用开启了rememberMe时,用户下次访问时可以是一个user，但绝不会是authc,因为authc是需要重新认证的
         * user表示用户不一定已通过认证,只要曾被Shiro记住过登录状态的用户就可以正常发起请求,比如rememberMe 说白了,以前的一个用户登录时开启了rememberMe,然后他关闭浏览器,下次再访问时他就是一个user,而不会authc
         *
         */
        // 设置请求需要什么条件，才能被访问
        LinkedHashMap<String, String> filterMap = new LinkedHashMap<>();
        /**
         * 如果时需要某个资源权限才能访问的话，会进入realm的授权方法，验证授权
         */
        filterMap.put("/user/add","perms[user:add]");
        filterMap.put("/user/update","perms[user:update]");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterMap);

        // 以上请求被拦截，则跳转到登录页面
        shiroFilterFactoryBean.setLoginUrl("/toLogin");

        // 登录之后，点击没有权限访问的页面时，会转到该请求
        shiroFilterFactoryBean.setUnauthorizedUrl("/unauth");

        return shiroFilterFactoryBean;
    }

    /**
     * DefaultWebSecurityManager
     * @param realm
     * @return
     */
    @Bean(name = "securityManager")
    public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("userRealm") Realm realm){
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        defaultWebSecurityManager.setRealm(realm);
        return defaultWebSecurityManager;
    }

    /**
     * 自定义realm
     * @return
     */
    @Bean
    public UserRealm userRealm(){
        // 设置自定义realm 在认证的时候对用户输入的密码加密的方式，以及hash散列的次数
        UserRealm userRealm = new UserRealm();
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        hashedCredentialsMatcher.setHashAlgorithmName("md5");
        hashedCredentialsMatcher.setHashIterations(2);
        userRealm.setCredentialsMatcher(hashedCredentialsMatcher);
        return userRealm;
    }
}
```



### 4、自定义Realm

```java
public class UserRealm extends AuthorizingRealm {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    // 授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        // 获取身份信息，就是用户名
        String primaryPrincipal = (String)principalCollection.getPrimaryPrincipal();

        // 获取授权实例
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

        // 从数据库中获取用户的权限，按理应该返回权限实例
        User user = userService.getUser(primaryPrincipal);
        // 设置权限
        info.addStringPermission(user.getPerms());

        return info;
    }

    // 认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        // 可获取到身份信息，不包括密码，一般是用户输入的用户名
         String principal = (String)authenticationToken.getPrincipal();

        // 从数据库中获取用户信息
        User user = userService.getUser(principal);
        // 匹配用户名是否正确
        if (!principal.equals(user.getName())){
            // 会抛出异常
            throw new UnknownAccountException("用户不存在");
        }

        // 密码认证的SimpleAuthenticationInfo实现类
        // SimpleAuthenticationInfo(principal传递数据库中正确的用户身份信息(用户名)，
        //                          传递数据库获取的密码用于与token中的密码进行比对，
        //                          加盐，
        //                          realm的名字(直接获取父类的就可以了，可以忽略不重要))
        return new SimpleAuthenticationInfo(user.getName(),
                                            user.getPwd(),
                                            ByteSource.Util.bytes(user.getName()),
                                            this.getName());
    }
}
```



### 5、Controller、Service、Mapper层 thymeleaf

使用thymeleaf还需要导入依赖，注入bean

```xml
<dependency>
    <groupId>com.github.theborakompanioni</groupId>
    <artifactId>thymeleaf-extras-shiro</artifactId>
    <version>2.0.0</version>
</dependency>
```



```java
@Bean
public ShiroDialect shiroDialect(){
    return new ShiroDialect();
}
```



```java
@RequestMapping("/login")
public String login(@RequestParam("username") String username, @RequestParam("password") String pwd, Model model){
    Subject subject = SecurityUtils.getSubject();
    UsernamePasswordToken token = new UsernamePasswordToken(username, pwd);
    try {
        // 获取到shiro的token之后，就调用login，进行shiro的realm的一系列操作，比如认证，授权等
        subject.login(token);
        model.addAttribute("msg","true");
        return "index";
    }catch (UnknownAccountException e){
        model.addAttribute("msg","用户名不存在");
        return "login";
    }catch (LockedAccountException e) {
        model.addAttribute("msg", "登录失败，该用户已被冻结");
        return "login";
    }catch (IncorrectCredentialsException e){
        model.addAttribute("msg","密码不正确");
        return "login";
    }catch (Exception e){
        return "login";
    }
}

@RequestMapping("/unauth")
@ResponseBody
public String unauth(){
    return "未授权，无法访问";
}

@RequestMapping("/logout")
public String logOut(Model model){
    Subject currentUser = SecurityUtils.getSubject();
    // 对当前用户进行退出登录操作
    currentUser.logout();
    model.addAttribute("msg","false");
    return "index";
}
```



```java
public interface UserService {
    User getUser(String name);
}


/*************************************************************/

@Service
public class UserServiceImpl implements UserService {
    private UserMapper userMapper;

    @Autowired
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public User getUser(String name) {
        return userMapper.getUser(name);
    }
}
```



```java
@Repository
@Mapper
public interface UserMapper {

    User getUser(@Param("name") String name);

}
```



```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heroc.mapper.UserMapper">
    <select id="getUser" parameterType="String" resultType="user">
        select * from user where name = #{name};
    </select>
</mapper>
```





## 七、实战 SpringBoot-Shiro

建立用户表、角色表、权限表。根据这几个表，实现权限管理资源。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForShiro/用户角色权限表关系.png)

具体实现：见shiro-demo项目代码



## 八、使用CacheManager

缓存都是将数据放入到内存中，提高数据查询的速度。因此增删改操作极少，而查询极多的数据适合缓存。



### 1、缓存介绍

- Cache缓存：计算机内存中一段数据

- 作用：用来减轻DB的访问压力，从而提高系统的查询效率

- 流程：

  ![](https://gitee.com/turbo30/study_pic/raw/master/pictureForShiro/缓存流程图.png)



### 2、使用Shiro中默认EhCache本地缓存

> 引入依赖

```xml
<dependency>
    <groupId>org.apache.shiro</groupId>
    <artifactId>shiro-ehcache</artifactId>
    <version>1.5.3</version>
</dependency>
```



> 配置

```java
@Bean
public UserRealms userRealms(){
    UserRealms userRealms = new UserRealms();
    HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
    hashedCredentialsMatcher.setHashAlgorithmName("md5");
    hashedCredentialsMatcher.setHashIterations(1024);
    userRealms.setCredentialsMatcher(hashedCredentialsMatcher);

    // 设置缓存管理器，实现的是本地缓存
    userRealms.setCacheManager(new EhCacheManager());
    // 启动全局缓存
    userRealms.setCachingEnabled(true);
    // 开启授权缓存，并取名字
    userRealms.setAuthorizationCachingEnabled(true);
    userRealms.setAuthorizationCacheName("authorizationCache");
    // 开启认证缓存，并取名字
    userRealms.setAuthenticationCachingEnabled(true);
    userRealms.setAuthenticationCacheName("authenticationCache");

    return userRealms;
}
```



### 3、使用Redis作为缓存

看项目代码shiro-demo



## 九、多Realm配置



### 1、使用场景

当shiro进行权限管理，数据来自不同数据源时，可以使用多个realm。



### 2、多个Realm处理方式

#### 1）链式处理

链式处理就是，比如用户登录时，需要查询两个数据库，一个mysql一个Oracle数据库，分别对应两个realm。此时，用户一登陆，就会分别执行两个realm，只要满足期中一个，就会被放行，认证成功。

```java
@Bean
public DefaultWebSecurityManager defaultWebSecurityManager(Realm userRealms){
    DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
    
    // 如果要添加多个reaml只能，用集合把所有realm封装起来，在setRealms
    ArrayList<Realm> realms = new ArrayList<>();
    realms.add(mysqlRealms);
    realms.add(orcaleRealms);
    securityManager.setRealms(realms);
    
    return securityManager;
}
```



#### 2）分支处理

根据不同的条件去执行对应的realm进行认证。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForShiro/多realm分支管理.png)

**怎么将类型传递过去呢？**

我们可以继承UsernamePasswordToken类，添加一个属性。就可以使用这个token传递除了用户名密码的其他数据了。



> 自定义token

自定义token用于存储判断量，用于判断执行哪个realm类。

在使用的时候，就可以使用HeroToken来存储值了。

```java
public class HeroToken extends UsernamePasswordToken {

    private String loginType; // 判断量

    public HeroToken() {
    }

    public HeroToken(String username, String password, String loginType) {
        super(username, password);
        this.loginType = loginType;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }
}
```



> 自定义认证器

继承shiro的ModularRealmAuthenticator类，并重写认证器，覆盖原来的链式处理模式。写成分支处理。

```java
public class HeroModularRealmAuthenticator extends ModularRealmAuthenticator {

    @Override
    protected AuthenticationInfo doAuthenticate(AuthenticationToken authenticationToken) throws AuthenticationException {
        // 获取token中的loginType
        HeroToken heroToken = (HeroToken) authenticationToken;
        String loginType = heroToken.getLoginType();

        assertRealmsConfigured();
        Collection<Realm> realms = getRealms();

        // 获取满足要求的realm，添加到集合中，这里就选择到了一个realm
        ArrayList<Realm> typeRealms = new ArrayList<>();
        realms.forEach(realm -> {
            if (realm.getName().startsWith(loginType)){
                typeRealms.add(realm);
            }
        });

        if (typeRealms.size() == 1) {
            return doSingleRealmAuthentication(typeRealms.iterator().next(), authenticationToken);
        } else {
            return doMultiRealmAuthentication(typeRealms, authenticationToken);
        }
    }
}
```



> 配置

在安全管理中，设置自定义的认证器，这时候shiro就会走我们自定义的认证器规则

```java
@Configuration
public class ShiroConfig {

    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(DefaultWebSecurityManager securityManager){
        ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
        shiroFilter.setSecurityManager(securityManager);

        HashMap<String, String> map = new HashMap<>();
        map.put("/register","anon");
        map.put("/login","anon");
        map.put("/user/**","anon");
        map.put("/logout","logout");
        shiroFilter.setFilterChainDefinitionMap(map);

        shiroFilter.setLoginUrl("/login");

        return shiroFilter;
    }

    @Bean
    public DefaultWebSecurityManager defaultWebSecurityManager(Realm userRealms){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();

        // 如果要添加多个reaml只能，用集合把所有realm封装起来，在setRealms
        ArrayList<Realm> realms = new ArrayList<>();
        realms.add(userRealms());
        realms.add(managerRealms());
        securityManager.setRealms(realms);
        //securityManager.setRealm(userRealms);
        // 设置缓存管理器，实现的是本地缓存
        // securityManager.setCacheManager(new EhCacheManager());

        // 使用redis缓存
        securityManager.setCacheManager(new RedisCacheManager());

        // ************************获取扩展的使用realm规则。自定义认证器
        securityManager.setAuthenticator(getHeroModularRealmAuthenticator());

        return securityManager;
    }

    @Bean
    public Realm userRealms(){
        UserRealms userRealms = new UserRealms();
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        hashedCredentialsMatcher.setHashAlgorithmName("md5");
        hashedCredentialsMatcher.setHashIterations(1024);
        userRealms.setCredentialsMatcher(hashedCredentialsMatcher);


        // 启动全局缓存
        userRealms.setCachingEnabled(true);
        // 开启授权缓存，并取名字
        userRealms.setAuthorizationCachingEnabled(true);
        userRealms.setAuthorizationCacheName("authorizationCache");
        // 开启认证缓存，并取名字
        userRealms.setAuthenticationCachingEnabled(true);
        userRealms.setAuthenticationCacheName("authenticationCache");

        return userRealms;
    }

    @Bean
    public Realm managerRealms(){
        ManagerRealms managerRealms = new ManagerRealms();
        return managerRealms;
    }

    // **************** 注入自定义认证器
    @Bean
    public ModularRealmAuthenticator getHeroModularRealmAuthenticator(){
        return new HeroModularRealmAuthenticator();
    }


    @Bean
    public ShiroDialect shiroDialect(){
        return new ShiroDialect();
    }

}

```



## 十、Session的持久化 Redis

持久化sessionID，达到多态共享session，有该sessionID，就不用重新登录了。或者以防止服务器崩后重启，而导致会话中断

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForShiro/持久化session.png)

> 实现EnterpriseCacheSessionDAO

```java
public class HeroWebSessionDAO extends EnterpriseCacheSessionDAO {

    public HeroWebSessionDAO() {
        super();
    }

    @Override
    protected Serializable doCreate(Session session) {
        Serializable sessionId = generateSessionId(session);
        assignSessionId(session, sessionId);
        getRedisTemplate().opsForValue().set(sessionId,session);
        return sessionId;
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
        return (Session) getRedisTemplate().opsForValue().get(sessionId);
    }

    @Override
    protected void doUpdate(Session session) {
        getRedisTemplate().opsForValue().set(session.getId(),session);
    }

    @Override
    protected void doDelete(Session session) {
        getRedisTemplate().delete(session.getId());
    }

    private RedisTemplate getRedisTemplate(){
        RedisTemplate redisTemplate = (RedisTemplate) ApplicationUtils.getContext("redisTemplate");
        return redisTemplate;
    }
}
```

> 配置

配置sessionManager和SessionDAO

然后再securityManager类中，指定sessionManager即可

```java
@Bean
public DefaultWebSecurityManager defaultWebSecurityManager(Realm userRealms){
    DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();

    // 设置自定义的sessionManager
    securityManager.setSessionManager(sessionManager());

    return securityManager;
}

/**
* session重写了一定要被securityManager接纳
* @return
*/
@Bean
public SessionManager sessionManager(){
    DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
    // 持久化sessionID，达到多态共享session，
    // 有该sessionID，就不用重新登录了。
    // 或者以防止服务器崩后重启，而导致会话中断
    sessionManager.setSessionDAO(heroWebSessionDAO());

    sessionManager.setGlobalSessionTimeout(15*1000);
    sessionManager.setSessionValidationInterval(20*1000);
    sessionManager.setDeleteInvalidSessions(true);
    // 禁止在url中追加sessionId信息
    sessionManager.setSessionIdUrlRewritingEnabled(false);
    return sessionManager;
}

@Bean
public HeroWebSessionDAO heroWebSessionDAO(){
    HeroWebSessionDAO heroWebSessionDAO = new HeroWebSessionDAO();
    // 给当前有效的sessionID取名字，在数据库中存储的是hash类型
    heroWebSessionDAO.setActiveSessionsCacheName("shiro-activeSessionCache");
    // 设置生成sessionID的格式
    heroWebSessionDAO.setSessionIdGenerator(new JavaUuidSessionIdGenerator());
    return heroWebSessionDAO;
}
```

