# Golang 高阶

https://pkg.go.dev/std

## 并发



### 协程

Golang 中的并发是**函数**相互独立运行的能力。**Goroutines** 是并发运行的函数。Golang 提供了 Goroutines 作为并发处理操作的一种方式。

创建一个协程非常简单，就是在一个任务函数前面添加一个go关键字：

```go
go task()
```



**实例**

```go
package main

import (
	"fmt"
	"time"
)

func showMsg(str string) {
	for i := 0; i < 5; i++ {
		fmt.Println("msg: " + str)
		time.Sleep(time.Millisecond * 100)
	}
}

func main() {
	go showMsg("java") // 开启了另一个线程，执行
	showMsg("go")
}

```

运行结果

```
msg: go
msg: java
msg: java
msg: go
msg: go
msg: java
msg: java
msg: go
msg: go
msg: java
```



### 通道

Go 提供了一种称为通道的机制，用于在 goroutine 之间**共享数据**。当您作为 goroutine 执行并发活动时，需要在 goroutine 之间共享资源或数据，通道充当 goroutine 之间的管道（管道）并提供一种机制来保证同步交换。

需要在声明通道时指定**数据类型**。我们可以共享内置、命名、结构和引用类型的值和指针。数据在通道上传递：在任何给定时间只有一个 goroutine 可以访问数据项：因此按照设计不会发生数据竞争。

根据数据交换的行为，有两种类型的通道：**无缓冲通道**和**缓冲通道**。无缓冲通道用于执行goroutine之间的同步通信，而缓冲通道用于执行异步通信。无缓冲通道保证在发送和接收发生的瞬间执行两个goroutine之间的交换。缓冲通道没有这样的保证。

**通道有make函数创建，该函数指定chan关键字和通道的元素类型**

#### 定义

```go
unbuffered := make(chan int) // 整型无缓冲通道
buffered := make(chan int, 10) // 整型有缓冲通道
```

使用内置函数`make`创建无缓冲和缓冲通道。`make`的第一个参数需要关键字`chan`，然后是通道 允许交换的数据类型

#### 使用

**发送**

```go
buffered := make(chan string, 10) // 整型有缓冲通道
buffered <- "Vince" // 通过<-将字符串发送到通道buffered中
```

**接收**

```go
data := <- buffered // 从通道中接收字符串
```



#### 通道发送和接收的特性

1. 对于同一个通道，发送操作之间互斥，接收操作之间也是互斥
2. 发送操作和接收操作中对元素的处理都是不可分割的
3. 发送操作在完全完成之前会被阻塞；接收操作也是如此



#### 实例

```go
package main

import (
	"fmt"
	"math/rand"
	"time"
)

// 定义一个通道
var valuesChannel = make(chan int,1) // 第二个参数给channel设定一个容量大小，这样避免channel里的值没有被读取而产生阻塞现象

// 模拟发送信息到通道中
func send() {
	// 产生随机数
	rand.Seed(time.Now().UnixNano())
	value := rand.Intn(10)
	fmt.Printf("seed msg to channel: %d\n", value)
	time.Sleep(time.Millisecond * 1000) // 延迟发送
	// 发送信息到通道
	valuesChannel <- value
}

func main() {
	go send()
	defer close(valuesChannel) // 程序执行完毕之后关闭通道
	// 从通道中获取信息。如果通道中没有信息，会一直阻塞，直到成功拿到信息
	data := <-valuesChannel
	fmt.Printf("get msg from channel: %d\n", data)
}

```

输出结果

```
waiting 5 millisecond...
seed msg to channel: 3
get msg from channel: 3
```



#### 遍历

**方式一：for循环+if判断**

```go
package main

import "fmt"

var channel = make(chan int)

func main() {
	go func() {
		for i := 1; i <= 5; i++ {
			channel <- i
		}
		close(channel)
	}()

	// 方式一
	for i := 0; i < 3; i++ {  // 如果 i<10（只要大于写入个数）上方没有正常关闭通道，则会产生死锁
		r := <-channel
		fmt.Println(r)
	}
    
    // 另一种方式
    for{
        v, ok := <- channel
        if ok {
            fmt.Println(v)
        }else{
            break
        }
    }
}

```



**方式二：for range**

```go
package main

import "fmt"

var channel = make(chan int)

func main() {
	go func() {
		for i := 1; i <= 5; i++ {
			channel <- i
		}
		close(channel)
	}()

	// 方式二
	for v := range channel {
		fmt.Println(v)
	}
}

```



> 如果写多读少，不会发生死锁
>
> 如果写少读多，则将通道中的数据读完之后，还有一些读取操作没有读到内容时：
>
> 		- 如果在写的时候就正常关闭了通道，那么剩下的读取就会自动读通道类型的默认值
> 		- 如果在写的时候没有正常关闭通道，那么就会产生死锁的情况

### WaitGroup 同步

**实例演示**

查看添加`WaitGroup`和不添加`WaitGroup`的区别

```go
package main

import (
	"fmt"
	"sync"
)

var wp sync.WaitGroup

func showInfo(i int) {
	defer wp.Done() // waitgroup计数减1 等价于 wp.Add(-1)
	fmt.Println(i)
}
func main() {
	for i := 0; i < 6; i++ {
		go showInfo(i)
		wp.Add(1) // waitgroup计数增1
	}

	wp.Wait() // 等待waitgroup计数减到0，才会往下执行

	fmt.Println("end...")
}

```



### runtime包

- `runtime.Gosched()` 让出cpu，让其他协程执行
- `runtime.Goexit()` 退出当前协程
- `runtime.NumCPU()` 查看cpu个数
- `runtime.GOMAXPROCS(2)` 设置可使用的cpu核心数，这里设置的是2个



### Mutext互斥锁实现同步

除了使用channel实现同步之外，还可以使用Mutex互斥锁的方式实现同步。

```go
package main

import (
	"fmt"
	"sync"
	"time"
)

var lock sync.Mutex // mutext锁
var wg sync.WaitGroup // 等待

var i int = 100

func add() {
	defer wg.Done()
	lock.Lock() // 加锁
	i += 1
	fmt.Printf("i++: %d\n", i)
	time.Sleep(time.Millisecond * 50)
	lock.Unlock() // 解锁
}

func sub() {
	defer wg.Done()
	lock.Lock() // 加锁
	time.Sleep(time.Millisecond * 2)
	i -= 1
	fmt.Printf("i--: %d\n", i)
	lock.Unlock() // 解锁
}

func main() {
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go add()
		wg.Add(1)
		go sub()
	}

	wg.Wait()

	fmt.Printf("i: %d\n", i)

}
```



### channel 关闭

所有的channel接收者都会在channel关闭时，立刻从阻塞等待中返回且ok值为false。这个广播机制常被利用，进行向多个订阅者同时发送信号。如：退出信号

```go
func dataProducer(ch chan int, wg *sync.WaitGroup) {
	go func() {
		for i := 0; i < 10; i++ {
			ch <- i
		}
		close(ch) // channel关闭了，在消费者处当channel的信息都接收完之后，消费者直接退出，不会阻塞

		wg.Done()
	}()

}

func dataReceiver(ch chan int, wg *sync.WaitGroup) {
	go func() {
		for {
			if data, ok := <-ch; ok { // ok 如果时true则还有值需要消费，false则channel关闭了且无接收的信息
				fmt.Println(data)
			} else {
				break
			}
		}
		wg.Done()
	}()

}

func TestCloseChannel(t *testing.T) {
	var wg sync.WaitGroup
	ch := make(chan int)
	wg.Add(1)
	dataProducer(ch, &wg)
	wg.Add(1)
	dataReceiver(ch, &wg)
	// wg.Add(1)
	// dataReceiver(ch, &wg)
	wg.Wait()

}
```



### select  通道的选择

1. select是Go中的一个控制结构，类似于`switch`语句，用于处理异步IO操作。`select`会监听case语句中channel的读写操作，当case中channel读写操作为非阻塞状态（即能读写）时，将会触发相应的动作。

   > select中的case语句必须是一个channel操作
   >
   > select中的default子句总是可运行的。

2. 如果有多个`case`都可以运行，`select`会随机公平地选出一个执行，其他不会执行。

3. 如果没有可运行的`case`语句，且有`defaul`语句，那么就会执行`default`的动作。

4. 如果没有可执行的`case`语句，且没有`default`语句，`select`将阻塞，直到某个`case`通信可以运行



**实例**

```go
package main

import (
	"fmt"
	"time"
)

var chanInt = make(chan int)
var chanStr = make(chan string)

func main() {
	go func() {
		chanInt <- 100
		chanStr <- "Vince"
		// defer close(chanInt)
		// defer close(chanStr)
	}()

	for {
		select {
		case r := <-chanInt:
			fmt.Println(r)
		case r := <-chanStr:
			fmt.Println(r)
        case <-time.After(time.Second*1): // 超时控制
            fmt.Println("超时！")
		default:
			fmt.Println("default....")
		}
		time.Sleep(time.Second)
	}
}
```



### Timer 工具

Timer顾名思义，就是定时器的意思，可以实现一些定时操作，内部也是通过channel来实现的。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForGo/timer%E5%B7%A5%E5%85%B7.png)



### Ticker 工具

Timer只执行一次，Ticker可以周期的执行。

```go
package main

import (
	"fmt"
	"time"
)

func main() {
	var ticker = time.NewTicker(time.Second) // 每个一秒执行一次ticker

	var counter int

	var chanInt = make(chan int)
	go func() {
		for range ticker.C { // 每隔1s执行一次
			select { // 随机发送选择某条信息发送到通道中
			case chanInt <- 1:
			case chanInt <- 2:
			case chanInt <- 3:
			}
		}
	}()

	for c := range chanInt { // 接收通道中的信息
		fmt.Printf("接收：%d\n", c)
		counter += c
		if counter > 10 {
			ticker.Stop()
			break
		}
	}

}
```



### 原子操作

atomic 提供的原子操作能够确保任一时刻只有一个goroutine对变量进行操作，善用 atomic 能够避免程序中出现大量的锁操作。

atomic常见操作有：

- 增减
- 载入 read
- 比较并交换 cas
- 交换
- 存储 write

```go
var i int32 = 100

// 原子操作
// 加
atomic.AddInt32(&i,1)
// 减
atomic.AddInt32(&i,-1)
// 载入 读
atomic.LoadInt32(&i)
// 存储 写
atomic.StoreInt32(&i,100)
// cas 返回bool值，修改成功true，否则false
atomic.CompareAndSwapInt32(&i,100,200) // 参数 修改的变量地址、旧值、新值
```



### 并发模式

#### 单例模式 sync.Once.Do(func)

```go
type Singleton struct {
	data string
}

var singleInstance *Singleton
var once sync.Once

func GetSingletonObj() *Singleton {
	once.Do(func() { // 只执行一次
		fmt.Println("Create Obj")
		singleInstance = new(Singleton)
	})
	return singleInstance
}

func TestGetSingletonObj(t *testing.T) {
	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			obj := GetSingletonObj()
			fmt.Printf("%X\n", unsafe.Pointer(obj))
			wg.Done()
		}()
	}
	wg.Wait()
}
```



#### 多个协程任意协程完成任务就返回

```go
func runTask(id int) string {
	time.Sleep(10 * time.Millisecond)
	return fmt.Sprintf("The result is from %d", id)
}

func FirstResponse() string {
	numOfRunner := 10
	ch := make(chan string, numOfRunner) // channel提供buffer是为了防止协程泄露，防止多个协程一直阻塞在系统中
	for i := 0; i < numOfRunner; i++ {
		go func(i int) {
			ret := runTask(i)
			ch <- ret // 往channel中放入消息
		}(i)
	}
	return <-ch // 只要有channel中有消息，就返回
}

func TestFirstResponse(t *testing.T) {
	t.Log("Before:", runtime.NumGoroutine()) // 打印协程数
	t.Log(FirstResponse())
	time.Sleep(time.Second * 1)
	t.Log("After:", runtime.NumGoroutine()) // 打印协程数

}
```



#### 多个协程全部完成之后返回

```go
func runTask(id int) string {
	time.Sleep(10 * time.Millisecond)
	return fmt.Sprintf("The result is from %d", id)
}

func AllResponse() string {
	numOfRunner := 10 // 标识
	ch := make(chan string, numOfRunner)
	for i := 0; i < numOfRunner; i++ {
		go func(i int) { // 开启10个协程
			ret := runTask(i)
			ch <- ret // 都往channel中存放信息
		}(i)
	}
	finalRet := ""
	for j := 0; j < numOfRunner; j++ { // for循环直到10个都返回了才退出
		finalRet += <-ch + "\n" // 接收信息才会往下执行
	}
	return finalRet
}

func TestFirstResponse(t *testing.T) {
	t.Log("Before:", runtime.NumGoroutine())
	t.Log(AllResponse())
	time.Sleep(time.Second * 1)
	t.Log("After:", runtime.NumGoroutine())

}
```



## 反射

通过反射，获取成员、方法、成员的标签

```go
import (
	"fmt"
	"reflect"
	"testing"
)

func TestTypeAndValue(t *testing.T) {
	var f int64 = 10
	t.Log(reflect.TypeOf(f), reflect.ValueOf(f))
	t.Log(reflect.ValueOf(f).Type())
}

func CheckType(v interface{}) {
	t := reflect.TypeOf(v)
	switch t.Kind() {
	case reflect.Float32, reflect.Float64:
		fmt.Println("Float")
	case reflect.Int, reflect.Int32, reflect.Int64:
		fmt.Println("Integer")
	default:
		fmt.Println("Unknown", t)
	}
}

func TestBasicType(t *testing.T) {
	var f float64 = 12
	CheckType(&f)

}

func TestDeepEqual(t *testing.T) {
	a := map[int]string{1: "one", 2: "two", 3: "three"}
	b := map[int]string{1: "one", 2: "two", 3: "three"}
	//	t.Log(a == b)
	t.Log("a==b?", reflect.DeepEqual(a, b))

	s1 := []int{1, 2, 3}
	s2 := []int{1, 2, 3}
	s3 := []int{2, 3, 1}

	t.Log("s1 == s2?", reflect.DeepEqual(s1, s2))
	t.Log("s1 == s3?", reflect.DeepEqual(s1, s3))

	c1 := Customer{"1", "Mike", 40}
	c2 := Customer{"1", "Mike", 40}
	fmt.Println(c1 == c2)
	fmt.Println(reflect.DeepEqual(c1, c2))
}

type Employee struct {
	EmployeeID string
	Name       string `format:"normal"`
	Age        int
}

func (e *Employee) UpdateAge(newVal int) {
	e.Age = newVal
}

type Customer struct {
	CookieID string
	Name     string
	Age      int
}

func TestInvokeByName(t *testing.T) {
	e := &Employee{"1", "Mike", 30}
	//按名字获取成员
	t.Logf("Name: value(%[1]v), Type(%[1]T) ", reflect.ValueOf(*e).FieldByName("Name"))
	if nameField, ok := reflect.TypeOf(*e).FieldByName("Name"); !ok {
		t.Error("Failed to get 'Name' field.")
	} else {
        // 获取成员的Tag为format的值
		t.Log("Tag:format", nameField.Tag.Get("format"))
	}
	reflect.ValueOf(e).MethodByName("UpdateAge").
		Call([]reflect.Value{reflect.ValueOf(1)})
	t.Log("Updated Age:", e)
}
```



通过反射：结构体不同，但只要成员名一样且类型一致就可以通过fillBySettings将成员名相同的进行值填充

```go
import (
	"errors"
	"reflect"
	"testing"
)


type Employee struct {
	EmployeeID string
	Name       string `format:"normal"`
	Age        int
}

func (e *Employee) UpdateAge(newVal int) {
	e.Age = newVal
}

type Customer struct {
	CookieID string
	Name     string
	Age      int
}

func fillBySettings(st interface{}, settings map[string]interface{}) error {

	// func (v Value) Elem() Value
	// Elem returns the value that the interface v contains or that the pointer v points to.
	// It panics if v's Kind is not Interface or Ptr.
	// It returns the zero Value if v is nil.

	if reflect.TypeOf(st).Kind() != reflect.Ptr {
		return errors.New("the first param should be a pointer to the struct type.")
	}
	// Elem() 获取指针指向的值
	if reflect.TypeOf(st).Elem().Kind() != reflect.Struct {
		return errors.New("the first param should be a pointer to the struct type.")
	}

	if settings == nil {
		return errors.New("settings is nil.")
	}

	var (
		field reflect.StructField
		ok    bool
	)

	for k, v := range settings { // .Elem() 可以返回指针指向的具体结构
		if field, ok = (reflect.ValueOf(st)).Elem().Type().FieldByName(k); !ok {
			continue
		}
		if field.Type == reflect.TypeOf(v) {
			vstr := reflect.ValueOf(st)
			vstr = vstr.Elem()
			vstr.FieldByName(k).Set(reflect.ValueOf(v))
		}

	}
	return nil
}

func TestFillNameAndAge(t *testing.T) {
	settings := map[string]interface{}{"Name": "Mike", "Age": 30}
	e := Employee{}
	if err := fillBySettings(&e, settings); err != nil {
		t.Fatal(err)
	}
	t.Log(e)
	c := new(Customer)
	if err := fillBySettings(c, settings); err != nil {
		t.Fatal(err)
	}
	t.Log(*c)
}
```





## OS 标准库

os标准库实现了平台（操作系统）无关的编程接口。

- O_RDONLY：只读模式打开文件；
- O_WRONLY：只写模式打开文件；
- O_RDWR：读写模式打开文件；
- O_APPEND：写操作时将数据附加到文件尾部（追加）；
- O_CREATE：如果不存在将创建一个新文件；
- O_EXCL：和 O_CREATE 配合使用，文件必须不存在，否则返回一个错误；
- O_SYNC：当进行一系列写操作时，每次都要等待上次的 I/O 操作完成再进行；
- O_TRUNC：如果可能，在打开时清空文件。



### 目录、文件操作

```go
package main

import (
	"fmt"
	"os"
)

// 创建文件
func creatFile() {
	file, err := os.Create("a.txt")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(file.Name())
	}
}

// 创建目录
func creatDir() {
	// err := os.Mkdir("test/a/b", os.ModePerm) // os.ModePerm 最高权限
	err := os.MkdirAll("test/a/b", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}
}

// 删除目录、文件
func removeDirOrFile() {
	err := os.RemoveAll("test")
	if err != nil {
		fmt.Println(err)
	}
}

// 获得当前工作目录
func getWd() {
	dir, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(dir)
	}
	// 修改工作目录
	/*	os.Chdir("D:/")
		dir, _ = os.Getwd()
		fmt.Println(dir)*/

	// 获得当前临时目录
	tempDir := os.TempDir()
	fmt.Println(tempDir)
}

// 重命名
func reName() {
	err := os.Rename("test.txt", "b.txt")
	if err != nil {
		fmt.Println(err)
	}
}

// 读文件
func readFile() {
	f, err := os.ReadFile("b.txt")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(f[:]))
}

// 写文件
func writeFile() {
	var s string = "hello zhangwenxing 368"
	err := os.WriteFile("b.txt", []byte(s), os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}
}

func main() {
	// creatFile()
	// creatDir()
	// removeDirOrFile()
	// getWd()
	// reName()
	readFile()
	writeFile()
}
```



### 文件读操作

seek、openfile、close

```go
package main

import (
	"fmt"
	"io"
	"os"
)

func readOps() {
	// 打开文件
	f, _ := os.OpenFile("b.txt", os.O_RDONLY, 755)
    // 偏移从第3个位置开始读
	f.Seek(3, 0)
	// 循环读取
	var buf = make([]byte, 5)
	for {
		n, err := f.Read(buf)
		if err == io.EOF {
			break
		}
		fmt.Printf("读了%d个字节  ", n)
		fmt.Printf("读取的内容为：")
		// 只输出buf中读取的有效个数n
        fmt.Printf("%c", buf[0:n])
		fmt.Println()
	}
	// 关闭文件
	f.Close()
}

func main() {
	readOps()
}
```



偏移位置，读取

```go
n, err := f.ReadAt(buf, 3) // 从第3个位置开始读取
```



读目录，判断是否为文件夹

```go
de,err := os.ReadDir("D:/")
for _,v := range de{
    v.IsDir() // 判断是否为文件夹
}
```



### 文件写操作

```go
package main

import "os"

// 通过字节写
func writeByte() {
	f, _ := os.OpenFile("a.txt", os.O_RDWR|os.O_APPEND, 755)
	f.Write([]byte("zhangwenxing 368 "))
	f.Close()
}

// 通过字符串写
func writeString() {
	f, _ := os.OpenFile("a.txt", os.O_RDWR|os.O_APPEND, 755)
	f.WriteString("zhangwenxing 368 ")
	f.Close()
}

// 在指定位置写
func writeAt() {
	f, _ := os.OpenFile("a.txt", os.O_RDWR, 755)
	f.WriteAt([]byte("postgraduate "), 17*2)
	f.Close()
}

func main() {
	writeByte()
	writeString()
	writeAt()
}

```



### 进程操作

打开notepad的进程操作

```go
package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	// 获取当前进程id
	pid := os.Getpid()
	fmt.Printf("当前进程%d \n", pid)
	// 获取当前进程的父进程id
	ppid := os.Getppid()
	fmt.Printf("当前进程的父进程%d \n", ppid)

	// 进程属性设置
	attr := &os.ProcAttr{
		// files 指定新进程继承的活动文件对象
		// 分别为：标准输入、标准输出、标准错误输出
		Files: []*os.File{os.Stdin, os.Stdout, os.Stderr},
		// 新进程的环境变量
		Env: os.Environ(),
	}

	// 开启一个notepad进程
	p, err := os.StartProcess("C:\\Windows\\System32\\notepad.exe", []string{"C:\\Windows\\System32\\notepad.exe", "D:\\a.txt"}, attr)
	if err != nil {
		fmt.Println(err)
	}

	// 通过pid找到进程
	fmt.Printf("进程id%d\n", p.Pid)
	process, err := os.FindProcess(p.Pid)

	// 5s之后执行杀死进程操作
	time.AfterFunc(time.Second*5, func() {
		process.Signal(os.Kill)
	})

	// 等待进程的退出
	process.Wait()
}

```



### 环境变量操作

```go
package main

import (
	"fmt"
	"os"
)

func main() {
	// 列出所有环境变量
	// fmt.Printf("环境变量：%v \n", os.Environ())

	// 获取环境变量值
	javaHome8 := os.Getenv("JAVA_HOME_8")
	fmt.Println(javaHome8)

	// 查找环境变量
	env, ok := os.LookupEnv("GOPATH")
	if ok {
		fmt.Println(env)
	}

	// 设置环境变量
	err := os.Setenv("TEST_ZWX", "D:\\")
	if err != nil {
		fmt.Println(err)
	}

	env, ok = os.LookupEnv("TEST_ZWX")
	if ok {
		fmt.Println(env)
	}

	// 清空所有环境变量，慎用
	// os.Clearenv()
}
```



## 标准库

### io包

Go 语言中，为了方便开发者使用，将 IO 操作封装在了如下几个包中：

- io 为 IO 原语（I/O primitives）提供基本的接口 os File Reader Writer
- io/ioutil 封装一些实用的 I/O 函数
- fmt 实现格式化 I/O，类似 C 语言中的 printf 和 scanf format fmt
- bufio 实现带缓冲I/O



![](https://gitee.com/turbo30/study_pic/raw/master/pictureForGo/io%E5%8C%85.png)



### ioutil包

封装一些实用的 I/O 函数

| 名称      | 作用                                                     |
| :-------- | :------------------------------------------------------- |
| ReadAll   | 读取数据，返回读到的字节 slice                           |
| ReadDir   | 读取一个目录，返回目录入口数组 []os.FileInfo             |
| ReadFile  | 读一个文件，返回文件内容（字节slice）                    |
| WriteFile | 根据文件路径，写入字节slice                              |
| TempDir   | 在一个目录创建指定前缀名的临时目录，返回新临时目录的路径 |
| TempFile  | 在一个目录中创建指定前缀的临时文件，返回 os.File         |



### bufio包



> bufio包实现了有缓冲的I/O。它包装一个io.Reader或io.Writer接口对象，创建另一个也实现了该接口，且同时还提供了缓冲和一些文本I/O的帮助函数的对象。



默认缓冲区大小

```go
const (
    defaultBufSize = 4096
)
```

可通过`	bufio.NewReaderSize(f,100000)` 设置缓冲区大小



#### 读写

读

```go
package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	f, _ := os.Open("a.txt")
	defer f.Close()
	reader := bufio.NewReader(f) // 缓存  读
	res, _ := reader.ReadString('\n')
	fmt.Println(res)
}
```



`bufio.Reset()` 重置缓冲区

`bufio.ReadSlice(',')` 读取 	以逗号分隔



写

```go
package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {

	f, err := os.OpenFile("a.txt", os.O_APPEND, 777)
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()
	writer := bufio.NewWriter(f)
	writer.WriteString("BUFIO Writer")
	writer.Flush()
}

```



#### Scanner

```go
package main

import (
	"bufio"
	"fmt"
	"strings"
)

func test1() {
	reader := strings.NewReader("ABC DEF GHJKL MNO")
	scanner := bufio.NewScanner(reader)
	scanner.Split(bufio.ScanWords) // 将按空格或者换行符区分
	// scanner.Split(bufio.ScanBytes) // 以字节分隔
	// scanner.Split(bufio.ScanLines) // 以行分隔
	// scanner.Split(bufio.ScanRunes) // 将逐字返回，且多字节（如中文）不会乱码.
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

func main() {
	test1()
}

```



### log包

golang内置了`log`包，实现简单的日志服务。通过调用`log`包的函数，可以实现简单的日志打印功能。



`log`包中有3个系列的日志打印函数，分别`print`系列、`panic`系列、`fatal`系列。



| 函数系列 | 作用                                                         |
| :------- | :----------------------------------------------------------- |
| prin     | 单纯打印日志                                                 |
| panic    | 打印日志，抛出panic异常，panic后面的代码不会执行，但`defer`语句会执行 |
| fatal    | 打印日志，强制结束程序（os.Exit（1）），`defer`函数不会执行  |



**实例**

```go
package main

import (
	"log"
	"os"
)

// log 初始化 配置log
func init() {

	// 日志配置 日期、时间、长文件路径
	log.SetFlags(log.Ldate | log.Ltime | log.Llongfile)

	// 默认只有日期和时间

	// 日志输入格式：
	/*	2023/01/13 22:37:09 D:/Workspace/GoWorks/src/awesomeProject/39log.go:11: my log
		2023/01/13 22:37:09 D:/Workspace/GoWorks/src/awesomeProject/39log.go:12: my log 1130
		2023/01/13 22:37:09 D:/Workspace/GoWorks/src/awesomeProject/39log.go:15: Vince   24*/

	// 设置日志打印前缀
	log.SetPrefix("Vince log ")

	// 设置日志输出到文件中
	f, err := os.OpenFile("test.log", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 666)
	if err != nil {
		log.Fatal("日志文件打开错误")
	}
	log.SetOutput(f)
}

func main() {
    log.Print("my log")
    log.Printf("my log %d", 1130)
    var name string = "Vince"
    var age int = 24
    log.Println(name, " ", age)
}

```



**自定义log**

```go
package main

import (
	"log"
	"os"
)

// 自定义log 的变量
var logger *log.Logger

func init() {

	f, err := os.OpenFile("test.log", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 666)
	if err != nil {
		log.Fatal("日志文件打开错误")
	}
    // 日志输入到f文件中
	logger = log.New(f, "Vince ", log.Ldate|log.Ltime|log.Lshortfile)
    
}

func main() {
	logger.Print("my log")
	logger.Printf("my log %d", 1130)
	var name string = "Vince"
	var age int = 24
	logger.Println(name, " ", age)
}

```



### builtin

这个包提供了一些类型声明、变量和常量声明，还有一些便利函数，这个包不需要导入，这些变量和函数就可以直接使用。

#### 常用方法

`append()` 切片追加元素，返回新的切片

`len()` 计算长度

`print()`

`println()`

`panic()` 程序直接终止，抛出异常



#### new 和 make 的区别

`new`和`make`区别：

1. `make`只能用来分配和初始化类型为`slice`、`map`、`chan`的数据，`new`可以分配任意类型的数据
2. `new`分配返回的是指针，记类型`*T`；`make`返回引用，即`T`
3. `new`分配空间被清零，`make`分配后，会进行初始化



### bytes 与 字符串

bytes切片与字符串可以相互转换，调用的方法基本都可以公用



`bytes.Contains(b1,b2)` b1中是否包含b2

`bytes.Count(s, sep1)` s中包含sep1的个数

`bytes.Reapeat(b, 3)` 将b重复3次返回新的字节切片

`bytes.Replace(s, old, new, 2)` 将s中old字节替换为new新的字节，共替换2次。最后一个参数如果是0：不替换；-1：无限次替换

`bytes.Runes(s)` 将s的长度以单个字为单位计数。eg：“你好世界” 没有Runes方法的长度为12，使用Runes之后长度为4

`bytes.Join(b1,b2)` 将b1数组，以b2为分隔符，拼接在一起返回



### time

```go
package main

import "time"

func main() {
	unix := time.Now().Unix() // 获得时间戳
	timeObj := time.Unix(unix, 0)
	timeObj.Year()
	timeObj.Month()
	timeObj.Hour()
	timeObj.Minute()
	timeObj.Second()
}
```



**时间格式化**

```go
time.Format("2006/01/02 15:04") // 一定要用2006年1月2日 15:04来设置格式
```



**解析字符串格式的时间**

```go
loc, err := time.LoadLocation("Asia/Shanghai") // 获得时区
timeObj, err := time.ParseInLocation("2006/01/02 15:04", "2023/01/13 23:56", loc)
// 第一个参数 时间格式； 第二个参数 需要解析的字符串时间； 第三个参数 时区
```



### JSON

这个包可以实现json的编码和解码，就是将json字符串转换为`struct`，或者将`struct`转换为json。



#### 核心的两个函数



```go
func Marshal(v interface{}) ([]byte, error)
```

> 将struct编码成json，可以接收任意类型



```go
func Unmarshal(data []byte, v interface{}) error
```

> 将json转码成struct结构体



**实例**

```go
package main

import (
	"encoding/json"
	"fmt"
	"os"
)

type EPerson struct {
	Name  string
	Age   int
	Email string
}

type MPerson struct {
	Eper   EPerson
	Parent []string
}

// 结构体转json
func test1() {
	var p = EPerson{
		Name:  "Vince",
		Age:   24,
		Email: "vince@gmail.com",
	}
	json, _ := json.Marshal(p)
	fmt.Println(string(json))
}

// json转结构体
func test2() {
	var b = []byte(`{"Name":"Vince","Age":24,"Email":"vince@gmail.com"}`)
	var p EPerson
	json.Unmarshal(b, &p)
	fmt.Println(p)
}

// json转map
func test3() {
	var b = []byte(`{"Name":"Vince","Age":24,"Email":"vince@gmail.com"}`)
	var m map[string]interface{}
	json.Unmarshal(b, &m)
	for k, v := range m {
		fmt.Println(k)
		fmt.Println(v)
	}
}

// 通过encoder将json写入到文件中
func test4() {
	var p = MPerson{
		Eper: EPerson{
			Name:  "Vince",
			Age:   24,
			Email: "vince@gmail.com",
		},
		Parent: []string{"Big Vince", "Big Kite"},
	}
	file, _ := os.OpenFile("b.txt", os.O_TRUNC|os.O_WRONLY, 0766)
	defer file.Close()
	encoder := json.NewEncoder(file)
	encoder.Encode(p)
}

func main() {
	//test1()
	//test2()
	//test3()
	test4()
}

```





### XML



xml包实现xml解析



#### 核心的两个函数

```go
func Marshal(v interface{}) ([]byte, error)
```

> 将struct编码成xml，可以接收任意类型



```go
func Unmarshal(data []byte, v interface{}) error
```

> 将xml转码成struct结构体



```go
package main

import (
	"encoding/xml"
	"fmt"
)

type XPerson struct {
	XMLName xml.Name `xml:"Person"`
	Name    string   `xml:"name"`
	Age     int      `xml:"age"`
	Email   string   `xml:"email"`
}

// 将结构体转为xml
func test11() {
	var xp = XPerson{
		Name:  "Vince",
		Age:   24,
		Email: "vince@gmail.com",
	}
	x, _ := xml.MarshalIndent(xp, " ", "  ")
	// x, _ := xml.Marshal(xp)  // 无缩进
	fmt.Println(string(x))

	/*	<Person>
		<name>Vince</name>
		<age>24</age>
		<email>vince@gmail.com</email>
	</Person>*/

}

// 将xml转为结构体
func test22() {
	var s = ` <Person>
   <name>Vince</name>
   <age>24</age>
   <email>vince@gmail.com</email>
 	</Person>`
    
	var b = []byte(s)
	var xp XPerson
	xml.Unmarshal(b, &xp)
	fmt.Println(xp)
}

func main() {
	//test11()
	test22()
}
```

