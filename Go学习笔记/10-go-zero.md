# Go-Zero

go-zero-example：https://github.com/zeromicro/zero-examples

zero-contrib：https://github.com/zeromicro/zero-contrib

go-zero-issue：https://github.com/zeromicro/go-zero/issues



## goctl 命令

文档：https://go-zero.dev/cn/docs/goctl/goctl

### goctl api



#### api语法

https://go-zero.dev/cn/docs/design/grammar/

```protobuf
/**
 * api语法示例及语法说明
 */

// api语法版本
syntax = "v1"

// import literal
import "foo.api"

// import group
import (
    "bar.api"
    "foo/bar.api"
)
info(
    author: "songmeizi"
    date:   "2020-01-08"
    desc:   "api语法示例及语法说明"
)

// type literal

type Foo{
    Foo int `json:"foo"`
}

// type group

type(
    Bar{
        Bar int `json:"bar"`
    }
)

// service block
@server(
    jwt:   Auth // 继承jwt鉴权
    group: foo // 将生成的代码放到foo文件夹下 
    prefix: foo // 路由的group 路由前缀
)
service foo-api{
    @doc "foo" // 用于生成文档
    @handler foo // 生成foo对应的handler类似java的controller层
    post /foo (Foo) returns (Bar) // 请求方式 请求 传参 returns 相应参数
}
```



>  **type 字段**

- tag表

  绑定参数时，以下四个tag只能选择其中一个

  | tag key | 描述                                                         | 提供方  | 有效范围          | 示例            |
  | ------- | ------------------------------------------------------------ | ------- | ----------------- | --------------- |
  | json    | json序列化tag                                                | golang  | request、response | `json:"fooo"`   |
  | path    | 路由path，如`/foo/:id`                                       | go-zero | request           | `path:"id"`     |
  | form    | 标志请求体是一个form（POST方法时）或者一个query(GET方法时`/search?name=keyword`) | go-zero | request           | `form:"name"`   |
  | header  | HTTP header，如 `Name: value`                                | go-zero | request           | `header:"name"` |

- tag修饰符

  常见参数校验描述

  | tag key  | 描述                                  | 提供方  | 有效范围 | 示例                         |
  | -------- | ------------------------------------- | ------- | -------- | ---------------------------- |
  | optional | 定义当前字段为可选参数                | go-zero | request  | `json:"name,optional"`       |
  | options  | 定义当前字段的枚举值,多个以竖线\|隔开 | go-zero | request  | `json:"gender,options=male"` |
  | default  | 定义当前字段默认值                    | go-zero | request  | `json:"gender,default=male"` |
  | range    | 定义当前字段数值范围                  | go-zero | request  | `json:"age,range=[0:120]"`   |



#### 命令

```bash
goctl api go -api user.api -dir . -style goZero
```

`-api` 指定api文件

`-dir` 指定生成的文件路径

`-style`指定代码格式，可以是goZero驼峰，gozero全小写，go_zero下划线形式



### goctl docker

```bash
goctl docker -go hello.go
```





### goctl rpc

```bash
goctl rpc protoc user.proto --go_out=. --go-grpc_out=. --zrpc_out=. --style=goZero
```

> 1. --go_out 与 --go-grpc_out 生成的最终目录必须一致
> 2. --go_out & --go-grpc_out 和 --zrpc_out 的生成的最终目录必须不为同一目录，否则pb.go和_grpc.pb.go就与main函数同级了，这是不允许的。 --go_out 与 --go-grpc_out 生产的目录会受 --go_opt 和 --grpc-go_opt 和proto源文件中 go_package值的影响，要想理解这里的生成逻辑，建议阅读 官方文文档：[Go Generated Code](https://developers.google.com/protocol-buffers/docs/reference/go-generated)



### goctl model

#### 生成规则

- 默认规则

  我们默认用户在建表时会创建createTime、updateTime字段(忽略大小写、下划线命名风格)且默认值均为`CURRENT_TIMESTAMP`，而updateTime支持`ON UPDATE CURRENT_TIMESTAMP`，对于这两个字段生成`insert`、`update`时会被移除，不在赋值范畴内，当然，如果你不需要这两个字段那也无大碍。

- ddl

  ```bash
  $ goctl model mysql ddl -h
  
  NAME:
  
     goctl model mysql ddl - generate mysql model from ddl
  
    
  
  USAGE:
  
     goctl model mysql ddl [command options] [arguments...]
  
    
  
  OPTIONS:
  
     --src value, -s value         the path or path globbing patterns of the ddl
  
     --dir value, -d value         the target dir
  
     --style value                 the file naming format, see [https://github.com/zeromicro/go-zero/tree/master/tools/goctl/config/readme.md]
  
     --cache, -c                   generate code with cache [optional]
  
     --idea                        for idea plugin [optional]
  
     --database value, --db value  the name of database [optional]
  
     --home value                  the goctl home path of the template, --home and --remote cannot be set at the same time, if they are, --remote has higher priority
  
     --remote value                the remote git repo of the template, --home and --remote cannot be set at the same time, if they are, --remote has higher priority
  
  ​                                 The git repo directory must be consistent with the https://github.com/zeromicro/go-zero-template directory structure
  
     --branch value                the branch of the remote repo, it does work with --remote
  ```

- datasource

  ```bash
  $ goctl model mysql datasource -h
  
  NAME:
  
     goctl model mysql datasource - generate model from datasource
  
    
  
  USAGE:
  
     goctl model mysql datasource [command options] [arguments...]
  
    
  
  OPTIONS:
  
     --url value              the data source of database,like "root:password@tcp(127.0.0.1:3306)/database"
  
     --table value, -t value  the table or table globbing patterns in the database
  
     --cache, -c              generate code with cache [optional]
  
     --dir value, -d value    the target dir
  
     --style value            the file naming format, see [https://github.com/zeromicro/go-zero/tree/master/tools/goctl/config/readme.md]
  
     --idea                   for idea plugin [optional]
  
     --home value             the goctl home path of the template, --home and --remote cannot be set at the same time, if they are, --remote has higher priority
  
     --remote value           the remote git repo of the template, --home and --remote cannot be set at the same time, if they are, --remote has higher priority
  
  ​                            The git repo directory must be consistent with the https://github.com/zeromicro/go-zero-template directory structure
  
     --branch value           the branch of the remote repo, it does work with --remote
  ```

生成代码仅基本的CURD结构。



#### 脚本

**命令：**

```bash
goctl model mysql datasource -url="${username}:${passwd}@tcp(${host}:${port})/${dbname}" -table="${tables}"  -dir="${modeldir}" -cache=true --style=goZero
```



**shell文件：**

genModel.sh

```shell
#!/usr/bin/env bash

# 使用方法：
# ./genModel.sh usercenter user
# ./genModel.sh usercenter user_auth
# 再将./genModel下的文件剪切到对应服务的model目录里面，记得改package


#生成的表名
tables=$2
#表生成的genmodel目录
modeldir=./genModel

# 数据库配置
host=127.0.0.1
port=33069
dbname=$1
username=root
passwd=PXDN93VRKUm8TeE7


echo "开始创建库：$dbname 的表：$2"
goctl model mysql datasource -url="${username}:${passwd}@tcp(${host}:${port})/${dbname}" -table="${tables}"  -dir="${modeldir}" -cache=true --style=goZero
```

使用

```shell
sh genModel.sh db_order t_order
```

