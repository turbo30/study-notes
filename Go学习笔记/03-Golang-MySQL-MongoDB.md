# GO - MySQL



## 获取驱动

https://pkg.go.dev/

```
go get -u github.com/go-sql-driver/mysql
```



## 测试

```go
package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

// 全局变量
var db *sql.DB

// 连接数据库
func initDB() (err error) {
	db, err = sql.Open("mysql", "root:123456@(172.0.0.1:3306)/go_db")
	if err != nil {
		return err
	}
	// 最大连接时长
	db.SetConnMaxLifetime(time.Minute * 3)
	// 最大连接数
	db.SetMaxOpenConns(10)
	// 空闲连接数
	db.SetMaxIdleConns(10)

	// 连接数据库
	err = db.Ping()
	if err != nil {
		fmt.Println("数据库连接失败")
		return err
	}
	return nil
}

// 关闭数据库
func closeDB() {
	db.Close()
}

func main() {
	err := initDB()
	if err != nil {
		panic("初始化数据库失败！")
	}
    
    // 查询
	rows, _ := db.Query("select * from user")
	var id int
	var username, password string
	for rows.Next() {
		rows.Scan(&id, &username, &password)
		fmt.Println(id, "  ", username, "  ", password)
	}
	closeDB()
}

```



## 插入操作

```go
package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

type User struct {
	id       int
	username string
	password string
}

.......

// 插入数据
func insertData(username, password string) {
	var str = "insert into user(username, password) values(?,?)"
	res, err := db.Exec(str, username, password) // 插入成功返回id
	if err != nil {
		fmt.Println("插入数据失败！", err)
		return
	}
	id, _ := res.LastInsertId()
	fmt.Println("插入数据成功，ID为：", id)

}

func main() {
	err := initDB()
	if err != nil {
		panic("初始化数据库失败！")
	}

	// 插入数据
	insertData("lily", "123456789")


	closeDB()
}
```



## 查询操作

```go
package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

type User struct {
	id       int
	username string
	password string
}

......

// 查询操作
func selectAll() {
	var user User
	var sql = "select * from user"
	rows, _ := db.Query(sql)
	// 非常重要，关闭row可以释放持有的数据库连接
	defer rows.Close()
	// db.QueryRow(sql, 1).Scan(&user.id, &user.username, &user.password) // 查一条数据
	for rows.Next() {
		// 一定要调用Scan，否则持有数据库连接不会被释放
		rows.Scan(&user.id, &user.username, &user.password)
		fmt.Println(user)
	}
}

func main() {
	err := initDB()
	if err != nil {
		panic("初始化数据库失败！")
	}

	// 查询所有记录
	selectAll()

	closeDB()
}

```



## 更新操作

```go
package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

type User struct {
	id       int
	username string
	password string
}

......

// 更新操作
func updateData(password string, id int) {
	var sql = "update user set password=? where id=?"
	res, _ := db.Exec(sql, password, id)
	rowsAffected, _ := res.RowsAffected()
	fmt.Println("更新成功，更新的行数为：", rowsAffected)
}

func main() {
	err := initDB()
	if err != nil {
		panic("初始化数据库失败！")
	}

	updateData("789456", 1)

	closeDB()
}

```



## 删除操作

```go
package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

type User struct {
	id       int
	username string
	password string
}

......

// 删除操作
func deleteData(id int) {
	var sql = "delete from user where id = ?"
	result, err := db.Exec(sql, id)
	if err != nil {
		fmt.Println("删除数据失败 ", err)
	}
	rowsAffected, err := result.RowsAffected()
	if err == nil {
		fmt.Println("删除成功，删除的行数为：", rowsAffected)
	}
}

func main() {
	err := initDB()
	if err != nil {
		panic("初始化数据库失败！")
	}

	deleteData(5)

	// 查询所有记录
	selectAll()

	closeDB()
}
```



# GO - MongoDB



## 获取驱动

https://pkg.go.dev/

```go
go get go.mongodb.org/mongo-driver/mongo
```



## 连接测试

```go
package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

// 全局
var client *mongo.Client

// 初始化连接
func initMongoDB() {
	var err error
	// "mongodb://user:password@localhost:27017/?authSource=admin"
	client, err = mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://root:vince1130@139.159.220.191:27017"))
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB")
}

func main() {
	initMongoDB()
}
```



## 插入操作

```go
package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type Student struct {
	Name string
	Age  int
}

var client *mongo.Client

func initMongoDB() {
	var err error
	// "mongodb://user:password@localhost:27017/?authSource=admin"
	client, err = mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://root:123456@127.0.0.1:27017"))
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB")
}

func addOne() {
	s1 := Student{Name: "Vince", Age: 24}
	collection := client.Database("go_db").Collection("students")
	result, err := collection.InsertOne(context.TODO(), s1)
	if err != nil {
		log.Fatal("插入数据失败 ", err)
	}
	fmt.Println("插入成功，ID为：", result.InsertedID)
}

func addMany() {
	collection := client.Database("go_db").Collection("students")
	s1 := Student{Name: "Tom", Age: 20}
	s2 := Student{Name: "Lily", Age: 21}
	students := []interface{}{s1, s2}
	res, err := collection.InsertMany(context.TODO(), students)
	if err != nil {
		log.Fatal("插入多个数据失败")
	}
	fmt.Println("插入成功，IDs为：", res.InsertedIDs)

}

func main() {
	initMongoDB()
	addOne()
	addMany()
}
```



## 查找操作 bson

`bson.D{{"name", "Tom"}}`可以设置查找条件



```go
package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type Student struct {
	Name string
	Age  int
}

var client *mongo.Client

......

func disconnect(ctx context.Context) {
	client.Disconnect(ctx)
}

// 查找数据
func findData() {
	ctx := context.TODO()
	defer disconnect(ctx)
	collection := client.Database("go_db").Collection("students")
	// bson.D{} 中可以写查询条件
	cur, err := collection.Find(ctx, bson.D{{"name", "Tom"}})
	if err != nil {
		log.Fatal("查找失败 ", err)
	}
	defer cur.Close(ctx)
	for cur.Next(ctx) {
		var res bson.D
		err := cur.Decode(&res) // 解析数据到res中
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(res)
		fmt.Println(res.Map()["name"]) // 获取数据中key为name的值
	}
}

func main() {
	initMongoDB()
	findData()
}
```



## 更新操作

`bson.D{{"$set", bson.D{{"key", "value"}, {"key", value}}}}` **$set** 必须写，表示更新的意思

```go
package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type Student struct {
	Name string
	Age  int
}

var client *mongo.Client

......

func disconnect(ctx context.Context) {
	client.Disconnect(ctx)
}


func updateMDb() {
	ctx := context.TODO()
	collection := client.Database("go_db").Collection("students")
	update := bson.D{{"$set", bson.D{{"name", "Big Tom"}, {"age", 30}}}}
    // 上下文； 更新条件； 更新语句
	res, err := collection.UpdateMany(ctx, bson.D{{"name", "Tom"}}, update)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("更新成功，共更新了：", res.ModifiedCount)
}

func main() {
	initMongoDB()
	updateMDb()
	findData()
}
```



## 删除操作

```go
package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type Student struct {
	Name string
	Age  int
}

var client *mongo.Client

......

func deleteMDb() {
	ctx := context.TODO()
	collection := client.Database("go_db").Collection("students")
    // bson.D{{"name", "Big Tom"}} 删除条件
	res, err := collection.DeleteMany(ctx, bson.D{{"name", "Big Tom"}})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("删除成功，共删除了：", res.DeletedCount)
}

func main() {
	initMongoDB()
	deleteMDb()
	findData()
}
```



