# Gin - Web框架

```go
go get -u github.com/gin-gonic/gin
```





```go
package main

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

type Person struct {
	Name   string `json:"name"`
	Age    int    `json:"age"`
	Email  string `json:"email"`
	Gender string `json:"-"` // - 转json时会忽略该字段
}

var person Person

func init() {
	person = Person{
		Name:   "Vince",
		Age:    24,
		Email:  "vince@gmail.com",
		Gender: "男",
	}
}

// 自定义拦截器
func myHandler() gin.HandlerFunc {
	return func(context *gin.Context) {
		context.Set("usersession", "user-01")
		context.Next() // 放行
		// context.Abort() // 阻止
	}
}

func main() {
	// 创建一个服务
	ginServer := gin.Default()
    
    // 全局使用拦截器
	ginServer.Use(myHandler())

	// 加载所有的html页面
	ginServer.LoadHTMLGlob("templates/*")
	// 加载css样式，js
	ginServer.Static("/statics", "./statics")

	// 访问地址，处理请求    Request     Response
	ginServer.GET("/user", func(context *gin.Context) {
		context.JSON(200, person)
	})

	// 返回html页面 经过拦截器
	ginServer.GET("/index", myHandler(), func(context *gin.Context) {
		log.Print("userSession: ", context.MustGet("usersession"))
		context.HTML(http.StatusOK, "index.html", person)
	})

	// /user/info?username=Vince&password=113011
	ginServer.GET("/user/info", func(context *gin.Context) {
		username := context.Query("username")
		password := context.Query("password")
		context.JSON(http.StatusOK, gin.H{"username": username, "password": password})
	})

	// /user/info/turbo/1130
	ginServer.GET("/user/info/:username/:password", func(context *gin.Context) {
		username := context.Param("username")
		password := context.Param("password")
		context.JSON(http.StatusOK, gin.H{"username": username, "password": password})
	})

	// 获取post的json数据
	ginServer.POST("/user/info", func(context *gin.Context) {
		data, _ := context.GetRawData()
		var person Person
		json.Unmarshal(data, &person)
		log.Print(person)
		context.JSON(http.StatusOK, person)
	})

	// form表单
	ginServer.POST("/user/form", func(context *gin.Context) {
		username := context.PostForm("username")
		password := context.PostForm("password")
		context.JSON(http.StatusOK, gin.H{"username": username, "password": password})
	})

	// 重定向 301
	ginServer.GET("/red", func(context *gin.Context) {
		context.Redirect(http.StatusMovedPermanently, "http://127.0.0.1:8080/index")
	})

	// 404
	ginServer.NoRoute(func(context *gin.Context) {
		context.HTML(http.StatusNotFound, "404.html", nil)
	})

	// 路由组 /user下的所有请求进入这里处理
	userGroup := ginServer.Group("/user")
	{
		userGroup.GET("/add", func(context *gin.Context) {
			context.JSON(http.StatusOK, "添加成功Add")
		})
		userGroup.GET("/login")
		userGroup.GET("/logout")
	}

	// 服务端口号
	ginServer.Run(":8080")
}

```

