# Xorm

官网：https://xorm.io/

文档：https://gitea.com/xorm/xorm/src/branch/master/README_CN.md



## 安装

```bash
go get xorm.io/xorm
```



## 实例

官方文档非常详细

```go
package main

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql" // 不能省略
	"log"
	"time"
	"xorm.io/xorm"
)

func main() {
	var (
		userName  string = "root"
		password  string = "vince1130"
		ipAddress string = "139.159.220.191"
		port      int    = 3306
		dbName    string = "go_db"
		charset   string = "utf8mb4"
	)

	dataSource := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s", userName, password, ipAddress, port, dbName, charset)

	// 创建引擎，连接数据库
	engine, _ := xorm.NewEngine("mysql", dataSource)
	type User struct {
		Id      int64
		Name    string
		Salt    string
		Age     int
		Passwd  string    `xorm:"varchar(200)"`
		Created time.Time `xorm:"created"`
		Updated time.Time `xorm:"updated"`
	}

	// 同步结构体到数据库，自动创建表。结构体增加字段，数据库会自动创建对应的列
	err := engine.Sync(new(User))
	if err != nil {
		log.Panic(err)
	}

	// 插入数据
    var user1 = User{
        Id:     1,
        Name:   "vince",
        Salt:   "zzz",
        Age:    24,
        Passwd: "1130",
    }
    var user2 = User{
        Id:     2,
        Name:   "vinceZ",
        Salt:   "xxx",
        Age:    24,
        Passwd: "113011",
    }
    var user = []User{user1, user2}
    n, err := engine.Insert(&user)
    if err != nil {
        log.Panic(err)
    }
    log.Printf("成功增加了%d条数据.", n)

	// 更新数据，根据ID更新数据，要更新哪个字段user就设置哪个字段
    var user = User{Name: "Will"}
    n, err := engine.ID(2).Update(&user)
    if err != nil {
        log.Panic(err)
    }
    log.Printf("成功更新了%d条数据.", n)

	// 删除数据，根据ID删除数据
    user = User{Name: "z"}
    n, err := engine.ID(1000).Delete(&user)
    if err != nil {
        log.Panic(err)
    }
    log.Printf("成功删除了%d条数据.", n)

	// 查询数据
}

```



## 事务

- 在一个Go程中有事务

```Go
session := engine.NewSession()
defer session.Close()

// add Begin() before any action
if err := session.Begin(); err != nil {
    // if returned then will rollback automatically
    return err
}

user1 := Userinfo{Username: "xiaoxiao", Departname: "dev", Alias: "lunny", Created: time.Now()}
if _, err := session.Insert(&user1); err != nil {
    return err
}

user2 := Userinfo{Username: "yyy"}
if _, err := session.Where("id = ?", 2).Update(&user2); err != nil {
    return err
}

if _, err := session.Exec("delete from userinfo where username = ?", user2.Username); err != nil {
    return err
}

// add Commit() after all actions
return session.Commit()
```

- 事务的简写方法

```Go
res, err := engine.Transaction(func(session *xorm.Session) (interface{}, error) {
    user1 := Userinfo{Username: "xiaoxiao", Departname: "dev", Alias: "lunny", Created: time.Now()}
    if _, err := session.Insert(&user1); err != nil {
        return nil, err
    }

    user2 := Userinfo{Username: "yyy"}
    if _, err := session.Where("id = ?", 2).Update(&user2); err != nil {
        return nil, err
    }

    if _, err := session.Exec("delete from userinfo where username = ?", user2.Username); err != nil {
        return nil, err
    }
    return nil, nil
})
```

