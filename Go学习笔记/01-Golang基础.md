

# Golang

官方文档：https://pkg.go.dev/std

学习视频【**郭宏志-老郭**】https://www.bilibili.com/video/BV1zR4y1t7Wj?p=1&vd_source=f603c8704f45c1725a1123d19f6f0069



## :four_leaf_clover: 数据类型



### 本数字类型



#### bool类型

```go
// bool类型 默认值 false
var isBool bool = true
fmt.Println(isBool)
fmt.Printf("%T,%t", isBool, isBool) // %T：类型   %t：当前值
```



#### 整型

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForGo/int%E6%95%B0%E6%8D%AE%E7%B1%BB%E5%9E%8B.png)

```go
	// int类型
	// uint 无符号； int 有符号
	// uint8、uint16、uint32、uint64
	// int8、int16、int32、int64
```



#### 浮点型

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForGo/float%E6%95%B0%E6%8D%AE%E7%B1%BB%E5%9E%8B.png)

```go
	// float类型 默认6位小数，四舍五入
	// float32、float64
```



#### 其他更多数字类型

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForGo/%E6%95%B0%E6%8D%AE%E7%B1%BB%E5%9E%8B.png)



#### 字符串

```go
var str string = "Vince"
str = "Chang"

str1 := 'A' // 输出65 类型int64
str2 := "B" // 输出B  类型string

// 字符串拼接
fmt.Println("Vince" + ",Chang")


```



### 数据类型转换

Go中一定是显式转换

```go
a := 3 // int
c := float64(a) // float
d := int(a) // int

```



## :four_leaf_clover: 算术运算符

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForGo/%E7%AE%97%E6%9C%AF%E8%BF%90%E7%AE%97%E7%AC%A6.png)



## :four_leaf_clover: 关系运算符

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForGo/%E5%85%B3%E7%B3%BB%E8%BF%90%E7%AE%97%E7%AC%A6.png)

```go
a := 2
b := 3

if a>b {
    fmt.Println("in....")
}else if a==b {
    fmt.Println("sconde...")
}else{
    fmt.Println("other...")
}
```



## :four_leaf_clover:逻辑关系符

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForGo/%E9%80%BB%E8%BE%91%E8%BF%90%E7%AE%97%E7%AC%A6.png)



## :four_leaf_clover:循环结构



### switch

默认

```go
switch {
    case false:
    	fmt.Println("1. case 为false")
    case true:
    	fmt.Println("2. case 为true")
}
// switch 没有表达式 默认为false 和 true 两类
```



### fallthrough 

```go
a := false
switch a {
    case false:
    	fmt.Println("1. case 为false")
    case true:
    	fmt.Println("2. case 为true")
}
// 输出:
// 1. case 为false

--------------------------------------fallthrough-----------------------------------------

a := false
switch a {
    case false:
    	fmt.Println("1. case 为false")
    	fallthrough // case穿透
    case true:
        if a==false {
            break // 终止fallthrough引发的case穿透
        }
    	fmt.Println("2. case 为true")
}
// 输出:
// 1. case 为false
// 2. case 为true
```



## :four_leaf_clover:数组

数组是**相同数据类型**的一组数据的集合，数组一旦定义**长度不能修改**，数组可以通过**下标（或者叫索引）**来访问元素。

### 定义

数组定义的语法如下：

```go
var variable_name [SIZE] variable_type
```

`variable_name`：数组名称

`SIZE`：数组长度，必须是常量

`variable_type`：数组保存元素的类型

**实例**

```go
package main

import "fmt"

func main(){
    var arr [3]int
}
```



### 初始化

```go
// 1
var arr1 = [3]int{1,3,4}

// 2
arr2 := [2]int{1,2}

// 3 省略数组长度
arr3 := [...]int{1,3,4,5,6}

// 4 指定索引进行初始化 arr4[0]值为1；arr4[3]值为11
arr4 := [...]int{0:1, 3:11}
```



数组长度获取：`len(arr)`



## :four_leaf_clover:字符串方法

str := "Vince"

- 获取长度 `len(str)`

- 获取第一个字符  `str[0]` 

- str.for 形成的for循环遍历字符串

  ```go
  str := "Vince"
  for i, v := range str {
      fmt.Printf("%d", i) // 输出index
      fmt.Printf("%c", v) // 输出对应的值
      fmt.Printf("\n")
  }
  
  // 输出：
  // 0V
  // 1i
  // 2n
  // 3c
  // 4e
  ```




## :four_leaf_clover:切片

前面我们学习了数组，数组是固定长度，可以容纳相同数据类型的元素的集合。当长度固定时，使用还是带来一些限制，比如：我们申请的长度太大浪费内存，太小又不够用。

鉴于上述原因，我们有了go语言的切片，可以把切片理解为，可变长度的数组，其实它底层就是使用数组实现的，增加了**自动扩容**功能。切片（Slice）是一个拥有相同类型元素的可变长度的序列。

### 语法

声明一个切片和声明一个数组类似，只要不添加长度就可以了

```go
var identifier []type
// var s1 []int
```

切片是引用类型，可以使用`make`函数来创建

```go
var s2 = make([]int, 2)
// make的第一个参数是类型，第二个参数是长度
```



### 初始化

**使用数组初始化**

```go
arr := [...]int{1,3,4}
s1 := arr[:] // 将arr中所有元素初始化给s1切片
```

**使用数组的部分元素初始化**

```go
arr := [...]int{1,3,4,45,6,7,9}
s1 := arr[2:5] // 用arr数组中的索引从2到4的元素进行初始化
s2 := arr[2:] // 用arr数组中索引从2开始到最后一个元素进行初始化
s3 := arr[:3] // 用arr数组中索引从0开始到索引为2的元素进行初始化
```



### 添加、删除、拷贝

**添加元素**

```go
var s1 = []int{}
s1 = append(s1,100)
s1 = append(s1,200)
s1 = append(s1,300)
// s1 存： [100,200,300]
```

**删除元素**

```go
// 删除索引为2的元素
var s1 = []int{1,2,3,4}
s1 = append(s1[:2],s1[3:]...)
// 将s1的索引从0到1的元素 和 s1的索引从3到最后的元素 拼接起来赋给s1 则将索引为2的元素删除
```

**拷贝**

```go
var s1 = []int{1,2,3,4}
s2 := s1 // s2与s1用的同一个内存地址

// 正确拷贝
var s3 = make([]int, 4)
copy(s3,s1) // 将s1拷贝到s3中
```



## :four_leaf_clover: Map

map是一种`key:value`键值对的数据结构容器。map内部实现是哈希表(`hash`)。**无序**

map 最重要的一点是通过 key 来快速检索数据，key 类似于索引，指向数据的值。

map是引用类型的。



### 语法

可以使用内建函数 make 也可以使用 map 关键字来定义 map

```go
/* 声明变量，默认 map 是 nil */
var map_variable map[key_data_type]value_data_type
/* 使用 make 函数 */
map_variable = make(map[key_data_type]value_data_type)
```

`map_variable`：map变量名称

`key_data_type`：key的数据类型

`value_data_type`：值的数据类型

**实例**

```go
// 方式一
m1 := make(map[string]string)
m1["name"] = "tom"
m1["age"] = "20"

// 方式二
m2 := map[string]string{
    "name":"tome",
    "age":"20",
}
```



### 访问

```go
m1 := map[string]string{
    "name":"tome",
    "age":"20",
}

var value = m1["name"]
fmt.Println(value)
```



### 判断某个键是否存在

```go
value, ok := map[key]
```

`value`：key在map中存在则返回对应的值

`ok`：key在map中存在为true，不存在则为false

如果key在map中存在则返回key对应的value和true，否则ok为false



### 遍历

```go
m1 := map[string]string{
    "name":"tome",
    "age":"20",
}

for k,v := range m1 {
    fmt.Println(k,v) // k为key，v为value
}
```





## :four_leaf_clover:函数

在GO语言中**公共函数以大写字母开始，私有函数以小写字母开头**



```go
package main

import "fmt"

func main() {
	printInfo()
	haveOnePar(1)
	haveTowPar(1, 2)
	haveOneRet(1)
	haveMoreRetSwap(1, 2)
}

// 无参无返回
func printInfo() {
	fmt.Println("printInfo")
}

// 有一个参数
func haveOnePar(a int) {
	fmt.Println(a)
}

// 有两个参数
func haveTowPar(a, b int) {
	fmt.Println(a, b)
}

// 有一个返回值
func haveOneRet(a int) int {
	return a
}

// 有多个返回值
func haveMoreRetSwap(a, b int) (int, int) {
	a,b = b,a
	return a, b
}

```



### 可变参数

```go
func myfunc(arg ...int){}
```



```go
package main

import "fmt"

func main() {
	varPar(1, 3, 5, 7, 9)
}

func varPar(sums ...int) {
	sum := 0
	for i := 0; i < len(sums); i++ {
		sum += sums[i]
	}
	fmt.Println(sum) // 输出25
}
```



### 值传递与引用传递

值传递

```go
package main

import "fmt"

func main() {
	// 方式一
	var arr1 [4]int
	for i := 0; i < 4; i++ {
		arr1[i] = i + 1
	}
	fmt.Println(arr1)

	// 方式二
	arr := [4]int{1, 2, 3, 4}
	fmt.Println(arr)
	// 值传递，不会对原始数据有影响
	updateArr(arr)
	fmt.Println(arr)
	// 默认值传递的数据类型：基础类型、数组、struct

}

func updateArr(arr [4]int) {
	arr[1] = 11
}
```



引用传递

```go
package main

import "fmt"

func main() {
	slice1 := []int{1, 2, 3, 4}
	fmt.Println(slice1)
	// 切片，引用传递，函数可以修改原始数据
	updateSlice(slice1)
	fmt.Println(slice1)

}

func updateSlice(slice1 []int) {
	slice1[1] = 11
}

```



### defer函数

defer语义：推迟、延迟

在go中，defer关键字来延迟一个语句的执行

- 多个defer，当函数执行到最后时，这些defer语句会按照逆序执行
- 如果有很多个使用了defer，采用的是栈形式 后进先出模式

```go
package main

import "fmt"

func main(){
    fmt.Println(1)
    defer fmt.Println(2)
    fmt.Println(3)
    fmt.Println(4)
    defer fmt.Println(5)
    fmt.Println(6)
}

// 输出结果是:
// 1
// 3
// 4
// 6
// 5
// 2
```



```go
package main

import "fmt"

func main(){
    a := 10
    fmt.Println(a)
    defer f(a) // 此时参数就已经传递进去了，只是在该函数最后执行该语句。所以调用f函数，a的值是10
    a++
    fmt.Println(a)
}

func f(s int){
    fmt.Println(a)
}

// 输出结果为
// 10
// 11
// 10
```



### 函数本质也是一种类型

```go
package main

import "fmt"

func main() {
	fmt.Printf("%T\n", f1) // func(int, int)
	fmt.Printf("%T\n", f2) // func(int, int) int
	fmt.Printf("%T\n", f3) // func()

	var fun func(int, int) // 定义函数变量
	fun = f1               // 赋值 地址一样
	fun(1, 2)              // 调用
}

func f1(a, b int) {
	fmt.Println(a, b)
}

func f2(a, b int) int {
	return 0
}

func f3() {
}

```



### 匿名函数

```go
package main

import "fmt"

func main() {
	// 第一种
	f1 := func() {
		fmt.Println("这是f1函数")
	}
	f1() // 执行f1函数

	// 第二种
	func() {
		fmt.Println("这是f2函数")
	}()

	// 第三种
	func(a, b int) {
		fmt.Println(a, b)
		fmt.Println("这是f3函数")
	}(1, 2)

}
```



### 回调函数

```go
package main

import "fmt"

func main() {
	operate(1, 2, add)

	operate(8, 4, sub)

	operate(8, 4, func(num1 int, num2 int) int {
		var res int
		if num2 == 0 {
			fmt.Println("被除数不能为0")
		} else {
			res = num1 / num2
		}
		return res
	})
}

func operate(a, b int, fun func(int, int) int) {
	res := fun(a, b)
	fmt.Println(res)
}

func add(a, b int) int {
	return a + b
}

func sub(a, b int) int {
	return a - b
}

```



### 闭包结构

```go
package main

import "fmt"

/*
一个外层函数中有一个内层函数，内层函数操作外层函数的变量
并且该外层函数返回值就是这个内层函数
这个外层函数和内层函数的局部变量，统称为闭包结构

闭包结构中的外层函数的局部变量并不会随着外层函数的结束而销毁，因为内层函数还在继续使用
*/
func main() {
	f1 := increment()
	fmt.Println(f1()) // 1
	fmt.Println(f1()) // 2
	fmt.Println(f1()) // 3

	f2 := increment()
	fmt.Println(f2()) // 1
	fmt.Println(f1()) // 4
	fmt.Println(f2()) // 2
}

func increment() func() int {
	i := 0
	fun := func() int {
		i++
		return i
	}
	return fun
}

```



## :four_leaf_clover: init函数

golang有一个特殊的函数`init`函数，先于`main`函数执行，实现包级别的一些**初始化**操作。

### 主要特点

- init函数先于main函数**自动执行**，不能被其他函数调用；
- init函数没有输入参数、返回值；
- 每个包可以有多个init函数；
- **包的每个源文件也可以有多个init函数**，这点比较特殊；
- 同一个包的init执行顺序，golang没有明确定义，编程时要注意程序不要依赖这个执行顺序。
- 不同包的init函数按照包导入的依赖关系决定执行顺序。



```go
package main

import "fmt"

var i int = initVar()

func initVar() int{
    fmt.Println("initVar...")
    return 100
}

func init(){
    fmt.Println("init...")
}

func main(){
    fmt.Println("main...")
}

// 输出结果：
intiVar...
init...
main...
```



## :four_leaf_clover: 指针

Go语言中的函数传参都是值拷贝，当我们想要修改某个变量的时候，我们可以创建一个指向该变量地址的指针变量。传递数据使用指针，而无须拷贝数据。

**类型指针不能进行偏移和运算**。

Go语言中的指针操作非常简单，只需要记住两个符号：`&`（取地址）和`*`（根据地址取值）。



### 指针地址和指针类型

每个变量在运行时都拥有一个地址，这个地址代表变量在内存中的位置。Go语言中使用`&`字符放在变量前面对变量进行**取地址**操作。 Go语言中的值类型`（int、float、bool、string、array、struct）`都有对应的指针类型，如：`*int、\*int64、\*string`等。



### 语法

```go
var var_name *var_type
```

`var_name`：指针类型

`var_type`：指针变量

`*`：用于指定变量是作为一个指针



### 数组指针

**定义语法**

```go
var ptr [MAX]*int; 表示数组里面的元素的类型是指针类型
```

**实例演示**

```go
package main

import "fmt"

const MAX int = 3

func main() {
	a := []int{ 1, 3, 5}
	var i int
	var ptr [MAX]*int;
	fmt.Println(ptr)   //这个打印出来是[<nil> <nil> <nil>]
    for  i = 0; i < MAX; i++ {
        ptr[i] = &a[i]
    }
    for i = 0; i< MAX; i++ {
        fmt.Print("a[%d] = %d\n", i, *ptr[i])
    }
}
```

运行结果

```
[<nil> <nil> <nil>]
a[0] = 1
a[1] = 3
a[2] = 5
```



## :four_leaf_clover:结构体

go语言没有面向对象的概念了，但是可以使用结构体来实现，面向对象编程的一些特性，例如：继承、组合等特性。

**值传递，用指针则是引用传递**



### 定义

上一节我们介绍了类型定义，结构体的定义和类型定义类似，只不过多了一个`struct`关键字，语法结构如下：

```go
type struct_variable_type struct {
   member definition;
   member definition;
   ...
   member definition;
}
```

`type`：结构体定义关键字

`struct_variable_type`：结构体类型名称

`struct`：关键字

`member definition`：成员定义

**实例**

```go
// 方式一
type Person struct{
    id int
    name string
    age int
    email string
}

// 方式二
type Person struct{
    id,age int
    name,email string
}
```



### 声明结构体变量

```go
// 方式一
var p1 Person
p1.id = 1
p1.name = "tom"

// 方式二
p2 := Person()
```



### 匿名结构体

```go
func main(){
    // 匿名结构体
    var per struct{
        id,age int
        name,email string
    }
    
    per.id = 1
    per.age = 20
    per.name = "Vince"
}
```



### 初始化

```go
type Person struct{
    id,age int
    name,email string
}

// 方式一：指定初始化
tom := Person{
    id: 1,
    age: 22,
    name: "Vince",
    email: "tom@gmail.com"
}

// 方式二：点方式      kite := Person()
var kite Person
kite.id = 1 
kite.age = 20
kite.name = "Vince"

// 方式三：根据结构体成员顺序初始化
tom := Person{
    1,
    22,
    "Vince",
    "tom@gmail.com"
}

// 方式四
var vince = new(Person)
vince.id = 2
vince.name= "Vince"
```



## :four_leaf_clover:方法

go语言没有面向对象的特性，也没有类对象的概念。但是，可以使用结构体来模拟这些特性，我们都知道面向对象里面有类方法等概念。我们也可以声明一些方法，属于某个结构体。



### 语法

Go中的方法，是一种**特殊的函数**，定义于struct之上(与struct关联、绑定)，被称为struct的接受者(receiver)。

通俗的讲，方法就是有接收者的函数。

语法格式如下：

```go
type mytype struct{}

func (recv mytype) my_method(para) return_type {}
func (recv *mytype) my_method(para) return_type {}
```

`mytype`：定义一个结构体

`recv`：接受该方法的结构体

`my_method`：方法名

`para`：参数

`return_type`：返回值类型

一个方法和一个函数非常类似，多了一个接受者类型

**实例**

```go
type Person struct{
    name string
}

// 该方法属于Person结构体的
func (per Person) eat(){
    fmt.Println("eat....")
}

func main(){
    per := Person{
        name:"Vince"
    }
    per.eat()
}
```



## :four_leaf_clover:接口

接口像是一个公司里面的领导，他会定义一些通用规范，只设计规范，而不实现规范。

go语言的接口，是一种新的**类型定义**，它把所有的**具有共性的方法**定义在一起，任何其他类型只要实现了这些方法就是实现了这个接口。

语法格式和方法非常类似。



### 实例

每个结构体都需要实现接口中的所有方法，否则编译不通过

```go
package main

import "fmt"

type USBer interface {
	read()
	write()
}

// Computer
type Computer struct {
	name string
}

func (c Computer) read() {
	fmt.Println("Computer read...  " + c.name)
}

func (c Computer) write() {
	fmt.Println("Computer write...  " + c.name)
}

func (c Computer) computerOwn() {
	fmt.Println("Computer own...  " + c.name)
}

// Mobile
type Mobile struct {
	name string
}

func (m Mobile) read() {
	fmt.Println("Mobile read...  " + m.name)
}

func (m Mobile) write() {
	fmt.Println("Mobile write...  " + m.name)
}

func main() {
	c := Computer{
		name: "Legend",
	}
	c.read()
	c.write()
	c.computerOwn()

	m := Mobile{
		name: "Huawei",
	}
	m.read()
	m.write()

	// 因为Computer实现了接口USBer，可以将其赋值给USBer，但是只能调用接口中的函数，不能调用Computer结构体特有的函数
	var usber USBer
	usber = c
	usber.read()
	usber.write()
}

```



### 接口嵌套

```go
package main

import "fmt"

// FlyFish接口嵌套了Flyer和Swimmer接口
type FlyFish interface {
	Flyer
	Swimmer
}

type Flyer interface {
	fly()
}

type Swimmer interface {
	swimming()
}

type Fish struct {
}

func (fish Fish) fly() {
	fmt.Println("fly....")
}

func (fish Fish) swimming() {
	fmt.Println("swimming...")
}

func main() {
	ff := Fish{}
	ff.fly()
	ff.swimming()
}

```



## :four_leaf_clover:模拟OOP思想

golang没有面向对象的概念，也没有封装的概念，但是可以通过结构体`struct`和函数**绑定**来实现OOP的属性和方法等特性。接收者 receiver **方法**。

**例如**，想要定义一个Person类，有name和age属性，有eat/sleep/work方法。

```go
package main

import "fmt"

type Person struct{
    name string
    age int
}

func (per Person) eat(){
    fmt.Println(per.name + " eat...")
}

func (per Person) sleep(){
    fmt.Println(per.name + " sleep...")
}

func (per Person) work(){
    fmt.Println(per.name + " work...")
}


func main(){
    per:=Person{
        name: "Vince",
        age: 22,
    }
    
    per.eat()
    per.sleep()
    per.work()
}
```



## :four_leaf_clover:模拟继承

golang本质上没有oop的概念，也没有继承的概念，但是可以通过**结构体嵌套**实现这个特性。

```go
package main

import "fmt"

type Animal struct {
	name string
	age  int
}

func (animal Animal) eat() {
	fmt.Println("eat...")
}

func (animal Animal) sleep() {
	fmt.Println("sleep...")
}

// Dog 继承 Animal
type Dog struct {
	Animal
}

// Cat 继承 Animal
type Cat struct {
	Animal
}

func main() {
	c := Cat{
		Animal{
			name: "cat",
			age:  2,
		},
	}

	d := Dog{
		Animal{
			name: "dog",
			age:  3,
		},
	}

	c.eat()
	c.sleep()

	d.eat()
	d.sleep()
}

```



:four_leaf_clover:模拟构造函数

```go
package main

import "fmt"

type Person struct {
	name string
	age  int
}

func NewPerson(name string, age int) (*Person, error) {
	if name == "" {
		return nil, fmt.Errorf("name 不能为空")
	}
	if age < 0 {
		return nil, fmt.Errorf("age 不能小于零")
	}
	return &Person{name: name, age: age}, nil
}

func main() {
	per, err := NewPerson("Vince", 24)
	if err == nil {
		fmt.Println(*per)
	}

}
```





## :four_leaf_clover:泛型

**any:**表示go里面所有的内置基本类型，等价于`interface{}`

**comparable:**表示go里面所有内置的可比较类型`int、unit、float、bool、struct、指针`等一切可以比较的类型



```go
package main

import "fmt"

func main() {
	intArr := []int{1, 3}
	stringArr := []string{"Vince", "Chang"}
	printArr(intArr)
	printAnyArr(stringArr)
}

func printArr[T string | int](arr []T) { // 指定string、int
	for _, val := range arr {
		fmt.Println(val)
	}
}

func printAnyArr[T any](arr []T) { // any 任何类型
	for _, val := range arr {
		fmt.Println(val)
	}
}

func printComparableArr[T comparable](arr []T) { // comparable 任何可比较类型
	for _, val := range arr {
		fmt.Println(val)
	}
}

```



#### 泛型类型

```go
package main

import "fmt"

func main() {
	// 定义泛型类型
	type Slice[T int | float64 | float32] []T

	// 利用泛型类型定义变量
	var a Slice[int] = []int{1, 3}
	var b Slice[float64] = []float64{1, 3}
	var c Slice[float32] = []float32{1, 3}

	// 打印
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
    
    // map类型的泛型定义
	type VinceMap[KEY int | string, VALUE int | float64] map[KEY]VALUE

	var vMap VinceMap[string, float64] = map[string]float64{
		"go":   9.9,
		"java": 9.0,
	}
	fmt.Println(vMap)
}
```



#### 泛型函数（泛型添加方法）

```go
package main

import "fmt"

type VinceSlice[T int | float64] []T

// 给泛型添加方法	返回T
func (s VinceSlice[T]) sum() T {
	var sum T
	for _, val := range s {
		sum += val
	}
	return sum
}

func main() {
	var vsInt VinceSlice[int] = []int{1, 3, 4, 5}
	fmt.Println(vsInt.sum())
	var vsFloat VinceSlice[float64] = []float64{1.1, 3, 4, 5}
	fmt.Println(vsFloat.sum())
}
```



#### 自定义泛型约束（类型起别名）

```go
package main

import "fmt"

// 给int8起别名
type int8Vince int8

type VinceInt interface {
	int | ~int8 | int16 | int32 | int64
}

func cmp[T VinceInt](a, b T) T {
	if a > b {
		return a
	}
	return b
}

func main() {
	res := cmp[int](11, 30) // 显示声明
	// res := cmp(11, 30)
	fmt.Println(res)
	
	// 如果在自定以泛型VinceInt中int8前没有加上~符号，就不会识别int8的衍生类型，就无法识别int8Vince
	cmp[int8Vince](11,01)
}

```





## :four_leaf_clover:go mod包管理

[关于go modules的使用和配置](https://www.cnblogs.com/xiaobaiskill/p/11819071.html#:~:text=goland%20%E5%8D%87%E7%BA%A7%E5%88%B0%E6%9C%80%E6%96%B0%E7%9A%84%2C%E6%97%A7%E7%9A%84goland%20%E7%89%88%E6%9C%AC%E6%97%B6%E4%B8%8D%E6%94%AF%E6%8C%81go%20mod%2C%20%E5%9C%A8preferences%20-%3E%20go%20-%3E,Modules%20%28vgo%29%20%E7%BB%99Enable%20Go%20Modules%20%28vgo%29%20Integration%20%E6%89%93%E5%8B%BE%E5%8B%BE%E5%B0%B1%E8%A1%8C)



### go mod 的一些命令

| 命令     | 说明                                                         |
| -------- | ------------------------------------------------------------ |
| download | download modules to local cache(下载依赖包 重要)             |
| edit     | edit go.mod from tools or scripts（编辑go.mod）              |
| graph    | print module requirement graph (打印模块依赖图)              |
| init     | initialize new module in current directory（在当前目录初始化mod 重要） |
| tidy     | add missing and remove unused modules(拉取缺少的模块，移除不用的模块 **重要**) |
| vendor   | make vendored copy of dependencies(将依赖复制到vendor下)     |
| verify   | verify dependencies have expected content (验证依赖是否正确） |
| why      | explain why packages or modules are needed(解释为什么需要依赖) |



> go mod引入包标红解决办法

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForGo/go%E5%BC%95%E5%85%A5%E5%8C%85%E6%A0%87%E7%BA%A2.png)

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForGo/go%E5%BC%95%E5%85%A5%E5%8C%85%E6%A0%87%E7%BA%A2%E8%A7%A3%E5%86%B3%E5%8A%9E%E6%B3%95.png)
