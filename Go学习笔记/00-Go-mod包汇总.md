# Go-mod包汇总



Go-mod详解（本地模块依赖）：https://www.cnblogs.com/gtea/p/15608898.html



### GO mod 命令

go module 使用前置条件: GO111MODULE 设置为 on

- 在当前文件夹下初始化一个新的 module，创建 go.mod 文件； `go mod init name`
- 拉取缺少的模块，移除不用的模块 ： `go mod tidy`
- 将依赖复制到 vendor 下 : `go mod vendor`
- 下载依赖 : `go mod download`
- 检验依赖: `go mod verify`
- 显示模块依赖图: `go mod graph`
- 解释为什么需要依赖: `go mod why`
- 编辑 go.mod 文件: `go eidt`
- 查看命令列表: `go mod`
- 清空缓存：`go clean --modcache`
- 查看命令帮助文档: `go help mod <command>`



### GOPROXY

1. [https://goproxy.cn](https://links.jianshu.com/go?to=https%3A%2F%2Fgoproxy.cn)
2. [https://goproxy.io](https://links.jianshu.com/go?to=https%3A%2F%2Fgoproxy.io)



```go
//go 1.13 及以上
go env -w GOPROXY=https://goproxy.cn,direct
```





```bash
# mysql
go get -u github.com/go-sql-driver/mysql

# mongodb
go get go.mongodb.org/mongo-driver/mongo

# gorm
go get -u gorm.io/gorm
go get -u gorm.io/driver/mysql

# xorm
go get xorm.io/xorm

# gin
go get -u github.com/gin-gonic/gin

# grpc
go get -u google.golang.org/grpc
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest

# uuid
go get -u -v github.com/google/uuid

# viper 读取运用yaml
go get github.com/spf13/viper

# mapstructure 为了使yaml中带下划线或中划线的key能够成功映射
go get github.com/mitchellh/mapstructure

# swagger
go install github.com/swaggo/swag/cmd/swag@latest
# 安装之后找到对用的文件目录配置环境变量就可以使用swag命令了
# 引入swagger依赖
go get -u github.com/swaggo/gin-swagger
go get -u github.com/swaggo/swag
# 生成api文档，根据注释生成docs
swag init

# fresh 热更新配合gin使用
go get github.com/pilu/fresh
```



**Redis：**

If you are using **Redis 6**, install go-redis/**v8**:

```
go get github.com/go-redis/redis/v8
```

If you are using **Redis 7**, install go-redis/**v9**:

```
go get github.com/go-redis/redis/v9
```
