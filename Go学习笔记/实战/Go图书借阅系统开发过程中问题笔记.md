



# 包不能循环引用。

主要问题是文件放错包位置，设计不合理。





# 字段也是对得上的，为何不映射不到？

翻了翻`viper`解析的部分源码，发现有些特殊情况，需要自定义`kv`的分隔符，有些时候，需要添加一些特殊标记来告诉解析器，这是一个合法的标识符。

将`Mysql`的结构体添加`mapstructure`，指定对应的字段就行了：

```go
# go get -u github.com/mitchellh/mapstructure v1.5.0 // indirect

type Mysql struct {
	RdsHost string `yaml:"rds_host" mapstructure:"rds_host"`
	RdsPort int    `yaml:"rds_port" mapstructure:"rds_port"`
}
```





# 为什么使用zap？

https://www.liwenzhou.com/posts/Go/zap/

### Go Logger的优势和劣势

#### 优势

它最大的优点是使用非常简单。我们可以设置任何`io.Writer`作为日志记录输出并向其发送要写入的日志。

#### 劣势

- 仅限基本的日志级别

  - 只有一个`Print`选项。不支持`INFO`/`DEBUG`等多个级别。

- 对于错误日志，它有

  `Fatal`和`Panic`

  - Fatal日志通过调用`os.Exit(1)`来结束程序
  - Panic日志在写入日志消息之后抛出一个panic
  - 但是它缺少一个ERROR日志级别，这个级别可以在不抛出panic或退出程序的情况下记录错误

- 缺乏日志格式化的能力——例如记录调用者的函数名和行号，格式化日期和时间格式。等等。

- 不提供日志切割的能力。





# 单元测试

参考：https://www.jianshu.com/p/1adc69468b6f

测试函数名称格式是：Test[^a-z]，即以Test开头，跟上非小写字母开头的字符串。每个测试函数都接受一个*testing.T类型参数，用于输出信息或中断测试。

测试方法有：

- Fail: 标记失败，但继续执行当前测试函数
- FailNow: 失败，立即终止当前测试函数执行
- Log: 输出错误信息
- Error: Fail + Log
- Fatal: FailNow + Log
- Skip: 跳过当前函数，通常用于未完成的测试用例



测试代码：

```go
// filename: add_test.go

package test
import (
    "testing"
)

func Add(a, b int) int {
    return a + b
}

func TestAdd1(t *testing.T) {
    if Add(2, 3) != 5 {
         t.Error("result is wrong!")
    } else {
         t.Log("result is right")
    }
}

func TestAdd2(t *testing.T) {
    if Add(2, 3) != 6 {
         t.Fatal("result is wrong!")
    } else {
         t.Log("result is right")
    }
}
```

运行以下命令，自动搜集所有的测试文件（*_test.go），提取全部测试函数。





# JSON接收时间字符串无法正确转换Time问题

参考博客 ：golang 时间出现 -62135596800 问题（解决方案）golang 时间出现 -62135596800 问题（解决方案）
Golang json转换时间格式问题Golang json转换时间格式问题
go 实际开发中 time.Time类型 提供是字符串, 而很多场景中需要对请求的接口再次处理，将go json化后的字符串转成time.Time类型
go程序 将时间转换成json 时 会默认把时间转换为`RFC3339 `格式

> 2018-01-14T21:45:54+08:00

先来看看time包中对格式的常量定义

```go
const (
    ANSIC       = "Mon Jan _2 15:04:05 2006"
    UnixDate    = "Mon Jan _2 15:04:05 MST 2006"
    RubyDate    = "Mon Jan 02 15:04:05 -0700 2006"
    RFC822      = "02 Jan 06 15:04 MST"
    RFC822Z     = "02 Jan 06 15:04 -0700" // RFC822 with numeric zone
    RFC850      = "Monday, 02-Jan-06 15:04:05 MST"
    RFC1123     = "Mon, 02 Jan 2006 15:04:05 MST"
    RFC1123Z    = "Mon, 02 Jan 2006 15:04:05 -0700" // RFC1123 with numeric zone
    RFC3339     = "2006-01-02T15:04:05Z07:00"
    RFC3339Nano = "2006-01-02T15:04:05.999999999Z07:00"
    Kitchen     = "3:04PM"
    // Handy time stamps.
    Stamp      = "Jan _2 15:04:05"
    StampMilli = "Jan _2 15:04:05.000"
    StampMicro = "Jan _2 15:04:05.000000"
    StampNano  = "Jan _2 15:04:05.000000000"
)
```

切入正题
怎么样把这个时间

`2018-01-14T21:45:54+08:00`

转换为

`2018-01-14 21:45:54`

其实琢磨了一下方法还是很简单

`str:="2018-01-14T21:45:54+08:00"`
//先将时间转换为字符串

`tt,_:=time.Parse("2006-01-02T15:04:05Z07:00",str)`

//格式化时间
`fmt.Println(tt.Format("2006-01-02 15:04:05"))`

就可以得到自己想要的时间了

time.Parse()的layout参数 就是上面常量定义的 RFC3339
如果其他格式 也只要复制对应的layout 就可以了

tt.Format（）

是将时间按照 自定义的方式 进行个格式化
这里的 2006-01-02 15:04:05 一定不能变





# Golang中使用 jwt生成token报错：key is of invalid type



```go
tokenString, err = token.SignedString("skey")
```

编译完，在`SignedString`生成token这一步报错 key is of invalid type

```console
time="2020-09-23 15:27:59" level=warning msg="key is of invalid type"
```

翻了下源码，参数 key 为`interface{}`类型，据我了解Golang的空接口应该可以接收任意类型的变量参数，但是问题可能就出在`key.([]byte)`这里。

```go
func (m *SigningMethodHMAC) Sign(signingString string, key interface{}) (string, error) {
    if keyBytes, ok := key.([]byte); ok {
        if !m.Hash.Available() {
            return "", ErrHashUnavailable
        }

        hasher := hmac.New(m.Hash.New, keyBytes)
        hasher.Write([]byte(signingString))

        return EncodeSegment(hasher.Sum(nil)), nil
    }

    return "", ErrInvalidKeyType
}
```

测试发现，参数key是`string`类型报错 key is of invalid type，换成`[]byte`类型编译通过。

```go
tokenString, err = token.SignedString([]byte("skey"))
```

不太明白，在网上翻了半天也没有想要答案，最后在官网文档上找到，链接在下边。

参考文档：[https://golang.org/doc/effective_go.html#interface_conversions](https://links.jianshu.com/go?to=https%3A%2F%2Fgolang.org%2Fdoc%2Feffective_go.html%23interface_conversions)





# Swagger无法生成

报错：Skipping 'system.Book', recursion detected.

解决：https://github.com/flipped-aurora/gin-vue-admin/issues/256

swag 不能高于1.6.7

> go install github.com/swaggo/swag/cmd/swag@v1.6.7
