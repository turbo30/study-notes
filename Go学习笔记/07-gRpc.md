# GRPC

远程过程调用

官方文档：http://doc.oschina.net/grpc

李文周文档：https://www.liwenzhou.com/posts/Go/gRPC/

学习文档：https://github.com/langwan/chihuo



## Protocol Buffers

### 安装

安装：https://github.com/protocolbuffers/protobuf/releases

配置环境变量

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForGo/protocolBuffers%E7%8E%AF%E5%A2%83%E5%8F%98%E9%87%8F%E8%AE%BE%E7%BD%AE.png)



项目引入grpc

```bash
go get -u google.golang.org/grpc
```



上面安装的时protocol编译器。可生成各种不同语言的代码。因此，除了这个编译器，需要配合各个语言的代码生成工具。对于go语言来说，称为`protoc-gen-go`。需要注意`github.com/golang/protobuf/protoc-gen-go`与`google.golang.org/protobuf/cmd/protoc-gen-go`是不同的。区别在于前者是旧版，后者是google接管后的新版，他们之间的API是不同的，也就是说用于生成的命令，以及生成的文件都是不一样的。因为目前的`gRPC-go`源码中的example用的是后者的生成方式，为了与时俱进，采用最新方式。需要两个库：

```bash
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
```

因为这些文件在安装`grpc`的时候，已经下载下来了，因此使用`install`命令就可以了，而不需要`get`命令

然后可以查看$GOWORKS/bin路径，应该有install的两个exe执行文件



## proto文件编写

编写名为`vince.proto`文件

```protobuf
// 使用的是proto3的语法
syntax = "proto3";

// 最后生成go文件是处在哪个目录哪个包中，.代表当前目录生成，service代表生成的go文件的包名是service
option go_package = ".;service";

// 定义一个服务，在这个服务中需要有一个方法，这个方法可以接收客户端的参数，再返回服务端的响应
// 定义了一个service，称为SayHello，这个服务中有一个rpc方法，名为SayHello
// 会发送一个HelloRequest，返回一个HelloResponse
service SayHello{
  rpc SayHello(HelloRequest) returns (HelloResponse){}
}

// message关键字
// 后面的“赋值”是定义这个变量再message中的位置，赋值的是标识号，代表参数的位置
// requestName这个变量在message中的第一个位置
message HelloRequest{
  string requestName = 1;
  int64 age = 2;
  repeated string name =3; // 生成的代码为string类型的切片  repeated就是声明为切片

  // 嵌套
  message Person{
    string name = 1;
  }
  Person info = 4;
}

message HelloResponse{
  string responseMsg = 1;
}
```



在编写上面的内容之后，在`.proto`文件同级目录下执行：

```protobuf
protoc --go_out=. vince.proto
protoc --go-grpc_out=. vince.protop
```
可以在生成go文件中自定义实现逻辑

> 如果命令无法执行，可以以管理员运行Goland软件



## 服务端代码编写

```go
package main

import (
	"context"
	"google.golang.org/grpc"
	pb "grpc_demo/vince-server/proto"
	"log"
	"net"
)

// 继承serve
type server struct {
	pb.UnimplementedSayHelloServer // 这是根据proto自动生成的方法
}

// 继承重写SayHello方法，自行实现
func (server) SayHello(c context.Context, req *pb.HelloRequest) (*pb.HelloResponse, error) {
	return &pb.HelloResponse{ResponseMsg: "hello " + req.RequestName}, nil
}

func main() {
	// 开启监听端口
	listen, _ := net.Listen("tcp", ":9090")

	// 创建一个服务，将方法和服务注册上去
	grpcServer := grpc.NewServer() // new一个服务
	pb.RegisterSayHelloServer(grpcServer, &server{}) // 将syahelloserver服务注册到grpc中

	// 启动grpc服务
	err := grpcServer.Serve(listen)
	if err != nil {
		log.Panic("GRPC服务启动失败 ", err)
	}
}
```



## 客户端代码编写

```go
package main

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	pb "grpc_demo/vince-client/proto"
	"log"
)

func main() {
	// 连接到server，这里没有使用传输加密方式
	conn, err := grpc.Dial("127.0.0.1:9090", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Panic(err)
	}
	defer conn.Close()

	// 建立连接
	client := pb.NewSayHelloClient(conn)

	// 调用grpc中注册的方法
	response, _ := client.SayHello(context.Background(), &pb.HelloRequest{RequestName: "Vince"})
	log.Println("调用服务端返回的信息：", response.GetResponseMsg())
}
```



## 认证-安全传输

- SSL/TLS认证方式（采用HTTP2协议）
- 基于Token的认证方式（基于安全连接）
- 不采用任何措施的连接，这是不安全的连接（默认采用http1）
- 自定义的身份认证

客户端和服务端之间调用，可以通过加入证书的方式，实现调用的安全性

TLS(安全传输层)，TLS是建立在传输层TCP协议之上的协议，服务于应用层，它的前身是SSL（安全套接字层），它实现了将应用层的报文进行加密后再交由TCP进行传输的功能。

TLS协议主要解决如下三个网络安全问题：

1、保密，通过加密encryption实现，所有信息都加密传输，第三方无法嗅探

2、完整性，通过MAC校验机制，一旦被篡改，通信双方会立刻发现

3、认证，双方认证，双方都可以配备证书，防止身份被冒充



`key`：服务器上的私钥文件，用于对发送给客户端数据的加密，以及对从客户端接收到的数据的解密

`crt`：由证书派发机构签名后的证书，或者是开发者自签名的证书，包含证书持有人信息，持有人的公钥，以及签署者的签名等信息

`csr`：证书签名请求文件，用于提交给证书派发机构对证书签名

`pem`：基于Base64编码的证书格式，扩展名包括PEM、CRT和CER



## SSL/TLS认证方式



### 实现方式一（pem与key）

关系图：

![image-20230204175858555](https://gitee.com/turbo30/study_pic/raw/master/pictureForGo/image-20230204175858555.png)



首先通过openssl生成证书和私钥

1、官网下载：https://www.openssl.org/source

- 其他人做的便捷版安装包：http://slproweb.com/products/Win32OpenSSL.html

2、配置环境变量

3、命令行测试openssl

4、

> 自生成CA证书

```bash
# 生成私钥 ca.key
openssl genrsa -out ca.key 2048

# 生成证书 全部回车即可，可以不填 ca.crt
openssl req -new -x509 -key ca.key -out ca.crt -days 36500

# 生成 ca.csr
openssl req -new -key ca.key -out ca.csr

```



> 自定义修改cfg配置

```bash
# 更改openssl.cnf (Linux 是openssl.cfg)
# 1复制一份openssl/bin目录下openssl.cnf文件到项目中
# 2找到[CA_default]，打开 copy_extensions = copy
# 3找到[req]，打开 req_extensions = v3_req
# 4找到[v3_req]，添加subjectAltName = @alt_names
# 5添加新的标签 [ alt_names ]，和标签字段
[ alt_names ]
DNS.1 = *.vince.com  # 这个就是允许符合这样的域名才能来访问
DNS.2 = ....
```



> 生成私钥

```bash
# 生成证书私钥 test.key
openssl genpkey -algorithm RSA -out test.key

# 通过私钥test.key生成证书请求文件 test.csr（注意cfg和cnf）
openssl req -new -nodes -key test.key -out test.csr -days 3650 -subj "/C=cn/OU=myorg/O=mycomp/CN=myname" -config ./openssl.cfg -extension v3_req
# test.csr是上面生成证书请求文件。ca.crt/server.key是CA证书文件和key，用来对test.csr进行签名认证。这两个文件再第一部分生成。

# 生成SAN证书 pem ：通过ca.crt、ca.key参与加上test.csr生成证书test.pem
openssl x509 -req -days 365 -in test.csr -out test.pem -CA ca.crt -CAkey ca.key -CAcreateserial -extfile ./openssl.cfg -extensions v3_req
```



> 服务端

```go
package main

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	pb "grpc_demo/vince-server/proto"
	"log"
	"net"
)

// 继承serve
type server struct {
	pb.UnimplementedSayHelloServer
}

// 继承重写SayHello方法，自行实现
func (server) SayHello(c context.Context, req *pb.HelloRequest) (*pb.HelloResponse, error) {
	log.Println("客户端调用了该方法...")
	return &pb.HelloResponse{ResponseMsg: "hello " + req.RequestName}, nil
}

func main() {
    
     // -----------------------------------------------------------------

	// 1 添加 证书，私钥
	credential, _ := credentials.NewServerTLSFromFile("D:\\Workspace\\GoWorks\\src\\grpc_demo\\vince-server\\key\\test.pem",
		"D:\\Workspace\\GoWorks\\src\\grpc_demo\\vince-server\\key\\test.key")
    

	// 创建一个服务，将方法和服务注册上去
	grpcServer := grpc.NewServer(grpc.Creds(credential)) // 2 加入认证 !!!
    
    // -----------------------------------------------------------------
    
    // 开启监听端口
	listen, _ := net.Listen("tcp", ":9090")
    
	pb.RegisterSayHelloServer(grpcServer, &server{})

	// 启动grpc服务
	err := grpcServer.Serve(listen)
	if err != nil {
		log.Panic("GRPC服务启动失败 ", err)
	}
}
```



> 客户端

```go
package main

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	pb "grpc_demo/vince-client/proto"
	"log"
)

func main() {
    
     // -----------------------------------------------------------------

	// 携带证书。第一个参数：证书；第二个参数：在openssl.cfg配置的alt_names域名
	cred, _ := credentials.NewClientTLSFromFile("D:\\Workspace\\GoWorks\\src\\grpc_demo\\vince-server\\key\\test.pem", "*.vince.com")

	// 连接到server，这里使用SSL传输加密方式
	conn, err := grpc.Dial("127.0.0.1:9090", grpc.WithTransportCredentials(cred))
	if err != nil {
		log.Panic(err)
	}
	defer conn.Close()
    
    // -----------------------------------------------------------------

	// 建立连接
	client := pb.NewSayHelloClient(conn)

	// 调用grpc中注册的方法
	response, _ := client.SayHello(context.Background(), &pb.HelloRequest{RequestName: "Vince"})
	log.Println("调用服务端返回的信息：", response.GetResponseMsg())
}
```



### 实现方式二（crt与key）



学习视频：https://www.bilibili.com/video/BV1VV4y1T7j8/?spm_id_from=333.788&vd_source=f603c8704f45c1725a1123d19f6f0069

代码：https://github.com/langwan/chihuo/tree/main/go%E8%AF%AD%E8%A8%80/grpc/grpc%20tls%E5%8F%8C%E5%90%91%E8%AE%A4%E8%AF%81



关系图：

![grpc-ssl/tls认证关系图](https://gitee.com/turbo30/study_pic/raw/master/pictureForGo/image-20230204171512594.png)

> 获取`ca.key`之后生成`ca.crt`证书，生成`client.key`，通过`client.key`生成证书请求文件`client.csr`，再将`ca.crt`证书与`client.csr`签一次名生成client的`client.crt`证书
>
> 任何服务都要通过ca.crt与该服务的.csr文件进行签名生成本服务器的证书



> 服务端

```go
package main

import (
	"app/rpc"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"google.golang.org/grpc/credentials"
	"io/ioutil"
	"log"
	"net"

	"google.golang.org/grpc"
)

func main() {

	certificate, err := tls.LoadX509KeyPair("../tls/server.crt", "../tls/server.key")
	if err != nil {
		panic(err)
	}

	certPool := x509.NewCertPool()
	ca, err := ioutil.ReadFile("../tls/ca.crt")
	if err != nil {
		panic(err)

	}
	if ok := certPool.AppendCertsFromPEM(ca); !ok {
		panic("AppendCertsFromPEM failed")
	}

	creds := credentials.NewTLS(&tls.Config{
		Certificates: []tls.Certificate{certificate},
		ClientAuth:   tls.RequireAndVerifyClientCert, // NOTE: this is optional!
		ClientCAs:    certPool,
	})

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 8000))

	if err != nil {
		log.Fatalf("启动grpc server失败")
		return
	}

	grpcServer := grpc.NewServer(grpc.Creds(creds))

	rpc.RegisterServerServer(grpcServer, Server{})

	log.Println("service start")
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("启动grpc server失败")
	}
}
```



> 客户端

```go
package main

import (
	"client/rpc"
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"io/ioutil"
	"log"
)

func main() {

	certificate, err := tls.LoadX509KeyPair("../tls/client.crt", "../tls/client.key")
	if err != nil {
		panic(err)
		return
	}

	certPool := x509.NewCertPool()
	ca, err := ioutil.ReadFile("../tls/ca.crt")
	if err != nil {
		panic(err)
		return
	}
	if ok := certPool.AppendCertsFromPEM(ca); !ok {
		return
	}

	creds := credentials.NewTLS(&tls.Config{
		Certificates:       []tls.Certificate{certificate},
		RootCAs:            certPool,
		InsecureSkipVerify: true,
	})

	conn, err := grpc.Dial("127.0.0.1:8000",
		grpc.WithTransportCredentials(creds))

	if err != nil {
		fmt.Printf("err: %v", err)
		return
	}

	ServerClient := rpc.NewServerClient(conn)

	helloRespone, err := ServerClient.Hello(context.Background(), &rpc.Empty{})
	if err != nil {
		fmt.Printf("err: %v", err)
		return
	}

	log.Println(helloRespone, err)

	registerResponse, err := ServerClient.Register(context.Background(), &rpc.RegisterRequest{Name: "chihuo", Password: "123456"})
	if err != nil {
		fmt.Printf("err: %v", err)
		return
	}

	log.Println(registerResponse, err)

}
```





脚本参考，根据自身需要修改

```bash
echo '清理并生成目录'

OUT=../../tls
DAYS=365
RSALEN=2048
CN=vince

rm -rf ${OUT}/*
mkdir ${OUT}  >> /dev/null 2>&1

cd ${OUT}


echo '生成CA的私钥'
openssl genrsa -out ca.key ${RSALEN} >> /dev/null 2>&1

echo '生成CA的签名证书'
openssl req -new \
-x509 \
-key ca.key \
-subj "/CN=${CN}" \
-out ca.crt

echo ''
echo '生成server端私钥'
openssl genrsa -out server.key ${RSALEN} >> /dev/null 2>&1

echo '生成server端自签名'
openssl req -new \
-key server.key \
-subj "/CN=${CN}" \
-out server.csr

echo '签发server端证书'
openssl x509 -req  -sha256 \
-in server.csr \
-CA ca.crt -CAkey ca.key -CAcreateserial \
-out server.crt -text >> /dev/null 2>&1

echo '删除server端自签名证书'
rm server.csr

echo ''
echo '生成client私钥'
openssl genrsa -out client.key ${RSALEN}  >> /dev/null 2>&1

echo '生成client自签名'
openssl req -new \
    -subj "/CN=${CN}" \
    -key client.key \
    -out client.csr
echo '签发client证书'
openssl x509 -req -sha256\
 -CA ca.crt -CAkey ca.key -CAcreateserial\
 -days  ${DAYS}\
 -in client.csr\
 -out client.crt\
 -text >> /dev/null 2>&1
echo '删除client端自签名'
rm client.csr

echo ''
echo '删除临时文件'
rm ca.srl

echo ''
echo '完成'
```



## Token认证

> 客户端



需要重写：

```go
type PerRPCCredentials interface {
	// GetRequestMetadata gets the current request metadata, refreshing tokens
	// if required. This should be called by the transport layer on each
	// request, and the data should be populated in headers or other
	// context. If a status code is returned, it will be used as the status for
	// the RPC (restricted to an allowable set of codes as defined by gRFC
	// A54). uri is the URI of the entry point for the request.  When supported
	// by the underlying implementation, ctx can be used for timeout and
	// cancellation. Additionally, RequestInfo data will be available via ctx
	// to this call.  TODO(zhaoq): Define the set of the qualified keys instead
	// of leaving it as an arbitrary string.
	GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error)
	// RequireTransportSecurity indicates whether the credentials requires
	// transport security.
	RequireTransportSecurity() bool
}
```



需要重写`google.golang.org/grpc/credentials/credentials.go#GetRequestMetadata`方法，用于设置自定义token信息。

`RequireTransportSecurity方法` 如果返回false就是不需要使用tls认证方式

```go
package main

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	pb "grpc_demo/vince-client/proto"
	"log"
)

// 自定义token结构体
type ClientTokenAuth struct {
}

// 重新google.golang.org/grpc/credentials/credentials.go里面的方法
func (c ClientTokenAuth) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
    // 自定义的token
	return map[string]string{
		"appId":  "vince",
		"appKey": "11301130",
	}, nil
}

func (c ClientTokenAuth) RequireTransportSecurity() bool {
	return false // 返回false就是不使用SSL
}

func main() {

	// 自定义token认证参数
	var opts []grpc.DialOption
    // 不需要传输加密
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
    // 在rpc调用之前执行ClientTokenAuth结构体绑定的一些方法
	opts = append(opts, grpc.WithPerRPCCredentials(new(ClientTokenAuth)))

	// 连接到server,添加token认证
	conn, err := grpc.Dial("127.0.0.1:9090", opts...)
	if err != nil {
		log.Panic(err)
	}
	defer conn.Close()

	// 建立连接
	client := pb.NewSayHelloClient(conn)

	// 调用grpc中注册的方法
	response, _ := client.SayHello(context.Background(), &pb.HelloRequest{RequestName: "Vince"})
	log.Println("调用服务端返回的信息：", response.GetResponseMsg())
}
```



> 服务端

```go
package main

import (
	"context"
	"errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"
	pb "grpc_demo/vince-server/proto"
	"log"
	"net"
)

// 继承serve
type server struct {
	pb.UnimplementedSayHelloServer
}

// 继承重写 vince_grpc.pb.go#SayHello 方法，自行实现业务逻辑
// tips：正确做法应该是通过拦截器进行token校验，这里直接继承了方法在应该是写业务逻辑的位置进行了token校验
func (s *server) SayHello(c context.Context, req *pb.HelloRequest) (*pb.HelloResponse, error) {
	// 自定义校验token，从元数据中获取信息
	md, ok := metadata.FromIncomingContext(c)
	if !ok {
		return nil, errors.New("没有token")
	}
	var appId string
	var appKey string
	// 元数据获取信息，key都是小写
	if v, ok := md["appid"]; ok {
		appId = v[0]
	}
	if v, ok := md["appkey"]; ok {
		appKey = v[0]
	}
	if appId != "vince" || appKey != "11301130" {
		return nil, errors.New("appId不正确或appKey不正确")
	}

	log.Println("客户端调用了该方法...")
	return &pb.HelloResponse{ResponseMsg: "hello " + req.RequestName}, nil
}

func main() {

	// 开启监听端口
	listen, _ := net.Listen("tcp", ":9090")

	// 创建一个服务，无需认证
	grpcServer := grpc.NewServer(grpc.Creds(insecure.NewCredentials()))
	pb.RegisterSayHelloServer(grpcServer, &server{})

	// 启动grpc服务
	err := grpcServer.Serve(listen)
	if err != nil {
		log.Panic("GRPC服务启动失败 ", err)
	}
}
```





## 拦截器

>  gRpc只支持一个拦截器使用
>
> 如果要使用多个拦截器，则需要第三方中间件`github.com/grpc-ecosystem/go-grpc-middleware`



### 拦截器记录日志，防止服务端抛出panic而使得程序终止

```go
package main

import (
	"app/rpc"
	"context"
	"fmt"
	"github.com/rs/zerolog"
	"os"
	"runtime/debug"

	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"net"
	"time"
)

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 8000))

	if err != nil {
		log.Fatal().Msg("启动grpc server失败")
		return
	}

    // 第一个参数就是设置拦截器
	grpcServer := grpc.NewServer(grpc.UnaryInterceptor(UnaryInterceptor()), grpc.KeepaliveEnforcementPolicy())

	rpc.RegisterServerServer(grpcServer, Server{})

	log.Info().Msg("service start")
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatal().Msg("启动grpc server失败")
	}
}

// 拦截器实现
func UnaryInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		st := time.Now()
		defer func() {
            // recover()捕获异常，记录日志
			if recoverError := recover(); recoverError != nil {
				log.Error().Str("method", info.FullMethod).Interface("recover", recoverError).Bytes("stack", debug.Stack()).Interface("req", req).Interface("resp", resp).Err(err).TimeDiff("runtime", time.Now(), st).Send()
			} else {
				log.Info().Str("method", info.FullMethod).Interface("req", req).Interface("resp", resp).Err(err).TimeDiff("runtime", time.Now(), st).Send()
			}
		}()
		resp, err = handler(ctx, req)
		return resp, err
	}
}
```



### 多个拦截器

```go
package main

import (
	"app/rpc"
	"context"
	"fmt"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/rs/zerolog"
	"os"

	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"net"
)

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 8000))

	if err != nil {
		log.Fatal().Msg("启动grpc server失败")
		return
	}

    // 设置多个拦截器
	grpcServer := grpc.NewServer(grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(LogUnaryServerInterceptor(), AuthUnaryServerInterceptor())))
	rpc.RegisterServerServer(grpcServer, Server{})

	log.Info().Msg("service start")
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatal().Msg("启动grpc server失败")
	}
}

// 日志拦截器
func LogUnaryServerInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		log.Info().Str("method", info.FullMethod).Msg("LogUnaryServerInterceptor")
		resp, err = handler(ctx, req)
		return resp, err
	}
}

// 认证拦截器
func AuthUnaryServerInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		log.Info().Str("method", info.FullMethod).Msg("AuthUnaryServerInterceptor")
		resp, err = handler(ctx, req)
		return resp, err
	}
}
```



## gRpc 使用 ETCD

官方文档：https://pkg.go.dev/go.etcd.io/etcd/client/v3#readme-get-started

`etcd.go` **:** 

```go
package main

import (
	"context"
	"fmt"
	clientv3 "go.etcd.io/etcd/client/v3"
	"go.etcd.io/etcd/client/v3/naming/endpoints"
	"log"
)

const etcdUrl = "http://localhost:2379"
const serviceName = "Vince/server"
const ttl = 10

var etcdClient *clientv3.Client

func etcdRegister(addr string) error {
	log.Printf("etcdRegister %s\b", addr)
    // 开启一个client
	etcdClient, err := clientv3.NewFromURL(etcdUrl)

	if err != nil {
		return err
	}

    // 获得一个管理者
	em, err := endpoints.NewManager(etcdClient, serviceName)
	if err != nil {
		return err
	}

	lease, _ := etcdClient.Grant(context.TODO(), ttl)

    // 通过管理者去注册一个节点
	err = em.AddEndpoint(context.TODO(), fmt.Sprintf("%s/%s", serviceName, addr), endpoints.Endpoint{Addr: addr}, clientv3.WithLease(lease.ID))
	if err != nil {
		return err
	}
	//etcdClient.KeepAlive(context.TODO(), lease.ID)
    // 保持活跃
	alive, err := etcdClient.KeepAlive(context.TODO(), lease.ID)
	if err != nil {
		return err
	}

	go func() {
		for {
			<-alive
			fmt.Println("etcd server keep alive")
		}
	}()

	return nil
}

func etcdUnRegister(addr string) error {
	log.Printf("etcdUnRegister %s\b", addr)
	if etcdClient != nil {
        // 获得管理者
		em, err := endpoints.NewManager(etcdClient, serviceName)
		if err != nil {
			return err
		}
        // 通过管理者去删除节点
		err = em.DeleteEndpoint(context.TODO(), fmt.Sprintf("%s/%s", serviceName, addr))
		if err != nil {
			return err
		}
		return err
	}

	return nil
}
```



`main.go` ：

```go
package main

import (
	"app/rpc"
	"context"
	"flag"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
)

func main() {
    // ---------- etcd相关操作 ------------
	var port int
	flag.IntVar(&port, "port", 8001, "port")
	flag.Parse()
	addr := fmt.Sprintf("localhost:%d", port)

	// 遇到关闭信号，则从etcd撤销注册
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, syscall.SIGTERM, syscall.SIGINT, syscall.SIGKILL, syscall.SIGHUP, syscall.SIGQUIT)
	go func() {
		s := <-ch
		etcdUnRegister(addr) // 从etcd中撤销注册
		if i, ok := s.(syscall.Signal); ok {
			os.Exit(int(i))
		} else {
			os.Exit(0)
		}
	}()

    // 将本服务注册到etcd
	err := etcdRegister(addr)

	if err != nil {
		panic(err)

	}
    
    // ---------- grpc相关操作 ------------
	lis, err := net.Listen("tcp", addr)

	if err != nil {
		panic(err)
	}

    // grpc设置一个拦截器
	grpcServer := grpc.NewServer(grpc.UnaryInterceptor(UnaryInterceptor()))

    // 注册服务
	rpc.RegisterServerServer(grpcServer, Server{})

	log.Printf("service start port %d\n", port)
	if err := grpcServer.Serve(lis); err != nil {
		panic(err)
	}
}

func UnaryInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		log.Printf("call %s\n", info.FullMethod)
		resp, err = handler(ctx, req)
		return resp, err
	}
}
```



## 负载均衡 : 轮询

`builder.go`

```go
package main

import "google.golang.org/grpc/resolver"

type VinceBuilder struct {
	addrs map[string][]string
}

func (b VinceBuilder) Build(target resolver.Target, cc resolver.ClientConn, opts resolver.BuildOptions) (resolver.Resolver, error) {
	r := &VinceResolver{}
	paths := b.addrs[target.URL.Path]
	addrs := make([]resolver.Address, len(paths))
	for i, s := range paths {
		addrs[i] = resolver.Address{Addr: s}
	}
	cc.UpdateState(resolver.State{Addresses: addrs})
	return r, nil
}

func (c VinceBuilder) Scheme() string {
	return "Vince"
}
```



`resolver.go`

```go
package main

import "google.golang.org/grpc/resolver"

type VinceResolver struct {
}

func (c VinceResolver) ResolveNow(options resolver.ResolveNowOptions) {

}

func (c VinceResolver) Close() {

}
```



`main.go`

```go
package main

import (
	"client/rpc"
	"context"
	"fmt"
	"google.golang.org/grpc/balancer/roundrobin"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/resolver"
	"log"
	"time"

	"google.golang.org/grpc"
)

func main() {
    // 构建builder
	bd := &VinceBuilder{addrs: map[string][]string{"/api": []string{"localhost:8001", "localhost:8002", "localhost:8003"}}}
    // 将其注册
	resolver.Register(bd)
    // 第三个参数就是设置 负载均衡的方式为轮询
	conn, err := grpc.Dial("Vince:///api", grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithDefaultServiceConfig(fmt.Sprintf(`{"LoadBalancingPolicy": "%s"}`, roundrobin.Name)))

	if err != nil {
		fmt.Printf("err: %v", err)
		return
	}

	ServerClient := rpc.NewServerClient(conn)

	for {
		helloRespone, err := ServerClient.Hello(context.Background(), &rpc.Empty{})
		if err != nil {
			fmt.Printf("err: %v", err)
			return
		}

		log.Println(helloRespone, err)
		time.Sleep(500 * time.Millisecond)
	}

}
```

