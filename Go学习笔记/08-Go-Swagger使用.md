# Swagger

> 
>
> # 快速上手文档:[swag使用指南](https://github.com/swaggo/swag/blob/master/README_zh-CN.md)
>
> # 官方文档:[swagger详细使用指南](https://swagger.io/docs/specification/about/)
>
> 



## 安装swag生成docs的命令

```bash
go install github.com/swaggo/swag/cmd/swag@v1.6.7
```

安装之后找到对用的文件目录配置环境变量就可以使用swag命令了



## 项目引入依赖

```bash
# 引入swagger依赖
go get -u github.com/swaggo/gin-swagger
go get -u github.com/swaggo/swag
```



## swagger相关注释

写接口注释

注解	描述

`@Tags` 用于将接口分类

`@Summary`	摘要

`@Description` 描述，可以有多个

`@Param`	参数格式，从左到右分别为：参数名、入参类型、数据类型、是否必填、注释

`@Success`	响应成功，从左到右分别为：状态码、参数类型、数据类型、注释

`@Failure`	响应失败，从左到右分别为：状态码、参数类型、数据类型、注释

`@Router`	路由，从左到右分别为：路由地址，HTTP 方法

`@Produce`	API 可以产生的 MIME 类型的列表，MIME 类型你可以简单的理解为响应类型，例如：json、xml、html 等等

`@Success` 200 {object} model.Tag "成功"

`@Failure` 400 {object} errcode.Error "请求错误"

`@Failure` 500 {object} errcode.Error "内部错误"



如：

```go
// @Tags 用户列表
// @Summary 获取多个标签
// @Produce  json
// @Param name  query string false "标签名称" maxlength(100)
// @Param state query int false "状态" Enums(0, 1) default(1)
// @Param page query int false "页码"
// @Param page_size query int false "每页数量"
// @Success 200 {object} model.Tag "成功"
// @Failure 400 {object} errcode.Error "请求错误"
// @Failure 500 {object} errcode.Error "内部错误"
// @Router /api/v1/tags [get]
func (t Tag) List(c *gin.Context) {}

// @Tags 用户列表
// @Summary 新增标签
// @Produce  json
// @Param name body string true "标签名称" minlength(3) maxlength(100)
// @Param state body int false "状态" Enums(0, 1) default(1)
// @Param created_by body string true "创建者" minlength(3) maxlength(100)
// @Success 200 {object} model.Tag "成功"
// @Failure 400 {object} errcode.Error "请求错误"
// @Failure 500 {object} errcode.Error "内部错误"
// @Router /api/v1/tags [post]
func (t Tag) Create(c *gin.Context) {}

// @Tags 用户列表
// @Summary 更新标签
// @Produce  json
// @Param id path int true "标签 ID"
// @Param name body string false "标签名称" minlength(3) maxlength(100)
// @Param state body int false "状态" Enums(0, 1) default(1)
// @Param modified_by body string true "修改者" minlength(3) maxlength(100)
// @Success 200 {array} model.Tag "成功"
// @Failure 400 {object} errcode.Error "请求错误"
// @Failure 500 {object} errcode.Error "内部错误"
// @Router /api/v1/tags/{id} [put]
func (t Tag) Update(c *gin.Context) {}

// @Tags 用户列表
// @Summary 删除标签
// @Produce  json
// @Param id path int true "标签 ID"
// @Success 200 {string} string "成功"
// @Failure 400 {object} errcode.Error "请求错误"
// @Failure 500 {object} errcode.Error "内部错误"
// @Router /api/v1/tags/{id} [delete]
func (t Tag) Delete(c *gin.Context) {}
```



## 根据注释生成文档

```bash
# 生成api文档，根据注释生成docs
swag init
```



## GIN开起swagger访问地址

```go
docs.SwaggerInfo.BasePath = global.LMS_CONFIG.System.RouterPrefix
Router.GET(global.LMS_CONFIG.System.RouterPrefix+"/swagger/*any",ginSwagger.WrapHandler(swaggerFiles.Handler))
```





