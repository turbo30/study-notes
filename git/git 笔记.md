# git 笔记

### git与svn的区别

svn是集中式版本控制，所有内容都存在一个服务器上。服务器挂了，所有东西都没了。

git是目前世界上最先进的分布式版本控制，每一个用户的电脑都存在一个库，以及服务器也是一个库。任何一个坏了，都不会影响整个内容的更新，可以不需要联网。由于bitkeeper于Linux的合作结束，Linux之父李纳斯·托沃兹花费2周左右的时间开发的。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForGit/理论.png)



### 【==git 初始化==】

`git init` 	在对应文件夹里进行初始化

### 【git bash 安装之后需要进行配置】

#### ---==配置全局签名==(标识提交代码的用户身份)

`git config --global user.name "heroC"`

`git config --global user.email "turbo30@foxmail.com"`

+ `C:\Users\Administrator\.gitconfig` 可查看用户配置
+ `F:\Program Files\Git\etc\gitconfig` 可查看系统配置

#### ---配置局部签名

`git config user.name "heroC"`

`git config user.email "turbo30@foxmial.com"`

+ __【注意】__就近原则，如果设置了局部签名，那么在该目录下提交时，会使用局部签名，没有局部签名则提交全局签名。签名的作用是标识身份     



### 【git 提交文件到本地仓库相关命令】

`git status`	查看文件状态

`git add`	提交文件到暂存区，被追踪

`git rm --cached <file>`	从暂存区撤回\<file\>文件

`git commit -m "..."`	从暂存区提交到本地仓库

`git commit <file> -m "...."`	从暂存区提交\<file>文件到本地仓库，-m引号里是对提交文件的描述，以及为什么要提交该文件 

`git commit -a`	将之前提交过的文件，修改之后可直接提交到本地仓库



### 【版本的前进后退 日志查询】

__多屏：空格向下翻页，b向上翻页，q退出__

`git log --pretty=oneline`

`git log --onelien`

`git reflog`

查看提交记录，--pretty=oneline日志以漂亮的一行显示（oneline显示的信息只展示当前版本以前的版本信息）

`git reset --hard hash索引值`	通过git reflog命令能够查到hash索引值，通过索引值进行回退

`git reset --hard HEAD^^^`	通过调动HEAD指针来退回版本，^有几个就退几个

`git reset --hard HEAD~3`	通过调动HEAD指针来退回版本，~后面的数字就是后退几个

_\*** ^和~都只能后退 ***_
__git reset的参数__

​	__`--soft`只对本地库有影响、__

​	__`--mixed`对本地库和暂存区有影响、__	

​	__`--hard`对本地库暂存区和本地 文件有影响__

##### [ 日志中相关信息 ]

​	(HEAD -> master)：HEAD为指针，指当前处在的版本
​	HEAD@{移动到当前版本需要的步数}：通过git reflog命令可显示

__恢复到对应版本之后__ ：使用`git push -f origin master`进行强制推送，再使用`git pull`拉回到本地



### 【文件内容前后版本比较】

`git diff <file>`	不加参数默认与暂存区比较

`git diff HEAD <file>`	表示对本地库当前指针里的\<file>文件做比较

`git diff HEAD^ <file>`	表示对本地库当前指针上一个版本的\<file>文件做比较



### 【==分支==】

`git branch -v`	查看分支

`git branch [branchName]`	创建一个分支

`git branch -d [branchName]` 删除分支

`git checkout [branchName]`	切换分支

`git merge [branchName]`	将指定的分支上的内容合并过来



### 【解决分支文件进行合并时冲突问题】

如果合并时有冲突，则会在分支名后显示__MERGING__表示正在合并

__步骤__

+ 第一步：vim编辑文件，删除特殊符号
+ 第二步：把文件修改到满意的程度，保存退出
+ 第三步：git add \<file>
+ 第四步：git commit -m "日志信息" (此时commit不能带具体的文件名)



### 【HTTPS本地仓库与远程仓库的连接与推送】

https是对应远程仓库的地址

`git remote add [name] [https]`	表示添加设置保存远程仓库地址，并取名为[name]

`git remote remove [name]` 删除指定的远程连接

`git push [name] [branch]`	将本地仓库branch支上的文件传输到远程仓库地址是存储了的[name]



### 【==克隆==下载远程仓库的文件】

`git clone [https]`	完整的把远程仓库下载到本地；创建远程地址别名，初始化本地库



### 【抓取远程仓库的文件并合并】

+ 方法1
  - `git fetch [name] [branch]`	抓取远程仓库地址http或者已存储了的[name]，上的[branch]分支上的内容
  - `git merge [name/branch]`     将远程仓库的分支合并过来
+ 方法2
  - `git pull [name] [branch]`    抓取远程仓库并进行合并



`git pull origin master --allow-unrelated-histories` 	强行抓取，将两个不相干的库合并到一起



### 【==SSH==本地仓库与远程仓库的连接推送】

添加https的ssh是对应远程仓库的地址

`rm -r .ssh/`	删除ssh目录

`ssh-keygen -t rsa -C [email]`	生成ssh公钥

公钥生成的文件在`C:\Users\Administrator\.ssh\id_rsa.pub`

在远程仓库添加ssh公钥即可。

创建一个ssh本地https `git remote add [name] [https]`

ssh公钥可免登录



### 【VIM】

`set nu`	可在vim编辑器中显示行标

`cd ..`	返回上一级

`mkdir <file>`	创建目录



### 【HASH】

hash可以用于文件的校验，保证数据的完整性
因为hash在同一种算法同一个文件进行运算出来的结果是相同的，如果结果不同，说明文件不一样



### 【查内容】

`tail -n 3 <flie.txt>`	可以查到最后3行的内容
`cat <file.txt>`	查看某文件内容



### 【删文件】

`rm <file>`

删除了文件之后，`git commit-m "..."`

然后进行 push 提交到远程库



### 【push失败解决办法】

`git pull --rebase origin master`

`git push -u origin master`



### 【删除文件的恢复】

+ 删除文件只在本地恢复

  使用`git reset [hash索引值]`退到有该文件的版本，使用`git push -f origin master`进行强制推送，再使用`git pull`拉回到本地

+ 删除文件在远程库恢复，拉回本地

  使用`git reset [hash索引值]`退到有该文件的版本，使用`git checkout <file>`恢复到本地文件夹中

  

### 【git rebase 修改commit】

使用`git log --oneline` 找到对应的记录

比如这里要修改32dfaf1

![image-20230224145136548](https://gitee.com/turbo30/study_pic/raw/master/pictureForGit/image-20230224145136548.png)  

使用`git rebase -i hash值`， 比如这里要修改 32dfaf1 的提交记录为update，一定要定位到修改记录的前一条

则使用命令`git rebase -i eef7b9b` 则出现如下图：

![image-20230224145416422](https://gitee.com/turbo30/study_pic/raw/master/pictureForGit/image-20230224145416422.png)

此时将要修改的记录的`pick`修改为`edit` 保存退出，会给出以下提示：

![image-20230224145519407](https://gitee.com/turbo30/study_pic/raw/master/pictureForGit/image-20230224145519407.png)

> git commit --amend 命令是：需要修改提交记录就使用这个命令
>
> git rebase --continue 命令是：当满意你的改变之后，也就是完成所有修改之后就执行这个命令进行确认

我这里执行`git commit --amend`进入修改页面，会逐一将标记为edit的记录进行修改

![image-20230224145727437](https://gitee.com/turbo30/study_pic/raw/master/pictureForGit/image-20230224145727437.png)

修改后保存退出。当所有都修改完之后，执行`git rebase --continue`即可
