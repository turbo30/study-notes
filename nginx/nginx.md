# Nginx



## 一、Nginx简介

Nginx是一款轻量级的Web 服务器/反向代理服务器及电子邮件（IMAP/POP3）代理服务器，在BSD-like 协议下发行。其特点是占有内存少，并发能力强，事实上nginx的并发能力在同类型的网页服务器中表现较好。专为性能优化而开发。能够支持高达 50,000 个并发连接数的响应。



### 1、反向代理

#### . 正向代理

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForNginx/正向代理.png)

#### . 反向代理

真实目标服务器的ip地址被隐藏，不会被暴露。

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForNginx/反向代理.png)



### 2、负载均衡

单个服务器解决不了大量的请求，解决方法就是增加服务器的数量，将请求分发到各个服务器上，将原先请求集中到一个服务器上改为将请求分发到多个服务器上，减轻每个服务器的负担。这就是负载均衡。



### 3、动静分离

将动态资源(jsp、servlet)和静态资源(html、css、js)分开部署到不同的服务器。



## 二、Nginx相关操作

必须进入nginx目录下的sbin目录，操作可执行的nginx



### 1、Nginx启动

```
./nginx
```



### 2、Nginx关闭

```
./nginx -s stop
```



### 3、Nginx版本号

```
./nginx -v
```



### 4、Nginx重加载

```
./nginx -s reload
```



## 三、Nginx配置文件

配置文件位置：在nginx目录下的conf目录下的nginx.conf文件

### . nginx.conf

配置文件中有三大板块：**全局块**、**events块**、**http块**

```conf
# 全局块
ser  www www;
worker_processes auto; # nginx服务器并发处理值，值越大可并发处理的数量就越多。最好与cpu数相等最为合适
error_log  /www/wwwlogs/nginx_error.log  crit;
pid        /www/server/nginx/logs/nginx.pid;
worker_rlimit_nofile 51200;

# events块，主要影响nginx与用户网络之间的连接
events
    {
        use epoll;
        worker_connections 51200; # nginx可支持最大连接数，灵活配置
        multi_accept on;
    }

# http块：http全局块、server块
http
    {
        include       mime.types;
                #include luawaf.conf;

                include proxy.conf;

                # aliyun need add proxy_hide_header
                proxy_hide_header X-Powered-By;
                proxy_hide_header Server;

        default_type  application/octet-stream;

        server_names_hash_bucket_size 512;
                client_header_buffer_size 32k;
        large_client_header_buffers 4 32k;
        client_max_body_size 50m;

        sendfile   on;
        tcp_nopush on;

        keepalive_timeout 60;

        tcp_nodelay on;

        fastcgi_connect_timeout 300;
        fastcgi_send_timeout 300;
        fastcgi_read_timeout 300;
        fastcgi_buffer_size 64k;
        fastcgi_buffers 4 64k;
        fastcgi_busy_buffers_size 128k;
        fastcgi_temp_file_write_size 256k;
                fastcgi_intercept_errors on;

        gzip on;
        gzip_min_length  1k;
        gzip_buffers     4 16k;
        gzip_http_version 1.1;
        gzip_comp_level 2;
        gzip_types     text/plain application/javascript application/x-javascript text/javascript text/css application/xml;
        gzip_vary on;
        gzip_proxied   expired no-cache no-store private auth;
        gzip_disable   "MSIE [1-6]\.";

        limit_conn_zone $binary_remote_addr zone=perip:10m;
                limit_conn_zone $server_name zone=perserver:10m;

        server_tokens off;
        access_log off;

    server
        {
            listen 80; # nginx监听的端口号
            server_name phpmyadmin;
            index index.html index.htm index.php;
            root  /www/server/phpmyadmin;

            #error_page   404   /404.html;
            include enable-php.conf;

            # aliyun need add ssl_protocols
            ssl_protocols TLSv1.2;

            location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$
            {
                expires 30d; # 静态文件图片，缓存到浏览器，30天后过期，在30天内如果静态资源没有改变，那么会返回304使用缓存资源，否则返回200重新到服务器下载资源
                autoindex on; # 表示列出当前文件夹中的所有内容
            }

            location ~ .*\.(js|css)?$
            {
                expires 12h;
            }

            location ~ /\.
            {
                deny all; # 禁止所有
            }

            access_log  /www/wwwlogs/access.log;
        }
		include /www/server/panel/vhost/nginx/*.conf;
}
```



### 1、关于location语法规则

1.location 是在 server 块中配置。

2.可以根据不同的 URI 使用不同的配置（location 中配置），来处理不同的请求。

3.location 是有顺序的，会被第一个匹配的 location 处理。



| 语法规则：             |                                                              |
| ---------------------- | ------------------------------------------------------------ |
| location = /uri        | =开头表示精确匹配，只有完全匹配上才能生效                    |
| location ^~ /uri       | ^~ 开头对URL路径进行前缀匹配，并且在正则之前                 |
| location ~ 正则表达式  | ~开头表示区分大小写的正则匹配                                |
| location ~*正则表达式  | ~*开头表示不区分大小写的正则匹配                             |
| location !~ 正则表达式 | !~区分大小写不匹配的正则                                     |
| location !~*正则表达式 | !~*不区分大小写不匹配的正则                                  |
| location /uri          | 不带任何修饰符，也表示前缀匹配，但是在正则匹配之后           |
| location /             | 通用匹配，任何未匹配到其它location的请求都会匹配到，相当于switch中的default |
| location @名称         | nginx内部跳转                                                |



### 2、location匹配顺序

(location =) > (location 完整路径) > (location ^~ 路径) > (location ~,~* 正则顺序) > (location 部分起始路径) > (/)

 

1.首先匹配=

2.其次匹配^~

3.再其次按照配置文件的顺序进行正则匹配

4.最后是交给/进行通用匹配

> 提示：当有匹配成功时，立刻停止匹配，按照当前匹配规则处理请求



### 3、location中root、alias、index

#### root属性

```
server{
    listen 80;
    server_name www.willhero.com;

    location /img {
        root /data/;  //设置虚拟主机主目录相对路径
        index  index.html;  //设置虚拟主机默认主页
    }
}
```

这个配置表示输入` www.willhero.com:80/img` 时会访问本机的/data/img/ 目录去找文件，默认主页为index.html

如果是`root img/`; 那么是在nginx的安装目录下的data目录，比如/usr/local/nginx/data/img/目录找文件



#### alias属性

```
server{
        listen 80;
        server_name www.willhero.com;

        location ^~ /img/ {
            alias /var/www/image/;                        
            index index.html;     
        }
}
```

当输入`www.willhero.com:80/img/a.jpg`时，会去本机的/var/www/image/找文件，需要注意的是用alias时，目录/var/www/image/; **最后的 / 一定要有**，而用root时可有可无。注意这里是image，alias会把location后面配置的路径丢弃掉，把当前匹配到的目录指向到指定的目录，即/var/www/image/a.jpg。



#### index属性

配置网站的初始页



## 四、Nginx配置实例



### 1、【反向代理1】

要求：打开浏览器，输入`www.willhero.com`跳转到linux系统tomcat主页面



#### (1) 在windows配置hosts文件

进入C:\Windows\System32\drivers\etc 找到hosts文件，

将`服务器的ip地址`与`www.willhero.com` 配置好，如：

```
192.168.1.30 www.willhero.com
```

> 需要注意的是：如果你使用的是阿里云国内服务器，通过hosts修改的域名去指向阿里云服务器ip地址，只能通过该域名访问一次，随即就会被阿里云把该域名给屏蔽掉。



#### (2) 防火墙要开启tomcat的端口号

```bash
#开启8080端口号
firewall-cmd --zone=public --list-port=8080/tcp --permanent

#重启防火墙
systemctl restart firewalld.service

#查看端口号是否开启
firewall-cmd --list-ports
```



#### (3) 修改nginx的代理配置

进入nginx目录下的conf目录，打开nginx.conf文件进行编辑：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForNginx/反向代理访问tomcat.png)

```
location / {
	proxy_pass http://127.0.0.1:8080;
}

# 如果要访问tomcat下的某个目录项目，比如访问tocat下我的test项目，最后一定要加/：
location / {
	proxy_pass http://127.0.0.1:8080/test/;
}
```



#### (4) 重加载nginx，访问即可

进入nginx目录下的sbin目录，进行重加载：

```
./nginx -s reload
```



在浏览器访问`www.willhero.com`就可以成功nginx的反向代理。



### 2、【反向代理2】

要求：

通过访问`http://www.willhero.com/dev` 进入到服务端口为8081的tomcat服务器

通过访问`http://www.willhero.com/por` 进入到服务端口为8082的tomcat服务器



#### (1) 基本准备

本地配置好映射路径，linux服务器中将两个tomcat的启动端口和关闭端口都改了。



#### (2) nginx的配置

进入nginx目录下的conf目录，打开nginx.conf文件在`http块`下的`server块`中添加如下配置：

```
location ~ /dev/ {
	proxy_pass http://127.0.0.1:8081;
}

location ~ /por/ {
	proxy_pass http://127.0.0.1:8082;
}
```



#### (3) 重加载nginx，访问即可

进入nginx目录下的sbin目录，进行重加载：

```
./nginx -s reload
```



在浏览器访问`www.willhero.com`就可以成功nginx的反向代理。



### 3、【负载均衡】



#### (1) 分配策略

默认策略：轮询 (每个请求按照时间顺序，进行逐一分配到每个服务器上)

```
ip_hash：每个请求按访问ip的hash结果分配，这样每个访客固定访问一个后端服务器，可以解决session的问题。

hash $request_uri：按访问url的hash结果来分配请求，使每个url定向到同一个后端服务器，后端服务器为缓存时比较有效。

fair(第三方)：按后端服务器的响应时间来分配请求，响应时间短的优先分配。

weight(权重)：指定轮询几率，weight和访问比率成正比，用于后端服务器性能不均的情况。
```

upstream 还可以为每个设备设置状态值，这些状态值的含义分别如下：

**down** 表示单前的server暂时不参与负载。

**weight** 默认为1.weight越大，负载的权重就越大。

**max_fails** ：允许请求失败的次数默认为1.当超过最大次数时，返回proxy_next_upstream 模块定义的错误.

**fail_timeou**t : max_fails次失败后，暂停的时间。

**backup**： 其它所有的非backup机器down或者忙的时候，请求backup机器。所以这台机器压力会最轻。



#### (2) 案例

在http块中加入upstream块，取名为myserver：

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForNginx/负载均衡配置.png)



### 4、【动静分离】



将静态资源统一放到一个目录文件夹下，然后根据访问路径，找到对应的location，根据location的root，去访问指定资源，返回给浏览器。将静态资源直接由nginx去访问管理。

动态资源，通过location代理，访问tomcat获取。

```
location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$
{
    root  /www/server/static/image/;
    expires  3d;
    # autoindex on;
}
# 访问图片，会到本服务器的 /www/server/static/image/ 目录下去找对应的静态资源返回给浏览器
```



### 5、【高可用】

[Nginx高可用视频讲解][https://www.bilibili.com/video/BV11J41197R7?p=14]

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForNginx/高可用.png)

对外使用虚拟ip，keepalived软件用于检测主服务器是否宕机，如果nginx主服务器宕机了，就会将虚拟ip绑定到备用nginx服务器上，使得访问备用nginx服务器。

在线安装keepalived：

```
yum install keepalived -y
```

安装后，在/etc/keepalived/ 下有一个配置文件



## 五、工作原理

![](https://gitee.com/turbo30/study_pic/raw/master/pictureForNginx/工作原理.png)

一个Nginx启动之后，有一个master和多个worker。

当有请求时，master接收这个请求，会通知所有闲着的worker去争抢这个请求。接收到了这个请求的worker就会对该请求负责到底，不会再去争抢新的请求，除非该请求已被操作完成。一个master多个worker的好处，可以轻松实现热部署。每个worker都是独立的进程，多个worker不会操作请求的中断。

#### worker数配置多少合适？

Nginx和Redis都是使用了io多路复用。**worker数与服务器的linux的cpu数相等最合适。**



#### 一个请求会用到多少个连接数？

Nginx接收到一个请求，可能会用到**2或者4个连接数**(worker_connections)。

当该请求只访问静态资源时，由于动静分离部署，则直接从服务器获取静态资源返回，即请求连接到worker，worker返回静态资源，产生2个连接数。

当该请求需要访问动态资源，访问tomcat时，即请求连接到worker，worker连接到tomcat，tomcat返回资源给worker，worker返回资源给浏览器，产生4个连接数。



#### Nginx有1个master，4个worker，每个worker最大连接数是1024，最大支持连接数是多少个？

- 对于只访问静态资源，最大支持连接数：4*1024/2个

  **worker_processes * worker_connections / 2**

- 对于只访问动态资源，最大支持连接数：4*1024/4个

  **worker_processes * worker_connections / 4**